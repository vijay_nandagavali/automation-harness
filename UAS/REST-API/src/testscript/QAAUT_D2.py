'''
Created on Jan2 24 2017

@author: Deepanshu
'''

import unittest

from resourcelibrary import clients, backups,login, logout,jobs, restore
from testdata import clients_testdata, backups_testdata, restore_testdata
from util import reportController, logHandler,clientMessageUtilities
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
import paramiko



class QAAUT_D2(unittest.TestCase):    
    def __init__(self, exeID, uebInfra, machineInfra): 
        self.executionId = exeID
        '''
        Initialize UEB Configuration test steps
        '''
        
        self.uebInfra=uebInfra
        self.machineInfra=machineInfra 
        '''
        Initialize machine environment
        ''' 
        self.name=machineInfra["linuxname"] if "linuxname" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["linuxipAddress"] if "linuxipAddress" in machineInfra.keys() else ""
        self.flavour=machineInfra["linuxflavour"] if "linuxflavour" in machineInfra.keys() else ""
        # self.includeList=machineInfra["linuxincludeList"] if "linuxincludeList" in machineInfra.keys() else ""
        # self.restoreFolder=machineInfra["linuxrestoreFolder"] if "linuxrestoreFolder" in machineInfra.keys() else ""
        self.includeList = "/root/Backup_folder"
        self.restoreFolder = "/root/Restore_folder"

        
        reportController.initializeReport(type(self).__name__, "Verify if multiple restore for the same data on different locations works."+ " : " + str(self.flavour))
        logHandler.logFile(self.executionId)
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify if multiple restore for the same data on different locations works."+ " : " + str(self.flavour))        
    def runTest(self):        
        try:
            osType = ""
            appliance_ip=self.uebInfra["Mainurl"]
            addedClientFlag = 0
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            clientMessageUtilities.SetupLinux(self.ipAddress)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(appliance_ip, self.name)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postLinuxClient(appliance_ip,token, self.name, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.linux_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted,   
                                                clients_testdata.linux_IsDefault, self.ipAddress)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Addition of client" , ExecutionStatus.PASS)
                print "client add response: "+str(actualResponse)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            else:
                addedClientFlag = 1
            
            '''
            Add Files on linux
            '''
            if(self.includeList!=""):
                clientMessageUtilities.addCustomSizedfilesOnLinux(self.ipAddress,self.includeList+"/",3,5,"M")
            # Step 3: Send the PUT request
            actualResponse = backups.putBackupClientIncList(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType   )
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            print "Actual Response for put backup - " + str(actualResponse)
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Backup Job Not successful" )
                
                
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            print "returned backup status" + backupJobStatus
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Backup successful")
                print "Backup Successful"
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Backup Failed with jobID:"+str(strJobId)
                
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            print "Backup ID based on jobid from Response - " + str(backupID)
            
            # create locations for restore
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(self.ipAddress, username='root', password='unitrends1')
            logHandler.logging.info("Client connection done")
            print "start"
            logHandler.logging.info("addition of first folder location Started")
            buildCommand = " mkdir /root/Restore_folder_1"
            logHandler.logging.info("creating first folder: " +buildCommand)
            print buildCommand
            stdin, stdout, stderr = client.exec_command(buildCommand)
            logHandler.logging.info("Command executed")
            logHandler.logging.info("addition of first folder location finished")
            
            logHandler.logging.info("addition of second folder location Started")
            buildCommand = " mkdir /root/Restore_folder_2"
            logHandler.logging.info("creating second folder: " +buildCommand)
            print buildCommand
            stdin, stdout, stderr = client.exec_command(buildCommand)
            logHandler.logging.info("Command executed")
            logHandler.logging.info("addition of second folder location finished")
            
            # Step 3: Send API for restore first location
            actualResponse = restore.postLinuxRestore(appliance_ip, token, backupID, clientId, self.includeList, str(self.restoreFolder)+"_1/", restore_testdata.sid)
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Triggering Restore of Client at first location" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print "Restore JobId - " + str(strJobId)
            
            #Check the status of Restore
            restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
             
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful at first location")
                reportController.addTestStepResult("Restore Successful at first location" , ExecutionStatus.PASS)
                clientMessageUtilities.verifyRestoreFolderAgainstBackupFolderOnLinux("/root/Backup_folder", "/root/Restore_folder_1/root/Backup_folder", self.ipAddress)
                logHandler.logging.info("checksum verified at first location")
                reportController.addTestStepResult("Checksum Verified at first location" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Restore not successful at first location", ExecutionStatus.FAIL)
            
            
            # Step 3: Send API for restore second location
            actualResponse = restore.postLinuxRestore(appliance_ip, token, backupID, clientId, self.includeList, str(self.restoreFolder) + "_2/", restore_testdata.sid)
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Triggering Restore of Client at second location" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print "Restore JobId - " + str(strJobId)
            
            #Check the status of Restore
            restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
             
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("Restore Successful at second location" , ExecutionStatus.PASS)
                clientMessageUtilities.verifyRestoreFolderAgainstBackupFolderOnLinux("/root/Backup_folder", "/root/Restore_folder_2/root/Backup_folder", self.ipAddress)
                logHandler.logging.info("checksum verified at first location")
                reportController.addTestStepResult("Checksum Verified at first location" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Restore not successful at second location", ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
              
            
        except Exception as e:
            print "found exception"
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            # remove locations of restore
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(self.ipAddress, username='root', password='unitrends1')
            logHandler.logging.info("Client connection done")
            print "start"
            logHandler.logging.info("removal of restore folder location Started")
            buildCommand = " rm -rf /root/Restore_folder_1 /root/Restore_folder_2 /root/Backup_folder /root/Restore_folder"
            logHandler.logging.info("removal of folder location: " +buildCommand)
            print buildCommand
            stdin, stdout, stderr = client.exec_command(buildCommand)
            logHandler.logging.info("Command executed")
            logHandler.logging.info("removal of restore folder location finished")
            
            
            print "addedClientFlag: " + str(addedClientFlag)
            #if(addedClientFlag):
                #clients.deleteClients(appliance_ip, token, clientId)
                #reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
             
            #Logout
            print "log out started"
            logoutResponse = logout.doUserLogout(appliance_ip, token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            print "generating report summary"
            reportController.generateReportSummary(appliance_ip, self.executionId)
'''
Created on Feb 6 , 2016

@author: gayatri.l
'''

import unittest
from resourcelibrary import clients, backups,login, logout,jobs, restore
from testdata import clients_testdata, backups_testdata, restore_testdata
from util import reportController, logHandler,clientMessageUtilities, featureHandler
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from Constants import ExecutionStatus
from resourcelibrary.backups import putBackupClientIncList,putBackupClientIncListWindows
import paramiko
from util.clientMessageUtilities import createFoldersWindows
from config.Constants import ExecutionStatus


class QAAUT_Win004(unittest.TestCase):
    def __init__(self, exeID, uebInfra, machineInfra):
        
        self.executionId = exeID
        
        '''
        Initialize UEB Configuration test steps
        '''
        self.uebInfra = uebInfra
        self.machineInfra = machineInfra
        
        '''
        initialize machine EnvironmentError
        '''
#         self.name="Win_2016"
#         self.ipAddress = "192.168.197.200"
        self.name=machineInfra["windowsname"] if "windowsname" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["windowsipAddress"] if "windowsipAddress" in machineInfra.keys() else ""
        self.flavour=machineInfra["windowsflavour"] if "windowsflavour" in machineInfra.keys() else ""
        self.includeList = "c:\Backup_folder"
        self.restoreFolder = "c:\Restore_folder"
        
        reportController.initializeReport(type(self).__name__, "Verify that Full backup and restore of client with include list, with encryption on" + " : " + str(self.flavour))
        logHandler.logFile(self.executionId)
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify that Full backup and restore of client with include list, with encryption on" + " : " + str(self.flavour))
        
    def runTest(self):
        try:
            reportController.tcExecutionStatus(ExecutionStatus.UNKNOWN)
                
            osType = ""
            appliance_ip = self.uebInfra["Mainurl"]
            print appliance_ip
            addedClientFlag = 0
            testCaseStep = "login"
            #login and get the authentication token
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                
            #create new folders on windows machine  
            #clientMessageUtilities.createFoldersWindows(self.ipAddress, self.includeList, self.restoreFolder) 
            #logHandler.logging.info("Folders added sucessfully")
            testCaseStep = "Add few files on windows machine"
            #Add few files on windows machine
            if (self.includeList!=''):
                clientMessageUtilities.addCustomSizedfiles_Window(self.ipAddress, self.includeList+"/", 3, "Backup", 5, "M")
             
            print "Check encryption status on appliance"
            logHandler.logging.info("Checking encryption status on appliance")
            
            testCaseStep = "check Encryption on Appliance"     
            encryption_status = featureHandler.Get_Encryption_Status(appliance_ip, token)
            if encryption_status == "off":
                print "encryption_status is off"
                logHandler.logging.info("Encryption Status is off")
                # Set encryption on
                testCaseStep = "Set encryption on"  
                logHandler.logging.info("setting encryption status ON")     
                featureHandler.Enable_Encryption_Appliance(appliance_ip, "unitrends1", token)
                logHandler.logging.info("encryption status set to ON")
                reportController.addTestStepResult("Appliance Encryption status set to ON", ExecutionStatus.PASS)
                print "encryption status set to ON"
            else:
                print "check if the encryption status is on"
                encryption_status = featureHandler.Get_Encryption_Status(appliance_ip, token)
                if encryption_status == "on":
                    print "encryption_status is on"
                    logHandler.logging.info("Encryption Status is on")
                    reportController.addTestStepResult("Appliance Encryption status : ON", ExecutionStatus.PASS)
            
            # check inline dedup appliance
            testCaseStep = "check inline dedup appliance"
            appliance_dedup = featureHandler.Check_Inline_dedup(appliance_ip)
            if appliance_dedup:
                print "Inline Deduplication is enabled"
                logHandler.logging.info("Inline Deduplication is enabled")
            else:
                print "Inline Deduplication is disabled"
                logHandler.logging.info("Inline Deduplication is disabled")
                print "Enabling Inline Deduplication"
                logHandler.logging.info("Enabling Inline Deduplication")
                
                testCaseStep = "inline dedup to on"
                #set inline dedup to on
                dedup_information = featureHandler.set_inline_dedup(appliance_ip, token)
                logHandler.logging.info("Inline Dedup set for appliance")
                reportController.addTestStepResult("Inline Dedup set for appliance", ExecutionStatus.PASS)
                print dedup_information
                
            #Send request to update a client
            #Check if client already exist
            #clientId = vclients.getSpecificClientByNameDB(self.ipAddress, self.name)
            clientId = vclients.getSpecificClientByNameDB(appliance_ip,self.name)
            print clientId
            logHandler.logging.info("Client ID returned from DB: "+str(clientId))
            if (clientId==0):
                # Add the client as it does not exist
                addedClientFlag =1
                print "get the actual response"
                testCaseStep = "Add Client"
                is_encrypted = True
                actualresponse = clients.postWindowClient(appliance_ip, token,self.name, osType, clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, clients_testdata.use_ssl, clients_testdata.win_InstallAgent, clients_testdata.is_auth_enabled, is_encrypted, clients_testdata.win_IsDefault, self.ipAddress, clientType="Windows")
                
 
                logHandler.logging.info("Actual Response - "+str(actualresponse))
                reportController.addTestStepResult("Addition of windows client", ExecutionStatus.PASS)
                #Get the client ID
                clientId = clients.getPostClientsRequestResponse(actualresponse, clients_testdata.keyForId)
                print clientId
            else:
                # set encryption to ON for already added client
                testCaseStep = "set encryption to ON for already added client"
                enc_status = featureHandler.Set_Encryption_On_Asset(appliance_ip, self.name, token)
                if enc_status:
                    print " encryption set to ON for already added client"
                    logHandler.logging.info("encryption set to ON for already added client")
                    reportController.addTestStepResult("Set encryption status to ON", ExecutionStatus.PASS)
                else:
                    print " failed to set encryption on on added client"
                    logHandler.logging.info("failed to set encryption on on added client")
                    reportController.addTestStepResult("Set encryption is OFF", ExecutionStatus.FAIL)
                addedClientFlag =1
        
            #Trigger backup of client with include list
            testCaseStep = "Trigger backup"
            actualResponse = putBackupClientIncListWindows(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType, backups_testdata.excudeSystemState)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            # logHandler.logging.info("Actual Response - " + str(actualresponse))
            print "Actual Response for put backup - " + str(actualResponse)
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Backup Job Not successful" )
            
            testCaseStep = "Check the status of backup"
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Full Backup successful")
                print "Full Backup Successful"
                reportController.addTestStepResult("Full Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Full Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Full Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Full Backup Failed with jobID:"+str(strJobId)
              
              
            #Get Backup No from jobid
            testCaseStep = "Get Backup No from jobid"
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            print "Backup ID based on jobid from Response - " + str(backupID)
              
                
            # check encryption status for backup
            testCaseStep = "check encryption status for backup"
            if featureHandler.check_Encrytion_of_backup(appliance_ip, backupID):
                logHandler.logging.info("Backup Encrypted" ) 
                reportController.addTestStepResult("Backup Encrypted" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup Not Encrypted" + str(backupID) ) 
                reportController.addTestStepResult("Backup Not Encrypted" , ExecutionStatus.FAIL)
            
            # check inline dedup status for backup
            testCaseStep = "check inline dedup status for backup"
            if featureHandler.verifySuccessfulDedupForABackup(appliance_ip, backupID):
                print "Backup inline deduplicated"
                logHandler.logging.info("Backup inline deduplicated" + str(backupID)) 
                reportController.addTestStepResult("Backup inline deduplicated" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup NOT inline deduplicated" + str(backupID)) 
                print "Backup NOT inline deduplicated"
                reportController.addTestStepResult("Backup NOT inline deduplicated" , ExecutionStatus.FAIL)
            
            testCaseStep = "restore backup"
            # Send API for restore of backup
            print "differential backup for job_id"+ str(backupID)
            actualResponse = restore.postWindowsRestore(appliance_ip, token, backupID, clientId, self.includeList, str(self.restoreFolder), restore_testdata.sid)
            
    
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Triggering Restore of Client" , ExecutionStatus.PASS)
            
            testCaseStep = "Extract jobid from response"
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print " differential backup Restore JobId - " + str(strJobId)
            
            #Check the status of Restore
            testCaseStep = "Check the status of Restore"
            restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
             
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("Restore Successful" , ExecutionStatus.PASS)
                print "restore job completed and successful"
                
                testCaseStep = "Verify if backup files are restore"
                #Verify if backup files are restore
                itemsinbackupFolder = clientMessageUtilities.verifyRestoreFileCount_Windows(self.includeList, self.ipAddress)
                itemsinrestoreFolder = clientMessageUtilities.verifyRestoreFileCount_Windows(self.restoreFolder + "\Backup_folder", self.ipAddress)
                if itemsinbackupFolder == itemsinrestoreFolder:
                    print "items matched"
                    logHandler.logging.info("Restore verification passed")
                    reportController.addTestStepResult("Restore verification", ExecutionStatus.PASS)
                else:
                    print "items not matched"
                    logHandler.logging.info("Restore verification failed")
                    reportController.addTestStepResult("Restore verification", ExecutionStatus.FAIL)
                    
                
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Restore not successful", ExecutionStatus.FAIL)
 
         
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            print "found exception"
            reportController.addTestStepResult(testCaseStep, ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))  
        finally:
            #Logout
            print "log out started"
            logoutResponse = logout.doUserLogout(appliance_ip,token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            print "generating report summary"
            reportController.generateReportSummary(appliance_ip, self.executionId)
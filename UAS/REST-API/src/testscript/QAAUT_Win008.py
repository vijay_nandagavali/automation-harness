'''
Created on Feb 6 , 2016

@author: gayatri.l
'''

import unittest
from resourcelibrary import clients, backups,login, logout,jobs, restore
from testdata import clients_testdata, backups_testdata, restore_testdata
from util import reportController, logHandler,clientMessageUtilities, featureHandler
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from Constants import ExecutionStatus
from resourcelibrary.backups import putBackupClientIncList,putBackupClientIncListWindows
import paramiko
from util.clientMessageUtilities import createFoldersWindows
from config.Constants import ExecutionStatus


class QAAUT_Win008(unittest.TestCase):
    def __init__(self, exeID, uebInfra, machineInfra):
        
        self.executionId = exeID
        
        '''
        Initialize UEB Configuration test steps
        '''
        self.uebInfra = uebInfra
        self.machineInfra = machineInfra
        
        '''
        initialize machine EnvironmentError
        '''
#         self.name="Win_2016"
#         self.ipAddress = "192.168.197.200"
        self.ipAddress=machineInfra["windowsipAddress"] if "windowsipAddress" in machineInfra.keys() else ""
        self.name=machineInfra["windowsname"] if "windowsname" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["windowsipAddress"] if "windowsipAddress" in machineInfra.keys() else ""
        self.flavour=machineInfra["windowsflavour"] if "windowsflavour" in machineInfra.keys() else ""
        self.includeList = "C:\Backup_folder"
        self.restoreFolder = "C:\Restore_folder"
        
        reportController.initializeReport(type(self).__name__, "Verify incremental backup and restore with No Dedup with Compression IZ4, Encryption Off is successful." + " : " + str(self.flavour))
        logHandler.logFile(self.executionId)
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\Verify incremental backup and restore with No Dedup with Compression IZ4, Encryption Off is successful." + " : " + str(self.flavour))
        
    def runTest(self):
        try:
            
            reportController.tcExecutionStatus(ExecutionStatus.UNKNOWN)
                
            osType = ""
            appliance_ip = self.uebInfra["Mainurl"]
            print appliance_ip
            addedClientFlag = 0
            #login and get the authentication token
            testCaseStep = "login"
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                
            #create new folders on windows machine  
            testCaseStep = "create new folders on windows machine "
            clientMessageUtilities.createFoldersWindows(self.ipAddress, self.includeList, self.restoreFolder) 
            logHandler.logging.info("Folders added sucessfully")
            
            #Add few files on windows machine
            testCaseStep = "Add few files on windows machine"
            if (self.includeList!=''):
                clientMessageUtilities.addCustomSizedfiles_Window(self.ipAddress, self.includeList+"/", 3, "Backup", 5, "M")
             
            # in order to apply LZ4 compression, we need to disable SISCompression on appliance:
            #check sisCompression status on appliance
            testCaseStep = "check sisCompression status on appliance"
            sisCompression_status = featureHandler.check_SISCompression_Status(appliance_ip)
            print sisCompression_status
            if sisCompression_status == "useSIScompression=No":
                print "SIScompression state is already set to "+ sisCompression_status
                logHandler.logging.info("SIScompression state is already set to " + sisCompression_status)
                reportController.addTestStepResult("Disable SISCompression", ExecutionStatus.PASS)
            else:
                #set SISCompression to off
                testCaseStep = "set SISCompression to off"
                featureHandler.change_SISCompression_status(appliance_ip, "useSIScompression=No")
                #check if SISCompression is set to off
                testCaseStep = "check if SISCompression is set to off"
                sisCompression_status = featureHandler.check_SISCompression_Status(appliance_ip)
                if sisCompression_status == "useSIScompression=No":
                    print "SISCompression status set to OFF"
                    logHandler.logging.info("SISCompression Status is " + sisCompression_status)
                    reportController.addTestStepResult("Disable SISCompression", ExecutionStatus.PASS)
                else:
                    print "SISCompression status set to ON"
                    logHandler.logging.info("SISCompression Status is " + sisCompression_status)
                    reportController.addTestStepResult("Disable SISCompression", ExecutionStatus.FAIL)   
            
            # Set File No Dedup on the appliance:
            testCaseStep = "Set File No Dedup on the appliance"
            featureHandler.set_no_dedup(appliance_ip, token)
            logHandler.logging.info("No Dedup set for appliance")
            reportController.addTestStepResult("No Dedup configuration",ExecutionStatus.PASS)
               
            #check compression type on appliance
            testCaseStep = "check compression type on appliance"
            compression_status= featureHandler.check_compression_mode(appliance_ip, "D2DBackups")
            if compression_status == "lz4":
                print "Compression on appliance " + compression_status
                logHandler.logging.info("Compression on appliance " + compression_status)
                reportController.addTestStepResult("Compression on appliance " + compression_status, ExecutionStatus.PASS)
            else:
                #set compression on appliance to lz4
                testCaseStep = "set compression on appliance to lz4"
                compression_status = featureHandler.setCompressionMode(appliance_ip, "D2DBackups", "lz4")
                #check if compression mode is set to lz4
                testCaseStep = "check if compression mode is set to lz4"
                compression_status= featureHandler.check_compression_mode(appliance_ip, "D2DBackups")
                if compression_status == "lz4":
                    print "Compression on appliance " + compression_status
                    logHandler.logging.info("Compression on appliance " + compression_status)
                    reportController.addTestStepResult("Compression on appliance " + compression_status, ExecutionStatus.PASS)
                else:
                    print ("Compression on appliance is " + compression_status)
                    logHandler.logging.info("Compression on appliance is " + compression_status)
                    reportController.addTestStepResult("Compression on appliance is " + compression_status, ExecutionStatus.FAIL)
                     
            #Send request to update a client
            #Check if client already exist
            #clientId = vclients.getSpecificClientByNameDB(self.ipAddress, self.name)
            clientId = vclients.getSpecificClientByNameDB(appliance_ip,self.name)
            print clientId
            logHandler.logging.info("Client ID returned from DB: "+str(clientId))
            if (clientId==0):
                testCaseStep = "Add the client"
                # Add the client as it does not exist
                addedClientFlag =1
                print "get the actual response"  
                
                actualresponse = clients.postWindowClient(appliance_ip, token,self.name, osType, clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, clients_testdata.use_ssl, clients_testdata.win_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted, clients_testdata.win_IsDefault, self.ipAddress, clientType="Windows") 
                logHandler.logging.info("Actual Response - "+str(actualresponse))
                reportController.addTestStepResult("Addition of windows client", ExecutionStatus.PASS)
                #Get the client ID
                testCaseStep = "Get the client ID"
                clientId = clients.getPostClientsRequestResponse(actualresponse, clients_testdata.keyForId)
                print clientId
            else:
                #set encryption off for already added client
                testCaseStep = "set encryption off for already added client"
                encryption_status_asset_off = featureHandler.Set_Encryption_OFF_Asset(appliance_ip, self.name, token)
                if encryption_status_asset_off == True:
                    print " Encryption set to off for the client"
                    logHandler.logging.info("Encryption set to off for the client")
                    reportController.addTestStepResult("Encryption on asset set to OFF", ExecutionStatus.PASS)
                else:
                    print " encryption is still set to ON for already added client"
                    logHandler.logging.info("encryption is still set to ON for already added client")
                    reportController.addTestStepResult("Encryption on asset set to ON", ExecutionStatus.FAIL)
                addedClientFlag =1
        
            #Trigger full backup of client with include list as a pre-requisite for incremental backup
            testCaseStep = "Trigger full backup"
            actualResponse = putBackupClientIncListWindows(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType, backups_testdata.excudeSystemState)
            # logHandler.logging.info("Actual Response - " + str(actualresponse))
            print "Actual Response for put backup - " + str(actualResponse)
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Backup Job Not successful" )
            
            #Check the status of backup
            testCaseStep = "Check the status of backup"
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Backup successful")
                print "Backup Successful"
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Backup Failed with jobID:"+str(strJobId)
            
            #Trigger incremental backup
            testCaseStep = "Trigger incremental backup"
            
            actualResponse =  putBackupClientIncListWindows(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.incrementalBackupType, backups_testdata.excudeSystemState)
            # logHandler.logging.info("Actual Response for incremental backup job- " + str(actualresponse))
            print "Actual Response for put backup - " + str(actualResponse)
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Backup Job Not successful" )
            
            #Check the status of backup
            testCaseStep = "Check the status of backup"
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Backup successful")
                print "Backup Successful"
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Backup Failed with jobID:"+str(strJobId)
              
              
            #Get Backup No from jobid
            testCaseStep = "Get Backup No from jobid"
            backupIDincre = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupIDincre)) 
            print "Backup ID based on jobid from Response - " + str(backupIDincre)
            print "Incremental backup job no" + str(backupIDincre)
            
            # Verify backup is not encrypted
            testCaseStep = "Verify backup is not encrypted"
            if featureHandler.check_Encrytion_of_backup(appliance_ip, backupIDincre):
                logHandler.logging.info("Backup Encrypted" + str(backupIDincre)) 
                reportController.addTestStepResult("Backup Encrypted" , ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Backup Not Encrypted" ) 
                reportController.addTestStepResult("Backup Not Encrypted" , ExecutionStatus.PASS)
                
            #check backup is lz4 compressed
            testCaseStep = "check backup is lz4 compressed"
            compression = featureHandler.check_lz4Compression_backup(appliance_ip, backupIDincre)
            print compression
            if compression == True:
                print "Backup is lz4 compressed"
                logHandler.logging.info("Backup is lz4 compressed")
                reportController.addTestStepResult("Backup compression type : lz4", ExecutionStatus.PASS)
            else:
                print "Backup is not lz4 compressed"
                logHandler.logging.info("Backup is not lz4 compressed")
                reportController.addTestStepResult("Backup compression type : lz4", ExecutionStatus.FAIL)
                
            #check backup is not deduplicated
            testCaseStep = "check backup is not deduplicated"
            dedeup_status = featureHandler.check_backup_notDeduplicated(appliance_ip, backupIDincre)
            if dedeup_status == True:
                logHandler.logging.info("Backup not deduplicated")
                reportController.addTestStepResult("Deduplication status : No Dedup", ExecutionStatus.PASS)
                print "Backup not deduplicated"
                
            else:
                logHandler.logging.info("Backup deduplicated")
                reportController.addTestStepResult("Deduplication status : No Dedup", ExecutionStatus.FAIL)
                print "Backup deduplicated"
                
            # Send API for restore
            testCaseStep = "Send API for restore"
            actualResponse = restore.postWindowsRestore(appliance_ip, token, backupIDincre, clientId, self.includeList, str(self.restoreFolder), restore_testdata.sid)
    
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Triggering Restore of Client" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print "Restore JobId - " + str(strJobId)
            
            #Check the status of Restore
            testCaseStep = "Check the status of Restore"
            restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
             
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("Restore Successful" , ExecutionStatus.PASS)
                print "restore job completed and successful"
                
                #Verify if backup files are restore
                testCaseStep = "Verify if backup files are restore"
                itemsinbackupFolder = clientMessageUtilities.verifyRestoreFileCount_Windows(self.includeList, self.ipAddress)
                itemsinrestoreFolder = clientMessageUtilities.verifyRestoreFileCount_Windows(self.restoreFolder + "\Backup_folder", self.ipAddress)
                if itemsinbackupFolder == itemsinrestoreFolder:
                    print "items matched"
                    logHandler.logging.info("Restore verification passed")
                    reportController.addTestStepResult("Restore verification", ExecutionStatus.PASS)
                else:
                    print "items not matched"
                    logHandler.logging.info("Restore verification failed")
                    reportController.addTestStepResult("Restore verification", ExecutionStatus.FAIL)
                    
                
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Restore not successful", ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        
        except Exception as e:
            print "found exception"
            reportController.addTestStepResult(testCaseStep, ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))  
        finally:
            
            sisCompression_Status = featureHandler.change_SISCompression_status(appliance_ip, "useSIScompression=Yes")
            # check is SISCompression is set to Off:
            if sisCompression_Status == "sisCompression_Status":
                print "SISCompression is set to " + sisCompression_Status
                logHandler.logging.info("SISCompression is set to " + sisCompression_Status)
            else:
                #check status of SISCompression
                sisCompression_status= featureHandler.check_SISCompression_Status(appliance_ip)
                print sisCompression_status
                logHandler.logging.info("SISCompression is " + sisCompression_Status)
            #Logout
            print "log out started"
            logoutResponse = logout.doUserLogout(appliance_ip,token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            print "generating report summary"
            reportController.generateReportSummary(appliance_ip, self.executionId)

            
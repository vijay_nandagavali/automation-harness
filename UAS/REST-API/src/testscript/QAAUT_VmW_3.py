'''
Created on Mar 1, 2017

@author: deepanshu.s
'''

import unittest
from resourcelibrary import clients, backups,login, logout, jobs, inventory, restore
from testdata import clients_testdata, backups_testdata, inventory_testdata, restore_testdata
from util import reportController, logHandler
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from time import sleep

class QAAUT_VmW_3(unittest.TestCase):    
    def __init__(self, exeID, uebInfra, machineInfra):            
        self.executionId = exeID
        '''
        Initialize UEB Configuration test steps
        '''
        
        self.uebInfra=uebInfra
        self.machineInfra=machineInfra 
        '''
        Initialize machine environment
        ''' 
        self.name=machineInfra["vmwarename"] if "vmwarename" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["vmwareipAddress"] if "vmwareipAddress" in machineInfra.keys() else ""
        self.flavour=machineInfra["vmwareflavour"] if "vmwareflavour" in machineInfra.keys() else ""
        self.instanceName= "small_linux_bat"
        self.vmType = "VMware"
        self.datastore = "datastore1"
                
        reportController.initializeReport(type(self).__name__, "Verify a Differential backup for the specified VMware instance" + " : " + str(self.flavour))
        logHandler.logFile(self.executionId)
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify a Differential backup for the specified VMware instance" + " : " + str(self.flavour))
        
        
    def runTest(self):        
        try:
            appliance_ip=self.uebInfra["Mainurl"]
            addedClientFlag = 0
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            
            #Step 1: check if vmware client exists
            uuid = vclients.getSpecificEsxServerFromDB(appliance_ip, self.name)
            print ("UUID returned from DB: " + str(uuid))
            logHandler.logging.info("UUID returned from DB: " + str(uuid))
            
            #Step 2: if client does not exists add a client
            if(uuid==0):
                actualResponse = clients.postVMClient(appliance_ip, token,clients_testdata.sid, self.vmType, 
                                                clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, 
                                                self.ipAddress, 
                                                clients_testdata.vm_username, clients_testdata.vm_password 
                                                )
                reportController.addTestStepResult("Add Virtual Host" , ExecutionStatus.PASS) 
                  
                responseResult = clients.getVMPostClientRequestResponse(actualResponse)
                print ("Response Result: " + str(responseResult))
                logHandler.logging.info("Response Result: " + str(responseResult))
                addedClientFlag =1
                uuid = vclients.getSpecificEsxServerFromDB(appliance_ip, self.name)
                print ("UUID returned from DB: " + str(uuid))
                logHandler.logging.info("UUID returned from DB: " + str(uuid))
            else:
                reportController.addTestStepResult("Add Virtual Host" , ExecutionStatus.PASS)                         
            # Step 3: Get instance Id from database
            instanceId = vbackups.getInstanceIdFromVmVMWare(appliance_ip, self.instanceName)
            print ("Instance ID returned from DB: " + str(instanceId))
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            
            #Step 4: Put request for full backup 
            actualResponse = backups.putHypervInstanceBackup(appliance_ip, token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            print "Actual Response for put backup - " + str(actualResponse)
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strInstanceName = responseResult["data"][0]["instance_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strInstanceName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strInstanceName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Master Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Master Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Master Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Master Backup Job Not successful" )
            
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            print "returned backup status" + backupJobStatus
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Backup successful")
                print "Backup Successful"
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Master Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Master Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Backup Failed with jobID:"+str(strJobId)
                
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            print "Backup ID based on jobid from Response - " + str(backupID)

            #Calling existing HyperV function for VMWare also
            actualResponse = backups.putHypervInstanceBackup(appliance_ip, token,instanceId,"Differential",backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            print "Actual Response for put backup - " + str(actualResponse)
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
             
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strInstanceName = responseResult["data"][0]["instance_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strInstanceName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strInstanceName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Differential Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Differential Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Differential Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Differential Backup Job Not successful" )
             
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            print "returned backup status" + backupJobStatus
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Differential Backup successful")
                print "Backup Successful"
                reportController.addTestStepResult("Differential Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Differential Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Differential Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Incremental Backup Failed with jobID:"+str(strJobId)
                 
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Differential Backup ID based on jobid from Response - " + str(backupID)) 
            print "Differential Backup ID based on jobid from Response - " + str(backupID)
 
            # restore parameters
            group = ""
            name = self.instanceName + "_restore"
            metadata = True
            template = False
            
            actualResponse = restore.putVMWareRestore(appliance_ip, token, backupID, clients_testdata.sid, uuid, self.datastore, group, name, metadata, template)
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Triggering Restore of Client" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print "Restore JobId - " + str(strJobId)
            
            list_of_restore_job_ids = strJobId.split(",")
            
            for ids in list_of_restore_job_ids:
                #Check the status of Restore
                restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, ids)
                 
                if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                    logHandler.logging.info("restore successful")
                    reportController.addTestStepResult("Restore Successful" , ExecutionStatus.PASS)
                else:
                    logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                    reportController.addTestStepResult("Restore not successful", ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            print "found exception"
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))   
        finally:
            #cleanup
            print "addedClientFlag: " + str(addedClientFlag)
            #if(addedClientFlag):
                #clients.deleteClients(appliance_ip,  token, clientId)
                #reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
             
            #Logout
            print "log out started"
            logoutResponse = logout.doUserLogout(appliance_ip, token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            print "generating report summary"
            reportController.generateReportSummary(appliance_ip, self.executionId)
'''
Created on Feb 9 , 2016

@author: gayatri.l
'''

import unittest

from resourcelibrary import clients, backups,login, logout,jobs, restore
from testdata import clients_testdata, backups_testdata, restore_testdata
from util import reportController, logHandler,clientMessageUtilities, featureHandler
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from Constants import ExecutionStatus
from resourcelibrary.backups import putBackupClientIncList, putBackupClientIncListWindows
import paramiko
from util.clientMessageUtilities import createFoldersWindows
from config.Constants import ExecutionStatus


class QAAUT_Win003(unittest.TestCase):
    def __init__(self, exeID, uebInfra, machineInfra):
        
        self.executionId = exeID
        
        '''
        Initialize UEB Configuration test steps
        '''
        self.uebInfra = uebInfra
        self.machineInfra = machineInfra
        
        '''
        initialize machine EnvironmentError
        '''
#         self.name="Win_2016"
#         self.ipAddress = "192.168.197.200"
        self.ipAddress=machineInfra["windowsipAddress"] if "windowsipAddress" in machineInfra.keys() else ""
        self.name=machineInfra["windowsname"] if "windowsname" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["windowsipAddress"] if "windowsipAddress" in machineInfra.keys() else ""
        self.flavour=machineInfra["windowsflavour"] if "windowsflavour" in machineInfra.keys() else ""

        self.includeList = "C:\Backup_folder"
        self.restoreFolder = "C:\Restore_folder"
       
        reportController.initializeReport(type(self).__name__, "Verify that differential backup and restore of client with Inline dedupe on and Encryption off is successful." + " : " + str(self.flavour))
        logHandler.logFile(self.executionId)
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify that differential backup and restore of client with Inline dedupe on and Encryption off is successful." + " : " + str(self.flavour))
        
        
    def runTest(self):
        try:
            reportController.tcExecutionStatus(ExecutionStatus.UNKNOWN)
                
            osType = ""
            appliance_ip = self.uebInfra["Mainurl"]
            print appliance_ip
            addedClientFlag = 0
            testCaseStep = "login"
            #login and get the authentication token
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            testCaseStep = "Add few files on windows machine"
            #Add few files on windows machine
            if (self.includeList!=''):
                clientMessageUtilities.addCustomSizedfiles_Window(self.ipAddress, self.includeList+"/", 3, "Backup", 5, "M")
            
            testCaseStep = "check Encryption on Appliance"  
            # check Encryption on Appliance
            encryption_status = featureHandler.Get_Encryption_Status(appliance_ip, token)

            print "connect to client"
            print "Encryption status of the appliance is " + encryption_status
            logHandler.logging.info("Encryption status of the appliance is " + encryption_status)
            reportController.addTestStepResult("Encryption status of the appliance is : " + encryption_status , ExecutionStatus.PASS)

            if encryption_status == "off":
                print "Encryption Off found"
                logHandler.logging.info("Encryption Off on Appliance found")
                testCaseStep = "set encryption on on appliance"
                # set encryption on on appliance
                featureHandler.Enable_Encryption_Appliance(appliance_ip, "unitrends1", token)
                print "encryption enabled for appliance"
                logHandler.logging.info("encryption enabled for appliance")
                reportController.addTestStepResult("Encryption On : on Appliance" , ExecutionStatus.PASS)
#                 reportController.addTestStepResult("Encryption Off" , ExecutionStatus.PASS)
            elif encryption_status == "on":
                reportController.addTestStepResult("Encryption On on Appliance" , ExecutionStatus.PASS)
                
            testCaseStep = "check inline dedup appliance"     
            # check inline dedup appliance
            appliance_dedup = featureHandler.Check_Inline_dedup(appliance_ip)
            if appliance_dedup:
                print "Inline Deduplication is enabled"
                logHandler.logging.info("Inline Deduplication is enabled")
            else:
                print "Inline Deduplication is disabled"
                logHandler.logging.info("Inline Deduplication is disabled")
                print "Enabling Inline Deduplication"
                logHandler.logging.info("Enabling Inline Deduplication")
                
                testCaseStep = "set inline dedup to on" 
                #set inline dedup to on
                dedup_information = featureHandler.set_inline_dedup(appliance_ip, token)
                logHandler.logging.info("Inline Dedup set for appliance")
                reportController.addTestStepResult("Inline Dedup set for appliance", ExecutionStatus.PASS)
                print dedup_information
                 
            #Send request to update a client
            #Check if client already exist
            #clientId = vclients.getSpecificClientByNameDB(self.ipAddress, self.name)
            clientId = vclients.getSpecificClientByNameDB(appliance_ip,self.name)
            print clientId
            logHandler.logging.info("Client ID returned from DB: "+str(clientId))
            if (clientId==0):
                # Add the client as it does not exist
                addedClientFlag =1
                print "get the actual response"  
                testCaseStep = "Add Client" 
                actualresponse = clients.postWindowClient(appliance_ip, token,self.name, osType, clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, clients_testdata.use_ssl, clients_testdata.win_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted, clients_testdata.win_IsDefault, self.ipAddress, clientType="Windows") 
                logHandler.logging.info("Actual Response - "+str(actualresponse))
                reportController.addTestStepResult("Addition of windows client", ExecutionStatus.PASS)
                #Get the client ID
                clientId = clients.getPostClientsRequestResponse(actualresponse, clients_testdata.keyForId)
                print clientId
            else:
                testCaseStep = "set encryption off for already added client" 
                #set encryption off for already added client
                encryption_status_asset_off = featureHandler.Set_Encryption_OFF_Asset(appliance_ip, self.name, token)
                if encryption_status_asset_off == True:
                    print " Encryption set to off for the client"
                    logHandler.logging.info("Encryption set to off for the client")
                    reportController.addTestStepResult("Encryption on asset set to OFF", ExecutionStatus.PASS)
                else:
                    print " encryption is still set to ON for already added client"
                    logHandler.logging.info("encryption is still set to ON for already added client")
                    reportController.addTestStepResult("Encryption on asset set to ON", ExecutionStatus.FAIL)
                addedClientFlag =1
            
            
            testCaseStep = "Trigger Full bakcup" 
        
            #Trigger full backup of client with include list as a prerequisite
            actualResponse = putBackupClientIncListWindows(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType, backups_testdata.excudeSystemState)
            # logHandler.logging.info("Actual Response - " + str(actualresponse))
            print "Actual Response for put backup - " + str(actualResponse)
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Backup Job Not successful" )
             
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Full Backup successful")
                print "Full Backup Successful"
                reportController.addTestStepResult("Full Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Full Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Full Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Full Backup Failed with jobID:"+str(strJobId)
               
            testCaseStep = "Get Backup No from jobid"   
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            print "Backup ID based on jobid from Response - " + str(backupID)
            print "backup no of full backup" + str(backupID)
             
            testCaseStep = "Add files to folder"  
            # Add files to folder
            if (self.includeList!=''):
                clientMessageUtilities.addCustomSizedfiles_Window(self.ipAddress, self.includeList+"/", 2, "differential", 5, "M")
                
            #Trigger differential backup
            testCaseStep = "Trigger differential backup"
            actualResponse =  putBackupClientIncListWindows(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.differentialBackupType, backups_testdata.excudeSystemState)
            # logHandler.logging.info("Actual Response for differential backup job- " + str(actualresponse))
            print "Actual Response for put backup - " + str(actualResponse)
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Backup Job Not successful" )
            
            #Check the status of backup
            testCaseStep = "Check the status of backup"
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful"):
                logHandler.logging.info(" Differential Backup successful")
                print " Differential Backup Successful"
                reportController.addTestStepResult(" Differential Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info(" Differential Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Differential Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Differential Backup Failed with jobID:"+str(strJobId)
              
            testCaseStep = "Get Backup No from jobid"
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            print "Backup ID based on jobid from Response - " + str(backupID)
            print "differential backup job no" + str(backupID)
              
            testCaseStep = "check encryption status for backup"    
            # check encryption status for backup
            if featureHandler.check_Encrytion_of_backup(appliance_ip, backupID):
                logHandler.logging.info("Backup Encrypted" + str(backupID)) 
                reportController.addTestStepResult("Backup Encrypted" , ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Backup Not Encrypted" ) 
                reportController.addTestStepResult("Backup Not Encrypted" , ExecutionStatus.PASS)
            
            testCaseStep = "check inline dedup status for backup"  
            # check inline dedup status for backup
            if featureHandler.verifySuccessfulDedupForABackup(appliance_ip, backupID):
                print "Backup inline deduplicated"
                logHandler.logging.info("Backup inline deduplicated" + str(backupID)) 
                reportController.addTestStepResult("Backup inline deduplicated" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup NOT inline deduplicated" + str(backupID)) 
                print "Backup NOT inline deduplicated"
                reportController.addTestStepResult("Backup NOT inline deduplicated" , ExecutionStatus.FAIL)
                
            testCaseStep = "restore of differential backup" 
            # Send API for restore of differential backup
            print "ntal backup for job_id"+ str(backupID)
            actualResponse = restore.postWindowsRestore(appliance_ip, token, backupID, clientId, self.includeList, str(self.restoreFolder), restore_testdata.sid)
            
    
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Triggering Restore of Client" , ExecutionStatus.PASS)
            
            testCaseStep = "Extract jobid from response" 
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print " differential backup Restore JobId - " + str(strJobId)
            
            testCaseStep = "Check the status of Restore" 
            #Check the status of Restore
            restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
             
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("Restore Successful" , ExecutionStatus.PASS)
                print "restore job completed and successful"
                
                testCaseStep = "Verify if backup files are restore"
                #Verify if backup files are restore
                itemsinbackupFolder = clientMessageUtilities.verifyRestoreFileCount_Windows(self.includeList, self.ipAddress)
                itemsinrestoreFolder = clientMessageUtilities.verifyRestoreFileCount_Windows(self.restoreFolder + "\Backup_folder", self.ipAddress)
                if itemsinbackupFolder == itemsinrestoreFolder:
                    print "items matched"
                    logHandler.logging.info("Restore verification passed")
                    reportController.addTestStepResult("Restore verification", ExecutionStatus.PASS)
                else:
                    print "items not matched"
                    logHandler.logging.info("Restore verification failed")
                    reportController.addTestStepResult("Restore verification", ExecutionStatus.FAIL)
                    
                
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Restore not successful", ExecutionStatus.FAIL)
                
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            print "found exception"
            reportController.addTestStepResult(testCaseStep, ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))  
        finally:
            #Logout
            print "log out started"
            logoutResponse = logout.doUserLogout(appliance_ip,token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            print "generating report summary"
            reportController.generateReportSummary(appliance_ip, self.executionId)

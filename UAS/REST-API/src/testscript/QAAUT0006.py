'''
Created on Jan 25, 2017

@author: pratik.p
'''
import unittest

from resourcelibrary import clients, backups,login, logout,jobs, restore
from testdata import clients_testdata, backups_testdata, restore_testdata
from util import reportController, logHandler,clientMessageUtilities
from verificationsript import vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig



class QAAUT0006(unittest.TestCase):    
    def __init__(self, exeID, uebInfra, machineInfra):
        self.executionId = exeID
        '''
        Initialize UEB Configuration test steps
        '''
        
        self.uebInfra=uebInfra
        self.machineInfra=machineInfra 
        '''
        Initialize machine environment
        ''' 
        self.name=machineInfra["linuxname"] if "linuxname" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["linuxipAddress"] if "linuxipAddress" in machineInfra.keys() else ""
        self.flavour=machineInfra["linuxflavour"] if "linuxflavour" in machineInfra.keys() else ""
        # self.includeList=machineInfra["linuxincludeList"] if "linuxincludeList" in machineInfra.keys() else ""
        # self.excludeList=machineInfra["linuxexcludeList"] if "linuxexcludeList" in machineInfra.keys() else ""
        # self.restoreFolder=machineInfra["linuxrestoreFolder"] if "linuxrestoreFolder" in machineInfra.keys() else ""
        self.includeList = "/root/Backup_folder"
        self.excludeList = "/root/Backup_folder/InctYPE_1.txt"
        self.restoreFolder = "/root/Restore_folder"

        reportController.initializeReport(type(self).__name__, "Verify Differential backup and restore of a linux client with exclude list" + " : " + str(self.flavour))
        logHandler.logFile(self.executionId)
        # print "log file generated"
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify Differential backup and restore of a linux client with exclude list" + " : " + str(self.flavour))
        
        
        
    def runTest(self):        
        try:
            reportController.tcExecutionStatus(ExecutionStatus.UNKNOWN)
            
            testCaseStep = "Login"
            osType = ""
            appliance_ip=self.uebInfra["Mainurl"]
            apiconfig.environmentIp = appliance_ip
            addedClientFlag = 0
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            # print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult(testCaseStep, ExecutionStatus.PASS)
            
            testCaseStep = "Setup Linux"
            clientMessageUtilities.SetupLinux(self.ipAddress)
            
            #Step 1: Send request to update a client
            testCaseStep="Client Addition"
            clientId = vclients.getSpecificClientByNameDB(appliance_ip, self.name)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postLinuxClient(appliance_ip,token, self.name, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.linux_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted,   
                                                clients_testdata.linux_IsDefault, self.ipAddress)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult(testCaseStep , ExecutionStatus.PASS)
                # print "client add response: "+str(actualResponse)
                # Step 2: Obtain the client ID
                testCaseStep = "Obtain the client ID"
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            
            '''
            Add Files on linux
            '''
            if(self.includeList!=""):
                testCaseStep="Add Files on linux"
                clientMessageUtilities.addCustomSizedfilesOnLinux(self.ipAddress,self.includeList+"/",3,5,"M")
            # Step 3: Send the PUT request
            testCaseStep = "Put Backup Client Exclude List"
            actualResponse = backups.putBackupClientExcList(appliance_ip,token, backups_testdata.verifyNone, int(clientId), self.includeList, self.excludeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            testCaseStep = "verify the response parameter - compare the value of client_id obtained in the response"
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            testCaseStep="Triggering Backup Job"
            if str(strJobId) != "-1":
                reportController.addTestStepResult( testCaseStep, ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                logHandler.logging.info("Triggering Backup Job Not successful" )
                raise Exception(testCaseStep)
            #Check the status of backup
            testCaseStep = "Check the status of backup"
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            testCaseStep="Full backup with exclude list"
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Backup successful")
                # print "Backup Successful"
                reportController.addTestStepResult( testCaseStep, ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                raise Exception("Full backup with exclude list with jobID : "+str(strJobId))
                # print "Backup Failed with jobID:"+str(strJobId)
                
            #Get Backup No from jobid
            testCaseStep = "Get Backup No from jobid"
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            # print "Backup ID based on jobid from Response - " + str(backupID)
            
            if(self.includeList !=""):
                testCaseStep = "Add custom sized files on linux"
                clientMessageUtilities.addCustomSizedfilesOnLinux(self.ipAddress,self.includeList,3,5,"M")
                logHandler.logging.info("File changes Done for incremental backup")
                # print "File changes Done for incremental backup"\
            testCaseStep = "Put Backup Client Exclude List"
            actualResponse = backups.putBackupClientExcList(appliance_ip,token, backups_testdata.verifyNone, int(clientId), self.includeList, self.excludeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.differentialBackupType)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            testCaseStep = "verify the response parameter - compare the value of client_id obtained in the response"
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            testCaseStep = "verify the response parameter - compare the value of client_id obtained in the response"
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            testCaseStep="Triggering Backup Job" 
            if str(strJobId) != "-1":
                reportController.addTestStepResult(testCaseStep, ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                logHandler.logging.info("Triggering Backup Job Not successful" )
                raise Exception(testCaseStep)
            #Check the status of backup
            testCaseStep = "Check the status of backup"
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            testCaseStep="Differential backup" 
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Differential Backup successful")
                # print "Differential Backup Successful"
                reportController.addTestStepResult(testCaseStep, ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Differential Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                raise Exception("Differential backup with jobID : "+str(strJobId))
                # print "Differential Backup Failed with jobID:"+str(strJobId)
                
            #Get Backup No from jobid
            testCaseStep = "Get Backup No from jobid"
            backupIDDiff = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Differential Backup ID based on jobid from Response - " + str(backupIDDiff)) 
            # print "Differential Backup ID based on jobid from Response - " + str(backupIDDiff)
            
                        
            
            # Step 3: Send API for restore
            testCaseStep="API request for restore of incremental backup"
            actualResponse = restore.postLinuxRestore(appliance_ip, token, backupIDDiff, clientId, self.includeList, self.restoreFolder, restore_testdata.sid)
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            # print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult(testCaseStep , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            testCaseStep= "Extract jobid from response"
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            # print "Restore JobId - " + str(strJobId)
            
            #Check the status of Restore
            testCaseStep = "Check the status of Restore"
            restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
            testCaseStep="Restore of differential backup with exclude list"
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult( testCaseStep, ExecutionStatus.PASS)
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                raise Exception(testCaseStep)
                
            if(self.includeList==""):
                    logHandler.logging.info("include list not present for linux so no client side verification")
            else:  
                clientMessageUtilities.deleteOnLinux(self.excludeList, self.ipAddress)
                checksum=clientMessageUtilities.verifyRestoreFolderAgainstBackupFolderOnLinux(self.includeList, self.restoreFolder+self.includeList, self.ipAddress)
                testCaseStep="Checksum verification after restore of differential backup with exclude list"
                if(checksum):
                    reportController.addTestStepResult(testCaseStep,ExecutionStatus.PASS)
                    logHandler.logging.info("Backup and Restore files matched")
                    # print "Backup and Restore files matched"
                else:
                    logHandler.logging.info("Backup and Restore files did not match")
                    raise Exception(testCaseStep)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult(testCaseStep , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))      
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(appliance_ip, token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(appliance_ip, self.executionId)
'''
Created on Jan2 24 2017

@author: Deepanshu
'''

import unittest

from resourcelibrary import clients, backups,login, logout,jobs, restore, settings
from testdata import clients_testdata, backups_testdata, restore_testdata,\
    settings_testdata
from util import reportController, logHandler,clientMessageUtilities, featureHandler
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
import time
import paramiko
from pip._vendor.colorama.ansi import Back



class QAAUT_D4(unittest.TestCase):    
    def __init__(self, exeID, uebInfra, machineInfra): 
        self.executionId = exeID
        '''
        Initialize UEB Configuration test steps
        '''
        
        self.uebInfra=uebInfra
        self.machineInfra=machineInfra 
        '''
        Initialize machine environment
        ''' 
        self.name=machineInfra["linuxname"] if "linuxname" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["linuxipAddress"] if "linuxipAddress" in machineInfra.keys() else ""
        self.flavour=machineInfra["linuxflavour"] if "linuxflavour" in machineInfra.keys() else ""
        # self.includeList=machineInfra["linuxincludeList"] if "linuxincludeList" in machineInfra.keys() else ""
        # self.restoreFolder=machineInfra["linuxrestoreFolder"] if "linuxrestoreFolder" in machineInfra.keys() else ""
        self.includeList = "/root/Backup_folder"
        self.restoreFolder = "/root/Restore_folder"
        
        reportController.initializeReport(type(self).__name__, "Verify incremental backup and restore with Inline Dedup and Encryption on."+ " : " + str(self.flavour))
        logHandler.logFile(self.executionId)
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify incremental backup and restore with Inline Dedup and Encryption on."+ " : " + str(self.flavour))
        
    def runTest(self):        
        try:
            osType = ""
            appliance_ip=self.uebInfra["Mainurl"]
            addedClientFlag = 0
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            clientMessageUtilities.SetupLinux(self.ipAddress)
            
            # check Encryption on Appliance
            encryption_status = featureHandler.Get_Encryption_Status(appliance_ip, token)
            if encryption_status == "off":
                print "Encryption Off found"
                logHandler.logging.info("Encryption Off on Appliance found")
                # set encryption on on appliance
                featureHandler.Enable_Encryption_Appliance(appliance_ip, "unitrends1", token)
                print "encryption enabled for appliance"
                logHandler.logging.info("encryption enabled for appliance")
                reportController.addTestStepResult("Encryption On : on Appliance" , ExecutionStatus.PASS)
#                 reportController.addTestStepResult("Encryption Off" , ExecutionStatus.PASS)
            elif encryption_status == "on":
                reportController.addTestStepResult("Encryption On on Appliance" , ExecutionStatus.PASS)
                
            
            # check inline dedup appliance
            appliance_dedup = featureHandler.Check_Inline_dedup(appliance_ip)
            if appliance_dedup:
                print "Inline Deduplication is enabled"
                logHandler.logging.info("Inline Deduplication is enabled")
            else:
                print "Inline Deduplication is disabled"
                logHandler.logging.info("Inline Deduplication is disabled")
                print "Enabling Inline Deduplication"
                logHandler.logging.info("Enabling Inline Deduplication")
                 
                dedup_information = featureHandler.set_inline_dedup(appliance_ip, token)
                print dedup_information
            
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(appliance_ip, self.name)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            is_encrypted = True
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postLinuxClient(appliance_ip, token, self.name, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.linux_InstallAgent, clients_testdata.is_auth_enabled, is_encrypted,   
                                                clients_testdata.linux_IsDefault, self.ipAddress)
                 
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                print "client added: " + clientId
            else:
                # set encryption to ON for already added client
                enc_status = featureHandler.Set_Encryption_On_Asset(appliance_ip, self.name, token)
                if enc_status:
                    print " encryption set to ON for already added client"
                    logHandler.logging.info("encryption set to ON for already added client")
                else:
                    print " failed to set encryption on on added client"
                    logHandler.logging.info("failed to set encryption on on added client")
                addedClientFlag =1
            '''
            Add Files on linux
            '''
            if(self.includeList!=""):
                clientMessageUtilities.addCustomSizedfilesOnLinux(self.ipAddress,self.includeList+"/",3,5,"M")
            
            
            # Step 2: Send the PUT request for FULL BACKUP as prerequsite
            actualResponse = backups.putBackupClientIncList(appliance_ip, token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType   )
            print "actual response:" + actualResponse
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Backup Job Not successful" )
            
            #Check the status of Full backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful" or backupJobStatus =="Warning" ):
                logHandler.logging.info("Master Backup successful")
                reportController.addTestStepResult("Master Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Master Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Master Backup not successful", ExecutionStatus.FAIL) 
            
            '''
            Add Files on linux
            '''
            if(self.includeList!=""):
                clientMessageUtilities.addCustomSizedfilesOnLinux(self.ipAddress,self.includeList+"/",3,5,"M")
            
            
            # Step 3:  Send the PUT request for Incremental BACKUP 
            actualResponse = backups.putBackupClientIncList(appliance_ip, token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.incrementalBackupType   )
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering INCREMENTAL Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering INCREMENTAL Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering INCREMENTAL Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering INCREMENTAL Backup Job Not successful" )
            
            
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response for INCREMENTAL BACKUP - " + str(strJobId) + " Client Name from Response - " + str(strClientName))
            
            # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
            dbResult = vbackups.getJobByIdFromDB(appliance_ip, strJobId)
            logHandler.logging.info("JOB triggered from jobID for INCREMENTAL BACKUP: " + str(dbResult))
             
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Database verification failed")
                reportController.addTestStepResult("Failed to find Job with Job ID" + str(strJobId), ExecutionStatus.FAIL)    
             
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful" or backupJobStatus =="Warning" ):
                logHandler.logging.info("Incremental Backup successful")
                reportController.addTestStepResult("Incremental Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Incremental Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Incremental Backup not successful", ExecutionStatus.FAIL) 
            
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            print "Backup ID based on jobid from Response - " + str(backupID)
            
            # check encryption status for backup
            if featureHandler.check_Encrytion_of_backup(appliance_ip, backupID):
                logHandler.logging.info("Backup Encrypted " + str(backupID)) 
                reportController.addTestStepResult("Backup Encrypted " , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup Not Encrypted" + str(backupID)) 
                reportController.addTestStepResult("Backup Not Encrypted with BackupID:"  + str(backupID), ExecutionStatus.FAIL)
            
            # check inline dedup status for backup
            if featureHandler.verifySuccessfulDedupForABackup(appliance_ip, backupID):
                print "Backup inline deduplicated"
                logHandler.logging.info("Backup inline deduplicated" + str(backupID)) 
                reportController.addTestStepResult("Backup inline deduplicated" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup NOT inline deduplicated" + str(backupID)) 
                print "Backup NOT inline deduplicated"
                reportController.addTestStepResult("Backup NOT inline deduplicated" , ExecutionStatus.PASS)
            
            time.sleep(20)
            
            # Step 3: Send API for restore 
            actualResponse = restore.postLinuxRestore(appliance_ip, token, backupID, clientId, self.includeList, str(self.restoreFolder), restore_testdata.sid)
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Triggering Restore of Client" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print "Restore JobId - " + str(strJobId)
            
            #Check the status of Restore
            restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
             
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("Restore Successful" , ExecutionStatus.PASS)
                clientMessageUtilities.verifyRestoreFolderAgainstBackupFolderOnLinux("/root/Backup_folder", "/root/Restore_folder/root/Backup_folder", self.ipAddress)
                logHandler.logging.info("checksum verified")
                reportController.addTestStepResult("Checksum Verified" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Restore not successful", ExecutionStatus.FAIL)
            
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
              
            
        except Exception as e:
            print "found exception"
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            # remove locations of restore
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(self.ipAddress, username='root', password='unitrends1')
            logHandler.logging.info("Client connection done")
            print "start"
            logHandler.logging.info("removal of restore folder location Started")
            buildCommand = " rm -rf /root/Restore_folder_1 /root/Restore_folder_2 /root/Backup_folder /root/Restore_folder"
            logHandler.logging.info("removal of folder location: " +buildCommand)
            print buildCommand
            stdin, stdout, stderr = client.exec_command(buildCommand)
            logHandler.logging.info("Command executed")
            logHandler.logging.info("removal of restore folder location finished")
            
            
            print "addedClientFlag: " + str(addedClientFlag)
            #if(addedClientFlag):
                #clients.deleteClients(appliance_ip, token, clientId)
                #reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
              
            #Logout
            print "log out started"
            logoutResponse = logout.doUserLogout(appliance_ip, token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            print "generating report summary"
            reportController.generateReportSummary(appliance_ip, self.executionId)
'''
Created on Feb 1, 2017

@author: divya.j
'''

import unittest

from resourcelibrary import clients, backups, login, logout, jobs, archive, storage
from testdata import clients_testdata, backups_testdata
from util import reportController, logHandler, clientMessageUtilities
from verificationsript import vbackups, vrestore, vclients, varchive
from config.Constants import ExecutionStatus
from config import apiconfig



class QAAUT0007(unittest.TestCase):    
    def __init__(self, exeID, uebInfra, machineInfra):
        self.executionId = exeID
        '''
        Initialize UEB Configuration test steps
        '''
        self.uebInfra=uebInfra
        self.machineInfra=machineInfra 

        '''
        Initialize machine environment
        ''' 
        self.name=machineInfra["linuxname"] if "linuxname" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["linuxipAddress"] if "linuxipAddress" in machineInfra.keys() else ""
        self.flavour=machineInfra["linuxflavour"] if "linuxflavour" in machineInfra.keys() else ""
        # self.includeList=machineInfra["linuxincludeList"] if "linuxincludeList" in machineInfra.keys() else ""
        self.includeList = "/root/Backup_folder"
                   
        reportController.initializeReport(type(self).__name__, "Verify RPM based full backup can be archived"+ " : " + str(self.flavour))
        logHandler.logFile(self.executionId)
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify RPM based full backup can be archived"+ " : " + str(self.flavour))   
        
    def runTest(self):        
        try:
            reportController.tcExecutionStatus(ExecutionStatus.UNKNOWN)
            
            testCaseStep = "Login"            
            osType = ""
            appliance_ip=self.uebInfra["Mainurl"]
            apiconfig.environmentIp = appliance_ip
            addedClientFlag = 0
            
            # Login to UEB an obtain authentication token
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            # print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            testCaseStatus = ExecutionStatus.FAIL
            reportController.addTestStepResult(testCaseStep, ExecutionStatus.PASS)
            
            testCaseStep = "Setup Linux"
            clientMessageUtilities.SetupLinux(self.ipAddress)
            
            #Step 1: Send request to add a client
            clientId = vclients.getSpecificClientByNameDB(appliance_ip, self.name)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            # print "Client ID returned from DB: " + str(clientId)
            
            testCaseStep = "Client Addition"
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postLinuxClient(appliance_ip,token, self.name, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.linux_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted,   
                                                clients_testdata.linux_IsDefault, self.ipAddress)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult(testCaseStep , ExecutionStatus.PASS)
                # print "client add response: "+str(actualResponse)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            
            # Add external storage attached to UEB as a Backup Copy target
            # Send API Request
            # print "adding backup copy target"
            testCaseStep = "API request to add attached storage to UEB"
            actualResponse = storage.postDirectStorage(appliance_ip, token, "/dev/sdc", "archive", "archive")
            reportController.addTestStepResult(testCaseStep , ExecutionStatus.PASS)
            
            '''
            Add Files on linux
            '''
            
            if(self.includeList!=""):
                testCaseStep = "Add Files on linux"
                clientMessageUtilities.addCustomSizedfilesOnLinux(self.ipAddress,self.includeList+"/",3,5,"M")
            # Step 3: Send the PUT request
            testCaseStep = "Send the PUT request"
            actualResponse = backups.putBackupClientIncList(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            testCaseStep = "verify the response parameter - compare the value of client_id obtained in the response"
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            testCaseStep = "Triggering Backup Job"
            if str(strJobId) != "-1":
                reportController.addTestStepResult(testCaseStep , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Backup Job successful" )
            else:
                logHandler.logging.info("Triggering Backup Job Not successful" )
                raise Exception(testCaseStep)
            #Check the status of backup
            testCaseStep = "Full backup"
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Backup successful")
                # print "Backup Successful"
                reportController.addTestStepResult( testCaseStep, ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                raise Exception(testCaseStep)
                # print "Backup Failed with jobID:"+str(strJobId)
                
            #Get Backup No from jobid
            testCaseStep = "Get Backup No from jobid"
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            # print "Backup ID based on jobid from Response - " + str(backupID)
            
            '''
            '''
            
            #Send API Request for Creating Full Archive
            testCaseStep = "Send API Request for Creating Full Archive"
            instanceId = vbackups.getInstanceIdFromClientName(appliance_ip, self.name)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            # print "Instance ID returned from DB: " + str(instanceId)
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            # print " performing archive ... "
            testCaseStep = "API request for backup copy of full backup"
            actualResponse = archive.putArchive(appliance_ip, token, instanceId, "archive", "Full")
            reportController.addTestStepResult(testCaseStep , ExecutionStatus.PASS)
                        
            #Parse the actual response
            # print "Parsing actual response of archive api"
            testCaseStep = "Verify response parameter of backup copy of full backup"
            jobNo = varchive.verifyPutArchiveResponse(actualResponse)
            logHandler.logging.info("Create Archive successful. Job Number: " + str(jobNo))
            # print "Create Archive successful. Job Number: " + str(jobNo)
            self.assertNotEqual(jobNo,"","Response verification failed")                        
            reportController.addTestStepResult(testCaseStep, ExecutionStatus.PASS)
            
            #Check the status of archive job
            # print "Checking status of archive job"
            testCaseStep = "Checking status of archive job"
            archiveJobStatus=jobs.waitForJobCompletion(appliance_ip, "Archive", backups_testdata.waittime, token, jobNo)
            logHandler.logging.info("Archive Job status: " + str(archiveJobStatus))
            # print "Archive Job status: " + str(archiveJobStatus)
            testCaseStep = "Backup copy of full backup of a linux client"
            if(archiveJobStatus!="Successful"):
                raise Exception("Backup copy of full backup of a linux client not successful")
            else:
                logHandler.logging.info("Archive successful")
                reportController.addTestStepResult(testCaseStep , ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS) 
            
        except Exception as e:
            reportController.addTestStepResult(testCaseStep, ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
        finally:
            #Logout
            # print " log out ..."
            logoutResponse = logout.doUserLogout(appliance_ip, token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(appliance_ip,self.executionId)
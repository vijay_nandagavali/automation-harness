'''
Created on Feb 6 , 2016

@author: gayatri.l
'''

import unittest

from resourcelibrary import clients, backups,login, logout,jobs, restore
from testdata import clients_testdata, backups_testdata, restore_testdata
from util import reportController, logHandler,clientMessageUtilities,\
    featureHandler
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from Constants import ExecutionStatus
from resourcelibrary.backups import putBackupClientIncList, putBackupClientIncListWindows
import paramiko
from util.clientMessageUtilities import createFoldersWindows


class QAAUT_Win001(unittest.TestCase):
    def __init__(self, exeID, uebInfra, machineInfra):
        
        self.executionId = exeID
        
        '''
        Initialize UEB Configuration test steps
        '''
        self.uebInfra = uebInfra
        self.machineInfra = machineInfra
        
        '''
        initialize machine EnvironmentError
        '''
        
        self.name = "Win2016StdGUI"
        self.ipAddress = "192.168.197.200"
        self.includeList = "c:\Backup_folder"
        self.restoreFolder = "c:\Restore_folder"
        
        reportController.initializeReport(type(self).__name__, "Verify that backup and restore of Windows2016 client with include list, Inline dedupe on and Encryption off is successful")
        logHandler.logFile(self.executionId)
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify that backup and restore of Windows2016 client with include list, Inline dedupe on and Encryption off is successful.")
        
    def runTest(self):
        try:
                
            osType = ""
            appliance_ip = self.uebInfra["Mainurl"]
            print appliance_ip
            addedClientFlag = 0
            #login and get the authentication token
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                
            #create new folders on windows machine  
            createFoldersWindows(self.ipAddress, self.includeList, self.restoreFolder) 
            logHandler.logging.info("Folders added sucessfully")
            
            #Add few files on windows machine
            if (self.includeList!=''):
                clientMessageUtilities.addCustomSizedfiles_Window(self.ipAddress, self.includeList+"/", 3, "Backup", 5, "M")
             
            print "Check encryption status"    
            #Check encryption on appliance
            encryption_status = featureHandler.Get_Encryption_Status(appliance_ip, token)
            print "connect to client"
            if encryption_status == "off":
                logHandler.logging.info("Encryption on appliance is off")
                reportController.addTestStepResult("Encryption Off" , ExecutionStatus.PASS)
                print "Encryption status is off"
            if encryption_status == "on":
                raise ValueError('Appliance Encryption was found as ON')
            
            # check inline dedup appliance
            appliance_dedup = featureHandler.Check_Inline_dedup(appliance_ip)
            if appliance_dedup:
                print "Inline Deduplication is enabled"
                logHandler.logging.info("Inline Deduplication is enabled")
            else:
                print "Inline Deduplication is disabled"
                logHandler.logging.info("Inline Deduplication is disabled")
                print "Enabling Inline Deduplication"
                logHandler.logging.info("Enabling Inline Deduplication")
                #set inline dedup to on
                dedup_information = featureHandler.set_inline_dedup(appliance_ip, token)
                logHandler.logging.info("Inline Dedup set for appliance")
                reportController.addTestStepResult("Inline Dedup set for appliance", ExecutionStatus.PASS)
                print dedup_information
                
            #Send request to update a client
            #Check if client already exist
            #clientId = vclients.getSpecificClientByNameDB(self.ipAddress, self.name)
            clientId = vclients.getSpecificClientByNameDB(appliance_ip,self.name)
            print clientId
            logHandler.logging.info("Client ID returned from DB: "+str(clientId))
            if (clientId==0):
                # Add the client as it does not exist
                addedClientFlag =1
                print "get the actual response"
                
                actualresponse = clients.postWindowClient(appliance_ip, token,self.name, osType, clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, clients_testdata.use_ssl, clients_testdata.win_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted, clients_testdata.win_IsDefault, self.ipAddress, clientType="Windows")
                
 
                
                logHandler.logging.info("Actual Response - "+str(actualresponse))
                reportController.addTestStepResult("Addition of windows client", ExecutionStatus.PASS)
                #Get the client ID
                clientId = clients.getPostClientsRequestResponse(actualresponse, clients_testdata.keyForId)
                print clientId
            else:
                addedClientFlag =1
        
            #Trigger backup of client with include list
            actualresponse = putBackupClientIncListWindows(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType, backups_testdata.excudeSystemState)
            logHandler.logging.info("Actual Response - " + str(actualresponse))
            print "Actual Response for put backup - " + str(actualresponse)
            reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualresponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Backup successful")
                print "Backup Successful"
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Backup Failed with jobID:"+str(strJobId)
              
              
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            print "Backup ID based on jobid from Response - " + str(backupID)
              
                
            # check encryption status for backup
            if featureHandler.check_Encrytion_of_backup(appliance_ip, backupID):
                logHandler.logging.info("Backup Not Encrypted Encrypted" + str(backupID)) 
                reportController.addTestStepResult("Backup Not Encrypted Encrypted" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup Not Encrypted" + str(backupID)) 
                reportController.addTestStepResult("Backup Not Encrypted" , ExecutionStatus.PASS)
            
            # check inline dedup status for backup
            if featureHandler.verifySuccessfulDedupForABackup(appliance_ip, backupID):
                print "Backup inline deduplicated"
                logHandler.logging.info("Backup inline deduplicated" + str(backupID)) 
                reportController.addTestStepResult("Backup inline deduplicated" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup NOT inline deduplicated" + str(backupID)) 
                print "Backup NOT inline deduplicated"
                reportController.addTestStepResult("Backup NOT inline deduplicated" , ExecutionStatus.PASS)
            
            
            # Send API for restore
            
            actualResponse = restore.postWindowsRestore(appliance_ip, token, backupID, clientId, self.includeList, str(self.restoreFolder), restore_testdata.sid)
    
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Triggering Restore of Client" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print "Restore JobId - " + str(strJobId)
            
            #Check the status of Restore
            restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
             
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("Restore Successful" , ExecutionStatus.PASS)
                print "restore job completed and successful"
                
                #Verify if backup files are restore
                verifyresult= clientMessageUtilities.verifyWindowsRestore(self.includeList, "self.restoreFolder" + "\Backup_folder", self.ipAddress)
                print verifyresult
                if verifyresult == "matched":
                    
                    logHandler.logging.info("Restore verification matched")
                    reportController.addTestStepResult("Restore verification matched", ExecutionStatus.PASS)
                else:
                    logHandler.logging.info("Restore verification not matched")
                    reportController.addTestStepResult("Restore verification not matched", ExecutionStatus.FAIL)
                    
                
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Restore not successful", ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
                    
                    
        except Exception as e:
            
            print "found exception"
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            print "addedClientFlag: " + str(addedClientFlag)
            if(addedClientFlag):
                clients.deleteClients(appliance_ip,  token, clientId)
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
             
            #Logout
            print "log out started"
            logoutResponse = logout.doUserLogout(appliance_ip, token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            print "generating report summary"
            reportController.generateReportSummary(appliance_ip, self.executionId)
            
            #delete created folders on windows machine
            
            clientMessageUtilities.deleteFoldersWindows(self.ipAddress, self.includeList)
            logHandler.logging.info("Backup Folder deleted")
            clientMessageUtilities.deleteFoldersWindows(self.ipAddress, self.restoreFolder)
            logHandler.logging.info("Restore folder deleted")
        
              
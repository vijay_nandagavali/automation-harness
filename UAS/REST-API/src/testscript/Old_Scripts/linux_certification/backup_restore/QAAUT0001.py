'''
Created on Jan 25, 2017

@author: pratik.p
'''
import unittest

from resourcelibrary import clients, backups,login, logout,jobs, restore
from testdata import clients_testdata, backups_testdata, restore_testdata
from util import reportController, logHandler,clientMessageUtilities
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig



class QAAUT0001(unittest.TestCase):    
    def __init__(self, exeID, uebInfra, machineInfra):
        self.executionId = exeID
        '''
        Initialize UEB Configuration test steps
        '''
        
        self.uebInfra=uebInfra
        self.machineInfra=machineInfra 
        '''
        Initialize machine environment
        ''' 
        self.name=machineInfra["linuxname"] if "linuxname" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["linuxipAddress"] if "linuxipAddress" in machineInfra.keys() else ""
        self.includeList=machineInfra["linuxincludeList"] if "linuxincludeList" in machineInfra.keys() else ""
        self.restoreFolder=machineInfra["linuxrestoreFolder"] if "linuxrestoreFolder" in machineInfra.keys() else ""
        #self.name = "linuxCl"
        #self.ipAddress = "192.168.192.119"
        #self.includeList = "/root/backup_Folder"
        #self.restoreFolder = "/root/restore_Folder"
        self.executionId = exeID
           
        
        
        
        testCaseName=""
        testCaseStep=""            
        reportController.initializeReport(type(self).__name__, "Put - Backup Inremental for a linux client and Restore")
        logHandler.logFile(self.executionId)
        print "log file generated"
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Backup Incremental for a linux client having include list")
        
        
        
    def runTest(self):        
        try:
            
            osType = ""
            appliance_ip=self.uebInfra["Mainurl"]
            #appliance_ip = "192.168.199.170"
            apiconfig.environmentIp = appliance_ip
            addedClientFlag = 0
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            clientMessageUtilities.SetupLinux(self.ipAddress)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(appliance_ip, self.name)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postLinuxClient(appliance_ip,token, self.name, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.linux_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted,   
                                                clients_testdata.linux_IsDefault, self.ipAddress)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                print "client add response: "+str(actualResponse)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            
            '''
            Add Files on linux
            '''
            if(self.includeList!=""):
                clientMessageUtilities.addCustomSizedfilesOnLinux(self.ipAddress,self.includeList+"/",3,5,"M")
            # Step 3: Send the PUT request
            actualResponse = backups.putBackupClientIncList(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType   )
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            print "Actual Response for put backup - " + str(actualResponse)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            
           
            
            # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
            '''
            dbResult = vbackups.getJobByIdFromDB(appliance_ip, strJobId)
            logHandler.logging.info("JOB triggered from jobID : " + str(dbResult))
            
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Database verification failed")
                reportController.addTestStepResult("Failed to find Job with Job ID" + str(strJobId), ExecutionStatus.FAIL)    
            '''
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Backup successful")
                print "Backup Successful"
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Backup Failed with jobID:"+str(strJobId)
                
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            print "Backup ID based on jobid from Response - " + str(backupID)
            
            if(self.includeList !=""):
                clientMessageUtilities.addCustomSizedfilesOnLinux(self.ipAddress,self.includeList,3,5,"M")
                logHandler.logging.info("File changes Done for incremental backup")
                print "File changes Done for incremental backup"\
            
            actualResponse = backups.putBackupClientIncList(appliance_ip,token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), self.includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.incrementalBackupType   )
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            print "Actual Response for put backup - " + str(actualResponse)
            reportController.addTestStepResult("Send API request Incremental Backup" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName)
            
           
            
            # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
            '''
            dbResult = vbackups.getJobByIdFromDB(appliance_ip, strJobId)
            logHandler.logging.info("JOB triggered from jobID : " + str(dbResult))
            
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Database verification failed")
                reportController.addTestStepResult("Failed to find Job with Job ID" + str(strJobId), ExecutionStatus.FAIL)    
            '''
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Incremental Backup successful")
                print "Incremental Backup Successful"
                reportController.addTestStepResult("Incremental Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Incremental Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Incremental Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Incremental Backup Failed with jobID:"+str(strJobId)
                
            #Get Backup No from jobid
            backupIDIncr = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Inremental Backup ID based on jobid from Response - " + str(backupIDIncr)) 
            print "Incremental Backup ID based on jobid from Response - " + str(backupIDIncr)
            
                        
            
            # Step 3: Send API for restore
            actualResponse = restore.postLinuxRestore(appliance_ip, token, backupIDIncr, clientId, self.includeList, self.restoreFolder, restore_testdata.sid)
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print "Restore JobId - " + str(strJobId)
            
            #Check the status of Restore
            restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("restore Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("restore not successful", ExecutionStatus.FAIL)
                
            if(self.includeList==""):
                    logHandler.logging.info("include list not present for linux so no client side verification")
            else:  
                checksum=clientMessageUtilities.verifyRestoreFolderAgainstBackupFolderOnLinux(self.includeList, self.restoreFolder+self.includeList, self.ipAddress)
                if(checksum):
                    reportController.addTestStepResult("Checksum restore verification successful",ExecutionStatus.PASS)
                    logHandler.logging.info("Backup and Restore files matched")
                    print "Backup and Restore files matched"
                else:
                    
                    reportController.addTestStepResult("Backup and Restore files did not match",ExecutionStatus.FAIL)
                    logHandler.logging.info("Backup and Restore files did not match")
            
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            '''
            if(addedClientFlag):
                clients.deleteClients(token, clientId)
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            '''
            reportController.generateReportSummary(appliance_ip,self.executionId)



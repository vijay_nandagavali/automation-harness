'''
Created on Jan 23, 2017

@author: pratik.p
'''

from testscript.linux_certification.backup_restore import QAAUT0000
from testscript.linux_certification.backup_restore import QAAUT0001
from testscript.linux_certification.backup_restore import QAAUT0002
from testscript.linux_certification.backup_restore import QAAUT0004
from testscript.linux_certification.backup_restore import QAAUT0005
from testscript.linux_certification.backup_restore import QAAUT0006
from testscript.linux_certification.backup_restore import QAAUT_D1
from testscript.linux_certification.backup_restore import QAAUT_D2
from testscript.linux_certification.backup_restore import QAAUT_D3
from testscript.linux_certification.backup_restore import QAAUT_D4
from testscript.linux_certification.archive import QAAUT0007
from testscript.linux_certification.archive import QAAUT0008
from testscript.linux_certification.archive import QAAUT0009
from testscript.linux_certification.archive import QAAUT0010

class linux_certification_driver(object):
    def __init__(self,exeID,uebInfra,machineInfra,testType):
        self.exeID = exeID
        self.uebInfra = uebInfra
        self.machineInfra = machineInfra
        self.testType=testType
        
    
    def callScripts(self):
        if(self.testType == "ALL" or self.testType == "Backup_Restore"):
            QAAUT0000.QAAUT0000(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT0001.QAAUT0001(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT0002.QAAUT0002(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT0004.QAAUT0004(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT0005.QAAUT0005(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT0006.QAAUT0006(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT_D1.QAAUT_D1(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT_D2.QAAUT_D2(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT_D3.QAAUT_D3(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT_D4.QAAUT_D4(self.exeID,self.uebInfra,self.machineInfra).runTest()            
        if(self.testType == "ALL" or self.testType == "Archive"):
            QAAUT0007.QAAUT0007(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT0008.QAAUT0008(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT0009.QAAUT0009(self.exeID,self.uebInfra,self.machineInfra).runTest()
            QAAUT0010.QAAUT0010(self.exeID,self.uebInfra,self.machineInfra).runTest()
            
            
        
        
    


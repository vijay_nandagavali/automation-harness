'''
Created on Feb 19, 2015

@author: Rahul A
'''

import unittest

from resourcelibrary import mailconfig, login, logout
from testdata import mailconfig_testdata
from util import reportController
from verificationsript import vmailconfig

from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1222_DeleteMailConfig(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Delete Mail Config")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Prerequisite setup - setup the schedule mail config parameter
            actualResponse = mailconfig.putMailConfigSchedule(token)
            print actualResponse
            reportController.addTestStepResult("Prerequisite setup - Send API request" , ExecutionStatus.PASS)
            # Verify the response parameter
            responseResult = mailconfig.getPutRequestResponse(actualResponse)
            print responseResult
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Prerequisite setup - Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification for prerequisite setup")
            # Step 2: Delete schedule from mail config
            actualResponse = mailconfig.deleteMailConfigSchedule(token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Verification 1: verify the response parameter
            responseResult = mailconfig.getDeleteRequestResponse(actualResponse)
            print responseResult
            #Verification 1: verify the response parameter
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification for delete mail config")
            #Verification 2: Verify the database results
            strScheduleInfo = vmailconfig.getMailScheduleInformationFromMachine()
            if(strScheduleInfo == ""):
                reportController.addTestStepResult("Mail config successfully deleted", ExecutionStatus.PASS)
            else:    
                reportController.addTestStepResult("Mail config deletion failed", ExecutionStatus.FAIL)
            # logout from the system
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
              
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
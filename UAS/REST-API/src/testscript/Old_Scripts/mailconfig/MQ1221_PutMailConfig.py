'''
Created on Feb 6, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import mailconfig, login, logout
from testdata import mailconfig_testdata
from util import reportController
from verificationsript import vmailconfig

from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1221_PutMailConfig(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Put Mail Config")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = mailconfig.putMailConfig(token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            #Verification 1: Verify the response parameter
            responseResult = mailconfig.getPutRequestResponse(actualResponse)
            print responseResult
            #Verification 1: verify the response parameter
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            # Verification2: Verify get Mail Config from machine
            actualResponse = mailconfig.getMailConfig(token)      
            mailconfigFromMachine = vmailconfig.getMailConfigInformationFromMachine()
            print mailconfigFromMachine
            
            mailconfigFromResponse = vmailconfig.getMailConfigInformationFromResponse(actualResponse)
            print mailconfigFromResponse
            
            self.assertListEqual(mailconfigFromMachine, mailconfigFromResponse, "Failed Data base verification: Expected Response [" + str(mailconfigFromMachine) + "] and Actual Response [" + str(mailconfigFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            # logout from the system
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
              
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
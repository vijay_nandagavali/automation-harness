'''
Created on Feb 6, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import mailconfig, login, logout
from testdata import mailconfig_testdata
from util import reportController
from verificationsript import vmailconfig

from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1220_GetMailConfig(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Mail Config")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            #Prerequisite setup
            actualResponse = mailconfig.putMailConfig(token)
            print actualResponse
            
            responseResult = mailconfig.getPutRequestResponse(actualResponse)
            print responseResult
            if(responseResult == "Pass"):
                reportController.addTestStepResult("Prerequisite setup", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Failed in prerequisite setup", ExecutionStatus.FAIL)    
            
            # Step 1: Send request to get all alerts
            actualResponse = mailconfig.getMailConfig(token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = mailconfig.getMailConfigParametersInJson(actualResponse)
            print actualResponseParams
            # sort expected response parameter list            
            mailconfig_testdata.responseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(mailconfig_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(mailconfig_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)  
            
            # Verification2: Verify get Mail Config from machine      
            mailconfigFromMachine = vmailconfig.getMailConfigInformationFromMachine()
            print mailconfigFromMachine
            
            mailconfigFromResponse = vmailconfig.getMailConfigInformationFromResponse(actualResponse)
            print mailconfigFromResponse
            
            self.assertListEqual(mailconfigFromMachine, mailconfigFromResponse, "Failed Data base verification: Expected Response [" + str(mailconfigFromMachine) + "] and Actual Response [" + str(mailconfigFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            # logout from the system
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
              
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
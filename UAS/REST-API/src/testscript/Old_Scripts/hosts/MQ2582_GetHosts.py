'''
Created on Jun 23, 2016

@author: rahula
'''
import unittest

from resourcelibrary import hostname, login, logout
from testdata import hostname_testdata
from util import reportController, logHandler
from verificationsript import vhostname
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2582_GetHosts(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get the entries in the hosts file")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            logHandler.logging.info("Auth Token: " + token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get hostname information
            actualResponse = hostname.getHostsInformation(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = hostname.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            # sort expected response parameter list            
            hostname_testdata.hostsresponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str( hostname_testdata.hostsresponseParameter))
            
            # Verification 1: Verify the response parameter        
            self.assertListEqual(hostname_testdata.hostsresponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(hostname_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            
            # Verification 2: Verify the API response        
            hostnameFromDB = vhostname.getHostFromDB()
            logHandler.logging.info("Data from DB: " + str(hostnameFromDB))
            hostnameFromResponse = vhostname.getHostsFromResponse(actualResponse)
            logHandler.logging.info("Data from response: " + str(hostnameFromResponse))
            
            self.assertEqual(hostnameFromDB, hostnameFromResponse, "Failed Data base verification: Expected Response [" + str(hostnameFromDB) + "] and Actual Response [" + str(hostnameFromResponse) + "]")
            logHandler.logging.info("Database verification done")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)



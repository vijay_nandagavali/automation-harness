'''
Created on Jun 24, 2016

@author: rahula
'''

import unittest

from resourcelibrary import hostname, login, logout
from testdata import hostname_testdata
from util import reportController, logHandler
from verificationsript import vhostname
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2583_PostHosts(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Post a hosts entry for a specified system")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            logHandler.logging.info("Auth Token: " + token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to post hosts information
            actualResponse = hostname.postHostsInformation(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Send API request done")
            
            # Verification 1: Verify the response parameter
            responseResult = hostname.getPostRequestResponse(actualResponse)
            print responseResult
            if (responseResult == "Pass"):
                logHandler.logging.info("Verified the response parameter")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Failed in response parameter verification")
                raise Exception ("Failed response verification")
            
            #Verification 2: Verify by reading the hosts file on the UEB Machine
            if(vhostname.verifyHostsInformationFromMachine()):
                logHandler.logging.info("Host entry found on the machine in the hosts file")
                reportController.addTestStepResult("Host entry found on the machine in the hosts file", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Failed to find the host entry on the machine in the hosts file")
                reportController.addTestStepResult("Failed to find host entry in the hosts file", ExecutionStatus.FAIL) 
                raise Exception("Failed in finding the host entry in the hosts file")   
           
            #Cleanup: Delete the hosts entry posted by this script
            actualResponse = hostname.deleteHostsInformation(token)
            reportController.addTestStepResult("Cleanup: Send API request - DELETE hosts " , ExecutionStatus.PASS)
            logHandler.logging.info("Cleanup: Send API request  - DELETE hosts done")
            
            # Verification 1: Verify the response parameter
            responseResult = hostname.getDeleteRequestResponse(actualResponse)
            print responseResult
            if (responseResult == "Pass"):
                logHandler.logging.info("Verified the response parameter")
                reportController.addTestStepResult("Verified response parameter", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Failed in response parameter verification")
                raise Exception ("Failed response verification")
            
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            #Logout from the system
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            logHandler.logging.info("Successfully logged out from the sytem")
            
            



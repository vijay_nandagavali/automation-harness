'''
Created on Mar 31, 2016

@author: Prateek
'''
import unittest
from util import reportController, logHandler
from resourcelibrary import login, clients, backups, restore, jobs, logout
from testdata import clients_testdata, backups_testdata, restore_testdata
from verificationsript import vbackups, vrestore, vclients
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1754_RestorewithPreserveFileStructureOff_Windows(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Restore with 'Preserve File structure' option OFF for Windows client on default location")
        logHandler.logging.info("TEST TITLE:::\tRestore - Preserve File Structure OFF - Windows client - Default location")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Test data used in the script
            winName = clients_testdata.getmachineName("WINDOWS")
            osType = clients_testdata.getType("WINDOWS")
            winIp = clients_testdata.getmachineIP("WINDOWS")
            includeList = clients_testdata.getShareName("WINDOWS")
            
            restoreDirectory = clients_testdata.getShareName("RESTORE")
            
            addedClientFlag = 0
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)
            logHandler.logging.info("Auth Token:" + str(token))         
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(winName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postWindowClient(token, winName, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.win_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted,   
                                                clients_testdata.win_IsDefault, winIp)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            
            
            
            
            
            if(clientId):
                #Create Master Backup
                actualResponse = backups.putBackupClientIncList(token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType   )
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                #Extract jobid and client name from response
                responseResult = backups.getPutRequestResponse(actualResponse)
                strJobId = responseResult["data"][0]["job_id"] 
                strClientName = responseResult["data"][0]["client_name"] 
                logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
                
                
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                if(backupJobStatus =="Successful" or backupJobStatus =="Warning" ):
                    logHandler.logging.info("Backup successful")
                    reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
                else:
                    logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                    reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL) 
                      
                #Get Backup No from jobid
                backupID = vrestore.getArchiveRestoreBackupIdFromDB(strJobId)
                logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            else:
                raise Exception("Client ID not found")
            
            
                      
            # Step 3: Send API for restore
            actualResponse = restore.postWindowsRestoreWithOptions(token, backupID, clientId, includeList, restore_testdata.sid, True, False, False, False, False, "")
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            
            # Get actual response parameter from response
            actualResponseParams = backups.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Restore Actual Response Parameters - " + str(actualResponseParams))
            
            restore_testdata.responseParameter.sort()
            logHandler.logging.info("Restore Expected Response Parameters - " + str(restore_testdata.responseParameter))
            
            self.assertListEqual(restore_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(restore_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify Restore response parameter", ExecutionStatus.PASS)
            
            # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
            dbResult = vbackups.getJobByIdFromDB(strJobId)
            logHandler.logging.info("JOB triggered from jobID : " + str(dbResult))
            
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Database verification failed")
                reportController.addTestStepResult("Failed to find Job with Job ID" + str(strJobId), ExecutionStatus.FAIL)
            
            #Check the status of Restore
            restoreJobStatus=jobs.waitForJobCompletion(restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("restore Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("restore not successful", ExecutionStatus.FAIL) 
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
            
        finally:
            #Cleanup
            if(addedClientFlag):
                clients.deleteClients(token, clientId)
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)

            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on Apr 25, 2016

@author: vijay.n
'''

import unittest
from util import reportController, logHandler
from resourcelibrary import login,logout,settings
from testdata import settings_testdata
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ6849_GET_VerifySuccessfulExecutionOfGETsettingsRESTAPI(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get Verify Successful Execution Of GET settings RESTAPI")
        logHandler.logging.info("TEST TITLE:::MQ6849_GET_VerifySuccessfulExecutionOfGETsettingsRESTAPI")
        self.executionId = exeID
        
    def runTest(self):        
        try:
                       
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            reportController.tcExecutionStatus(ExecutionStatus.PASS) 
            
            systemInformation = settings.getSettings(settings_testdata.sid, token)
            
            actualResponseParams = settings.getSettingsJobParametersInJson(systemInformation)
            settings_testdata.responseParameter.sort()
            logHandler.logging.info("Actual Response Parameters- " + str(actualResponseParams))
                        
            self.assertListEqual(settings_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(settings_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verified response parameter", ExecutionStatus.PASS)  
                                             
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
            
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)            
            reportController.generateReportSummary(self.executionId)            


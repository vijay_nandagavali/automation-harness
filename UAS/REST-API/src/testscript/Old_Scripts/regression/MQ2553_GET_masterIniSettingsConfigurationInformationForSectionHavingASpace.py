'''
Created on Mar 31, 2016

@author: Vijay.N
'''
import unittest
from util import reportController, logHandler
from resourcelibrary import login,logout,settings
from testdata import settings_testdata
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2553_GET_masterIniSettingsConfigurationInformationForSectionHavingASpace(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Verify Configuration Information for the section having spaces in it")
        logHandler.logging.info("TEST TITLE:::\tRestore - Verify Configuration Information for the section having spaces in it")
        self.executionId = exeID
        
    def runTest(self):        
        try:
                       
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            reportController.tcExecutionStatus(ExecutionStatus.PASS) 
            
            systemInformation = settings.getServerInformation(settings_testdata.sid, token)
            
            respResult = settings.getPutRequestResponse(systemInformation)
            
            serverField = respResult ["Server Information"][0]["field"] 
            serverDescription = respResult ["Server Information"][0]["description"] 
            serverValue = respResult ["Server Information"][0]["value"] 
                       
            logHandler.logging.info("Actual Response - " + str(systemInformation))
            
            logHandler.logging.info("Server Field is - " + str(serverField))
            logHandler.logging.info("Server Value is- " + str(serverValue))
            logHandler.logging.info("The Server Description is - " + str(serverDescription))

            actualResponseParams = settings.getSettingsJobParametersInJson(systemInformation)
            settings_testdata.responseParameter.sort()
            logHandler.logging.info("Actual Response Parameters- " + str(actualResponseParams))
                        
            self.assertListEqual(settings_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(settings_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verified response parameter", ExecutionStatus.PASS)  
            
                                 
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
            
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)            

            
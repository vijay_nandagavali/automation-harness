'''
Created on Mar 30, 2016

@author: rahula
'''

import unittest
from resourcelibrary import restore, login, logout, clients, backups, jobs
from testdata import restore_testdata, clients_testdata, backups_testdata
from util import reportController, logHandler, clientMessageUtilities
from verificationsript import vrestore, vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig
from testdata.backups_testdata import includeList

class MQ1731_RestorewithOverwriteOff_RestoreNewOnlyOff_Windows(unittest.TestCase):
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Restore with 'Overwrite Existing files' option OFF and 'Restore New files only' option OFF for Windows client on non default location")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Restore with 'Overwrite Existing files' option OFF and 'Restore New files only' option OFF for Windows client on non default location")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Test data used in the script
            '''
            winName = "win7-client1"
            winIp = "192.168.197.179"
            includeList = "E:/Backup_Test"
            fileName = backups_testdata.win37_fileName
            restoreDir = "E:/Restore_Test"
            restoreFileName = str(includeList).replace("C:/", "") + "/" + str(fileName)
            restoreSearchStr = restore_testdata.searchString
            '''
            winName = clients_testdata.getmachineName("win7-client1")
            winIp = clients_testdata.getmachineIP("win7-client1")
            includeList = clients_testdata.getShareName("win7-client1")
            fileName = backups_testdata.win37_fileName
            restoreDir = clients_testdata.getShareName("win7-client1-Restore")           
            restoreFileName = str(includeList).replace("C:/", "") + "/" + str(fileName)
            restoreSearchStr = restore_testdata.searchString
            
            
            osType = clients_testdata.win_OsType
           
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            addedClientFlag = 0
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(winName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag = 1
                actualResponse = clients.postWindowClient(token, winName, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.win_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted,   
                                                clients_testdata.win_IsDefault, winIp)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            
            #Create Dedup Folder on client
            createFolder = clientMessageUtilities.addDedupFolder(includeList, winIp)
            logHandler.logging.info("Folder created: " + str(createFolder))  
            
            #Add 8KB file to incudeList folder
            testresult = clientMessageUtilities.addCustomSizedfiles(includeList, "8", "Master", "8", "KB", winIp)
            self.assertTrue(testresult, "Failed to add 8KB files")
            logHandler.logging.info("8KB files added")
            reportController.addTestStepResult("Add 8KB files", ExecutionStatus.PASS)
            
            # Create Master Backup
            actualResponse = backups.putBackupClientIncList(token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType   )
            
            logHandler.logging.info("Actual Response - " + str(actualResponse))
                        
            #Extract jobid and client name from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
                        
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful" or backupJobStatus =="Warning" ):
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL) 
            
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID))            
            
            #Edit the file
            testresult=clientMessageUtilities.modifyFile(includeList, fileName, winIp)
            self.assertTrue(testresult, "Failed to edit the file")
            logHandler.logging.info("File edit successful..")
            reportController.addTestStepResult("Edit file", ExecutionStatus.PASS)
            
            # Create Master Backup
            actualResponse = backups.putBackupClientIncList(token, backups_testdata.jobType, backups_testdata.verifyNone, int(clientId), includeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType   )
            logHandler.logging.info("Actual Response - " + str(actualResponse))
                        
            #Extract jobid and client name from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
                        
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful" or backupJobStatus =="Warning" ):
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL) 
            
            #Get Backup No from jobid
            backupID2 = vrestore.getArchiveRestoreBackupIdFromDB(strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID2))            
            
            # Trigger restore with options
            #actualResponse = restore.postWindowsRestore(token, backupID, clientId, includeList, restore_testdata.sid)
            actualResponse = restore.postWindowsRestoreWithOptions(token, backupID, clientId, includeList, restore_testdata.sid, False, False, False, False, True, restoreDir)
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            
            
            # Get actual response parameter from response
            actualResponseParams = backups.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Restore Actual Response Parameters - " + str(actualResponseParams))
            
            restore_testdata.responseParameter.sort()
            logHandler.logging.info("Restore Expected Response Parameters - " + str(restore_testdata.responseParameter))
            
            self.assertListEqual(restore_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(restore_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify Restore response parameter", ExecutionStatus.PASS)
            
            # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
            dbResult = vbackups.getJobByIdFromDB(strJobId)
            logHandler.logging.info("JOB triggered from jobID : " + str(dbResult))
            
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Database verification failed")
                reportController.addTestStepResult("Failed to find Job with Job ID" + str(strJobId), ExecutionStatus.FAIL)    
            
            #Check the status of Restore
            restoreJobStatus=jobs.waitForJobCompletion(restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("restore Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("restore not successful", ExecutionStatus.FAIL) 
            
            #Verify the file content to see if the previous version of the file is overwritten
            testresult=clientMessageUtilities.searchFileForText(includeList, fileName, restoreSearchStr, winIp)
            self.assertTrue(testresult, "Found the search text in file")
            logHandler.logging.info("File is not overwritten with the original data..")
            reportController.addTestStepResult("Verify existing file", ExecutionStatus.PASS)
            
            #Verify if the restore file version is copied to the target location
            testresult=clientMessageUtilities.searchFileForText(restoreDir, restoreFileName, restoreSearchStr, winIp)
            self.assertTrue(testresult, "Found the search text in file")
            logHandler.logging.info("Restore file is copied to the target location..")
            reportController.addTestStepResult("Verify Restore file", ExecutionStatus.PASS)
                       
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            
            #cleanup
            if(addedClientFlag):
                clients.deleteClients(token, clientId)
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
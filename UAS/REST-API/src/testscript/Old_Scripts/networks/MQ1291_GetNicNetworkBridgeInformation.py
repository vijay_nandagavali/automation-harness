'''
Created on Apr 7, 2015

@author: Amey.k
'''

import unittest
from resourcelibrary import networks, login, logout
from testdata import networks_testdata
from util import reportController, logHandler
from verificationsript import vnetworks
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1291_GetNicNetworkBridgeInformation(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get Network NIC Bridge Information")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - NIC Network Bridge Information")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = networks.getNetworkNicBridgeInformation(token, networks_testdata.systemId, networks_testdata.nic)            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = networks.getNetworkNicBridgeParametersInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            # sort expected response parameter list            
            networks_testdata.networkBridgeNicResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(networks_testdata.networkBridgeNicResponseParameter))
            
            # Verification 1: Verify get all alerts response parameter
            if (len(actualResponseParams)!=0):       
                self.assertListEqual(networks_testdata.networkBridgeNicResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(networks_testdata.networkBridgeNicResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)  
                      
                # Verification 2: Verify get all alerts API response        
                networkFromMachine = vnetworks.getNetworkBridgeInformationFromMachine()
                logHandler.logging.info("Data returned from Machine: " + str(networkFromMachine))
                if(networkFromMachine == []):
                    networkFromMachine = ['']
                logHandler.logging.info("Data returned from Machine: " + str(networkFromMachine))
                
                networkFromResponse = vnetworks.getNetworkBridgeInformationFromResponse(actualResponse)
                logHandler.logging.info("Data from response: " + str(networkFromResponse))
                
                self.assertListEqual(networkFromMachine, networkFromResponse, "Failed Data base verification: Expected Response [" + str(networkFromMachine) + "] and Actual Response [" + str(networkFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)            
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
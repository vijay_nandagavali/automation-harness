'''
Created on Nov 12, 2014

@author: Amey.k

'''

import unittest

from resourcelibrary import networks, login, logout
from testdata import networks_testdata
from util import reportController
from verificationsript import vnetworks
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1324_GetVirtualBridgeNetworkParameters(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Virtual Bridge Network Parameters")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = networks.getVirtualBridgeInformation(token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = networks.gefVirtualBridgeNetworkParametersInJson(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            networks_testdata.virtualBridgeResponseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(networks_testdata.virtualBridgeResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(networks_testdata.virtualBridgeResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            networkFromMachine = vnetworks.getVirtualBridgeNetworkInformation()
            print networkFromMachine
            networkFromResponse = vnetworks.getVirtualBridgeNetworkInformationFromResponse(actualResponse)
            print networkFromResponse
            self.assertListEqual(networkFromMachine, networkFromResponse, "Failed Data base verification: Expected Response [" + str(networkFromMachine) + "] and Actual Response [" + str(networkFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            

'''
Created on Oct 21, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import networks, login, logout
from testdata import networks_testdata
from util import reportController
from verificationsript import vnetworks
from config.Constants import ExecutionStatus
from config import apiconfig




class MQ1099_GetNetworkInformation(unittest.TestCase):    
    def __init__(self, exeID):
           
        reportController.initializeReport(type(self).__name__, "Get Network Information")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = networks.getNetworkInformation(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = networks.getNetworkListParametersinJson(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            networks_testdata.responseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(networks_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(networks_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            networkFromMachine = vnetworks.getNetworkInformationFromMachine()
            print networkFromMachine
            networkFromResponse = vnetworks.getNetworkInformationFromResponse(actualResponse)
            print networkFromResponse
            
            #Removing mismatch items from the lists, change in command line output
            del networkFromMachine[5]
            del networkFromMachine[4]
            del networkFromMachine[3]
            del networkFromMachine[2]
            del networkFromResponse[5]
            del networkFromResponse[4]
            del networkFromResponse[3]
            del networkFromResponse[2]
            self.assertListEqual(networkFromMachine, networkFromResponse, "Failed Data base verification: Expected Response [" + str(networkFromMachine) + "] and Actual Response [" + str(networkFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
             
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
 


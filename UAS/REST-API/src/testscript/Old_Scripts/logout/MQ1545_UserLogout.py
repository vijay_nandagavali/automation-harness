'''
Created on Sep 26, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import logout

from util import reportController

import json


class MQ1545_UserLogout(unittest.TestCase):
   
#     def __init__(self):
#         logHandler.initializeReport(type(self).__name__)
    
        
    def doLogout(self, token):
        actualResponse = logout.doUserLogout(token)
        responseData = json.loads(actualResponse[0])
        # print str(responseData)
        if (str(responseData[u'result'][0][u'code']) == u'0'):
            print "Logout Successful"
            # logHandler.addTestStepResult("Logout,Pass")
            reportController.addTestStepResult("Logout", "Pass")
        else:
            reportController.addTestStepResult("Logout", "Fail")
            
        
        
    
#     def runTest(self):        
#         try:
#             logHandler.initializeReport(type(self).__name__)
#             token = UEB869_GetAuthenticationToken.UEB869_GetAuthenticationToken().getAuthTokenAfterLogin()
#             print token
#             # Step 1: Send request to get all alerts
#             count = vlogout.getPreAuthTokenVerificationDBCount()
#             print "UUID count before logout is called is: " + str(count)
#             actualResponse = logout.doUserLogout(token)
# 
#             logHandler.addTestStepResult("Send API request,Pass")
# 
#             
#             # Verification 1: Verify get all alerts response parameter        
#             self.assertListEqual(logout_testdata.response, actualResponse, "Failed Response verification: Expected response [" + str(logout_testdata.response) + "] and Actual parameter [" + str(actualResponse) + "]")
#             logHandler.addTestStepResult("Verify response parameter,Pass")        
#             # Verification 2: Verify get all alerts API response        
#             ListFromDB = vlogout.getAuthTokenVerificationDB(count)
#             # jobsFromDB.sort()
#             print ListFromDB
#              
#             ListFromResponse = vlogout.getLogoutVerification(actualResponse[0])
#             # jobsFromResponse.sort()
#             print ListFromResponse
#             self.assertListEqual(ListFromDB, ListFromResponse, "Failed Data base verification: Expected Response [" + str(ListFromDB) + "] and Actual Response [" + str(ListFromResponse) + "]")
#             logHandler.addTestStepResult("Compare API response with data base values,Pass")
#             logHandler.addTestStepResult(type(self).__name__ + ",Pass")
#         except Exception as e:
#             logHandler.addTestStepResult("\"" + str(e) + "\"" + ",Fail")
#             logHandler.addTestStepResult(type(self).__name__ + ",Fail")
#             print e    
#         finally:
#             logHandler.generateReportSummary()
            
# alert = UEB1855_UserLogout()
# alert.runTest()
# print "Passed!!"

'''
Created on Aug 8, 2016

@author: vijay.n
'''

import unittest
from util import reportController, logHandler, exceptionHandler, clientMessageUtilities
from testdata import login_testdata
from resourcelibrary import login
from verificationsript import vencryption
from Constants import ExecutionStatus
from time import sleep
from __builtin__ import str
from config import apiconfig


class Backup(unittest.TestCase):    
    def __init__(self, exeID, tcid, uebConfig, uebInfra, machineInfra):
        self.executionId = exeID
        '''
        Initialize UEB Configuration test steps
        '''
        self.clientFlavour=uebConfig["clientFlavour"]
        self.backupType=uebConfig["backupType"]
        self.dedupType=uebConfig["dedupType"]
        self.sisCompression=uebConfig["sisCompression"] if "sisCompression" in uebConfig.keys() else ""
        self.encryptionStatus=uebConfig["encryptionStatus"]
        self.clientType=uebConfig["clientType"]
        self.compressionType=uebConfig["compressionType"] if "compressionType" in uebConfig.keys() else ""
        self.uebInfra=uebInfra
        self.machineInfra=machineInfra 
        '''
        Initialize machine environment
        ''' 
        self.name=machineInfra[self.clientFlavour+"name"] if self.clientFlavour+"name" in machineInfra.keys() else ""
        self.ipAddress=machineInfra[self.clientFlavour+"ipAddress"] if self.clientFlavour+"ipAddress" in machineInfra.keys() else ""
        self.includeList=machineInfra[self.clientFlavour+"includeList"] if self.clientFlavour+"includeList" in machineInfra.keys() else ""
           
        self.username=machineInfra[self.clientFlavour+"username"] if self.clientFlavour+"username" in machineInfra.keys() else ""
        self.password=machineInfra[self.clientFlavour+"password"] if self.clientFlavour+"password" in machineInfra.keys() else ""
        self.shareName=machineInfra[self.clientFlavour+"shareName"] if self.clientFlavour+"shareName" in machineInfra.keys() else ""
        
        self.excludeList=machineInfra[self.clientFlavour+"excludeList"] if self.clientFlavour+"excludeList" in machineInfra.keys() else ""
        self.restoreFolder=machineInfra[self.clientFlavour+"restoreFolder"] if self.clientFlavour+"restoreFolder" in machineInfra.keys() else ""
        self.esxName=machineInfra[self.clientFlavour+"esxName"] if self.clientFlavour+"esxName" in machineInfra.keys() else ""
        self.instanceName=machineInfra[self.clientFlavour+"instanceName"] if self.clientFlavour+"instanceName" in machineInfra.keys() else ""
        self.resourcePool=machineInfra[self.clientFlavour+"resourcePool"] if self.clientFlavour+"resourcePool" in machineInfra.keys() else ""
        self.databaseName=machineInfra[self.clientFlavour+"databaseName"] if self.clientFlavour+"databaseName" in machineInfra.keys() else ""
        self.serverName=machineInfra[self.clientFlavour+"serverName"] if self.clientFlavour+"serverName" in machineInfra.keys() else ""
        self.port=machineInfra[self.clientFlavour+"port"] if self.clientFlavour+"port" in machineInfra.keys() else ""
        self.target=machineInfra[self.clientFlavour+"target"] if self.clientFlavour+"target" in machineInfra.keys() else ""
        self.lun=machineInfra[self.clientFlavour+"lun"] if self.clientFlavour+"lun" in machineInfra.keys() else ""
        self.clientIp=machineInfra[self.clientFlavour+"instanceIP"] if self.clientFlavour+"instanceIP" in machineInfra.keys() else self.ipAddress
        testCaseName=""
        testCaseStep=""
        
        logHandler.logFile(self.executionId)
        logHandler.logging.info("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
        logHandler.logging.info("-*-*-*-*-*-*-*-*-*-*-*-*Start Of Test Case: "+str(uebConfig["testScriptId"])+" *-*-*-*-*-*-*-*-*-*-*-*-")
        logHandler.logging.info("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
        
        testCaseName = reportController.getTestcaseName(self.clientType,self.clientFlavour,self.backupType,self.dedupType,self.sisCompression,
                                                        self.compressionType,self.encryptionStatus," ")
                
        reportController.initializeReport(tcid, testCaseName)
        
        
    def runTest(self): 
        try: 
            '''
            test case status variable, to be made fail in case of an exception.
            '''
            testCaseStatus = 'pass'
            print self.uebInfra
            
            apiconfig.environmentIp=self.uebInfra["Mainurl"]
            print apiconfig.environmentIp
            
            ''' for ssl error '''
            import ssl
            if hasattr(ssl, '_create_unverified_context'):
                ssl._create_default_https_context = ssl._create_unverified_context
             
            '''
            login and get the authentication token
            '''
            testCaseStep = "login"
            token = login.getAuthenticationToken( apiconfig.machineuser,apiconfig.machinepassword)
            logHandler.logging.info("Auth Token:" + str(token))                     
            self.assertNotEqual(token, "", "Empty token received")
            
            
            print "Login Successful"
            testCaseStatus = ExecutionStatus.PASS
            reportController.addTestStepResult(testCaseStep, ExecutionStatus.PASS)
            
            
        except:
            logHandler.logging.error(exceptionHandler.PrintException())
            testCaseStatus = ExecutionStatus.FAIL
            reportController.addTestStepResult(testCaseStep, ExecutionStatus.FAIL)
        finally:
            reportController.tcExecutionStatus(testCaseStatus)
            reportController.generateReportSummary(self.executionId)
            
        

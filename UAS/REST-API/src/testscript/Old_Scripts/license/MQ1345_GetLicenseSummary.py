'''
Created on Oct 20, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import license, login, logout
from testdata import license_testdata
from util import reportController
from verificationsript import vlicense
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1345_GetLicenseSummary(unittest.TestCase):
        
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get the license summary information for the specified system")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = license.getLicenseSummaryInformation(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = license.getListOfActualResponseParameter(actualResponse)            
            # sort expected response parameter list            
            license_testdata.summaryResponseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(license_testdata.summaryResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(license_testdata.summaryResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            licenseFromMachine = vlicense.getSummaryLicenseInformationFromMachine()
            print licenseFromMachine
            licenseFromResponse = vlicense.getSummaryLicenseInformationFromResponse(actualResponse)
            print licenseFromResponse
            # Commented for now need to find another way to get license info from db
            #self.assertListEqual(licenseFromMachine, licenseFromResponse, "Failed Data base verification: Expected Response [" + str(licenseFromMachine) + "] and Actual Response [" + str(licenseFromResponse) + "]")
            #reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
              
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)            
'''
Created on Aug 24, 2014

@author: Nikhil S
'''

import unittest
from resourcelibrary import license, login, logout
from testdata import license_testdata
from util import reportController, logHandler
from verificationsript import vlicense
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1902_GetUsedAndLicensedResources(unittest.TestCase):
        
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get the used and licensed for the specified system")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Get the used and licensed for the specified system")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
           
            
            # Step 1: Send request to get all alerts
            actualResponse = license.getUsedAndLicensedResources(token)
            logHandler.logging.info("actual Response" + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = license.getListOfActualResponseParameterUsedAndLiscResource(actualResponse)          
            logHandler.logging.info("actualResponseParams " + str(actualResponseParams))  
            
            # sort expected response parameter list            
            license_testdata.reponseParamUsedAndLicenseRes.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(license_testdata.reponseParamUsedAndLicenseRes, actualResponseParams, "Failed Response verification: Expected response [" + str(license_testdata.reponseParamUsedAndLicenseRes) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            logHandler.logging.info("Verification 1 done")  
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            licenseInfoResponse = vlicense.getUsedAndLiscFromResponse(actualResponse)
            logHandler.logging.info("Values from response: " + str(licenseInfoResponse))
            licenseInfoDatabase = vlicense.getSysDetailsForLiscence(licenseInfoResponse[0])
            logHandler.logging.info("Values from database: " + str(licenseInfoDatabase))
         
            # Commented for now need to find another way to get license info from db
            self.assertListEqual(licenseInfoResponse, licenseInfoDatabase, "Failed Data base verification: Expected Response [" + str(licenseInfoResponse) + "] and Actual Response [" + str(licenseInfoDatabase) + "]")
            logHandler.logging.info("Verification 2 done")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                      
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
               
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            #cleanup
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)            
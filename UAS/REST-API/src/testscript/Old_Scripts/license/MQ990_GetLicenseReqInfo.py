'''
Created on Oct 20, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import license, login, logout
from testdata import license_testdata
from util import reportController
from verificationsript import vlicense
from config.Constants import ExecutionStatus
from config import apiconfig




class MQ990_GetLicenseReqInfo(unittest.TestCase):
        
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get information needed to request a license for the specified system")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = license.getLicenseRequestInformation(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = license.getLicenseReqResponseParameter(actualResponse)
            print actualResponseParams            
            # sort expected response parameter list            
            license_testdata.reqResponseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(license_testdata.reqResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(license_testdata.reqResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            licenseFromMachine = vlicense.getReqLicenseInformationFromMachine()
            print "++++"+str(licenseFromMachine)
            licenseFromResponse = vlicense.getReqLicenseInformationFromResponse(actualResponse)
            print "++++"+str(licenseFromResponse)
            self.assertListEqual(licenseFromMachine, licenseFromResponse, "Failed Data base verification: Expected Response [" + str(licenseFromMachine) + "] and Actual Response [" + str(licenseFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
              
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
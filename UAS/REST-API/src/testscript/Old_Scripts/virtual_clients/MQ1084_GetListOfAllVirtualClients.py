'''
Created on Oct 1, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import login, logout, virtual_clients
from testdata import virtual_clients_testdata
from util import reportController
from verificationsript import vvirtual_clients

from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1084_GetListOfAllVirtualClients(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Gets the list of WIR virtual clients, and of current IR sessions for VMware and Hyper-V, if they exist")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = virtual_clients.getListOfAllVirtualClients(token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = virtual_clients.getListOfActualResponseParameter(actualResponse)
            print actualResponseParams
            # sort expected response parameter list            
            virtual_clients_testdata.responseParameter.sort()
            virtual_clients_testdata.dataNotFoundResponse.sort()
            # Verification 1: Verify get all alerts response parameter 
            if(virtual_clients_testdata.dataNotFoundResponse == actualResponseParams):
                reportController.addTestStepResult("Data not found", ExecutionStatus.PASS)
            else:
                self.assertListEqual(virtual_clients_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(virtual_clients_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response
                        
                clientListFromDB = vvirtual_clients.getVirtualClientListFromDB(actualResponse)
                print clientListFromDB
                
                clientListFromResponse = vvirtual_clients.getVirtualClientListFromResponse(actualResponse)
                print clientListFromResponse
                self.assertListEqual(clientListFromDB, clientListFromResponse, "Failed Data base verification: Expected Response [" + str(clientListFromDB) + "] and Actual Response [" + str(clientListFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
                
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            


'''
Created on Oct 1, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import login, logout, virtual_clients
from testdata import virtual_clients_testdata
from util import reportController
from verificationsript import vvirtual_clients

from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1285_GetVirtualClientCandidates(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Gets the list of WIR virtual clients, and of current IR sessions for VMware and Hyper-V, if they exist")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = virtual_clients.getListOfAllVirtualClientCandidates(token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = virtual_clients.getListOfActualResponseCandidateParameter(actualResponse)
            flag = 'E'
            if 'VirtualID' in actualResponseParams and 'BackupID' in actualResponseParams:
                flag = 'VB'
            elif 'BackupID' in actualResponseParams:
                flag = 'B'
            elif 'VirtualID' in actualResponseParams:
                flag = 'V'    
            print actualResponseParams
            print flag
            # sort expected response parameter list            
            virtual_clients_testdata.responseParameter.sort()
            virtual_clients_testdata.dataNotFoundResponse.sort()
            # Verification 1: Verify get all alerts response parameter 
            if(virtual_clients_testdata.dataNotFoundResponse == actualResponseParams):
                reportController.addTestStepResult("Data not found", ExecutionStatus.PASS)
            else:
                if flag == 'E':
                    responseParameterForVerification1 = virtual_clients_testdata.responseParameter_E
                if flag == 'V':
                    responseParameterForVerification1 = virtual_clients_testdata.responseParameter_V
                if flag == 'B':        
                    responseParameterForVerification1 = virtual_clients_testdata.responseParameter_B
                if flag == 'VB':
                    responseParameterForVerification1 = virtual_clients_testdata.responseParameter_VB
                print flag
                self.assertListEqual(responseParameterForVerification1, actualResponseParams, "Failed Response verification: Expected response [" + str(virtual_clients_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response
                       
                clientListFromDB = vvirtual_clients.getVirtualClientCandidatesFromDB(actualResponse)
                print "database--->" + str(clientListFromDB)
                
                clientListFromResponse = vvirtual_clients.getVirtualClientCandidatesFromResponse(actualResponse)
                print "response--->" + str(clientListFromResponse)
                self.assertListEqual(clientListFromDB, clientListFromResponse, "Failed Data base verification: Expected Response [" + str(clientListFromDB) + "] and Actual Response [" + str(clientListFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
                
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            


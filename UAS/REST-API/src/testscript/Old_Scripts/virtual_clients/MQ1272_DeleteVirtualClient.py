'''
Created on Mar 13, 2015

@author: Nikhil.S
'''

import unittest
from resourcelibrary import login, logout, virtual_clients, clients, inventory
from testdata import virtual_clients_testdata,clients_testdata, inventory_testdata,backups_testdata
from util import reportController, logHandler
from verificationsript import vvirtual_clients,vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1272_DeleteVirtualClient(unittest.TestCase):    
    def __init__(self, exeID):          
        reportController.initializeReport(type(self).__name__, "Delete WIR virtual client")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - WIR virtual client")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            
            osType = clients_testdata.getType("HyperV")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to check clients
            clientAddedFlag=0            
            cid = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(cid))
            if(cid==0):
                clientAddedFlag=1
                newClientCreated = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                cid = clients.getPostClientsRequestResponse(newClientCreated, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(cid))
                if (cid):
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
            
#             result = inventory.waitForInventorySync(token, 120, inventory_testdata.sid)
#             logHandler.logging.info("Result: " + str(result))
#             self.assertNotEqual(result,"Fail","Inventory sync failed")
#             reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
#                             
#             instanceId = vbackups.getInstanceIdFromVm(backups_testdata.hypervInstanceName)
#             logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
#             self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            vid = vvirtual_clients.getVirtualClientIDFromDB(str(cid))
            logHandler.logging.info("Virtual Client ID returned from DB: " + str(vid))
            if(vid == 0):
                NewCreatedVid=virtual_clients.postVirtualClient(token, cid, virtual_clients_testdata.Processors, virtual_clients_testdata.Memory, virtual_clients_testdata.efi, virtual_clients_testdata.Disks, virtual_clients_testdata.Include, virtual_clients_testdata.Exclude)
                reportController.addTestStepResult("Send API Request for Create Virtual client", ExecutionStatus.PASS)
                
                vid = vvirtual_clients.getVirtualClientIDFromDB(str(cid))
                logHandler.logging.info("Virtual Client ID returned from DB: " + str(vid))
                
                # Verification 1: verify the response parameter
                if (vid):
                    reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed response verification", ExecutionStatus.FAIL)
                
            actualResponse = virtual_clients.deleteVirtualClient(token, vid)
            reportController.addTestStepResult("Send API Request for Delete Virtual client", ExecutionStatus.PASS)
            
            #Verification 2: Fetch added virtualclient from db
            #dbResult = vvirtual_clients.getVirtualClientStateFromDB(str(vid))
            dbResult = vvirtual_clients.getVirtualClientIDFromDB(str(cid))
            logHandler.logging.info("DB Result: " + str(dbResult))
            #if(dbResult=='destroy'):
            if(dbResult):
                reportController.addTestStepResult("Delete Virtual Client  - Database verification failed", ExecutionStatus.FAIL)
            else:
                reportController.addTestStepResult("Delete Virtual Client  - Database verification passed", ExecutionStatus.PASS)    
            
            #cleanup
            if(clientAddedFlag):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)

            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
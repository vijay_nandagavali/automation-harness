'''
Created on Mar 11, 2015

@author: Nikhil.S
'''

import unittest
from resourcelibrary import login, logout, virtual_clients, clients
from testdata import virtual_clients_testdata,clients_testdata
from util import reportController, logHandler
from verificationsript import vvirtual_clients,vclients
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1270_GetVirtualClientByID(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Gets WIR virtual clients having specific ID")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Specific Virtual Client")
        self.executionId = exeID       
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            
            osType = clients_testdata.getType("HyperV")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            cid = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(cid))
            if(cid==0):
                newClientCreated = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                cid = clients.getPostClientsRequestResponse(newClientCreated, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(cid))
            
            vid = vvirtual_clients.getVirtualClientIDFromDB(str(cid))
            logHandler.logging.info("Virtual Client ID returned from DB: " + str(vid))
            if(vid== 0):
                newVirtualClientCreated = virtual_clients.postVirtualClient(token, cid, virtual_clients_testdata.Processors, virtual_clients_testdata.Memory, virtual_clients_testdata.efi, virtual_clients_testdata.Disks, virtual_clients_testdata.Include, virtual_clients_testdata.Exclude)
                #vid = virtual_clients.getPostRequestResponse(newVirtualClientCreated)    
            vid = vvirtual_clients.getVirtualClientIDFromDB(str(cid)) #this line will be removed once the defect is fixed UNIBP-2371
            logHandler.logging.info("Virtual Client ID returned from DB: " + str(vid))
            if(vid <> 0):
                actualResponse = virtual_clients.getDetailsOfVirtualClientByID(token,vid)
                if (len(actualResponse)==0):
                    reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                    # Get actual response parameter from response
                    actualResponseParams = virtual_clients.getListOfActualResponseParameterVirtualClientByID(actualResponse)
                    logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                    # sort expected response parameter list            
                    virtual_clients_testdata.responseParameter_VirtualClientByID.sort()
                    virtual_clients_testdata.dataNotFoundResponse.sort()
                    # Verification 1: Verify get all alerts response parameter 
                    if(virtual_clients_testdata.dataNotFoundResponse == actualResponseParams):
                        reportController.addTestStepResult("Data not found", ExecutionStatus.PASS)
                    else:
                        self.assertListEqual(virtual_clients_testdata.responseParameter_VirtualClientByID, actualResponseParams, "Failed Response verification: Expected response [" + str(virtual_clients_testdata.responseParameter_VirtualClientByID) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                        reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                        # Verification 2: Verify get all alerts API response
                        clientListFromDB = vvirtual_clients.getVirtualClientDetailsByIDFromDB(actualResponse)
                        logHandler.logging.info("Data returned from DB: " + str(clientListFromDB))
                        
                        clientListFromResponse = vvirtual_clients.getVirtualClientDetailsByIDFromResponse(actualResponse)
                        logHandler.logging.info("Data from response: " + str(clientListFromResponse))
                        
                        self.assertListEqual(clientListFromDB, clientListFromResponse, "Failed Data base verification: Expected Response [" + str(clientListFromDB) + "] and Actual Response [" + str(clientListFromResponse) + "]")
                        reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                else:
                    reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)
            else:
                raise Exception("No virtual client available")
            reportController.tcExecutionStatus(ExecutionStatus.PASS)

        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
        
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)

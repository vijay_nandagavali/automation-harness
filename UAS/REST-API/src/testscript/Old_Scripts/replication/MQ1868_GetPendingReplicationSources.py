'''
Created on August 7, 2015

@author: Nikhil S
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, systems, logout, replication 
from testdata import systems_testdata, replication_testdata
from verificationsript import vreplication
import time
from time import sleep

class MQ1868_GetPendingReplicationSources(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Returns a list of pending replication sources that are awaiting target acceptance of a request to configure replication.")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tList of pending replication sources.")
        self.executionId = exeID
    
    def runTest(self):
        try:
            token = login.getAuthenticationToken_Source(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            token_Tar = login.getAuthenticationToken_Target(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token for Target: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
                        
            #prerequsite
            replication.deletePendingTargetSource()
            PreReq_Target = systems.postMakeTarget(token_Tar, systems_testdata.network, systems_testdata.mask, systems_testdata.port)
            logHandler.logging.info("Prerequisite to Enable this appliance as a Backup Copy Target : " + str(PreReq_Target))
            addTarget = replication.postReplicationTarget(token, replication_testdata.tar_Type_Appliance, replication_testdata.tar_Name, replication_testdata.tar_IP, replication_testdata.tar_insecure)
            logHandler.logging.info("addTarget : " + str(addTarget))
            logHandler.logging.info("Prerequisite to add a Backup Copy Target : " + str(PreReq_Target))
            
            #Send API Request
            actualResponse = replication.getPendingReplicationSources(token_Tar)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            reportController.addTestStepResult("Send API request to get Pending Sources", ExecutionStatus.PASS)
            # Get actual response parameter from response - verification1
            actualResponseParams = replication.getPendingRepSrcParam(actualResponse)
            print "actualResponseParams    " + str(actualResponseParams)
            logHandler.logging.info("actual Response Params: " + str(actualResponseParams))
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(replication_testdata.responseParameterPendingRepSrc, actualResponseParams, "Failed Response verification: Expected response [" + str(replication_testdata.responseParameterPendingRepSrc) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        

            #Verification 2: DB verification
            pendingSrcDetials_Response = vreplication.getPendingRepSrcFromResponse(actualResponse)
            logHandler.logging.info("pendingSrcDetials_Response: " + str(pendingSrcDetials_Response))
            pendingSrcDetials_DB = vreplication.getPendingRepSrcFromDB()
            logHandler.logging.info("pendingSrcDetials_DB: " + str(pendingSrcDetials_DB))
            self.assertListEqual(pendingSrcDetials_Response, pendingSrcDetials_DB,  "Failed Data base verification: Expected Response [" + str(pendingSrcDetials_DB) + "] and Actual Response [" + str(pendingSrcDetials_Response) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
                                   
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup - removing of target from API to be added later, this is temp cleanup 
            replication.deletePendingTargetSource()
            logHandler.logging.info("Cleanup done ! ")
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
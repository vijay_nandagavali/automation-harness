'''
Created on August 5, 2015

@author: Nikhil S
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, systems, logout, replication 
from testdata import systems_testdata, replication_testdata
from verificationsript import vreplication
import time
from time import sleep

class MQ1813_PostConfigureReplicationTarget_Appliance(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Replication be configured to a specified target for Appliance.")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tReplication be configured to a specified target for Appliance.")
        self.executionId = exeID
    
    def runTest(self):
        try:
            token = login.getAuthenticationToken_Source(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            token_Tar = login.getAuthenticationToken_Target(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token for Target: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
                        
            #prerequsite
            PreReq_Target = systems.postMakeTarget(token_Tar, systems_testdata.network, systems_testdata.mask, systems_testdata.port)
            logHandler.logging.info("Prerequisite to Enable this appliance as a Backup Copy Target : " + str(PreReq_Target))
    
            #Send API Request
            actualResponse = replication.postReplicationTarget(token, replication_testdata.tar_Type_Appliance, replication_testdata.tar_Name, replication_testdata.tar_IP, replication_testdata.tar_insecure)
            logHandler.logging.info("Actual Response : " + str(actualResponse))
            reportController.addTestStepResult("Send API request for Post Direct storage" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response - verification1
            actualResponseParams = systems.postResponseParser(actualResponse)
            print actualResponseParams
            logHandler.logging.info("actual Response Params: " + str(actualResponseParams))
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)

            #DB verification
            tarDetails_DB = vreplication.getRepTargetFromDB(replication_testdata.tar_Name)
            logHandler.logging.info("Target added:   " + str(tarDetails_DB))
            if(tarDetails_DB):
                reportController.addTestStepResult("Target Add - Database verification passed", ExecutionStatus.PASS)
                logHandler.logging.info("Target added successfully!")
            else:
                reportController.addTestStepResult("Target Add - Database verification failed", ExecutionStatus.FAIL)   
                logHandler.logging.info("Target not added!")
                raise Exception("Database verification failed")
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup - removing of target from API to be added later, this is temp cleanup 
            replication.deletetarget_TempCleanup(replication_testdata.tar_Name)

            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
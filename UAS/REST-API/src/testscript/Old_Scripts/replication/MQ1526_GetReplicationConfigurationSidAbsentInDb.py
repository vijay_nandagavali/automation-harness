'''
Created on Jun 10, 2015

@author: Nikhil S
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler, masterFileOperation
from testdata import replication_testdata
from resourcelibrary import login, replication, logout, jobs
from verificationsript import vreplication
import util

class MQ1526_GetReplicationConfigurationSidAbsentInDb(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get Replication Configuration with SId not present in DB")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Get Replication Configuration with SId not present in DB")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            print "login"
            
            #Send GET API request
            actualResponse = replication.getReplicationConfig(token,replication_testdata.sid_invalid)
            print "actualResponse  " + str(actualResponse)
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = jobs.getListOfJobParameterInJson(actualResponse)
            print actualResponseParams
            
            print "Inside Verification 1"
            # Verification 1: Verify get all replication strategy response parameter        
            self.assertListEqual(replication_testdata.responseParmeterReplicationConfig, actualResponseParams, "Failed Response verification: Expected response [" + str(replication_testdata.responseParmeterReplicationConfig) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            # Verification 2: Verify get all replication config from master.ini     
            print "Inside Verification 2"        
#             repConfigFromMasterFile = []
#             maxCon = util.masterFileOperation.parse_config(replication_testdata.MaxConcurrentParameterName)
#             print maxCon
#             repConfigFromMasterFile.append(maxCon)
#             repMailTo = util.masterFileOperation.parse_config(replication_testdata.ReportMailToParameterName)
#             print repMailTo
#             repConfigFromMasterFile.append(repMailTo)
#             repTime = util.masterFileOperation.parse_config(replication_testdata.ReportTimeParameterName)
#             print repTime
#             repConfigFromMasterFile.append(repTime)
#             repConfigFromMasterFile.sort()
#             
            repConfigFromResponse = vreplication.getRepConfigFromResponse(actualResponse)    
            print repConfigFromResponse
            
            self.assertListEqual(replication_testdata.responseRepConfig_Invalid, repConfigFromResponse, "Failed Data base verification: Expected Response [" + str(replication_testdata.responseRepConfig_InvalidSid) + "] and Actual Response [" + str(repConfigFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)   
            
                        
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
        
'''
Created on Jun 9, 2015

@author: Nikhil S
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler, masterFileOperation
from testdata import replication_testdata
from resourcelibrary import login, replication, logout
from verificationsript import vreplication
import util

class MQ1551_GetReplicationConfigUsingSidAsString(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get Replication Configuration using sid as string")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Get Replication Configuration using sid as string")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            print "login"
            
            #Send PUT API request
            actualResponse = replication.getReplicationConfig(token,replication_testdata.sid_string)
            print "actualResponse  " + str(actualResponse)
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            repConfigFromResponse = vreplication.getRepConfigFromResponse(actualResponse)    
            
            
            print repConfigFromResponse
            if(str(replication_testdata.responseRepConfig_InvalidSid)==str(repConfigFromResponse)):
                print "pass"
                reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
                reportController.tcExecutionStatus(ExecutionStatus.PASS)
            else:
                print "fail"    
                reportController.addTestStepResult("Response verification", ExecutionStatus.FAIL)
                reportController.tcExecutionStatus(ExecutionStatus.FAIL)
                
            
                       
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.PASS)
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
        
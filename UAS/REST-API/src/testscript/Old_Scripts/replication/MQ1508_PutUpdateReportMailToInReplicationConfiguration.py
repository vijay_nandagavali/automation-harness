'''
Created on Jun 9, 2015

@author: Nikhil S
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler, masterFileOperation
from testdata import replication_testdata
from resourcelibrary import login, replication, logout
from verificationsript import vpolicy
import util

class MQ1508_PutUpdateReportMailToInReplicationConfiguration(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Modify existing Report_Mail_To Configuration")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Modify existing Report_Mail_To Configuration")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            print "login"
            
            #Send PUT API request
            actualResponse = replication.putUpdateReplicationConfig(token,reportMailTo=replication_testdata.report_Mail_to_Update)
            print "actualResponse  " + str(actualResponse)
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            #Verify response
            responseResult = vpolicy.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = []
            repMailTo = util.masterFileOperation.parse_config(replication_testdata.ReportMailToParameterName)
            print repMailTo
            dbResult.append(repMailTo)
            dbResult.sort()
            
            
            expecteddatalist =[replication_testdata.report_Mail_to_Update]
            expecteddatalist.sort()
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertListEqual(dbResult, expecteddatalist, "Failed Data base verification: Expected Response [" + str(expecteddatalist) + "] and Actual Response [" + str(dbResult) + "]")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            print "DP verification done"
            
           
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
        
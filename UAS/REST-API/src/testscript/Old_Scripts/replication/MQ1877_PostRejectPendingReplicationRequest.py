'''
Created on August 7, 2015

@author: Nikhil S
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, systems, logout, replication 
from testdata import systems_testdata, replication_testdata
from verificationsript import vreplication
import time
from time import sleep

class MQ1877_PostRejectPendingReplicationRequest(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Reject the pending replication request made by an incoming source. ")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tRejects pending replication requests.")
        self.executionId = exeID
    
    def runTest(self):
        try:
            requestID =0
            token = login.getAuthenticationToken_Source(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            token_Tar = login.getAuthenticationToken_Target(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token for Target: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
                        
            #prerequsite
            replication.deletePendingTargetSource()
            replication.deleteRejectedTarget()
            PreReq_Target = systems.postMakeTarget(token_Tar, systems_testdata.network, systems_testdata.mask, systems_testdata.port)
            logHandler.logging.info("Prerequisite to Enable this appliance as a Backup Copy Target : " + str(PreReq_Target))
            addTarget = replication.postReplicationTarget(token, replication_testdata.tar_Type_Appliance, replication_testdata.tar_Name, replication_testdata.tar_IP, replication_testdata.tar_insecure)
            logHandler.logging.info("addTarget : " + str(addTarget))
            logHandler.logging.info("Prerequisite to add a Backup Copy Target : " + str(PreReq_Target))
             
            requestID = replication.getRequestIDfromHostName_DB(replication_testdata.tar_Name)
            logHandler.logging.info("Request ID : " + str(requestID))
            
            #Send API Request
            actualResponse = replication.postRejectPendingRepReq(token_Tar, requestID)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            reportController.addTestStepResult("Send API request to reject pending replication request", ExecutionStatus.PASS)
            
            # Get actual response parameter from response - verification1
            actualResponseParams = replication.postResponseParser(actualResponse)
            logHandler.logging.info("actual Response Params: " + str(actualResponseParams))
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)

            #Verification 2: DB verification
            status_DB = vreplication.getRequestStatusFromHostName_DB(requestID)
            logHandler.logging.info("Pending replication request status:   " + str(status_DB))
            if(str(status_DB) == 'rejected'):
                reportController.addTestStepResult("Pending replication request 'rejected' - Database verification passed", ExecutionStatus.PASS)
                logHandler.logging.info("Pending replication request rejected!")
            else:
                reportController.addTestStepResult("Pending replication request 'rejected' - Database verification failed", ExecutionStatus.FAIL)   
                logHandler.logging.info("Pending replication request not rejected!")
                raise Exception("Database verification failed")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)                                   
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup - removing of target from API to be added later, this is temp cleanup 
            replication.deletePendingTargetSource()
            replication.deleteRejectedTarget()
            logHandler.logging.info("Cleanup done ! ")
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
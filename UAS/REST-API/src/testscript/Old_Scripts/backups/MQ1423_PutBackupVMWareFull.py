'''
Created on May 22, 2015

@author: rahula
'''

import unittest
from resourcelibrary import clients, backups,login, logout, jobs, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler, jsonHandler
from verificationsript import vbackups
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from time import sleep

class MQ1423_PutBackupVMWareFull(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Start a backup for the specified VMware instance")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Backup VMWare Instance")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            #Test data used in the script
            vmName=clients_testdata.getmachineName("VM1")
            vmIp=clients_testdata.getmachineIP("VM1")
            instanceName = clients_testdata.getShareName("VM1")
            vmType = clients_testdata.getType("VM1")
            vmUsername = clients_testdata.getUserName("VM1")
            vmPassword = clients_testdata.getPassword("VM1")
            esxName = clients_testdata.getmachineName("ESX-Server")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            uuid = vclients.getSpecificEsxServerFromDB(esxName)
            logHandler.logging.info("UUID returned from DB: " + str(uuid))
            
            if(uuid==0):
                addedClientFlag =1
                actualResponse = clients.postVMClient(token,clients_testdata.sid, vmName, vmType, 
                                                clients_testdata.vm_priority, clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, clients_testdata.vm_use_ssl, 
                                                clients_testdata.vm_defaultschedule, clients_testdata.vm_port, vmIp, 
                                                clients_testdata.vm_existing_credential, vmUsername, vmPassword, 
                                                clients_testdata.vm_domain)
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS) 
                  
                responseResult = clients.getVMPostClientRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))                            
                uuid = vclients.getSpecificEsxServerFromDB(clients_testdata.vm_esx_server_name)
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromVmVMWare(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            # Step 3: Send the PUT request
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            print  "Actual Response - " + str(actualResponse)           
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - ensure that Job Id is returned as -1
            responseResult = backups.getPutRequestResponse(actualResponse)
            print "responseResult: "+str(responseResult)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID from Response: " + str(strJobId))
            print "Job ID from Response: " + str(strJobId)
            if(strJobId == -1):
                raise Exception("Incremental backup not triggered")
            else:    
                reportController.addTestStepResult("Backup job not triggered,Job Id returned is -1 as expected.", ExecutionStatus.PASS)
            
            #Verification 2: Verify the error message returned in the response object
            #print responseResult["data"][0]["message"]
            actualResponseParams=jsonHandler.getListOfMediaParameterInJson(actualResponse, True)
            logHandler.logging.info("actual response parameters " + str(actualResponseParams))
            print "actual response parameters " + str(actualResponseParams)                     
            self.assertListEqual(backups_testdata.vmwareInstanceBackupResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(backups_testdata.vmwareInstanceBackupResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
        
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:            
            #cleanup
            if(addedClientFlag == 1 or result == 'Fail'):
                clients.deleteVMClient(token, uuid)
                logHandler.logging.info("Client already existing is deleted..")
                reportController.addTestStepResult("Delete existing client", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on May 22, 2015

@author: garima.g
'''

import unittest
from resourcelibrary import clients, backups,login, logout, jobs, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vbackups
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from time import sleep

class MQ1417_PutBackupVMWareIncremental(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Start a incremental backup for the specified VMware instance")    
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Incremental Backup VMware Instance")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Test data used in the script
            vmName=clients_testdata.getmachineName("VM1")
            vmIp=clients_testdata.getmachineIP("VM1")
            instanceName = clients_testdata.getShareName("VM1")
            vmType = clients_testdata.getType("VM1")
            vmUsername = clients_testdata.getUserName("VM1")
            vmPassword = clients_testdata.getPassword("VM1")
            esxName = clients_testdata.getmachineName("ESX-Server")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: check if vmware client exists
            uuid = vclients.getSpecificEsxServerFromDB(esxName)
            logHandler.logging.info("UUID returned from DB: " + str(uuid))
            
            #Step 2: if client does not exists add a client
            if(uuid==0):
                actualResponse = clients.postVMClient(token,clients_testdata.sid, vmName, vmType, 
                                                clients_testdata.vm_priority, clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, clients_testdata.vm_use_ssl, 
                                                clients_testdata.vm_defaultschedule, clients_testdata.vm_port, vmIp, 
                                                clients_testdata.vm_existing_credential, vmUsername, vmPassword, 
                                                clients_testdata.vm_domain)
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS) 
                  
                responseResult = clients.getVMPostClientRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))
                addedClientFlag =1
                uuid = vclients.getSpecificEsxServerFromDB(esxName)
                logHandler.logging.info("UUID returned from DB: " + str(uuid))
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                     
            # Step 3: Get instance Id from database
            instanceId = vbackups.getInstanceIdFromVmVMWare(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            fullBackups = vbackups.getFullVMWareBackupsFromDB(instanceId)
            logHandler.logging.info("HyperV Full backups returned from DB: " + str(fullBackups))
            
            if(len(fullBackups) == 0):
                #Step 4: Put request for full backup 
                actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
                logHandler.logging.info("Actual Response - " + str(actualResponse))                        
                reportController.addTestStepResult("Send API request for full backup" , ExecutionStatus.PASS)
            
                # Step 5: verify the response parameter - if the full backup job is created
                responseResult = backups.getPutRequestResponse(actualResponse)
                strJobId = '-1'
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID from Response: " + str(strJobId))
                if(strJobId == '-1'):
                    raise Exception("Failed to trigger backup")
            
                # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
                dbResult = vbackups.getJobByIdFromDB(strJobId)
                logHandler.logging.info("Job ID returned from DB: " + str(dbResult))
            
                if(dbResult == "Pass"):
                    logHandler.logging.info("Database verification successful..")
                    reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
                else:
                    raise Exception("Failed to find Job with Job ID" + str(strJobId) + " in DB")
            
            #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                if(backupJobStatus!="Successful"):
                    logHandler.logging.error("Backup not successful")
                    reportController.addTestStepResult("Backup success", ExecutionStatus.FAIL)
                else:
                    logHandler.logging.info("Backup successful")
                    reportController.addTestStepResult("Backup Success" , ExecutionStatus.PASS)
                
                logHandler.logging.info("Thread sleep for 60 sec..")
                sleep(60)
                    
            #Calling existing HyperV function for VMWare also
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,"Incremental",backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            reportController.addTestStepResult("Send API request for Incremental backup" , ExecutionStatus.PASS)                     
            
            #Response verification
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID from Response: " + str(strJobId))
            if(strJobId == '-1'):
                raise Exception("Failed to trigger incremental backup")
            
            # Get actual response parameter from response
            actualResponseParams = backups.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            backups_testdata.vmwareInstanceBackupResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(backups_testdata.vmwareInstanceBackupResponseParameter))
            self.assertListEqual(backups_testdata.vmwareInstanceBackupResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(backups_testdata.vmwareInstanceBackupResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vbackups.getJobByIdFromDB(strJobId)
            logHandler.logging.info("Job ID returned from DB: " + str(dbResult))
            
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful..")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                raise Exception("Failed to find Job with Job ID" + str(strJobId) + " in DB")
            
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            self.assertEqual(backupJobStatus, "Successful", "Incremental Backup not successful")
            logHandler.logging.info("Incremental backup successful...")
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #cleanup
            if(addedClientFlag == 1 or result == 'Fail'):
                clients.deleteVMClient(token, uuid)
                logHandler.logging.info("Client already existing is deleted..")
                reportController.addTestStepResult("Delete existing client", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
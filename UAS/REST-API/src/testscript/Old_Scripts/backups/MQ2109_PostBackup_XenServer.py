'''
Created on Nov 4, 2015

@author: Prateek
'''

import unittest
from resourcelibrary import clients, backups,login, logout, jobs, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vbackups, vclients
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2109_PostBackup_XenServer(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Post Backup for Xen Server")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Backup for Xen Server")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Check if the client is present
            clientId = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            
            if(clientId==0):
                addedClientFlag =1
                #Step 2: Add Client
                actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver, 
                                                       clients_testdata.system_xenserver, clients_testdata.ip_xenserver, 
                                                       clients_testdata.username_xenserver, clients_testdata.password_xenserver, 
                                                       clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, 
                                                       clients_testdata.is_synchable_xenserver, clients_testdata.is_auth_enabled_xenserver)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                
                logHandler.logging.info("Client ID returned from response: " + str(clientId))
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromVmVMWare(backups_testdata.xenserverInstanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
                
            # Step 3: Send the PUT request to create a backup                
            actualResponse = backups.putXenServerInstanceBackup(token,instanceId,backups_testdata.xenserverInstanceName,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.xenserverVerify,backups_testdata.sid)
            reportController.addTestStepResult("Send API request for Put HyperV instance backup" , ExecutionStatus.PASS)
            
            responseResult = backups.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response result: " + str(responseResult))
            
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"]
            logHandler.logging.info("Job ID returned from response: " + str(strJobId))
            
            if(strJobId == '-1'):
                raise Exception("Failed to trigger backup")
            
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus!="Successful"):
                logHandler.logging.error("Backup not successful..")
                reportController.addTestStepResult("Backup", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup" , ExecutionStatus.PASS) 
            
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup ID returned from DB: " + str(bid))
            
            # Step 3: Send the POST request
            actualResponse = backups.postBackup(token, int(clientId))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                        
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            # Get actual response parameter from response
            actualResponseParams = backups.getPostRequestResponse(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            if(len(actualResponseParams) > 1):
                backups_testdata.backupResponseParameter.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(backups_testdata.backupResponseParameter))
                
                self.assertListEqual(backups_testdata.xenBackupResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(backups_testdata.backupResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
                # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID        
                restorableBackupsFromDB = vbackups.getFilteredBackupsFromDB(clientId)
                logHandler.logging.info("Restorable backups returned from DB: " + str(restorableBackupsFromDB))
                
                restorableBackupsFromResponse = vbackups.getFilteredBackupsFromResponse(actualResponse)
                logHandler.logging.info("Restorable backups returned from response: " + str(restorableBackupsFromResponse))
                
                self.assertListEqual(restorableBackupsFromDB, restorableBackupsFromResponse, "Failed Data base verification: Expected Response [" + str(restorableBackupsFromDB) + "] and Actual Response [" + str(restorableBackupsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Backup data not found", ExecutionStatus.PASS)
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #cleanup
            if(addedClientFlag == 1 or result == 'Fail'):
                clients.deleteClients(token, clientId) 
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)

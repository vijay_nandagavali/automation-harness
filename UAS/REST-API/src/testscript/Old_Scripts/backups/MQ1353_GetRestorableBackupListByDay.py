'''
Created on Feb 16, 2015

@author: Nikhil.S
'''
import unittest
from resourcelibrary import login, logout, backups
from testdata import backups_testdata
from util import reportController, logHandler
from verificationsript import vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1353_GetRestorableBackupListByDay(unittest.TestCase):
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get the list of Restorable backups list by Day")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Restorable backups list by day")
        self.executionId = exeID
        
    def runTest(self):                
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = backups.getRestorableBackupListByDay(token) 
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            # Get actual response parameter from response
            actualResponseParams = backups.getListOfActualBackupResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))           
            
            # sort expected response parameter list
            respLength = len(actualResponseParams)
            logHandler.logging.info("Length of Response: " + str(respLength))
            expectedResponseParams = []
            if(respLength > 1):
                expectedResponseParams = backups_testdata.responseParameter
            else:
                expectedResponseParams = ['catalog'] 
            
            expectedResponseParams.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(expectedResponseParams))
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(expectedResponseParams, actualResponseParams, "Failed Response verification: Expected response [" + str(expectedResponseParams) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            # Verification 2: Verify get all alerts API response        
            restorableBackupsFromDB = vbackups.getRestorableBackupsFromDB(actualResponse)
            logHandler.logging.info("Restorable backups returned from DB: " + str(restorableBackupsFromDB))
            
            restorableBackupsFromResponse = vbackups.getRestorableBackupsFromResponse(actualResponse)
            logHandler.logging.info("Restorable backups from response: " + str(restorableBackupsFromResponse))
            
            self.assertListEqual(restorableBackupsFromDB, restorableBackupsFromResponse, "Failed Data base verification: Expected Response [" + str(restorableBackupsFromDB) + "] and Actual Response [" + str(restorableBackupsFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
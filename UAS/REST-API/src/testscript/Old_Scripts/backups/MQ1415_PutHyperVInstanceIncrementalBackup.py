'''
Created on May 21, 2015

@author: rahula
'''
import unittest
from resourcelibrary import clients, backups,login, logout, jobs, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vbackups
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1415_PutHyperVInstanceIncrementalBackup(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Incremental backup fails for a HyperV instance with no full backup")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Incremental backup fails for a HyperV instance with no full backup")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(clientId))
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromVm(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            fullBackups = vbackups.getFullHyperVBackupsFromDB(clientId, instanceId)
            logHandler.logging.info("Full backups returned from DB: " + str(fullBackups))
            if(len(fullBackups) == 0):                
                # Step 3: Send the PUT request
                actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.incrementalBackupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
                logHandler.logging.info("Actual Response - " + str(actualResponse))            
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                
                # Verification 1: verify the response parameter - ensure that Job Id is returned as -1
                responseResult = backups.getPutRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))
                strJobId = '-1'
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID from Response: " + str(strJobId))
                if(strJobId == -1):
                    reportController.addTestStepResult("Backup job not triggered,Job Id returned is -1 as expected.", ExecutionStatus.PASS)
                else:    
                    raise Exception("Full backup was not taken, still triggered an incremental backup")
                
                #Verification 2: Verify the error message returned in the response object
                strErrorMessage = responseResult["data"][0]["message"]
                logHandler.logging.info("Error message returned in the response object " + str(strErrorMessage))                     
                if(strErrorMessage == backups_testdata.incrementalFailureMessage):
                    reportController.addTestStepResult("Verified error message in the response object -" + strErrorMessage,ExecutionStatus.PASS)
                else:
                    reportController.addTestStepResult("Failed error message verification. Error message returned - " + strErrorMessage, ExecutionStatus.FAIL)    
            else:
                logHandler.logging.info("Full backups already present for this client instance")
                reportController.addTestStepResult("Full backups already available", ExecutionStatus.FAIL)
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #cleanup
            if(addedClientFlag == 1 or result == 'Fail'):
                clients.deleteClients(token, clientId) 
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
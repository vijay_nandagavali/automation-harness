'''
Created on Feb 23, 2015

@author: Rahul A.
'''

import unittest, time
from resourcelibrary import clients, backups,login, logout, jobs, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vbackups, vclients
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1274_PostSearchBackups(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "POST method, Starts a search of backups for a protected asset")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Search backups")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Check if the client is present
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                #Step 2: Add Client
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                logHandler.logging.info("Actual Response - " + str(actualResponse))  
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(clientId))
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
                
            # Step 3: Send the PUT request to create a backup
            
            
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            reportController.addTestStepResult("Send API request for Put HyperV instance backup" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            responseResult = backups.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response result: " + str(responseResult))
            
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"]
            logHandler.logging.info("Job ID returned from response: " + str(strJobId))
            
            if(strJobId=='-1'):
                raise Exception("Failed to trigger backup")
                
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus!="Successful"):
                logHandler.logging.error("Backup not successful..")
                reportController.addTestStepResult("Backup", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup" , ExecutionStatus.PASS) 
            
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup ID returned from DB: " + str(bid))
            
            # Step 3: Send the POST request
            #backups_testdata.postResponseParameter.sort()
            actualResponse = backups.postBackupSearch(token, bid, backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request for Post backup search" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            # Get actual response parameter from response
            actualResponseParams = backups.getPostSearchRequestResponse(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            if(len(actualResponseParams) > 1):
                backups_testdata.postBackupSearchResponseParameter.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(backups_testdata.postBackupSearchResponseParameter))
                self.assertListEqual(backups_testdata.postBackupSearchResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(backups_testdata.postBackupSearchResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("No data found...")
                reportController.addTestStepResult("No test data found", ExecutionStatus.PASS)    
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #cleanup
            if(addedClientFlag == 1 or result == 'Fail'):
                clients.deleteClients(token, clientId)
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
                        
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
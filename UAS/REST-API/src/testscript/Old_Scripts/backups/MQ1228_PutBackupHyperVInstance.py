'''
Created on Feb 23, 2015

@author: Sayali K
'''

import unittest
from resourcelibrary import clients, backups,login, logout, jobs, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vbackups
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1228_PutBackupHyperVInstance(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Start a backup for the specified Hyper-V instance")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Backup HyperV Instance")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(clientId))
           
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            # Step 3: Send the PUT request 
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID from Response: " + str(strJobId))
            if(strJobId == '-1'):
                raise Exception("Failed to trigger backup")
            
            # Get actual response parameter from response
            actualResponseParams = backups.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            backups_testdata.instanceBackupResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(backups_testdata.instanceBackupResponseParameter))
            self.assertListEqual(backups_testdata.instanceBackupResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(backups_testdata.instanceBackupResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
            dbResult = vbackups.getJobByIdFromDB(strJobId)
            logHandler.logging.info("Job ID returned from DB: " + str(dbResult))
            
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful..")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                raise Exception("Failed to find Job with Job ID" + str(strJobId) + " in DB")
            
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus!="Successful"):
                logHandler.logging.error("Backup not successful")
                reportController.addTestStepResult("Backup success", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup Success" , ExecutionStatus.PASS)
                
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #cleanup
            if(addedClientFlag == 1 or result == 'Fail'):
                clients.deleteClients(token, clientId) 
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
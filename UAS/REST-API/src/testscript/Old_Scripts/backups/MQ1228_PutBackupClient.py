'''
Created on Feb 23, 2015

@author: Rahul A.
'''

import unittest

from resourcelibrary import clients, backups,login, logout,jobs
from testdata import clients_testdata, backups_testdata
from util import reportController, logHandler
from verificationsript import vbackups
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from testdata.clients_testdata import keyForId

class MQ1228_PutBackupClient(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Put backup for a client")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Backup client")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            
            # Step 3: Send the PUT request
            actualResponse = backups.putBackupClient(token, int(clientId))
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId))
            
            # Get actual response parameter from response
            actualResponseParams = backups.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            backups_testdata.linuxBackupResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(backups_testdata.backupResponseParameter))
            
            self.assertListEqual(backups_testdata.backupResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(backups_testdata.backupResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
            dbResult = vbackups.getJobByIdFromDB(strJobId)
            logHandler.logging.info("DB Result: " + str(dbResult))
            
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Database verification failed")
                reportController.addTestStepResult("Failed to find Job with Job ID" + str(strJobId), ExecutionStatus.FAIL)    
            
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus!="Successful"):
                logHandler.logging.info("Backup not successful")
                reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS) 
            
            #cleanup
            if(addedClientFlag):
                clients.deleteClients(token, clientId)
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
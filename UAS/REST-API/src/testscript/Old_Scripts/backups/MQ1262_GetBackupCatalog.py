'''
Created on Mar 5, 2015

@author: abhijeet.m
'''
import unittest
from config.Constants import ExecutionStatus
from testdata import backups_testdata
from util import reportController, logHandler
from verificationsript import vbackups
from resourcelibrary import login, logout, backups
from config import apiconfig

class MQ1262_GetBackupCatalog(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "GET backup catalog")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Backup Catalog")
        self.executionId = exeID         
    
    def runTest(self):
        try:                     
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                                   
            # Send Get backup catalog request backup/catalog
            responseParams = backups.getBackupCatalogResponseParameter(token)
            logHandler.logging.info("Actual Response Parameters - " + str(responseParams))
            
            backups_testdata.backupsCatalogResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(backups_testdata.backupsCatalogResponseParameter))           
                        
            if(len(responseParams) > 1):
                # Verify API response
                self.assertListEqual(backups_testdata.backupsCatalogResponseParameter, responseParams, "Failed Response verification: Expected response [" + str(backups_testdata.backupsCatalogResponseParameter) + "] and Actual parameter [" + str(responseParams) + "]")            
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
                # Verify with DB
                clientIDs = vbackups.getClientIDsFromBackupCatalogResponse(token)
                logHandler.logging.info("Client IDs from response: " + str(clientIDs))
                
                backupIDs = vbackups.getBackupIDsFromBackupCatalogResponse(token)
                logHandler.logging.info("Backup IDs from response: " + str(backupIDs))
                
                backupIDsFromDB = vbackups.getBackupIDsFromDBForClients(clientIDs)
                logHandler.logging.info("Backup IDs from DB: " + str(backupIDs))
                
                for backupID in backupIDs:
                    self.assertIn(backupID, backupIDsFromDB, backupID + " is not present in DB")
                    reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup catalog data not found")
                reportController.addTestStepResult("Backup Catalog data not found", ExecutionStatus.PASS) 
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
            
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
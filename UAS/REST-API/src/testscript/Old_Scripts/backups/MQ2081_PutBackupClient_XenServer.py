'''
Created on Oct 22, 2015

@author: Prateek
'''
import unittest

from resourcelibrary import clients, backups, login, logout, jobs, inventory
from testdata import clients_testdata, backups_testdata
from util import reportController, logHandler, jsonHandler
from verificationsript import vbackups
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from testdata.clients_testdata import keyForId
from time import sleep

class MQ2081_PutBackupClient_XenServer(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Put backup for a Xen Server client")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Backup Xen Server client")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver, 
                                                       clients_testdata.system_xenserver, clients_testdata.ip_xenserver, 
                                                       clients_testdata.username_xenserver, clients_testdata.password_xenserver, 
                                                       clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, 
                                                       clients_testdata.is_synchable_xenserver, clients_testdata.is_auth_enabled_xenserver)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                sleep(60);
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            
            # Step 3: Send the PUT request
#             result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
#             logHandler.logging.info("Result: " + str(result))
#             self.assertNotEqual(result,"Fail","Inventory sync failed")
#             reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromVmVMWare(backups_testdata.xenserverInstanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            # Step 3: Send the PUT request
            actualResponse = backups.putXenServerInstanceBackup(token,instanceId,backups_testdata.xenserverInstanceName,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.xenserverVerify,backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            print  "Actual Response - " + str(actualResponse)           
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - ensure that Job Id is returned as -1
            responseResult = backups.getPutRequestResponse(actualResponse)
            print "responseResult: "+str(responseResult)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID from Response: " + str(strJobId))
            print "Job ID from Response: " + str(strJobId)
            if(strJobId == -1):
                raise Exception("Backup not triggered")
            else:    
                reportController.addTestStepResult("Backup job triggered", ExecutionStatus.PASS)
            
            #Verification 2: Verify the error message returned in the response object
            #print responseResult["data"][0]["message"]
            actualResponseParams=jsonHandler.getListOfMediaParameterInJson(actualResponse, True)
            logHandler.logging.info("actual response parameters " + str(actualResponseParams))
            print "actual response parameters " + str(actualResponseParams)                     
            self.assertListEqual(backups_testdata.instanceBackupResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(backups_testdata.vmwareInstanceBackupResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            
            dbResponse=vbackups.getJobByIdFromDB(strJobId)
            if(dbResponse=="Failed"):
                reportController.addTestStepResult("db Verification", ExecutionStatus.FAIL)
            else:
                reportController.addTestStepResult("db Verification", ExecutionStatus.PASS)
                reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.xenWaitTime, token, strJobId)    
            if(backupJobStatus!="Successful"):
                logHandler.logging.error("Backup not successful")
                reportController.addTestStepResult("Backup success", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup Success" , ExecutionStatus.PASS)
        
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:            
            #cleanup
            if(addedClientFlag == 1):
                clients.deleteVMClient(token, clientId)
                logHandler.logging.info("Client already existing is deleted..")
                reportController.addTestStepResult("Delete existing client", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
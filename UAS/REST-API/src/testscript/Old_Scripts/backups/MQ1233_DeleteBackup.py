'''
Created on Feb 23, 2015

@author: Rahul A.
'''

import unittest
from resourcelibrary import clients, backups,login, logout, jobs, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vbackups, vclients
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1233_DeleteBackup(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete a backup with specific id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Specific Backup")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Prerequisite -Post a backup job and fetch backup id (job id)
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            addedClientFlag = 0
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                   
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Prerequisite - Create a client - Send API request" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(clientId))
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")

                 
            # Step 3: Send the PUT request to create a backup            
            actualResponse = backups.putHypervInstanceBackup(token, instanceId, backups_testdata.backupType, backups_testdata.storageType, backups_testdata.verifyLevel, backups_testdata.sid)
            reportController.addTestStepResult("Prerequisite - Create a backup - Send API request" , ExecutionStatus.PASS)
                 
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            strJobId = '0'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID returned from response: " + str(strJobId))
            
            if(strJobId == '0'):
                raise Exception("Failed to trigger backup")
                                              
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus!="Successful"):
                logHandler.logging.error("Backup not successful..")
                reportController.addTestStepResult("Backup", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup" , ExecutionStatus.PASS) 
            
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup ID returned from DB: " + str(bid))
             
            # Step 2: Send the DELETE request
            actualResponse = backups.deleteBackup(token, str(bid))
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter
            responseResult = backups.getDeleteRequestResponse(actualResponse)               
            logHandler.logging.info("Response Result: " + str(responseResult))
            
            if (responseResult == "Pass"):
                logHandler.logging.info("Response parameter verification successful")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            # Verification 2: verify in the database view sm_backups where device column is set to 'deleted'
            dbResult = vbackups.confirmBackupDeletion(bid)
            logHandler.logging.info("DB Result: " + str(dbResult))
            
            if(dbResult == "(deleted)"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Database verification failed")    
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
            
        finally:
            #cleanup
            if(addedClientFlag == 1 or result == 'Fail'):
                clients.deleteClients(token, clientId)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
                 
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
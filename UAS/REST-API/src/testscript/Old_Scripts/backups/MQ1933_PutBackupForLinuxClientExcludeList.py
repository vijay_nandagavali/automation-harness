'''
Created on Sep 8, 2015

@author: Prateek
'''


import unittest

from resourcelibrary import clients, backups,login, logout,jobs
from testdata import clients_testdata, backups_testdata
from util import reportController, logHandler
from verificationsript import vbackups
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from testdata.clients_testdata import keyForId

class MQ1933_PutBackupForLinuxClientExcludeList(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Put - Backup for a linux client having exclude list")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Backup for a linux client having exclude list")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            linuxName = clients_testdata.getmachineName("LINUX")
            osType = clients_testdata.getType("LINUX")
            linuxIp = clients_testdata.getmachineIP("LINUX")
            excludeList = clients_testdata.getShareName("LINUX")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(linuxName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postLinuxClient(token, linuxName, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.linux_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted,   
                                                clients_testdata.linux_IsDefault, linuxIp)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            
            # Step 3: Send the PUT request
            actualResponse = backups.putBackupClientExcList(token, backups_testdata.backupType, backups_testdata.verifyNone, int(clientId), excludeList, backups_testdata.emailReport, backups_testdata.failureReport, backups_testdata.storageType, backups_testdata.backupType   )
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strClientName = responseResult["data"][0]["client_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strClientName))
            
            # Get actual response parameter from response
            actualResponseParams = backups.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            backups_testdata.linuxBackupResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(backups_testdata.linuxBackupResponseParameter))
            
            self.assertListEqual(backups_testdata.linuxBackupResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(backups_testdata.linuxBackupResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
            dbResult = vbackups.getJobByIdFromDB(strJobId)
            logHandler.logging.info("JOB triggered from jobID : " + str(dbResult))
            
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Database verification failed")
                reportController.addTestStepResult("Failed to find Job with Job ID" + str(strJobId), ExecutionStatus.FAIL)    
            
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus =="Successful" or backupJobStatus =="Warning" ):
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL) 
            
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            if(addedClientFlag):
                clients.deleteClients(token, clientId)
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)


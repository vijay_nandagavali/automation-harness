'''
Created on Nov 14, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import users, login, logout
from testdata import users_testdata
from util import reportController
from verificationsript import vusers
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1093_GetAllUsers(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get All Users")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = users.getAllUsers(token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = users.getUsersResponseParameters(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            users_testdata.responseParameter.sort()
            
            # Verification 1: Verify get all alerts response parameter
            if (actualResponseParams[0]=='administrator'):   
                self.assertListEqual(users_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(users_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            else:
                users_testdata.responseParameter1.sort()
                self.assertListEqual(users_testdata.responseParameter1, actualResponseParams, "Failed Response verification: Expected response [" + str(users_testdata.responseParameter1) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            usersFromDB = vusers.getAllUsersFromDB()
            print usersFromDB
            usersFromResponse = vusers.getUsersFromResponse(actualResponse)
            print usersFromResponse
            self.assertDictEqual(usersFromDB, usersFromResponse, "Failed Data base verification: Expected Response [" + str(usersFromDB) + "] and Actual Response [" + str(usersFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
             
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            

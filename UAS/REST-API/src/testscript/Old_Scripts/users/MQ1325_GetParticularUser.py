'''
Created on Nov 14, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import users, login, logout
from testdata import users_testdata
from util import reportController
from verificationsript import vusers
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1325_GetParticularUser(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Particular User")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = users.getParticularUser(users_testdata.userId, token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = users.getUsersResponseParameters(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            users_testdata.rootUserresponseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(users_testdata.rootUserresponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(users_testdata.rootUserresponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            usersFromDB = vusers.getSpecificUserFromDB(users_testdata.userId)
            print usersFromDB
            usersFromResponse = vusers.getUsersFromResponse(actualResponse)
            print usersFromResponse
            self.assertDictEqual(usersFromDB, usersFromResponse, "Failed Data base verification: Expected Response [" + str(usersFromDB) + "] and Actual Response [" + str(usersFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            

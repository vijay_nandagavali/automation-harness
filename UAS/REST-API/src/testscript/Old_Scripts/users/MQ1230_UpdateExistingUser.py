'''
Created on Feb 23, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import users, login, logout
from testdata import users_testdata
from util import reportController
from verificationsript import vusers
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1230_UpdateExistingUser(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Update Existing User")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = users.updateExistingUser(token, users_testdata.currentPassword, users_testdata.newPassword, users_testdata.userId)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            responseResult = users.getPutRequestResponse(actualResponse)
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
        
      
            # Verification 2: Verify get all alerts API response        
            changedPasswordFromDB = vusers.getChangedPasswordFromDB(users_testdata.userId)
            print changedPasswordFromDB
            if (changedPasswordFromDB == users_testdata.newPasswordString):
                reportController.addTestStepResult("User password successfully updated", ExecutionStatus.PASS)
            else:
                raise Exception ("User password not updated")
            
            users.updateExistingUser(token, users_testdata.newPassword, users_testdata.currentPassword, users_testdata.userId)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
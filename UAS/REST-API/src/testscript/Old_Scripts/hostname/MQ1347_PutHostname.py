'''
Created on Feb 12, 2015

@author: Rahul A.
'''

import unittest

from resourcelibrary import hostname, login, logout
from testdata import hostname_testdata
from util import reportController
from verificationsript import vhostname
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1347_PutHostname(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Put the hostname for the specified system")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get hostname information
            actualResponse = hostname.putHostnameInformation(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Verification 1: Verify the response parameter
            responseResult = hostname.getPutRequestResponse(actualResponse)
            print responseResult
            # Verification 1: verify the response parameter
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            # Verification 2: Verify in the DB        
            dbResult = vhostname.getHostnameFromDB()
            strName =  dbResult[1]
            strHostname = hostname_testdata.puthostName
            
            if(strName == strHostname):
                reportController.addTestStepResult("Hostname - Database verification passed test", ExecutionStatus.PASS)
                print "Database verification passed test"
            else:
                reportController.addTestStepResult("Hostname - Database verification failed", ExecutionStatus.FAIL)
                print "Hostname - Database verification failed test"
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)



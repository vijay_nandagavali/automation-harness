'''
Created on Sept 2, 2015

@author: Nikhil S
'''

import unittest

from resourcelibrary import updates, login, logout
from testdata import updates_testdata
from util import reportController,logHandler
from verificationsript import vupdates
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1923_GetAvailableUpdates(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get available updates")    
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - available updates")
        self.executionId = exeID  
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
                       
            # Step 1: Send request to get all alerts
            actualResponse = updates.getAllUpdates(token, updates_testdata.sid)
            reportController.addTestStepResult("API request sent", ExecutionStatus.PASS)
            logHandler.logging.info("API request sent! ")
            
            #Get number of updates 
            responseResult = updates.getRequestResponse(actualResponse)
            #logHandler.logging.info("API Response " + str(responseResult))
            updatesAvailable = responseResult["updates"]["count"] 
            logHandler.logging.info("Number of updates available:  " + str(updatesAvailable))
            
            if(updatesAvailable == 0):
                reportController.addTestStepResult("Updates not Available ", ExecutionStatus.PASS)
            else:
                
                #Verification #1
                # Get actual response parameter from response
                actualResponseParams = updates.getListOfActualResponseParameter(actualResponse)
                #print actualResponseParams
                logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                
                self.assertListEqual(updates_testdata.responseParameters, actualResponseParams, "Failed Response verification: Expected response [" + str(updates_testdata.responseParameters) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                logHandler.logging.info("Verification 1: Actual Params" + str(actualResponseParams) + "Expected Params: " +str(updates_testdata.responseParameters))
                reportController.addTestStepResult("Verify Restore response parameter", ExecutionStatus.PASS)
            
                #Verification #2
                valuesFromResponse = vupdates.getUpdatesFromResponse(actualResponse)
                print valuesFromResponse
                logHandler.logging.info("Actual Values from Response - " + str(valuesFromResponse))
                        
                valuesFromScript = vupdates.getUpdatesFromScript(updatesAvailable)
                print valuesFromScript
                logHandler.logging.info("Actual Values from Scripts - " + str(valuesFromScript))
                
                self.assertListEqual(valuesFromResponse, valuesFromScript, "Failed Response verification: Expected response [" + str(valuesFromScript) + "] and Actual parameter [" + str(valuesFromResponse) + "]")
                logHandler.logging.info("Verification 2: Actual Params" + str(valuesFromResponse) + "Expected Params: " +str(valuesFromScript))
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
              
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
             
            #logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
            

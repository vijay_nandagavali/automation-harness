'''
Created on Mar 2, 2015

@author: Amey.k
'''

import unittest
from resourcelibrary import inventory, login, logout
from testdata import inventory_testdata
from util import reportController, logHandler
from verificationsript import vinventory
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1259_GetInventorySyncInformation(unittest.TestCase):    
    def __init__(self, exeID):          
        reportController.initializeReport(type(self).__name__, "Get Inventory Sync Information")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Inventory Sync Information")
        self.executionId = exeID        
    
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = inventory.getInventorySyncInformation(token,inventory_testdata.sid)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = inventory.getListOfInventoryParametersInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            # sort expected response parameter list            
            inventory_testdata.responseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(inventory_testdata.responseParameter))
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(inventory_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(inventory_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            
            # Verification 2: Verify get all alerts API response        
            inventorysyncFromDB = vinventory.getIventorySyncFromDB()
            logHandler.logging.info("Data returned from DB: " + str(inventorysyncFromDB))
            
            inventorysyncFromResponse = vinventory.getInventorySyncFromResponse(actualResponse)
            logHandler.logging.info("Data from response: " + str(inventorysyncFromResponse))
            
            self.assertListEqual(inventorysyncFromDB, inventorysyncFromResponse, "Failed Data base verification: Expected Response [" + str(inventorysyncFromDB) + "] and Actual Response [" + str(inventorysyncFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
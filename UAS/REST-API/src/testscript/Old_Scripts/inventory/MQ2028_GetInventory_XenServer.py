'''
Created on Oct 15, 2015

@author: rahula
'''

import unittest
from resourcelibrary import login, logout, inventory
from testdata import inventory_testdata
from util import reportController, logHandler
from verificationsript import vinventory
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2028_GetInventory_XenServer(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get Xen Server Inventory details")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Xen Server Inventory")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Send API Request
            actualResponse = inventory.getInventory(token)      
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            #Get actual parameters from response
            actualResponseParams = vinventory.getInventoryParametersFromResponse(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            inventory_testdata.inventoryResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(inventory_testdata.inventoryResponseParameter))
            
            self.assertListEqual(actualResponseParams, inventory_testdata.inventoryResponseParameter, "Response parameter verification failed")
            reportController.addTestStepResult("Verify response parameters", ExecutionStatus.PASS)
            
            #Database verification
            dataFromResponse = vinventory.getInventoryResponseFromJson(actualResponse)
            logHandler.logging.info("Data from response: " + str(dataFromResponse))
            
            #dataFromDB = vinventory.getInventoryDataFromDB()
            dataFromDB = vinventory.getXenInventoryDataFromDB()
            logHandler.logging.info("Data from DB: " + str(dataFromDB))
            
            dbResult = len(dataFromDB) 
            dbResult = dbResult - 2
            responseResult = len(dataFromResponse)
            
            #self.assertListEqual(dataFromDB, dataFromResponse, "Database verification failed")
            if(dbResult == responseResult):
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
                logHandler.logging.info("Database verification done")
            else:
                reportController.addTestStepResult("Database verification failed", ExecutionStatus.FAIL)    
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
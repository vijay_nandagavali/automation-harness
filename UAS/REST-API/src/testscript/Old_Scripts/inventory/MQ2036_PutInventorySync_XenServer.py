'''
Created on Oct 15, 2015

@author: rahula
'''

import unittest
from resourcelibrary import inventory, login, logout
from testdata import inventory_testdata
from util import reportController, logHandler
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2036_PutInventorySync_XenServer(unittest.TestCase):    
    def __init__(self, exeID):          
        reportController.initializeReport(type(self).__name__, "Put Inventory Sync")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Inventory Sync")
        self.executionId = exeID        
    
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = inventory.putInventorySync(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Parse the actual response
            responseResult = inventory.parsePutInventorySyncResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            
            if(responseResult == 'Pass'):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception("Response verification failed")        
            
            # Verification 2: Verify get all alerts API response        
            syncStatus = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(syncStatus))
            self.assertNotEqual(syncStatus,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                       
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
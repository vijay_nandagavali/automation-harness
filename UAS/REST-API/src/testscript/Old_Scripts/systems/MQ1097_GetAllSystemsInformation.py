'''
Created on Nov 26, 2014

@author: Amey.k
'''
import unittest

from resourcelibrary import systems, login, logout
from testdata import systems_testdata
from util import reportController
from verificationsript import vsystems
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1097_GetAllSystemsInformation(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get All System Information")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = systems.getAllSystemsInformation(token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = systems.getListOfActualResponseParameter(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            systems_testdata.responseParameter1.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(systems_testdata.responseParameter1, actualResponseParams, "Failed Response verification: Expected response [" + str(systems_testdata.responseParameter1) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response      
            systemsFromDB = vsystems.getSystemsListFromDB()
            print systemsFromDB
            systemsCount = vsystems.getSystemsCountFromDB()
            systemsFromResponse = vsystems.getSystemsListFromResponse(actualResponse, systemsCount)
            print systemsFromResponse
            self.assertListEqual(systemsFromDB, systemsFromResponse, "Failed Data base verification: Expected Response [" + str(systemsFromDB) + "] and Actual Response [" + str(systemsFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
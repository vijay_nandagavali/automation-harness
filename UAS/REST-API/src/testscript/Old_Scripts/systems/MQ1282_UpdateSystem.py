'''
Created on Mar 24, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import systems, login, logout
from testdata import systems_testdata
from util import reportController
from verificationsript import vsystems
from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1282_UpdateSystem(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Update the System Information")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = systems.updateSystemName(token, systems_testdata.systemId, systems_testdata.oldSystemName)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = systems.putResponseParse(actualResponse)
            if (actualResponseParams == "Pass"):
                reportController.addTestStepResult("System name updated successfully", ExecutionStatus.PASS)
            else:
                raise Exception ("System name could not be resumed")          
          
            # Verification 2: Verify get all alerts API response      
            systemNameFromDB = vsystems.getSystemNameFromDB(systems_testdata.systemId)
            print systemNameFromDB
            if (str(systemNameFromDB) == systems_testdata.newSystemName):
                reportController.addTestStepResult("System name updated in the database", ExecutionStatus.PASS)
            systems.updateSystemName(token, systems_testdata.systemId,systems_testdata.oldSystemName)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
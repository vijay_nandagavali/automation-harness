'''
Created on Nov 26, 2014

@author: Amey.k
'''

import unittest
from resourcelibrary import systems, login, logout
from testdata import systems_testdata
from util import reportController, logHandler
from verificationsript import vsystems
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1326_GetSpecificSystemInformation(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get Specific System Information")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Specific system information")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = systems.getSpecificSystemInformation(token, systems_testdata.systemId)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = systems.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                        
            # sort expected response parameter list            
            systems_testdata.responseParameter1.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(systems_testdata.responseParameter1))
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(systems_testdata.responseParameter1, actualResponseParams, "Failed Response verification: Expected response [" + str(systems_testdata.responseParameter1) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                    
            # Verification 2: Verify get all alerts API response      
            systemsFromDB = vsystems.getspecificSystemInformationFromDB(systems_testdata.systemId)
            logHandler.logging.info("Data returned from DB: " + str(systemsFromDB))
            
            systemsCount = vsystems.getSystemsCountFromDB()
            logHandler.logging.info("Count returned from DB: " + str(systemsCount))
            
            systemsFromResponse = vsystems.getSystemsListFromResponse(actualResponse, systemsCount)
            logHandler.logging.info("Data from response: " + str(systemsFromResponse))
            
            self.assertListEqual(systemsFromDB, systemsFromResponse, "Failed Data base verification: Expected Response [" + str(systemsFromDB) + "] and Actual Response [" + str(systemsFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)

        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
        
        finally: 
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)

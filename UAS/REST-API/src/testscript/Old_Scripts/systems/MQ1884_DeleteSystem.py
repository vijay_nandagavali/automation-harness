'''
Created on Aug 17, 2015

@author: Nikhil S
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import systems_testdata,replication_testdata
from resourcelibrary import login, systems, logout
from verificationsript import vsystems

class MQ1884_DeleteSystem(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete a remote system")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Delete a remote system")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            print "login"
            
            #Prerequsite
            addSys = systems.postAddSystem(token, replication_testdata.tar_Name, apiconfig.environmentIpTarget) 
            logHandler.logging.info("Add system as prerequsite : " + str(addSys))
            
            #send APIRequest
            sysID_DB = vsystems.getSystemIDFromDB(replication_testdata.tar_Name)
            actualResponse = systems.deleteSystem(token, sysID_DB)            
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = systems.postResponseParser(actualResponse)
            print actualResponseParams
            logHandler.logging.info("actual Response Params: " + str(actualResponseParams))
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            # Verification 2: 
            sysID_DB = vsystems.getSystemIDFromDB(replication_testdata.tar_Name) 
            logHandler.logging.info("Deleted system ID :(0 indicates successful deletion, any other integer means the system still exists in sys.) " + str(sysID_DB))
            
            if(sysID_DB == 0):
                logHandler.logging.error("Database verificaton passed.")
                reportController.addTestStepResult("Database verificaton Passed", ExecutionStatus.PASS)
            else:
                logHandler.logging.error("Database verificaton Failed.")
                reportController.addTestStepResult("Database verificaton failed", ExecutionStatus.FAIL)
                raise Exception("Database verification failed")
                                   
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            #systems.deleteSystem(token, sysID_DB)
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
'''
Created on Mar 25, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import systems, login, logout
from testdata import systems_testdata
from util import reportController
from verificationsript import vsystems
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1284_GetSystemDetails(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get System Details")
        self.executionId = exeID
                
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = systems.getSystemDetails(token, systems_testdata.systemId)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = systems.getListOfActualResponseParameter(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            systems_testdata.responseParameterSystemDetails.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(systems_testdata.responseParameterSystemDetails, actualResponseParams, "Failed Response verification: Expected response [" + str(systems_testdata.responseParameterSystemDetails) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response      
            systemsFromDB = vsystems.getSystemDetailsFromDBAndMachine(systems_testdata.systemId)
            print systemsFromDB
            systemsFromResponse = vsystems.getSystemDetailsFromResponse(actualResponse)
            print systemsFromResponse
            self.assertListEqual(systemsFromDB, systemsFromResponse, "Failed Data base verification: Expected Response [" + str(systemsFromDB) + "] and Actual Response [" + str(systemsFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
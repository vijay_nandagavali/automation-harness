'''
Created on Aug 3, 2015

@author: Nikhil S
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import systems_testdata
from resourcelibrary import login, systems, logout
from verificationsript import vsystems

class MQ1791_PostMakeTarget(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Post make the local system capable of being a hot backup-copy target")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Post make the local system capable of being a hot backup-copy target")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            print "login"
            
            #Send POST API request
            actualResponse = systems.postMakeTarget(token, systems_testdata.network, systems_testdata.mask, systems_testdata.port) 
            print "actualResponse  " + str(actualResponse)
            logHandler.logging.info("Actual Response : " + str(actualResponse))
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = systems.postResponseParser(actualResponse)
            print actualResponseParams
            logHandler.logging.info("actual Response Params: " + str(actualResponseParams))
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            # Verification 2: 
            responseResult = vsystems.checkMakeTarget()
            logHandler.logging.info("Response Result for cmc_openvpn server confirm : " + str(responseResult))
            self.assertNotEquals(responseResult, "Fail", "Failed cmc_openvpn script verification.")
            reportController.addTestStepResult("Compare API response with script", ExecutionStatus.PASS)   
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
'''
Created on Aug 4, 2015

@author: Nikhil S
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import systems_testdata,replication_testdata
from resourcelibrary import login, systems, logout
from verificationsript import vsystems

class MQ1883_PostAddRemoteSys(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Post adds a remote system to be managed by the caller")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Post adds a remote system to be managed by the caller")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            sysID_DB = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            print "login"
            
            #Send POST API request
            actualResponse = systems.postAddSystem(token, replication_testdata.tar_Name, apiconfig.environmentIpTarget) 
            print "actualResponse  " + str(actualResponse)
            logHandler.logging.info("Actual Response : " + str(actualResponse))
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = systems.postResponseParser(actualResponse)
            print actualResponseParams
            logHandler.logging.info("actual Response Params: " + str(actualResponseParams))
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            # Verification 2: 
            sysID_DB = vsystems.getSystemIDFromDB(replication_testdata.tar_Name) 
            sysName_DB = vsystems.getSystemNameFromDB(sysID_DB)
            logHandler.logging.info("Added system Name : " + str(sysName_DB))
            logHandler.logging.info(str(replication_testdata.tar_Name))
            if(str(sysName_DB) == str(replication_testdata.tar_Name)):
                logHandler.logging.error("Database verificaton passed." + str(sysName_DB))
                reportController.addTestStepResult("Database verificaton Passed", ExecutionStatus.PASS)
            else:
                logHandler.logging.error("Database verificaton Failed." + str(sysName_DB))
                reportController.addTestStepResult("Database verificaton failed", ExecutionStatus.FAIL)
                raise Exception("Database verification failed")
                                   
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            systems.deleteSystem(token, sysID_DB)
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
'''
Created on Apr 15, 2015

@author: Nikhil.S
'''

import unittest, time
from resourcelibrary import reports, login, logout, clients, backups, jobs, inventory
from testdata import reports_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vreports, vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1355_GetBackupLegallyHeld(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get a list of legally held backups for the specified time range.")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Legally held backups list")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            
            addedClientFlag = 0
            #prerequsite
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag = 1
                #Step 2: Add Client
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                  
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(clientId))
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            # Step 3: Send the PUT request to create a backup
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            responseResult = backups.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            
            strJobId = responseResult["data"][0]["job_id"]
            logHandler.logging.info("Job ID returned from response: " + str(strJobId))
            if(strJobId=='-1'):
                reportController.addTestStepResult("Failed to trigger backup", ExecutionStatus.FAIL)
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            logHandler.logging.info("Backup Job Status: " + str(backupJobStatus))
            if(backupJobStatus!="Successful"):
                reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL)
            else:
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS) 
            
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup ID returned from DB: " + str(bid))
            
            logHandler.logging.info("Thread sleep for 60 sec")
            time.sleep(60)
                        
            actualResponse = backups.putBackupOnLegalHold(token,bid,backups_testdata.daysToLegalHold)
            reportController.addTestStepResult("Backup on Legal hold" , ExecutionStatus.PASS)
            
            # Step 1: Send request to get storage status
            actualResponse = reports.getListOfBackupLegallyHeld(token)            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = reports.getListOfBackupFailureParameterInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            paramCount = len(actualResponseParams)
            logHandler.logging.info("Parameter count: " + str(paramCount)) 
            
            # If backup failure data is not present, the response returns only 3 parameters - count, start and total
            if(paramCount > 3):
                # sort expected response parameter list            
                reports_testdata.backupLegallyHeldResponseParameter.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(reports_testdata.backupLegallyHeldResponseParameter))
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(reports_testdata.backupLegallyHeldResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(reports_testdata.backupLegallyHeldResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                
                
                # Verification 2: Verify get all alerts API response                 
                BackupFailureReportsFromResponse = vreports.getBackupLegallyHeldReportsFromResponse(actualResponse)
                logHandler.logging.info("From json response: " + str(BackupFailureReportsFromResponse))
              
                datareductionReportsFromDB = vreports.getBackupLegallyHeldReportsFromDB(actualResponse)
                logHandler.logging.info("From database response: " + str(datareductionReportsFromDB))
             
                self.assertListEqual(datareductionReportsFromDB, BackupFailureReportsFromResponse, "Failed Data base verification: Expected Response [" + str(datareductionReportsFromDB) + "] and Actual Response [" + str(BackupFailureReportsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Backup failure data not found", ExecutionStatus.PASS)
            #cleanup
            if(addedClientFlag==1):
                clients.deleteClients(token, clientId)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
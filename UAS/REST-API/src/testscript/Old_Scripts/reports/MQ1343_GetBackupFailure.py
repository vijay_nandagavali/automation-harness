'''
Created on Nov 27, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import reports, login, logout
from testdata import reports_testdata
from util import reportController
from verificationsript import vreports
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1343_GetBackupFailure(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get a list of failed backups for the specified time range.")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get storage status
            actualResponse = reports.getListOfBackupFailure(token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = reports.getListOfBackupFailureParameterInJson(actualResponse)
            
            paramCount = len(actualResponseParams)    
            
            # If backup failure data is not present, the response returns only 3 parameters - count, start and total
            if(paramCount > 3):
                # sort expected response parameter list            
                reports_testdata.backupfailureResponseParameter.sort()
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(reports_testdata.backupfailureResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(reports_testdata.backupfailureResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response                 
                BackupFailureReportsFromResponse = vreports.getBackupFailureReportsFromResponse(actualResponse)
                print "From json response: "+str(BackupFailureReportsFromResponse)
              
                datareductionReportsFromDB = vreports.getBackupFailureReportsFromDB(actualResponse)
                print "From database response: "+str(datareductionReportsFromDB)
             
                self.assertListEqual(datareductionReportsFromDB, BackupFailureReportsFromResponse, "Failed Data base verification: Expected Response [" + str(datareductionReportsFromDB) + "] and Actual Response [" + str(BackupFailureReportsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Backup failure data not found", ExecutionStatus.PASS)
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            

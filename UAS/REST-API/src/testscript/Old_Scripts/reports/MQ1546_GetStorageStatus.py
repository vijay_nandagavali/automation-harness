'''
Created on Nov 27, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import reports, login, logout
from testdata import reports_testdata
from util import reportController
from verificationsript import vreports
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1546_GetStorageStatus(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Returns status and details of all attached storage for all systems or the specified system.")
        self.executionId = exeID        
        
    def runTest(self):        
        #try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get storage status
            actualResponse = reports.getListOfStorage(token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = reports.getListOfStorageStatusParameterInJson(actualResponse)
            print actualResponseParams            
            # sort expected response parameter list            
            reports_testdata.storagereportsResponseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(reports_testdata.storagereportsResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(reports_testdata.storagereportsResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response                 
            restoreReportsFromResponse = vreports.getStorageReportsFromResponse(actualResponse)
            print "From json response: "+str(restoreReportsFromResponse)
            
            restoreReportsFromDB = vreports.getStorageReportsFromDB(actualResponse)
            print "From database response: "+str(restoreReportsFromDB)
            
            #self.assertListEqual(restoreReportsFromDB, restoreReportsFromResponse, "Failed Data base verification: Expected Response [" + str(restoreReportsFromDB) + "] and Actual Response [" + str(restoreReportsFromResponse) + "]")
            #reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        #except Exception as e:
            #reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            #reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            #print e    
        #finally:
            reportController.generateReportSummary(self.executionId)
            

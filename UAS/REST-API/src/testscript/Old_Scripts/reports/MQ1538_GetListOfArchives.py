'''
Created on Nov 27, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import reports, login, logout
from testdata import reports_testdata
from util import reportController, logHandler
from verificationsript import vreports, varchive
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1538_GetListOfArchives(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "GET List Of Archives")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Archive History Report")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = reports.getListOfArchives(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)

            # Get actual response parameter from response
            actualResponseParams = reports.getListOfArchiveParameterInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            archiveCount = varchive.getArchiveCountFromDB()
            logHandler.logging.info("Archive Count from DB: " + str(archiveCount))
            
            if(archiveCount > 0):
                expectedParams = reports_testdata.archivereportsResponseParameter        
            else:
                expectedParams = reports_testdata.archivereportsResponseParameter1
            
            #Sort expected parameters
            expectedParams.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(expectedParams))
            
            if(len(actualResponseParams) > 3):
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(expectedParams, actualResponseParams, "Failed Response verification: Expected response [" + str(reports_testdata.archivereportsResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)   
            else:
                reportController.addTestStepResult("Archive not available", ExecutionStatus.PASS)
                
            # Verification 2: Verify get all alerts API response                 
            archiveReportsFromResponse = vreports.getArchiveReportsFromResponse(actualResponse)
            logHandler.logging.info("From json response: "+str(archiveReportsFromResponse))
            
            archiveReportsFromDB = vreports.getArchiveReportsFromDB(actualResponse)
            logHandler.logging.info("From database response: "+str(archiveReportsFromDB))
            
            self.assertListEqual(archiveReportsFromDB, archiveReportsFromResponse, "Failed Data base verification: Expected Response [" + str(archiveReportsFromDB) + "] and Actual Response [" + str(archiveReportsFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                      
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
            
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on Nov 27, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import reports, login, logout
from testdata import reports_testdata
from util import reportController, logHandler
from verificationsript import vreports
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1547_GetStorageDataReduction(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Returns a history of data reduction for the specified range of days or a default date range.")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Storage Data Reduction")        
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get storage status
            actualResponse = reports.getListOfStorageDataReduction(token)            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = reports.getListOfStorageDataReductionParameterInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            expectedResponseParams = []
            if(len(actualResponseParams) > 3):
                expectedResponseParams = reports_testdata.datareductionreportsResponseParameter1
            else:
                expectedResponseParams = reports_testdata.datareductionreportsResponseParameter
            # sort expected response parameter list
            expectedResponseParams.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(expectedResponseParams))            
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(expectedResponseParams, actualResponseParams, "Failed Response verification: Expected response [" + str(expectedResponseParams) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response                 
            datareductionReportsFromResponse = vreports.getDataReductionReportsFromResponse(actualResponse)
            logHandler.logging.info("Data from response: " + str(datareductionReportsFromResponse))
             
            datareductionReportsFromDB = vreports.getDataReductionReportsFromDB(actualResponse)
            logHandler.logging.info("Data returned from DB: " + str(datareductionReportsFromDB))
            
            self.assertListEqual(datareductionReportsFromDB, datareductionReportsFromResponse, "Failed Data base verification: Expected Response [" + str(datareductionReportsFromDB) + "] and Actual Response [" + str(datareductionReportsFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)

        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
        
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
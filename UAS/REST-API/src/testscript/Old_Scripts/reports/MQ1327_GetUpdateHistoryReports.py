'''
Created on Nov 27, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import reports, login, logout
from testdata import reports_testdata
from util import reportController
from verificationsript import vreports
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1327_GetUpdateHistoryReports(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Update History Reports")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = reports.getUpdateHistoryReports(token, reports_testdata.sid)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = reports.getListOfActualResponseParameter(actualResponse)
            actualParamLen = len(actualResponseParams)
            if(actualParamLen > 4):                        
                # sort expected response parameter list            
                reports_testdata.responseParameter.sort()
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(reports_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(reports_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response      
                updateHistoryFromDB = vreports.getUpdateHistoryListFromDB()
                print updateHistoryFromDB
                historyCount = vreports.getUpdateHistoryCountFromDB()
                updateHistoryFromResponse = vreports.getUpdateHistoryListFromResponse(actualResponse, historyCount)
                print updateHistoryFromResponse
                self.assertListEqual(updateHistoryFromDB, updateHistoryFromResponse, "Failed Data base verification: Expected Response [" + str(updateHistoryFromDB) + "] and Actual Response [" + str(updateHistoryFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            

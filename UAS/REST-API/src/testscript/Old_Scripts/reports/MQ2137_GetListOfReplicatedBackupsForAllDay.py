'''
Created on Nov 18, 2015

@author: Prateek
'''

from resourcelibrary import reports, login, logout
from testdata import reports_testdata
from util import reportController, logHandler
from verificationsript import vreports
from config.Constants import ExecutionStatus
from config import apiconfig
import unittest, time

class MQ2137_GetListOfReplicatedBackupsForAllDay(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get list of replicated backups for all days")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGet - Get list of replicated backups for all days")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Login
            
            token = login.getAuthenticationToken_Source(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            actualResponse = reports.getListOfReplicatedBackups(token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            # Get actual response parameter from response
            actualResponseParams = reports.getListOfBackupFailureParameterInJson(actualResponse)
            print actualResponseParams 
            logHandler.logging.info("Actual Response Parameters: " + str(actualResponseParams))           
            
            reports_testdata.replicationResponseParameter.sort()
            
            self.assertListEqual(reports_testdata.replicationResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(reports_testdata.replicationResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            
            
            replicationReportFromResponse = vreports.getReplicationReportFromResponse(actualResponse)
            print "From json response: "+str(replicationReportFromResponse)
            logHandler.logging.info("Actual Response values: " + str(replicationReportFromResponse))
              
            replicationReportFromDB = vreports.getReplicationReportFromDB(actualResponse)
            print "From database response: "+str(replicationReportFromDB)
            logHandler.logging.info("DB Response: " + str(replicationReportFromDB))
            
            self.assertListEqual(replicationReportFromDB, replicationReportFromResponse, "Failed DB verification: Expected response [" + str(replicationReportFromDB) + "] and Actual parameter [" + str(replicationReportFromResponse) + "]")
            reportController.addTestStepResult("Data Base Verification", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)

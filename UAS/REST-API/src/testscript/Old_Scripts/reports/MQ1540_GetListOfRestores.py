'''
Created on Nov 27, 2014

@author: Amey.k
'''

import unittest
from resourcelibrary import reports, login, logout
from testdata import reports_testdata
from util import reportController, logHandler
from verificationsript import vreports
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1540_GetListOfRestores(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "GET List Of Restores")        
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser, apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = reports.getListOfRestores(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)

            # Get actual response parameter from response
            actualResponseParams = reports.getListOfArchiveParameterInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            # sort expected response parameter list            
            countParams = len(actualResponseParams)
            logHandler.logging.info("Parameter Length: " + str(countParams))
            if(countParams >3):
                reports_testdata.restorereportsResponseParameter.sort()
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(reports_testdata.restorereportsResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(reports_testdata.restorereportsResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response                 
                restoreReportsFromResponse = vreports.getBackupReportsFromResponse(actualResponse)
                logHandler.logging.info("From json response: " + str(restoreReportsFromResponse))
            
                restoreReportsFromDB = vreports.getRestoreReportsFromDB(actualResponse)
                logHandler.logging.info("From database response: " + str(restoreReportsFromDB))
            
                self.assertListEqual(restoreReportsFromDB, restoreReportsFromResponse, "Failed Data base verification: Expected Response [" + str(restoreReportsFromDB) + "] and Actual Response [" + str(restoreReportsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("List of restores is empty", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)           
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
        
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
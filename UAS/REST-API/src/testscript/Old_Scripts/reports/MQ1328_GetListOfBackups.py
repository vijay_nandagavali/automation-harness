'''
Created on Nov 27, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import reports, login, logout
from testdata import reports_testdata
from util import reportController
from verificationsript import vreports
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1328_GetListOfBackups(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "GET List Of Backups For Specified Time Range")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = reports.getListOfBackups(token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = reports.getListOfActualResponseParameter(actualResponse)
            #print actualResponseParams            
            # sort expected response parameter list            
            if(len(actualResponseParams) >3):
                reports_testdata.backupreportsResponseParameter.sort()
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(reports_testdata.backupreportsResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(reports_testdata.backupreportsResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response      
                backupReportsFromDB = vreports.getBackupReportsFromDB(actualResponse)
                print "From database response: "+str(backupReportsFromDB)
            
                backupReportsFromResponse = vreports.getBackupReportsFromResponse(actualResponse)
                print "From json response: "+str(backupReportsFromResponse)
            
                self.assertListEqual(backupReportsFromDB, backupReportsFromResponse, "Failed Data base verification: Expected Response [" + str(backupReportsFromDB) + "] and Actual Response [" + str(backupReportsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            else:
                reportController.addTestStepResult("No backup data was found", ExecutionStatus.PASS)
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            

'''
Created on May 7, 2015

@author: Savitha Peri
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout 
from testdata import archive_testdata
from verificationsript import varchive


class MQ1391_GetArchiveMediaCandidates(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Get Archive Media candidates")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Archive media candidates")
        self.executionId = exeID
    
    def runTest(self):
        try:
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            #print token
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Send API Request
            actualResponse = archive.getArchiveMediaCandidates(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            actualResponseParams = varchive.getMediaCandidatesResponseParams(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                        
            archive_testdata.mediaCandidatesResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.mediaCandidatesResponseParameter))
            
            self.assertListEqual(archive_testdata.mediaCandidatesResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.mediaCandidatesResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #DB verification
            dataFromDB =  ['eSATA','disk','Recovery Archive','disk', 'USB', 'disk']
            dataFromDB = varchive.getMediaCandidatesDataFromDB(dataFromDB)                        
            logHandler.logging.info("Data from DB: " + str(dataFromDB))
            
            dataFromResponse = varchive.getMediaCandidatesDataFromResponse(actualResponse)
            logHandler.logging.info("Data from Response: " + str(dataFromResponse))
            
            self.assertListEqual(dataFromDB, dataFromResponse, "Database verification")         
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            logHandler.logging.info("Database verification successful..")
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            #print e
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
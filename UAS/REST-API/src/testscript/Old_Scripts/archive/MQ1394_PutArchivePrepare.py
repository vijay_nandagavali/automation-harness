'''
Created on Apr 29, 2015

@author: rahula
'''

import unittest

from resourcelibrary import login, logout
from resourcelibrary import archive
from util import reportController
from config.Constants import ExecutionStatus
from config import apiconfig
from testdata import archive_testdata
from verificationsript import varchive
from time import sleep

class MQ1394_PutArchivePrepare(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Prepare the media for a specific system")
        self.executionId = exeID 
        self.prereq="false"       
        
    def runTest(self,prereq="false"):        
        try:
            self.prereq = prereq
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get success status
            actualResponse = archive.putPrepareArchiveMedia(token, archive_testdata.media_name, archive_testdata.archive_label)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            #Verification 1: Verify the response parameter   
            responseResult = archive.getPutRequestResponse(actualResponse)
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify the response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2: Verify in the database
            sleep(1200)
            dbResult = varchive.getArchivePrepareStatusFromDB()
            print dbResult
            if(dbResult >= 1):
                reportController.addTestStepResult("Database verification in archive sets successful", ExecutionStatus.PASS)
            else:
                raise Exception("Failed database verification - record not found in archive sets table")    
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            if(self.prereq=="false"):
                reportController.generateReportSummary(self.executionId)
            

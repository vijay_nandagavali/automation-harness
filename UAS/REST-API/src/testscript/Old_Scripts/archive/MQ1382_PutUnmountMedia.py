'''
Created on Nov 21, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import login, logout
from resourcelibrary import archive
from util import reportController
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1382_PutUnmountMedia(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Unmount the specified archive by name")
        self.executionId = exeID 
        self.prereq="false"       
        
    def runTest(self,prereq="false"):        
        try:
            self.prereq = prereq
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get success status
            actualResponse = archive.mountMedia(token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response   
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            if(self.prereq=="false"):
                reportController.generateReportSummary(self.executionId)
            

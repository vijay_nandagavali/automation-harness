'''
Created on Nov 21, 2014

@author: Amey.k
'''

import unittest, time
from resourcelibrary import login, logout, archive, storage
from util import reportController, logHandler
from config.Constants import ExecutionStatus
from config import apiconfig
from testdata import archive_testdata, storage_testdata
from verificationsript import varchive, vstorage

class MQ954_GetArchiveMediaSets(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get the list of the archive media connected to the specified system.")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Archive media sets")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser, apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageAddedFlag = 0            
            storageId = vstorage.getStorageIdByName(storage_testdata.googleCloudStorageName)
            logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
            
            if(storageId == 0):
                storageAddedFlag = 1
                logHandler.logging.info("Cloud Storage not available. Adding the same...")
                actualResponse = storage.postStorage(token)
                reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                logHandler.logging.info("Cloud storage " + storage_testdata.googleCloudStorageName + " is added")
                
                storageId = vstorage.getStorageIdByName(storage_testdata.googleCloudStorageName)
                logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
                if (storageId):
                    logHandler.logging.info("Storage added successfully...")
                    reportController.addTestStepResult("Add Storage as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add cloud storage")
                
                logHandler.logging.info("Thread sleep for 30 sec")
                time.sleep(30)
            
            #Check if the storage is online
            onlineStatus = ''
            onlineStatus = vstorage.getOnlineStatusFromDB(storageId)
            logHandler.logging.info("Online status returned from DB: " + str(onlineStatus))
            dbResult = ''
            
            if(onlineStatus == 'True'):
                logHandler.logging.info(str(storage_testdata.googleCloudStorageName) + " Cloud Storage is already online..")
                reportController.addTestStepResult("Cloud storage already online", ExecutionStatus.PASS)                
            else:
                #Send API request
                actualResponse = storage.putOnlineStorage(storage_testdata.sid, token, storageId)
                reportController.addTestStepResult("Send API request for Put cloud storage online" , ExecutionStatus.PASS)
                
                dbResult = vstorage.getOnlineStatusFromDB(storageId)
                logHandler.logging.info("Online status returned from DB: " + str(dbResult))
                if (dbResult == 'True'):
                    logHandler.logging.info("Database verification successful")
                    reportController.addTestStepResult("Cloud Storage Online", ExecutionStatus.PASS)
                else:
                    raise Exception("Cloud Storage Offline")
                    
            # Step 1: Send request to get all alerts
            actualResponse = archive.getMediaArchiveSetsInformation(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = archive.getMediaArchiveSetParametersinJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            if(len(actualResponseParams)>1):
                # sort expected response parameter list            
                archive_testdata.archiveMediaSetResponseParameter.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveMediaSetResponseParameter))                
                
                # Verification 1: Verify get all alerts response parameter       
                self.assertListEqual(archive_testdata.archiveMediaSetResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
                # Verification 2: Verify the archive media set names obtained from database        
                archiveMediaSetsFromDB = varchive.getArchiveMediaSetsFromDB(storage_testdata.googleCloudStorageName)
                logHandler.logging.info("Data returned from DB: " + str(archiveMediaSetsFromDB))
                
                archiveMediaSetsFromResponse = varchive.getArchiveMediaSetsFromResponse(actualResponse)
                logHandler.logging.info("Data from response: " + str(archiveMediaSetsFromResponse))
                
                dbCount = len(archiveMediaSetsFromDB)
                responseCount = len(archiveMediaSetsFromResponse)
                if(dbCount == responseCount):
                    reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                else:
                    reportController.addTestStepResult("Failed database verification: count in database " + str(dbCount) + " Count in response " + str(responseCount), ExecutionStatus.FAIL)
                
            else:
                reportController.addTestStepResult("Archive Media Sets not present", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Cleanup
            if(storageAddedFlag == 1 or dbResult == 'False'):
                storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Delete cloud storage" , ExecutionStatus.PASS)
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)

            reportController.generateReportSummary(self.executionId)
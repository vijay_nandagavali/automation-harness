'''
Created on Apr 28, 2015

@author: garima.g
'''
import unittest

from resourcelibrary import login, logout
from resourcelibrary import archive
from util import reportController,logHandler
from config.Constants import ExecutionStatus
from config import apiconfig
from testdata import archive_testdata
from verificationsript import varchive

class MQ1393_GetListOfArchiveSet(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get the list of archive sets")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - All Archive Sets")
        self.executionId = exeID 
        
    def runTest(self):        
        try:            
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)                     
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)                
                
            # Step 1: Send request to get success status
            actualResponse = archive.getArchiveList(token)            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            # Get actual response parameter from response   
            actualResponseParams = archive.getArchiveSetsInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                        
            if(len(actualResponseParams) > 1):
                # sort expected response parameter list            
                archive_testdata.archiveSetResponseParameter.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveSetResponseParameter))
                
                # Verification 1: Verify get all alerts response parameter                 
                self.assertListEqual(archive_testdata.archiveSetResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.archiveSetResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                
                # Verification 2: Verify Values for response parameters                            
                archiveDataFromDB = varchive.getArchiveSetsDataFromDB()
                logHandler.logging.info("Archive data from DB - " + str(archiveDataFromDB))
                                
                actualResponseValuesFromJson = varchive.getArchiveSetResponseParams(actualResponse)
                logHandler.logging.info("Archive data from Response - " + str(actualResponseValuesFromJson)) 
                                
                self.assertListEqual(archiveDataFromDB, actualResponseValuesFromJson, "Database Verification Failed")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)                                         
            else:
                logHandler.logging.info("No Archive Sets Present")
                reportController.addTestStepResult("Archive Set Not Present", ExecutionStatus.PASS)
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            #if(self.prereq=="false"):
            reportController.generateReportSummary(self.executionId)

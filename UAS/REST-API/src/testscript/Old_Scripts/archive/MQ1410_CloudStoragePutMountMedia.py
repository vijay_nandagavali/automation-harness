'''
Created on May 18, 2015

@author: garima.g
'''

import unittest

from resourcelibrary import login, logout
from resourcelibrary import archive,storage
from util import reportController,logHandler, jsonHandler
from config.Constants import ExecutionStatus
from config import apiconfig
from verificationsript import vstorage
from testdata import archive_testdata, clients_testdata

class MQ1410_CloudStoragePutMountMedia(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Mount the specified archive by name")
        self.executionId = exeID 
               
        
    def runTest(self):        
        try:
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)
            logHandler.logging.info("Auth Token: " + str(token))            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #check if storage already exists. if yes delete and then create new
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            if(storageId <> 0):
                logHandler.logging.info("Storage already available with same name. Deleting the same..")
                
                #Delete the existing storage
                actualResponse = storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Send API request for Delete Storage", ExecutionStatus.PASS)
                
                #Parse the response
                result = storage.getDeleteRequestResponse(actualResponse)
                if(result == "Pass"):
                    logHandler.logging.info("Storage deleted successfully..")
                    reportController.addTestStepResult("Delete Storage", ExecutionStatus.PASS)
                else:
                    raise Exception("Delete storage failed")
              
            # Step 1: Send request to get success status         
            #Send API Request for post storage
            actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
            reportController.addTestStepResult("Send API request for Post storage" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))                    
            
            #check if storage was added
            dbStorageId = 0
            dbStorageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(dbStorageId))
            
            
            if(dbStorageId <> 0):
                actualResponse = archive.mountMediaForCloud(token,cloudStorageName)               
                reportController.addTestStepResult("Send API request for Mount" , ExecutionStatus.PASS)
                jsonVal = jsonHandler.putMountMediaResponseFromJson(actualResponse)
                self.assertListEqual(jsonVal, archive_testdata.media_mount_response, "response value does not match")
                reportController.addTestStepResult("Mount response", ExecutionStatus.PASS)
                actualResponse = storage.deleteStorage(dbStorageId, token)
                reportController.addTestStepResult("Delete Storage" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Storage not added. Mount API call skipped")
                raise Exception("No cloud storage Available. Mount call skipped")
                 
            # Get actual response parameter from response   
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:            
            reportController.generateReportSummary(self.executionId)
            

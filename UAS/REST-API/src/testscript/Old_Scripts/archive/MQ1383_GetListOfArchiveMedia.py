'''
Created on Nov 21, 2014

@author: Amey.k
'''

import unittest
from resourcelibrary import login, logout, archive
from util import reportController, logHandler
from config.Constants import ExecutionStatus
from config import apiconfig
from testdata import archive_testdata
from verificationsript import varchive

class MQ1383_GetListOfArchiveMedia(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get the list of the archive media connected to the specified system.")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - List of Archive media")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser, apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = archive.getMediaInformation(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = archive.getMediaListParametersinJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            if(len(actualResponseParams)>1):
                # sort expected response parameter list            
                archive_testdata.responseParameter.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.responseParameter))
                # Verification 1: Verify get all alerts response parameter 
                       
                self.assertListEqual(archive_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
                # Verification 2: Verify get all alerts API response        
                alertsFromDB = varchive.getConnectedMediaFromDB()
                logHandler.logging.info("Data returned from DB: " + str(alertsFromDB))
                
                alertsFromResponse = varchive.getConnectedMediaFromResponse(actualResponse)
                logHandler.logging.info("Data from response: " + str(alertsFromResponse))
                
                self.assertEqual(alertsFromDB, alertsFromResponse, "Failed Data base verification: Expected Response [" + str(alertsFromDB) + "] and Actual Response [" + str(alertsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Archive Media not present", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
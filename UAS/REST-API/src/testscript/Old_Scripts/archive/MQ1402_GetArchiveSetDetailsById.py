'''
Created on May 6, 2015

@author: garima.g
'''
import unittest

from resourcelibrary import login, logout
from resourcelibrary import archive
from util import reportController,logHandler
from config.Constants import ExecutionStatus
from config import apiconfig
from testdata import archive_testdata
from verificationsript import varchive

class MQ1402_GetArchiveSetDetailsById(unittest.TestCase):  
    """tests API (GET)/archive/sets/id """  
    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get archive sets details By Id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Archive Sets Details By Id")
        self.executionId = exeID 
        
    def runTest(self):        
        try:            
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)                     
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)                
            
            #Get 1st archive_set_id from Archive_Sets
            archiveSetId = varchive.getArchiveSetIdFromDB()
            logHandler.logging.info("Archive Set ID returned from DB: " + str(archiveSetId))
            
            if(archiveSetId <> []):
                # Step 1: Send request to get success status
                actualResponse = archive.getArchiveSetDetailsById(token,archiveSetId)            
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                            
                # Get actual response parameter from response   
                actualResponseParams = archive.getArchiveSetByIdInJson(actualResponse)
                logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                
                for i in range(0,len(actualResponseParams)):
                    logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams[i]))
                             
                if(len(actualResponseParams) > 0):
                    # sort expected response parameter list            
                    archive_testdata.archiveSetByIdResponseParamter.sort()
                    logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveSetByIdResponseParamter))
                    logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveSetByIdProfileResParam))
                    logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveSetByIdProfileOptionsResParam))
                    logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveSetByIdProfileInstancesResParam))
                     
                    # Verification 1: Verify get all alerts response parameter                         
                    self.assertListEqual(archive_testdata.archiveSetByIdResponseParamter, actualResponseParams[0], "Failed Response verification: Expected response [" + str(archive_testdata.archiveSetByIdResponseParamter) + "] and Actual parameter [" + str(actualResponseParams[0]) + "]")
                    reportController.addTestStepResult("Verify response parameter Set", ExecutionStatus.PASS)
                    
                    self.assertListEqual(archive_testdata.archiveSetByIdProfileResParam, actualResponseParams[1], "Failed Response verification: Expected response [" + str(archive_testdata.archiveSetByIdProfileResParam) + "] and Actual parameter [" + str(actualResponseParams[1]) + "]")
                    reportController.addTestStepResult("Verify response parameter Profile", ExecutionStatus.PASS)
                    
                    self.assertListEqual(archive_testdata.archiveSetByIdProfileOptionsResParam, actualResponseParams[2], "Failed Response verification: Expected response [" + str(archive_testdata.archiveSetByIdProfileOptionsResParam) + "] and Actual parameter [" + str(actualResponseParams[2]) + "]")
                    reportController.addTestStepResult("Verify response parameter Profile Options", ExecutionStatus.PASS)
                    
                    if (len(actualResponseParams[3]) == 3):         
                        self.assertListEqual(archive_testdata.archiveSetByIdProfileInstancesResParam, actualResponseParams[3], "Failed Response verification: Expected response [" + str(archive_testdata.archiveSetByIdProfileInstancesResParam) + "] and Actual parameter [" + str(actualResponseParams[3]) + "]")
                        reportController.addTestStepResult("Verify response parameter Profile Instance", ExecutionStatus.PASS)
                    else:
                        self.assertListEqual(archive_testdata.archiveSetByIdProfileInstancesResParamWoSecName, actualResponseParams[3], "Failed Response verification: Expected response [" + str(archive_testdata.archiveSetByIdProfileInstancesResParamWoSecName) + "] and Actual parameter [" + str(actualResponseParams[3]) + "]")
                        reportController.addTestStepResult("Verify response parameter Profile Instance", ExecutionStatus.PASS)
                        
                    # Verification 2: Verify Values for response parameters                            
                    archiveDataFromDB = varchive.getArchiveSetDataByIdFromDB(archiveSetId)
                    for i in range(0,len(archiveDataFromDB)):
                        logHandler.logging.info("Archive data from DB - " + str(archiveDataFromDB[i]))
                                     
                    actualResponseValuesFromJson = varchive.getArchiveSetByIdResponseParams(actualResponse)
                    for i in range(0,len(actualResponseValuesFromJson)):
                        logHandler.logging.info("Archive data from Response - " + str(actualResponseValuesFromJson[i]))
                    
                    for i in range(0,len(actualResponseValuesFromJson)):                                
                        self.assertListEqual(archiveDataFromDB[i], actualResponseValuesFromJson[i], "Database Verification Failed")
                        reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)                                                                             
            else:
                logHandler.logging.info("No Archive Sets Present")
                reportController.addTestStepResult("Archive Set Not Present", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
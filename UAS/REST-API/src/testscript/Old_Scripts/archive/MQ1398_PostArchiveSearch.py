'''
Created on Apr 30, 2015

@author: Savitha Peri
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout 
from testdata import archive_testdata
from verificationsript import varchive


class MQ1398_PostArchiveSearch(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Post Archive Search")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Archive search")
        self.executionId = exeID
    
    def runTest(self):
        try:
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            #print token
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #verify if any archives are available in DB
            flArchiveCount = varchive.getFileLevelArchiveCountFromDB()
            logHandler.logging.info("File level Archive count returned from DB - " + str(flArchiveCount))
            
            if(flArchiveCount == 0):
                logHandler.logging.error("No file level archives available in DB")
                raise Exception("File level Archives not available in DB")
            
            #Send API Request
            actualResponse = archive.postArchiveSearch(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            actualResponseParams = varchive.getArchiveSearchResponseParams(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                        
            archive_testdata.archiveSearchResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveSearchResponseParameter))
            
            self.assertListEqual(archive_testdata.archiveSearchResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.archiveSearchResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #DB verification
#             archiveDataFromDB = varchive.getSpecificArchiveDataFromDB(archiveSetId)
#             logHandler.logging.info("Archive data from DB - " + str(archiveDataFromDB))
#             
#             archiveDataFromResponse = varchive.getArchiveDataFromResponse(actualResponse)
#             logHandler.logging.info("Archive data from Response - " + str(archiveDataFromResponse)) 
#              
#             self.assertListEqual(archiveDataFromDB, archiveDataFromResponse, "Database verification failed")
#             reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            #print e
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
'''
Created on Jun 10, 2015

@author: savitha.p
'''

import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout, clients, inventory, jobs, storage, backups 
from testdata import backups_testdata, clients_testdata, archive_testdata, inventory_testdata
from verificationsript import varchive, vbackups, vclients, vstorage

class MQ1549_PutVmwareFullArchive(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Create New Full Archive for vmware client")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\t PUT - Vmware Full Archive")
        self.executionId = exeID
    
    def runTest(self):
        try:            
            #Test data used in the script
            vmName=clients_testdata.getmachineName("VM1")
            vmIp=clients_testdata.getmachineIP("VM1")
            instanceName = clients_testdata.getShareName("VM1")
            vmType = clients_testdata.getType("VM1")
            vmUsername = clients_testdata.getUserName("VM1")
            vmPassword = clients_testdata.getPassword("VM1")
            esxName = clients_testdata.getmachineName("ESX-Server")
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageAddedFlag = 0            
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
            
            if(storageId == 0):
                storageAddedFlag = 1
                logHandler.logging.info("Cloud Storage not available. Adding the same...")
                actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
                reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                logHandler.logging.info("Cloud storage " + cloudStorageName + " is added")
                
                storageId = vstorage.getStorageIdByName(cloudStorageName)
                logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
                if (storageId):
                    logHandler.logging.info("Storage added successfully...")
                    reportController.addTestStepResult("Add Storage as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add cloud storage")
                
                logHandler.logging.info("Thread sleep for 30 sec")
                time.sleep(30)
                
            uuid = vclients.getSpecificEsxServerFromDB(esxName)
            logHandler.logging.info("UUID returned from DB: " + str(uuid))
            
            if(uuid == 0):
                # Step 1: Send request to update a client
                actualResponse = clients.postVMClient(token,clients_testdata.sid, vmName, vmType, 
                                                clients_testdata.vm_priority, clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, clients_testdata.vm_use_ssl, 
                                                clients_testdata.vm_defaultschedule, clients_testdata.vm_port, vmIp, 
                                                clients_testdata.vm_existing_credential, vmUsername, vmPassword, 
                                                clients_testdata.vm_domain)
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                
                uuid = vclients.getSpecificEsxServerFromDB(esxName)
                logHandler.logging.info("UUID returned from DB: " + str(uuid))
                if(uuid):
                    reportController.addTestStepResult("Add client", ExecutionStatus.PASS)
                else:
                    raise Exception("Add client failed")
            
            result = inventory.waitForInventorySync(token, 120, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromVmVMWare(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            #Check Full backups
            fullBackups = vbackups.getFullVMWareBackupsFromDB(instanceId)
            logHandler.logging.info("Vmware Full backups returned from DB: " + str(fullBackups))
            
            if(len(fullBackups) == 0):
                #Create Full Backup
                actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
                reportController.addTestStepResult("Send API request for Creating new vmware Backup" , ExecutionStatus.PASS)
        
                # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
                responseResult = backups.getPutRequestResponse(actualResponse)
                strJobId = '-1'
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID from Response: " + str(strJobId))
                if(strJobId == '-1'):
                    raise Exception("Failed to trigger backup")
        
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                logHandler.logging.info("Backup job status: " + str(backupJobStatus))
                if(backupJobStatus!="Successful"):
                    raise Exception("HyperV Full Backup not successful")
                else:
                    logHandler.logging.info("Backup successful")
                    reportController.addTestStepResult("HyperV Full Backup" , ExecutionStatus.PASS)
            
                bid=vbackups.getBackupId(strJobId)
                logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
            
                logHandler.logging.info("Thread sleep for 60 sec..")
                time.sleep(60)
                        
            #Erase Cloud Storage
            mediaLabel = archive_testdata.cloud_archive_label
            actualResponse = archive.putPrepareArchiveMedia(token, cloudStorageName, mediaLabel)
            reportController.addTestStepResult("Send API request for Archive Media Prepare" , ExecutionStatus.PASS)
        
            #Verification 1: Verify the response parameter   
            responseResult = archive.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
        
            dbResult = varchive.getArchivePrepareStatusFromDB(cloudStorageName)
            logHandler.logging.info("DB Result: " + str(dbResult))
        
            if(dbResult == 0):
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Failed database verification - some records found in archive sets table")  
            
            #Send API Request
            actualResponse = archive.putArchive(token, instanceId, cloudStorageName, "Full")
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            jobNo = varchive.verifyPutArchiveResponse(actualResponse)
            logHandler.logging.info("Create Archive successful. Job Number: " + str(jobNo))                        
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            archiveJobStatus = jobs.waitForJobCompletion(archive_testdata.archiveJobType, archive_testdata.archiveWaitTime, token, jobNo)
            logHandler.logging.info("Backup job status: " + str(archiveJobStatus))
            
            if(archiveJobStatus!="Successful"):
                reportController.addTestStepResult("Archive not successful", ExecutionStatus.FAIL)
                raise Exception("Archive not successful")
            else:
                logHandler.logging.info("Archive successful..")
                reportController.addTestStepResult("Archive successful" , ExecutionStatus.PASS)
            
            #DB verification
            #Alternate function called
            archiveId = varchive.verifyPutArchiveDB2(jobNo)
            logHandler.logging.info("Archive Id returned from DB: " + str(archiveId))
            self.assertNotEqual(archiveId, "", "Database verification")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
            #Cleanup
            if(storageAddedFlag):
                storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Delete cloud storage" , ExecutionStatus.PASS)
            
#             if(clientAddedFlag):
#                 clients.deleteVMClient(token, uuid)
#                 logHandler.logging.info("Client that is added in this test has been deleted..")
#                 reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)        
'''
Created on May 5, 2015

@author: Savitha Peri
'''

import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout, storage
from testdata import archive_testdata, storage_testdata, clients_testdata
from verificationsript import varchive, vstorage

class MQ1560_CloudStoragePostArchiveCatalog(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Post Archive Catalog for Cloud Storage")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Archive catalog for Cloud Storage")
        self.executionId = exeID
    
    def runTest(self):
        try:
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageAddedFlag = 0            
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
            
            if(storageId == 0):
                logHandler.logging.info("Cloud Storage not available. Adding the same...")
                actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
                reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                logHandler.logging.info("Cloud storage " + cloudStorageName + " is added")
                
                storageId = vstorage.getStorageIdByName(cloudStorageName)
                logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
                if (storageId):
                    logHandler.logging.info("Storage added successfully...")
                    reportController.addTestStepResult("Add Storage as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add cloud storage")
                storageAddedFlag = 1                
                logHandler.logging.info("Thread sleep for 30 sec")
                time.sleep(30)
            
            #Check if the storage is online
            onlineStatus = ''
            onlineStatus = vstorage.getOnlineStatusFromDB(storageId)
            logHandler.logging.info("Online status returned from DB: " + str(onlineStatus))
            dbResult = ''
        
            if(onlineStatus == 'True'):
                logHandler.logging.info(str(cloudStorageName) + " Cloud Storage is already online..")
                reportController.addTestStepResult("Cloud storage already online", ExecutionStatus.PASS)                
            else:
                #Send API request
                actualResponse = storage.putOnlineStorage(storage_testdata.sid, token, storageId)
                reportController.addTestStepResult("Send API request for Put cloud storage online" , ExecutionStatus.PASS)
            
                dbResult = vstorage.getOnlineStatusFromDB(storageId)
                logHandler.logging.info("Online status returned from DB: " + str(dbResult))
                if (dbResult == 'True'):
                    logHandler.logging.info("Database verification successful")
                    reportController.addTestStepResult("Cloud Storage Online", ExecutionStatus.PASS)
                else:
                    raise Exception("Cloud Storage Offline")
                
            #Send API Request
            actualResponse = archive.postArchiveCatalog(token, cloudStorageName)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            actualResponseParams = varchive.getPostArchiveCatalogResponseParams(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                        
            archive_testdata.postArchiveCatalogResponseParamter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.postArchiveCatalogResponseParamter))
            expectedResponseParams=[]
            if(len(actualResponseParams)==1):
                expectedResponseParams=['result']
            else:
                expectedResponseParams=archive_testdata.postArchiveCatalogResponseParamter
            
            self.assertListEqual(expectedResponseParams, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.postArchiveCatalogResponseParamter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #DB verification
            dataFromDB = varchive.getPostArchiveCatalogDataFromDB(cloudStorageName)
            logHandler.logging.info("Data from DB: " + str(dataFromDB))
            print "Data from DB: " + str(dataFromDB)
            
            dataFromResponse = varchive.getPostArchiveCatalogFromResponse(actualResponse)
            logHandler.logging.info("Data from Response: " + str(dataFromResponse))
            print "Data from Response: " + str(dataFromResponse)
            
            if(dataFromDB == dataFromResponse):
                logHandler.logging.info("Database verification successful..")
                print "Database verification successful.."
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Database verification failed..")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup
            if(storageAddedFlag == 1 or dbResult == 'False'):
                storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Delete cloud storage" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)     
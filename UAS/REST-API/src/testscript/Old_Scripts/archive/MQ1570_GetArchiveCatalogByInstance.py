'''
Created on Apr 23, 2015

@author: Savitha Peri
'''

import unittest

from util import reportController, logHandler
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout
from config import apiconfig
from testdata import archive_testdata
from verificationsript import varchive

class MQ1570_GetArchiveCatalogByInstance(unittest.TestCase):
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get Archive catalog - view by instance")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE:::\tGET - Archive Catalog, view by Instance")
        self.executionId = exeID
        
    def runTest(self):
        try:            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Send API Request
            actualResponse = archive.getCatalogByInstance(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            #Parse the actual response
            actualResponseParams = archive.getCatalogByInstanceParametersinJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            if(len(actualResponseParams) > 2):
                archive_testdata.catalogByInstanceResponseParameter.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.catalogByInstanceResponseParameter))
            
                self.assertListEqual(archive_testdata.catalogByInstanceResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.catalogByInstanceResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
   
                #DB verification
                dataFromResponse = archive.getCatalogByInstanceResponseFromJson(actualResponse)
                logHandler.logging.info("Data from response: " + str(dataFromResponse))
                
                dataFromDB = varchive.getArchiveCatalogDataFromDB()
                logHandler.logging.info("Data from DB: " + str(dataFromDB))
                
                #Compare Db values with Response values
                result = varchive.compareResponseAndDbData(dataFromResponse, dataFromDB)
                if(result == 'Pass'): 
                    reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
                elif(result == 'Fail'):
                    raise Exception("Database verification failed")                
            else:
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS) 
            
            reportController.generateReportSummary(self.executionId)
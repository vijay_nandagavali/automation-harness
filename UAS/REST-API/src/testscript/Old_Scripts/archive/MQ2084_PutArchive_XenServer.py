'''
Created on Oct 27, 2015

@author: Nikhil S
'''

import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout, clients, storage, backups, jobs, inventory
from testdata import backups_testdata, clients_testdata, storage_testdata, archive_testdata, inventory_testdata
from verificationsript import varchive, vbackups, vclients, vstorage
from time import sleep

class MQ2084_PutArchive_XenServer(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Create New  Full Archive for Xenserver")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\t PUT - Xenserver Full Archive")
        self.executionId = exeID
    
    def runTest(self):
        try:   
            print "start!!!"         
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            dbResult = ''
            clientAdded = 0
            storageAddedFlag = 0            
            storageId = vstorage.getStorageIdByName(storage_testdata.googleCloudStorageName)
            logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
            
            if(storageId == 0):
                commandResponse= storage.deleteStorageByCommandline(storage_testdata.googleCloudStorageBucketName)
                logHandler.logging.info("Cloud Storage not available. Adding the same...")
                actualResponse = storage.postStorage(token)
                reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                logHandler.logging.info("Cloud storage " + storage_testdata.googleCloudStorageName + " is added")
                
                storageId = vstorage.getStorageIdByName(storage_testdata.googleCloudStorageName)
                logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
                if (storageId):
                    logHandler.logging.info("Storage added successfully...")
                    reportController.addTestStepResult("Add Storage as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add cloud storage")
                storageAddedFlag = 1                
                logHandler.logging.info("Thread sleep for 30 sec")
                time.sleep(30)
            
            cid = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
            logHandler.logging.info("Client ID from DB: " + str(cid))
            
            if(cid==0):
                clientAdded = 1
                # Step 1: Send request to update a client
                actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver, 
                                                       clients_testdata.system_xenserver, clients_testdata.ip_xenserver, 
                                                       clients_testdata.username_xenserver, clients_testdata.password_xenserver, 
                                                       clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, 
                                                       clients_testdata.is_synchable_xenserver, clients_testdata.is_auth_enabled_xenserver)
                logHandler.logging.info("Actual response for Add client: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Add client" , ExecutionStatus.PASS)
               
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client from response: " + str(cid))
                # Verification 1: verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully...")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
            
            sleep(60)
            instanceId = vbackups.getInstanceIdFromVm(backups_testdata.xenserverInstanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            #Check Full backups
            fullBackups = vbackups.getFullHyperVBackupsFromDB(cid, instanceId)
            logHandler.logging.info("XenServer Full backups returned from DB: " + str(fullBackups))
            
            if(len(fullBackups) == 0):
                #Create Full Backup
                actualResponse = backups.putXenServerInstanceBackup(token,instanceId,backups_testdata.xenserverInstanceName,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.xenserverVerify,backups_testdata.sid)
                reportController.addTestStepResult("Send API request for Creating new XenServer Backup" , ExecutionStatus.PASS)
        
                # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
                responseResult = backups.getPutRequestResponse(actualResponse)
                strJobId = '-1'
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID from Response: " + str(strJobId))
                if(strJobId == '-1'):
                    raise Exception("Failed to trigger backup")
        
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.xenWaitTime, token, strJobId)
                if(backupJobStatus!="Successful"):
                    raise Exception("XenServer Full Backup not successful")
                else:
                    logHandler.logging.info("Backup successful")
                    reportController.addTestStepResult("XenServer Full Backup" , ExecutionStatus.PASS)
            
                bid=vbackups.getBackupId(strJobId)
                logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
            
                logHandler.logging.info("Thread sleep for 60 sec..")
                time.sleep(60)
            
            #Check if the storage is online
            onlineStatus = ''
            onlineStatus = vstorage.getOnlineStatusFromDB(storageId)
            logHandler.logging.info("Online status returned from DB: " + str(onlineStatus))
        
            if(onlineStatus == 'True'):
                logHandler.logging.info(str(storage_testdata.googleCloudStorageName) + " Cloud Storage is already online..")
                reportController.addTestStepResult("Cloud storage already online", ExecutionStatus.PASS)                
            else:
                #Send API request
                actualResponse = storage.putOnlineStorage(storage_testdata.sid, token, storageId)
                reportController.addTestStepResult("Send API request for Put cloud storage online" , ExecutionStatus.PASS)
            
                dbResult = vstorage.getOnlineStatusFromDB(storageId)
                logHandler.logging.info("Online status returned from DB: " + str(dbResult))
                if (dbResult == 'True'):
                    logHandler.logging.info("Database verification successful")
                    reportController.addTestStepResult("Cloud Storage Online", ExecutionStatus.PASS)
                else:
                    raise Exception("Cloud Storage Offline")
              
            #Erase Cloud Storage
            mediaLabel = archive_testdata.cloud_archive_label
            actualResponse = archive.putPrepareArchiveMedia(token, storage_testdata.googleCloudStorageName, mediaLabel)
            reportController.addTestStepResult("Send API request for Archive Media Prepare" , ExecutionStatus.PASS)
        
            #Verification 1: Verify the response parameter   
            responseResult = archive.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
        
            dbResult = varchive.getArchivePrepareStatusFromDB(storage_testdata.googleCloudStorageName)
            logHandler.logging.info("DB Result: " + str(dbResult))
        
            if(dbResult == 0):
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Failed database verification - some records found in archive sets table")            
            
            #Send API Request for Creating Full Archive
            actualResponse = archive.putArchive(token, instanceId, storage_testdata.googleCloudStorageName, "Full")
            reportController.addTestStepResult("Send API request for Create Archive" , ExecutionStatus.PASS)
                        
            #Parse the actual response
            jobNo = varchive.verifyPutArchiveResponse(actualResponse)
            logHandler.logging.info("Create Archive successful. Job Number: " + str(jobNo))
            self.assertNotEqual(jobNo,"","Response verification failed")                        
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #Check the status of archive job
            archiveJobStatus=jobs.waitForJobCompletion("Archive", backups_testdata.waittime, token, jobNo)
            logHandler.logging.info("Archive Job status: " + str(archiveJobStatus))
            if(archiveJobStatus!="Successful"):
                raise Exception("Xen Server Archive not successful")
            else:
                logHandler.logging.info("Archive successful")
                reportController.addTestStepResult("Xen Server Full Archive" , ExecutionStatus.PASS)
                
            #DB verification
            archiveBackupNos = varchive.getIncrementalArchiveBackupNosFromDB(instanceId)
            logHandler.logging.info("Backup Nos associated with Archives returned from DB: " + str(archiveBackupNos))
            
            xenVBackupNos = vbackups.getXenBackupsFromDB(cid, instanceId)
            logHandler.logging.info("Xen Server Backup Nos returned from DB: " + str(xenVBackupNos))
            xenBackups = [xenVBackupNos[0]]
            logHandler.logging.info("Xen Server Backups returned from DB: " + str(xenBackups))
            
            self.assertListEqual(archiveBackupNos, xenBackups, "Database verification")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
            #Wait for Job completion
#             jobStatus = jobs.waitForScheduleJobCompletion("Archive", 1200, token, jobNo)
#             logHandler.logging.info("Job Status : " + str(jobStatus))
#             self.assertEqual(jobStatus,"No jobs","Job still available in Active Jobs")
            
            #Cleanup
#             if(clientAdded):
#                 clients.deleteClients(token, cid)
#                 logHandler.logging.info("Client deleted..")
#                 reportController.addTestStepResult("Delete Client" , ExecutionStatus.PASS)

            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            if(storageAddedFlag == 1 or dbResult == 'False'):
                storage.deleteStorage(storageId, token)
                commandResponse= storage.deleteStorageByCommandline(storage_testdata.googleCloudStorageBucketName)
                reportController.addTestStepResult("Delete cloud storage" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on Jun 1, 2015

@author: savitha.p
'''

import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout, storage, restore, jobs, clients, backups
from testdata import archive_testdata, storage_testdata, backups_testdata, clients_testdata
from verificationsript import varchive, vstorage, vbackups, vrestore, vclients

class MQ1466_CloudStoragePutArchiveMediaPrepare(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Prepare Archive Media for Cloud Storage")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\t PUT - Archive Media Prepare for Cloud Storage")
        self.executionId = exeID
    
    def runTest(self):
        try:            
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
            
            storageAddedFlag = 0
            addedClientFlag = 0
            
            if(storageId == 0):
                logHandler.logging.info("Cloud Storage not available. Adding the same...")
                actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
                actualResponseParams = vstorage.postStorageResponseParameters(actualResponse)
                logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                paramLength = len(actualResponseParams)
                logHandler.logging.info("Actual Response Parameter Length - " + str(paramLength))
                
                expectedParams = []
                if(paramLength == 4):
                    expectedParams = storage_testdata.postStorageResponseParameter
                else:
                    expectedParams = storage_testdata.postStorageResponseParameter1
                
                expectedParams.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(expectedParams))
                
                self.assertListEqual(expectedParams, actualResponseParams, "Failed Response verification: Expected response [" + str(expectedParams) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)                
                logHandler.logging.info("Cloud storage " + cloudStorageName + " is added")
                
                storageId = vstorage.getStorageIdByName(cloudStorageName)
                logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
                storageAddedFlag = 1
                
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS)
                
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(clientId))
                
                logHandler.logging.info("Thread sleep for 90 sec..")
                time.sleep(90)
                
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            
            if(instanceId == None):
                raise Exception("Instance ID not found")
            
            archiveList = varchive.getArchivesForParticularStorageFromDB(cloudStorageName)
            logHandler.logging.info("Archive list returned from DB: " + str(archiveList))
            
            if(len(archiveList) == 0):
                fullBackups = vbackups.getFullHyperVBackupsFromDB(clientId, instanceId)
                logHandler.logging.info("HyperV Full backups returned from DB: " + str(fullBackups))
            
                if(len(fullBackups) == 0):
                    actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
                    reportController.addTestStepResult("Send API request for Creating new HyperV Backup" , ExecutionStatus.PASS)
                
                    # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
                    responseResult = backups.getPutRequestResponse(actualResponse)
                    logHandler.logging.info("Response Result: " + str(responseResult))
                    
                    strJobId = '-1'
                    strJobId = responseResult["data"][0]["job_id"] 
                    logHandler.logging.info("Job ID from Response: " + str(strJobId))
                    if(strJobId == '-1'):
                        raise Exception("Failed to trigger backup")
            
                    #Check the status of backup
                    backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                    if(backupJobStatus!="Successful"):
                        raise Exception("HyperV Full Backup not successful")
                    else:
                        logHandler.logging.info("Backup successful")
                        reportController.addTestStepResult("HyperV Full Backup" , ExecutionStatus.PASS)
                
                    bid=vbackups.getBackupId(strJobId)
                    logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
                
                    logHandler.logging.info("Thread sleep for 60 sec..")
                    time.sleep(60)
            
                #Create Archive
                actualResponse = archive.putArchive(token, instanceId, cloudStorageName,"Full")
                reportController.addTestStepResult("Send API request for Put Archive" , ExecutionStatus.PASS)
                                
                jobNo = varchive.verifyPutArchiveResponse(actualResponse)
                logHandler.logging.info("Create Archive successful. Job Number: " + str(jobNo))                        
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                
                #Check the status of archive
                archiveJobStatus=jobs.waitForJobCompletion("Archive", backups_testdata.waittime, token, jobNo)
                if(archiveJobStatus!="Successful"):
                    raise Exception("HyperV Full Archive not successful")
                else:
                    logHandler.logging.info("Archive successful")
                    reportController.addTestStepResult("HyperV Full Archive" , ExecutionStatus.PASS)
                
                logHandler.logging.info("Thread sleep for 30 sec.")
                time.sleep(30)
                
                #DB verification
                archiveId = varchive.verifyPutArchiveDB(jobNo)
                logHandler.logging.info("Archive Id returned from DB: " + str(archiveId))
                self.assertNotEqual(archiveId, "", "Database verification")
                
            #Send API request
            actualResponse = archive.putPrepareArchiveMedia(token, cloudStorageName, archive_testdata.cloud_archive_label)
            reportController.addTestStepResult("Send API request for Archive Media Prepare" , ExecutionStatus.PASS)
            
            #Verification 1: Verify the response parameter   
            responseResult = archive.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify the response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2: Verify in the database
            dbResult = varchive.getArchivePrepareStatusFromDB(cloudStorageName)
            logHandler.logging.info("DB Result: " + str(dbResult))
            
            if(dbResult == 0):
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Failed database verification - some records found in archive sets table")
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Cleanup
            if(storageAddedFlag):
                storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Delete cloud storage" , ExecutionStatus.PASS)

#             if(addedClientFlag):
#                 clients.deleteClients(token, clientId) 
#                 reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)

            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
               
            reportController.generateReportSummary(self.executionId)
'''
Created on Jun 5, 2015

@author: savitha.p
'''
import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout, clients, storage, backups, jobs, inventory
from testdata import backups_testdata, clients_testdata, storage_testdata, archive_testdata, inventory_testdata
from verificationsript import varchive, vbackups, vclients, vstorage

class MQ1496_PutHyperVIncrementalArchive(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Create New  Incremental Archive for HyperV")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\t PUT - HyperV Incremental Archive")
        self.executionId = exeID
    
    def runTest(self):
        try:            
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageAddedFlag = 0            
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
            
            if(storageId == 0):
                storageAddedFlag = 1
                logHandler.logging.info("Cloud Storage not available. Adding the same...")
                actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
                reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                logHandler.logging.info("Cloud storage " + cloudStorageName + " is added")
                
                storageId = vstorage.getStorageIdByName(cloudStorageName)
                logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
                if (storageId):
                    logHandler.logging.info("Storage added successfully...")
                    reportController.addTestStepResult("Add Storage as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add cloud storage")
                
                logHandler.logging.info("Thread sleep for 30 sec")
                time.sleep(30)
            
            cid = vclients.getSpecificClientByNameDB(clients_testdata.name)
            logHandler.logging.info("Client ID from DB: " + str(cid))
            
            if(cid==0):
                # Step 1: Send request to update a client
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                logHandler.logging.info("Actual response for Add client: " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
               
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client from response: " + str(cid))
                # Verification 1: verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully...")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
                
                            
            result = inventory.waitForInventorySync(token, 120, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            #Check Full backups
            fullBackups = vbackups.getFullHyperVBackupsFromDB(cid, instanceId)
            logHandler.logging.info("HyperV Full backups returned from DB: " + str(fullBackups))
            
            if(len(fullBackups) == 0):
                #Create Full Backup
                actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
                reportController.addTestStepResult("Send API request for Creating new HyperV Backup" , ExecutionStatus.PASS)
        
                # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
                responseResult = backups.getPutRequestResponse(actualResponse)
                strJobId = '-1'
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID from Response: " + str(strJobId))
                if(strJobId == '-1'):
                    raise Exception("Failed to trigger backup")
        
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                if(backupJobStatus!="Successful"):
                    raise Exception("HyperV Full Backup not successful")
                else:
                    logHandler.logging.info("Backup successful")
                    reportController.addTestStepResult("HyperV Full Backup" , ExecutionStatus.PASS)
            
                bid=vbackups.getBackupId(strJobId)
                logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
            
                logHandler.logging.info("Thread sleep for 60 sec..")
                time.sleep(60)
            
               
            #Check Incremental Backups
            incrBackups = vbackups.getIncrementalHyperVBackupsFromDB(cid, instanceId)
            logHandler.logging.info("HyperV Incremental backups returned from DB: " + str(incrBackups))
            
            if(len(incrBackups) == 0 or incrBackups < fullBackups):                
                #Create Incremental Backup
                actualResponse = backups.putHypervInstanceBackup(token,instanceId,"Incremental",backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
                reportController.addTestStepResult("Send API request for Incremental HyperV backup" , ExecutionStatus.PASS)
                
                #Response verification
                responseResult = backups.getPutRequestResponse(actualResponse)
                strJobId = '-1'
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID from Response: " + str(strJobId))
                if(strJobId == '-1'):
                    raise Exception("Failed to trigger incremental backup")
                
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                self.assertEqual(backupJobStatus, "Successful", "Incremental Backup not successful")
                logHandler.logging.info("Incremental backup successful...")
            
            #Erase Cloud Storage
            mediaLabel = archive_testdata.cloud_archive_label
            actualResponse = archive.putPrepareArchiveMedia(token, cloudStorageName, mediaLabel)
            reportController.addTestStepResult("Send API request for Archive Media Prepare" , ExecutionStatus.PASS)
        
            #Verification 1: Verify the response parameter   
            responseResult = archive.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
        
            dbResult = varchive.getArchivePrepareStatusFromDB(cloudStorageName)
            logHandler.logging.info("DB Result: " + str(dbResult))
        
            if(dbResult == 0):
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Failed database verification - some records found in archive sets table")            
            
            #Verify if the cloud storage is Online. If not, put the storage Online.
            onlineStatus = ''
            onlineStatus = vstorage.getOnlineStatusFromDB(storageId)
            logHandler.logging.info("Online status returned from DB: " + str(onlineStatus))
            
            if(onlineStatus == 'True'):
                logHandler.logging.info(str(cloudStorageName) + " Cloud Storage is already online..")
                reportController.addTestStepResult("Cloud storage already online", ExecutionStatus.PASS)
                
            else:
                #Send API request
                actualResponse = storage.putOnlineStorage(storage_testdata.sid, token, storageId)
                reportController.addTestStepResult("Send API request for Put cloud storage online" , ExecutionStatus.PASS)
            
                responseResult = storage.getPutRequestResponse(actualResponse)

                # Verification 1: verify the response parameter
                if (responseResult == "Pass"):
                    logHandler.logging.info("Storage made online successfully..")
                    reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed response verification")
                
                logHandler.logging.info("Thread sleep for 15 sec")
                time.sleep(15)
            
            #Send API Request for Creating Incremental Archive
            actualResponse = archive.putArchive(token, instanceId, cloudStorageName, "Incremental")
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            jobNo = varchive.verifyPutArchiveResponse(actualResponse)
            logHandler.logging.info("Create Archive successful. Job Number: " + str(jobNo))
            self.assertNotEqual(jobNo,"","Response verification failed")                        
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #Check the status of archive job
            archiveJobStatus=jobs.waitForJobCompletion("Archive", backups_testdata.waittime, token, jobNo)
            if(archiveJobStatus!="Successful"):
                raise Exception("HyperV Full and Incremental Archive not successful")
            else:
                logHandler.logging.info("Archive successful")
                reportController.addTestStepResult("HyperV Full and Incremental Archive" , ExecutionStatus.PASS)
                
            #DB verification
            archiveBackupNos = varchive.getIncrementalArchiveBackupNosFromDB(instanceId)
            archiveBackupNos.sort()
            logHandler.logging.info("Backup Nos associated with Archives returned from DB: " + str(archiveBackupNos))
            
            hyperVBackupNos = vbackups.getHyperVBackupsFromDB(cid, instanceId)
            hyperVBackupNos.sort()
            logHandler.logging.info("HyperV Backup Nos returned from DB: " + str(hyperVBackupNos))
            
            self.assertListEqual(archiveBackupNos, hyperVBackupNos, "Database verification")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
            #Wait for Job completion
#             jobStatus = jobs.waitForScheduleJobCompletion("Archive", 1200, token, jobNo)
#             logHandler.logging.info("Job Status : " + str(jobStatus))
#             self.assertEqual(jobStatus,"No jobs","Job still available in Active Jobs")
            
            #Cleanup
#             clients.deleteClients(token, cid)
#             logHandler.logging.info("Client deleted..")
#             reportController.addTestStepResult("Delete Client" , ExecutionStatus.PASS)

            if(storageAddedFlag):
                storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Delete cloud storage" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)        
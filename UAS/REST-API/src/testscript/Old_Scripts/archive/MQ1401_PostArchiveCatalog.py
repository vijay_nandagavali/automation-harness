'''
Created on May 5, 2015

@author: Savitha Peri
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout 
from testdata import archive_testdata
from verificationsript import varchive

class MQ1401_PostArchiveCatalog(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Post Archive Catalog")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Archive catalog")
        self.executionId = exeID
    
    def runTest(self):
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Send API Request
            actualResponse = archive.postArchiveCatalog(token, archive_testdata.media_name)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            actualResponseParams = varchive.getPostArchiveCatalogResponseParams(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                        
            archive_testdata.postArchiveCatalogResponseParamter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.postArchiveCatalogResponseParamter))
            
            self.assertListEqual(archive_testdata.postArchiveCatalogResponseParamter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.postArchiveCatalogResponseParamter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #DB verification
            dataFromDB = varchive.getPostArchiveCatalogDataFromDB(archive_testdata.media_name)
            logHandler.logging.info("Data from DB: " + str(dataFromDB))
            
            dataFromResponse = varchive.getPostArchiveCatalogFromResponse(actualResponse)
            logHandler.logging.info("Data from Response: " + str(dataFromResponse))
            
            if(dataFromDB == dataFromResponse):
                logHandler.logging.info("Database verification successful..")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Database verification failed")
                            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)        
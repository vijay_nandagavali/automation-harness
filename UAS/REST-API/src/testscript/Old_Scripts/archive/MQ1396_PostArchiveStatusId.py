'''
Created on Apr 30, 2015

@author: Savitha Peri
'''
import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout, clients, inventory, jobs, storage
from testdata import archive_testdata, clients_testdata, backups_testdata, storage_testdata, inventory_testdata
from verificationsript import varchive, vclients, vbackups, vstorage

class MQ1396_PostArchiveStatusId(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Post Archive Status By ID")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Archive status By ID")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #verify if any archives are available in DB
            archiveCount = varchive.getArchiveCountFromDB()
            logHandler.logging.info("Archive count returned from DB - " + str(archiveCount))
            
            clientAddedFlag = 0
            storageAddedFlag = 0            
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
            
            if(storageId == 0):
                logHandler.logging.info("Cloud Storage not available. Adding the same...")
                actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
                reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                logHandler.logging.info("Cloud storage " + cloudStorageName + " is added")
                
                storageId = vstorage.getStorageIdByName(cloudStorageName)
                logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
                if (storageId):
                    logHandler.logging.info("Storage added successfully...")
                    reportController.addTestStepResult("Add Storage as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add cloud storage")
                
                storageAddedFlag = 1                                
                logHandler.logging.info("Thread sleep for 30 sec")
                time.sleep(30)
            
            #verify if any archives are available in DB
            archiveCount = varchive.getArchiveCountFromDB()
            logHandler.logging.info("Archive count returned from DB - " + str(archiveCount))
            
            if(archiveCount == 0):
                logHandler.logging.info("No archives available in DB. Creating the same..")
                cid = vclients.getSpecificClientByNameDB(clientName)
                logHandler.logging.info("Client ID from DB: " + str(cid))
                
                if(cid==0):
                    # Step 1: Send request to update a client
                    actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                    logHandler.logging.info("Actual response for Add client: " + str(actualResponse))
                    reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                   
                    # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                    cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                    logHandler.logging.info("Client from response: " + str(cid))
                    # Verification 1: verify the response parameter
                    if (cid):
                        logHandler.logging.info("Client added successfully...")
                        reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                    else:
                        raise Exception ("Failed to add a client")
                    clientAddedFlag = 1
                                        
                result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
                logHandler.logging.info("Result: " + str(result))
                self.assertNotEqual(result,"Fail","Inventory sync failed")
                reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                
                instanceId = vbackups.getInstanceIdFromVm(instanceName)
                logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
                self.assertNotEqual(instanceId,None,"Instance Id not found")
                
                #Check if the storage is online
                onlineStatus = ''
                onlineStatus = vstorage.getOnlineStatusFromDB(storageId)
                logHandler.logging.info("Online status returned from DB: " + str(onlineStatus))
                dbResult = ''
            
                if(onlineStatus == 'True'):
                    logHandler.logging.info(str(cloudStorageName) + " Cloud Storage is already online..")
                    reportController.addTestStepResult("Cloud storage already online", ExecutionStatus.PASS)                
                else:
                    #Send API request
                    actualResponse = storage.putOnlineStorage(storage_testdata.sid, token, storageId)
                    reportController.addTestStepResult("Send API request for Put cloud storage online" , ExecutionStatus.PASS)
                
                    dbResult = vstorage.getOnlineStatusFromDB(storageId)
                    logHandler.logging.info("Online status returned from DB: " + str(dbResult))
                    if (dbResult == 'True'):
                        logHandler.logging.info("Database verification successful")
                        reportController.addTestStepResult("Cloud Storage Online", ExecutionStatus.PASS)
                    else:
                        raise Exception("Cloud Storage Offline")
                    
                #Erase Cloud Storage
                mediaLabel = archive_testdata.cloud_archive_label
                actualResponse = archive.putPrepareArchiveMedia(token, cloudStorageName, mediaLabel)
                reportController.addTestStepResult("Send API request for Archive Media Prepare" , ExecutionStatus.PASS)
            
                #Verification 1: Verify the response parameter   
                responseResult = archive.getPutRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))
            
                dbResult = varchive.getArchivePrepareStatusFromDB(cloudStorageName)
                logHandler.logging.info("DB Result: " + str(dbResult))
            
                if(dbResult == 0):
                    reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
                else:
                    raise Exception("Failed database verification - some records found in archive sets table")  
            
                #Send API Request
                actualResponse = archive.putArchive(token, instanceId, cloudStorageName, "Full")
                reportController.addTestStepResult("Send API request for Create Archive" , ExecutionStatus.PASS)
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                
                #Parse the actual response
                jobNo = varchive.verifyPutArchiveResponse(actualResponse)
                logHandler.logging.info("Create Archive successful. Job Number: " + str(jobNo))                        
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                
                archiveJobStatus = jobs.waitForJobCompletion(archive_testdata.archiveJobType, archive_testdata.archiveWaitTime, token, jobNo)
                logHandler.logging.info("Archive job status: " + str(archiveJobStatus))
                
                if(archiveJobStatus!="Successful"):
                    reportController.addTestStepResult("Archive not successful", ExecutionStatus.FAIL)
                    raise Exception("Archive not successful")
                else:
                    logHandler.logging.info("Archive successful..")
                    reportController.addTestStepResult("Archive successful" , ExecutionStatus.PASS)
                
                #DB verification
                archiveId = varchive.verifyPutArchiveDB(jobNo)
                logHandler.logging.info("Archive Id returned from DB: " + str(archiveId))
                self.assertNotEqual(archiveId, "", "Database verification")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
            archiveSetId = varchive.getArchiveSetIdFromDB()
            logHandler.logging.info("Archive Set Id returned from DB: " + str(archiveSetId))
            #Send API Request
            actualResponse = archive.postArchiveStatusId(token, archiveSetId)
            reportController.addTestStepResult("Send API request for Post Archive Status by ID" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            actualResponseParams = varchive.getArchiveStatusResponseParams(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                        
            archive_testdata.archiveStatusResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveStatusResponseParameter))
            
            self.assertListEqual(archive_testdata.archiveStatusResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.archiveStatusResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #DB verification
            archiveDataFromDB = varchive.getSpecificArchiveDataFromDB(archiveSetId)
            logHandler.logging.info("Archive data from DB - " + str(archiveDataFromDB))
            
            archiveDataFromResponse = varchive.getArchiveDataFromResponse(actualResponse)
            logHandler.logging.info("Archive data from Response - " + str(archiveDataFromResponse)) 
             
            self.assertListEqual(archiveDataFromDB, archiveDataFromResponse, "Database verification failed")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
                
#             if(clientAddedFlag):
#                 clients.deleteClients(token, cid)
#                 logHandler.logging.info("Client that is added in this test has been deleted..")
#                 reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
            
        finally:
            #Cleanup
            if(storageAddedFlag == 1 or result == 'Fail'):
                storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Delete cloud storage" , ExecutionStatus.PASS)

            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)        
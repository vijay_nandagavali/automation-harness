'''
Created on Apr 28, 2015

@author: Savitha Peri
'''

import unittest
from util import logHandler, reportController, apiHelper
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout 
from testdata import archive_testdata
from verificationsript import varchive


class MQ1397_GetArchiveFilesFromSpecifiedArchive(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Get Archive files from specified archive")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Archive files from specified archive")
        self.executionId = exeID
    
    def runTest(self):
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            #print token
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #verify if any file level archives are available in DB
            flArchiveCount = varchive.getFileLevelArchiveCountFromDB()
            logHandler.logging.info("File level Archive count returned from DB - " + str(flArchiveCount))
            
            if(flArchiveCount == 0):
                logHandler.logging.error("No file level archives available in DB")
                raise Exception("File level archives not available in DB")
            
            archiveId = varchive.getFileLevelArchiveIdFromDB()
            logHandler.logging.info("File level Archive Id returned from DB - " + str(archiveId))
            
            #Send API Request
            actualResponse = archive.getArchiveFilesFromSpecifiedArchive(token, archiveId)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Verify the response parameters
            actualResponseParams = varchive.getArchiveFilesActualResponseParameters(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                        
            archive_testdata.archiveFilesResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveFilesResponseParameter))
            self.assertListEqual(archive_testdata.archiveFilesResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.archiveFilesResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            #print e
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)        
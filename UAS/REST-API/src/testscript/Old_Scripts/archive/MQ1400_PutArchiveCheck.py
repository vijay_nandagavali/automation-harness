'''
Created on May 5, 2015

@author: Savitha Peri
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout, clients, inventory
from testdata import archive_testdata, clients_testdata, inventory_testdata
from verificationsript import varchive, vbackups, vclients

class MQ1400_PutArchiveCheck(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Put Archive Check")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\t PUT - Archive Check")
        self.executionId = exeID
    
    def runTest(self):
        try:            
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            cid = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID from DB: " + str(cid))
            clientAddedFlag = 0
            if(cid==0):
                clientAddedFlag = 1
                # Step 1: Send request to update a client
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                logHandler.logging.info("Actual response for Add client: " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
               
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client from response: " + str(cid))
                # Verification 1: verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully...")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
                
            result = inventory.waitForInventorySync(token, 120, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")

            #Send API Request
            actualResponse = archive.putArchiveCheck(token, instanceId, archive_testdata.media_name, "Full")
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            
            #Parse the actual response
            actualResponseParams = varchive.verifyPutArchiveCheckResponseParams(actualResponse)
            logHandler.logging.info("Actual response parameters: " + str(actualResponseParams))
            
            archive_testdata.archiveCheckResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(archive_testdata.archiveCheckResponseParameter))
            
            self.assertListEqual(archive_testdata.archiveCheckResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_testdata.archiveCheckResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #Cleanup
#             if(clientAddedFlag):
#                 clients.deleteClients(token, cid)
#                 logHandler.logging.info("Client that is added in this test has been deleted..")
#                 reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
'''
Created on Sep 17, 2014

@author: Amey.k
'''
import unittest

from resourcelibrary import audits, login, logout
from testdata import audits_testdata
from util import reportController, logHandler
from verificationsript import vaudits
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1130_GetAuditHistory(unittest.TestCase):
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get the audit history")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Audit History")
        self.executionId = exeID
        
    def runTest(self):                
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = audits.getAllAudits(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            # Get actual response parameter from response
            actualResponseParams = audits.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            if(len(actualResponseParams)>1):
                # sort expected response parameter list            
                audits_testdata.responseParameter.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(audits_testdata.responseParameter))
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(audits_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(audits_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                print "before audits fromDB"
                # Verification 2: Verify get all alerts API response        
                auditsFromDB = vaudits.getAuditsFromDB2(actualResponse)
                logHandler.logging.info("Data returned from DB: " + str(auditsFromDB))
                print "Data returned from DB: " + str(auditsFromDB)
            
                auditsFromResponse = vaudits.getAuditsFromResponse(actualResponse)
                logHandler.logging.info("Data from response: " + str(auditsFromResponse))
                print "Data from response: " + str(auditsFromResponse)
            
                self.assertListEqual(auditsFromDB, auditsFromResponse, "Failed Data base verification: Expected Response [" + str(auditsFromDB) + "] and Actual Response [" + str(auditsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Audit history was not found", ExecutionStatus.PASS)
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
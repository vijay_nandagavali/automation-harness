'''
Created on Mar 10, 2015

@author: Amey.k
'''

import unittest, time
from resourcelibrary import restore, login, logout, clients, jobs, joborders, inventory
from testdata import restore_testdata, clients_testdata, backups_testdata, joborders_testdata, inventory_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vclients, vbackups, vjoborders, vjobs
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1302_FileLevelRestoreHyperv(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "End-to-end restore scenario")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tEnd to End Scenario - File Level Restore HyperV")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            
            '''Setting pre-requisites like adding client,taking backup and monitoring it'''
            clientId = vclients.getSpecificClientByNameDB(clients_testdata.name)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            clientAdded = 0
            if(clientId==0):
                actualResponse = clients.postClient(token,clients_testdata.sid, clients_testdata.name, clients_testdata.os_type, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clients_testdata.ip, 
                                                    clients_testdata.existing_credential)
                logHandler.logging.info("Actual response for Add client: " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
               
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client from response: " + str(clientId))
                # Verification 1: verify the response parameter
                if (clientId):
                    logHandler.logging.info("Client added successfully...")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
                clientAdded = 1
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromVm(clients_testdata.instance_name)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
                    
            joid=vjoborders.findJobOrderByName(joborders_testdata.name)
            logHandler.logging.info("JOID returned from DB: " + str(joid))
            if(joid):
                joid = str(joid)+"b"
                joborders.deleteJoborders(token, joid)
                reportController.addTestStepResult("Delete the schedule" , ExecutionStatus.PASS)
                logHandler.logging.info("Thread sleep for 60 sec")
                time.sleep(60)
            
            instanceIdList = []
            instanceIdList.append(instanceId)
            
            mcDateTime = vjoborders.getDateTimeFromMachine()
            logHandler.logging.info("Machine date time: " + str(mcDateTime))
                        
            start_date = mcDateTime[0]
            start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
            logHandler.logging.info("Start Date: " + str(start_date))
            logHandler.logging.info("Start Time: " + str(start_time))

            # Step 1: Send request to get all alerts
            actualResponse = joborders.createNewJoborder(token, joborders_testdata.name, joborders_testdata.type, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)

            # Get actual response parameter from response
            joid = joborders.getPostRequestResponse(actualResponse)
            logHandler.logging.info("JOID returned from response: " + str(joid))
            if (joid): 
                reportController.addTestStepResult("Joborder Created Successfully", ExecutionStatus.PASS) 
            else:
                raise Exception("Failed to create Joborder")
            
            # Verify the response values against DB              
            jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(joid).replace('b', ""))
            logHandler.logging.info("JOID count from DB: " + str(jorderCountFromDB))
            if (jorderCountFromDB !=0):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database Verification", ExecutionStatus.PASS)
            else:
                raise Exception ("Joborder not created")
                    
            joid = str(joid).replace('b', '')
            
            logHandler.logging.info("Thread sleep for 120 sec..")
            time.sleep(120)
            
            strJobId = vjobs.getJobIdFromJoborder(joid)
            logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
            if(strJobId =='0'):
                raise Exception("Failed to trigger backup")   
                
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            #print backupJobStatus
            logHandler.logging.info("Backup job status: " + str(backupJobStatus))
            
            if(backupJobStatus!="Successful"):
                raise Exception("Backup not successful")
                
            else:
                logHandler.logging.info("Backup successful..")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)   
            
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
            
            logHandler.logging.info("Thread sleep for 60 sec")
            time.sleep(60)
            
            #Create File level restore
            actualResponse = restore.createHypervFileLevelRestore(token,str(bid) , restore_testdata.sid)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            responseParse = restore.postResponseParse(actualResponse)
            logHandler.logging.info("Response Parse: " + str(responseParse))
            if (responseParse == "Pass"):
                reportController.addTestStepResult("Restore job triggered successfully", ExecutionStatus.PASS)
            else:
                raise Exception ("Restore job did not get triggered")                     
 
            #Check the status of restore - commented out as the check fails consistently
            #restoreJobStatus=jobs.waitForJobCompletion(restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
            #if(restoreJobStatus!="Successful"):
            #    raise Exception("Restore not successful..")
            #else:
            #    logHandler.logging.info("Restore successful")
            #    reportController.addTestStepResult("Hyper-V VM Restore successful" , ExecutionStatus.PASS)
            
            # Cleanup
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
                      
            joid = str(joid)+"b"
            joborders.deleteJoborders(token, joid)
            logHandler.logging.info("Job order created in this test has been deleted..")
            reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)
                       
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
        
        finally:
            #Delete client
            if(clientAdded == 1 or result == 'Fail'):                
                clients.deleteClients(token, clientId) 
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
             
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
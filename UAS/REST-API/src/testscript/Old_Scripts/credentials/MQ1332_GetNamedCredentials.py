'''
Created on Jan 27, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import credentials, login, logout
from testdata import credentials_testdata
from util import reportController
from verificationsript import vcredentials
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1332_GetNamedCredentials(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Named Credentials")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            cred = vcredentials.getAddedCredentialFromDB(credentials_testdata.credentialDisplayname)
            if (len(cred) == 0):
                credentials.createNewCredentials(token, credentials_testdata.credentialDisplayname, credentials_testdata.credentialUsername, credentials_testdata.credentialPassword, credentials_testdata.credentialDomain, credentials_testdata.isDefaultCredentialNamed)
                cred = vcredentials.getAddedCredentialFromDB(credentials_testdata.credentialDisplayname)
            actualResponse = credentials.getNamedCredentials(token, credentials_testdata.sid)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = credentials.getListOfActualResponseParameter(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            credentials_testdata.responseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(credentials_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(credentials_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response
            count = vcredentials.getCredentialsCountFromDB()       
            credFromDB = vcredentials.getClientCredentialsFromDB()
            #del credFromDB[0]
            #del credFromDB[0]
            print credFromDB
            credFromResponse = vcredentials.getCredentialsFromResponse(actualResponse, count)
            print credFromResponse
            self.assertListEqual(credFromDB, credFromResponse, "Failed Data base verification: Expected Response [" + str(credFromDB) + "] and Actual Response [" + str(credFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            credentials.deleteCredentials(token, str(cred[0]))
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
'''
Created on Jul 20, 2015

@author: rahula
'''

import unittest

from resourcelibrary import credentials, login, logout
from testdata import credentials_testdata
from util import reportController
from verificationsript import vcredentials
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1728_GetCredentialsUsingCredentialId(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Client Credentials for a specific credential id")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Get credentials from DB
            cred = vcredentials.getAddedCredentialFromDB(credentials_testdata.credentialDisplayname)
            if (len(cred) == 0):
                credentials.createNewCredentials(token, credentials_testdata.credentialDisplayname, credentials_testdata.credentialUsername, credentials_testdata.credentialPassword, credentials_testdata.credentialDomain, credentials_testdata.isDefaultCredential)
                cred = vcredentials.getAddedCredentialFromDB(credentials_testdata.credentialDisplayname)
            
            #Get credential id from DB
            credId = vcredentials.getCredentialIdFromDB()
            #Send API request
            actualResponse = credentials.getClientCredentialsForSpecificId(token, credId)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = credentials.getListOfActualResponseParameter(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            credentials_testdata.responseParameter_MQ1728.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(credentials_testdata.responseParameter_MQ1728, actualResponseParams, "Failed Response verification: Expected response [" + str(credentials_testdata.responseParameter_MQ1728) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response
            #count = vcredentials.getCredentialsCountFromDB()       
            credFromDB = vcredentials.getSpecificClientCredentialsFromDB(credId)
            print credFromDB
            #del credFromDB[0]
            #del credFromDB[0]
            credFromResponse = vcredentials.getSpecificCredentialsFromResponse(actualResponse)
            print credFromResponse
            self.assertListEqual(credFromDB, credFromResponse, "Failed Data base verification: Expected Response [" + str(credFromDB) + "] and Actual Response [" + str(credFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            #Cleanup
            credentials.deleteCredentials(token, credId)
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
             
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
            
        finally:
            #Logout 
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
'''
Created on Feb 2, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import credentials, login, logout
from testdata import credentials_testdata
from util import reportController
from verificationsript import vcredentials
from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1338_RemoveClientCredentialAssociation(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Remove Client Credential Association")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            cred = vcredentials.getAddedCredentialFromDB(credentials_testdata.credentialDisplayname)
            if (len(cred) == 0):
                credentials.createNewCredentials(token, credentials_testdata.credentialDisplayname, credentials_testdata.credentialUsername, credentials_testdata.credentialPassword, credentials_testdata.credentialDomain, credentials_testdata.isDefaultCredential)
                cred = vcredentials.getAddedCredentialFromDB(credentials_testdata.credentialDisplayname)

            actualResponse = credentials.removeClientCredentialAssociation(token, credentials_testdata.sid, credentials_testdata.clientId, credentials_testdata.appId)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            responseResult = credentials.getDeleteRequestResponse(actualResponse)
            print responseResult
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            # Verification 2: Verify get all alerts API response
            cred.sort();      
            credFromDB = vcredentials.getClientCredentialAssociationFromDB(str(cred[0]))
            print credFromDB
            if (len(credFromDB) == 0):
                reportController.addTestStepResult("Credential binding with client removed successfully", ExecutionStatus.PASS)
            else:
                raise Exception ("Credential binding with client removal failed")
            
            credentials.deleteCredentials(token, str(cred[1]))
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
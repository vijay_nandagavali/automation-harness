'''
Created on Jan 08, 2016

@author: Prateek
'''

import unittest

from resourcelibrary import login
from testdata import login_testdata
from util import reportController, logHandler
from verificationsript import vlogin
from config.Constants import ExecutionStatus
import json


class MQ1544_GetAuthenticationToken(unittest.TestCase):
   
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get authentication token")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Get authentication token")
        self.executionId = exeID
        
        
    def getAuthTokenAfterLogin(self):
        actualResponse = login.getAuthenticationToken()
        print actualResponse
        responseData = json.loads(actualResponse)
        auth_token = responseData['auth_token']
        if(auth_token != ""):
            print "Valid Auth Token received"
            print "Login Successful"
            reportController.addTestStepResult("Login", "Pass")
        else:
            reportController.addTestStepResult("Login", "Fail")
        return auth_token
        
    def runTest(self):        
        try:
            
            # Step 1: Send request to get all alerts
            count = vlogin.getPreAuthTokenVerificationDBCount()
            
            logHandler.logging.info("UUID count before login is called is: " + str(count))
            reportController.addTestStepResult("Pre AuthToken Verification from DB", ExecutionStatus.PASS)
            
            actualResponse = login.getAuthenticationToken()
            
            logHandler.logging.info("actual response: " + str(actualResponse))
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = login.getActualResponseParameter(actualResponse)
                       
            logHandler.logging.info("actual response parameters: " + str(actualResponseParams))
            # sort expected response parameter list            
            login_testdata.responseParameter.sort()
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(login_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(login_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            # Verification 2: Verify get all alerts API response        
            ListFromDB = vlogin.getAuthTokenVerificationDB(count)
            # jobsFromDB.sort()
            
            logHandler.logging.info("list from DB: " + str(ListFromDB))
             
            ListFromResponse = vlogin.getAuthTokenResponseVerification(actualResponse)
            # jobsFromResponse.sort()
            logHandler.logging.info("list from response: " + str(ListFromResponse))
            self.assertListEqual(ListFromDB, ListFromResponse, "Failed Data base verification: Expected Response [" + str(ListFromDB) + "] and Actual Response [" + str(ListFromResponse) + "]")
            reportController.addTestStepResult("DB verification", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
        except Exception as e:
            logHandler.logging.error("Exception: "+str(e))
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)   
        finally:
            reportController.generateReportSummary(self.executionId)
# alert = UEB869_GetAuthenticationToken()
# alert.runTest()

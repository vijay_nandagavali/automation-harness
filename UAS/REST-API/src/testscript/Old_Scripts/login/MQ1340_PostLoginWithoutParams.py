'''
Created on Sep 25, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import login
from testdata import login_testdata
from util import reportController
from verificationsript import vlogin
from config.Constants import ExecutionStatus


class MQ1340_PostLoginWithoutParams(unittest.TestCase):
    
   
    def __init__(self, exeID):       
        reportController.initializeReport(type(self).__name__, "Get Login response with blank parameters")
        self.executionId = exeID
        

    def runTest(self):        
        try:
            
            # Step 1: Send request to get all alerts
            count = vlogin.getPreAuthTokenVerificationDBCount()
            print "UUID count before login is called is: " + str(count)
            actualResponse = login.getLoginResponse()
            print actualResponse
            # Get actual response parameter from response
            actualResponseParams = login.getActualResponseParameter(actualResponse)
            print actualResponseParams            
            # sort expected response parameter list            
            
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(login_testdata.InvalidResponseMessage, actualResponseParams, "Failed Response verification: Expected response [" + str(login_testdata.InvalidResponseMessage) + "] and Actual parameter [" + str(actualResponseParams) + "]")
        
            reportController.addTestStepResult("Login with blank parameters", "Pass")
            reportController.tcExecutionStatus(ExecutionStatus.PASS)

            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
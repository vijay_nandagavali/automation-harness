'''
Created on Jul 9, 2015

@author: rahula
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1704_PostRetentionAssociationBasedOnInstanceId(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Post Retention Settings with instance_id only")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Retention Settings with instance_id only")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                            
            #get Instance ID from Database
            dbInstanceIdResult = vpolicy.getInstanceIdFromDB();
            logHandler.logging.info("Instance Id from database: " + str(dbInstanceIdResult))
            reportController.addTestStepResult("Get Instance_Id From Database ", ExecutionStatus.PASS)
            
            #Send API Request to associate the policy
            actualPostResponse = policy.postPolicyAssocation(token, dbInstanceIdResult, 0)
            logHandler.logging.info("Actual Response: " + str(actualPostResponse))
            self.assertTrue(actualPostResponse[1] == 500 , "Invalid POST Succeeded")
            reportController.addTestStepResult("Send API Request- POST policy association", ExecutionStatus.PASS)
            
            #Verify response
            responseResult = vpolicy.verifyPostPolicymessage(actualPostResponse[0])
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertTrue(responseResult == policy_testdata.expectedErrorMessageForMQ1704,"Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.verifyPostPolicyAssocationCountFromDB(0,0)
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertTrue(dbResult == 0,"Database verification failed")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)         
                
                   
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        
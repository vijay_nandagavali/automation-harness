'''
Created on Jun 5, 2015

@author: rahula
'''

import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1492_PutYearsPolicyDecreaseYears(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Decrease existing policy by id of year_policy")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\PUT decrease existing policy by id ({id}) of year_policy")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
              
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameGeneric)
            logHandler.logging.info("Policy Id from database for policy Name " +str(policy_testdata.policyNameGeneric)+ ": " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result for deleting existing policy: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From DB By ID  ", ExecutionStatus.PASS)
            
            #Send API request
            actualResponse = policy.postPolicyGeneric(token, policy_testdata.policyNameGeneric, 0,0,0,5)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            self.assertTrue(actualResponse[1] == 201 , "POST was not successful")
            reportController.addTestStepResult("Send API Request- POST policy", ExecutionStatus.PASS)
            
            #Verify response
            responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("POST policy - Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.verifyPostPolicyCountFromDB(policy_testdata.policyNameGeneric)
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertNotEqual(dbResult,"0","Database verification failed")
            reportController.addTestStepResult("POST policy - Database verification", ExecutionStatus.PASS)        
                        
            #Fetch the policy ID from DB
            policyID = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameGeneric)            
            
            #Send a PUT request to decrease the number of years
            actualResponse = policy.putModifyPolicyById(token, policyID,policy_testdata.policyNameGeneric,policy_testdata.decreaseYearPolicyDays, policy_testdata.decreaseYearPolicyWeeks, policy_testdata.decreaseYearPolicyMonths, policy_testdata.decreaseYearPolicyYears,False)         
            logHandler.logging.info("actualResponse:  " + str(actualResponse))
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)    
                        
            #Verify response
            responseResult = vpolicy.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.getPolicyDetailsFromDB(policy_testdata.policyNameGeneric)
            expecteddatalist =[policy_testdata.decreaseYearPolicyDays, policy_testdata.decreaseYearPolicyWeeks, policy_testdata.decreaseYearPolicyMonths,policy_testdata.decreaseYearPolicyYears]
            expecteddatalist.sort()
            logHandler.logging.info("expected results: " + str(expecteddatalist))
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertListEqual(dbResult, expecteddatalist, "Failed Data base verification: Expected Response [" + str(expecteddatalist) + "] and Actual Response [" + str(dbResult) + "]")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            
            #cleanup
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameGeneric)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From Database By ID  ", ExecutionStatus.PASS)
                        
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
        
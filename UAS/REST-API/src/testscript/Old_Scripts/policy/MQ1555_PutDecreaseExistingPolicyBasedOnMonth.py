'''
Created on Jun 11, 2015

@author: garima.g
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1555_PutDecreaseExistingPolicyBasedOnMonth(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Decrease existing policy by id of month_policy")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\PUT decrease existing policy by id ({id}) of month_policy")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)             
            
            policyID = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameMonths)
            if(policyID == 0):
                #Pre-requisite - Send API request to post month based policy
                actualResponse = policy.postPolicyGeneric(token, policy_testdata.policyNameMonths, 0, 0, 11, 0)
                self.assertTrue(actualResponse[1] == 201 , "POST was not successful")
                reportController.addTestStepResult("Send POST API Request", ExecutionStatus.PASS)
                
                #Verify response
                responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
                logHandler.logging.info("Response result: " + str(responseResult))
                self.assertNotEqual(responseResult,"Fail","Response verification failed")
                reportController.addTestStepResult("POST Response verification", ExecutionStatus.PASS)
            
            
            #Fetch the policy ID from DB
            policyID = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameMonths)            
            
            #Send a PUT request to decrease the number of years
            actualResponse = policy.putModifyPolicyById(token, policyID, policy_testdata.policyNameMonths, policy_testdata.decreaseMonthPolicyDays, policy_testdata.decreaseMonthPolicyWeeks, policy_testdata.decreaseMonthPolicyMonths,policy_testdata.decreaseMonthPolicyYears,False) 
            logHandler.logging.info("actualResponse  " + str(actualResponse))           
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
                        
            #Verify response
            responseResult = vpolicy.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.getPolicyDetailsFromDB(policy_testdata.policyNameMonths)
            expecteddatalist =[policy_testdata.decreaseMonthPolicyDays, policy_testdata.decreaseMonthPolicyWeeks, policy_testdata.decreaseMonthPolicyMonths,policy_testdata.decreaseMonthPolicyYears]
            expecteddatalist.sort()
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertListEqual(dbResult, expecteddatalist, "Failed Data base verification: Expected Response [" + str(expecteddatalist) + "] and Actual Response [" + str(dbResult) + "]")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)                      
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            
            #Cleanup
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameMonths)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
                       
            #Delete  Policy  from DB 
            deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
            logHandler.logging.info("Response result: " + str(deleteResponse))            
            reportController.addTestStepResult("Delete Policy From Database By ID  ", ExecutionStatus.PASS)    
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        
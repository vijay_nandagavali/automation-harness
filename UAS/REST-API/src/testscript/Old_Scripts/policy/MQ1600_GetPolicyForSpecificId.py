'''
Created on Jun 19, 2015

@author: rahula
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1600_GetPolicyForSpecificId(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "GET information for a policy with specific id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET information for a policy with specific id")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameMonths)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From DB By ID  ", ExecutionStatus.PASS)
           
            #Pre-requsite - Send POST request to create a policy
            actualResponse = policy.postPolicyByMonths(token,policy_testdata.policyNameMonths )
            self.assertTrue(actualResponse[1] == 201 , "POST was not successful")  
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            #Verify response
            responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Pre-requisite - Policy creation", ExecutionStatus.PASS)
            
            #Get information for the specific policy created as a pre-requisite
            policyId = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameMonths)
            actualResponse = policy.getPolicyByID(token, policyId)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            #Verification 1 - verify response parameters
            actualResponseParams = policy.getListOfActualResponseParameter(actualResponse)          
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            policy_testdata.responseParameters.sort()  
                        
            logHandler.logging.info("Test data parameters - " + str(policy_testdata.responseParameters))       
            self.assertListEqual(policy_testdata.responseParameters, actualResponseParams, "Failed Response verification: Expected response [" + str(policy_testdata.responseParameters) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #Verification 2 - DB verification
            policyFromResponse = vpolicy.getPolicyInformationFromResponse(actualResponse)
           
            policyFromDB = vpolicy.getPolicyInformationFromDB(policyId)
            
            self.assertListEqual(policyFromDB, policyFromResponse, "Failed Data base verification: Expected Response [" + str(policyFromDB) + "] and Actual Response [" + str(policyFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameMonths)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Cleanup - Delete Policy From DB By ID  ", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)                    
           
            reportController.generateReportSummary(self.executionId)
        
'''
Created on Jun 16, 2015

@author: rahula
'''

import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler, jsonHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1579_DeletePolicyWithoutAssociatedId(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "DELETE - Policy without associated id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Policy without associated id")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Delete a policy without specifying associated id
            deleteResponse = policy.deletePolicyWithoutID(token)
            logHandler.logging.info("Response result: " + str(deleteResponse))            
            reportController.addTestStepResult("Delete Policy without associated id  ", ExecutionStatus.PASS)
            
            #Verify the response status code - expected code is 500
            responseStatusCode = deleteResponse[1]
            logHandler.logging.info("Expected response Status Code:" + str(responseStatusCode))
            if(responseStatusCode == 500):
                reportController.addTestStepResult("Response Status Code verification", ExecutionStatus.PASS)
            else:
                raise Exception("Failed Response Status Code verification - Expected - 500 + Actual" + str(responseStatusCode))    
            
            #Verify error message 
            errorMsg = jsonHandler.deleteResponseMessage(deleteResponse[0])
            logHandler.logging.info("Expected error message:" + str(policy_testdata.expectedErrorMessageMQ1579))
            logHandler.logging.info("Response Error Message:" + str(errorMsg))
            if(errorMsg == policy_testdata.expectedErrorMessageMQ1579):
                reportController.addTestStepResult("Expected error message verification", ExecutionStatus.PASS)
            else:
                raise Exception("Failed error message verification. Expected = " + str(policy_testdata.expectedErrorMessageMQ1579) + "Actual - " + str(errorMsg))
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)   
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
            
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)        
            reportController.generateReportSummary(self.executionId)
        
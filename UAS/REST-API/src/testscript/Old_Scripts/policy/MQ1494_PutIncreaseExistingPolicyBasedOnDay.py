'''
Created on Jun 5, 2015

@author: mangesh.b
'''


import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1494_PutIncreaseExistingPolicyBasedOnDay(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Increase Existing  policy based on Days")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Increase Existing Policy based on Days")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)

            #Prerequisite to add a New policy Based On Days 
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            logHandler.logging.info("Policy Id from database for policy Name " +str(policy_testdata.policyName)+ ": " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result for deleting existing policy: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From DB By ID  ", ExecutionStatus.PASS)
            
            #Send API request
            actualResponse = policy.postPolicyGeneric(token, policy_testdata.policyName, 30,0,0,0)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            self.assertTrue(actualResponse[1] == 201 , "POST was not successful")
            reportController.addTestStepResult("Send API Request- POST policy", ExecutionStatus.PASS)
            
            #Verify response
            responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("POST policy - Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.verifyPostPolicyCountFromDB(policy_testdata.policyName)
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertNotEqual(dbResult,"0","Database verification failed")
            reportController.addTestStepResult("POST policy - Database verification", ExecutionStatus.PASS)                    
            
            # Get Policy ID from DB 
            policyID = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            
            #Send PUT API request
            actualResponse = policy.putModifyPolicyById(token,policyID,policy_testdata.policyName, policy_testdata.increasepolicyDays, policy_testdata.policyWeeks, policy_testdata.policyMonths, policy_testdata.policyYears,False) 
            logHandler.logging.info("actualResponse:  " + str(actualResponse))
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)          
                      
            #Verify response
            responseResult = vpolicy.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response result for Put Request: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification for Put Request ", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.getPolicyDetailsFromDB(policy_testdata.policyName)
            expecteddatalist =[policy_testdata.increasepolicyDays, policy_testdata.policyWeeks, policy_testdata.policyMonths,policy_testdata.policyYears]
            expecteddatalist.sort()
            logHandler.logging.info("Expected result: "+ str(expecteddatalist))
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertListEqual(dbResult, expecteddatalist, "Failed Data base verification: Expected Response [" + str(expecteddatalist) + "] and Actual Response [" + str(dbResult) + "]")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            
            #cleanup
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From Database By ID  ", ExecutionStatus.PASS)
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        

'''
Created on Jul 1, 2015

@author: garima.g
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1686_GetRetentionPolicyBasedOnSid(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "GET Retention Policy Based on Sid")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET Retention Policy Based on Sid")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                            
            #Send GET API request
            actualResponse = policy.getPolicy(token)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = policy.getPolicyParametersFromJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters: " + str(actualResponseParams))
            if(len(actualResponseParams) > 0):
                actualResponseParams.sort() 
                policy_testdata.responseParameters.sort()
                                                
                # Verification 1: Verify get all policy response parameter        
                self.assertListEqual(policy_testdata.responseParameters, actualResponseParams, "Failed Response verification: Expected response [" + str(policy_testdata.responseParameters) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("No data Found", ExecutionStatus.PASS)
                 
            # Verification 2: Verify get all policy details from DB     
                
            PolicyFromDB = vpolicy.getAllPolicyInformationFromDB()
            logHandler.logging.info("Database Values: " + str(PolicyFromDB))        
#             
            policyFromResponse = vpolicy.getPolicydetailsFromResponse(actualResponse) 
            logHandler.logging.info("Actual Response Values:  " + str(policyFromResponse))   
            print policyFromResponse
#              
            self.assertListEqual(PolicyFromDB, policyFromResponse, "Failed Data base verification: Expected Response [" + str(PolicyFromDB) + "] and Actual Response [" + str(policyFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)   
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)    
              
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            
           
            reportController.generateReportSummary(self.executionId)

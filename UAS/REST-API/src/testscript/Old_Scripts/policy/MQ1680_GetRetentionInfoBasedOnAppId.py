'''
Created on Jun 29, 2015

@author: garima.g
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1680_GetRetentionInfoBasedOnAppId(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get Retention Information Based on App id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Retention Information Based on App Id")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            appId = vpolicy.GetAppIdFromDB()
             
            #Send GET API request
            actualResponse = policy.getRetention(token,"app_id",appId )
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = policy.getRetentionParametersFromJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters: " + str(actualResponseParams))
                                                
            # Verification 1: Verify get all replication strategy response parameter        
            self.assertListEqual(policy_testdata.RetentionInfoParameterList, actualResponseParams, "Failed Response verification: Expected response [" + str(policy_testdata.responseParmeterRetentionStrategy) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                 
            # Verification 2: Verify get all replication strategy from DB     
                
            PolicyAssoFromDB = vpolicy.getInstancePolicyAssociationFromDB("app_id",appId)
            logHandler.logging.info("Database Values(unsorted):  " + str(PolicyAssoFromDB))
            PolicyAssoFromDB.sort()              
            logHandler.logging.info("Database Values(sorted):  " + str(PolicyAssoFromDB))
#             
            policyAssoFromResponse = vpolicy.getInstancePolicyAssociationFromResponse(actualResponse) 
            logHandler.logging.info("Actual Response Values(unsorted):  " + str(policyAssoFromResponse))   
            policyAssoFromResponse.sort()       
            logHandler.logging.info("Actual Response Values(sorted):  " + str(policyAssoFromResponse))       
#              
            self.assertListEqual(PolicyAssoFromDB, policyAssoFromResponse, "Failed Data base verification: Expected Response [" + str(PolicyAssoFromDB) + "] and Actual Response [" + str(policyAssoFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)   
              
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
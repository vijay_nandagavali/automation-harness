'''
Created on Jul 8, 2015

@author: Garima.G
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1703_PostRetentionAssociationBasedOnPolicyId(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Post Retention Settings with policy_id only")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Retention Settings with policy_id only")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            policyAdded=False
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB("")
            logHandler.logging.info("Policy Id from database: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult == 0):        
                
                #Send API request
                actualResponse = policy.postPolicyGeneric(token,policy_testdata.policyName, 0,0,0,1)
                logHandler.logging.info("Actual Response: " + str(actualResponse))
                self.assertTrue(actualResponse[1] == 201 , "POST was not successful")
                reportController.addTestStepResult("Send API Request- POST policy", ExecutionStatus.PASS)
                
                #Verify response
                responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
                logHandler.logging.info("Response result: " + str(responseResult))
                self.assertNotEqual(responseResult,"Fail","Response verification failed")
                reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
                
                #DB verification
                dbResult = vpolicy.verifyPostPolicyCountFromDB(policy_testdata.policyName)
                logHandler.logging.info("DB result: " + str(dbResult))
                self.assertNotEqual(dbResult,"0","Database verification failed")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)  
                dbPolicyIdResult = vpolicy.getPolicyIDFromDB(token,policy_testdata.policyName)
                
                policyAdded = True
            
            #send post API Request
            if(dbPolicyIdResult != 0):
                actualPostResponse = policy.postPolicyAssocation(token,0, dbPolicyIdResult)
                logHandler.logging.info("Actual Response: " + str(actualPostResponse))
                self.assertTrue(actualPostResponse[1] == 500 , "Invalid POST Succeeded")
                reportController.addTestStepResult("Send API Request- POST policy association", ExecutionStatus.PASS)
                
                #Verify response
                responseResult = vpolicy.verifyPostPolicymessage(actualPostResponse[0])
                logHandler.logging.info("Response result: " + str(responseResult))
                self.assertTrue(responseResult == policy_testdata.expectedErrorMessageForMQ1703,"Response verification failed")
                reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
                
                #DB verification
                dbResult = vpolicy.verifyPostPolicyAssocationCountFromDB(0,0)
                logHandler.logging.info("DB result: " + str(dbResult))
                self.assertTrue(dbResult == 0,"Database verification failed")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)         
                
                   
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            #get policy_id from DB
            if(policyAdded == True):
                dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
                logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
                reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
                if(dbPolicyIdResult != 0):
                    #Delete  Policy  from DB 
                    deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                    logHandler.logging.info("Response result -DELETE policy: " + str(deleteResponse))            
                    reportController.addTestStepResult("Delete Policy From DB By ID  ", ExecutionStatus.PASS)
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        
'''
Created on Jun 4, 2015

@author: mangesh.b
'''

import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1487_PostPolicyBasedonDays(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Create New policy based on Days")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Policy based on Days")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            logHandler.logging.info("Policy Id from database for policy Name " +str(policy_testdata.policyName)+ ": " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result for deleting existing policy: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From DB By ID  ", ExecutionStatus.PASS)
            
            #Send API request
            actualResponse = policy.postPolicyGeneric(token, policy_testdata.policyName, 20,0,0,0)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            self.assertTrue(actualResponse[1] == 201 , "POST was not successful")
            reportController.addTestStepResult("Send API Request- POST policy", ExecutionStatus.PASS)
            
            #Verify response
            responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.verifyPostPolicyCountFromDB(policy_testdata.policyName)
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertNotEqual(dbResult,0,"Database verification failed")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)

            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            
            #cleanup
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result -DELETE policy: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From DB By ID  ", ExecutionStatus.PASS)
            
            #Logout            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        
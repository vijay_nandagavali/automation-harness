'''
Created on Jun 24, 2015

@author: garima.g
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout, jobs
from verificationsript import vpolicy

class MQ1658_DeleteRetentionWithoutID(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete Retention Information without id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Retention Information without Id")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                                   
            #Send DELETE request
            deleteResponse = policy.deleteRetention(token)
            logHandler.logging.info("Actual Response: " + str(deleteResponse))
            reportController.addTestStepResult("Delete Retention Information without id", ExecutionStatus.PASS)            
            
            #Verify the response status code - expected code is 500 (internal server error)          
            responseStatusCode = deleteResponse[1]
            logHandler.logging.info("Expected response Status Code:" + str(responseStatusCode))
            if(responseStatusCode == 500):
                reportController.addTestStepResult("Response Status Code verification", ExecutionStatus.PASS)
            else:
                raise Exception("Failed Response Status Code verification - Expected - 500 + Actual" + str(responseStatusCode))    
            
            #Verify error message 
            errorMsg = vpolicy.getDeleteMessage(deleteResponse[0])            
            logHandler.logging.info("Expected error message:" + str(policy_testdata.expectedErrorMessageMQ1658))
            logHandler.logging.info("Response Error Message:" + str(errorMsg))
            if(errorMsg == policy_testdata.expectedErrorMessageMQ1658):
                reportController.addTestStepResult("Expected error message verification", ExecutionStatus.PASS)
            else:
                raise Exception("Failed error message verification. Expected = " + str(policy_testdata.expectedErrorMessageMQ1658))
                          
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)


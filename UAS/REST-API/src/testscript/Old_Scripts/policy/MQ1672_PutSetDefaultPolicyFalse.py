'''
Created on Jun 26, 2015

@author: garima.g
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1672_PutSetDefaultPolicyFalse(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Modify an existing policy by id - set Is_default = False")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Modify an existing policy by id - Set Is_default = False")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #get policy_id from DB
            defaultPolicyName =  vpolicy.getDefaultPolicyFromDB()
            logHandler.logging.info("get default policy name DB result: " + str(defaultPolicyName))
            reportController.addTestStepResult("Get Default Policy From Database ", ExecutionStatus.PASS)
            
            if(defaultPolicyName != ""):                
                dbPolicyIdResult = vpolicy.getPolicyIDFromDB(defaultPolicyName)
                logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
                reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
                
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From DB By ID  ", ExecutionStatus.PASS)         
                    
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameForModify)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
                                               
            
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From DB By ID  ", ExecutionStatus.PASS)
           
            #Send API request
            actualResponse = policy.postPolicyGeneric(token,policy_testdata.policyNameForModify,7,1,0,0,True)
            self.assertTrue(actualResponse[1] == 201 , "POST was not successful")  
            reportController.addTestStepResult("Post Send API Request", ExecutionStatus.PASS)
            
            #Verify response
            responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Post Response verification failed")
            reportController.addTestStepResult("Post Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.verifyPostPolicyCountFromDB(policy_testdata.policyNameForModify)
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertNotEqual(dbResult,"0","Post Database verification failed")
            reportController.addTestStepResult("Post Database verification", ExecutionStatus.PASS)
            
            #Fetch the policy ID from DB
            policyID = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameForModify)
            logHandler.logging.info("policyID  " + str(policyID))
            
            #Send PUT API request
            actualResponse = policy.putModifyPolicyById(token,policyID,policy_testdata.policyNameForModify,policy_testdata.ModifyPolicyIsDefaultFalseDays,policy_testdata.ModifyPolicyIsDefaultFalseWeeks,policy_testdata.ModifyPolicyIsDefaultFalseMonths,policy_testdata.ModifyPolicyIsDefaultFalseYears,policy_testdata.ModifyPolicyIsDefaultFalseDefault)
            logHandler.logging.info("actualResponse  " + str(actualResponse))
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
                        
            #Verify response
            responseResult = vpolicy.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.getAllPolicyDetailsFromDB(policy_testdata.policyNameForModify)
            expecteddatalist =[policy_testdata.ModifyPolicyIsDefaultFalseDays,policy_testdata.ModifyPolicyIsDefaultFalseWeeks,policy_testdata.ModifyPolicyIsDefaultFalseMonths,policy_testdata.ModifyPolicyIsDefaultFalseYears,policy_testdata.ModifyPolicyIsDefaultFalseDefault]            
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertListEqual(dbResult, expecteddatalist, "Failed Data base verification: Expected Response [" + str(expecteddatalist) + "] and Actual Response [" + str(dbResult) + "]")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
  
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Cleanup
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameForModify)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
              
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From Database By ID  ", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
'''
Created on Jun 4, 2015

@author: Nikhil.S
'''

import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout, jobs
from verificationsript import vpolicy

class MQ1485_GetRetentionStrategyBySID(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Retention Strategy By SID")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Retention Strategy By SID")
        self.executionId = exeID
    
    def runTest(self):
        try:            
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                        
            #Send GET API request
            actualResponse = policy.getRetentionStrategy(token)
            logHandler.logging.info("actualResponse  " + str(actualResponse))
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            # Verification 1: Verify get all replication strategy response parameter   
            
            # Get actual response parameter from response
            actualResponseParams = policy.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("actual Parameters: " + str(actualResponseParams))                                
            logHandler.logging.info("expected Parameters: " + str(policy_testdata.responseParmeterRetentionStrategy))     
            self.assertListEqual(policy_testdata.responseParmeterRetentionStrategy, actualResponseParams, "Failed Response verification: Expected response [" + str(policy_testdata.responseParmeterRetentionStrategy) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                
            # Verification 2: Verify get all replication strategy from DB                                 
            strategyFromDB = vpolicy.verifyRetentionPolicyFromDB()    
            strategyFromDB = [strategyFromDB]
            logHandler.logging.info("strategy from DB: " + str(strategyFromDB))            
            strategyFromResponse = vpolicy.getRetentionStrategyFromResponse(actualResponse)    
            strategyFromResponse = [strategyFromResponse]
            logHandler.logging.info("strategy from Response: " + str(strategyFromResponse))             
            self.assertListEqual(strategyFromDB, strategyFromResponse, "Failed Data base verification: Expected Response [" + str(strategyFromDB) + "] and Actual Response [" + str(strategyFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)                 
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        
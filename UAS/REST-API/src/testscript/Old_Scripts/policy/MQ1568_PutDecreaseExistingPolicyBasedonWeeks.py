'''
Created on Jun 12, 2015

@author: rahula
'''

import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1568_PutDecreaseExistingPolicyBasedonWeeks(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Decrease Existing  policy based on Weeks")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDecrease existing Policy based on Weeks")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)

            #Prerequsite to add a New policy Based On Days 
            #Send API request
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameWeeks)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Pre-requisite - Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From DB By ID if one already exists", ExecutionStatus.PASS)
           
            #Send API request
            actualResponse = policy.postPolicyByWeeks(token, policy_testdata.policyNameWeeks)
            self.assertTrue(actualResponse[1] == 201 , "POST was not successful")  
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            #Verify response
            responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #Fetch the policy ID from DB
            policyID = vpolicy.getPolicyIDFromDB(policy_testdata.policyNameWeeks)            
            
            #Send a PUT request to decrease the number of years
            actualResponse = policy.putModifyWeeksPolicyDecreaseWeeks(token, policyID)          
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
                        
            #Verify response
            responseResult = vpolicy.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.getPolicyDetailsFromDB(policy_testdata.policyNameWeeks)
            expecteddatalist =[policy_testdata.decreaseWeeksPolicyDays, policy_testdata.decreaseWeeksPolicyWeeks, policy_testdata.decreaseWeeksPolicyMonths,policy_testdata.decreaseWeeksPolicyYears]
            expecteddatalist.sort()
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertListEqual(dbResult, expecteddatalist, "Failed Data base verification: Expected Response [" + str(expecteddatalist) + "] and Actual Response [" + str(dbResult) + "]")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
        
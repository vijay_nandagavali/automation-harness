'''
Created on Jun 8, 2015

@author: rahula
'''

import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1504_GetRetentionPolicies(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get retention policies")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Get retention policies")
        self.executionId = exeID
    
    def runTest(self):
        try:        
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)    
                        
            #Send GET API request
            actualResponse = policy.getRetentionPolicies(token)
            logHandler.logging.info("actualResponse  " + str(actualResponse))
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            # Verification 1: Verify get all replication strategy response parameter 
            # Get actual response parameter from response
            actualResponseParams = policy.getListOfActualResponseParameter(actualResponse) 
            logHandler.logging.info("actual Parameters: " + str(actualResponseParams))                                
            logHandler.logging.info("expected Parameters: " + str(policy_testdata.responseParmeterRetentionPolicies))        
            if(len(actualResponseParams) > 0):                     
                self.assertListEqual(policy_testdata.responseParmeterRetentionPolicies, actualResponseParams, "Failed Response verification: Expected response [" + str(policy_testdata.responseParmeterRetentionStrategy) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("No Data Found", ExecutionStatus.PASS)
                
            # Verification 2: Verify get all replication strategy from DB                             
            policiesFromDB = vpolicy.getRetentionPoliciesFromDB()    
            logHandler.logging.info("policies from Database: "+ str(policiesFromDB))            
            policiesFromResponse = vpolicy.getRetentionPoliciesFromResponse(actualResponse)
            logHandler.logging.info("policies from Response: " +  str(policiesFromResponse))
            self.assertDictEqual(policiesFromDB, policiesFromResponse,"Failed Data base verification: Expected Response [" + str(policiesFromDB) + "] and Actual Response [" + str(policiesFromResponse) + "]" )
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
              
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
        
'''
Created on Jul 13, 2015

@author: rahula
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1712_DeleteDisassociatePolicyByInstanceId(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete disassociate the policy with the instance specified by id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Delete disassociate the policy with the instance specified by id")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Prerequsite setup - Post retention settings to associate a policy id with an instance id
            policyAdded=False
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB("")
            logHandler.logging.info("Policy Id from database: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult == 0):        
                
                #Send API request
                actualResponse = policy.postPolicyGeneric(token,policy_testdata.policyName, 0,0,0,1)
                logHandler.logging.info("Actual Response: " + str(actualResponse))
                self.assertTrue(actualResponse[1] == 201 , "POST was not successful")
                reportController.addTestStepResult("Send API Request- POST policy", ExecutionStatus.PASS)
                
                #Verify response
                responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
                logHandler.logging.info("Response result: " + str(responseResult))
                self.assertNotEqual(responseResult,"Fail","Response verification failed")
                reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
                
                #DB verification
                dbResult = vpolicy.verifyPostPolicyCountFromDB(policy_testdata.policyName)
                logHandler.logging.info("DB result: " + str(dbResult))
                self.assertNotEqual(dbResult,"0","Database verification failed")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)  
                dbPolicyIdResult = vpolicy.getPolicyIDFromDB(token,policy_testdata.policyName)
                
                policyAdded = True
                
            #get Instance ID from Database
            dbInstanceIdResult = vpolicy.getInstanceIdFromDB();
            logHandler.logging.info("Instance Id from database: " + str(dbInstanceIdResult))
            reportController.addTestStepResult("Get Instance_Id From Database ", ExecutionStatus.PASS)
            
            if(dbInstanceIdResult != 0 and dbPolicyIdResult != 0):
                #send post API Request
                actualPostResponse = policy.postPolicyAssocation(token,dbInstanceIdResult, dbPolicyIdResult)
                logHandler.logging.info("Actual Response: " + str(actualPostResponse))
                self.assertTrue(actualPostResponse[1] == 201 , "POST was not successful")
                reportController.addTestStepResult("Send API Request- POST policy association", ExecutionStatus.PASS)
                
                #Verify response
                responseResult = vpolicy.verifyPostPolicyResponse(actualPostResponse[0])
                logHandler.logging.info("Response result: " + str(responseResult))
                self.assertNotEqual(responseResult,"Fail","Response verification failed")
                reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
                
                #DB verification
                dbResult = vpolicy.verifyPostPolicyAssocationCountFromDB(dbInstanceIdResult,dbPolicyIdResult)
                logHandler.logging.info("DB result: " + str(dbResult))
                self.assertNotEqual(dbResult,"0","Database verification failed")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)         
                
                #Send Delete API request to disassociate the policy
                deleteResponse  = policy.deleteRetention(token, dbInstanceIdResult)
                logHandler.logging.info("Actual Response: " + str(deleteResponse))
                reportController.addTestStepResult("Disassociate policy by instance id ", ExecutionStatus.PASS)
            
                #Verify response
                responseResult = vpolicy.verifyDeletePolicyResponse(deleteResponse[0])
                logHandler.logging.info("Response result: " + str(responseResult))
                self.assertNotEqual(responseResult,"Fail","Delete Response verification failed")
                reportController.addTestStepResult("Delete Response verification", ExecutionStatus.PASS)   
                
                dbResult = vpolicy.verifyPostPolicyAssocationCountFromDB(dbInstanceIdResult,dbPolicyIdResult)
                logHandler.logging.info("DB result: " + str(dbResult))
                self.assertEqual(dbResult,0,"Database verification failed")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)      
            else:
                logHandler.logging.info("API call skipped: Prerequisite setup failed, no application instance found") 
                reportController.addTestStepResult("Skipping the test: relevant data not found", ExecutionStatus.PASS)
                   
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            #get policy_id from DB
            if(policyAdded == True):
                dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
                logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
                reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
                if(dbPolicyIdResult != 0):
                    #Delete  Policy  from DB 
                    deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                    logHandler.logging.info("Response result -DELETE policy: " + str(deleteResponse))            
                    reportController.addTestStepResult("Delete Policy From DB By ID  ", ExecutionStatus.PASS)
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        
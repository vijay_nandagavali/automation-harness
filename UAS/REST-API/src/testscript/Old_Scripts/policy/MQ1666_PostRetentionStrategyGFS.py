'''
Created on Jun 25, 2015

@author: Garima.G
'''

import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1666_PostRetentionStrategyGFS(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "POST - Retention Strategy GFS")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Retention Strategy GFS")
        self.executionId = exeID
    
    def runTest(self):
        try:         
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                        
            #Send GET API request
            actualResponse = policy.getRetentionStrategy(token)
            logHandler.logging.info("actualResponse  " + str(actualResponse))
            reportController.addTestStepResult("Send API Request - get existing strategy", ExecutionStatus.PASS)                                     
            
            strategyFromResponse = vpolicy.getRetentionStrategyFromResponse(actualResponse)      
            logHandler.logging.info("existing strategy: "+ str(strategyFromResponse))  
                         
            if(strategyFromResponse == policy_testdata.retentionNameGFS):
                actualResponse = policy.postRetentionStrategy(token,policy_testdata.retentionNameMinMax)
                self.assertTrue(actualResponse[1] == 201 , "POST was not successful")  
                reportController.addTestStepResult("Send API Request - Post Strategy MinMax", ExecutionStatus.PASS)
            
            actualResponse = policy.postRetentionStrategy(token,policy_testdata.retentionNameGFS)
            self.assertTrue(actualResponse[1] == 201 , "POST was not successful")  
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
                                
            #Verify response
            responseResult = vpolicy.verifyPostRetentionStrategyResponse(actualResponse[0])
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #DB verification
            strategyFromDB = vpolicy.verifyRetentionPolicyFromDB()                
            logHandler.logging.info("DB result: " + str(strategyFromDB))
            self.assertTrue(strategyFromDB == policy_testdata.retentionNameGFS, "Failed Data base verification: Expected Response [" + str(policy_testdata.retentionNameGFS) + "] and Actual Response [" + str(strategyFromDB) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        


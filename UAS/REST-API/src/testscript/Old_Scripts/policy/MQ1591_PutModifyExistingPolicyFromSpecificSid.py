'''
Created on Jun 18, 2015

@author: rahula
'''
import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1591_PutModifyExistingPolicyFromSpecificSid(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Put Modify Existing Policy from the specific Sid")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Modify Existing Policy from the specific Sid")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            print "login"
            #Prerequsite to add a policy

            #Fetch the policy id
            policyID = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            print "policyID  " + str(policyID)
            if(policyID == 0):
                #Pre-requisite - Send API request to post month based policy
                #actualResponse = policy.postPolicyByYears(token, policy_testdata.policyName)
                actualResponse = policy.postPolicyGeneric(token,policy_testdata.policyName, 0,0,0,1)
                self.assertTrue(actualResponse[1] == 201 , "POST was not successful")
                reportController.addTestStepResult("Send POST API Request", ExecutionStatus.PASS)
                
                #Verify response
                responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
                logHandler.logging.info("Response result: " + str(responseResult))
                self.assertNotEqual(responseResult,"Fail","Response verification failed")
                reportController.addTestStepResult("POST Response verification", ExecutionStatus.PASS)
            
            #Fetch the policy id
            policyID = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            
            #Send PUT API request
            actualResponse = policy.putUpdatePolicyBySid(token,policyID)
            print "actualResponse  " + str(actualResponse)
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
                        
            #Verify response
            responseResult = vpolicy.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Response verification failed")
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            #DB verification
            dbResult = vpolicy.getPolicyDetailsFromDB(policy_testdata.policyName)
            expecteddatalist =[policy_testdata.updatepolicyDays, policy_testdata.updatepolicyWeeks, policy_testdata.updatepolicyMonths,policy_testdata.updatepolicyYears]
            expecteddatalist.sort()
            logHandler.logging.info("DB result: " + str(dbResult))
            self.assertListEqual(dbResult, expecteddatalist, "Failed Data base verification: Expected Response [" + str(expecteddatalist) + "] and Actual Response [" + str(dbResult) + "]")
            reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            
           
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #cleanup
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #Delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From Database By ID  ", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        
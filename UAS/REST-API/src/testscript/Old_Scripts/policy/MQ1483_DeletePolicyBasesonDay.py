'''
Created on Jun 4, 2015

@author: mangesh.b
'''

import unittest
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import policy_testdata
from resourcelibrary import login, policy, logout
from verificationsript import vpolicy

class MQ1483_DeletePolicyBasesonDay(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete policy based on Days")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Policy based on Days")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login                
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
             
            if(dbPolicyIdResult != 0):
                #If policy exists, delete  Policy  from DB 
                deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
                logHandler.logging.info("Response result: " + str(deleteResponse))            
                reportController.addTestStepResult("Delete Policy From Database By ID  ", ExecutionStatus.PASS)                            
                            
            #Send API request
            actualResponse = policy.postPolicyGeneric(token,policy_testdata.policyName,5,0,0,0)
            self.assertTrue(actualResponse[1] == 201 , "POST was not successful")  
            reportController.addTestStepResult("POST API Request", ExecutionStatus.PASS)
            
            #Verify response
            responseResult = vpolicy.verifyPostPolicyResponse(actualResponse[0])
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Post Response verification failed")
            reportController.addTestStepResult("Post Response verification", ExecutionStatus.PASS)
            
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            reportController.addTestStepResult("Get Policy_ID From Database ", ExecutionStatus.PASS)
                        
            #delete policy API request
            deleteResponse = policy.deletePolicyByID(token, dbPolicyIdResult)
            logHandler.logging.info("Response result: " + str(deleteResponse))            
            reportController.addTestStepResult("Delete Policy From Database By ID  ", ExecutionStatus.PASS)    
                      
            #Verify response
            responseResult = vpolicy.verifyDeletePolicyResponse(deleteResponse)
            logHandler.logging.info("Response result: " + str(responseResult))
            self.assertNotEqual(responseResult,"Fail","Delete Response verification failed")
            reportController.addTestStepResult("Delete Response verification", ExecutionStatus.PASS)
            
            #get policy_id from DB
            dbPolicyIdResult = vpolicy.getPolicyIDFromDB(policy_testdata.policyName)
            logHandler.logging.info("DB result: " + str(dbPolicyIdResult))
            self.assertTrue(dbPolicyIdResult == 0, "Delete was not successful")  
            reportController.addTestStepResult("Database verification for Delete policy ", ExecutionStatus.PASS)

            reportController.tcExecutionStatus(ExecutionStatus.PASS)          
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
        
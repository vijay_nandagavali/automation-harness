'''
Created on Mar 11, 2015

@author: Amey.k
'''

import unittest
from resourcelibrary import capabilities, login, logout
from testdata import capabilities_testdata
from util import reportController, logHandler
from verificationsript import vcapabilities
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1269_GetPlatformCapabilities(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get Platform Capabilities")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Platform Capabilities")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = capabilities.getListOfCapabilities(token)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = capabilities.getCapabilitiesParametersInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            
            actualParamLen = len(actualResponseParams)
            logHandler.logging.info("Actual Parameter Length: " + str(actualParamLen))
            
            
            if(actualParamLen == 8):
                expectedParams = capabilities_testdata.responseParameterCE
            elif(actualParamLen < 8):
                expectedParams = capabilities_testdata.responseParameter
            elif(actualParamLen == 9):
                if(actualResponseParams.index("CE")==-1):
                    expectedParams = capabilities_testdata.responseParameter1
                else:
                    expectedParams = capabilities_testdata.responseParameter3
            else:
                expectedParams = capabilities_testdata.responseParameter2
                
            # sort expected response parameter list            
            expectedParams.sort()
            
            logHandler.logging.info("Expected Response Parameters - " + str(expectedParams))
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(expectedParams, actualResponseParams, "Failed Response verification: Expected response [" + str(expectedParams) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            
            # Verification 2: Verify get all alerts API response
            capabilitiesListFromMachine = vcapabilities.getCapabilitiesFromMachine()
            logHandler.logging.info("Data returned from Machine: " + str(capabilitiesListFromMachine))
             
            capabilitiesListFromResponse = vcapabilities.getCapabilitiesFromResponse(actualResponse)
            logHandler.logging.info("Data from response: " + str(capabilitiesListFromResponse))
            
            self.assertListEqual(capabilitiesListFromMachine, capabilitiesListFromResponse, "Failed Data base verification: Expected Response [" + str(capabilitiesListFromMachine) + "] and Actual Response [" + str(capabilitiesListFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
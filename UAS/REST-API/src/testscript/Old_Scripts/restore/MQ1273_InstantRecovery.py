'''
Created on Mar 16, 2015

@author: Amey.k
'''

import unittest, time
from resourcelibrary import restore, login, logout, clients, backups, jobs, inventory
from testdata import restore_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vrestore, vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1273_InstantRecovery(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Recover HyperV VM")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Instant Recovery for HyperV")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            vmIp = clients_testdata.getmachineIP("RESTORE")
            restoreName = clients_testdata.getmachineName("RESTORE")
            restoreDirectory = clients_testdata.getShareName("RESTORE")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts            
            '''Setting pre-requisites like adding client,taking backup and monitoring it'''
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            clientCreated = 0
            if(clientId==0):
                clientCreated = 1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                
                reportController.addTestStepResult("Client Added" , ExecutionStatus.PASS)
                clientId = vclients.getSpecificClientByNameDB(clients_testdata.name)
                logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            #instanceId = vbackups.getInstanceIdFromVm(backups_testdata.hypervInstanceNameForIR)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")    
            
            fullBackups = vbackups.getFullHyperVBackupsFromDB(clientId, instanceId)
            logHandler.logging.info("Full HyperV Backups returned from DB: " + str(fullBackups))
            if(len(fullBackups) == 0):
                actualResponse = backups.putHypervInstanceBackup(token,instanceId, backups_testdata.backupType, backups_testdata.storageType, backups_testdata.verifyLevel, backups_testdata.sid)
                reportController.addTestStepResult("Send API request for Put Backup for HyperV", ExecutionStatus.PASS)
                
                responseResult = backups.getPutRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))
                
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID returned from response: " + str(strJobId))
                  
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                logHandler.logging.info("Backup job status: " + str(backupJobStatus))
            
                if(backupJobStatus!="Successful"):
                    reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL)
                    raise Exception("Backup not successful")
                
                else:
                    logHandler.logging.info("Backup successful..")
                    reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)   
                 
                bid=vbackups.getBackupId(strJobId)
                logHandler.logging.info("Backup ID returned from DB: " + str(bid))
                
                logHandler.logging.info("Thread sleep for 60 sec")
                time.sleep(60)
            else:
                bid = fullBackups[0][0]
                logHandler.logging.info("Backup ID: " + str(bid))
            
            actualResponse = restore.putInstantRecoveryJob(token,int(bid) , restoreDirectory, restore_testdata.sid, restoreName, vmIp, restore_testdata.audit, clientName)
            reportController.addTestStepResult("Send API request for Put Instant Recovery" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = restore.postResponseParse(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            # sort expected response parameter list          
            if (actualResponseParams == "Pass"):
                reportController.addTestStepResult("Instant Recovery Job triggered successfully", ExecutionStatus.PASS)
            else:
                raise Exception ("Instant Recovery Job start failed")
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)

        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
        
        finally:
            #Delete client
            if(clientCreated == 1 or result == 'Fail'):                
                clients.deleteClients(token, clientId) 
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
             
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
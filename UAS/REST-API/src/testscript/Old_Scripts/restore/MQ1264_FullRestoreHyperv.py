'''
Created on Mar 9, 2015

@author: Amey.k
'''

import unittest, time
from resourcelibrary import restore, login, logout, clients, backups, jobs, inventory
from testdata import restore_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vrestore, vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1264_FullRestoreHyperv(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Restore Hyper-V VM full backup")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Full HyperV Restore")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            restoreDirectory = clients_testdata.getShareName("RESTORE")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
             
            '''Setting pre-requisites like adding client,taking backup and monitoring it'''
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            clientAdded = 0
            if(clientId==0):
                clientAdded = 1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                logHandler.logging.info("Actual response for Add client: " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
               
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client from response: " + str(cid))
                # Verification 1: verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully...")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            #Check Full backups
            fullBackups = vbackups.getFullHyperVBackupsFromDB(clientId, instanceId)
            logHandler.logging.info("HyperV Full backups returned from DB: " + str(fullBackups))
            
            if(len(fullBackups) == 0):
                actualResponse = backups.putHypervInstanceBackup(token,instanceId, backups_testdata.backupType, backups_testdata.storageType, backups_testdata.verifyLevel, backups_testdata.sid)
                reportController.addTestStepResult("Send API request for PUT Backup", ExecutionStatus.PASS)
                
                responseResult = backups.getPutRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID returned from response: " + str(strJobId))
                  
                # Get the status of the backup job
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                logHandler.logging.info("Backup job status: " + str(backupJobStatus))
                
                if(backupJobStatus!="Successful"):
                    reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL)
                    raise Exception("Backup not successful")
                    
                else:
                    logHandler.logging.info("Backup successful..")
                    reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
                 
                bid=vbackups.getBackupId(strJobId)
                logHandler.logging.info("Backup ID returned from DB: " + str(bid))
                
                logHandler.logging.info("Thread sleep for 60 sec")
                time.sleep(60)     
            else:
                bid = fullBackups[0][0]
                logHandler.logging.info("Backup ID: " + str(bid))
                
            #Send API request for POST HyperV Restore
            actualResponse = restore.putHypervRestore(token, str(bid), restoreDirectory, restore_testdata.sid)
            reportController.addTestStepResult("Send API request for POST Restore" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = restore.getRestoreJobParametersInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            # sort expected response parameter list          
            restore_testdata.responseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(restore_testdata.responseParameter))
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(restore_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(restore_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)  
            
            # Verification 2: Verify get all alerts API response            
            restoreJobIdFromResponse = vrestore.getRestoreJobIdFromResponse(actualResponse)
            logHandler.logging.info("Data from response: " + str(restoreJobIdFromResponse))
            
            restoreJobIdFromDB = vrestore.checkRestoreJobFromDB(str(restoreJobIdFromResponse))
            logHandler.logging.info("Data returned from DB: " + str(restoreJobIdFromDB))
            
            if (len(restoreJobIdFromDB)!=0):
                reportController.addTestStepResult("Restore Job triggered successfully", ExecutionStatus.PASS)
            else:
                raise Exception ("Restore job not present in the database")
            
            restoreJobStatus=jobs.waitForJobCompletion(restore_testdata.jobType, restore_testdata.waittime, token, restoreJobIdFromResponse)
            logHandler.logging.info("Restore job status: " + str(restoreJobStatus))
            
            if(restoreJobStatus!="Successful"):
                reportController.addTestStepResult("Restore not successful", ExecutionStatus.FAIL)
                raise Exception("Restore not successful")                
            else:
                logHandler.logging.info("Restore successful..")
                reportController.addTestStepResult("Restore Successful" , ExecutionStatus.PASS)
            
            #DB verification
            rbid=vbackups.getBackupId(restoreJobIdFromResponse)
            logHandler.logging.info("Restore Backup ID returned from DB: " + str(rbid))
            
            resBackupId = vrestore.getRestoreBackupsFromDB(clientId, bid)
            logHandler.logging.info("Backup Id returned: " + str(resBackupId))
            
            if(resBackupId <> 0 and resBackupId == rbid):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Database verification failed")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup
            if(clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)

            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
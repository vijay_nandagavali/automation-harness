'''
Created on June 2, 2015

@author: Nikhil.S
'''

import unittest

from resourcelibrary import restore, login, logout, clients, backups, jobs
from testdata import restore_testdata, clients_testdata, backups_testdata
from util import reportController, logHandler
from verificationsript import vrestore, vclients, vbackups

from config.Constants import ExecutionStatus
from config import apiconfig
import time
from time import sleep


class MQ1468_PostFileLevelRestoreVMWare(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Restore VM Ware file level backup")
        self.executionId = exeID
        
        
    def runTest(self):        
        print "Restore VM Ware file level backup"
        try:
            #Test data used in the script
            vmName=clients_testdata.getmachineName("VM1")
            vmIp=clients_testdata.getmachineIP("VM1")
            instanceName = clients_testdata.getShareName("VM1")
            vmType = clients_testdata.getType("VM1")
            vmUsername = clients_testdata.getUserName("VM1")
            vmPassword = clients_testdata.getPassword("VM1")
            esxName = clients_testdata.getmachineName("ESX-Server")
            esxDatastore = clients_testdata.getShareName("ESX-Server")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            
            #Step 1: Send request to update a client
            uuid = vclients.getSpecificEsxServerFromDB(esxName)
            logHandler.logging.info("UUID returned from DB: " + str(uuid))
            
            if(uuid==0):
                addedClientFlag =1
                actualResponse = clients.postVMClient(token,clients_testdata.sid, vmName, vmType, 
                                                clients_testdata.vm_priority, clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, clients_testdata.vm_use_ssl, 
                                                clients_testdata.vm_defaultschedule, clients_testdata.vm_port, vmIp, 
                                                clients_testdata.vm_existing_credential, vmUsername, vmPassword, 
                                                clients_testdata.vm_domain)
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS) 
                  
                responseResult = clients.getVMPostClientRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))                            
                uuid = vclients.getSpecificEsxServerFromDB(esxName)
                
            # Step 3: Send the PUT request
            logHandler.logging.info("Thread sleep for 60sec..")
            sleep(60)
            instanceId = vbackups.getInstanceIdFromVmVMWare(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            if (instanceId!=""):
                actualResponse = backups.putHypervInstanceBackup(token,instanceId, backups_testdata.backupType, backups_testdata.storageType, backups_testdata.verifyLevel, backups_testdata.sid)
            else:
                raise Exception ("No such VM exists")
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            print strJobId
            
            
            # Get the status of the backup job
                
            verificationCnt = 0
            backupjobstatus=""
            while (verificationCnt < restore_testdata.waittime):
                actualResponse = jobs.getBackupJobs(token)
                backupjobstatus = jobs.getJobStatus(actualResponse,strJobId)
                if((backupjobstatus == 'Successful') or (backupjobstatus == 'Error')):
                    break
                verificationCnt += 1
                print "Waiting for Backup to complete.... CheckCount: " + str(verificationCnt) +" Status: "+backupjobstatus
                time.sleep(1)
            reportController.addTestStepResult("VM ware Backup successful" , ExecutionStatus.PASS)
               
               
            bid=vbackups.getBackupId(strJobId)
            print bid
               
            sleep(60)    
            actualResponse = restore.createHypervFileLevelRestore(token,str(bid) , restore_testdata.sid)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response

            # sort expected response parameter list
            responseParse = restore.postResponseParse(actualResponse)
            print responseParse
            if (responseParse == "Pass"):
                reportController.addTestStepResult("Restore job triggered successfully", ExecutionStatus.PASS)
            else:
                raise Exception ("Restore job did not get triggered")                     
 
            sleep(300)
            reportController.addTestStepResult("Hyper-V VM Restore successful" , ExecutionStatus.PASS)
            
            
            #cleanup
            print uuid
            clients.deleteVMClient(token, uuid)
            logHandler.logging.info("Client already existing is deleted..")
            reportController.addTestStepResult("Delete existing client", ExecutionStatus.PASS)
            logout.doUserLogout(token)
            
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
               
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
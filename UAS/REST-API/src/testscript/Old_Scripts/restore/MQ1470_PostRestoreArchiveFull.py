'''
Created on May 29, 2015

@author: savitha.p
'''

import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, archive, logout, storage, restore, jobs, clients, backups, inventory
from testdata import archive_testdata, backups_testdata, clients_testdata, inventory_testdata
from verificationsript import varchive, vstorage, vbackups, vrestore, vclients

class MQ1470_PostRestoreArchiveFull(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Restore Archive Full")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\t POST - Restore Archive Full")
        self.executionId = exeID
    
    def runTest(self):
        try:            
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
            
            storageAddedFlag = 0
            addedClientFlag = 0
            dbResult = ''
            
            if(storageId == 0):
                logHandler.logging.info("Cloud Storage not available. Adding the same...")
                actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
                reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                logHandler.logging.info("Cloud storage " + cloudStorageName + " is added")
                
                storageId = vstorage.getStorageIdByName(cloudStorageName)
                logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
                storageAddedFlag = 1
                
            #Find if any client with same name is already available in DB. If not available, add the client.
            cid = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client returned from DB: " + str(cid))
            if(cid==0):
                # Step 1: Send request to update a client
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                logHandler.logging.info("Add client API - Actual Response: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client returned from response: " + str(cid))
            
                # Verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully..")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
                addedClientFlag = 1
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
                        
            if (instanceId == None):
                raise Exception("Instance ID not found")
            
            archiveIds = varchive.getArchiveIdsFromDB(cloudStorageName, instanceId)
            logHandler.logging.info("Archive Ids returned from DB: " + str(archiveIds))
                        
            if(len(archiveIds) > 0):
                archiveId = archiveIds[0]
            else:
                logHandler.logging.info("No archives available for this storage. Hence creating the archive..")
                                      
                #Check Full backups
                fullBackups = vbackups.getFullHyperVBackupsFromDB(cid, instanceId)
                logHandler.logging.info("HyperV Full backups returned from DB: " + str(fullBackups))
                
                if(len(fullBackups) == 0):
                    actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
                    reportController.addTestStepResult("Send API request for Creating new HyperV Backup" , ExecutionStatus.PASS)
            
                    # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
                    responseResult = backups.getPutRequestResponse(actualResponse)
                    strJobId = '-1'
                    strJobId = responseResult["data"][0]["job_id"] 
                    logHandler.logging.info("Job ID from Response: " + str(strJobId))
                    if(strJobId == '-1'):
                        raise Exception("Failed to trigger backup")
            
                    #Check the status of backup
                    backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                    if(backupJobStatus!="Successful"):
                        raise Exception("HyperV Full Backup not successful")
                    else:
                        logHandler.logging.info("Backup successful")
                        reportController.addTestStepResult("HyperV Full Backup" , ExecutionStatus.PASS)
                
                    bid=vbackups.getBackupId(strJobId)
                    logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
                
                    logHandler.logging.info("Thread sleep for 60 sec..")
                    time.sleep(60)
                
                #Erase Cloud Storage
                mediaLabel = archive_testdata.cloud_archive_label
                actualResponse = archive.putPrepareArchiveMedia(token, cloudStorageName, mediaLabel)
                reportController.addTestStepResult("Send API request for Archive Media Prepare" , ExecutionStatus.PASS)
            
                #Verification 1: Verify the response parameter   
                responseResult = archive.getPutRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))
            
                if (responseResult == "Pass"):
                    reportController.addTestStepResult("Verify the response parameter", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed response verification")
            
                #Verification 2: Verify in the database
                dbResult = varchive.getArchivePrepareStatusFromDB(cloudStorageName)
                logHandler.logging.info("DB Result: " + str(dbResult))
            
                if(dbResult == 0):
                    reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
                else:
                    raise Exception("Failed database verification - some records found in archive sets table")
                
                #Create an Archive
                actualResponse = archive.putArchive(token, instanceId, cloudStorageName, "Full")
                reportController.addTestStepResult("Send API request for Creating new HyperV Archive" , ExecutionStatus.PASS)                                
                
                jobNo = varchive.verifyPutArchiveResponse(actualResponse)
                logHandler.logging.info("Create Archive successful. Job Number: " + str(jobNo))
                
                #Check the status of backup
                archiveJobStatus=jobs.waitForJobCompletion("Archive", backups_testdata.waittime, token, jobNo)
                if(archiveJobStatus!="Successful"):
                    raise Exception("Archive for HyperV Full Backup not successful")
                else:
                    logHandler.logging.info("Archive successful")
                    reportController.addTestStepResult("Archive for HyperV Full Backup" , ExecutionStatus.PASS)
                
                archiveId = varchive.verifyPutArchiveDB(jobNo)
                logHandler.logging.info("Archive Id returned from DB: " + str(archiveId))
                reportController.addTestStepResult("Archive Database verification" , ExecutionStatus.PASS)
                
                logHandler.logging.info("Thread sleep for 60 sec")
                time.sleep(60)
            
            actualResponse = restore.postRestoreArchiveFull(token, archiveId)
            reportController.addTestStepResult("Send API request for Archive Restore" , ExecutionStatus.PASS)
            
            jobNo = restore.getJobNumberFromResponse(actualResponse)
            logHandler.logging.info("Job number returned from Response: " + str(jobNo))
            
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
            
            if(jobNo == 0):
                raise Exception("Job not found in Response")
            
            jobStatus = jobs.waitForJobCompletion("Archive", backups_testdata.waittime, token, jobNo)
            logHandler.logging.info("Job Status: " + str(jobStatus))
            
            if(jobStatus != 'Successful'):
                raise Exception("Archive restore job failed with job status as " + str(jobStatus))
            
            logHandler.logging.info("Archive Restore job successful")
            reportController.addTestStepResult("Archive Restore", ExecutionStatus.PASS)
            
            #DB verification
            restoreBackupId = vrestore.getArchiveRestoreBackupIdFromDB(jobNo) 
            logHandler.logging.info("Restore Backup Id returned from DB: " + str(restoreBackupId))
            self.assertNotEqual(restoreBackupId, "0", "Database verification failed")
            reportController.addTestStepResult("Database verification successful" , ExecutionStatus.PASS)
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            reportController.generateReportSummary(self.executionId)
            #Cleanup
            if(storageAddedFlag or dbResult == 'Fail'):
                storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Delete cloud storage" , ExecutionStatus.PASS)
            
            if(addedClientFlag == 1 or result == 'Fail'):
                clients.deleteClients(token, cid) 
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
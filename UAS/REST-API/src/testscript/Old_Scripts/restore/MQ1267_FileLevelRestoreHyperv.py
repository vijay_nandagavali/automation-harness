'''
Created on Mar 10, 2015

@author: Amey.k
'''

import unittest, time
from resourcelibrary import restore, login, logout, clients, backups, jobs, inventory
from testdata import restore_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vrestore, vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1267_FileLevelRestoreHyperv(unittest.TestCase): 
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Restore Hyper-V VM file level backup")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - File Level Restore HyperV")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            
            '''Setting pre-requisites like adding client,taking backup and monitoring it'''
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            addedClient = 0
            if(clientId==0):
                addedClient = 1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                    
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            #instanceId = vbackups.getInstanceIdFromVm(backups_testdata.hypervInstanceNameForFLR)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")

            fullBackups = vbackups.getFullHyperVBackupsFromDB(clientId, instanceId)
            logHandler.logging.info("Full HyperV Backups returned from DB: " + str(fullBackups))
            if(len(fullBackups) == 0):
                actualResponse = backups.putHypervInstanceBackup(token, instanceId, backups_testdata.backupType, backups_testdata.storageType, backups_testdata.verifyLevel, backups_testdata.sid)
                reportController.addTestStepResult("Create Backup", ExecutionStatus.PASS)
                
                responseResult = backups.getPutRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))
                
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID returned from response: " + str(strJobId))
                    
                if(strJobId == '-1'):
                    raise Exception("Failed to trigger backup")
    
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                if(backupJobStatus!="Successful"):
                    raise Exception("Backup not successful..")
                else:
                    logHandler.logging.info("Backup successful")
                    reportController.addTestStepResult("Backup" , ExecutionStatus.PASS) 
                
                bid=vbackups.getBackupId(strJobId)
                logHandler.logging.info("Backup ID returned from DB: " + str(bid))
                
                logHandler.logging.info("Thread sleep for 60 sec")
                time.sleep(60)
            else:
                bid = fullBackups[0][0]
                logHandler.logging.info("Backup ID: " + str(bid))
                
            actualResponse = restore.createHypervFileLevelRestore(token,str(bid) , restore_testdata.sid)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            responseParse = restore.postResponseParse(actualResponse)
            logHandler.logging.info("Response Parse: " + str(responseParse))
            
            if (responseParse == "Pass"):
                reportController.addTestStepResult("Restore job triggered successfully", ExecutionStatus.PASS)
            else:
                raise Exception ("Restore job did not get triggered")                     
 
            reportController.tcExecutionStatus(ExecutionStatus.PASS)

        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
        
        finally:
            #Delete client
            if(addedClient == 1 or result == 'Fail'):                
                clients.deleteClients(token, clientId) 
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
             
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
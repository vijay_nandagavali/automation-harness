'''
Created on May 27, 2015

@author: rahula
'''

import unittest

from resourcelibrary import clients, backups,login, logout, jobs, restore
from testdata import clients_testdata, backups_testdata, restore_testdata
from util import reportController, logHandler
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from time import sleep

class MQ1444_PostFullRestoreVMWare(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "POST Restore VMWare Full Backup")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Restore VMWare Full Backup")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            #Test data used in the script
            vmName=clients_testdata.getmachineName("VM1")
            vmIp=clients_testdata.getmachineIP("VM1")
            instanceName = clients_testdata.getShareName("VM1")
            vmType = clients_testdata.getType("VM1")
            vmUsername = clients_testdata.getUserName("VM1")
            vmPassword = clients_testdata.getPassword("VM1")
            esxName = clients_testdata.getmachineName("ESX-Server")
            esxDatastore = clients_testdata.getShareName("ESX-Server")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            uuid = vclients.getSpecificEsxServerFromDB(esxName)
            logHandler.logging.info("UUID returned from DB: " + str(uuid))
            
            if(uuid==0):
                addedClientFlag =1
                actualResponse = clients.postVMClient(token,clients_testdata.sid, vmName, vmType, 
                                                clients_testdata.vm_priority, clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, clients_testdata.vm_use_ssl, 
                                                clients_testdata.vm_defaultschedule, clients_testdata.vm_port, vmIp, 
                                                clients_testdata.vm_existing_credential, vmUsername, vmPassword, 
                                                clients_testdata.vm_domain)
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS) 
                  
                responseResult = clients.getVMPostClientRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))                            
                uuid = vclients.getSpecificEsxServerFromDB(esxName)
            # Step 3: Send the PUT request
            logHandler.logging.info("Thread sleep for 60sec..")
            sleep(60)
            instanceId = vbackups.getInstanceIdFromVmVMWare(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            
            #calling the existing function for VMWare since the call parameters are same.
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID from Response: " + str(strJobId))
            if(strJobId == '-1'):
                raise Exception("Failed to trigger backup")
              
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus!="Successful"):
                logHandler.logging.error("Backup not successful")
                reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup Success" , ExecutionStatus.PASS)
            
            bid=vbackups.getBackupId(strJobId)
            print bid
            
            sleep(60)
            #actualResponse = restore.putHypervRestore(token, str(bid), restore_testdata.directory, restore_testdata.sid)
            actualResponse =restore.putVMWareRestore(token, str(bid), restore_testdata.sid, str(uuid), esxDatastore, restore_testdata.group, esxName, restore_testdata.metadata, restore_testdata.template)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = restore.getRestoreJobParametersInJson(actualResponse)
            print actualResponseParams
            # sort expected response parameter list          
            restore_testdata.responseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(restore_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(restore_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)  
            
            # Verification 2: Verify the API response
            restoreJobIdFromResponse = vrestore.getRestoreJobIdFromResponse(actualResponse)
            print restoreJobIdFromResponse
            #restoreJobIdFromDB = vrestore.checkRestoreJobFromDB(str(restoreJobIdFromResponse))
            restoreJobCountFromDB = vrestore.checkRestoreVMWareJobFromDB(str(restoreJobIdFromResponse))
            if (restoreJobCountFromDB!= 0):
                reportController.addTestStepResult("Restore Job triggered successfully", ExecutionStatus.PASS)
            else:
                raise Exception ("Restore job not present in the database")
            
            # Code commented as it consistently fails
            #restoreJobStatus=jobs.waitForJobCompletion(restore_testdata.jobType, restore_testdata.waittime, token, restoreJobIdFromResponse)
            #print backupJobStatus
            #logHandler.logging.info("Restore job status: " + str(restoreJobStatus))
            
            #if(restoreJobStatus!="Successful"):
            #    reportController.addTestStepResult("Restore not successful", ExecutionStatus.FAIL)
            #    raise Exception("Restore not successful")
                
            #else:
            #    logHandler.logging.info("Restore successful..")
            #    reportController.addTestStepResult("Restore Successful" , ExecutionStatus.PASS)
 
            #cleanup
            if(addedClientFlag):
                clients.deleteVMClient(token, uuid)
                logHandler.logging.info("Client already existing is deleted..")
                reportController.addTestStepResult("Delete existing client", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)

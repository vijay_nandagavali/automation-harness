'''
Created on Oct 26, 2015

@author: Prateek
'''
import unittest, time
from resourcelibrary import restore, login, logout, clients, backups, jobs, inventory
from testdata import restore_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vrestore, vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2082_FullRestore_XenServer(unittest.TestCase):
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Restore full backup for Xen Server instance")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Restore full backup for Xen Server instance")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver, 
                                                       clients_testdata.system_xenserver, clients_testdata.ip_xenserver, 
                                                       clients_testdata.username_xenserver, clients_testdata.password_xenserver, 
                                                       clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, 
                                                       clients_testdata.is_synchable_xenserver, clients_testdata.is_auth_enabled_xenserver)
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                time.sleep(60)
            
            
            instanceId = vbackups.getInstanceIdFromVmVMWare(backups_testdata.xenserverInstanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            # Step 2: Trigger a full backup
            actualResponse = backups.putXenServerInstanceBackup(token,instanceId,backups_testdata.xenserverInstanceName,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.xenserverVerify,backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
                        
            #Extract jobid and client name from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            print "responseResult: "+str(responseResult)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID from Response: " + str(strJobId))
            print "Job ID from Response: " + str(strJobId)
            if(strJobId == -1):
                raise Exception("Backup not triggered")
            else:    
                reportController.addTestStepResult("Backup job triggered", ExecutionStatus.PASS)
                        
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.xenWaitTime, token, strJobId)
            if(backupJobStatus =="Successful" or backupJobStatus =="Warning" or backupJobStatus == ""):
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL) 
            
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID))            
            
            # Step 3: Send API for restore
            actualResponse = restore.postXenRestore(token, int(backupID), restore_testdata.sid, restore_testdata.xenHost, restore_testdata.xenStorageRepository, restore_testdata.xenName, restore_testdata.xenMetadata)
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobIdInit = responseResult["id"] 
            logHandler.logging.info("Restore JobId original - " + str(strJobIdInit))
            print "Restore JobId original - " + str(strJobIdInit)
            strJobIdList = strJobIdInit.split(',')
            logHandler.logging.info("Restore JobId List- " + str(strJobIdList))
            print "Restore JobId List- " + str(strJobIdList)
            strJobId = strJobIdList[1]
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print "Restore JobId - " + str(strJobId)
            
            # Get actual response parameter from response
            actualResponseParams = backups.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Restore Actual Response Parameters - " + str(actualResponseParams))
            print "Restore Actual Response Parameters - " + str(actualResponseParams)
            
            restore_testdata.responseParameter.sort()
            logHandler.logging.info("Restore Expected Response Parameters - " + str(restore_testdata.responseParameter))
            
            self.assertListEqual(restore_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(restore_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify Restore response parameter", ExecutionStatus.PASS)
            
            # Verification 2: Verify the results obtained from Database - jobs table. Search by Job ID
            dbResult = vbackups.getJobByIdFromDB(strJobId)
            logHandler.logging.info("JOB triggered from jobID : " + str(dbResult))
            
            if(dbResult == "Pass"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Verified database results", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Database verification failed")
                reportController.addTestStepResult("Failed to find Job with Job ID" + str(strJobId), ExecutionStatus.FAIL)    
            
            #Check the status of Restore
            restoreJobStatus=jobs.waitForJobCompletion(restore_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" or restoreJobStatus == ""):
                logHandler.logging.info("restore successful")
                reportController.addTestStepResult("restore Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("restore not successful", ExecutionStatus.FAIL) 
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #cleanup
            if(addedClientFlag):
                clients.deleteClients(token, clientId)
                reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)

            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
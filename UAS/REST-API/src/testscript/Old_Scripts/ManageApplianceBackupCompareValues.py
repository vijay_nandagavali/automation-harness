'''
Created on Sep 25, 2015

@author: savitha.p
'''

import unittest, time
from config import apiconfig
from config.Constants import ExecutionStatus
from util import reportController, logHandler
from testdata import systems_testdata, clients_testdata, backups_testdata
from resourcelibrary import login, systems, logout, clients, backups, jobs
from verificationsript import vsystems, vclients, vbackups

class ManageApplianceBackupCompareValues(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Manage Appliance Backup Compare Values")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\t Manage Appliance Backup Compare Values")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            
            clientDetails = []
            for i in range(0,len(systems_testdata.list_Systems)):
                clientDetails.append([])
                appName = systems_testdata.list_Systems[i][0]
                appIp = systems_testdata.list_Systems[i][1]
                name = "vCenter-RRC"
                ip = "192.168.139.254"
                if(i < 9):
                    vmName = "Windows8_0" + str(i+1)
                else:
                    vmName = "Windows8_" + str(i+1)
                
                #Add Managed Appliances
                actualResponse = systems.postAddSystem(token, appName, appIp) 
                logHandler.logging.info("Appliance System IP: " + str(appIp))
                clientDetails[i].append(str(appIp))
                sysid = vsystems.getSystemIDFromDB(appName)
                logHandler.logging.info("Appliance System ID: " + str(sysid))
                clientDetails[i].append(str(sysid))
                
                #Add ESX client on each of the Managed appliance
                logHandler.logging.info("Client Name: " + str(name))
                actualResponse = clients.postClientForSid(token, sysid, clients_testdata.vm_name, clients_testdata.vm_os_type, 
                                                clients_testdata.vm_priority, clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, clients_testdata.vm_use_ssl, 
                                                clients_testdata.vm_defaultschedule, clients_testdata.vm_port, ip, clients_testdata.vm_existing_credential, 
                                                clients_testdata.vm_username, clients_testdata.vm_password, clients_testdata.vm_domain)
                logHandler.logging.info("Add ESX host on Managed Appliance response: " + str(actualResponse))
                
                clientid = vclients.getSpecificClientByNameDB(name)          
                logHandler.logging.info("Client ID: " + str(clientid))
                clientDetails[i].append(str(clientid))
                
                logHandler.logging.info("Instance Name: " + str(vmName))
                instanceId = vbackups.getInstanceIdFromVmVMWareForSid(appIp, vmName)
                logHandler.logging.info("Instance ID: " + str(instanceId))
                clientDetails[i].append(str(instanceId))
                
                mgdToken = login.getAuthenticationToken_System(apiconfig.machineuser,apiconfig.machinepassword, appIp)            
                logHandler.logging.info("Managed Appliance Auth Token: " + str(mgdToken))
                clientDetails[i].append(str(mgdToken))
                
            logHandler.logging.info("Client Details: " + str(clientDetails))   
            
            for i in range(0, len(clientDetails)):
                instanceId = int(clientDetails[i][3])
                sid = clientDetails[i][1]
                strJobId = '-1'
                
                #Trigger Backup for Instance ID
                actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,sid)
                logHandler.logging.info("Trigger Backup actual response: " + str(actualResponse))
                 
                #Get the Job ID
                responseResult = backups.getPutRequestResponse(actualResponse)
                strJobId = responseResult["data"][0]["job_id"] 
                logHandler.logging.info("Job ID from Response: " + str(strJobId))
                 
                if(strJobId == '-1'):
                    raise Exception("Failed to trigger backup")
                clientDetails[i].append(str(strJobId))
            
            logHandler.logging.info("Client Details: " + str(clientDetails))
            
            completeFlag = False
            job = []
                        
            while(completeFlag != True):
                job = []
                mgrData = []
                mgdData = []
                #Retrieve the Active Job status on Manager Appliance and Managed Appliance
                for i in range(0,len(clientDetails)):
                    envIp = clientDetails[i][0]
                    mgrEnvIp = apiconfig.environmentIp
                    mgdSid = clientDetails[i][1]
                    sid = "1"
                    jobId = clientDetails[i][5]
                    mgdToken = clientDetails[i][4]                    
                    
                    logHandler.logging.info("Manager Appliance " + str(mgrEnvIp) + " .. Managed Appliance " + str(envIp))
                    
                    mgrResponse = jobs.getJobs(token, mgrEnvIp)
                    mgrData.append(jobs.getJobPercentComplete(mgrResponse, mgdSid, jobId)) 
                    
                    mgdResponse = jobs.getJobs(mgdToken, envIp)
                    mgdData.append(jobs.getJobPercentComplete(mgdResponse, sid, jobId))
                    
                    percentComplete = jobs.getJobPercentComplete(mgrResponse, mgdSid, jobId)
                    logHandler.logging.info("Percent Complete: " + str(percentComplete))
                    job.append(str(percentComplete))
                    
                logHandler.logging.info("Job Percent: " + str(job))
                logHandler.logging.info("Manager Appliance Data: " + str(mgrData))
                logHandler.logging.info("Managed Appliance Data: " + str(mgdData))
                                
                #Verify if all the jobs have completed 100%
                flag1 = True
                for i in range(0,len(job)):
                    if(job[i] != '100' and job[i] != 'Done'):
                        flag1 = False
                        break
                
                if(flag1 == True):
                    completeFlag = True
                    
                logHandler.logging.info("Waiting for 20 sec..")
                time.sleep(20)
            
            logHandler.logging.info("Client Details: " + str(clientDetails))
            logHandler.logging.info("Job Details: " + str(job))   
            
            sysid = vsystems.getSystemIDFromDB(systems_testdata.list_Systems[0][0])
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        
        finally:
            #Cleanup
            #Delete managed appliances from Manager appliance
            if(len(clientDetails) > 0):
                for i in range(0, len(clientDetails)):
                    sysId = clientDetails[i][1]
                    systems.deleteSystem(token, sysId)
                    
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
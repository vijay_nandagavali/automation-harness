'''
Created on Feb 6, 2015

@author: Amey.k
'''

import unittest, time
from resourcelibrary import joborders, login, logout, clients, jobs, inventory
from testdata import joborders_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders, vclients, vjobs, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1070_GetAllJoborders(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get All Joborders")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - All job orders")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            Prereq_flag=0
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            actualResponse = joborders.getAllJoborders(token)
            reportController.addTestStepResult("Send API request for Get All joborders" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = joborders.getJobordersParametersInJson(actualResponse)
            logHandler.logging.info("Get All joborders - Actual Response parameters: " + str(actualResponseParams))
            
            clientAdded = 0
            if(len(actualResponseParams)==0):
                Prereq_flag = 1
                cid = vclients.getSpecificClientByNameDB(clients_testdata.name)
                logHandler.logging.info("Client ID from DB: " + str(cid))
                if(cid==0):
                    clientAdded = 1
                    # Step 1: Send request to update a client
                    actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                     clients_testdata.existing_credential)
                    logHandler.logging.info("Actual response for Add client: " + str(actualResponse))
                    reportController.addTestStepResult("Send API request for Add client" , ExecutionStatus.PASS)
                
                    # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                    cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                    logHandler.logging.info("Client from response: " + str(cid))
                    # Verification 1: verify the response parameter
                    if (cid):
                        logHandler.logging.info("Client added successfully...")
                        reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                    else:
                        logHandler.logging.error("Failed to add client")
                        raise Exception ("Failed to add a client")
                
                result = inventory.waitForInventorySync(token, 120, inventory_testdata.sid)
                logHandler.logging.info("Result: " + str(result))
                self.assertNotEqual(result,"Fail","Inventory sync failed")
                reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                
                instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
                logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
                self.assertNotEqual(instanceId,None,"Instance Id not found")
                reportController.addTestStepResult("Instance ID found", ExecutionStatus.PASS)
                            
                instanceIdList = []
                instanceIdList.append(instanceId)
                
                mcDateTime = vjoborders.getDateTimeFromMachine()
                logHandler.logging.info("Machine date time: " + str(mcDateTime))
            
                start_date = mcDateTime[0]
                start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
                logHandler.logging.info("Start Date: " + str(start_date))
                logHandler.logging.info("Start Time: " + str(start_time))

                actualResponse = joborders.createNewJoborder(token, joborders_testdata.name, joborders_testdata.type, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
                logHandler.logging.info("Actual response for create job order: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for create joborder" , ExecutionStatus.PASS)
                
                # Get actual response parameter from response
                joid = joborders.getPostRequestResponse(actualResponse)
                logHandler.logging.info("JOID from response: " + str(joid))
                if (joid):
                    logHandler.logging.info("Joborder created successfully...")
                    reportController.addTestStepResult("Create New Joborder", ExecutionStatus.PASS)
                else:
                    logHandler.logging.error("Failed to create new joborder")
                    raise Exception ("Failed to Create New Joborder")
                
                actualResponse = joborders.getAllJoborders(token)
                reportController.addTestStepResult("Send API request for Get All joborders" , ExecutionStatus.PASS)
                logHandler.logging.info("Get All joborders - Actual Response: " + str(actualResponse))
                
                actualResponseParams = joborders.getJobordersParametersInJson(actualResponse)
                logHandler.logging.info("Get All joborders - Actual Response parameters: " + str(actualResponseParams))
                
            joborders_testdata.responseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(joborders_testdata.responseParameter))
        
            self.assertListEqual(joborders_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(joborders_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        

            count = vjoborders.getJobordersCountFromDB() 
            logHandler.logging.info("Job order count from DB: " + str(count))      
            joListFromDB = vjoborders.getJobordersFromDB()
            logHandler.logging.info("Job orders from DB: " + str(joListFromDB))
             
            joListFromResponse = vjoborders.getJobordersFromResponse(actualResponse, count)
            logHandler.logging.info("Job orders from response: " + str(joListFromResponse))
            
            self.assertListEqual(joListFromDB, joListFromResponse, "Failed Data base verification: Expected Response [" + str(joListFromDB) + "] and Actual Response [" + str(joListFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)

            # Cleanup
            if(Prereq_flag):
                logHandler.logging.info("Thread sleep for 120 sec..")
                time.sleep(120)
                                
                strJobId = vjobs.getJobIdFromJoborder(str(joid).replace("b", ""))
                logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
                if(str(strJobId)=='0'):
                    raise Exception("Failed to trigger backup")
                
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                logHandler.logging.info("Backup job status: " + str(backupJobStatus))
            
                if(backupJobStatus!="Successful"):
                    raise Exception("Backup not successful")                
                else:
                    logHandler.logging.info("Backup successful..")
                    reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
                
                # Cleanup
                logHandler.logging.info("Thread sleep for 60 sec..")
                time.sleep(60)
                
                joborders.deleteJoborders(token, joid)
                logHandler.logging.info("Job order has been deleted..")
                reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    

        finally:
            if(clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)  
        
            reportController.generateReportSummary(self.executionId)
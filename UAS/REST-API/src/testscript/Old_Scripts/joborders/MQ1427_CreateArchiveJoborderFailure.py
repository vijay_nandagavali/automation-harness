'''
Created on May 25, 2015

@author: rahula
'''
import unittest, time
from resourcelibrary import joborders, login, logout, clients, inventory
from testdata import joborders_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders, vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1427_CreateArchiveJoborderFailure(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Creation of archive job with client fails as no backup type is specified")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Creation of archive job with client fails as no backup type is specified")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            
            #Find if any client with same name is already available in DB. If not available, add the client.
            cid = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client returned from DB: " + str(cid))
            clientAdded = 0
            if(cid==0):
                # Step 1: Send request to update a client
                clientAdded = 1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                     clients_testdata.existing_credential)
                logHandler.logging.info("Add client API - Actual Response: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                    
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client returned from response: " + str(cid))
                
                # Verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully..")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
            
            #Find if any job orders are already available in DB with the same name. If so, delete the same.
            joid=vjoborders.findJobOrderByName(joborders_testdata.archiveName)
            logHandler.logging.info("JOID returned from DB: " + str(joid))
            if(joid):
                joid = str(joid)+"a"
                joborders.deleteJoborders(token, str(joid))
                logHandler.logging.info("Already available joborder with the same name is deleted..")
                reportController.addTestStepResult("Delete the schedule" , ExecutionStatus.PASS)
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")          
            
            instanceIdList = []
            instanceIdList.append(instanceId)
            
            mcDateTime = vjoborders.getDateTimeFromMachine()
            logHandler.logging.info("Machine date time: " + str(mcDateTime))
            
            start_date = mcDateTime[0]
            start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
            logHandler.logging.info("Start Date: " + str(start_date))
            logHandler.logging.info("Start Time: " + str(start_time))
            
            logHandler.logging.info("Creation of a new archive job order should fail as no backup type is specified.....")
            actualResponse = joborders.createNewJoborder(token, joborders_testdata.archiveName, joborders_testdata.archiveType, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
            logHandler.logging.info("Actual response from API: " + str(actualResponse))
            reportController.addTestStepResult("Send API request for POST joborder" , ExecutionStatus.PASS)
            
            #Verification 1: Verify the response code for the post request
            responseResult = joborders.parseErrorResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            
            responseCode = responseResult["result"][0]["code"]
            if(responseCode == '0'):
                logHandler.logging.info("Verified the response code")
                reportController.addTestStepResult("Verified the response code", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Failed to verify the response code")
                reportController.addTestStepResult("Failed to verify the response code", ExecutionStatus.FAIL)  
            
            strError = responseResult["result"][0]["message"]
            logHandler.logging.info("Error message: " + str(strError))
            if(strError == joborders_testdata.archiveError):
                logHandler.logging.info("Verified the error message" + strError)
                reportController.addTestStepResult("Verified the error message" + strError, ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Failed to verify the error message" + "Expected - " + str(joborders_testdata.archiveError) + "Actual - " + strError)
                reportController.addTestStepResult("Failed to verify the error message", ExecutionStatus.PASS)  
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            if (clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Client not added in this test. Hence cannot be deleted..")
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)

            
            reportController.generateReportSummary(self.executionId)
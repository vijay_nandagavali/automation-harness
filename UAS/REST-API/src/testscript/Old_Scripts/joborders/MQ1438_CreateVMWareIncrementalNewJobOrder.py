'''
Created on May 26, 2015

@author: garima.g
'''


import unittest, time
from resourcelibrary import joborders, login, logout, clients, jobs, inventory
from testdata import joborders_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders, vclients, vjobs, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1438_CreateVMWareIncrementalNewJobOrder(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Create a new VMware Incremental joborder")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Create New VMware Incremental Job Order")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Test data used in the script
            vmName=clients_testdata.getmachineName("VM1")
            vmIp=clients_testdata.getmachineIP("VM1")
            instanceName=clients_testdata.getShareName("VM1")
            vmType = clients_testdata.getType("VM1")
            vmUsername = clients_testdata.getUserName("VM1")
            vmPassword = clients_testdata.getPassword("VM1")
            esxName = clients_testdata.getmachineName("ESX-Server")
            
            
            #Login
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            uuid = vclients.getSpecificEsxServerFromDB(esxName)
            logHandler.logging.info("UUID returned from DB: " + str(uuid))
          
            if(uuid==0):
                # Step 1: Send request to update a client
                addedClientFlag =1
                actualResponse = clients.postVMClient(token,clients_testdata.sid, vmName, vmType, 
                                                clients_testdata.vm_priority, clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, clients_testdata.vm_use_ssl, 
                                                clients_testdata.vm_defaultschedule, clients_testdata.vm_port, vmIp, 
                                                clients_testdata.vm_existing_credential, vmUsername, vmPassword, 
                                                clients_testdata.vm_domain)
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS) 
                
                responseResult = clients.getVMPostClientRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))                            
                uuid = vclients.getSpecificEsxServerFromDB(esxName)                
            
            #Find if any job orders are already available in DB with the same name. If so, delete the same.
            fjoid=vjoborders.findJobOrderByName(joborders_testdata.jobOrderNameVmWareFull)
            logHandler.logging.info("JOID returned from DB: " + str(fjoid))
            if(fjoid):
                fjoid = str(fjoid)+"b"
                joborders.deleteJoborders(token, str(fjoid))
                logHandler.logging.info("Already available joborder with the same name is deleted..")
                reportController.addTestStepResult("Delete the schedule" , ExecutionStatus.PASS)
            
            #Find if any job orders are already available in DB with the same name. If so, delete the same.
            ijoid=vjoborders.findJobOrderByName(joborders_testdata.jobOrderNameVmWareIncre)
            logHandler.logging.info("JOID returned from DB for Incremental Joborder: " + str(ijoid))
            if(ijoid):
                ijoid = str(ijoid)+"b"
                joborders.deleteJoborders(token, str(ijoid))
                logHandler.logging.info("Already available joborder with the same name is deleted..")
                reportController.addTestStepResult("Delete the VMWare Incremental joborder" , ExecutionStatus.PASS)
                
            logHandler.logging.info("Thread sleep for 30 sec..")
            time.sleep(30)
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromVmVMWare(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
                        
            instanceIdList = []
            instanceIdList.append(instanceId)
            
            vmWareFullBackups = vbackups.getFullVMWareBackupsFromDB(instanceId)
            logHandler.logging.info("VMware full backups returned from DB: " + str(vmWareFullBackups))
            
            fullJoborderAdded = 0
            if(len(vmWareFullBackups) == 0):
                mcDateTime = vjoborders.getDateTimeFromMachine()
                logHandler.logging.info("Machine date time: " + str(mcDateTime))
                
                start_date = mcDateTime[0]
                start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
                logHandler.logging.info("Start Date: " + str(start_date))
                logHandler.logging.info("Start Time: " + str(start_time))
                
                logHandler.logging.info("Creating a new job order.....")
                actualResponse = joborders.createNewJoborder(token, joborders_testdata.jobOrderNameVmWareFull, joborders_testdata.type, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
                logHandler.logging.info("Actual response from API: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for POST joborder" , ExecutionStatus.PASS)                
                                    
                # Get actual response parameter from response
                fjoid = joborders.getPostRequestResponse(actualResponse)
                logHandler.logging.info("JOID from response: " + str(fjoid))
                if (fjoid): 
                    logHandler.logging.info("Job order created successfully..")
                    reportController.addTestStepResult("Joborder Created Successfully", ExecutionStatus.PASS) 
                else:
                    raise Exception("Failed to create Joborder")  
                
                # Verify the response values against DB              
                jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(fjoid).replace('b', ""))
                logHandler.logging.info("JOID count from DB: " + str(jorderCountFromDB))
                if (jorderCountFromDB !=0):
                    logHandler.logging.info("Database verification successful")
                    reportController.addTestStepResult("Database Verification", ExecutionStatus.PASS)
                else:
                    raise Exception ("Joborder not created for VMware Full Backup")
                
                fullJoborderAdded = 1
                
                fjoid = str(fjoid).replace('b', '')
                
                logHandler.logging.info("Thread sleep for 120 sec..")
                time.sleep(120)
                strJobId = vjobs.getJobIdFromJoborder(fjoid)
                #print strJobId
                logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
                if(strJobId =='0'):
                    raise Exception("Failed to trigger Full backup")   
                    
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                #print backupJobStatus
                logHandler.logging.info("Backup job status: " + str(backupJobStatus))
                
                if(backupJobStatus!="Successful"):
                    raise Exception("Full Backup not successful")
                else:
                    logHandler.logging.info("Full Backup successful..")
                    reportController.addTestStepResult("Full Backup Successful" , ExecutionStatus.PASS)   
                
                bid=vbackups.getBackupId(strJobId)
                logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))                    
                
                fjoid = str(fjoid)+"b"
                joborders.deleteJoborders(token, fjoid)
                logHandler.logging.info("Full Job order created in this test has been deleted..")
                reportController.addTestStepResult("Full Job order deleted" , ExecutionStatus.PASS)
                
            mcDateTime = vjoborders.getDateTimeFromMachine()
            logHandler.logging.info("Machine date time: " + str(mcDateTime))
                
            start_date = mcDateTime[0]
            start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
            logHandler.logging.info("Start Date: " + str(start_date))
            logHandler.logging.info("Start Time: " + str(start_time))
            
            logHandler.logging.info("Creating a new job order for VMware Incremental Backup.....")
            actualResponse = joborders.createNewJoborder(token, joborders_testdata.jobOrderNameVmWareIncre, joborders_testdata.type, start_date, start_time, "Incremental", joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
            logHandler.logging.info("Actual response from API: " + str(actualResponse))
            reportController.addTestStepResult("Send API request for POST VMware incremental joborder" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            ijoid = joborders.getPostRequestResponse(actualResponse)
            logHandler.logging.info("JOID from response: " + str(ijoid))
            if (ijoid): 
                logHandler.logging.info("Job order created successfully for VMware Incremental Backup..")
                reportController.addTestStepResult("Joborder Created Successfully for VMware Incremental Backup", ExecutionStatus.PASS) 
            else:
                raise Exception("Failed to create Joborder for VMware Incremental Backup")  
                
            # Verify the response values against DB              
            jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(ijoid).replace('b', ""))
            logHandler.logging.info("JOID count from DB: " + str(jorderCountFromDB))
            if (jorderCountFromDB !=0):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database Verification", ExecutionStatus.PASS)
            else:
                raise Exception ("Joborder not created for VMware Incremental Backup")
            
            ijoid = str(ijoid).replace('b', '')
                
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
            strJobId = vjobs.getJobIdFromJoborder(ijoid)
            logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
            
            if(strJobId =='0'):
                raise Exception("Failed to trigger Incremental backup")   
            if(strJobId =='0'):
                raise Exception("Failed to trigger backup")   
                
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            logHandler.logging.info("Backup job status: " + str(backupJobStatus))
            
            if(backupJobStatus!="Successful"):
                raise Exception("Backup not successful")
            else:
                logHandler.logging.info("Backup successful..")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)   
            
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
                                  
            # Cleanup
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
                      
            joid = str(ijoid)+"b"
            joborders.deleteJoborders(token, joid)
            logHandler.logging.info("Job order created in this test has been deleted..")
            reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            if (addedClientFlag == 1 or result == 'Fail'):
                clients.deleteVMClient(token, uuid)
                logHandler.logging.info("Client already existing is deleted..")
                reportController.addTestStepResult("Delete existing client", ExecutionStatus.PASS)
                            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)

            reportController.generateReportSummary(self.executionId)
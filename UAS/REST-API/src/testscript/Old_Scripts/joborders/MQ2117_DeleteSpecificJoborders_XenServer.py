'''
Created on Nov 5, 2015

@author: Prateek
'''
import unittest, time
from resourcelibrary import joborders, login, logout, jobs, inventory, clients
from testdata import joborders_testdata, backups_testdata, inventory_testdata, clients_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders, vbackups, vjobs, vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from time import sleep

class MQ2117_DeleteSpecificJoborders_XenServer(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete the specified joborder by id for Xen Server.")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Specific Joborder for Xen Server")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            clientAdded = 0
            # Step 1: Create a new joborder
            id = vjoborders.findJobOrderByName(joborders_testdata.name)
            logHandler.logging.info("Schedule ID returned from DB: " + str(id))
            id=str(id)+"b"
            if(id=="0b"):
                cid = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
                logHandler.logging.info("Client ID from DB: " + str(cid))
                if(cid==0):
                    clientAdded = 1
                    # Step 1: Send request to update a client
                    actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver,clients_testdata.system_xenserver,
                                                        clients_testdata.ip_xenserver, clients_testdata.username_xenserver, clients_testdata.password_xenserver, 
                                                        clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, clients_testdata.is_synchable_xenserver,
                                                        clients_testdata.is_auth_enabled_xenserver)
                    logHandler.logging.info("Actual Response for Add client: " + str(actualResponse))
                    reportController.addTestStepResult("Send API request for Add client" , ExecutionStatus.PASS)
                    
                    # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                    cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                    logHandler.logging.info("Client ID from Response: " + str(cid))
                    # Verification 1: verify the response parameter
                    if (cid):
                        logHandler.logging.info("Client added successfully...")
                        reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                    else:
                        logHandler.logging.error("Failed to add client")
                        raise Exception ("Failed to add a client")
                    
                result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
                logHandler.logging.info("Result: " + str(result))
                self.assertNotEqual(result,"Fail","Inventory sync failed")
                reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                sleep(60)
                            
                instanceId = vbackups.getInstanceIdFromVm(backups_testdata.xenserverInstanceName)
                logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
                self.assertNotEqual(instanceId,None,"Instance Id not found")
                reportController.addTestStepResult("Instance ID found", ExecutionStatus.PASS)
                
                instancIDList = []
                instancIDList.append(instanceId) 
                
                mcDateTime = vjoborders.getDateTimeFromMachine()
                logHandler.logging.info("Machine date time: " + str(mcDateTime))
            
                start_date = mcDateTime[0]
                start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
                logHandler.logging.info("Start Date: " + str(start_date))
                logHandler.logging.info("Start Time: " + str(start_time))
                            
                actualResponse = joborders.createNewJoborder(token, joborders_testdata.name, joborders_testdata.type, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instancIDList)
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                # Get actual response parameter from response
                id = joborders.getPostRequestResponse(actualResponse)
                logHandler.logging.info("ID returned from response: " + str(id))
            
                reportController.addTestStepResult("Create New Joborder", ExecutionStatus.PASS)
                
                logHandler.logging.info("Thread sleep for 120 sec..")
                time.sleep(120)
                
                strJobId = vjobs.getJobIdFromJoborder(str(id).replace("b", ""))
                logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
                if(str(strJobId)=='0'):
                    raise Exception("Failed to trigger backup")
                
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                #print backupJobStatus
                logHandler.logging.info("Backup job status: " + str(backupJobStatus))
            
                if(backupJobStatus =="Successful" or backupJobStatus =="Warning" or backupJobStatus == ""):
                    logHandler.logging.info("Backup successful")
                    reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
                else:
                    raise Exception("Backup not successful") 
                
                
                
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
            
            actualResponse = joborders.deleteJoborders(token, id)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            responseResult = joborders.getDeleteRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            # Verification 2: Verify get all alerts API response                  
            joFromDB = vjoborders.getSpecificJoborderFromDB(str(id).replace('b', ''))
            logHandler.logging.info("Data returned from DB: " + str(joFromDB))
            if (len(joFromDB) == 0):
                reportController.addTestStepResult("Database verification successful", ExecutionStatus.PASS)
            else:
                raise Exception ("Database verification failed")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup
            if(clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)

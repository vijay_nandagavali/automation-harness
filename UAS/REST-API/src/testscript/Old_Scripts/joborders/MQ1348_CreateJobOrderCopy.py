'''
Created on Apr 14, 2015

@author: Savitha
'''
import unittest

from util import reportController
from config.Constants import ExecutionStatus
from config import apiconfig

from resourcelibrary import joborders, login, logout
from testdata import joborders_testdata
from verificationsript import vjoborders


class MQ1348_CreateJobOrderCopy(unittest.TestCase):
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Create a copy of the joborder")
        self.executionId = exeID
    
    def runTest(self):
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            
            # Hard coding the value of job_id (joid) as Host backup is currently not supported by API
            # Note: Job order copy works only for Host backup, but not for instance backup
            joid = "44b"

            # Send request for creating job order copy
            actualResponse = joborders.createJobOrderCopy(token, joid)
            print actualResponse
            reportController.addTestStepResult("Send API request for job order copy", ExecutionStatus.PASS)
            
            # Verify the response 
            joidcopy = joborders.getPostRequestResponse(actualResponse)
            print joidcopy
            if (joidcopy):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            # verify the data against database
            jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(joidcopy).replace('b', ""))
            print jorderCountFromDB
            if (jorderCountFromDB !=0):
                reportController.addTestStepResult("Joborder copy successfully created", ExecutionStatus.PASS)
            else:
                raise Exception ("Joborder copy not created")
            
            # Cleanup
            actualResponse = joborders.deleteJoborders(token, joidcopy)
             
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
         
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
              
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e
                    
        finally:
            reportController.generateReportSummary(self.executionId)
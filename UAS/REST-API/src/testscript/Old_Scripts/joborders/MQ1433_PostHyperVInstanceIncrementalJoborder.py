'''
Created on May 25, 2015

@author: Savitha Peri
'''
import unittest, time
from resourcelibrary import joborders, login, logout, clients, jobs, inventory
from testdata import joborders_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders, vclients, vjobs, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1433_PostHyperVInstanceIncrementalJoborder(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Create a new joborder for HyperV Incremental Backup")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - HyperV Incremental Job order")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Find if any client with same name is already available in DB. If not available, add the client.
            cid = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client returned from DB: " + str(cid))
            clientAdded = 0
            if(cid==0):
                # Step 1: Send request to update a client
                clientAdded = 1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                     clients_testdata.existing_credential)
                logHandler.logging.info("Add client API - Actual Response: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                    
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client returned from response: " + str(cid))
                
                # Verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully..")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            instanceIdList = []
            instanceIdList.append(instanceId)
                  
            #Find if any job orders are already available in DB with the same name. If so, delete the same.
            fjoid=vjoborders.findJobOrderByName(joborders_testdata.name)
            logHandler.logging.info("JOID returned from DB for Full Joborder: " + str(fjoid))
            if(fjoid):
                fjoid = str(fjoid)+"b"
                joborders.deleteJoborders(token, str(fjoid))
                logHandler.logging.info("Already available joborder with the same name is deleted..")
                reportController.addTestStepResult("Delete the HyperV Full joborder" , ExecutionStatus.PASS)
            
            #Find if any job orders are already available in DB with the same name. If so, delete the same.
            ijoid=vjoborders.findJobOrderByName(joborders_testdata.incrName)
            logHandler.logging.info("JOID returned from DB for Incremental Joborder: " + str(ijoid))
            if(ijoid):
                ijoid = str(ijoid)+"b"
                joborders.deleteJoborders(token, str(ijoid))
                logHandler.logging.info("Already available joborder with the same name is deleted..")
                reportController.addTestStepResult("Delete the HyperV Incremental joborder" , ExecutionStatus.PASS)
            
            hypervFullBackups = vbackups.getFullHyperVBackupsFromDB(cid, instanceId)
            logHandler.logging.info("HyperV full backups returned from DB: " + str(hypervFullBackups))
            
            fullJoborderAdded = 0
            if(len(hypervFullBackups) == 0):
                mcDateTime = vjoborders.getDateTimeFromMachine()
                logHandler.logging.info("Machine date time: " + str(mcDateTime))
                
                start_date = mcDateTime[0]
                start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
                logHandler.logging.info("Start Date: " + str(start_date))
                logHandler.logging.info("Start Time: " + str(start_time))
                
                logHandler.logging.info("Creating a new job order.....")
                actualResponse = joborders.createNewJoborder(token, joborders_testdata.name, joborders_testdata.type, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
                logHandler.logging.info("Actual response from API: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for POST HyperV full joborder" , ExecutionStatus.PASS)
                
                # Get actual response parameter from response
                fjoid = joborders.getPostRequestResponse(actualResponse)
                logHandler.logging.info("JOID from response: " + str(fjoid))
                if (fjoid): 
                    logHandler.logging.info("Job order created successfully for HyperV Full Backup..")
                    reportController.addTestStepResult("Joborder Created Successfully for HyperV Full Backup", ExecutionStatus.PASS) 
                else:
                    raise Exception("Failed to create Joborder for HyperV Full Backup")  
                
                # Verify the response values against DB              
                jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(fjoid).replace('b', ""))
                logHandler.logging.info("JOID count from DB: " + str(jorderCountFromDB))
                if (jorderCountFromDB !=0):
                    logHandler.logging.info("Database verification successful")
                    reportController.addTestStepResult("Database Verification", ExecutionStatus.PASS)
                else:
                    raise Exception ("Joborder not created for HyperV Full Backup")
                
                fullJoborderAdded = 1
                
                fjoid = str(fjoid).replace('b', '')
                
                logHandler.logging.info("Thread sleep for 120 sec..")
                time.sleep(120)
                strJobId = vjobs.getJobIdFromJoborder(fjoid)
                #print strJobId
                logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
                if(strJobId =='0'):
                    raise Exception("Failed to trigger Full backup")   
                    
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                #print backupJobStatus
                logHandler.logging.info("Backup job status: " + str(backupJobStatus))
                
                if(backupJobStatus!="Successful"):
                    reportController.addTestStepResult("Full Backup not successful", ExecutionStatus.FAIL)
                    raise Exception("Full Backup not successful")
                    
                else:
                    logHandler.logging.info("Full Backup successful..")
                    reportController.addTestStepResult("Full Backup Successful" , ExecutionStatus.PASS)   
                
                bid=vbackups.getBackupId(strJobId)
                #print bid
                logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
            
            mcDateTime = vjoborders.getDateTimeFromMachine()
            logHandler.logging.info("Machine date time: " + str(mcDateTime))
                
            start_date = mcDateTime[0]
            start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
            logHandler.logging.info("Start Date: " + str(start_date))
            logHandler.logging.info("Start Time: " + str(start_time))
            
            logHandler.logging.info("Creating a new job order for HyperV Incremental Backup.....")
            actualResponse = joborders.createNewJoborder(token, joborders_testdata.incrName, joborders_testdata.type, start_date, start_time, "Incremental", joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
            logHandler.logging.info("Actual response from API: " + str(actualResponse))
            reportController.addTestStepResult("Send API request for POST HyperV incremental joborder" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            ijoid = joborders.getPostRequestResponse(actualResponse)
            logHandler.logging.info("JOID from response: " + str(ijoid))
            if (ijoid): 
                logHandler.logging.info("Job order created successfully for HyperV Incremental Backup..")
                reportController.addTestStepResult("Joborder Created Successfully for HyperV Incremental Backup", ExecutionStatus.PASS) 
            else:
                raise Exception("Failed to create Joborder for HyperV Incremental Backup")  
                
            # Verify the response values against DB              
            jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(ijoid).replace('b', ""))
            logHandler.logging.info("JOID count from DB: " + str(jorderCountFromDB))
            if (jorderCountFromDB !=0):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database Verification", ExecutionStatus.PASS)
            else:
                raise Exception ("Joborder not created for HyperV Incremental Backup")
            
            ijoid = str(ijoid).replace('b', '')
                
            logHandler.logging.info("Thread sleep for 120 sec..")
            time.sleep(120)
            strJobId = vjobs.getJobIdFromJoborder(ijoid)
            logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
            
            if(strJobId =='0'):
                raise Exception("Failed to trigger Incremental backup")   
                    
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            logHandler.logging.info("Backup job status: " + str(backupJobStatus))
                
            if(backupJobStatus!="Successful"):
                reportController.addTestStepResult("Incremental Backup not successful", ExecutionStatus.FAIL)
                raise Exception("Incremental Backup not successful")
            else:
                logHandler.logging.info("Incremental Backup successful..")
                reportController.addTestStepResult("Incremental Backup Successful" , ExecutionStatus.PASS)   
                
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))

            # Cleanup
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
            
            ijoid = str(ijoid)+"b"
            joborders.deleteJoborders(token, ijoid)
            logHandler.logging.info("Job order created in this test has been deleted..")
            reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)
            
            if(fullJoborderAdded):
                fjoid = str(fjoid)+"b"
                joborders.deleteJoborders(token, fjoid)
                logHandler.logging.info("Full Job order created in this test has been deleted..")
                reportController.addTestStepResult("Full Job order deleted" , ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            if (clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)

            reportController.generateReportSummary(self.executionId)
'''
Created on Aug 20, 2015

@author: Nikhil S
'''

import unittest, time
from resourcelibrary import joborders, login, logout, clients, jobs, inventory, backups
from testdata import joborders_testdata, clients_testdata, backups_testdata, inventory_testdata,replication_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders, vclients, vjobs, vbackups, vreplication
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1894_PostCreateReplicationJoborder(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Create a new joborder for Replication")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Create New Job Order for Replication")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            replicationTarget = clients_testdata.getmachineName("ReplicationTarget")
            
            #Login
            joid = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Find if any client with same name is already available in DB. If not available, add the client.
            cid = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client returned from DB: " + str(cid))
            clientAdded = 0
            if(cid==0):
                # Step 1: Send request to update a client
                clientAdded = 1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                     clients_testdata.existing_credential)
                logHandler.logging.info("Add client API - Actual Response: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                    
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client returned from response: " + str(cid))
                
                # Verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully..")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            instanceIdList = []
            instanceIdList.append(instanceId)
            
            targetID = vreplication.getTargetIDFromDB(replicationTarget)
            logHandler.logging.info("Target ID is : " + str(targetID))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
                       
            #Find if any job orders are already available in DB with the same name. If so, delete the same.
            joid=vjoborders.findReplicationJobOrderByName(joborders_testdata.replicationJobName)
            logHandler.logging.info("Replication JOB ID returned from DB: " + str(joid))
            if(joid):
                joid = str(joid)+"x"
                joborders.deleteJoborders(token, str(joid))
                logHandler.logging.info("Already available joborder with the same name is deleted..")
                reportController.addTestStepResult("Delete the existing Job order" , ExecutionStatus.PASS)
            
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
            
            logHandler.logging.info("Creating a new job order.....")
            actualResponse = joborders.createNewReplicationJoborder(token,instanceIdList, targetID, joborders_testdata.replicationJobName, joborders_testdata.replicationType)
            logHandler.logging.info("Actual response from API: " + str(actualResponse))
            reportController.addTestStepResult("Send API request for POST joborder" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            joid = joborders.getPostRequestResponse(actualResponse)
            logHandler.logging.info("JOID from response: " + str(joid))
            if (joid): 
                logHandler.logging.info("Job order created successfully..")
                reportController.addTestStepResult("Joborder Created Successfully", ExecutionStatus.PASS) 
            else:
                raise Exception("Failed to create Joborder")              
            
            # Verify the response values against DB              
            jorderCountFromDB = vjoborders.getAddedReplicationJoborderFromDB(str(joid).replace('x', ""))
            logHandler.logging.info("JOBID count from DB: " + str(jorderCountFromDB))
            if (jorderCountFromDB !=0):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database Verification", ExecutionStatus.PASS)
            else:
                raise Exception ("Joborder not created")
                    
            joid = str(joid).replace('x', '')
            
            #trigger a backup
            triggerBackup = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            logHandler.logging.info("Backup triggered - " + str(triggerBackup))            
            reportController.addTestStepResult("Backup triggered" , ExecutionStatus.PASS)
            
            #Get joiID
            responseResult = backups.getPutRequestResponse(triggerBackup)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Backup Job ID from Response: " + str(strJobId))
            if(strJobId == '-1'):
                raise Exception("Failed to trigger backup")
                           
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            #print backupJobStatus
            logHandler.logging.info("Backup job status: " + str(backupJobStatus))
            
            if(backupJobStatus!="Successful"):
                raise Exception("Backup not successful")
                
            else:
                logHandler.logging.info("Backup successful..")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)   
            
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
           
            time.sleep(60)
                       
            replicationJoibID = vjobs.getReplicationJobID(bid) 
            replicationJoibID = str(replicationJoibID) + ".rep"
            logHandler.logging.info("Replication Job ID: " + str(replicationJoibID))
             
#             #Check the status of Replication
#             backupJobStatus=jobs.waitForJobCompletion(backups_testdata.replicationJobType, backups_testdata.waittime, token, replicationJoibID)
#             #print backupJobStatus
#             logHandler.logging.info("replication job status: " + str(backupJobStatus))
#             
#             if(backupJobStatus!="Successful"):
#                 raise Exception("replication not successful")
#                 
#             else:
#                 logHandler.logging.info("replication successful..")
#                 reportController.addTestStepResult("replication Successful" , ExecutionStatus.PASS)   
           
            replicationJobStatus = vjobs.getReplicationBackupStatus(bid)
            logHandler.logging.info("replication Job Status: " + str(replicationJobStatus))
            if(replicationJobStatus == 2):
                logHandler.logging.info("replication successful database verified..")
            else:    
                logHandler.logging.info("replication unsuccessful database verified..")
                raise Exception("replication not successful - Database")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e)) 
               
        finally:
            # Cleanup
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
                      
            joid = str(joid)+"x"
            joborders.deleteJoborders(token, joid)
            logHandler.logging.info("Job order created in this test has been deleted..")
            reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)
            
            if (clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
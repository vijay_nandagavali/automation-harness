'''
Created on May 25, 2015

@author: garima.g
'''

import unittest, time
from resourcelibrary import joborders, login, logout, clients, jobs, inventory
from testdata import joborders_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders, vclients, vjobs, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1437_CreateVMWareFullNewJobOrder(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Create a new VMWare Full joborder")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Create New VMWare Full Job Order")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            vmName=clients_testdata.getmachineName("VM1")
            vmIp=clients_testdata.getmachineIP("VM1")
            instanceName=clients_testdata.getShareName("VM1")
            vmType = clients_testdata.getType("VM1")
            vmUsername = clients_testdata.getUserName("VM1")
            vmPassword = clients_testdata.getPassword("VM1")
            esxName = clients_testdata.getmachineName("ESX-Server")
            
            #Login
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            uuid = vclients.getSpecificEsxServerFromDB(esxName)
            logHandler.logging.info("UUID returned from DB: " + str(uuid))
          
            if(uuid==0):
                # Step 1: Send request to update a client
                addedClientFlag =1
                actualResponse = clients.postVMClient(token,clients_testdata.sid, vmName, vmType, 
                                                clients_testdata.vm_priority, clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, clients_testdata.vm_use_ssl, 
                                                clients_testdata.vm_defaultschedule, clients_testdata.vm_port, vmIp, 
                                                clients_testdata.vm_existing_credential, vmUsername, vmPassword, 
                                                clients_testdata.vm_domain)
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS) 
                
                responseResult = clients.getVMPostClientRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))                            
                uuid = vclients.getSpecificEsxServerFromDB(esxName)                
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromVmVMWare(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            instanceIdList = []
            instanceIdList.append(instanceId)
            
            #Find if any job orders are already available in DB with the same name. If so, delete the same.
            joid=vjoborders.findJobOrderByName(joborders_testdata.jobOrderNameVmWareFull)
            logHandler.logging.info("JOID returned from DB: " + str(joid))
            if(joid):
                joid = str(joid)+"b"
                joborders.deleteJoborders(token, str(joid))
                logHandler.logging.info("Already available joborder with the same name is deleted..")
                reportController.addTestStepResult("Delete the schedule" , ExecutionStatus.PASS)
            
            mcDateTime = vjoborders.getDateTimeFromMachine()
            logHandler.logging.info("Machine date time: " + str(mcDateTime))
            
            start_date = mcDateTime[0]
            start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
            logHandler.logging.info("Start Date: " + str(start_date))
            logHandler.logging.info("Start Time: " + str(start_time))
            
            logHandler.logging.info("Creating a new job order.....")
            actualResponse = joborders.createNewJoborder(token, joborders_testdata.jobOrderNameVmWareFull, joborders_testdata.type, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
            logHandler.logging.info("Actual response from API: " + str(actualResponse))
            reportController.addTestStepResult("Send API request for POST joborder" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            joid = joborders.getPostRequestResponse(actualResponse)
            logHandler.logging.info("JOID from response: " + str(joid))
            if (joid): 
                logHandler.logging.info("Job order created successfully..")
                reportController.addTestStepResult("Joborder Created Successfully", ExecutionStatus.PASS) 
            else:
                raise Exception("Failed to create Joborder")  
            
            # Verify the response values against DB              
            jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(joid).replace('b', ""))
            logHandler.logging.info("JOID count from DB: " + str(jorderCountFromDB))
            if (jorderCountFromDB !=0):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database Verification", ExecutionStatus.PASS)
            else:
                raise Exception ("Joborder not created")
                    
            joid = str(joid).replace('b', '')
            
            logHandler.logging.info("Thread sleep for 120 sec..")
            time.sleep(120)
            strJobId = vjobs.getJobIdFromJoborder(joid)
            #print strJobId
            logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
            if(strJobId =='0'):
                raise Exception("Failed to trigger backup")   
                
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            logHandler.logging.info("Backup job status: " + str(backupJobStatus))
            
            if(backupJobStatus!="Successful"):
                raise Exception("Backup not successful")
            else:
                logHandler.logging.info("Backup successful..")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)   
            
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
                                  
            # Cleanup
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
                      
            joid = str(joid)+"b"
            joborders.deleteJoborders(token, joid)
            logHandler.logging.info("Job order created in this test has been deleted..")
            reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            if (addedClientFlag == 1 or result == 'Fail'):
                clients.deleteVMClient(token, uuid)
                logHandler.logging.info("Client already existing is deleted..")
                reportController.addTestStepResult("Delete existing client", ExecutionStatus.PASS)
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
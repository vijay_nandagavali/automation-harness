'''
Created on May 26, 2015

@author: rahula
'''
import unittest, time
from resourcelibrary import joborders, login, logout, clients, inventory, jobs, archive, storage
from testdata import joborders_testdata, clients_testdata, backups_testdata, inventory_testdata, storage_testdata, archive_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders, vclients, vbackups, vjobs, varchive, vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1435_PostCreateArchiveJoborder(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Creation of a new archive joborder")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Creation of a new archive joborder")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageAddedFlag = 0            
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
            
            if(storageId == 0):
                logHandler.logging.info("Cloud Storage not available. Adding the same...")
                actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
                reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                
                storageId = vstorage.getStorageIdByName(cloudStorageName)
                logHandler.logging.info("Storage Id returned from DB: " + str(storageId))
                if (storageId):
                    logHandler.logging.info("Storage added successfully...")
                    reportController.addTestStepResult("Add Storage as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add cloud storage")
                storageAddedFlag = 1                
                logHandler.logging.info("Thread sleep for 30 sec")
                time.sleep(30)
                
            #Find if any client with same name is already available in DB. If not available, add the client.
            cid = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client returned from DB: " + str(cid))
            clientAdded = 0
            if(cid==0):
                # Step 1: Send request to update a client
                clientAdded = 1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                     clients_testdata.existing_credential)
                logHandler.logging.info("Add client API - Actual Response: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                    
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client returned from response: " + str(cid))
                
                # Verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully..")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
            
            #Find if any job orders are already available in DB with the same name. If so, delete the same.
            joid=vjoborders.findJobOrderByName(joborders_testdata.archiveName)
            logHandler.logging.info("JOID returned from DB: " + str(joid))
            if(joid):
                joid = str(joid)+"a"
                joborders.deleteJoborders(token, str(joid))
                logHandler.logging.info("Already available joborder with the same name is deleted..")
                reportController.addTestStepResult("Delete the schedule" , ExecutionStatus.PASS)
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")            
            
            instanceIdList = []
            instanceIdList.append(instanceId)
            
            #Check if the storage is online
            onlineStatus = ''
            onlineStatus = vstorage.getOnlineStatusFromDB(storageId)
            logHandler.logging.info("Online status returned from DB: " + str(onlineStatus))
            dbResult = ''
        
            if(onlineStatus == 'True'):
                logHandler.logging.info(str(cloudStorageName) + " Cloud Storage is already online..")
                reportController.addTestStepResult("Cloud storage already online", ExecutionStatus.PASS)                
            else:
                #Send API request
                actualResponse = storage.putOnlineStorage(storage_testdata.sid, token, storageId)
                reportController.addTestStepResult("Send API request for Put cloud storage online" , ExecutionStatus.PASS)
            
                dbResult = vstorage.getOnlineStatusFromDB(storageId)
                logHandler.logging.info("Online status returned from DB: " + str(dbResult))
                if (dbResult == 'True'):
                    logHandler.logging.info("Database verification successful")
                    reportController.addTestStepResult("Cloud Storage Online", ExecutionStatus.PASS)
                else:
                    raise Exception("Cloud Storage Offline")
              
            #Erase Cloud Storage
            mediaLabel = archive_testdata.cloud_archive_label
            actualResponse = archive.putPrepareArchiveMedia(token, cloudStorageName, mediaLabel)
            reportController.addTestStepResult("Send API request for Archive Media Prepare" , ExecutionStatus.PASS)
        
            #Verification 1: Verify the response parameter   
            responseResult = archive.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
        
            dbResult = varchive.getArchivePrepareStatusFromDB(cloudStorageName)
            logHandler.logging.info("DB Result: " + str(dbResult))
        
            if(dbResult == 0):
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Failed database verification - some records found in archive sets table")
            
            mcDateTime = vjoborders.getDateTimeFromMachine()
            logHandler.logging.info("Machine date time: " + str(mcDateTime))
            
            start_date = mcDateTime[0]
            start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
            logHandler.logging.info("Start Date: " + str(start_date))
            logHandler.logging.info("Start Time: " + str(start_time))
            
            logHandler.logging.info("Creation of a new archive job order.....")
            actualResponse = joborders.createNewArchiveJoborder(token, joborders_testdata.archiveName, joborders_testdata.archiveType, start_date, start_time, joborders_testdata.archiveType, cloudStorageName, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
            logHandler.logging.info("Actual response from API: " + str(actualResponse))
            reportController.addTestStepResult("Send API request for POST joborder" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            joid = joborders.getPostRequestResponse(actualResponse)
            logHandler.logging.info("JOID from response: " + str(joid))
            if (joid): 
                logHandler.logging.info("Job order created successfully..")
                reportController.addTestStepResult("Joborder Created Successfully", ExecutionStatus.PASS) 
            else:
                raise Exception("Failed to create Joborder")  
            
            # Verify the response values against DB              
            jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(joid).replace('a', ""))
            logHandler.logging.info("JOID count from DB: " + str(jorderCountFromDB))
            if (jorderCountFromDB !=0):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database Verification", ExecutionStatus.PASS)
            else:
                raise Exception ("Joborder not created")
                    
            joid = str(joid).replace('a', '')
            
            logHandler.logging.info("Thread sleep for 120 sec..")
            time.sleep(120)
            strJobId = vjobs.getJobIdFromJoborder(joid)
            #print strJobId
            logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
            
            if(strJobId != '0'):
                reportController.addTestStepResult("Triggered the backup copy (archive) job", ExecutionStatus.PASS)
            else:
                raise Exception("Failed to trigger a backup copy (archive) job")
            
            #Check the status of archive job
            archiveJobStatus=jobs.waitForJobCompletion("Archive", backups_testdata.waittime, token, strJobId)
            logHandler.logging.info("Archive Job status: " + str(archiveJobStatus))
            if(archiveJobStatus!="Successful"):
                raise Exception("Archive not successful")
            else:
                logHandler.logging.info("Archive successful")
                reportController.addTestStepResult("Archive successful" , ExecutionStatus.PASS)
                    
            if(joid):
                joid = str(joid)+"a"
                joborders.deleteJoborders(token, str(joid))
                logHandler.logging.info("Joborder created in this test is deleted..")
                reportController.addTestStepResult("Delete schedule" , ExecutionStatus.PASS)
                
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup
            if(storageAddedFlag == 1 or dbResult == 'False'):
                storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Delete cloud storage" , ExecutionStatus.PASS)
            
            if (clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)

            reportController.generateReportSummary(self.executionId)
'''
Created on Feb 9, 2015

@author: Sayali.k
'''

import unittest, time
from resourcelibrary import joborders, login, logout, clients, jobs, inventory
from testdata import joborders_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders,vclients,vjobs,vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1068_GetSpecificJoborders(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get the details of a joborder by its id on the system")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Specific job orders")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
                      
            joid = vjoborders.findJobOrderByName(joborders_testdata.name)
            logHandler.logging.info("JOID from DB: " + str(joid))
            joid=str(joid)+"b"
            
            clientAdded = 0
            if(joid=='0b'):
                cid = vclients.getSpecificClientByNameDB(clients_testdata.name)
                logHandler.logging.info("Client ID from DB: " + str(cid))
                if(cid==0):
                    clientAdded = 1
                    # Step 1: Send request to update a client
                    actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                     clients_testdata.existing_credential)
                    logHandler.logging.info("Actual Response for Add client: " + str(actualResponse))
                    reportController.addTestStepResult("Send API request for Add client" , ExecutionStatus.PASS)
                    
                    # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                    cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                    logHandler.logging.info("Client ID from Response: " + str(cid))
                    # Verification 1: verify the response parameter
                    if (cid):
                        logHandler.logging.info("Client added successfully...")
                        reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                    else:
                        logHandler.logging.error("Failed to add client")
                        raise Exception ("Failed to add a client")
                    
                result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
                logHandler.logging.info("Result: " + str(result))
                self.assertNotEqual(result,"Fail","Inventory sync failed")
                reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                
                instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
                logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
                self.assertNotEqual(instanceId,None,"Instance Id not found")
                reportController.addTestStepResult("Instance ID found", ExecutionStatus.PASS)
                
                instanceIdList = []
                instanceIdList.append(instanceId)
                
                mcDateTime = vjoborders.getDateTimeFromMachine()
                logHandler.logging.info("Machine date time: " + str(mcDateTime))
            
                start_date = mcDateTime[0]
                start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
                logHandler.logging.info("Start Date: " + str(start_date))
                logHandler.logging.info("Start Time: " + str(start_time))

                # Step 1: Create a new joborder
                actualResponse = joborders.createNewJoborder(token, joborders_testdata.name, joborders_testdata.type, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
                logHandler.logging.info("Actual response for create job order: " + str(actualResponse))
                
                # Get actual response parameter from response
                joid = joborders.getPostRequestResponse(actualResponse)
                logHandler.logging.info("JOID from response: " + str(joid))
                if (joid):
                    logHandler.logging.info("Joborder created successfully...")
                    reportController.addTestStepResult("Create New Joborder", ExecutionStatus.PASS)
                else:
                    logHandler.logging.error("Failed to create new joborder")
                    raise Exception ("Failed to Create New Joborder")
                
                joid = str(joid).replace('b', '')
            
                logHandler.logging.info("Thread sleep for 120 sec..")
                time.sleep(120)
                strJobId = vjobs.getJobIdFromJoborder(joid)
                logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
                if(strJobId =='0'):
                    raise Exception("Failed to trigger backup")   
                
                #Check the status of backup
                backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
                logHandler.logging.info("Backup job status: " + str(backupJobStatus))
            
                if(backupJobStatus!="Successful"):
                    reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL)
                    raise Exception("Backup not successful")
                
                else:
                    logHandler.logging.info("Backup successful..")
                    reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)   
            
                bid=vbackups.getBackupId(strJobId)
                logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
                
                joid = str(joid) + "b"
            logHandler.logging.info("Thread sleep for 15 sec..")
            time.sleep(15)
            
            actualResponse = joborders.getSpecificJoborders(token, str(joid))
            logHandler.logging.info("Actual response for Get specific joborder: " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = joborders.getSpecificJobordersParametersInJson(actualResponse)
            logHandler.logging.info("Actual response parameters: " + str(actualResponseParams))
            
            joborders_testdata.specificresponseParameter.sort()
                    
            self.assertListEqual(joborders_testdata.specificresponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(joborders_testdata.specificresponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            
            # Verification 2: Verify get all alerts API response       
            joListFromDB = vjoborders.getSpecificJoborderFromDB(str(joid).replace('b', ''))
            logHandler.logging.info("Job order list from DB: " + str(joListFromDB))
            
            joListFromResponse = vjoborders.getSpecificJoborderFromResponse(actualResponse)
            logHandler.logging.info("Job order list from response: " + str(joListFromResponse))
            
            self.assertListEqual(joListFromDB, joListFromResponse, "Failed Data base verification: Expected Response [" + str(joListFromDB) + "] and Actual Response [" + str(joListFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)  
            
            # Cleanup
            joid = str(joid)
            joborders.deleteJoborders(token, joid)
            logHandler.logging.info("Job order has been deleted..")
            reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)       
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            if(result == 'Fail' or clientAdded == 1):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on Oct 28, 2015

@author: Nikhil. S
'''

import unittest, time
from resourcelibrary import joborders, login, logout, clients, jobs, inventory
from testdata import joborders_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler, dateTimeHandler
from verificationsript import vjoborders, vclients, vjobs, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2086_POSTCreateNewJoborder_XenServer(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Create a new joborder XenServer")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Create New Job Order XenServer")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Find if any client with same name is already available in DB. If not available, add the client.
            cid = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
            logHandler.logging.info("Client returned from DB: " + str(cid))
            clientAdded = 0
            if(cid==0):
                # Step 1: Send request to update a client
                clientAdded = 1
                actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver, 
                                                       clients_testdata.system_xenserver, clients_testdata.ip_xenserver, 
                                                       clients_testdata.username_xenserver, clients_testdata.password_xenserver, 
                                                       clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, 
                                                       clients_testdata.is_synchable_xenserver, clients_testdata.is_auth_enabled_xenserver)
                logHandler.logging.info("Add client API - Actual Response: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                    
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client returned from response: " + str(cid))
                
                # Verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully..")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
                
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                
            instanceId = vbackups.getInstanceIdFromVm(backups_testdata.xenserverInstanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            instanceIdList = []
            instanceIdList.append(instanceId)
            
            #Find if any job orders are already available in DB with the same name. If so, delete the same.
            joid=vjoborders.findJobOrderByName(joborders_testdata.name)
            logHandler.logging.info("JOID returned from DB: " + str(joid))
            if(joid):
                joid = str(joid)+"b"
                joborders.deleteJoborders(token, str(joid))
                logHandler.logging.info("Already available joborder with the same name is deleted..")
                reportController.addTestStepResult("Delete the schedule" , ExecutionStatus.PASS)
            
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
            
            mcDateTime = vjoborders.getDateTimeFromMachine()
            logHandler.logging.info("Machine date time: " + str(mcDateTime))
            
            start_date = mcDateTime[0]
            start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
            logHandler.logging.info("Start Date: " + str(start_date))
            logHandler.logging.info("Start Time: " + str(start_time))
            
            logHandler.logging.info("Creating a new job order.....")
            actualResponse = joborders.createNewJoborder(token, joborders_testdata.name, joborders_testdata.type, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
            logHandler.logging.info("Actual response from API: " + str(actualResponse))
            reportController.addTestStepResult("Send API request for POST joborder" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            joid = joborders.getPostRequestResponse(actualResponse)
            logHandler.logging.info("JOID from response: " + str(joid))
            if (joid): 
                logHandler.logging.info("Job order created successfully..")
                reportController.addTestStepResult("Joborder Created Successfully", ExecutionStatus.PASS) 
            else:
                raise Exception("Failed to create Joborder")              
            
            # Verify the response values against DB              
            jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(joid).replace('b', ""))
            logHandler.logging.info("JOID count from DB: " + str(jorderCountFromDB))
            if (jorderCountFromDB !=0):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database Verification", ExecutionStatus.PASS)
            else:
                raise Exception ("Joborder not created")
                    
            joid = str(joid).replace('b', '')
            
            logHandler.logging.info("Thread sleep for 120 sec..")
            time.sleep(120)
            strJobId = vjobs.getJobIdFromJoborder(joid)
            logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
            if(strJobId =='0'):
                raise Exception("Failed to trigger backup")   
                
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.xenWaitTime, token, strJobId)
            #print backupJobStatus
            logHandler.logging.info("Backup job status: " + str(backupJobStatus))
            
            if(backupJobStatus!="Successful"):
                raise Exception("Backup not successful")
                
            else:
                logHandler.logging.info("Backup successful..")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)   
            
            bid=vbackups.getBackupId(strJobId)
            #print bid
            logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
                                  
            # Cleanup
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
                      
            joid = str(joid)+"b"
            joborders.deleteJoborders(token, joid)
            logHandler.logging.info("Job order created in this test has been deleted..")
            reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e)) 
               
        finally:
            if (clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
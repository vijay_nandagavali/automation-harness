'''
Created on Apr 15, 2015

@author: Savitha
'''

import unittest, time
from util import reportController, logHandler, dateTimeHandler
from config.Constants import ExecutionStatus
from config import apiconfig
from resourcelibrary import joborders, login, logout, clients, inventory, jobs
from testdata import joborders_testdata, inventory_testdata, clients_testdata, backups_testdata
from verificationsript import vjoborders, vclients, vbackups, vjobs

class MQ1357_PutRunJobOrder(unittest.TestCase):
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Run a job order")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Run Job order")
        self.executionId = exeID
        
    def runTest(self):
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth_Token: " + token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)       
                       
            # Retrieve the job id of non-active jobs from database
            joid = vjoborders.getNonActiveJobIdfromDB()
            logHandler.logging.info("JOID returned from DB: " + str(joid))
            clientAdded = 0
            if(joid <> 0):
                pass
            else:
                logHandler.logging.info("No Non Active job orders available. Creating a new job order..")
                cid = vclients.getSpecificClientByNameDB(clientName)
                logHandler.logging.info("Client ID returned from DB: " + str(cid))
                if(cid==0):
                    clientAdded = 1
                    # Step 1: Send request to update a client
                    actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                     clients_testdata.existing_credential)
                    reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                    
                    # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                    cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                    logHandler.logging.info("Client ID returned from response: " + str(cid))
                    # Verification 1: verify the response parameter
                    if (cid):
                        reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                    else:
                        raise Exception ("Failed to add a client")
                
                result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
                logHandler.logging.info("Result: " + str(result))
                self.assertNotEqual(result,"Fail","Inventory sync failed")
                reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                
                instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
                logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
                self.assertNotEqual(instanceId,None,"Instance Id not found")
                instanceIdList = []
                instanceIdList.append(instanceId)
                
                mcDateTime = vjoborders.getDateTimeFromMachine()
                logHandler.logging.info("Machine date time: " + str(mcDateTime))
            
                start_date = mcDateTime[0]
                start_time = dateTimeHandler.getTimePlusTwoMinutes(mcDateTime[1])
                logHandler.logging.info("Start Date: " + str(start_date))
                logHandler.logging.info("Start Time: " + str(start_time))
                
                # Send API request for creating a new job order
                actualResponse = joborders.createNewJoborder(token, joborders_testdata.name, joborders_testdata.type, start_date, start_time, joborders_testdata.backup_type, joborders_testdata.storage, joborders_testdata.email_addresses, joborders_testdata.run_on, instanceIdList)
                logHandler.logging.info("Actual Response from Create New Job Order: " + str(actualResponse))  
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                 
                # Verify the response
                joid = joborders.getPostRequestResponse(actualResponse)
                logHandler.logging.info("JOID from Response: " + str(joid))
                if (joid):
                    reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed response verification. Job order not created..")            
             
                # Verify the new job order created against the Database 
                jorderCountFromDB = vjoborders.getAddedJoborderFromDB(str(joid).replace('b', ""))
                logHandler.logging.info("Job order count from DB: " + str(jorderCountFromDB))
                if (jorderCountFromDB !=0):
                    reportController.addTestStepResult("Joborder successfully created", ExecutionStatus.PASS)
                else:
                    raise Exception ("Joborder not created")
                 
                joid = str(joid).replace("b", "")
                
                logHandler.logging.info("Thread sleep for 30 sec....")
                time.sleep(30)
             
            # Send request for running a job order
            actualResponse = joborders.runJobOrder(token, str(joid) + "b")
            reportController.addTestStepResult("Send API request for job order copy", ExecutionStatus.PASS)
             
            # Verify the response 
            response = joborders.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response of Put Request: " + str(response))
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                         
            # verify the data against database
            statusFromDB = vjoborders.getJobRunStatusfromDB(joid)
            logHandler.logging.info("Job run status returned from DB: " + str(statusFromDB))
            if (statusFromDB == 1):
                reportController.addTestStepResult("Run Joborder successful", ExecutionStatus.PASS)
            else:
                raise Exception ("Cannot run the Joborder")         
            strJobId = vjobs.getJobIdFromJoborder(str(joid).replace("b", ""))
            logHandler.logging.info("Job Number corresponding to the schedule created returned from DB: " + str(strJobId))
            if(strJobId =='0'):
                raise Exception("Failed to trigger backup")
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            logHandler.logging.info("Backup job status: " + str(backupJobStatus))
        
            if(backupJobStatus!="Successful"):
                reportController.addTestStepResult("Backup not successful", ExecutionStatus.FAIL)
                raise Exception("Backup not successful")
            else:
                logHandler.logging.info("Backup successful..")
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)   
        
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup Id corresponding to the Job Number %s: %s", str(strJobId), str(bid))
        
            logHandler.logging.info("Thread sleep for 60 sec..")
            time.sleep(60)
            
            #Cleanup
            joborders.deleteJoborders(token, str(joid)+"b")
            logHandler.logging.info("Job order has been deleted..")
            reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            if(clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
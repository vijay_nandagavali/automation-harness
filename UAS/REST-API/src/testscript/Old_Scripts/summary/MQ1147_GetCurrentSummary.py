'''
Created on Oct 22, 2014

@author: Amey.k
'''

import unittest
from resourcelibrary import summary, login, logout
from testdata import summary_testdata
from util import reportController, logHandler
from verificationsript import vsummary
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1147_GetCurrentSummary(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Get Current Summary")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Current Summary")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = summary.getSummaryCurrent(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = summary.getListOfSummaryParameterInJson(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            # sort expected response parameter list            
            summary_testdata.summarycurrentsresponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(summary_testdata.summarycurrentsresponseParameter))
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(summary_testdata.summarycurrentsresponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(summary_testdata.summarycurrentsresponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            summaryFromDB = vsummary.getSummaryCurrentFromDB()
            logHandler.logging.info("Data returned from DB: " + str(summaryFromDB))
            summaryFromResponse = vsummary.getSummaryCurrentFromResponse(actualResponse)
            logHandler.logging.info("Data from response: " + str(summaryFromResponse))
            
            if(summaryFromResponse[2] == 'n/a'):
                summaryFromResponse[2] = '0'
            self.assertListEqual(summaryFromDB, summaryFromResponse, "Failed Data base verification: Expected Response [" + str(summaryFromDB) + "] and Actual Response [" + str(summaryFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                       
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
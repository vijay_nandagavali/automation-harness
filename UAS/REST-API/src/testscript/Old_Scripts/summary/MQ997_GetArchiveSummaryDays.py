'''
Created on Oct 17, 2014

@author: Amey.k
'''
import unittest

from resourcelibrary import summary, login, logout
from testdata import summary_testdata
from util import reportController
from verificationsript import vsummary
from config.Constants import ExecutionStatus
from config import apiconfig



class MQ997_GetArchiveSummaryDays(unittest.TestCase):    
    def __init__(self, exeID):
           
        reportController.initializeReport(type(self).__name__, "Get Archive Summary Days")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = summary.getArchiveSummaryByDays(token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = summary.getArchiveSummaryDaysParameterInJson(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list
            archive_summary = ['archive_size', 'archive_speed']
            # print backup_summary           
            archive_summary.sort()
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(archive_summary, actualResponseParams, "Failed Response verification: Expected response [" + str(archive_summary) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        

            summarydayList = vsummary.getDateList(7)
            summaryFromResponse = vsummary.getArchiveSummaryDaysFromResponse(actualResponse)
            
            print summaryFromResponse
            count = 0
            for s in summarydayList:
                for item in summaryFromResponse:
                    if item in s:
                            count = count + 1
            print count
                        
#           self.assertListEqual(backupsummaryFromDB, backupsummaryFromResponse, "Failed Data base verification: Expected Response [" + str(backupsummaryFromDB) + "] and Actual Response [" + str(backupsummaryFromResponse) + "]")
            if(count >= 1 & count <= 14):
                reportController.addTestStepResult("Day range matches", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            


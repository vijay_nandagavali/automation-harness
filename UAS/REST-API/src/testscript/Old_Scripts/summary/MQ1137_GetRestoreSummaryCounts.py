'''
Created on Oct 2, 2014

@author: Amey.k
'''
import unittest

from resourcelibrary import summary, login, logout
from testdata import summary_testdata
from util import reportController
from verificationsript import vsummary
from config.Constants import ExecutionStatus
from config import apiconfig



class MQ1137_GetRestoreSummaryCounts(unittest.TestCase):
    def __init__(self, exeID):    
            
        reportController.initializeReport(type(self).__name__, "Get Restore Summary Counts")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = summary.getRestoreSummaryCounts(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = summary.getListOfSummaryParameterInJson(actualResponse)
            restoreParams = actualResponseParams[1:4]
            # print actualResponseParams            
            # sort expected response parameter list            
            summary_testdata.responseParameter.sort()
            print "rp: "+str(summary_testdata.responseParameter)
            restore_testdata = summary_testdata.responseParameter[13:16]
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(restore_testdata, restoreParams, "Failed Response verification: Expected response [" + str(restore_testdata) + "] and Actual parameter [" + str(restoreParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            summaryFromDB = vsummary.getSummaryCountsFromDB()
            restoresummaryFromDB = summaryFromDB[9:12]
            print restoresummaryFromDB
            summaryFromResponse = vsummary.getSummaryCountsFromResponse(actualResponse)
            restoresummaryFromResponse = summaryFromResponse[9:12]
            print restoresummaryFromResponse
            self.assertListEqual(restoresummaryFromDB, restoresummaryFromResponse, "Failed Data base verification: Expected Response [" + str(restoresummaryFromDB) + "] and Actual Response [" + str(restoresummaryFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)


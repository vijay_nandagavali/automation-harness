'''
Created on Oct 2, 2014

@author: Amey.k
'''
import unittest

from resourcelibrary import summary, login, logout
from testdata import summary_testdata
from util import reportController
from verificationsript import vsummary
from config.Constants import ExecutionStatus
from config import apiconfig



class MQ1143_GetBackupSummaryCounts(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Backup Summary Counts")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = summary.getBackupSummaryCounts(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = summary.getListOfSummaryParameterInJson(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            summary_testdata.responseParameter1.sort()
            print summary_testdata.responseParameter1
            backup_testdata = summary_testdata.responseParameter1
            print backup_testdata
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(backup_testdata, actualResponseParams, "Failed Response verification: Expected response [" + str(backup_testdata) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            backupsummaryFromDB = vsummary.getBackupSummaryCountsFromDB()
            
            print backupsummaryFromDB
            backupsummaryFromResponse = vsummary.getBackupSummaryCountsFromResponse(actualResponse)
            
            print backupsummaryFromResponse
            self.assertListEqual(backupsummaryFromDB, backupsummaryFromResponse, "Failed Data base verification: Expected Response [" + str(backupsummaryFromDB) + "] and Actual Response [" + str(backupsummaryFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            


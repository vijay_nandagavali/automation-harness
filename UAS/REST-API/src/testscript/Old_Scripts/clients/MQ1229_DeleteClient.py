'''
Created on Feb 12, 2015

@author: Rahul A.
'''

import unittest, time
from resourcelibrary import clients, login, logout, joborders
from testdata import clients_testdata
from util import reportController, logHandler
from verificationsript import vclients, vjoborders
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1229_DeleteClient(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Delete the client with the specified id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Specified Client")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            
            osType = clients_testdata.getType("HyperV")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            cid = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(cid))
            if(cid==0):
                # Step 1: Send request to update a client
#                 deletehost.deleteHostIfPresent()
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(cid))
                # Verification 1: verify the response parameter
                if (cid):
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
            
#             joborderCountFromDB = vjoborders.getJobordersCountFromDB()
#             logHandler.logging.info("Job order count from DB: " + str(joborderCountFromDB))
#             
#             if(joborderCountFromDB <> 0):
#                 #Delete job orders
#                 joids=vjoborders.getAllJobordersFromDB()
#                 logHandler.logging.info("JOIDs returned from DB: " + str(joids))
#                 
#                 if(len(joids) > 0):
#                     for i in range(0,len(joids)):
#                         joid = str(joids[i])+"b"
#                         joborders.deleteJoborders(token, joid)
#                         logHandler.logging.info("Job order " + str(joid[i]) + " is deleted..")
#                         reportController.addTestStepResult("Job order deleted" , ExecutionStatus.PASS)
#                 
#                 logHandler.logging.info("Thread sleep for 10 sec")
#                 time.sleep(10)
#             
            actualResponse = clients.deleteClients(token, cid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request for Delete client" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            responseResult = clients.getDeleteRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                logHandler.logging.info("Response parameter verification successful")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2: Fetch added client from db
            dbResult = vclients.getClientByIdFromDB(cid)
            logHandler.logging.info("DB Result: " + str(dbResult))
              
            if(dbResult):
                logHandler.logging.error("Database verification failed")
                reportController.addTestStepResult("Delete Client - Database verification failed", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Delete Client - Database verification passed", ExecutionStatus.PASS)   
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on Feb 12, 2015

@author: Rahul A.
'''

import unittest

from resourcelibrary import clients, login, logout
from testdata import clients_testdata
from util import reportController
#from util import deletehost
from verificationsript import vclients

from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1354_PutUpdateClient(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Update a client on the system")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            
            osType = clients_testdata.getType("HyperV")
            
            addedClientFlag =0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clientName)
            print clientId
            if(clientId==0):
                #deletehost.deleteHostIfPresent()
                addedClientFlag =1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
                print actualResponse
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                
                # Verification 1: verify the prerequisite
                if (clientId):
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
            # Step 1: Send request to update a client
            actualResponse = clients.putUpdateClient(token, clientId, clientName, clients_testdata.updateClientPriority)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
            responseResult = clients.getPutRequestResponse(actualResponse)
            print responseResult
            # Verification 1: verify the response parameter
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            dbResult = vclients.getClientNamePriorityFromDB(clientId)
            
            #Verification 2: Fetch client name and priority from DB and compare with test data used
            
            self.assertListEqual(dbResult, [clientName, clients_testdata.updateClientPriority], "Failed Data base verification: Expected Response [" + clientName +","+ str(clients_testdata.updateClientPriority) + "] and Actual Response [" + str(dbResult) + "]")             
            if(addedClientFlag):
                #cleanup
                clients.deleteClients(token, clientId) 
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)

            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
                 
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            


'''
Created on Oct 6, 2014

@author: Amey.k
'''


import unittest
from testdata import clients_testdata
from resourcelibrary import clients, login, logout, virtual_clients
from util import reportController
# from util import deletehost
from verificationsript import vclients, vvirtual_clients
from config.Constants import ExecutionStatus
from os.path import sys
from config import apiconfig



class MQ1322_GetParticularClientDetails(unittest.TestCase):    
    def __init__(self, exeID):
           
        reportController.initializeReport(type(self).__name__, "Get Particular Client Details")
        self.executionId = exeID

        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            
            osType = clients_testdata.getType("HyperV")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            cid = vclients.getSpecificClientByNameDB(clientName)
            print cid
            if(cid!=0):
                #Check if the virtual client is associated with this client
                vid = vvirtual_clients.getVirtualClientIDFromDB(str(cid))
                print vid
                #Delete the virtual client if associated with this client
                if(vid != 0):
                    actualResponse = virtual_clients.deleteVirtualClient(token, vid)
                clients.deleteClients(token, cid)
            #deletehost.deleteHostIfPresent()
            actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                    clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                    clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                    clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                    clients_testdata.existing_credential)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                
            # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
            cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            print cid
            # Step 1: Send request to get all alerts
            actualResponse = clients.getDetailsOfParticularClient(cid, token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)    
            
            # Get actual response parameter from response
            actualResponseParams = clients.getListOfActualResponseParameter(actualResponse)
            print actualResponseParams
            # sort expected response parameter list            
            clients_testdata.responseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(clients_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(clients_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
    #       # Verification 2: Verify get all alerts API response
            # count = vclients.getClientCountFromDB()        
            clientListFromDB = vclients.getSpecificClientDetailsFromDB(cid)
            print clientListFromDB
                
            clientListFromResponse = vclients.getSpecificClientDetailsFromResponse(actualResponse)
            print clientListFromResponse
            self.assertListEqual(clientListFromDB, clientListFromResponse, "Failed Data base verification: Expected Response [" + str(clientListFromDB) + "] and Actual Response [" + str(clientListFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
             
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)



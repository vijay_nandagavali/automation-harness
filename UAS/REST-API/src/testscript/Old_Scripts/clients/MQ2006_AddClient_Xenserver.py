'''
Created on Oct 12, 2015

@author: Prateek
'''
import unittest
from resourcelibrary import clients, login, logout, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2006_AddClient_Xenserver(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Add a client or host to the specified system for XenServer")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Add client for XenServer")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
              
            #Find if any client with same name is already available in DB. If not available, add the client.
            cid = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
            logHandler.logging.info("Client returned from DB: " + str(cid))
            clientAdded = 0
            if(cid==0):
                clientAdded = 1
                # Step 1: Send request to update a client
                actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver, clients_testdata.system_xenserver, clients_testdata.ip_xenserver, clients_testdata.username_xenserver, clients_testdata.password_xenserver, clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, clients_testdata.is_synchable_xenserver, clients_testdata.is_auth_enabled_xenserver)
                logHandler.logging.info("Add client API - Actual Response: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                    
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client returned from response: " + str(cid))
                
                # Verify the response parameter
                if (cid):
                    logHandler.logging.info("Client added successfully..")
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    logHandler.logging.error("Failed to add client")
                    raise Exception ("Failed to add a client")
            
                #Verification 2: Fetch added client from db
                dbResult = vclients.getClientByIdFromDB(cid)
                
                if(dbResult):
                    reportController.addTestStepResult("Client Add - Database verification passed", ExecutionStatus.PASS)
                else:
                    reportController.addTestStepResult("Client Add - Database verification failed", ExecutionStatus.FAIL)   
            
            #result = inventory.waitForInventorySync(token, 120, inventory_testdata.sid)
            #logHandler.logging.info("Result: " + str(result))
            #self.assertNotEqual(result,"Fail","Inventory sync failed")
            #reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                        
            #instanceId = vbackups.getInstanceIdFromVm(clients_testdata.instance_name_xenserver)
            #logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            #self.assertNotEqual(instanceId,None,"Instance Id not found")
            #reportController.addTestStepResult("Instance ID found", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e)) 
               
        finally:
            #cleanup
            if (clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
               
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)

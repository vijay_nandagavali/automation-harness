
'''
Created on Feb 12, 2015

@author: Rahul A.
'''

import unittest

from resourcelibrary import clients, login, logout
from testdata import clients_testdata
from util import reportController, logHandler
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1387_AddVMClient(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Add a client or host to the specified system")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Add VMware client")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            vmName=clients_testdata.getmachineName("VM1")
            vmIp=clients_testdata.getmachineIP("VM1")
            
            vmType = clients_testdata.getType("VM1")
            vmUsername = clients_testdata.getUserName("VM1")
            vmPassword = clients_testdata.getPassword("VM1")
            esxName = clients_testdata.getmachineName("ESX-Server")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            uuid = vclients.getSpecificEsxServerFromDB(esxName)
            logHandler.logging.info("UUID returned from DB: " + str(uuid))
            
            if(uuid):
                clients.deleteVMClient(token, uuid)
                logHandler.logging.info("Client already existing is deleted..")
                reportController.addTestStepResult("Delete existing client", ExecutionStatus.PASS)

            # Step 1: Send request to update a client
            actualResponse = clients.postVMClient(token,clients_testdata.sid, vmName, vmType, 
                                                clients_testdata.vm_priority, clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, clients_testdata.vm_is_auth_enabled, clients_testdata.vm_use_ssl, 
                                                clients_testdata.vm_defaultschedule, clients_testdata.vm_port, vmIp, 
                                                clients_testdata.vm_existing_credential, vmUsername, vmPassword, 
                                                clients_testdata.vm_domain)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            responseResult = clients.getVMPostClientRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
            
            
            uuId = vclients.getSpecificEsxServerFromDB(esxName)
            logHandler.logging.info("UUID returned from DB: " + str(uuId))
            if(uuId):
                reportController.addTestStepResult("Database verificaion", ExecutionStatus.PASS)
            else:
                raise Exception("Database verification failed")
               
            #cleanup
            clients.deleteVMClient(token, uuId)
            logHandler.logging.info("Client added in the test is deleted..")
            reportController.addTestStepResult("Delete client in the test", ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
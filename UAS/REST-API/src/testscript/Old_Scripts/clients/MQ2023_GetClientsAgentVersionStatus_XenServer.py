'''
Created on Oct 14, 2015

@author: Prateek
'''

import unittest
from testdata import clients_testdata
from resourcelibrary import clients, login, logout
from util import reportController, logHandler
from verificationsript import vclients
from config.Constants import ExecutionStatus
from os.path import sys
from config import apiconfig




class MQ2023_GetClientsAgentVersionStatus_XenServer(unittest.TestCase):    
    def __init__(self, exeID):
           
        reportController.initializeReport(type(self).__name__, "Get XenServer Clients Agent Version Status")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Get Particular XenServer Client Details")
        self.executionId = exeID

        
        
    def runTest(self):        
#         try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all client details and agent version status
            actualResponse = clients.getListOfAllClientsWithAgentVersionStatus(token, clients_testdata.sid)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = clients.getListOfActualResponseParameterAgentCheckSpecific(actualResponse)
            print actualResponseParams
            logHandler.logging.info("Actual Response Parameters: " + str(actualResponseParams))
            if len(actualResponseParams):
                # sort expected response parameter list            
                clients_testdata.responseParameter_AgentCheck.sort()
                # Verification 1: Verify get all agent check for client response parameter        
                self.assertListEqual(clients_testdata.responseParameter_AgentCheck, actualResponseParams, "Failed Response verification: Expected response [" + str(clients_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter for client agent check", ExecutionStatus.PASS)        
                # Verification 2: Verify get all agent check for client API response      
                clientListFromDB = vclients.getAgentCheckSpecificClientDetailsFromDB()
                logHandler.logging.info("Client List from DB: " + str(clientListFromDB))
                print clientListFromDB
                
                clientListFromResponse = vclients.getAgentCheckSpecificClientDetailsFromResponse(actualResponse)
                logHandler.logging.info("client List From Response: " + str(clientListFromResponse))
                print clientListFromResponse
                self.assertListEqual(clientListFromDB, clientListFromResponse, "Failed Data base verification: Expected Response [" + str(clientListFromDB) + "] and Actual Response [" + str(clientListFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
#         except Exception as e:
#             reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
#             
#             reportController.tcExecutionStatus(ExecutionStatus.FAIL)
#             print e    
#         finally:
            reportController.generateReportSummary(self.executionId)



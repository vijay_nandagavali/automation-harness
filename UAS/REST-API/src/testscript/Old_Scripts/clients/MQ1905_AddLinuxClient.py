'''
Created on Aug 27, 2015

@author: Garima.g
'''
import unittest
from resourcelibrary import clients, login, logout, inventory
from testdata import clients_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1905_AddLinuxClient(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Add a Linux client to the specified system")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Add Linux client")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            linuxName = clients_testdata.getmachineName("LINUX")
            osType = clients_testdata.getType("LINUX")
            linuxIp = clients_testdata.getmachineIP("LINUX")
            
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
              
            #Find if any client with same name is already available in DB. If not available, add the client.
            cid = vclients.getSpecificClientByNameDB(linuxName)
            logHandler.logging.info("Client returned from DB: " + str(cid))
            #clientAdded = 1
            if(cid!=0):
                clients.deleteClients(token, cid)
                logHandler.logging.info("Delete existing client")
            #    clientAdded = 0
            
            
            # Step 1: Send request to update a client
            actualResponse = clients.postLinuxClient(token, linuxName, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.linux_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted,   
                                                clients_testdata.linux_IsDefault, linuxIp)
            logHandler.logging.info("Add client API - Actual Response: " + str(actualResponse))
            reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                
            # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
            cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            logHandler.logging.info("Client returned from response: " + str(cid))
            
            # Verify the response parameter
            if (cid):
                logHandler.logging.info("Client added successfully..")
                reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
            else:
                logHandler.logging.error("Failed to add client")
                raise Exception ("Failed to add a client")
        
            #Verification 2: Fetch added client from db
            dbResult = vclients.getClientByIdFromDB(cid)
            
            if(dbResult):
                reportController.addTestStepResult("Client Add - Database verification passed", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Client Add - Database verification failed", ExecutionStatus.FAIL)   
            
            result = inventory.waitForInventorySync(token, 120, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e)) 
               
        finally:
            #cleanup
            #if (clientAdded == 1 or result == 'Fail'):
            clients.deleteClients(token, cid)
            logHandler.logging.info("Client that is added in this test has been deleted..")
            reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
               
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
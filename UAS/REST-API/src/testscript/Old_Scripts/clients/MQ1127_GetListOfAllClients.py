'''
Created on Oct 1, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import clients, login, logout
from testdata import clients_testdata
from util import reportController, logHandler
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1127_GetListOfAllClients(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get List Of All Clients")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - List of All Clients")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = clients.getListOfAllClients(token)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = clients.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            if(len(actualResponseParams)>1):
                # sort expected response parameter list
                if(len(actualResponseParams)==23):
                    expectedParams = clients_testdata.responseParameter.sort()
                    logHandler.logging.info("Expected Response Parameters - " + str(expectedParams))
                else:
                    expectedParams = clients_testdata.responseParameter.sort()
                    del expectedParams[2]
                    logHandler.logging.info("Expected Response Parameters - " + str(expectedParams))
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(expectedParams, actualResponseParams, "Failed Response verification: Expected response [" + str(expectedParams) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
#               
                # Verification 2: Verify get all alerts API response
                count = vclients.getClientCountFromDB()
                logHandler.logging.info("Count returned from DB: " + str(count))
                     
                clientListFromDB = vclients.getClientListFromDB(actualResponse)
                logHandler.logging.info("Data returned from DB: " + str(clientListFromDB))
            
                clientListFromResponse = vclients.getClientListFromResponse(actualResponse, count)
                logHandler.logging.info("Data from response: " + str(clientListFromResponse))
                
                self.assertListEqual(clientListFromDB, clientListFromResponse, "Failed Data base verification: Expected Response [" + str(clientListFromDB) + "] and Actual Response [" + str(clientListFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("No Client data found", ExecutionStatus.PASS)  
                  
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
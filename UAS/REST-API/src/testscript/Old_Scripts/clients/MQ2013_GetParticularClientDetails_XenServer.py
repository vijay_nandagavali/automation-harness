'''
Created on Oct 13, 2015

@author: Prateek
'''

import unittest
from testdata import clients_testdata
from resourcelibrary import clients, login, logout, virtual_clients
from util import reportController, logHandler
# from util import deletehost
from verificationsript import vclients, vvirtual_clients
from config.Constants import ExecutionStatus
from os.path import sys
from config import apiconfig



class MQ2013_GetParticularClientDetails_XenServer(unittest.TestCase):    
    def __init__(self, exeID):
           
        reportController.initializeReport(type(self).__name__, "Get Particular XenServer Client Details")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Get Particular XenServer Client Details")
        self.executionId = exeID

        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            cid = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
            print cid
            if(cid!=0):
                #Check if the virtual client is associated with this client
                vid = vvirtual_clients.getVirtualClientIDFromDB(str(cid))
                print vid
                #Delete the virtual client if associated with this client
                if(vid != 0):
                    actualResponse = virtual_clients.deleteVirtualClient(token, vid)
                clients.deleteClients(token, cid)
            #deletehost.deleteHostIfPresent()
            actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver,clients_testdata.system_xenserver,
                                                        clients_testdata.ip_xenserver, clients_testdata.username_xenserver, clients_testdata.password_xenserver, 
                                                        clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, clients_testdata.is_synchable_xenserver,
                                                         clients_testdata.is_auth_enabled_xenserver)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                
            # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
            cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            print cid
            # Step 1: Send request to get all alerts
            actualResponse = clients.getDetailsOfParticularClient(cid, token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)    
            
            # Get actual response parameter from response
            actualResponseParams = clients.getListOfActualResponseParameter(actualResponse)
            print actualResponseParams
            # sort expected response parameter list            
            clients_testdata.xenresponseParameter2.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(clients_testdata.xenresponseParameter2 , actualResponseParams, "Failed Response verification: Expected response [" + str(clients_testdata.xenresponseParameter2) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
    #       
            # Verification 2: Verify the values from DB
            #Remove the version information as Xen client version information is not stored in DB
            clientListFromDB = vclients.getSpecificClientDetailsFromDB(cid)
            del clientListFromDB[4]
            print clientListFromDB
                
            clientListFromResponse = vclients.getSpecificClientDetailsFromResponse(actualResponse)
            del clientListFromResponse[4]
            print clientListFromResponse
            self.assertListEqual(clientListFromDB, clientListFromResponse, "Failed Data base verification: Expected Response [" + str(clientListFromDB) + "] and Actual Response [" + str(clientListFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
             
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)


'''
Created on Oct 12, 2015

@author: indrajit.n
'''
import unittest, time
from resourcelibrary import clients, login, logout, joborders
from testdata import clients_testdata
from util import reportController, logHandler
from verificationsript import vclients, vjoborders
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2007_DeleteClient_XenServer(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "Delete the client with the specified id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Specified Client")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            cid = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
            logHandler.logging.info("Client ID returned from DB: " + str(cid))
            if(cid==0):
                # Step 1: Send request to update a client
#                 deletehost.deleteHostIfPresent()
                actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver, clients_testdata.system_xenserver, clients_testdata.ip_xenserver, clients_testdata.username_xenserver, clients_testdata.password_xenserver, clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, clients_testdata.is_synchable_xenserver, clients_testdata.is_auth_enabled_xenserver)
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                cid = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(cid))
                # Verification 1: verify the response parameter
                if (cid):
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
              
            actualResponse = clients.deleteClients(token, cid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request for Delete client" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            responseResult = clients.getDeleteRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                logHandler.logging.info("Response parameter verification successful")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2: Fetch added client from db
            dbResult = vclients.getClientByIdFromDB(cid)
            logHandler.logging.info("DB Result: " + str(dbResult))
              
            if(dbResult):
                logHandler.logging.error("Database verification failed")
                reportController.addTestStepResult("Delete Client - Database verification failed", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Delete Client - Database verification passed", ExecutionStatus.PASS)   
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on Oct 14, 2015

@author: rahula
'''
import unittest

from resourcelibrary import clients, login, logout
from testdata import clients_testdata
from util import reportController, logHandler
#from util import deletehost
from verificationsript import vclients

from config.Constants import ExecutionStatus
from config import apiconfig


class MQ2024_PutUpdateClient_XenServer(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Update a Xen client on the Xen Server system")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tUpdate a Xen client on the Xen Server system")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clients_testdata.name_xenserver)
            print clientId
            if(clientId==0):
                #deletehost.deleteHostIfPresent()
                #addedClientFlag =1
                actualResponse = clients.postXenClient(token, clients_testdata.name_xenserver, clients_testdata.os_type_xenserver, clients_testdata.system_xenserver, clients_testdata.ip_xenserver, clients_testdata.username_xenserver, clients_testdata.password_xenserver, clients_testdata.is_enabled_xenserver, clients_testdata.is_encrypted_xenserver, clients_testdata.is_synchable_xenserver, clients_testdata.is_auth_enabled_xenserver)
                logHandler.logging.info("Add client API - Actual Response: " + str(actualResponse))
                reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
                print actualResponse
                
                # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                
                # Verification 1: verify the prerequisite
                if (clientId):
                    reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
            # Step 1: Send request to update a client
            actualResponse = clients.putUpdateClientXen(token, clientId, "BackupWin")
            print actualResponse
            logHandler.logging.info("PUT update client API - Actual Response: " + str(actualResponse))
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Verification 1: Send PUT request, update client name and priority by sending JSON parameter
            responseResult = clients.getPutRequestResponse(actualResponse)
            print responseResult
            
            # Verification 1: verify the response parameter
            if (responseResult == "Pass"):
                logHandler.logging.info("Verified the response parameter for PUT update client request")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Response parameter verification failed for PUT update client request")
                raise Exception ("Failed response verification")
            
            dbResult = vclients.getClientNamePriorityFromDB(clientId)
            
            #Verification 2: Fetch client name and priority from DB and compare with test data used
            self.assertListEqual(dbResult, ["BackupWin", 500], "Failed Data base verification: Expected Response [" + clients_testdata.updateClientName +","+ str(clients_testdata.updateClientPriority) + "] and Actual Response [" + str(dbResult) + "]")             
            
            #cleanup
            clients.deleteClients(token, clientId) 
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)

            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            


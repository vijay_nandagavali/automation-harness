'''
Created on Oct 30, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import traps, login, logout
from testdata import traps_testdata
from util import reportController
from verificationsript import vtraps
from config.Constants import ExecutionStatus
from config import apiconfig



class MQ1537_GetTrapHistory(unittest.TestCase):    
    def __init__(self, exeID):
          
        reportController.initializeReport(type(self).__name__, "Get Trap History")
        self.executionId = exeID
        
    
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = traps.getTrapsHistory(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = traps.getListOfActualResponseParameter(actualResponse)
            print actualResponseParams
            # sort expected response parameter list            
            traps_testdata.responseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(traps_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(traps_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            trapsFromDB = vtraps.getAllHistoryFromDB()
            print trapsFromDB
            count = vtraps.getHistoryCountFromDB()
            trapsFromResponse = vtraps.getAllHistoryFromResponse(actualResponse, count)
            print trapsFromResponse
            self.assertListEqual(trapsFromDB, trapsFromResponse, "Failed Data base verification: Expected Response [" + str(trapsFromDB) + "] and Actual Response [" + str(trapsFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            


'''
Created on Jul 1, 2016

@author: rahula
'''

import unittest
from resourcelibrary import login, logout, traps
from testdata import traps_testdata
from util import reportController, logHandler
from verificationsript import vtraps
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2586_PutTestTrap(unittest.TestCase):    
    def __init__(self, exeID):
        reportController.initializeReport(type(self).__name__, "PUT Sends a test trap to all configured and enabled trap destinations")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Traps")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            #Login
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Send API Request
            actualResponse = traps.putTestTrap(token)      
            reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
            
            #Verification 1: Verify the request response
            responseResult = traps.getPutRequestResponse(actualResponse)
            print responseResult
            if (responseResult == "Pass"):
                logHandler.logging.info("Verified the response parameter")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Failed in response parameter verification")
                raise Exception ("Failed response verification")
            
            #Database verification
            dataFromDB = vtraps.getTestTrapFromDB()
            logHandler.logging.info("Data from DB: " + str(dataFromDB))
            
            if (dataFromDB == "Test Trap"):
                logHandler.logging.info("Database verification successful")
                reportController.addTestStepResult("Database verification successful", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Failed in database verification")
                raise Exception ("Failed database verification")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
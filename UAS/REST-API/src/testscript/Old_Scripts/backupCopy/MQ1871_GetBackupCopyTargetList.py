'''
Created on Aug 10, 2015

@author: Garima.g
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, systems, logout, backupCopy, replication 
from testdata import systems_testdata, replication_testdata, backupCopy_testdata
from verificationsript import vreplication, vbackupCopy
import time
from time import sleep

class MQ1871_GetBackupCopyTargetList(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Returns a list of backup-copy targets")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tList of backup-copy targets.")
        self.executionId = exeID
    
    def runTest(self):
        try:
            token = login.getAuthenticationToken_Source(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            token_Tar = login.getAuthenticationToken_Target(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token for Target: " + str(token_Tar))
            self.assertNotEqual(token, "", "Empty token received")
                         
#             #prerequisite
            replication.deletePendingTargetSource()
            PreReq_Target = systems.postMakeTarget(token_Tar, systems_testdata.network, systems_testdata.mask, systems_testdata.port)
            logHandler.logging.info("Prerequisite to Enable this appliance as a Backup-Copy Target : " + str(PreReq_Target))
            addTarget = replication.postReplicationTarget(token, replication_testdata.tar_Type_Appliance, replication_testdata.tar_Name, replication_testdata.tar_IP, replication_testdata.tar_insecure)
            logHandler.logging.info("addTarget : " + str(addTarget))
            logHandler.logging.info("Prerequisite to get Backup-Copy Target : " + str(PreReq_Target))
             
            #Send API Request
            actualResponse = backupCopy.getBackupCopyTarget(token)
            logHandler.logging.info("Actual Response: " + str(actualResponse))
            reportController.addTestStepResult("Send API request to get Backup-Copy target list", ExecutionStatus.PASS)
            # Get actual response parameter from response - verification1
            actualResponseParams = backupCopy.getBackupCopyTargetListParam(actualResponse)
            print "actualResponseParams    " + str(actualResponseParams)
            logHandler.logging.info("actual Response Params: " + str(actualResponseParams))
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(backupCopy_testdata.responseParameterBackupCopyTargetList, actualResponseParams, "Failed Response verification: Expected response [" + str(backupCopy_testdata.responseParameterBackupCopyTargetList) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            actualParams2 =vbackupCopy.getTargetListParametersFromResponse(actualResponse)
            if(len(actualParams2) != 0):
                backupCopy_testdata.responseParameterBackupCopyTargetList2.sort()
                self.assertListEqual(actualParams2, backupCopy_testdata.responseParameterBackupCopyTargetList2,"Failed Response verification: Expected response [" + str(backupCopy_testdata.responseParameterBackupCopyTargetList2) + "] and Actual parameter [" + str(actualParams2) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)    
            else:
                reportController.addTestStepResult("No Data Found", ExecutionStatus.PASS)  
                   
# 
            #Verification 2: DB verification   
            targetListFromResponse = vbackupCopy.getTargetFromResponse(actualResponse)
            targetListFromResponse.sort()
            logHandler.logging.info("target list from response: "+ str(targetListFromResponse))
            targetListFromDatabase = vbackupCopy.getTargetListFromDB()
            targetListFromDatabase.sort()
            logHandler.logging.info("target list from database: "+ str(targetListFromDatabase))    
            self.assertListEqual(targetListFromResponse, targetListFromDatabase,  "Failed Data base verification: Expected Response [" + str(targetListFromDatabase) + "] and Actual Response [" + str(targetListFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
#             
                                   
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup - removing of target from API to be added later, this is temp cleanup 
            replication.deletePendingTargetSource()
            logHandler.logging.info("Cleanup done ! ")
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)

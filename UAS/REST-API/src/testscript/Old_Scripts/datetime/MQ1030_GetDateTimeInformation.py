'''
Created on Nov 13, 2014

@author: Amey.k
'''
import unittest

from resourcelibrary import datetime, login, logout
from testdata import datetime_testdata
from util import reportController
from verificationsript import vdatetime
from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1030_GetDateTimeInformation(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Date Time Information")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = datetime.getAllDateInformation(token)
            print actualResponse
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = datetime.getAllDateResponseParameters(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            datetime_testdata.responseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(datetime_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(datetime_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            datetimeFromMachine = vdatetime.getDateTimeInformationFromMachine()
            print datetimeFromMachine
            datetimeFromResponse = vdatetime.getDateTimeInformationFromResponse(actualResponse)
            print datetimeFromResponse
            self.assertListEqual(datetimeFromMachine, datetimeFromResponse, "Failed Data base verification: Expected Response [" + str(datetimeFromMachine) + "] and Actual Response [" + str(datetimeFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            

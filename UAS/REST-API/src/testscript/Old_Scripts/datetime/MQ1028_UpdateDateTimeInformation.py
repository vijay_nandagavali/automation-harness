'''
Created on Feb 19, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import datetime, login, logout
from testdata import datetime_testdata
from util import reportController
from verificationsript import vdatetime
from config.Constants import ExecutionStatus
from config import apiconfig



class MQ1028_UpdateDateTimeInformation(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Update Date Time Information")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = datetime.updateDateTime(token, datetime_testdata.year, datetime_testdata.month, datetime_testdata.day, datetime_testdata.hour, datetime_testdata.minute, datetime_testdata.second, datetime_testdata.timezone, datetime_testdata.ntpEnabled, datetime_testdata.ntpLocal, datetime_testdata.ntpSync)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            responseResult = datetime.getPostRequestResponse(actualResponse)
            print responseResult
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            # Verification 2: Verify get all alerts API response        
            datetimeFromMachine = vdatetime.getDateTimeInformationFromMachine()
            print datetimeFromMachine
            actualResponse = datetime.getAllDateInformation(token)
            datetimeFromResponse = vdatetime.getDateTimeInformationFromResponse(actualResponse)
            print datetimeFromResponse
            self.assertListEqual(datetimeFromMachine, datetimeFromResponse, "Failed Data base verification: Expected Response [" + str(datetimeFromMachine) + "] and Actual Response [" + str(datetimeFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            #cleanup
            datetime.updateDateTimeToNTP(token,datetime_testdata.timezone,datetime_testdata.ntpEnabled_true)
            #logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
'''
Created on Feb 19, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import datetime, login, logout
from testdata import datetime_testdata
from util import reportController
from verificationsript import vdatetime
from config.Constants import ExecutionStatus
from config import apiconfig
from datetime import date



class MQ1027_DeleteNtpServers(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Delete Ntp servers")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = datetime.deleteNtpservers(token, datetime_testdata.sid)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            responseResult = datetime.getDeleteRequestResponse(actualResponse)
            print responseResult
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            # Verification 2: Verify get all alerts API response        
            ntpserverList = vdatetime.getNtpserverlistFromMachine()
            print ntpserverList
                        
            if (len(ntpserverList) != 0):
                reportController.addTestStepResult("Ntp servers successfully deleted", ExecutionStatus.PASS)
            else:
                raise Exception ("Ntp servers not deleted")
                       
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
             #cleanup
            datetime.updateDateTimeToNTP(token,datetime_testdata.timezone,datetime_testdata.ntpEnabled_true)
            #logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
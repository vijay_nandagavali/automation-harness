'''
Created on July 21, 2015

@author: Nikhil S
'''

import unittest
from resourcelibrary import clients, login, logout, assets  
from testdata import clients_testdata, assets_testdata
from util import reportController, logHandler
from verificationsript import vclients, vassets
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1732_GetProtectedAsset(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get Protected Assets")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET Protected Assets")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Check if the clients are present. delete all the existing clients except the one with id=1 
            clientId = vclients.getAllClientsFromDB()
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(len(clientId)>=1):
                for x in range(0, len(clientId)):
                    clients.deleteClients(token, clientId[x]) 
                    logHandler.logging.info("ClientID deleted is:  "+str(clientId[x]))
                               
            addedClientResponse = clients.postClient(token,clients_testdata.sid, clients_testdata.name, clients_testdata.os_type, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clients_testdata.ip, 
                                                     clients_testdata.existing_credential)
                   
            logHandler.logging.info("Actual Response - " + str(addedClientResponse))
            reportController.addTestStepResult("Post client- adding a new client" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
            clientId = clients.getPostClientsRequestResponse(addedClientResponse, clients_testdata.keyForId)
            logHandler.logging.info("Client ID returned from response: " + str(clientId))
                    
            
            actualResponse = assets.getProtectedAssets(token)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            reportController.addTestStepResult("Send API request for GET protected assets" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = clients.getListOfActualResponseParameter(actualResponse)
            actualResponseParams.sort
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
                        
            assets_testdata.protectedAssetsResponseParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(assets_testdata.protectedAssetsResponseParameter))         
            
            # Verification 1: Verify get all response parameter        
            self.assertListEqual(assets_testdata.protectedAssetsResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(assets_testdata.protectedAssetsResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameters", ExecutionStatus.PASS)      
            
            # Verification 2: Verify the database
            protectedAssetsDetailsFromDB = vassets.getProtectedAssetsDetailsFromDB(actualResponse)
            protectedAssetsDetailsFromResponse = vassets.getProtectedAssetsDetailsFromResponse(actualResponse)
            self.assertListEqual(protectedAssetsDetailsFromDB, protectedAssetsDetailsFromResponse, "Failed Data base verification: Expected Response [" + str(protectedAssetsDetailsFromDB) + "] and Actual Response [" + str(protectedAssetsDetailsFromResponse) + "]")
            reportController.addTestStepResult("Verify API response values with data base values", ExecutionStatus.PASS)
                                   
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #delete added client
            clients.deleteClients(token, clientId) 
            logHandler.logging.info("Cleanup ClientID deleted is:  "+str(clientId))
            
            reportController.addTestStepResult("Cleanup - Delete added client.", ExecutionStatus.PASS)
                        
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
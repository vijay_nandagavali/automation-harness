'''
Created on Jul 16, 2015

@author: Nikhil S
'''

import unittest, time
from resourcelibrary import clients, backups, login, logout, jobs, inventory, commands_Exe

from testdata import commands_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler, linuxMachineConnect
from verificationsript import vbackups, vclients
from config.Constants import ExecutionStatus
from config import apiconfig
import commands

class MQ1720_PostRunSupportTunnelONCmd(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "POST method, Run Support Tunnel Command to turn on the tunnel")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Run Support Tunnel Command to turn on the tunnel")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #prerequsite - Check tunnel status, shut it off if it is on
            commandResponse = commands_Exe.executecommand()
            logHandler.logging.info("Prerequsite command Response - " + str(commandResponse))  
            checkSubString = commandResponse.find("/var/log/dpu/support.rsa")
            logHandler.logging.info("Prerequsite checkSubString - " + str(checkSubString))

            if(int(checkSubString) != -1):
                actualResponse = commands_Exe.postCommand(token, commands_testdata.command_Support_Tunnel)
                logHandler.logging.info("TUnnel is on, Turning off the tunnel ")
                reportController.addTestStepResult("Prerequsite Turning off the tunnel", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Tunnel is already off !")
            
            #Request
            actualResponse = commands_Exe.postCommand(token, commands_testdata.command_Support_Tunnel)
            logHandler.logging.info("Actual Response - " + str(actualResponse))  
            reportController.addTestStepResult("Send API request for Post client" , ExecutionStatus.PASS)
            
            actualResponseParams = commands_Exe.getPostRequestResponse(actualResponse)
            logHandler.logging.info("actualResponseParams - " + str(actualResponseParams))  
            
            # Verification# 1
            self.assertListEqual(commands_testdata.responseparameter, actualResponseParams, "Failed Response verification: Expected response [" + str(commands_testdata.responseparameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verification 1 passed", ExecutionStatus.PASS)
            logHandler.logging.info("Verification 1 passed as expected")
            
            # Verification# 2
            commandResponse = commands_Exe.executecommand()
            logHandler.logging.info("command Response - " + str(commandResponse))  
            
            checkSubString = commandResponse.find("/var/log/dpu/support.rsa")
            logHandler.logging.info("checkSubString - " + str(checkSubString))
                      
            if(checkSubString <> -1):
                logHandler.logging.info("Verification 2 passed as expected")
                reportController.addTestStepResult("Verification 2 passed, tunnel turned on", ExecutionStatus.PASS)
                reportController.tcExecutionStatus(ExecutionStatus.PASS)    
            else:           
                logHandler.logging.info("Verification 2 failed")
                reportController.addTestStepResult("Verification 2 failed, tunnel did not turn on", ExecutionStatus.FAIL)
                reportController.tcExecutionStatus(ExecutionStatus.FAIL)  
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
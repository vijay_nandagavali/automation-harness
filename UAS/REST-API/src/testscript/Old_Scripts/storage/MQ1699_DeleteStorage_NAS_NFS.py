'''
Created on Jul 6, 2015

@author: Divya.s
'''

import unittest

from resourcelibrary import storage, login, logout
from testdata import storage_testdata, clients_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1699_DeleteStorage_NAS_NFS(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete a Specific Cloud Storage NAS-NFS")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Cloud Storage NAS-NFS")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #testdata 
            nfsShareName = clients_testdata.getShareName("NFS")
            nfsName = clients_testdata.getmachineName("NFS")
            nfsIP = clients_testdata.getmachineIP("NFS")
            nfsUserName = clients_testdata.getUserName("NFS")
            nfsPassword = clients_testdata.getPassword("NFS")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)   
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(nfsName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            
            if(storageId == 0):
                logHandler.logging.info("No storage found with name: " + str(nfsName) + " Adding the same...")
                actualResponse = storage.postNasStorage(token, storage_testdata.nasStorageUsage, nfsName, storage_testdata.nasNfsProtocol, storage_testdata.nasNfsPort, nfsIP, nfsShareName, nfsUserName, nfsPassword)
                reportController.addTestStepResult("Send API request for Post storage" , ExecutionStatus.PASS)
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                
                dbStorageId = 0
                dbStorageId = vstorage.getStorageIdByName(nfsName)
                logHandler.logging.info("Storage ID returned from DB: " + str(dbStorageId))
                
                if(dbStorageId):
                    logHandler.logging.info("Storage added successfully..")
                    reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                    storageId = dbStorageId
                else:
                    raise Exception("Storage addition failed..")
                                
            #Send API request
            actualResponse = storage.deleteStorage(storageId, token)
            reportController.addTestStepResult("Send API request for Delete cloud storage" , ExecutionStatus.PASS)
            
            responseResult = storage.getDeleteRequestResponse(actualResponse)
            
            # Verification 1: verify the response parameter
            if (responseResult == "Pass"):
                logHandler.logging.info("Storage deleted successfully..")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2: Verify in the Database
            dbResult = vstorage.getUpdatedStorageListFromDB(storageId)
            if (dbResult == 0):
                logHandler.logging.info("Database verification - Passed test")
                reportController.addTestStepResult("No records found against the deleted storage. Database verification done.", ExecutionStatus.PASS)
            else:
                logHandler.logging.error("Database verification - Failed test")
                reportController.addTestStepResult("Records found in the Database against the deleted Storage. Database verification failed", ExecutionStatus.FAIL)    
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)

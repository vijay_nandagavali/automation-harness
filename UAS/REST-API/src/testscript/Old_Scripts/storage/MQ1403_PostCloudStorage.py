'''
Created on May 7, 2015

@author: Savitha Peri
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, storage, logout, archive 
from testdata import storage_testdata, archive_testdata, clients_testdata
from verificationsript import vstorage, varchive

class MQ1403_PostCloudStorage(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Post Storage - Google cloud storage")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Storage - Google cloud storage")
        self.executionId = exeID
    
    def runTest(self):
        try:
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            cloudBucket = clients_testdata.getType("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            #print token
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            dbResult = ''
            if(storageId <> 0):
                logHandler.logging.info("Storage already available with same name. Deleting the same..")
                
                #Check if the storage is online
                onlineStatus = ''
                onlineStatus = vstorage.getOnlineStatusFromDB(storageId)
                logHandler.logging.info("Online status returned from DB: " + str(onlineStatus))
                            
                if(onlineStatus == 'True'):
                    logHandler.logging.info(str(cloudStorageName) + " Cloud Storage is already online..")
                    reportController.addTestStepResult("Cloud storage already online", ExecutionStatus.PASS)                
                else:
                    #Send API request
                    actualResponse = storage.putOnlineStorage(storage_testdata.sid, token, storageId)
                    reportController.addTestStepResult("Send API request for Put cloud storage online" , ExecutionStatus.PASS)
                
                    dbResult = vstorage.getOnlineStatusFromDB(storageId)
                    logHandler.logging.info("Online status returned from DB: " + str(dbResult))
                    if (dbResult == 'True'):
                        logHandler.logging.info("Database verification successful")
                        reportController.addTestStepResult("Cloud Storage Online", ExecutionStatus.PASS)
                    else:
                        raise Exception("Cloud Storage Offline")
                
                #Erase Cloud Storage
                mediaLabel = archive_testdata.cloud_archive_label
                actualResponse = archive.putPrepareArchiveMedia(token, cloudStorageName, mediaLabel)
                reportController.addTestStepResult("Send API request for Archive Media Prepare" , ExecutionStatus.PASS)
            
                #Verification 1: Verify the response parameter   
                responseResult = archive.getPutRequestResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))
            
                dbResult = varchive.getArchivePrepareStatusFromDB(cloudStorageName)
                logHandler.logging.info("DB Result: " + str(dbResult))
            
                if(dbResult == 0):
                    reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
                else:
                    raise Exception("Failed database verification - some records found in archive sets table")                
                
                #Delete the existing storage
                actualResponse = storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Send API request for Delete Storage", ExecutionStatus.PASS)
                commandResponse= storage.deleteStorageByCommandline(cloudBucket)
               
                #Parse the response
                result = storage.getDeleteRequestResponse(actualResponse)
                if(result == "Pass"):
                    logHandler.logging.info("Storage deleted successfully..")
                    reportController.addTestStepResult("Delete Storage", ExecutionStatus.PASS)
                else:
                    raise Exception("Delete storage failed")
                      
            #Send API Request
            commandResponse = storage.deleteStorageByCommandline(cloudBucket)
            actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
            reportController.addTestStepResult("Send API request for Post storage" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            actualResponseParams = vstorage.postStorageResponseParameters(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            paramLength = len(actualResponseParams)
            logHandler.logging.info("Actual Response Parameter Length - " + str(paramLength))
            
            expectedParams = []
            if(paramLength == 4):
                expectedParams = storage_testdata.postStorageResponseParameter
            elif(paramLength == 1):
                expectedParams = ['result']
            else:
                expectedParams = storage_testdata.postStorageResponseParameter1
            
            expectedParams.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(expectedParams))
            
            self.assertListEqual(expectedParams, actualResponseParams, "Failed Response verification: Expected response [" + str(expectedParams) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
            #DB verification
            dbStorageId = 0
            dbStorageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(dbStorageId))
            
            if(dbStorageId):
                logHandler.logging.info("Database verification successful..")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Database verification failed")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on Sep 9, 2015

@author: Prateek
'''

import unittest
from resourcelibrary import storage, login, logout
from testdata import clients_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1937_PostStorageISCSI(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Post ISCSI storage")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPost - ISCSI Storage")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            iscsiStorageName = clients_testdata.getmachineName("ISCSI")
            iscsiTarget = clients_testdata.getShareName("ISCSI")
            iscsiHost = clients_testdata.getmachineIP("ISCSI")
            iscsiLun = clients_testdata.getType("ISCSI")
            iscsiUserName = clients_testdata.getUserName("ISCSI")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId=vstorage.getStorageIdByName(iscsiStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            if(storageId == 0):
                reportController.addTestStepResult("The specified storage not found", ExecutionStatus.PASS)
            else:                    
                # Step 1: Send request to get all alerts
                actualResponsedeletion = storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                logHandler.logging.info("Actual Response - " + str(actualResponsedeletion))
                
                responseResultdeletion = storage.getDeleteRequestResponse(actualResponsedeletion)
            
            # Verification 1: verify the response parameter
                if (responseResultdeletion == "Pass"):
                    logHandler.logging.info("Storage deleted successfully..")
                    reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed response verification")
                
            actualResponse = storage.postISCSIStorage(token, iscsiStorageName, iscsiHost, iscsiTarget, iscsiLun, iscsiUserName)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            responseResult = vstorage.parseCifsNasStorageResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            
            if(responseResult == "Pass"):
                reportController.addTestStepResult("Response verification" , ExecutionStatus.PASS)
            else:
                raise Exception("Response verification failed")
            
            storageId=vstorage.getStorageIdByName(iscsiStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            if(storageId == 0):
                logHandler.logging.info("Storage not found in DB ")
                reportController.addTestStepResult("iscsi Storage not found in DB", ExecutionStatus.FAIL)
            else:
                logHandler.logging.info("Storage found in DB ")
                reportController.addTestStepResult("iscsi Storage found in DB", ExecutionStatus.PASS) 
                
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)                   
                

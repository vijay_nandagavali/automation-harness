'''
Created on May 11, 2015

@author: Savitha Peri
'''

import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, storage, logout 
from testdata import storage_testdata, clients_testdata
from verificationsript import vstorage


class MQ1405_PutCloudStorage(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Put Storage - Google cloud storage")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Storage - Google cloud storage")
        self.executionId = exeID
    
    def runTest(self):
        try:
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            cloudBucket = clients_testdata.getType("GOOGLECLOUD")
            cloudSize = clients_testdata.getmachineIP("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            #print token
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(cloudBucket)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            
            if(storageId == 0):
                logHandler.logging.info("Cloud Storage not available. Adding the same..")
                
                commandResponse = storage.deleteStorageByCommandline(cloudBucket)
                actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
                reportController.addTestStepResult("Send API request for Post storage" , ExecutionStatus.PASS)
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                dbStorageId = 0
                dbStorageId = vstorage.getStorageIdByName(cloudStorageName)
                logHandler.logging.info("Storage ID returned from DB: " + str(dbStorageId))
                
                if(dbStorageId):
                    logHandler.logging.info("Storage added successfully..")
                    reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                    storageId = dbStorageId
                else:
                    raise Exception("Storage addition failed..")
                
                logHandler.logging.info("Thread sleep for 30 sec")
                time.sleep(30)
            
            #Check if the storage is online
            onlineStatus = ''
            onlineStatus = vstorage.getOnlineStatusFromDB(storageId)
            logHandler.logging.info("Online status returned from DB: " + str(onlineStatus))
                        
            if(onlineStatus == 'True'):
                logHandler.logging.info(str(cloudStorageName) + " Cloud Storage is already online..")
                reportController.addTestStepResult("Cloud storage already online", ExecutionStatus.PASS)                
            else:
                #Send API request
                actualResponse = storage.putOnlineStorage(storage_testdata.sid, token, storageId)
                reportController.addTestStepResult("Send API request for Put cloud storage online" , ExecutionStatus.PASS)
            
                dbResult = vstorage.getOnlineStatusFromDB(storageId)
                logHandler.logging.info("Online status returned from DB: " + str(dbResult))
                if (dbResult == 'True'):
                    logHandler.logging.info("Database verification successful")
                    reportController.addTestStepResult("Cloud Storage Online", ExecutionStatus.PASS)
                else:
                    raise Exception("Cloud Storage Offline")
                       
            #Send API Request
            actualResponse = storage.putCloudStorage(token, storageId, cloudUsename, cloudSize)
            reportController.addTestStepResult("Send API request for Put storage" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            response = storage.getPutRequestResponse(actualResponse)
            if(response == "Pass"):
                logHandler.logging.info("Response verification successful..")
                reportController.addTestStepResult("Verify response", ExecutionStatus.PASS)
            else:
                logHandler.logging.error("Response verification failed..")
                reportController.addTestStepResult("Verify response", ExecutionStatus.FAIL)
                        
            #DB verification
            storageSize = vstorage.getStorageSizeFromDB(storageId)
            logHandler.logging.info("Storage size returned from DB: " + str(storageSize))
            if (storageSize == 'fail'):
                raise Exception("Database verification failed")
            else:
                logHandler.logging.info("Storage Size (in GB) returned from DB: " + str(int(storageSize)/1024))
                if(int(storageSize)/1024 == cloudSize):
                    logHandler.logging.info("Database verification successful..")
                    reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
                else:
                    logHandler.logging.error("Database verification failed..")
                    reportController.addTestStepResult("Database verification", ExecutionStatus.FAIL)
            
            #Check if the storage is online
            onlineStatus = ''
            onlineStatus = vstorage.getOnlineStatusFromDB(storageId)
            logHandler.logging.info("Online status returned from DB: " + str(onlineStatus))
                        
            if(onlineStatus == 'True'):
                logHandler.logging.info(str(cloudStorageName) + " Cloud Storage is already online..")
                reportController.addTestStepResult("Cloud storage already online", ExecutionStatus.PASS)                
            else:
                #Send API request
                actualResponse = storage.putOnlineStorage(storage_testdata.sid, token, storageId)
                reportController.addTestStepResult("Send API request for Put cloud storage online" , ExecutionStatus.PASS)
            
                dbResult = vstorage.getOnlineStatusFromDB(storageId)
                logHandler.logging.info("Online status returned from DB: " + str(dbResult))
                if (dbResult == 'True'):
                    logHandler.logging.info("Database verification successful")
                    reportController.addTestStepResult("Cloud Storage Online", ExecutionStatus.PASS)
                else:
                    raise Exception("Cloud Storage Offline")
                
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
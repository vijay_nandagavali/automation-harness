'''
Created on Sep 3, 2015

@author: Prateek.P
'''

import unittest

from resourcelibrary import storage, login, logout
from testdata import storage_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1926_GetStorageInformationForSpecificDevice(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Storage Information For Specific Device")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGet Storage Information For Specific Device")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send API request
            actualResponse = storage.getAllStorageInformationOnId(storage_testdata.deviceid, token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            # Get actual response parameter from response
            actualResponseParams = storage.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            expectedResponseParams =[]
            storageTypes = vstorage.getStorageTypeFromJson(actualResponse)
            if(len(storageTypes)):
                expectedResponseParams = vstorage.getStorageListParameters(storageTypes)
                
            expectedResponseParams = expectedResponseParams + storage_testdata.responseParameter
            expectedResponseParams.sort()
                
            logHandler.logging.info("Expected Response Parameters - " + str(expectedResponseParams))
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(expectedResponseParams, actualResponseParams, "Failed Response verification: Expected response [" + str(expectedResponseParams) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            
            #Verification 2: Verify get all alerts API response      
            storageFromDB = vstorage.getStorageListFromDB()
            logHandler.logging.info("Storage data returned from DB: " + str(storageFromDB))
             
            storageCount = vstorage.getStorageCountFromDB()
            storageFromResponse = vstorage.getStorageListFromResponse(actualResponse, storageCount)
            logHandler.logging.info("Storage data from response: " + str(storageFromResponse))
             
            self.assertListEqual(storageFromDB, storageFromResponse, "Failed Data base verification: Expected Response [" + str(storageFromDB) + "] and Actual Response [" + str(storageFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with database values", ExecutionStatus.PASS)
             
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
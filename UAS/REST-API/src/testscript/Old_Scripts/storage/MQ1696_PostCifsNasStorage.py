'''
Created on Jul 6, 2015

@author: savitha.p
'''

import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, storage, logout 
from testdata import storage_testdata, clients_testdata
from verificationsript import vstorage

class MQ1696_PostCifsNasStorage(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Post Storage - CIFS NAS storage")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Storage - CIFS NAS storage")
        self.executionId = exeID
    
    def runTest(self):
        try:
            cifsName = clients_testdata.getmachineName("CIFS")
            cifsShareName = clients_testdata.getShareName("CIFS")
            cifsIP = clients_testdata.getmachineIP("CIFS")
            cifsUserName = clients_testdata.getUserName("CIFS")
            cifsPassword = clients_testdata.getPassword("CIFS")
                        
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(cifsName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            
            if(storageId == 0):
                shareNameStorageId = vstorage.getStorageIdBasedOnShareNameFromDB(cifsShareName)
                logHandler.logging.info("Storage ID based on sharename returned from DB: " + str(shareNameStorageId))
                
                if(shareNameStorageId <> 0):
                    storageId = shareNameStorageId
            
            if(storageId <> 0):
                logHandler.logging.info("Storage already available with same name. Deleting the same..")
                
                actualResponse = storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Send API request for Delete existing storage" , ExecutionStatus.PASS)
            
                result = storage.getDeleteRequestResponse(actualResponse)
            
                if (result == "Pass"):
                    logHandler.logging.info("Storage deleted successfully..")
                    reportController.addTestStepResult("Delete existing storage", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to delete existing storage")
                
                logHandler.logging.info("Thread sleep for 10 sec")
                time.sleep(10)
    
            #Send API Request
            actualResponse = storage.postNasStorage(token, storage_testdata.nasStorageUsage, cifsName, 
                                                    storage_testdata.cifsNasStorageProtocol, storage_testdata.cifsNasStoragePort, 
                                                    cifsIP, cifsShareName, 
                                                    cifsUserName, cifsPassword)
            reportController.addTestStepResult("Send API request for Post NAS storage" , ExecutionStatus.PASS)
            print "actualresponse: "+str(actualResponse)
            #Parse the actual response
            responseResult = vstorage.parseCifsNasStorageResponse(actualResponse)
            responseStatus="fail"
            if(responseResult!="false"):
                responseStatus="Pass"
            logHandler.logging.info("Response Result: " + str(responseResult))
            
            if(responseStatus == "Pass"):
                reportController.addTestStepResult("Response verification" , ExecutionStatus.PASS)
            else:
                raise Exception("Response verification failed")
                        
            #DB verification
            storageId = 0
            storageId = vstorage.getStorageIdByName(cifsName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            
            if(storageId):
                logHandler.logging.info("Database verification successful..")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Database verification failed")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup
            if(storageId):
                actualResponse = storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Send API request for Delete storage" , ExecutionStatus.PASS)
            
                result = storage.getDeleteRequestResponse(actualResponse)
                if (result == "Pass"):
                    logHandler.logging.info("Storage deleted successfully..")
                    reportController.addTestStepResult("Delete Storage", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to delete storage")
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
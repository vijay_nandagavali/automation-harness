'''
Created on Jul 6, 2015

@author: rahula
'''

import unittest, time

from resourcelibrary import storage, login, logout
from testdata import storage_testdata, clients_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1700_DeleteCifsNasStorageBySidId(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Delete a CIFS NAS Storage on a specified system with specified id")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Delete a CIFS NAS Storage on a specified system with specified id")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            cifsName = clients_testdata.getmachineName("CIFS")
            cifsShareName = clients_testdata.getShareName("CIFS")
            cifsIP = clients_testdata.getmachineIP("CIFS")
            cifsUserName = clients_testdata.getUserName("CIFS")
            cifsPassword = clients_testdata.getPassword("CIFS")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Prerequisite setup: Post a CIFS NAS Storage
            #Send API Request
            storageId = vstorage.getStorageIdByName(cifsName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            
            if(storageId == 0):
                shareNameStorageId = vstorage.getStorageIdBasedOnShareNameFromDB(cifsShareName)
                logHandler.logging.info("Storage ID based on sharename returned from DB: " + str(shareNameStorageId))
                
                if(shareNameStorageId <> 0):
                    storageId = shareNameStorageId
            
            if(storageId <> 0):
                logHandler.logging.info("Storage is available as expected. Using the same.")
            else:
                actualResponse = storage.postNasStorage(token, storage_testdata.nasStorageUsage, cifsName, 
                                                    storage_testdata.cifsNasStorageProtocol, storage_testdata.cifsNasStoragePort, 
                                                    cifsIP, cifsShareName, 
                                                    cifsUserName, cifsPassword)
                reportController.addTestStepResult("Send API request for Post NAS storage" , ExecutionStatus.PASS)
                logHandler.logging.info("Sent a POST request for creating a storage")
                #Parse the actual response
                responseResult = vstorage.parseCifsNasStorageResponse(actualResponse)
                responseStatus="fail"
                logHandler.logging.info("Response Result: " + str(responseResult))
                if(responseResult!="false"):
                    responseStatus="Pass"
            
                if(responseStatus == "Pass"):
                    reportController.addTestStepResult("Response verification" , ExecutionStatus.PASS)
                else:
                    raise Exception("Response verification failed")
            
            storageId = vstorage.getStorageIdByName(cifsName)
            if(storageId == 0):
                raise Exception("No storage found with the name: " + str(cifsName))
            
            #Send API request to delete the storage. 
            actualResponse = storage.deleteStorageWithSidId(token, storage_testdata.sid, storageId)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            responseResult = storage.getDeleteRequestResponse(actualResponse)
            
            # Verification 1: verify the response parameter
            if (responseResult == "Pass"):
                logHandler.logging.info("Storage deleted successfully..")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2: Verify in the Database
            dbResult = vstorage.getUpdatedStorageListFromDB(str(storageId))
            if (dbResult == 0):
                logHandler.logging.info("Database verification - Passed test")
                reportController.addTestStepResult("No records found against the deleted storage. Database verification done.", ExecutionStatus.PASS)
            else:
                logHandler.logging.error("Database verification - Failed test")
                reportController.addTestStepResult("Records found in the Database against the deleted Storage. Database verification failed", ExecutionStatus.FAIL)    
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
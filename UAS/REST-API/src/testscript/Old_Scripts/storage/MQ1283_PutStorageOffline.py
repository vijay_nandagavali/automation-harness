'''
Created on Feb 16, 2015

@author: Sayali K.
'''

import unittest

from resourcelibrary import storage, login, logout
from testdata import storage_testdata, clients_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1283_PutStorageOffline(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Make the storage offline so as not available for a particular system")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Storage offline")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            storageName = clients_testdata.getmachineName("AddedStorage")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            storageId=vstorage.getStorageIdByName(storageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            if(storageId == 0):
                reportController.addTestStepResult("The specified storage not found", ExecutionStatus.PASS)
            else:
                actualResponse = storage.putOfflineStorage(storage_testdata.sid, token, storageId)
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                
                responseResult = storage.getPutRequestResponse(actualResponse)
                
                # Verification 1: verify the response parameter
                if (responseResult == "Pass"):
                    logHandler.logging.info("Response verification successful")
                    reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed response verification")
                                
                #Verification 2: Verify in the Database
                dbResult = vstorage.getOnlineStatusFromDB(storageId)
                logHandler.logging.info("DB Result: " + str(dbResult))
                if (dbResult == 'False'):
                    logHandler.logging.info("Database verification - Passed test")
                    reportController.addTestStepResult("The Specified Storage is Offline. Database verification passed", ExecutionStatus.PASS)
                else:
                    logHandler.logging.info("Database verification - Failed test")
                    reportController.addTestStepResult("Database verification failed", ExecutionStatus.FAIL)    
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
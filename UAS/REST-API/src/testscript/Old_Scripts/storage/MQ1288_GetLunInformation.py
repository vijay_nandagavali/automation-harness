'''
Created on Nov 21, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import storage, login, logout
from testdata import storage_testdata, clients_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1288_GetLunInformation(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get Luhn Storage Information")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Luhn Storage information")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            iscsiStorageName = clients_testdata.getmachineName("ISCSI")
            iscsiTarget = clients_testdata.getShareName("ISCSI")
            iscsiHost = clients_testdata.getmachineIP("ISCSI")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId=vstorage.getStorageIdByName(iscsiStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            if(storageId == 0):
                logHandler.logging.info("Specified storage not found")
                reportController.addTestStepResult("The specified storage not found", ExecutionStatus.PASS)
            else:                    
                # Step 1: Send request to get all alerts
                actualResponse = storage.getListOfLunInformation(token, iscsiTarget, iscsiHost)
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                
                # Get actual response parameter from response
                actualResponseParams = storage.getListOfActualResponseParameterISCSI(actualResponse)
                logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))            
                
                storage_testdata.responseParameter_LUN.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(storage_testdata.responseParameter_LUN))
                
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(storage_testdata.responseParameter_LUN, actualResponseParams, "Failed Response verification: Expected response [" + str(storage_testdata.responseParameter_LUN) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                               
                # Verification 2: Verify get all alerts API response    
                storageFromResponse = vstorage.getTargetInformationLUNFromResponse(actualResponse)
                logHandler.logging.info("Data from response: " + str(storageFromResponse))
                  
                storageFromDB = vstorage.getTargetInformationLUNFromDB()
                logHandler.logging.info("Data returned from DB: " + str(storageFromDB))
                            
                self.assertListEqual(storageFromDB, storageFromResponse, "Failed Data base verification: Expected Response [" + str(storageFromDB) + "] and Actual Response [" + str(storageFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
'''
Created on Feb 16, 2015

@author: Rahul A.
'''

import unittest

from resourcelibrary import storage, login, logout
from testdata import storage_testdata, clients_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1039_DeleteStorage(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Delete a Specific Storage")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Storage")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            storageName = clients_testdata.getmachineName("AddedStorage")
             
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(storageName)
            if(storageId == 0):
                raise Exception("No storage found with the name: " + str(storageName))
            
            actualResponse = storage.deleteStorage(storageId, token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            responseResult = storage.getDeleteRequestResponse(actualResponse)
            
            # Verification 1: verify the response parameter
            if (responseResult == "Pass"):
                logHandler.logging.info("Storage deleted successfully..")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2: Verify in the Database
            dbResult = vstorage.getUpdatedStorageListFromDB(storageId)
            if (dbResult == 0):
                logHandler.logging.info("Database verification - Passed test")
                reportController.addTestStepResult("No records found against the deleted storage. Database verification done.", ExecutionStatus.PASS)
            else:
                logHandler.logging.error("Database verification - Failed test")
                reportController.addTestStepResult("Records found in the Database against the deleted Storage. Database verification failed", ExecutionStatus.FAIL)    
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
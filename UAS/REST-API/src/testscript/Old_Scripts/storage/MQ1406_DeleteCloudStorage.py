'''
Created on May 11, 2015

@author: Savitha Peri
'''

import unittest

from resourcelibrary import storage, login, logout
from testdata import storage_testdata, clients_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1406_DeleteCloudStorage(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete a Specific Cloud Storage")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Cloud Storage")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            cloudStorageName = clients_testdata.getmachineName("GOOGLECLOUD")
            cloudShareName = clients_testdata.getShareName("GOOGLECLOUD")
            cloudUsename = clients_testdata.getUserName("GOOGLECLOUD")
            cloudPassword = clients_testdata.getPassword("GOOGLECLOUD")
            cloudBucket = clients_testdata.getType("GOOGLECLOUD")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(cloudStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            
            if(storageId == 0):
                logHandler.logging.info("No storage found with name: " + str(cloudStorageName) + " Adding the same...")
                
                commandResponse = storage.deleteStorageByCommandline(cloudBucket)
                actualResponse = storage.postStorage(token, cloudShareName, cloudUsename, cloudPassword)
                reportController.addTestStepResult("Send API request for Post storage" , ExecutionStatus.PASS)
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                
                dbStorageId = 0
                dbStorageId = vstorage.getStorageIdByName(cloudStorageName)
                logHandler.logging.info("Storage ID returned from DB: " + str(dbStorageId))
                
                if(dbStorageId):
                    logHandler.logging.info("Storage added successfully..")
                    reportController.addTestStepResult("Add cloud storage", ExecutionStatus.PASS)
                    storageId = dbStorageId
                else:
                    raise Exception("Storage addition failed..")
                                
            #Send API request
            actualResponse = storage.deleteStorage(storageId, token)
            reportController.addTestStepResult("Send API request for Delete cloud storage" , ExecutionStatus.PASS)
            commandResponse = storage.deleteStorageByCommandline(cloudBucket)
            
            responseResult = storage.getDeleteRequestResponse(actualResponse)
            
            # Verification 1: verify the response parameter
            if (responseResult == "Pass"):
                logHandler.logging.info("Storage deleted successfully..")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2: Verify in the Database
            dbResult = vstorage.getUpdatedStorageListFromDB(storage_testdata.deletestorageid)
            if (dbResult == 0):
                logHandler.logging.info("Database verification - Passed test")
                reportController.addTestStepResult("No records found against the deleted storage. Database verification done.", ExecutionStatus.PASS)
            else:
                logHandler.logging.error("Database verification - Failed test")
                reportController.addTestStepResult("Records found in the Database against the deleted Storage. Database verification failed", ExecutionStatus.FAIL)    
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
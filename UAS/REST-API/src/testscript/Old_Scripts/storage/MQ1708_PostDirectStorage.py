'''
Created on July 13, 2015

@author: Nikhil S
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, storage, logout, archive 
from testdata import storage_testdata, clients_testdata
from verificationsript import vstorage, varchive
import time
from time import sleep

class MQ1708_PostDirectStorage(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Create Direct Storage.")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tCreate Direct Storage.")
        self.executionId = exeID
    
    def runTest(self):
        try:
            directStorageName = clients_testdata.getmachineName("DirectStorage")
            directDiskPartition = clients_testdata.getShareName("DirectStorage")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(directStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
                      
            if(storageId <> 0):
                logHandler.logging.info("Storage already available with same name. Deleting the same..")
                
                actualResponse = storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Send API request for Delete existing storage" , ExecutionStatus.PASS)
            
                result = storage.getDeleteRequestResponse(actualResponse)
            
                if (result == "Pass"):
                    logHandler.logging.info("Storage deleted successfully..")
                    reportController.addTestStepResult("Delete existing storage", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to delete existing storage")
                
                logHandler.logging.info("Thread sleep for 10 sec")
                time.sleep(10)
    
            #Send API Request
            actualResponse = storage.postDirectStorage(token, directDiskPartition, directStorageName, storage_testdata.directUsage)
            reportController.addTestStepResult("Send API request for Post Direct storage" , ExecutionStatus.PASS)
            
            #Parse the actual response
            responseResult = vstorage.parseCifsNasStorageResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            
            if(responseResult == "Pass"):
                reportController.addTestStepResult("Response verification" , ExecutionStatus.PASS)
            else:
                raise Exception("Response verification failed")
                        
            #DB verification
            storageId = 0
            storageId = vstorage.getStorageIdByName(directStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            
            if(storageId):
                logHandler.logging.info("Database verification successful..")
                reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
            else:
                raise Exception("Database verification failed")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup
            if(storageId):
                actualResponse = storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Send API request for Delete storage" , ExecutionStatus.PASS)
             
                result = storage.getDeleteRequestResponse(actualResponse)
                if (result == "Pass"):
                    logHandler.logging.info("Storage deleted successfully..")
                    reportController.addTestStepResult("Delete Storage", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to delete storage")
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
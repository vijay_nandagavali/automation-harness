'''
Created on Feb 16, 2015

@author: Sayali K.
'''

import unittest

from resourcelibrary import storage, login, logout
from testdata import storage_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1287_GetAvailableDisks(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Gets the list of internal disks that have been attached to the appliance")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Available disks")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            actualResponse = storage.getAvailableDisks(storage_testdata.sid, token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))            
            
            actualResponseParams = storage.getAvailableDisksResponse(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))   
            
            if (len(actualResponseParams)>1):
                storage_testdata.diskResponseParameter.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(storage_testdata.diskResponseParameter))
                
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(storage_testdata.diskResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(storage_testdata.diskResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            
                # Verification 2: Verify get all alerts API response
                listAvailabeDisk = vstorage.getListOfAvailabeDisk(actualResponse)
                listAvailabeDisk.sort()
                logHandler.logging.info("Data returned from response: " + str(listAvailabeDisk))
                
                listAvailabeDiskFromMachine = vstorage.getListOfAvailabeDiskFromMachine()
                listAvailabeDiskFromMachine.sort()
                logHandler.logging.info("Data returned from machine: " + str(listAvailabeDiskFromMachine))
                                
                self.assertListEqual(listAvailabeDisk, listAvailabeDiskFromMachine, "Failed Data base verification: Expected Response [" + str(listAvailabeDiskFromMachine) + "] and Actual Response [" + str(listAvailabeDisk) + "]")
                reportController.addTestStepResult("Verify response and machine data", ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Data not present")
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
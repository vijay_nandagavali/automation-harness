'''
Created on Jul 6, 2015

@author: Nikhil.S
'''

import unittest, time
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, storage, logout 
from testdata import storage_testdata, clients_testdata
from verificationsript import vstorage

class MQ1697_PutNfsNasStorageUpdate(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Put Storage - Update NFS NAS storage")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Storage - Update NFS NAS storage")
        self.executionId = exeID
    
    def runTest(self):
        try:
            #testdata 
            nfsShareName = clients_testdata.getShareName("NFS")
            nfsName = clients_testdata.getmachineName("NFS")
            nfsIP = clients_testdata.getmachineIP("NFS")
            nfsUserName = clients_testdata.getUserName("NFS")
            nfsPassword = clients_testdata.getPassword("NFS")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId = vstorage.getStorageIdByName(nfsName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            
            if(storageId == 0):
                shareNameStorageId = vstorage.getStorageIdBasedOnShareNameFromDB(nfsShareName)
                logHandler.logging.info("Storage ID based on sharename returned from DB: " + str(shareNameStorageId))
                
                if(shareNameStorageId <> 0):
                    storageId = shareNameStorageId
            
            if(storageId <> 0):
                logHandler.logging.info("Storage already available with same name. Deleting the same..")
                
                actualResponse = storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Send API request for Delete existing storage" , ExecutionStatus.PASS)
            
                result = storage.getDeleteRequestResponse(actualResponse)
            
                if (result == "Pass"):
                    logHandler.logging.info("Storage deleted successfully..")
                    reportController.addTestStepResult("Delete existing storage", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to delete existing storage")
                
                logHandler.logging.info("Thread sleep for 10 sec")
                time.sleep(10)
    
            #Send API Request
            actualResponse = storage.postNasStorage(token, storage_testdata.nasStorageUsage, nfsName, storage_testdata.nasNfsProtocol, storage_testdata.nasNfsPort, nfsIP, nfsShareName, nfsUserName, nfsPassword)
            reportController.addTestStepResult("Send API request for Post NAS storage" , ExecutionStatus.PASS)
            
            actualResponse = str(actualResponse).strip("\n")
            
            #Parse the actual response
            responseResult = vstorage.parseNFSNasStorageResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            
            if(responseResult == 0):
                raise Exception("Response verification failed")
            else:
                reportController.addTestStepResult("Response verification" , ExecutionStatus.PASS)
                
            
            storageId = 0
            storageId = vstorage.getStorageIdByName(nfsName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            
            actualResponse = storage.updateStorageNfsNAS(storageId, token, storage_testdata.nasStorageUsage, storage_testdata.nasNfsProtocol, storage_testdata.nasNfsPort_Update, nfsUserName, nfsPassword)
            reportController.addTestStepResult("Send API request for Put storage" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            
            #Parse the actual response
            response = storage.getPutRequestResponse(actualResponse)
            if(response == "Pass"):
                logHandler.logging.info("Response verification successful..")
                reportController.addTestStepResult("Verify response", ExecutionStatus.PASS)
            else:
                logHandler.logging.error("Response verification failed..")
                reportController.addTestStepResult("Verify response", ExecutionStatus.FAIL)
            
            #DB verification
            storagePort = vstorage.getPortNumFromDB(storageId)
            logHandler.logging.info("Storage Port Number returned from DB: " + str(storagePort))
            if (storagePort == 'fail'):
                raise Exception("Database verification failed")
            else:
                logHandler.logging.info("storagePort returned from DB: " + str(storagePort))
                if(str(storagePort) == str(storage_testdata.nasNfsPort_Update)):
                    logHandler.logging.info("Database verification successful..")
                    reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
                    reportController.tcExecutionStatus(ExecutionStatus.PASS)
                else:
                    logHandler.logging.error("Database verification failed..")
                    reportController.addTestStepResult("Database verification", ExecutionStatus.FAIL)
                    reportController.tcExecutionStatus(ExecutionStatus.FAIL)
                     
                        
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Cleanup
            if(storageId):
                actualResponse = storage.deleteStorage(storageId, token)
                reportController.addTestStepResult("Send API request for Delete storage" , ExecutionStatus.PASS)
            
                result = storage.getDeleteRequestResponse(actualResponse)
                if (result == "Pass"):
                    logHandler.logging.info("Storage deleted successfully..")
                    reportController.addTestStepResult("Delete Storage", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to delete storage")
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
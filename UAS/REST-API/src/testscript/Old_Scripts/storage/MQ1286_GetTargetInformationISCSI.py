'''
Created on Nov 21, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import storage, login, logout
from testdata import storage_testdata, clients_testdata
from util import reportController, logHandler
from verificationsript import vstorage
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1286_GetTargetInformationISCSI(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get ISCSI target information")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - ISCSI Target information")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            iscsiStorageName = clients_testdata.getmachineName("ISCSI")
            
            iscsiHost = clients_testdata.getmachineIP("ISCSI")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            storageId=vstorage.getStorageIdByName(iscsiStorageName)
            logHandler.logging.info("Storage ID returned from DB: " + str(storageId))
            if(storageId == 0):
                reportController.addTestStepResult("The specified storage not found", ExecutionStatus.PASS)
            else:                    
                # Step 1: Send request to get all alerts
                actualResponse = storage.getTargetInformationISCSI(token, iscsiHost)
                reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                
                # Get actual response parameter from response
                actualResponseParams = storage.getListOfActualResponseParameterISCSI(actualResponse)
                logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))            
                      
                storage_testdata.responseParameter_ISCSI.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(storage_testdata.responseParameter_ISCSI))
                
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(storage_testdata.responseParameter_ISCSI, actualResponseParams, "Failed Response verification: Expected response [" + str(storage_testdata.responseParameter_ISCSI) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response    
                storageFromResponse = vstorage.getTargetInformationISCSIFromResponse(actualResponse)
                logHandler.logging.info("Data returned from response: " + str(storageFromResponse))
                  
                storageFromDB = vstorage.getTargetInformationISCSIFromDB()
                logHandler.logging.info("Data returned from DB: " + str(storageFromDB))
                            
                self.assertListEqual(storageFromDB, storageFromResponse, "Failed Data base verification: Expected Response [" + str(storageFromDB) + "] and Actual Response [" + str(storageFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
                
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
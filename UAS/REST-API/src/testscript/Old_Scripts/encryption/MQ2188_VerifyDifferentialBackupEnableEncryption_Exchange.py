'''
Created on Dec 3, 2015

@author: Nikhil S
'''

import unittest
from resourcelibrary import encryption, login, logout, clients, inventory, backups, jobs
from testdata import encryption_testdata, clients_testdata, inventory_testdata, backups_testdata
from util import reportController, logHandler
from verificationsript import vencryption, vclients, vbackups
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2188_VerifyDifferentialBackupEnableEncryption_Exchange(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Verify that Exchange Differential backups are encrypted with encryption enabled at Database instance level")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify that Exchange Differential backups are encrypted with encryption enabled at Database instance level")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            
            #Test Data
            clientName = clients_testdata.getmachineName("EXCHANGE")
            clientIp = clients_testdata.getmachineIP("EXCHANGE")
            testDBName = clients_testdata.getShareName("EXCHANGE")
            osType = clients_testdata.getType("EXCHANGE")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            #encryption.enableEncryption(token, encryption_testdata.systemId, encryption_testdata.passphrase)
            actualResponse = encryption.enableEncryption(token, encryption_testdata.systemId, encryption_testdata.passphrase)            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            responseResult = encryption.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Encryption enabled at UEB level successfully.", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed to enable Encryption at UEB level.")
            
            #Step 1: Send request to add a client
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            clientAdded = 0
            if(clientId==0):
                clientAdded = 1
                actualResponse = clients.postWindowClient(token, clientName, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.use_ssl, clients_testdata.win_InstallAgent, clients_testdata.is_auth_enabled, clients_testdata.is_encrypted_True,   
                                                clients_testdata.win_IsDefault, clientIp, clientType = "Exchange")
                  
                logHandler.logging.info("Actual Response - " + str(actualResponse))
                reportController.addTestStepResult("Add exchange client " , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID - " + str(clientId))
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Inventory sync Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
            
            #Enable encryption for the instance 
            instanceId = vbackups.getInstanceIdFromHyperv(testDBName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            instanceEncryptResponse= encryption.enableInstanceEncryption(token, instanceId)
            logHandler.logging.info("Instance encryption Response - " + str(instanceEncryptResponse)) 
            
            # Trigger full backup 
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyNone,backups_testdata.sid)
            logHandler.logging.info("Actual Response of Full backup triggered - " + str(actualResponse))            
            reportController.addTestStepResult("Trigger full backup" , ExecutionStatus.PASS)
            
            #Verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("FUll backup Job ID from Response: " + str(strJobId))
            if(strJobId == '-1'):
                raise Exception("Failed to trigger backup")
            
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus!="Successful"):
                logHandler.logging.error("Full Backup not successful")
                reportController.addTestStepResult("Full Backup unsuccessful", ExecutionStatus.FAIL)
                raise Exception("Full Backup not successful")
            else:
                logHandler.logging.info("Full Backup successful")
                reportController.addTestStepResult("Full Backup Successful" , ExecutionStatus.PASS)
                
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Full Backup ID returned from DB: " + str(bid))
            
            #Trigger Differential Backup
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.differentialBackupType,backups_testdata.storageType,backups_testdata.verifyNone,backups_testdata.sid)
            logHandler.logging.info("Actual Response of Differential backup triggered - " + str(actualResponse))            
            reportController.addTestStepResult("Trigger Differential backup" , ExecutionStatus.PASS)
            
            #Verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Differential backup Job ID from Response: " + str(strJobId))
            if(strJobId == '-1'):
                raise Exception("Failed to trigger Differential backup")
            
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus!="Successful"):
                logHandler.logging.error("Differential Backup not successful")
                reportController.addTestStepResult("Differential Backup unsuccessful", ExecutionStatus.FAIL)
                raise Exception("Differential Backup not successful")
            else:
                logHandler.logging.info("Differential Backup successful")
                reportController.addTestStepResult("Differential Backup Successful" , ExecutionStatus.PASS)
                
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Differential Backup ID returned from DB: " + str(bid))
            
            
            #verify encryption     
            catalogResponse= backups.getBackupCatalogInstanceResponse(token)
            encrypt = vencryption.getEncryptionFromCatalogResponse(catalogResponse, testDBName, bid)
            if(encrypt == False):
                logHandler.logging.error("Backup not encrypted")
                reportController.addTestStepResult("encryption verification", ExecutionStatus.FAIL)
                raise Exception("Backup not encrypted")
            else:
                logHandler.logging.info("Backup encrypted")
                reportController.addTestStepResult("encryption verification" , ExecutionStatus.PASS)
           
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Cleanup
            if(clientAdded == 1 or result == 'Fail'):
                clients.deleteClients(token, clientId)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
            
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
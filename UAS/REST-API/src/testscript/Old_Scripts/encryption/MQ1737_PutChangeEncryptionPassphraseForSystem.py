'''
Created on Jul 22, 2015

@author: rahula
'''
import unittest
from resourcelibrary import encryption, login, logout
from testdata import encryption_testdata
from util import reportController, logHandler
from verificationsript import vencryption
from config.Constants import ExecutionStatus
from config import apiconfig
from time import sleep


class MQ1737_PutChangeEncryptionPassphraseForSystem(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "PUT change Encryption Passphrase for a system")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Change Encryption Passphrase for a system")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Prerequisite 1: Remove encryption key from command line and reboot the system       
            logHandler.logging.info("Prerequisite: Removing the encryption key - passphrase from the appliance")
            encryption.removeEncryptionPassphrase()     
            reportController.addTestStepResult("Removed the encryption key - passphrase from the appliance", ExecutionStatus.PASS)     
            #Wait for system reboot
            sleep(120)
            
            # Prerequisite 2: Send request to set the encryption passphrase
            actualResponse = encryption.createEncryptionPassphrase(token, encryption_testdata.systemId, encryption_testdata.passphrase)            
            reportController.addTestStepResult("Send API request to create a new passphrase" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response and verify it
            responseResult = encryption.getPostRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            reportController.tcExecutionStatus(ExecutionStatus.PASS) 
            
            #Send API request to change the passphrase for a system
            actualResponse = encryption.changeEncryptionPassphrase(token,encryption_testdata.systemId , encryption_testdata.passphrase, encryption_testdata.changepassphrase)
            reportController.addTestStepResult("Send API request to change the passphrase" , ExecutionStatus.PASS)
            
            # Verification 1: Get actual response parameter from response and verify it
            responseResult = encryption.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2: Call enable encryption API with the changed passphrase 
            actualResponse = encryption.enableEncryption(token, encryption_testdata.systemId, encryption_testdata.changepassphrase)            
            reportController.addTestStepResult("Use the changed passphrase to enable encryption" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            responseResult = encryption.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            # Verification 2: Verify get all alerts API response        
            encryptionFromMachine = vencryption.getEncryptionInformationFromMAchine()
            logHandler.logging.info("Data returned from Machine: " + str(encryptionFromMachine[0]))
            if (str(encryptionFromMachine[0]) == "on"):
                reportController.addTestStepResult("Encryption enabled", ExecutionStatus.PASS)
            else:
                raise Exception ("Encryption not enabled")    
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Cleanup - remove encryption
            logHandler.logging.info("Cleanup: Removing the encryption key - passphrase from the appliance")
            encryption.removeEncryptionPassphrase()     
            reportController.addTestStepResult("Cleanup: Removed the encryption key - passphrase from the appliance", ExecutionStatus.PASS)     
            #Wait for system reboot
            sleep(120)
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
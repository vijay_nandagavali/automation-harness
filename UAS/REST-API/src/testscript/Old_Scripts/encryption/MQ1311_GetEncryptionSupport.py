'''
Created on Dec 9, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import encryption, login, logout
from testdata import encryption_testdata
from util import reportController
from verificationsript import vencryption
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1311_GetEncryptionSupport(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Encryption Support")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = encryption.getEncryptionSupportInformation(token, encryption_testdata.systemId)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = encryption.getListOfActualResponseParameter(actualResponse)
            # print actualResponseParams            
            # sort expected response parameter list            
            encryption_testdata.encryptionSupportResponseParameter.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(encryption_testdata.encryptionSupportResponseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(encryption_testdata.encryptionSupportResponseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            encryptionFromMachine = vencryption.getEncryptionSupportInformationFromMachine()
            print encryptionFromMachine
            encryptionFromResponse = vencryption.getEncryptionSupportInformationFromResponse(actualResponse)
            print encryptionFromResponse
            self.assertListEqual(encryptionFromMachine, encryptionFromResponse, "Failed Data base verification: Expected Response [" + str(encryptionFromMachine) + "] and Actual Response [" + str(encryptionFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
'''
Created on Jul 17, 2015

@author: rahula
'''
import unittest
from resourcelibrary import encryption, login, logout
from testdata import encryption_testdata
from util import reportController, logHandler
#from verificationsript import vencryption
from config.Constants import ExecutionStatus
from config import apiconfig
from time import sleep


class MQ1722_PostCreateEncryptionPassphrase(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "POST Create Encryption Passphrase")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Create Encryption Passphrase")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Prerequisite setup - remove encryption key from command line and reboot the system       
            logHandler.logging.info("Removing the encryption key - passphrase from the appliance")
            encryption.removeEncryptionPassphrase()     
            reportController.addTestStepResult("Removed the encryption key - passphrase from the appliance", ExecutionStatus.PASS)     
            #Wait for system reboot
            sleep(180)
            
            # Step 1: Send request to set the encryption passphrase
            #encryption.enableEncryption(token, encryption_testdata.systemId, encryption_testdata.passphrase)
            actualResponse = encryption.createEncryptionPassphrase(token, encryption_testdata.systemId, encryption_testdata.passphrase)            
            reportController.addTestStepResult("Send API request to create a new passphrase" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response and verify it
            responseResult = encryption.getPostRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
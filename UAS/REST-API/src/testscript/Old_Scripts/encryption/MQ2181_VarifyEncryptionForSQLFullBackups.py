'''
Created on Nov 30, 2015

@author: Prateek
'''
import unittest
from resourcelibrary import encryption, login, logout, backups, clients, inventory, jobs
from testdata import encryption_testdata, clients_testdata, backups_testdata, inventory_testdata
from util import reportController, logHandler
from verificationsript import vencryption, vbackups, vclients
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ2181_VarifyEncryptionForSQLFullBackups(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Encryption verification for sql full backups")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tEncryption Verification for Sql Full backups")
        self.executionId = exeID  
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("SQL")
            osType = clients_testdata.getType("HyperV")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Enable encryption for UEB
            #encryption.enableEncryption(token, encryption_testdata.systemId, encryption_testdata.passphrase)
            actualResponse = encryption.enableEncryption(token, encryption_testdata.systemId, encryption_testdata.passphrase)            
            reportController.addTestStepResult("Send API request for enabling encryption" , ExecutionStatus.PASS) 
            
            clientId = vclients.getSpecificClientByNameDB(clientName)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId):
                clients.deleteClients(token, clientId)
                logHandler.logging.info("client deleted")
            
            addedClientFlag =1
            actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                clients_testdata.is_encrypted_on, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                clients_testdata.existing_credential)
                  
            logHandler.logging.info("Actual Response for post client- " + str(actualResponse))
            reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
            clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
            logHandler.logging.info("Client ID returned from response: " + str(clientId)) 
                
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                            
            instanceId = vbackups.getInstanceIdFromVm(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            instanceEncryptResponse= encryption.enableInstanceEncryption(token, instanceId)
            logHandler.logging.info("Instance encryption Response - " + str(instanceEncryptResponse)) 
            
            # Step 3: Send the PUT request 
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))            
            reportController.addTestStepResult("Send API request for full backup" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = '-1'
            strJobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID from Response: " + str(strJobId))
            if(strJobId == '-1'):
                raise Exception("Failed to trigger backup")
            
            backupJobStatus=jobs.waitForJobCompletion(backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            if(backupJobStatus!="Successful"):
                logHandler.logging.error("Backup not successful")
                reportController.addTestStepResult("Backup success", ExecutionStatus.FAIL)
                raise Exception("Backup not successful")
            else:
                logHandler.logging.info("Backup successful")
                reportController.addTestStepResult("Backup Success" , ExecutionStatus.PASS)
                
            bid=vbackups.getBackupId(strJobId)
            logHandler.logging.info("Backup ID returned from DB: " + str(bid))
            
            catalogResponse= backups.getBackupCatalogInstanceResponse(token)
            
            #varify encryption
            encrypt = vencryption.getEncryptionFromCatalogResponse(catalogResponse, instanceName, bid)
            if(encrypt == False):
                logHandler.logging.error("Backup not encrypted")
                reportController.addTestStepResult("encryption verification", ExecutionStatus.FAIL)
                raise Exception("Backup not encrypted")
            else:
                logHandler.logging.info("Backup encrypted")
                reportController.addTestStepResult("encryption verification" , ExecutionStatus.PASS)
                
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
        
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
            
        finally:
            #Cleanup
            if(addedClientFlag == 1 or result == 'Fail'):
                clients.deleteClients(token, clientId)
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)

            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
            
            
                
            
                
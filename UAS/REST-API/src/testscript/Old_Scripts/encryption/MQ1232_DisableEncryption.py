'''
Created on Feb 24, 2015

@author: Amey.k
'''

import unittest, time
from resourcelibrary import encryption, login, logout
from testdata import encryption_testdata
from util import reportController, logHandler
from verificationsript import vencryption
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1232_DisableEncryption(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Disable Encryption")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Disable Encryption")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            logHandler.logging.info("Thread sleep for 60 sec")
            time.sleep(60)            
            
            # Step 1: Send request to get all alerts            
            actualResponse = encryption.disableEncryption(token, encryption_testdata.systemId, encryption_testdata.passphrase)            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            responseResult = encryption.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
                   
            # Verification 2: Verify get all alerts API response        
            encryptionFromMachine = vencryption.getEncryptionInformationFromMAchine()
            logHandler.logging.info("Data returned from Machine: " + str(encryptionFromMachine))
            
            if (str(encryptionFromMachine[0]) == "off"):
                reportController.addTestStepResult("Encryption disabled", ExecutionStatus.PASS)
            else:
                raise Exception ("Encryption not disabled")
                        
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on Jul 21, 2015

@author: rahula
'''
import unittest
from resourcelibrary import encryption, login, logout
from testdata import encryption_testdata
from util import reportController, logHandler
from verificationsript import vencryption
from config.Constants import ExecutionStatus
from config import apiconfig
from time import sleep


class MQ1730_PutEncryptionPersistentForSystem(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "PUT make encryption persistent for a system")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Make encryption persistent for a system")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Prerequisite setup - remove encryption persistence from command line and reboot the system       
            logHandler.logging.info("Removing the encryption persistence")
            encryption.removeEncryptionPersistence()    
            reportController.addTestStepResult("Removed the encryption persistence from the appliance", ExecutionStatus.PASS)     
            #Wait for system reboot
            sleep(120)
            
            # Step 1: Send request to make encryption persistent for a system
            actualResponse = encryption.persistEncryption(token, encryption_testdata.systemId, encryption_testdata.passphrase)            
            reportController.addTestStepResult("Send API request to make encryption persistent for a system" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response and verify it
            responseResult = encryption.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
            #Verify that a file has been created as cryptoDaemonPersistence on an appliance
            responseResult = vencryption.checkPersistence()
            logHandler.logging.info("Check Encryption Persistence Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Check Encryption Persistence", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            reportController.tcExecutionStatus(ExecutionStatus.PASS)   
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            reportController.generateReportSummary(self.executionId)
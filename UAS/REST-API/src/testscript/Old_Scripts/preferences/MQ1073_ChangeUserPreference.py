'''
Created on Oct 8, 2014

@author: Amey.k
'''

import unittest
from resourcelibrary import preferences, login, logout
from testdata import preferences_testdata
from util import reportController
from verificationsript import vpreferences
from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1073_ChangeUserPreference(unittest.TestCase):    
    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Change User Preference")
        self.executionId = exeID

    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            listresponse = []
            # Step 1: Send request to get all alerts
            actualResponse = preferences.changeUserPreferences(preferences_testdata.username, token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            listresponse.append(str(actualResponse).strip(' \r\t\n'))
            # vpreferences.getAllPreferencesFromResponse(actualResponse)
            # Get actual response parameter from response
          
         
            # sort expected response parameter list            
            preferences_testdata.response.sort()
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(preferences_testdata.response, listresponse, "Failed Response verification: Expected response [" + str(preferences_testdata.response) + "] and Actual parameter [" + str(listresponse) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
            # Verification 2: Verify get all alerts API response        
            prefFromDB = vpreferences.getUserSpecificPreferencesFromDB()
            del prefFromDB['show_eula']
            del prefFromDB['dashboard_settings']
            del prefFromDB['dash_forum_posts_refresh']
            del prefFromDB['dash_daily_feed_refresh']
            changedResponse = preferences.getUserPreferences(preferences_testdata.username, token)
            prefFromResponse = vpreferences.getAllPreferencesFromResponse(changedResponse)
            self.assertDictEqual(prefFromDB, prefFromResponse, "Failed Data base verification: Expected Response [" + str(prefFromDB) + "] and Actual Response [" + str(prefFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)        
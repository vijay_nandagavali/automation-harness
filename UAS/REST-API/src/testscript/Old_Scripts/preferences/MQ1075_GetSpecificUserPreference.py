'''
Created on Oct 8, 2014

@author: Amey.k
'''

import unittest
from resourcelibrary import preferences, login, logout
from testdata import preferences_testdata
from util import reportController, logHandler
from verificationsript import vpreferences
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1075_GetSpecificUserPreference(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Get Specific User Preference")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tGET - Specific User Preference")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            # Step 1: Send request to get all alerts
            actualResponse = preferences.getUserSpecificPreference(preferences_testdata.username, token, preferences_testdata.preference)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # vpreferences.getAllPreferencesFromResponse(actualResponse)
            # Get actual response parameter from response
            actualResponseParams = preferences.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            preferences_testdata.preferenceParameter.sort()
            logHandler.logging.info("Expected Response Parameters - " + str(preferences_testdata.preferenceParameter))
            
            # Verification 1: Verify get all alerts response parameter        
            self.assertListEqual(preferences_testdata.preferenceParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(preferences_testdata.preferenceParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
            reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                    
            # Verification 2: Verify get all alerts API response        
            prefFromDB = vpreferences.getUserSpecificPreferenceParameterFromDB()
            logHandler.logging.info("Data returned from DB: " + str(prefFromDB))
            
            prefFromResponse = vpreferences.getSpecificUserParameterFromResponse(actualResponse)
            logHandler.logging.info("Data from response: " + str(prefFromResponse))
            
            self.assertDictEqual(prefFromDB, prefFromResponse, "Failed Data base verification: Expected Response [" + str(prefFromDB) + "] and Actual Response [" + str(prefFromResponse) + "]")
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
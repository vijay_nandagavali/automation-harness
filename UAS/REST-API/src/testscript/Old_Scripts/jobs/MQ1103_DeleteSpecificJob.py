'''
Created on Mar 05, 2015

@author: Rahul A.
'''

import unittest, time
from resourcelibrary import jobs, login, logout, clients, backups, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from verificationsript import vjobs, vclients, vbackups
from util import reportController, logHandler
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1103_DeleteSpecificJob(unittest.TestCase):   
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete a specific job")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Specific Job")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            addedClientFlag = 0
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clients_testdata.name)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            if(clientId==0):
                addedClientFlag =1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                     clients_testdata.existing_credential)
                  
                reportController.addTestStepResult("Send API request for Post Client" , ExecutionStatus.PASS)
                # Step 2: Obtain the client ID
                clientId = clients.getPostClientsRequestResponse(actualResponse, clients_testdata.keyForId)
                logHandler.logging.info("Client ID returned from response: " + str(clientId))
                if (clientId):
                        reportController.addTestStepResult("Add Client as a prerequisite", ExecutionStatus.PASS)
                else:
                    raise Exception ("Failed to add a client")
                    
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            # Step 3: Send the PUT request
            actualResponse = backups.putHypervInstanceBackup(token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            reportController.addTestStepResult("Send API request to create Backup job" , ExecutionStatus.PASS)
             
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            JobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID from response: " + str(JobId))
            
            #Send DELETE job request
            actualResponse = jobs.deleteJob(token, JobId)
            reportController.addTestStepResult("Send API request to delete job" , ExecutionStatus.PASS)
            
            #Verification 1 - verify response parameter
            responseResult = jobs.verifyDeleteResponse(actualResponse)
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
            
            #Verification 2 - Query the database for job_no where (status & 32776) >0;
            dbList = vjobs.getDeletedJobFromDB()
            logHandler.logging.info("Data returned from DB: " + str(dbList))
            
            for bid in range(0, len(dbList)):
                deleteflag = 0
                if(str(dbList[bid])==str(JobId)):
                    deleteflag=1
                    reportController.addTestStepResult("Job successfully deleted from the Database", ExecutionStatus.PASS)
                    break
            
            if(deleteflag==0):
                raise Exception ("Job not deleted from the Database")
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
                
        finally:
            if(addedClientFlag == 1 or result == 'Fail'):
                logHandler.logging.info("Thread sleep for 30 sec")
                time.sleep(30)
                clients.deleteClients(token, clientId) 
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
                   
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
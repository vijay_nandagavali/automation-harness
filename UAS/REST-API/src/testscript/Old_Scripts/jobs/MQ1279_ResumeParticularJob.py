'''
Created on Mar 23, 2015

@author: Amey.k
'''

import unittest, time
from resourcelibrary import jobs, login, logout, clients, backups, inventory
from testdata import clients_testdata, backups_testdata, inventory_testdata
from verificationsript import vclients, vbackups
from util import reportController, logHandler
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1279_ResumeParticularJob(unittest.TestCase):   
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Resume a specific job")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPUT - Resume particular job")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            #Test data used in the script
            clientName=clients_testdata.getmachineName("HyperV")
            clientIp=clients_testdata.getmachineIP("HyperV")
            instanceName=clients_testdata.getShareName("HyperV")
            osType = clients_testdata.getType("HyperV")
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
             
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(clients_testdata.name)
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            clientCreated = 0
            if(clientId==0):
                clientCreated = 1
                actualResponse = clients.postClient(token,clients_testdata.sid, clientName, osType, 
                                                     clients_testdata.priority, clients_testdata.is_enabled, clients_testdata.is_synchable, 
                                                     clients_testdata.is_encrypted, clients_testdata.is_auth_enabled, clients_testdata.use_ssl, 
                                                     clients_testdata.defaultschedule, clients_testdata.port, clientIp, 
                                                     clients_testdata.existing_credential)
                reportController.addTestStepResult("Client Added" , ExecutionStatus.PASS)
                clientId = vclients.getSpecificClientByNameDB(clients_testdata.name)
                logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            
            result = inventory.waitForInventorySync(token, inventory_testdata.waittime, inventory_testdata.sid)
            logHandler.logging.info("Result: " + str(result))
            self.assertNotEqual(result,"Fail","Inventory sync failed")
            reportController.addTestStepResult("Inventory sync", ExecutionStatus.PASS)
                                
            instanceId = vbackups.getInstanceIdFromHyperv(instanceName)
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")

            # Step 3: Send the PUT request - use hard coded client id as 41 till host deletion issue get resolved then uncomment above code
            actualResponse = backups.putHypervInstanceBackup(token,instanceId, backups_testdata.backupType, backups_testdata.storageType, backups_testdata.verifyLevel, backups_testdata.sid)
            reportController.addTestStepResult("Backup Triggered" , ExecutionStatus.PASS)
             
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            JobId = responseResult["data"][0]["job_id"] 
            logHandler.logging.info("Job ID returned from response: " + str(JobId))
            
            #Send PUT Request for Resume job
            actualResponse = jobs.resumeJob(token, JobId)
            reportController.addTestStepResult("Send API request for Resume job" , ExecutionStatus.PASS)

            #Verification 1 - verify response parameter
            responseResult = jobs.verifyResumedJobResponse(actualResponse)
            logHandler.logging.info("Response Result: " + str(responseResult))
            if (responseResult == "Pass"):
                reportController.addTestStepResult("Job resumed successfully", ExecutionStatus.PASS)
            else:
                raise Exception ("Job could not be resumed")
            
            logHandler.logging.info("Thread sleep for 60 sec")
            time.sleep(60)
            
            jobs.deleteJob(token, JobId)
            logHandler.logging.info("Job deleted..")
            reportController.addTestStepResult("Delete job" , ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)

        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
        
        finally:
            #Delete client
            if(clientCreated == 1 or result == 'Fail'):                
                clients.deleteClients(token, clientId) 
                logHandler.logging.info("Client that is added in this test has been deleted..")
                reportController.addTestStepResult("Client deleted" , ExecutionStatus.PASS)
             
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
'''
Created on Sep 22, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import login, logout
from resourcelibrary import jobs
from testdata import jobs_testdata

from verificationsript import vjobs
from util import reportController
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1119_GetListofActiveJobs(unittest.TestCase):
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get List Of Active Jobs")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser, apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = jobs.getJobs(token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = jobs.getListOfJobParameterInJson(actualResponse)
            print actualResponseParams
            
            if(len(actualResponseParams)>1):
                # sort expected response parameter list            
                jobs_testdata.responseParameter.sort()
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(jobs_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(jobs_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                # Verification 2: Verify get all alerts API response                           
                jobsFromDB = vjobs.getJobsFromDB()
                jobsFromDB.sort()
                print jobsFromDB
            
                jobsFromResponse = vjobs.getJobsFromResponse(actualResponse)
                jobsFromResponse.sort()
                print jobsFromResponse
                self.assertListEqual(jobsFromDB, jobsFromResponse, "Failed Data base verification: Expected Response [" + str(jobsFromDB) + "] and Actual Response [" + str(jobsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Jobs are not present", ExecutionStatus.PASS)			
             
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)                    
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
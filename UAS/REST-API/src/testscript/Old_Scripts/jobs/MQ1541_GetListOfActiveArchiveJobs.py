'''
Created on Sep 22, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import login, logout
from resourcelibrary import jobs
from testdata import jobs_testdata

from verificationsript import vjobs
from util import reportController
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1541_GetListOfActiveArchiveJobs(unittest.TestCase):
   
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get List Of Active Restore Jobs")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = jobs.getArchiveJobs(token)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = jobs.getListOfJobParameterInJson(actualResponse)
            print actualResponseParams
            if(len(actualResponseParams)>1):             
                # sort expected response parameter list            
                jobs_testdata.responseParameterArchive.sort()
                
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(jobs_testdata.responseParameterArchive, actualResponseParams, "Failed Response verification: Expected response [" + str(jobs_testdata.responseParameterArchive) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response   
                list_Restore_Ids = vjobs.getListOfJobSpecificIds(actualResponse)     
                jobsFromDB = vjobs.getSpecificJobsFromDB(list_Restore_Ids)
                jobsFromDB.sort()
                print jobsFromDB
                
                jobsFromResponse = vjobs.getSpecificJobsFromResponse(actualResponse)
                jobsFromResponse.sort()
                print jobsFromResponse
                self.assertListEqual(jobsFromDB, jobsFromResponse, "Failed Data base verification: Expected Response [" + str(jobsFromDB) + "] and Actual Response [" + str(jobsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)




'''
Created on Jul 20, 2015

@author: savitha.p
'''

import unittest
from util import logHandler, reportController
from config import apiconfig
from config.Constants import ExecutionStatus
from resourcelibrary import login, jobs, logout
from testdata import jobs_testdata

class MQ1726_GetListOfActiveIRJobs(unittest.TestCase):
    def __init__(self,exeID):
        reportController.initializeReport(type(self).__name__, "Get list of all active IR jobs")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\t GET - List of Active IR Jobs")
        self.executionId = exeID
    
    def runTest(self):
        try:            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Send API Request
            actualResponse = jobs.getListofActiveIRJobs(token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
                        
            #Parse the actual response
            actualResponseParams = jobs.getActiveIRJobParameters(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            
            logHandler.logging.info("Length of Actual Response Parameters - " + str(len(actualResponseParams)))
            if(len(actualResponseParams) > 0):
                expectedParams = jobs_testdata.irResponseParameter
                expectedParams.sort()
                logHandler.logging.info("Expected Response Parameters - " + str(expectedParams))
                
                self.assertListEqual(actualResponseParams, expectedParams, "Response verification failed")
                reportController.addTestStepResult("Response verification" , ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Data not present" , ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.generateReportSummary(self.executionId)
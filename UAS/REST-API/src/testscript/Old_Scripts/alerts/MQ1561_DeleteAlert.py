'''
Created on Jun 11, 2015

@author: savitha.p
'''

import unittest
from resourcelibrary import alerts, login, logout
from util import reportController, logHandler
from verificationsript import valerts
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1561_DeleteAlert(unittest.TestCase):    
    def __init__(self, exeID):            
        reportController.initializeReport(type(self).__name__, "Delete an Alert")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tDELETE - Alert")
        self.executionId = exeID
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            #Get open alerts from DB
            openAlerts = valerts.getUnresolvedAlertsFromDB()
            logHandler.logging.info("Open Alerts: " + str(openAlerts))
            
            if (len(openAlerts) > 0):
                #Fetch the Alert ID
                alertId = openAlerts[0]
                logHandler.logging.info("Alert ID: " + str(alertId))
                
                #Send DELETE request
                actualResponse = alerts.deleteAlert(token, alertId)
                reportController.addTestStepResult("Send API Request", ExecutionStatus.PASS)
                
                #Parse Actual Response
                responseResult = valerts.verifyDeleteAlertFromResponse(actualResponse)
                logHandler.logging.info("Response Result: " + str(responseResult))
                if(responseResult == 'Pass'):
                    logHandler.logging.info("Response verification successful")
                    reportController.addTestStepResult("Response verification", ExecutionStatus.PASS)
                else:
                    raise Exception("Response verification failed")
                
                #DB verification
                alertStatus = valerts.getStatusOfParticularAlertFromDB(alertId)
                logHandler.logging.info("Alert Status: " + str(alertStatus))
                if(alertStatus == True):
                    logHandler.logging.info("Database verification successful")
                    reportController.addTestStepResult("Database verification", ExecutionStatus.PASS)
                else:
                    raise Exception("Database verification failed")                
            else:
                logHandler.logging.info("No open alerts available....")
                reportController.addTestStepResult("No open alerts available", ExecutionStatus.PASS)
                        
            #Logout
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))    
        finally:
            reportController.generateReportSummary(self.executionId)
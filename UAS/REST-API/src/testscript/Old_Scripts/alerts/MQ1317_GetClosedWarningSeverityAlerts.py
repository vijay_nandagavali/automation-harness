'''
Created on Dec 2, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import alerts, login, logout
from testdata import alerts_testdata
from util import reportController
from verificationsript import valerts
from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1317_GetClosedWarningSeverityAlerts(unittest.TestCase):
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Closed Warning Severity Alerts")
        self.executionId = exeID
    
   
    def runTest(self):        
        
        try:
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            
            actualResponse = alerts.getSeverityLevelClosedAlerts(alerts_testdata.severity[1], token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            
            actualResponseParams = alerts.getListOfActualResponseParameter(actualResponse)
            
            # sort expected response parameter list            
            
            # Verification 1: Verify get all alerts response parameter
            if (len(actualResponseParams)>1):
                alerts_testdata.responseParameter.sort()     
                self.assertListEqual(alerts_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(alerts_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                # Verification 2: Verify get all alerts API response  
                alertIdList = valerts.getListOfAlertId(actualResponse)      
                alertsFromDB = valerts.getClosedSeverityAlertsFromDB(alerts_testdata.warning, alertIdList)
                
                print alertsFromDB
                alertsFromResponse = valerts.getAllAlertsFromResponse(actualResponse)
                print alertsFromResponse
                self.assertDictEqual(alertsFromDB, alertsFromResponse, "Failed Data base verification: Expected Response [" + str(alertsFromDB) + "] and Actual Response [" + str(alertsFromResponse) + "]")
            else:
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            

'''
Created on Dec 2, 2014

@author: Amey.k
'''

import unittest

from resourcelibrary import alerts, login, logout
from testdata import alerts_testdata
from util import reportController, logHandler
from verificationsript import valerts
from config.Constants import ExecutionStatus
from config import apiconfig


class MQ1314_GetOpenWarningSeverityAlerts(unittest.TestCase):
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Open Warning Severity Alerts")
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tPOST - Get Open Warning Severity Alerts")
        self.executionId = exeID
    
   
    def runTest(self):        
        
        try:
            
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            logHandler.logging.info("Auth Token: " + str(token))
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            
            actualResponse = alerts.getSeverityLevelOpenAlerts(alerts_testdata.severity[1], token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            logHandler.logging.info("Actual Response" + str(actualResponse))
            # Get actual response parameter from response
            
            actualResponseParams = alerts.getListOfActualResponseParameter(actualResponse)
            logHandler.logging.info("Actual Response Parameters - " + str(actualResponseParams))
            if (len(actualResponseParams)>1): 
                # sort expected response parameter list            
                alerts_testdata.responseParameter.sort()
                logHandler.logging.info("Test data parameters - " + str(alerts_testdata.responseParameter))
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(alerts_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(alerts_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
                # Verification 2: Verify get all alerts API response      
                listAlertId = valerts.getListOfAlertId(actualResponse)  
                alertsFromDB = valerts.getOpenSeverityAlertsFromDB(alerts_testdata.warning, listAlertId)
                print alertsFromDB
                logHandler.logging.info("Alerts from DB - " + str(alertsFromDB))
                alertsFromResponse = valerts.getAllAlertsFromResponse(actualResponse)
                print alertsFromResponse
                logHandler.logging.info("Alerts from API Response - " + str(alertsFromResponse))
                self.assertDictEqual(alertsFromDB, alertsFromResponse, "Failed Data base verification: Expected Response [" + str(alertsFromDB) + "] and Actual Response [" + str(alertsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)
                logHandler.logging.info("Data not found - marking execution status as PASS")
                
            logoutResult = logout.doUserLogout(token)
            if(logoutResult == "Pass"):
                logHandler.logging.info("Logout successful..")
                reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Logout failed" , ExecutionStatus.FAIL)
                raise Exception("Logout failed")
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
            

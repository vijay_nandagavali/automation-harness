__all__ = ['MQ1090_GetCriticalSeverityAlerts', 'MQ1091_GetWarningSeverityAlerts', 'MQ1092_GetNoticeSeverityAlerts', 'MQ1312_GetOpenAlerts', 'MQ1313_GetClosedAlerts', 'MQ1314_GetOpenWarningSeverityAlerts', 'MQ1315_GetOpenNoticeSeverityAlerts', 'MQ1316_GetOpenCriticalSeverityAlerts', 'MQ1317_GetClosedWarningSeverityAlerts', 'MQ1318_GetClosedNoticeSeverityAlerts', 'MQ1319_GetClosedCriticalSeverityAlerts', 'MQ1320_GetListOfAllAlerts', 'MQ1321_GetDetailsOfSpecificAlertID', 'MQ1561_DeleteAlert']
# Don't modify the line above, or this line! 
import automodinit 
automodinit.automodinit(__name__, __file__, globals()) 
del automodinit 
# Anything else you want can go after here, it won't get modified. 
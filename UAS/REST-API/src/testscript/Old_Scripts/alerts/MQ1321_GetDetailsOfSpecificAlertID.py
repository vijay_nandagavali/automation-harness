'''
Created on Sep 9, 2014

@author: Abhijeet.m
'''

import unittest

from resourcelibrary import alerts, login, logout
from testdata import alerts_testdata
from util import reportController
from verificationsript import valerts
from config.Constants import ExecutionStatus
from config import apiconfig





class MQ1321_GetDetailsOfSpecificAlertID(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get Details of Specific Alert")
        self.executionId = exeID
        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = alerts.getAlertsOfAlertID(alerts_testdata.alertID, token)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            actualResponseParams = alerts.getListOfActualResponseParameter(actualResponse)
            if (len(actualResponseParams)>1): 
                # sort expected response parameter list            
                alerts_testdata.responseParameter.sort()
                # Verification 1: Verify get all alerts response parameter        
                self.assertListEqual(alerts_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(alerts_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response        
                alertsFromDB = valerts.getSpecificAlertFromDB(alerts_testdata.alertID)
                alertsFromResponse = valerts.getAllAlertsFromResponse(actualResponse)
                self.assertDictEqual(alertsFromDB, alertsFromResponse, "Failed Data base verification: Expected Response [" + str(alertsFromDB) + "] and Actual Response [" + str(alertsFromResponse) + "]")
                reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            else:
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)    
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)



'''
Created on Feb 25, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import forum, login, logout
from testdata import forum_testdata
from util import reportController
from verificationsript import vforum
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1236_CreateNewForum(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Create New Forum")
        self.executionId = exeID        
        
    def runTest(self):        
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            
            actualResponse = forum.postForum(token, forum_testdata.sid, forum_testdata.userId, forum_testdata.forumUsername, forum_testdata.forumPassword, forum_testdata.displayName,forum_testdata.domainName,forum_testdata.superuser)
            
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            # Get actual response parameter from response
            responseResult = forum.getPostResponseParse(actualResponse, forum_testdata.keyForId)
            if (responseResult != ""):
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)
            else:
                raise Exception ("Failed response verification")
                   
            # Verification 2: Verify get all alerts API response        
            forumInfoFromMachine = vforum.getForumFromDB(forum_testdata.userId)
            print forumInfoFromMachine
            if (len(forumInfoFromMachine) != 0):
                reportController.addTestStepResult("Forum successfully created for a user", ExecutionStatus.PASS)
            else:
                raise Exception ("Forum not created")
            
            
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
            
            
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
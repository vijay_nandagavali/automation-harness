'''
Created on Feb 26, 2015

@author: Amey.k
'''

import unittest

from resourcelibrary import forum, login, logout
from testdata import forum_testdata
from util import reportController
from verificationsript import vforum
from config.Constants import ExecutionStatus
from config import apiconfig

class MQ1235_GetUserForumInformation(unittest.TestCase):    
    def __init__(self, exeID):
            
        reportController.initializeReport(type(self).__name__, "Get User Forum Information")
        self.executionId = exeID        
        
    def runTest(self):
        try:
            token = login.getAuthenticationToken(apiconfig.machineuser,apiconfig.machinepassword)            
            print token
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            # Step 1: Send request to get all alerts
            actualResponse = forum.getUserForumInformation(token, forum_testdata.userId, forum_testdata.sid)
            reportController.addTestStepResult("Send API request" , ExecutionStatus.PASS)
            
            # Get actual response parameter from response
            actualResponseParams = forum.getForumParametersInJson(actualResponse)
            print actualResponseParams
            # sort expected response parameter list            
            forum_testdata.responseParameter.sort()
            # Verification 1: Verify get all alerts response parameter
            if (len(actualResponseParams)>1):        
                self.assertListEqual(forum_testdata.responseParameter, actualResponseParams, "Failed Response verification: Expected response [" + str(forum_testdata.responseParameter) + "] and Actual parameter [" + str(actualResponseParams) + "]")
                reportController.addTestStepResult("Verify response parameter", ExecutionStatus.PASS)        
                # Verification 2: Verify get all alerts API response
                cred_id = vforum.getForumFromDB(forum_testdata.userId)       
                forumListFromDB = vforum.getForumInformationFromDB(forum_testdata.userId,cred_id[0])
                print forumListFromDB
                forumListFromResponse = vforum.getForumInformationFromResponse(actualResponse)
                print forumListFromResponse
                self.assertListEqual(forumListFromDB, forumListFromResponse, "Failed Data base verification: Expected Response [" + str(forumListFromDB) + "] and Actual Response [" + str(forumListFromResponse) + "]")
            else:
                reportController.addTestStepResult("Data not present", ExecutionStatus.PASS)
            reportController.addTestStepResult("Compare API response with data base values", ExecutionStatus.PASS)
            logoutResponse = logout.doUserLogout(token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
           
            reportController.tcExecutionStatus(ExecutionStatus.PASS)
        
        
        
        except Exception as e:
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            print e    
        finally:
            reportController.generateReportSummary(self.executionId)
'''
Created on Mar 1, 2017

@author: deepanshu.s
'''

import unittest
from resourcelibrary import clients, backups,login, logout, jobs, inventory, restore
from testdata import clients_testdata, backups_testdata, inventory_testdata, restore_testdata
from util import reportController, logHandler, clientMessageUtilities
from verificationsript import vbackups, vrestore
from verificationsript import vclients
from config.Constants import ExecutionStatus
from config import apiconfig
from time import sleep

class QAAUT_HyV_Full(unittest.TestCase):    
    def __init__(self, exeID, uebInfra, machineInfra):            
        self.executionId = exeID
        '''
        Initialize UEB Configuration test steps
        '''
        
        self.uebInfra=uebInfra
        self.machineInfra=machineInfra 
        '''
        Initialize machine environment
        ''' 
        self.name=machineInfra["hypervname"] if "hypervname" in machineInfra.keys() else ""
        self.ipAddress=machineInfra["hypervipAddress"] if "hypervipAddress" in machineInfra.keys() else ""
        self.flavour=machineInfra["hypervflavour"] if "hypervflavour" in machineInfra.keys() else ""
        self.instanceName="centos6"
        self.instanceIP = "192.168.139.219"
        self.vmType = "HyperV"
        self.datastore = "datastore1"
        self.includeList = "/root/Backup_folder"
        self.restoreFolder = "/root/Restore_folder"
                
        reportController.initializeReport(type(self).__name__, "Verify a full backup for the specified HyperV instance" + " : " + str(self.flavour))
        logHandler.logFile(self.executionId)
        logHandler.logging.info("EXECUTION ID :::\t" + str(exeID))
        logHandler.logging.info("TEST TITLE :::\tVerify a full backup for the specified HyperV instance" + " : " + str(self.flavour))
        
        
    def runTest(self):        
        try:
            appliance_ip=self.uebInfra["Mainurl"]
            addedClientFlag = 0
            token = login.getAuthenticationToken(appliance_ip,apiconfig.machineuser,apiconfig.machinepassword)            
            logHandler.logging.info("Auth Token: " + str(token))
            print "Auth Token: " + str(token)
            self.assertNotEqual(token, "", "Empty token received")
            reportController.addTestStepResult("Login", ExecutionStatus.PASS)
            
            
            #Step 1: Send request to update a client
            clientId = vclients.getSpecificClientByNameDB(appliance_ip, self.name)
            print ("Client ID returned from DB: " + str(clientId))
            logHandler.logging.info("Client ID returned from DB: " + str(clientId))
            
            #Step 2: if client does not exists add a client
            if(clientId==0):
                actualResponse = clients.postClient(appliance_ip, token,clients_testdata.sid, self.vmType, 
                                                clients_testdata.vm_is_enabled, clients_testdata.vm_is_synchable, 
                                                clients_testdata.vm_is_encrypted, self.name, 
                                                self.ipAddress, 
                                                clients_testdata.vm_username, clients_testdata.vm_password 
                                                )
                print actualResponse
                reportController.addTestStepResult("Add Virtual Host" , ExecutionStatus.PASS) 
                  
                addedClientFlag =1
                clientId = vclients.getSpecificClientByNameDB(appliance_ip, self.name)
                logHandler.logging.info("Client ID returned from DB: " + str(clientId))
                ("Client ID returned from DB: " + str(clientId))
            else:
                reportController.addTestStepResult("Add Virtual Host" , ExecutionStatus.PASS)                         
            # Step 3: Get instance Id from database
            instanceId = vbackups.getInstanceIdFromHyperv(appliance_ip, self.instanceName)
            print ("Instance ID returned from DB: " + str(instanceId))
            logHandler.logging.info("Instance ID returned from DB: " + str(instanceId))
            self.assertNotEqual(instanceId,None,"Instance Id not found")
            
            clientMessageUtilities.SetupLinux(self.instanceIP)
            clientMessageUtilities.addCustomSizedfilesOnLinux(self.instanceIP,self.includeList+"/",3,5,"M")
            
            
            #Step 4: Put request for full backup 
            actualResponse = backups.putHypervInstanceBackup(appliance_ip, token,instanceId,backups_testdata.backupType,backups_testdata.storageType,backups_testdata.verifyLevel,backups_testdata.sid)
            logHandler.logging.info("Actual Response - " + str(actualResponse))
            print "Actual Response for put backup - " + str(actualResponse)
            # reportController.addTestStepResult("Triggering Backup Job" , ExecutionStatus.PASS)
            
            # Verification 1: verify the response parameter - compare the value of client_id obtained in the response
            responseResult = backups.getPutRequestResponse(actualResponse)
            strJobId = responseResult["data"][0]["job_id"] 
            strInstanceName = responseResult["data"][0]["instance_name"] 
            logHandler.logging.info("JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strInstanceName))
            print "JOID from Response - " + str(strJobId) + "Client Name from Response - " + str(strInstanceName)
            if str(strJobId) != "-1":
                reportController.addTestStepResult("Triggering Master Backup Job" , ExecutionStatus.PASS)
                logHandler.logging.info("Triggering Master Backup Job successful" )
            else:
                reportController.addTestStepResult("Triggering Master Backup Job" , ExecutionStatus.FAIL)
                logHandler.logging.info("Triggering Master Backup Job Not successful" )
            
            #Check the status of backup
            backupJobStatus=jobs.waitForJobCompletion(appliance_ip, backups_testdata.jobType, backups_testdata.waittime, token, strJobId)
            print "returned backup status" + backupJobStatus
            if(backupJobStatus =="Successful"):
                logHandler.logging.info("Backup successful")
                print "Backup Successful"
                reportController.addTestStepResult("Backup Successful" , ExecutionStatus.PASS)
            else:
                logHandler.logging.info("Master Backup not successful.  Backup Job Status is: " + str(backupJobStatus))
                reportController.addTestStepResult("Master Backup Failed with jobID:"+str(strJobId), ExecutionStatus.FAIL)
                print "Backup Failed with jobID:"+str(strJobId)
                
            #Get Backup No from jobid
            backupID = vrestore.getArchiveRestoreBackupIdFromDB(appliance_ip,strJobId)
            logHandler.logging.info("Backup ID based on jobid from Response - " + str(backupID)) 
            print "Backup ID based on jobid from Response - " + str(backupID)
                    
            # restore parameters
            actualResponse = restore.putHypervRestore(appliance_ip, token, backupID, self.instanceName, clientId)
            logHandler.logging.info("Restore API - Actual Response - " + str(actualResponse))
            print "Restore API - Actual Response - " + str(actualResponse)
            reportController.addTestStepResult("Triggering Restore of Client" , ExecutionStatus.PASS)
             
            #Verification 1: Extract jobid from response
            responseResult = backups.getPutRequestResponse(actualResponse)
            #strJobId = responseResult["data"][0]["id"]
            strJobId = responseResult["id"] 
            logHandler.logging.info("Restore JobId - " + str(strJobId))
            print "Restore JobId - " + str(strJobId)
             
            list_of_restore_job_ids = strJobId.split(",")
             
            for ids in list_of_restore_job_ids:
                #Check the status of Restore
                restoreJobStatus=jobs.waitForJobCompletion(appliance_ip,restore_testdata.jobType, backups_testdata.waittime, token, ids)
                  
                if(restoreJobStatus =="Successful" or restoreJobStatus =="Warning" ):
                    logHandler.logging.info("restore successful")
                    reportController.addTestStepResult("Restore Successful" , ExecutionStatus.PASS)
                else:
                    logHandler.logging.info("restore not successful.  restore Job Status is: " + str(backupJobStatus))
                    reportController.addTestStepResult("Restore not successful", ExecutionStatus.FAIL)
                    
            isVMPoweredOn = vrestore.isHyperVRestoredMachineAlive(self.instanceName,self.ipAddress)
            if(isVMPoweredOn):
                reportController.addTestStepResult("HyperV VM PoweredOn successful",ExecutionStatus.PASS)
                logHandler.logging.info("HyperV Restored VM Powered On Successfully")
            else:
                reportController.addTestStepResult("VM PowerOn Failed",ExecutionStatus.FAIL)
                logHandler.logging.info("Restored VM Powered On Failed")
                
            if(self.includeList==""):
                logHandler.logging.info("include list not present for linux HyperV vm,  so no client side verification")
            else:  
                checksum=clientMessageUtilities.verifyRestoreFolderAgainstBackupFolderOnLinux(self.includeList, self.restoreFolder+self.includeList, self.instanceIP)
                if(checksum):
                    reportController.addTestStepResult("Checksum restore verification successful",ExecutionStatus.PASS)
                    logHandler.logging.info("Backup and Restore files matched")
                else:
                    reportController.addTestStepResult("Backup and Restore files did not match",ExecutionStatus.FAIL)
                    logHandler.logging.info("Backup and Restore files did not match") 
            reportController.tcExecutionStatus(ExecutionStatus.PASS)            
            
        except Exception as e:
            print "found exception"
            reportController.addTestStepResult("\"" + str(e) + "\"" , ExecutionStatus.FAIL)
            reportController.tcExecutionStatus(ExecutionStatus.FAIL)
            logHandler.logging.error("Exception: " + str(e))   
        finally:
            #cleanup
            print "addedClientFlag: " + str(addedClientFlag)
            #if(addedClientFlag):
                #clients.deleteClients(appliance_ip,  token, clientId)
                #reportController.addTestStepResult("Delete client" , ExecutionStatus.PASS)
             
            #Logout
            print "log out started"
            logoutResponse = logout.doUserLogout(appliance_ip, token)
            self.assertNotEqual(logoutResponse, "Fail", "Logout not successful")
            reportController.addTestStepResult("Logout", ExecutionStatus.PASS)
            print "generating report summary"
            reportController.generateReportSummary(appliance_ip, self.executionId)
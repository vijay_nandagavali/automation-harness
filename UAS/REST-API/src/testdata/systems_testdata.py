'''
Created on Nov 26, 2014

@author: Amey.k
'''

responseParameter = ['archiving', 'customer_id', 'customer_name', 'host', 'id', 'local', 'location_id', 'location_name', 'name', 'registered_assets', 'replicating', 'role', 'status', 'total_mb_free', 'total_mb_size', 'version', 'version_status']
responseParameter1 = ['archiving', 'customer_id', 'customer_name', 'host', 'id', 'local', 'location_id', 'location_name', 'name', 'registered_assets', 'replicating', 'role', 'online', 'status', 'total_mb_free', 'total_mb_size', 'version', 'version_status', 'storage_name']
systemId = "1"
oldSystemName = "my-ueb"
newSystemName = "test-ueb"

responseParameterSystemDetails = ['name','asset_tag','version','os_version','processors','memory','max_backup']

network = "172.17.3.0"
mask = "255.255.255.0"
port ="1194"

list_Systems = [["UEB-152", "192.168.139.152"], ["UEB-153", "192.168.139.153"], ["UEB-154", "192.168.139.154"], ["UEB-155", "192.168.139.155"], 
["UEB-156", "192.168.139.156"], ["UEB-157", "192.168.139.157"], ["UEB-158", "192.168.139.158"], ["UEB-159", "192.168.139.159"], ["UEB-160", "192.168.139.160"], 
["UEB-161", "192.168.139.161"], ["UEB-162", "192.168.139.162"], ["UEB-163", "192.168.139.163"], ["UEB-164", "192.168.139.164"], ["UEB-166", "192.168.139.166"], 
["UEB-167", "192.168.139.167"], ["UEB-168", "192.168.139.168"], ["UEB-169", "192.168.139.169"], ["UEB-170", "192.168.139.170"], ["UEB-172", "192.168.139.172"], 
["UEB-173", "192.168.139.173"], ["UEB-174", "192.168.139.174"], ["UEB-175", "192.168.139.175"], ["UEB-176", "192.168.139.176"], ["UEB-177", "192.168.139.177"], 
["UEB-178", "192.168.139.178"], ["UEB-179", "192.168.139.179"], ["UEB-180", "192.168.139.180"], ["UEB-181", "192.168.139.181"], ["UEB-182", "192.168.139.182"], 
["UEB-183", "192.168.139.183"], ["UEB-184", "192.168.139.184"], ["UEB-185", "192.168.139.185"], ["UEB-186", "192.168.139.186"], ["UEB-187", "192.168.139.187"], 
["UEB-188", "192.168.139.188"], ["UEB-189", "192.168.139.189"], ["UEB-190", "192.168.139.190"], ["UEB-191", "192.168.139.191"], ["UEB-192", "192.168.139.192"], 
["UEB-193", "192.168.139.193"], ["UEB-194", "192.168.139.194"], ["UEB-195", "192.168.139.195"], ["UEB-196", "192.168.139.196"], ["UEB-197", "192.168.139.197"], 
["UEB-198", "192.168.139.198"], ["UEB-199", "192.168.139.199"], ["UEB-200", "192.168.139.200"], ["UEB-201", "192.168.139.201"], ["UEB-202", "192.168.139.202"], 
["UEB-203", "192.168.139.203"]]

dict_Systems2 = { 
"Satori_Raghav2":"192.168.199.212"
}

sysName="UEB-152"
sysIP="192.168.139.152"
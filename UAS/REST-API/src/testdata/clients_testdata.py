'''
Created on Oct 1, 2014

@author: Amey.k
'''
from config import initializeEnvironment
from config import Constants
from model import Environment
from util import logHandler
from os.path import sys

exeId = str(sys.argv[1])   
    
def getmachineIP(flavour):
    envJsonPath = Constants.getEnvironmentJsonPath(exeId)
    envObj = Environment.Environment(envJsonPath)
    machineInfra=initializeEnvironment.initializeMachineEnvironment(envObj, flavour, "1")
    client_IP=envObj.getDictValue(machineInfra, "ipAddress")
    logHandler.logging.info("Client IP: " + str(client_IP))
    return client_IP

def getmachineName(flavour):
    envJsonPath = Constants.getEnvironmentJsonPath(exeId)
    envObj = Environment.Environment(envJsonPath)
    machineInfra=initializeEnvironment.initializeMachineEnvironment(envObj, flavour, "1")
    client_Name=envObj.getDictValue(machineInfra, "name")
    logHandler.logging.info("Client Name:" + str(client_Name))
    return str(client_Name)

def getShareName(flavour):
    envJsonPath = Constants.getEnvironmentJsonPath(exeId)
    envObj = Environment.Environment(envJsonPath)
    machineInfra=initializeEnvironment.initializeMachineEnvironment(envObj, flavour, "1")
    share_Name=envObj.getDictValue(machineInfra, "shareName")
    logHandler.logging.info("Share Name:" + str(share_Name))
    return str(share_Name)

def getUserName(flavour):
    envJsonPath = Constants.getEnvironmentJsonPath(exeId)
    envObj = Environment.Environment(envJsonPath)
    machineInfra=initializeEnvironment.initializeMachineEnvironment(envObj, flavour, "1")
    userName=envObj.getDictValue(machineInfra, "username")
    logHandler.logging.info("Username:" + str(userName))
    return str(userName)

def getPassword(flavour):
    envJsonPath = Constants.getEnvironmentJsonPath(exeId)
    envObj = Environment.Environment(envJsonPath)
    machineInfra=initializeEnvironment.initializeMachineEnvironment(envObj, flavour, "1")
    password=envObj.getDictValue(machineInfra, "password")
    logHandler.logging.info("Password:" + str(password))
    return str(password)

def getType(flavour):
    envJsonPath = Constants.getEnvironmentJsonPath(exeId)
    envObj = Environment.Environment(envJsonPath)
    machineInfra=initializeEnvironment.initializeMachineEnvironment(envObj, flavour, "1")
    type=envObj.getDictValue(machineInfra, "type")
    logHandler.logging.info("Type:" + str(type))
    return str(type)

responseParameter = ['applications', 'asset_type', 'file_level_instance_id', 'grandclient', 'id', 'ip', 'is_enabled', 'is_encrypted', 'is_hyperv_cluster', 'is_synchable', 'machine_type', 'machine_type_id', 'name','os_family', 'os_type', 'os_type_id', 'priority', 'system_id', 'system_name', 'use_ssl', 'version', 'supports_agent_push', 'credentials']
responseParameter1 = ['asset_type', 'id', 'ip', 'name', 'system_id', 'system_name', 'username']
responseParameter_AgentCheck = ['address', 'id', 'message', 'name', 'update_available', 'version']
responseParameterListOfVolumes = ['directory', 'id', 'isBranch', 'name', 'type']


clientId = "1"

updateClientId = "2"
keyForId = "id"
sid = "1"
updateClientName = "HyperVAuto"
updateClientPriority = 200
#Parameters for add client
os_type = "Hyper-V"
name = "HyperVAuto"
instance_name="vm-1"
username = "Admininstrator"
password = "Unitrends1"
priority = 300
is_enabled = True
is_synchable = False
is_encrypted= False
is_encrypted_on= True
is_auth_enabled = True
use_ssl = False
defaultschedule = False
port = 10000
ip = "192.168.197.35"
existing_credential = True  
directory = 'C:' 
responseParameterListOfVolumes1 = ['C:/Documents and Settings/', ]

#Parameters for add client Xen
name_xenserver = "WinBackup"
os_type_xenserver = "Xen"
system_xenserver = "1"
ip_xenserver = "192.168.139.119"
username_xenserver = "root"
password_xenserver = "unitrends1"
is_enabled_xenserver = True
is_encrypted_xenserver = False
is_synchable_xenserver = False
is_auth_enabled_xenserver = True
instance_name_xenserver = "fa0b3800-e2da-4228-86bb-06291ebad5b5"

vm_os_type = "VMware"
vm_name = "test"
vm_priority = 300
vm_is_enabled = True
vm_is_synchable = False
vm_is_encrypted= False
vm_is_auth_enabled = True
vm_use_ssl = False
vm_defaultschedule = False
vm_port = 10000
vm_ip = "192.168.197.2"
vm_existing_credential = False
vm_username = "root"
vm_password = "unitrends1"
vm_domain = ""
vm_client_name = "vCenter-RRC"
vm_esx_server_name = "localhost.localdomain"

win_name = "Win63"
win_IP = "192.168.197.63"
win_OsType = "Windows"
win_InstallAgent = False
win_IsDefault = False

linux_name = "LinuxClient"
linux_IP = "192.168.197.51"
linux_OsType = "Linux"
linux_InstallAgent = False
linux_IsDefault = False

xenresponseParameter = ['applications', 'asset_type', 'credentials', 'file_level_instance_id', 'grandclient', 'id', 'ip', 'is_enabled', 'is_encrypted', 'is_hyperv_cluster', 'is_synchable', 'machine_type', 'machine_type_id', 'name', 'os_family', 'os_type', 'os_type_id', 'priority', 'supports_agent_push', 'system_id', 'system_name', 'use_ssl', 'version']
xenresponseParameter1 = ['applications', 'asset_type', 'file_level_instance_id', 'grandclient', 'id', 'ip', 'is_enabled', 'is_encrypted', 'is_hyperv_cluster', 'is_synchable', 'machine_type', 'machine_type_id', 'name', 'os_family', 'os_type', 'os_type_id', 'priority', 'supports_agent_push', 'system_id', 'system_name', 'use_ssl', 'version']
xenresponseParameter2 = ['applications', 'asset_type', 'credentials', 'file_level_instance_id', 'generic_property', 'grandclient', 'id', 'ip', 'is_enabled', 'is_encrypted', 'is_hyperv_cluster', 'is_synchable', 'machine_type', 'machine_type_id', 'name', 'os_family', 'os_type', 'os_type_id', 'priority', 'remote_address', 'supports_agent_push', 'system_id', 'system_name', 'use_ssl', 'version']

exchange_name = "XchangeAuto"
exchange_IP = "192.168.199.227"
exchange_database = "autotest"
is_encrypted_True= True
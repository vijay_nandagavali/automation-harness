'''
Created on Oct 1, 2014

@author: Amey.k
'''

responseParameter = ["system_id","system_name","wir","virtual_id","type","hypervisor_type","vm_name","ServerName","Mode","vm_ir","hv_ir"]
dataNotFoundResponse = ['hv_ir', 'system_id', 'system_name', 'vm_ir', 'wir']
responseParameter_VirtualClientByID = ["Included_Volumes","Memory","Processors","real_client","real_client_id","real_client_name","real_disks","real_memory","real_processors","virtual_client","virtual_id"]
sid = "1"
Processors = "1"
Memory = "1024"
efi = "Unknown"
Disks = "1"
Include = "C:/"
Exclude = ""
keyForVirtualId = "virtual_id"
type = "wir"
deleteFromHypervisor = "1"
responseParameter_VB =['BackupID', 'Candidates', 'ClientID', 'ClientName', 'ClientOS', 'GrandClient', 'VirtualID', 'system_id', 'system_name']
responseParameter_V =['BackupID', 'Candidates', 'ClientID', 'ClientName', 'ClientOS', 'GrandClient', 'system_id', 'system_name']
responseParameter_B =['Candidates', 'ClientID', 'ClientName', 'ClientOS', 'GrandClient', 'VirtualID', 'system_id', 'system_name']
#responseParameter_E =['Candidates', 'ClientID', 'ClientName', 'ClientOS', 'GrandClient', 'system_id', 'system_name']
responseParameter_E =['candidates', 'system_id', 'system_name']
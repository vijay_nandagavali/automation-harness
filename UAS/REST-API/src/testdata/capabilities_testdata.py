'''
Created on Mar 11, 2015

@author: Amey.k
'''

responseParameterCE = ['CE','virtual_machine','open_license','ui','vmware_ir','hyperv_ir','backup_strategies','backup_min_recurrence']
responseParameter = ['virtual_machine','open_license','ui','vmware_ir','hyperv_ir','backup_strategies','backup_min_recurrence']
responseParameter1 = ['virtual_machine','open_license','ui','legacy_archiving_mode','deduplication', 'vmware_ir','hyperv_ir','replication', 'failover_virtualization']
responseParameter2 = ['dedupNT','inlineDedup','virtual_machine','open_license','ui','legacy_archiving_mode','deduplication', 'vmware_ir','hyperv_ir','replication', 'failover_virtualization']
responseParameter3 = ['CE', 'deduplication', 'failover_virtualization', 'hyperv_ir', 'open_license', 'replication', 'ui', 'virtual_machine', 'vmware_ir']
'''
Created on Oct 20, 2014

@author: Amey.k
'''

responseParameter = ['asset_tag','mkt_name','name','install_date','expiration_date','feature_string','feature_string_description','key','class']
reqResponseParameter = ["link", "registration", "Appliance","ApplianceType","AssetTag","Class","ClientLock",
                    "Clients","Comment","DaemonHost","DaemonHostID","DaemonIP","Description","ExpirationDate","FeatureString",
                    "FriendlyFeature","IP","InstallDate","Key","LicensedCapacity","Memory","MemoryGB","Name","OS","ProcessorCache",
                    "ProcessorCores","ProcessorFrequency","ProcessorType","Product","Reserve","SerialNumber","Type","UserString",
                    "Users","Vendor information","Version","VF", "request"]
sid = "1"
summaryResponseParameter = ['title']
reponseParamUsedAndLicenseRes = ['applications', 'applications', 'license', 'license', 'limits', 'resources', 'servers', 'servers', 'sid', 'sockets', 'sockets', 'usage', 'vms', 'vms', 'workstations', 'workstations']
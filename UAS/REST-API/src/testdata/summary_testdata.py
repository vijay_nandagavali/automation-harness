'''
Created on Sep 18, 2014

@author: Amey.k
'''

responseParameter= ['last_updated', 'backup_errors', 'backup_not_protected','backup_not_protected-not_adjusted','backup_protected-not_adjusted','rpo_days','rpo_protected','backup_protected', 'replication_errors', 'replication_protected', 'replicated_backup_errors', 'replicated_backup_protected', 'replicated_backup_not_protected', 'restore_active', 'restore_active_flr', 'restore_active_ir', 'restore_recent', 'archive_errors', 'archive_protected', 'wir_errors', 'wir_protected']
responseParameter1=['last_updated', 'backup_errors', 'backup_not_protected', 'backup_not_protected-not_adjusted', 'backup_protected', 'backup_protected-not_adjusted', 'rpo_days', 'rpo_protected']
summarydaysresponseParameter3 = ['backup_speed','backup_size','backup_data_speed','replication_speed','restore_speed','archive_size','archive_speed']
summarydaysresponseParameter1 = ['backup_speed','backup_size','backup_data_speed']
summarydaysresponseParameter2 = ['backup_speed','backup_size','backup_data_speed','replication_speed']
summarydaysresponseParameter = ['archive_size', 'backup_data_speed', 'backup_size', 'backup_speed', 'replication_speed', 'replication_stats', 'restore_speed']
no_days = 2
summarycurrentsresponseParameter = ['alert_count','sync_last','sync_progress','sync_running','sync_status','message', 'max_severity']
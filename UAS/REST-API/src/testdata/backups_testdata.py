'''
Created on Feb 17, 2015

@author: nikhil.s, Rahul A.
'''

responseParameter= ['app_id', 'app_type', 'local_system_id', 'app_type','synth_capable','encrypted','catalog','day', 'instances', 'app_name', 'client_id', 'client_name', 'database_name', 'instance_id', 'instance_name', 'system_id', 'system_name','backups','id','type','start_date','disks','legal_hold','replicated','vmware_template','imported_from_archive','storage', 'status','hold','size']
backupsCatalogResponseParameter = ['System', 'app_name', 'app_type', 'backups', 'client_id', 'client_name', 'database_name', 'instance_id', 'instance_name', 'instances', 'system_id', 'system_name']
#specificBackupParameter = ['certified', 'client_config', 'client_id', 'client_name', 'complete', 'disk_metadata', 'duration', 'encrypted', 'gfs_rp_type', 'id', 'last', 'legalhold', 'output', 'purgeable', 'replicated', 'size', 'start_time', 'status', 'synth_capable', 'synthesized', 'type', 'verified', 'verify_status', 'vmware_template']
specificBackupParameter = ['certified', 'client_config', 'client_id', 'client_name', 'complete', 'disk_metadata', 'duration', 'encrypted', 'files', 'gfs_rp_type', 'id', 'last', 'legalhold', 'output', 'purgeable', 'replicated', 'size', 'start_time', 'status', 'synth_capable', 'synthesized', 'type', 'verified', 'verify_status', 'vmware_template']
postResponseParameter = ["id","client_id","client_name","type","start_time","synthesized","synth_capable","disk_metadata","client_config","replicated","duration","complete","status","size","encrypted","verified","verify_status","vmware_template","certified","output","purgeable","last","instance_id","app_id","app_name","is_cluster","guid","vm_name"]
postBackupSearchResponseParameter = ["pid","timestr"]
sid = "1"
backupType = "Full"
transactionType = "Transaction"
storageType = "internal"
verifyLevel = "inline"
backupResponseParameter = ['legalhold','gfs_rp_type','app_id', 'app_name', 'certified', 'client_config', 'client_id', 'client_name', 'complete', 'disk_metadata', 'duration', 'encrypted', 'guid', 'id', 'instance_id', 'is_cluster', 'last', 'output', 'purgeable', 'replicated', 'size', 'start_time', 'status', 'synth_capable', 'synthesized', 'type', 'verified', 'verify_status', 'vm_name', 'vmware_template']
#xenBackupResponseParameter = ['app_id', 'app_name', 'certified', 'client_config', 'client_id', 'client_name', 'complete', 'database_name', 'disk_metadata', 'duration', 'encrypted', 'gfs_rp_type', 'id', 'instance_id', 'last', 'legalhold', 'output', 'purgeable', 'replicated', 'server_instance_name', 'server_name', 'size', 'start_time', 'status', 'synth_capable', 'synthesized', 'type', 'verified', 'verify_status', 'vmware_template']
xenBackupResponseParameter = ['app_id', 'app_name', 'certified', 'client_config', 'client_id', 'client_name', 'client_os', 'complete', 'database_name', 'disk_metadata', 'duration', 'encrypted', 'gfs_rp_type', 'id', 'instance_id', 'last', 'legalhold', 'output', 'purgeable', 'replicated', 'server_instance_name', 'server_name', 'size', 'start_time', 'status', 'synth_capable', 'synthesized', 'type', 'verified', 'verify_status', 'vmware_template']
instanceBackupResponseParameter = ['host_name', 'instance', 'instance_name', 'job_id']
vmwareInstanceBackupResponseParameter = ['host_name', 'instance', 'instance_name', 'job_id', 'message']
linuxBackupResponseParameter = ['client_name', 'instance_name', 'job_id']
waittime = 1800
xenWaitTime = 3600
waittimeInstance = 900
hypervInstanceName = 'Win_Auto1' #'Win7_Auto' #"Win_CBT"
databaseInstanceName = 'TestDB'
hypervInstanceNameForIR = "win2k8_r2sp1"
hypervInstanceNameForFLR = "CBTNewTest"
jobType="Backup"
daysToLegalHold = "2"
incrementalBackupType = "Incremental"
incrementalFailureMessage = "Please run a Hyper-V Full backup. (Either a restore was recently performed, or no existing Hyper-V Full was found.)"
vmWareInstanceName = "Linux_vmware_Auto"
vmincrementalFailureMessage = "Please run a VMware Full backup. (Either a restore was recently performed, or no existing VMware Full was found.)"
verifyNone = "none"
includeList = "C:/PCBP/Help/"
linuxIncludelist="/root/etc"
emailReport = False
failureReport = False
backupRespParam_Window = ["client_name","job_id"]
differentialBackupType = "Differential"
selectiveBackupType = "Selective" 
xenserverInstanceName = "CentOS 6 (64-bit) (1)_restore_FromArchive"
xenserverVerify = "none"

win37_fileName = "Backup_File_1Master"

excudeSystemState = ["registry"]

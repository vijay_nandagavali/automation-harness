'''
Created on July 22, 2014

@author: Nikhil S
'''

protectedAssetsResponseParameter = ['Hyper-V 2012', 'applications', 'asset_type', 'file-level', 'file_level_instance_id', 'grandclient', 'id', 'ip', 'is_enabled', 'is_encrypted', 'is_hyperv_cluster', 'is_synchable', 'machine_type', 'machine_type_id', 'name', 'os_type', 'os_type_id', 'priority', 'retention', 'supports_agent_push', 'system_id', 'system_name', 'use_ssl', 'version']
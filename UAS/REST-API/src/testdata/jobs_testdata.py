'''
Created on Sep 22, 2014

@author: Amey.k
'''

responseParameter= ['application', 'name', 'type', 'mode', 'status', 'id', 'suspendable', 'resumable', 'client_name', 'instance_name', 'start_date', 'duration', 'system_name','percent_complete','comment','asset_name','system_id']
responseParameterArchive = ['application', 'asset_name', 'client_name', 'comment', 'duration', 'id', 'instance_name', 'mode', 'name','percent_complete', 'resumable', 'start_date', 'status', 'suspendable', 'system_id', 'system_name', 'type']
responseParameterSystemJobs = ['category', 'event_time', 'message', 'notification_id', 'system', 'username']
sid = "1"
#flrResponseParameter = ['id','name','status','comment','percent_complete','start_date','mode','type','application','client_name','instance_name','asset_name','duration','available_targets','share_details','unique','suspendable','resumable','system_id','system_name']
flrResponseParameter = ['app_type', 'application', 'asset_name', 'available_targets', 'client_name', 'comment', 'duration', 'id', 'instance_name', 'iscsi_details', 'mode', 'name', 'percent_complete', 'resumable', 'share_details', 'start_date', 'status', 'suspendable', 'system_id', 'system_name', 'type', 'unique']
irResponseParameter = ['id','name','status','comment','percent_complete','start_date','mode','type','application','client_name','instance_name','asset_name','duration','audit','unique','suspendable','resumable','system_id','system_name']
responseParameterRestore = ['application', 'asset_name', 'audit', 'client_name', 'comment', 'duration', 'id', 'instance_name', 'mode', 'name', 'percent_complete', 'resumable', 'start_date', 'status', 'suspendable', 'system_id', 'system_name', 'type', 'unique']
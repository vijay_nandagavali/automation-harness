'''
Created on Sep 9, 2014

@author: Abhijeet.m
'''

media_name = "DeviceAddedDisk"
cloud_media_name = "Cloud_Storage_Auto"
archive_label = "MyLabel"
cloud_archive_label = "Cloud_Target"
responseParameter = ['name','is_busy','is_mounted','is_initialized','is_infinite','seed_ok','activity','mb_size','mb_used','mb_free','media_serials','media_label','type']
archiveMediaSetResponseParameter = ['date', 'description', 'needs_import']
prereq_UEBNINE_774= "UEBNINE769_PUT_Mount_Unmount_Media";
sid = "1"
catalogByInstanceResponseParameter = ['view','catalog','app_name','client_id','client_name','database_name','instance_id','instance_name','system_id','system_name','archives','arch_success','arch_is_imported','arch_client_id','arch_instance_id','arch_client_name','arch_os_type_id','arch_os_type','arch_app_id','arch_app_name','arch_instance_description','arch_orig_backup_id','arch_type','arch_size','arch_files','arch_compressed','arch_encrypted','arch_deduped','arch_fastseed','arch_vmware_template','arch_id','arch_set_id','arch_elapsed','arch_start_date','arch_archive_start_date','arch_error_string']
archiveFilesResponseParameter = ['data','id','directory','name','type','count','total']
archiveStatusResponseParameter = ['status','archive_set_id','success','is_imported','client_id','instance_id','client_name','os_type_id','os_type','app_id','app_name','instance_description','orig_backup_id','type','size','files','compressed','encrypted','deduped','fastseed','vmware_template','id','set_id','date','elapsed','backup_date','storage']
archiveSearchResponseParameter = ['files','media_serial','filename','size','date','id','set_id'] 
client_name = "HyperVAuto"
file_name = "log.txt"
archive_description = "MyArchive_Auto"
archiveCheckResponseParameter = ['result','min','likely','max','fit']
postArchiveCatalogResponseParamter = ['ids','messages']
archiveSetResponseParameter = ['status', 'available', 'description', 'system_name', 'sid', 'date', 'id']
mediaCandidatesResponseParameter = ['data','name','type']
archiveSetByIdResponseParamter = ['available', 'date', 'description', 'elapsed_time', 'id', 'is_imported', 'media_label', 'media_serials', 'originating_asset', 'profile', 'status']
archiveSetByIdProfileResParam = ['clients', 'description', 'end_date', 'instances', 'localdirs', 'options', 'start_date', 'target', 'types']
archiveSetByIdProfileOptionsResParam = ['append', 'compress', 'dedup', 'email_report', 'encrypt', 'fastseed', 'purge', 'retention_days']
archiveSetByIdProfileInstancesResParam = ['id', 'primary_name', 'secondary_name']
archiveSetByIdProfileInstancesResParamWoSecName = ['id', 'primary_name']
media_mount_response = ['code','0']
archiveJobType = "Archive"
archiveWaitTime = 7200
reserved = "Reserved"
instance = "instance"
fullBacktype = 1031
metadataBacktype = 1038

'''
Created on Mar 9, 2015

@author: Amey.k
'''

backupId = "102"
sid = "1"
directory = "E:"
responseParameter = ['id']
waittime = 1800

hypervHost = "HyperVAuto"
vmName = "test_vm_restore"
vmIp = "192.168.197.199"
audit = True
jobType="Restore"

#data for VMWare Full restore
host = "097ac40c-96f0-0706-0809-0cc47a09f096"
datastore = "NFS-DS1-251"
group = ""
name = "localhost.localdomain",
metadata = True
template = False

successStatus = 3168

#uuid for xen host obtain from DB xen_servers table
xenHost = "fa0b3800-e2da-4228-86bb-06291ebad5b5"
xenName = "AutomationBackup_restorenew"
#obtain from DB xen_vm_disks table
xenStorageRepository = " e35362d1-72e5-4da3-8202-ca05e5e96f2d "
xenMetadata = True

searchString = "_Modified"
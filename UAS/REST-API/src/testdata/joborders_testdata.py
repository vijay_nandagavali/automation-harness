'''
Created on Feb 6, 2015

@author: Amey.k
'''
import datetime


responseParameter = ['id','name','type','app_id','sid','system_name','description','enabled','last_time','next_time','last_status','status','calendar_str','application_name']
specificresponseParameter = ["id", "name","type","app_id","calendar","backup_type","start_date","start_time", "schedule_run","run_on","email_report","failure_report","storage","verify","instances"]
name = "TestSchedule"
replicationJobName = "Backup Copy Job"
replicationType = "replication"
type = 'Backup'
incrName = "TestScheduleIncr"
backup_type = 'Full'
storage = 'Internal'
email_addresses = ''
run_on = ['1','2','3','4','5','6','0']
hypervInstanceName = 'Win7_Auto' #"Win_CBT"
jobOrderNameVmWareFull = "VMwareFull"
archiveName = "TestArchiveSchedule"
archiveType = "Archive"
cloudStorage = "Cloud_Storage_Auto"
archiveError = "Archive jobs with clients must specify at least one backup type"
archiveStorage = 'DeviceAddedDisk'
jobOrderNameVmWareIncre = "VMwareIncre"
archive_types = 'Full'

'''
Created on June 8, 2015

@author: Nikhil S.
'''

UEB_MASTER_INI_PATH = "/usr/bp/bpinit/master.ini"
DEDUP_PARAMETER = "ReportMailTo"

sid = 1
sid_invalid = 111
sid_string = "xyz"

max_Concurrent = "5"
max_Concurrent_Update = "9"

report_Mail_to = "test@a4.com"
report_Mail_to_Update = "update@a4.com"

report_Time = "9:09"
report_Time_Update = "10:05"
report_Time_Invalid = "99:99"

MaxConcurrentParameterName = "MaxConcurrent"
ReportMailToParameterName = "ReportMailTo"
ReportTimeParameterName = "ReportTime"

responseParmeterReplicationConfig = ['max_concurrent','queue_scheme','report_mail_to','report_time','strategy']
responseRepConfig_InvalidSid = ['5', '9:09', 'test@a4.com']
responseRepConfig_Invalid = ['False', 'False', 'False']

tar_Name = "VMware_UEB_TAR" 
tar_IP = "192.168.197.61"
tar_Type_Appliance = "appliance"
tar_insecure = "1"

responseParameterPendingRepSrc = ['asset_tag', 'created', 'host', 'request_id', 'status']


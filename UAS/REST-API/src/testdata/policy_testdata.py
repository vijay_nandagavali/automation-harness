
'''
Created on Jun 3, 2015

@author: savitha.p
'''

policyName = "Days"
policyNameYearsForever = 'years_forever_policy'
policyNameForever = "Forever_Policy"
policyNameMonths = "Policy_Months"
policyNameMonthsForever = "Forever_Month_Policy"
PolicyNameMonthsDelete = "Month_Policy"
PolicyNameYearsDelete = "Year_Policy"
policyNameDaysForever = "days_forever_policy" 
policyNameWeeksForver = "forever_week_policy"
policyNameForModify = "Modify_Policy"
policyNameGeneric = "New_Policy"
updatepolicyDays = 15
updatepolicyWeeks = 15
updatepolicyMonths = 15
updatepolicyYears = 15

#test data for MQ1484
policyNameForupdate = "????????"
modifyPolicyDays = 7
modifyPolicyWeeks = 2
modifyPolicyMonths = 0
modifyPolicyYears = 1

sid = 1
responseParmeterRetentionStrategy = ['strategy']

#test data for MQ1492_PutYearsPolicyDecreaseYears
decreaseYearPolicyDays = 0
decreaseYearPolicyWeeks = 0
decreaseYearPolicyMonths = 0
decreaseYearPolicyYears = 2


#Test Data for MQ-1491 and MQ-1494
decreasepolicyDays = 35
increasepolicyDays = 45
policyWeeks = 0
policyMonths =  0
policyYears = 0

#test data for MQ1500_PutYearsPolicyIncreaseYears
increaseYearPolicyDays = 0
increaseYearPolicyWeeks = 0
increaseYearPolicyMonths = 0
increaseYearPolicyYears = 7

responseParmeterRetentionPolicies = ['days', 'id', 'is_default', 'months', 'name', 'weeks', 'years']

#test data for MQ1554_PutMonthsPolicyIncreaseMonths
increaseMonthPolicyDays = 0
increaseMonthPolicyWeeks = 0
increaseMonthPolicyMonths = 5
increaseMonthPolicyYears = 0


#Test data for MQ1555_PutDecreaseExistingPolicyBasedOnMonth
decreaseMonthPolicyDays = 0
decreaseMonthPolicyWeeks = 0
decreaseMonthPolicyMonths = 2
decreaseMonthPolicyYears = 0

policyNameWeeks = "Policy_Weeks"

#Test data for MQ1568_PutDecreaseExistingPolicyBasedonWeeks
decreaseWeeksPolicyDays = 0
decreaseWeeksPolicyWeeks = 2
decreaseWeeksPolicyMonths = 0
decreaseWeeksPolicyYears = 0

#Test data for MQ1574_PutModifyExistingPolicyDefaultTrue
ModifyPolicyIsDefaultDays = 3
ModifyPolicyIsDefaultWeeks = 1
ModifyPolicyIsDefaultMonths = 2
ModifyPolicyIsDefaultYears = 7
ModifyPolicyIsDefaultDefault = True

#Test data for MQ1567_PutIncreaseExistingPolicyBasedonWeeks
increaseWeeksPolicyDays = 0
increaseWeeksPolicyWeeks = 15
increaseWeeksPolicyMonths = 0
increaseWeeksPolicyYears = 0

#Test dat for MQ1575_PostCreatePolicyOnValidInformation

PostPolicyValidInfoDays = 7
PostPolicyValidInfoWeeks = 1
PostPolicyValidInfoMonths = 0
PostPolicyValidInfoYears = 0

policyNameUnicode = "Policy_!#@$%^"

#Test data for MQ1579
expectedErrorMessageMQ1579 = "Policy ID must be specified."

#Test data for MQ1578
policyNameSid = "Policy_Sid"

#test data for MQ1477

expectedErrorMessageForMQ1477 = "Retention Policy days is set to hold forever but weeks or months or years is set smaller retention time. Need to either set days,weeks,months,years to a positive value or set weeks, months, years to 0 to hold all daily backups forever."



#Test data for MQ1598_PostNewPolicyBasedOndefaultInfo

PostPolicyDefaultInfoDays = 0
PostPolicyDefaultInfoWeeks = 4
PostPolicyDefaultInfoMonths = 0
PostPolicyDefaultInfoYears = 0
PostPolicyDefaultIsDafault = True


#Test data for MQ1600 & MQ1686
responseParameters = ['days', 'id', 'is_default', 'months', 'name', 'weeks', 'years']


#test data for MQ-1580
RetentionInfoParameterList = ['app_id', 'app_name', 'client_id', 'days', 'instance_id', 'instance_name', 'months', 'policy_id', 'policy_name', 'weeks', 'years']

#Test data for MQ1658
expectedErrorMessageMQ1658 = "Asset ID must be specified."

#Test Data for retention strategy
retentionNameGFS = "GFS"
retentionNameMinMax = "MinMax"

#Test data for MQ1672_PutSetDefaultPolicyFalse
ModifyPolicyIsDefaultFalseDays = 3
ModifyPolicyIsDefaultFalseWeeks = 1
ModifyPolicyIsDefaultFalseMonths = 2
ModifyPolicyIsDefaultFalseYears = 7
ModifyPolicyIsDefaultFalseDefault = False


#test data for MQ1702
expectedErrorMessageForMQ1702 = "missing required fields in input array: instance_id"

#test data for MQ1703
expectedErrorMessageForMQ1703 = "missing required fields in input array: instance_id"

#test data for MQ1704
expectedErrorMessageForMQ1704 = "missing required fields in input array: policy_id"


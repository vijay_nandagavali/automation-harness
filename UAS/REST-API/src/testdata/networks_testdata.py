'''
Created on Oct 21, 2014

@author: Amey.k
'''

responseParameter = ['id', 'name', 'dns', 'dns1', 'dns2', 'search', 'ip', 'netmask', 'gateway', 'mac', 'boot', 'link', 'speed', 'duplex']
networkId = "1"
networkBridgeResponseParameter = ['bridge','nics']
networkBridgeNicResponseParameter = ['bridge']
virtualBridgeResponseParameter = ['network', 'netmask', 'gateway', 'dhcprange', 'message']
systemId = "1"
nic = "eth1"

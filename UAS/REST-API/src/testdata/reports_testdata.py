'''
Created on Nov 27, 2014

@author: Amey.k
'''

responseParameter = ['count', 'end_date', 'error_count', 'messages', 'start', 'start_date', 'status', 'system_name', 'total', 'version_from', 'version_to']
reportsResponseParameter = []
sid = "1"

#backupreportsResponseParameter = ['count','total','start','app_name','certify_status','client_name','complete','database_name','elapsed_time' 
#                                  ,'encrypted','end_date','files','id','instance_name','replication_status','size','start_date','status',
#                                  'system_name','type','verify_status']


backupreportsResponseParameter = ['app_name', 'certify_status', 'client_name', 'complete', 'count', 'database_name', 'elapsed_time', 'encrypted', 'end_date', 
                                  'id', 'instance_name', 'replication_status', 'size', 'start', 'start_date', 'status', 'system_name', 
                                  'total', 'type', 'verify_status', 'files']

archivereportsResponseParameter = ['archive_set_id', 'archives', 'available', 'count', 'date', 'description', 'is_imported', 'start', 'status', 'system_name', 'target', 'total'] 
# ['count','total','start', 'archives', 'archive_set_id','available','date','description','is_imported','status' 
#                                   ,'system_name','target','app_name','archive_id','archive_time','backup_time','client_name','compressed','deduped',
#                                   'elapsed_time','encrypted','fastseed','files','instance_description','is_imported','orig_backup_id','os_type','size',
#                                   'success','type']

archivereportsResponseParameter1 = ['count','total','start', 'archives', 'archive_set_id','available','date','description','is_imported','status','system_name','target']

restorereportsResponseParameter = ['count','total','start', 'app_name','certify_status','client_name','complete','database_name','elapsed_time','encrypted','end_date','files','id','instance_name','size','start_date','status','system_name','type']

storagereportsResponseParameter = ['alerts', 'count', 'created', 'daily_growth_rate', 'data', 'day', 'dedup', 'id', 'id', 'is_default', 'mb_free', 'mb_free', 'mb_size', 'mb_size', 'mb_used', 'mb_used', 'message', 'name', 'online', 'protocol', 'resolved', 'severity', 'size_history', 'sname', 'source_id', 'source_name', 'start', 'status', 'system_id', 'system_id', 'system_name', 'total', 'type', 'updated', 'usage']

datareductionreportsResponseParameter1 = ['count','total','start','date','dedup','data_reduction','system_name']
datareductionreportsResponseParameter = ['count','total','start']

backupfailureResponseParameter1 = ['count', 'start', 'total']
backupfailureResponseParameter = ['app_name', 'certify_status', 'client_name', 'complete', 'count', 'database_name', 'elapsed_time', 'encrypted', 'end_date', 'id', 'instance_name', 'replication_status', 'size', 'start', 'start_date', 'status', 'system_name', 'total', 'type', 'verify_status']

backupLegallyHeldResponseParameter = ['app_name', 'client_name', 'count', 'database_name', 'hold_days', 'hold_end_date', 'id', 'instance_name', 'size', 'start', 'start_date', 'system_name', 'total', 'type']

replicationResponseParameter = ['app_name', 'certify_status', 'client_name', 'complete', 'count', 'database_name', 'elapsed_time', 'encrypted', 'end_date', 'files', 'id', 'instance_name', 'replicated_size', 'replication_elapsed_time', 'replication_end_date', 'replication_start_date', 'replication_status', 'size', 'start', 'start_date', 'status', 'system_name', 'total', 'type', 'verify_status']
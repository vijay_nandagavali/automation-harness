'''
Created on Mar 2, 2015

@author: Amey.k
'''

responseParameter = ['alert_count','sync_last','sync_progress','sync_running','sync_status','message', 'max_severity']
sid = "1"
waittime = 180
inventoryResponseParameter = ['id', 'name', 'nodes', 'type', 'type_family']
hyperVInstanceName = "HyperVAuto"
vmwareInstanceName = "vCenter-RRC"

app_id = 1
resource_pool_key = 'ha-root-pool'

name_xenserver = "WinBackup"
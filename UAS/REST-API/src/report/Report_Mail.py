import os
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
import shutil
import smtplib
import time, datetime
from util import Get_Suite_Name

from config import apiconfig
from report import Report


from email.MIMEBase import MIMEBase
from email import Encoders
def send_mail(executionId):
    assert type(apiconfig.MAIL_RECEIVER_LIST) == list
    
    #createZip(apiconfig.UAS_DIR_PATH + "/Reports"+"/UEB_AutomationExecutionReport_Email-Execution" + "_" + str(datetime.date.today()))
    #zipfilePath = apiconfig.UAS_DIR_PATH + "/Reports.zip"
    file_to_attach = (apiconfig.UAS_DIR_PATH + "/Reports"+"/UEB_AutomationExecutionReport_Detailed-" + str(executionId) + ".html")
    msg = MIMEMultipart()
    user = "a4@afourtech.com"
    password = "@four123"
    msg['From'] = user
    msg['To'] = COMMASPACE.join(apiconfig.MAIL_RECEIVER_LIST)
    msg['Date'] = formatdate(localtime=True)
    
#     if(flag == 'gfs'):
#         msg['Subject'] = "[Unitrends] GFS REST API Automation Report " + time.strftime("%c")
#     elif(flag == 'uf'):
#         msg['Subject'] = "[Unitrends] UF Replication REST API Automation Report " + time.strftime("%c")
#     else:
#         
    Suite_Name = str(Get_Suite_Name.Get_Suite_Name(executionId))
    msg['Subject'] = "Unitrends | " + Suite_Name + " | Automation Report " + time.strftime("%c")
    
    emailBody = open(Report.REPORT_FILE_PATH_EMAIL.replace("&EId", executionId), 'r').read()
    
    '''msg.attach( MIMEText("Hi All,\n\nPlease find the attached automation report\n\n" + emailBody +
    "Thanks and Regards,\nAutomation Team") )'''
    msg.attach(MIMEText(emailBody, 'html'))
    
    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(file_to_attach, "rb").read())
    Encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="%s"' % ("UEB_AutomationExecutionReport_Detailed-Execution"+ ".html"))
    msg.attach(part)
    
    server = "smtp.gmail.com:587"
    smtp = smtplib.SMTP(server)
    smtp.starttls()
    # smtp.login(user, password)
    toaddress = apiconfig.MAIL_RECEIVER_LIST
    notifyUser(smtp, user, password, user, toaddress, msg.as_string())
#     smtp.sendmail(user, toaddress, msg.as_string())
#     smtp.close()
    print "Mail Sent..."

def notifyUser(smtp, smtp_user, smtp_password, from_email, to_email, msg):
    smtp.login(smtp_user, smtp_password)
    smtp.sendmail(from_email, to_email, msg)
    smtp.quit()
    
def createZip(filePath):
    shutil.make_archive(filePath, "zip", filePath)
    print "Zip created" 
    
    
    
    
    
    
    

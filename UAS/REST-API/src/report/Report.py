'''
Created on Sep 16, 2014

@author: pravinpr
'''
from os import path
from util import dateTimeHandler, updateJira

from Cheetah.Template import Template

from config import apiconfig, Constants
from model.ExecutionSummary import ExecutionSummary
from config.Constants import ExecutionStatus
import datetime
now = datetime.datetime.now()
ntime = str(now.hour)

TEMPLATE_FILE_PATH_DETAILED = apiconfig.UAS_DIR_PATH + "/Template/UEB_AutomationExecutionReport_Detailed.tmpl"
TEMPLATE_FILE_PATH = apiconfig.UAS_DIR_PATH + "/Template/UEB_AutomationExecutionReport.tmpl"
TEMPLATE_FILE_PATH_EMAIL = apiconfig.UAS_DIR_PATH + "/Template/UEB_AutomationExecutionReport_Email.tmpl"

REPORT_FILE_PATH_DETAILED = apiconfig.UAS_DIR_PATH + "/Reports/UEB_AutomationExecutionReport_Detailed-&EId.html"
REPORT_FILE_PATH = apiconfig.UAS_DIR_PATH + "/Reports/UEB_AutomationExecutionReport-&EId.html"
REPORT_FILE_PATH_EMAIL = apiconfig.UAS_DIR_PATH + "/Reports/UEB_AutomationExecutionReport_Email-&EId.html"

def generateReport(templateFilePath, execSumm, outputFilePath):
    result = Template(
        file=templateFilePath, searchList=[{'execSumm': execSumm}])
    #print result
    open(outputFilePath, 'w').write(str(result))

def writeExecutionSummary(testCaseResult, executionId, uebVersion, startTime, harnessFlag):
    fileName = apiconfig.UAS_DIR_PATH + "/Reports/" + executionId + ".json"
    if path.exists(fileName):
        jsonFile = open(fileName)
        execSumm = ExecutionSummary.fromJson(jsonFile.read())
        execSumm.setStartTime(execSumm.startTime)
        jsonFile.close()
    else:
        execSumm = ExecutionSummary(executionId, uebVersion)
        execSumm.setStartTime(startTime)
        
    if (testCaseResult.getExecutionStatus() == ExecutionStatus.PASS):
        execSumm.setPassedTC(execSumm.getPassedTC() + 1)
    else:
        execSumm.setFailedTC(execSumm.getFailedTC() + 1)
        
    if (execSumm.getExecutionResult() != ExecutionStatus.FAIL):
        execSumm.setExecutionResult(testCaseResult.getExecutionStatus())
    
    execSumm.setTotalTC(execSumm.getTotalTC() + 1)
    
    execSumm.setEndTime(dateTimeHandler.getCurrentDateTime(Constants.DATE_TIME_FORMAT))
    
    duration = dateTimeHandler.convertStringToDateTimeObj(execSumm.getEndTime(), Constants.DATE_TIME_FORMAT) - dateTimeHandler.convertStringToDateTimeObj(execSumm.getStartTime(), Constants.DATE_TIME_FORMAT)
    execSumm.setDuration(dateTimeHandler.getDateTimeInHMSFormat(str(duration)))
    #print duration
    
    execSumm.getTestCaseResultList().append(testCaseResult)
    jsonStr = execSumm.toJson()
    jsonFile = open(fileName, "w")
    jsonFile.write(jsonStr)
    jsonFile.close()
    
    generateReport(TEMPLATE_FILE_PATH_EMAIL, execSumm,
                   REPORT_FILE_PATH_EMAIL.replace("&EId", executionId)) 
    
    if (harnessFlag == False):
        generateReport(TEMPLATE_FILE_PATH_DETAILED, execSumm,
                       REPORT_FILE_PATH_DETAILED.replace("&EId", executionId))
        
        generateReport(TEMPLATE_FILE_PATH, execSumm,
                       REPORT_FILE_PATH.replace("&EId", executionId))
        

        


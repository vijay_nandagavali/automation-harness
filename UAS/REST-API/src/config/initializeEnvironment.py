from config import apiconfig, Constants


def initializeUEBEnvironment(envObj):
    infraDetails = envObj.getJsonDetails("$.uebs[?(@.name=='Main')];index=1")
    infraDetailsSource = envObj.getJsonDetails("$.uebs[?(@.name=='Primary')];index=1")
    infraDetailsTarget = envObj.getJsonDetails("$.uebs[?(@.name=='Secondary')];index=1")
    
    apiconfig.environmentIp = envObj.getDictValue(infraDetails, "url")
    apiconfig.environmentIpSource = envObj.getDictValue(infraDetailsSource, "url")
    apiconfig.environmentIpTarget = envObj.getDictValue(infraDetailsTarget, "url")
    
    
    apiconfig.dbuser = envObj.getDictValue(infraDetails, "dbUser")
    apiconfig.dbpassword = envObj.getDictValue(infraDetails, "dbPassword")
    apiconfig.dbname = envObj.getDictValue(infraDetails, "dbName")
    apiconfig.machineuser = envObj.getDictValue(infraDetails, "username")
    apiconfig.machinepassword = envObj.getDictValue(infraDetails, "password")
    apiconfig.MAIL_RECEIVER_LIST = envObj.getJsonDetails("$.emailRecipients")[0]
    Constants.ENVIRONMENT_URL = "https://" + apiconfig.environmentIp
    Constants.ENVIRONMENT_URL_SOURCE = "https://" + apiconfig.environmentIpSource
    Constants.ENVIRONMENT_URL_TARGET = "https://" + apiconfig.environmentIpTarget
    
def initializeMachineEnvironment(envObj,flavour,index):
    machineInfra = envObj.getJsonDetails("$.machines[?(@.flavour=="+"'"+flavour+"'"+")];index=1")
    return machineInfra
    


'''
Created on Sep 9, 2014

@author: Abhijeet.m
'''

environmentIp = "192.168.199.210"
environmentIpSource = "192.168.197.80"
environmentIpTarget = "192.168.199.81"

#environmentIp = "192.168.199.248" #For GFS
dbuser = "postgres"
dbpassword = "passwd"
dbname = "bpdb"
machineuser = 'root'
machinepassword = 'unitrends1'
UAS_DIR_PATH = "/root/UAS"

MAIL_RECEIVER_LIST = ['divya.jagannathan@afourtech.com', 'deepanshu.sagar@afourtech.com', 'gayatri.londhe@afourtech.com']
# MAIL_RECEIVER_LIST = ['pune-uniqa@afourtech.com', 'mahesh.kulkarni@afourtech.com']

rabbitmqServer="192.168.197.141"
rabbitmqExchangeName = "harnessexchange"
serverbindingkey = "harnesskey"

driverIP="192.168.197.15"

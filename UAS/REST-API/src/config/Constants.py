'''
Created on Sep 11, 2014

@author: AFour Technologies
'''
from config import apiconfig

#UEB_VERSION = "9.0.0"
DATE_TIME_FORMAT = '%m/%d/%Y %H:%M:%S'
ENVIRONMENT_URL = "https://" + apiconfig.environmentIp
ENVIRONMENT_URL_SOURCE = "https://" + apiconfig.environmentIpSource
ENVIRONMENT_URL_TARGET = "https://" + apiconfig.environmentIpTarget
# ENVIRONMENT_JSON_PATH = ''
config_path = "/Config/"

class ExecutionStatus(object):
    PASS = "Pass"
    FAIL = "Fail"
    UNKNOWN = "Unknown"
    
def getEnvironmentJsonPath(exeId):
    path = str(apiconfig.UAS_DIR_PATH) + str(config_path) + str(exeId) + "/Environment.json"
    return path

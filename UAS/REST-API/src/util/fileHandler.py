'''
Created on Jun 26, 2015

@author: savitha.p
'''
from config import apiconfig

def createProductLogFile(exeId):
    fo = open(apiconfig.UAS_DIR_PATH + "/REST-API/Logs/RestAPI_Product_" + str(exeId) + ".log", "a+")
    return fo.name

def writeFile(filename, content):
    fo = open(filename, "a+")
    fo.writelines(content+"\n")
    fo.close()
''' 
Created on Sep 16, 2014

@author: Abhijeet.m
'''


from config import Constants
from model.TestCaseResult import TestCaseResult
from util import dateTimeHandler, uebVersionHandler
from report import Report

testCaseObject = None
tsStartTime = ''
executionStartTime = ''


def initializeReport(fileName, tcDesc):
    global executionStartTime, testCaseObject
    tcStartTime = dateTimeHandler.getCurrentDateTime(Constants.DATE_TIME_FORMAT)
    testCaseObject = TestCaseResult(fileName, tcDesc)
    testCaseObject.setStartTime(tcStartTime)    
    executionStartTime = tcStartTime
    

def addTestStepResult(stepDesc, stepStatus):  
    global testCaseObject  
    testCaseObject.addTestCaseStepResult(stepDesc, stepStatus, "")
    
def tcExecutionStatus(tcExeStatus):
    global testCaseObject  
    testCaseObject.setExecutionStatus(tcExeStatus)

def generateReportSummary(appliance_ip,executionId):
    global executionStartTime, testCaseObject
    testCaseObject.setEndTime(dateTimeHandler.getCurrentDateTime(Constants.DATE_TIME_FORMAT));
    duration = dateTimeHandler.convertStringToDateTimeObj(testCaseObject.getEndTime(), Constants.DATE_TIME_FORMAT) - dateTimeHandler.convertStringToDateTimeObj(testCaseObject.getStartTime(), Constants.DATE_TIME_FORMAT)
    testCaseObject.setDuration(dateTimeHandler.getDateTimeInHMSFormat(str(duration)))
    uebVersion = uebVersionHandler.getUEBVersion(appliance_ip)
    Report.writeExecutionSummary(testCaseObject, executionId, uebVersion, executionStartTime, False)

def getTestcaseName(clientType,clientFlavour,backupType,dedupType,sisCompression,
                                                        compressionType,encryptionStatus,recoveryType):
    testCaseName = backupType+"-"+clientType+"-"+clientFlavour
   
    if dedupType:
        testCaseName = testCaseName+"-"+dedupType
        
        if dedupType == "inlinededup" or dedupType == "filededup":       
            '''
            if metadataGeneration:
                testCaseName = testCaseName +"-DedupNT"
            else:    
                testCaseName = testCaseName +"-DedupDb "
            '''
            if sisCompression == "on":
                testCaseName = testCaseName +"-SISenabled "
            elif sisCompression == "off":    
                testCaseName = testCaseName +"-SISdisabled "
                
        if dedupType == "nodedup" or dedupType == "filededup":
            testCaseName = testCaseName +"-"+compressionType
                   
    else:
        testCaseName = testCaseName+"-NoDedup"
        
    if encryptionStatus == "on":
        testCaseName = testCaseName +"-EncryptionEnabled"
    elif  encryptionStatus == "off":
        testCaseName = testCaseName +"-EncryptionDisabled"
        
    """     
    if clientType == "windows" or clientType == "linux" : 
        testCaseName = testCaseName +"-RecoveryType-SpecificFileRestore"
    else:
        testCaseName = testCaseName +"-Recovery Type-FileLevelRecovery"
    """
    return testCaseName   
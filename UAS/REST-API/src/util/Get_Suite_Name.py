'''
Created on Feb 23, 2017

@author: deepanshu.s
'''
from model import Environment
from config import Constants
import jsonHandler
import json, pprint

def Get_Suite_Name(exeId):
    envJsonPath = Constants.getEnvironmentJsonPath(exeId)
    with open(envJsonPath) as data_file:    
        data = json.load(data_file)
        return data["name"][:-12]

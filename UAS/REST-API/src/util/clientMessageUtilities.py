'''
Created on May 13, 2015

@author: Amey.k
'''
from util import messagesender, logHandler
from config import apiconfig
from testdata import clients_testdata, backups_testdata
import paramiko
from time import sleep
import logging


def addCustomSizedfiles_Window(clientIP,dirPath,fileCount,folderType,fileSize,sizeType): 
    print ("connecting to client : " + str(clientIP))
    print clientIP
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print "\n connecting \n"
    ssh.connect(clientIP,username = 'Administrator', password = 'Unitrends1')
    print "\n connected \n"
    #cmd = "powershell -InputFormat none -OutputFormat TEXT new-item "+str(dirPath)+" -itemtype directory"
    fileSizeBytes = 0
    if str(sizeType) == "K":
        fileSizeBytes = 1000 * fileSize
    elif str(sizeType) == "M":
        fileSizeBytes = 1000000 * fileSize
    elif str(sizeType) == "G": 
        fileSizeBytes = 1000000000 * fileSize
        
    for count in range(1,fileCount+1):
	sleep(10)
        cmd = "powershell -InputFormat none -OutputFormat TEXT fsutil file createnew "+ str(dirPath)+str(folderType)+"_"+str(count) + ".txt " + str(fileSizeBytes)
	sleep(5)
        stdin, stdout, stderr = ssh.exec_command(cmd)
	sleep(3)        
    print "Files Added - "+str(fileCount)


def SetupLinux(clientIP):
    #dd if=/dev/urandom of=mastertest.txt,mastertest1.txt bs=1M count=1
    try:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(clientIP, username='root', password='unitrends1')
        logHandler.logging.info("Client connection done")
        print "start"
        logHandler.logging.info("Setup Linux Started")
        print "Setup Linux Started"
        buildCommand = " rm -rf /root/Backup_folder /root/Restore_folder /root/Restore_folder_1 /root/Restore_folder_2"
        logHandler.logging.info("Removing previous folders" +buildCommand)
        print buildCommand
        stdin, stdout, stderr = client.exec_command(buildCommand)
        logHandler.logging.info("Command executed")
        sleep(2)
    
        buildCommand = " mkdir /root/Backup_folder /root/Restore_folder"
        logHandler.logging.info("creating folders" +buildCommand)
        print buildCommand
        stdin, stdout, stderr = client.exec_command(buildCommand)
        logHandler.logging.info("Command executed")
        sleep(2)
    
        buildCommand = " ls /root"
        logHandler.logging.info("checking folders" +buildCommand)
        print buildCommand
        stdin, stdout, stderr = client.exec_command(buildCommand)
        folders = stdout.read()
        logHandler.logging.info(str(folders))
        if  "Backup_folder" in folders and "Restore_folder"  in folders:
            print "present"
        else:
            SetupLinux(clientIP)
        logHandler.logging.info("checking executed")
        sleep(2)
    
        client.close()
        logHandler.logging.info("Client Connection close")
        print "done"
    except Exception as e:
        logHandler.logging.error("Exception: " + str(e))
        
def addCustomSizedfilesOnLinux(clientIP, dirPath,fileCount, fileSize,sizeType):
    #dd if=/dev/urandom of=mastertest.txt,mastertest1.txt bs=1M count=1
    try:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(clientIP, username='root', password='unitrends1')
        logHandler.logging.info("Client connection done")
        print "start"
        for count in range(1,fileCount+1):
            buildCommand = "dd if=/dev/urandom of="+str(dirPath) + "/" + "InctYPE" +"_"+str(count) + ".txt bs=" +str(fileSize) + str(sizeType) + " count=1"
            logHandler.logging.info("Command to change files--" +buildCommand)
            print buildCommand
            stdin, stdout, stderr = client.exec_command(buildCommand)
            logHandler.logging.info("Command executed")
            sleep(2)

        client.close()
        logHandler.logging.info("Client Connection close")
        print "done"
    except Exception as e:
        logHandler.logging.error("Exception: " + str(e))
    
def verifyRestoreFolderAgainstBackupFolderOnLinux(backupPath, restorePath,clientIP):   
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(clientIP, username='root', password='unitrends1')
    #  diff -q /root/Ayyaz/dir1 /root/Ayyaz/dir2
    buildCommand = "diff -q " + str(backupPath) + " " + str(restorePath)
    logHandler.logging.info(buildCommand)
    print buildCommand
    stdin, stdout, stderr = client.exec_command(buildCommand)
    for line in stdout.read().splitlines():
        print line
        if("Only" in line):
            print "Backup and Restore Folder content mismatch"
            return False
    else:
        print "Backup and Restore Folder content matched"
        return True
        
    client.close()
    print "done"
    
def verifyRestoreFileCount_Windows(dirName, clientIP):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print "\n connecting \n"
    logHandler.logging.info("\n connecting \n")
    ssh.connect(clientIP, username='Administrator', password='Unitrends1')
    print "\n connected \n"
    logHandler.logging.info("\n connected \n")
    cmd = "powershell -InputFormat none -OutputFormat TEXT write-host @(Get-ChildItem " + str(dirName) + " -recurse).count;"
    stdin, stdout, stderr = ssh.exec_command(cmd)
    #print "Number of Items are: " + stdout.read()
    return stdout.read()
    logHandler.logging.info("Number of Items are: " + stdout.read())
    print "Count of files in folder done"


    
def verifyChecksum_Windows(dirName, clientIP):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print "\n connecting \n"
    logHandler.logging.info("\n connecting \n")
    ssh.connect(clientIP, username='Administrator', password='Unitrends1')
    print "\n connected \n"
    logHandler.logging.info("\n connected \n")
    fun = """ -Recurse | ?{!$_.psiscontainer} | %{[Byte[]]$contents += [System.IO.File]::ReadAllBytes($_.fullname)}; [string]::Join('',$(([System.Security.Cryptography.SHA1]::Create()).ComputeHash($contents) | %{'{0:x2}' -f $_}))"""

    cmd = "powershell -InputFormat none -OutputFormat TEXT dir " + str(dirName) + str(fun)
    stdin, stdout, stderr = ssh.exec_command(cmd)
    print(str(dirName) + ": ")
    print(stdout.read())
    logHandler.logging.info("checksum output: "+stdout.read())
    return stdout.read()

def deleteOnLinux(filepath,clientIP):   
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(clientIP, username='root', password='unitrends1')
    #  diff -q /root/Ayyaz/dir1 /root/Ayyaz/dir2
    buildCommand = "rm -f " + str(filepath)
    logHandler.logging.info(buildCommand)
    print buildCommand
    stdin, stdout, stderr = client.exec_command(buildCommand)
            
    client.close()
    print "done"


def verifyWindowsRestore(sourceDir, destDir, clientIP):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print "\n connecting \n"
    ssh.connect(clientIP, username='Administrator', password='Unitrends1')
    print "\n connected \n"
    cmd = "powershell -InputFormat none -OutputFormat TEXT robocopy " + "'" + str(sourceDir) + "'" + " '" + str(destDir)+ "'" + " /e /l /it /njh /njs "
    stdin, stdout, stderr = ssh.exec_command(cmd)
    array = []
    array2 = []
    array3 = []
    array.append(stdout.readlines())
    for i in array[0]:
        array2.append(str(i).split())
    print array2
    for i in range(len(array2))[2:]:
        array3.append(str(array2[i][0]))

    if "*EXTRA" in array3 or "New Dir" in array3 or "New File" in array3 or "Newer" in array3 or "New" in array3:
        print "mismatch"
    else:
        print "match"
  
        
def createFoldersWindows(clientIP,backupfolder, restorefolder):  
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print "connecting"
    logHandler.logging.info("\n connecting \n") 
    ssh.connect(clientIP, username='Administrator', password='Unitrends1')
    logHandler.logging.info("\n connected \n")
    print "connected"
    cmd = "mkdir " + backupfolder + " " + restorefolder
    sleep(10)
    stdin, stdout, stderr = ssh.exec_command(cmd)
    logHandler.logging.info("Folders created") 
    
def deleteFoldersWindows(ClientIp,FolderPath):
     
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print "\n connecting \n"
    #logHandler.logging.info("\n connecting \n")
    ssh.connect(ClientIp, username='Administrator', password='Unitrends1')
    print "\n connected \n"
    sleep(5)
    cmd = "RD /S /Q" + " " + FolderPath
    sleep(5)
    stdin, stdout, stderr = ssh.exec_command(cmd)
    #print (stderr.read())
    stderr = stderr.read()
    nofolderText = "The system cannot find the file specified."
    if stderr.rstrip() == nofolderText:
        logHandler.logging.info("no folder exist") 
    else:
        logHandler.logging.info("folder deleted succesfully") 









'''    


def add8MBfiles(dirName,fileCount,folderType,clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"Add8MbFiles", "FolderType":"'+str(folderType)+'","DirName":"' +str(dirName)+'"'+',"FileCount":"' + str(fileCount) + '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    print routing_key
    testresult=messageSenderObj.sendMessage(routing_key, message)
    print testresult
    print "Completed Adding 8 MB files...."  
    return testresult

def addDedupFolder(dirName,clientIP):
    message = '{"action":"executeClientUtilities", "payload":{"UtilityType":"AddDedupFolder","DirName":"'+str(dirName)+ '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    print routing_key
    testresult=messageSenderObj.sendMessage(routing_key, message)
    print testresult
    print "Completed Adding Dedup Folder...."  
    return testresult

def deleteFolder(dirName,clientIP):
    message = '{"action":"executeClientUtilities", "payload":{"UtilityType":"DeleteFolder","DirName":"'+str(dirName)+ '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    print routing_key
    testresult=messageSenderObj.sendMessage(routing_key, message)
    print testresult
    print "Completed Deleting Folder...."  
    return testresult

def addCustomSizedfiles(dirName,fileCount,folderType,fileSize,sizeType,clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"AddCustomSizedFiles", "FolderType":"'+str(folderType)+'","DirName":"' +str(dirName)+'"'+',"FileCount":"' +str(fileCount)+ \
                '","FileSize":"' +str(fileSize)+'"'+',"SizeType":"' +str(sizeType)+ '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    print routing_key
    testresult=messageSenderObj.sendMessage(routing_key, message)
    print testresult
    print "Completed Adding Custom sized files...."  
    return testresult

def modifyFile(dirName,fileName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"ModifyFile","DirName":"' +str(dirName)+'", "FileName":"' + str(fileName) + '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Completed file modification....")  
    return testresult

def searchFileForText(dirName,fileName, searchString, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"ReadTextFile","DirName":"' +str(dirName)+'", "FileName":"' + str(fileName) + '", "SearchString":"' + str(searchString) + '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Completed file search with the given string....")  
    return testresult

def deleteFile(dirName,fileName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"DeleteFile","DirName":"' +str(dirName)+'", "FileName":"' + str(fileName) + '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Completed file search with the given string....")  
    return testresult

def compareFileTimestamp(dirName,fileName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"CompareFileTimeStamp","DirName":"' +str(dirName)+'", "FileName":"' + str(fileName) + '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Timestamp comparison of the given file completed....")  
    return testresult

def createDatabase(serverName, userId, passwd, testDBName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"CreateDB","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Create Database complete...")  
    return testresult

def deleteDatabase(serverName, userId, passwd, testDBName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"DeleteDB","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Delete Database complete...")  
    return testresult

def createTableWithValues(serverName, userId, passwd, testDBName, tableCount, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"CreateTableWithValues","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'", "tableCount":"' + str(tableCount) + '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Create Table with values complete...")  
    return testresult

def verifyRestore(backupPath, restorePath,clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"VerifyRestoreBackup", "backupPath":"' + str(backupPath) + '", "restorePath":"' + str(restorePath) + '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Verify restore complete...")  
    return testresult

def verifyRestoreFileCount(restorePath,clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"CheckRestoreFileCount", "DirectoryPath":"' + str(restorePath) + '"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Verify restore complete...")  
    return testresult

def verifyTableexistsinDatabase(serverName,userId,passwd,testDBname,tableName,clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"VerifyTableexistsinDatabase", "serverName":"' + str(serverName) + '", "userId":"' + str(userId) + '","passwd":"'+str(passwd)+'", "testDBName": "'+ str(testDBname) +'", "tableName": "'+ str(tableName) +'"}}'
    messageSenderObj=messagesender.messagesender()
    routing_key=(apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Verify restore complete...")  
    return testresult

def verifyTableDoesNotexistsinDatabase(serverName,userId,passwd,testDBname,tableName,clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"VerifyTableDoesNotexistsinDatabase", "serverName":"' + str(serverName) + '", "userId":"' + str(userId) + '","passwd":"'+str(passwd)+'", "testDBName": "'+ str(testDBname) +'", "tableName": "'+ str(tableName) +'"}}'
    messageSenderObj=messagesender.messagesender()
    routing_key=(apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Verify restore complete...")  
    return testresult

def verifylisteddirectoriesinfolder(dirPath,clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"VerifyListedDirectoriesInFolder", "backupPath":"'+str(dirPath)+'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    print routing_key
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    print testresult
    print "Completed checking restore file path...."  
    return testresult

def executePowerShellCommandForFullResponse(command,clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"ExecutePowershellScriptFullResponse","Command":"'+str(command)+'" }}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    print routing_key
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Execution of power shell command done !")
    return testresult

def getPathFromVMID(VMID,clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"getPathforVM","vmID":"'+str(VMID)+'" }}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    print routing_key
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Path of VMID found !")
    logHandler.logging.info("Test Result: " + str(testresult))    
    return testresult

def createColumnnMasterKeyDb(serverName, userId, passwd, testDBName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"createColumnMasterKey","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Create Column Master Key complete...")  
    return testresult

def createColumnnEncryptionKeyDb(serverName, userId, passwd, testDBName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"createColumnEncryptionKey","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Create Column Encryption Key complete...")  
    return testresult

def createEncryptedTableDB(serverName, userId, passwd, testDBName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"createEncryptedTable","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Create Encrypted table complete...")  
    return testresult

def createProcedure(serverName, userId, passwd, testDBName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"createEncryptedTableProcedure","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Create Encrypted Table Procedure complete...")  
    return testresult

def serverAuthenticateLogin(serverName, userId, passwd, testDBName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"createServerAuthenticateLogin","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Create server authenticate login complete...")  
    return testresult

def databaseUserCreate(serverName, userId, passwd, testDBName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"createDatabaseUser","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Create Database User complete...")  
    return testresult

def ExecuteScript(clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"execute_batch_script_EncryptedDb"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Execute Scipt Passed...")  
    return testresult

def deleteUser(serverName, userId, passwd, testDBName, clientIP):
    message = '{"action":"executeClientUtilities","payload":{"UtilityType":"DeleteUser","serverName":"' +str(serverName)+'", "userId":"' + str(userId) + '", "passwd":"' + str(passwd) + '", "testDBName":"' + str(testDBName) +'"}}'
    messageSenderObj = messagesender.messagesender()
    routing_key = (apiconfig.driverIP+clientIP).replace(".","")
    logHandler.logging.info("Routing Key: " + str(routing_key))
    testresult=messageSenderObj.sendMessage(routing_key, message)
    logHandler.logging.info("Test Result: " + str(testresult))
    logHandler.logging.info("Delete User complete...")  
    return testresult





'''
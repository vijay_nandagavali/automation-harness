'''
Created on Sep 16, 2014

@author: Abhijeet.m
'''

import logging
from config import apiconfig


def logFile(exeId):
    logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s\t%(filename)s : %(lineno)d\t%(levelname)s\t:: %(message)s',
                    filename=apiconfig.UAS_DIR_PATH + "/REST-API/Logs/RestAPI_" + str(exeId) + ".log",
                    filemode='a',
                    maxBytes=20,
                    backupCount=500)



import urllib
import urllib2
import xml.etree.ElementTree as ET
from testdata import clients_testdata

#UEB Details
ueb_ip = '192.168.197.185'
ueb_username = 'root'
ueb_password = 'unitrends1'
ver = '8.1.0'
base64Password = 'dW5pdHJlbmRzMQ=='
base64Username = 'cm9vdA=='
name = clients_testdata.name
ip = clients_testdata.ip

def Get_Login_Response():
    
        #----------------------------------------------------------------------
        # Request to login to ueb to get the session specific Authentication String
        #----------------------------------------------------------------------
        loginUrl = "https://"+ueb_ip+'/recoveryconsole/bpl/login.php?' + 'name=' + ueb_username+ '&rx=8283700' + '&ver=' + ver + '&gcv=0' + '&lvl=3'
        print loginUrl
        values_login = {'base64Password': base64Password, 'base64Username': base64Username, 'password': ueb_password}
        response_login = Send_Post_Req(loginUrl, values_login)
        print response_login
        
        return response_login
    
def Get_Xml_Element(parent_tag, get_tag, response="",is_parent=0):
        get_tag_value = ""
        root = ET.fromstring(response)
        if is_parent == 1:
            if (root.find(get_tag) != None):
                get_tag_value = root.find(get_tag).text
        else:
            for child_tag in root.findall(parent_tag):
                get_tag_value = child_tag.find(get_tag).text
                break
        return get_tag_value
    
def Send_Post_Req(url, values):
        data = urllib.urlencode(values)
        req = urllib2.Request(url, data)
        try :
            response = urllib2.urlopen(req, timeout=300)
        except urllib2.HTTPError, e:
                return str(e.code)
        the_response = response.read()           
        return the_response

def chk_host_present(AuthString):
    hostpresentflag = 0 
    chkHostUrl = "https://"+ueb_ip+'/recoveryconsole/bpl/hosts.php?type=list&sid=1&rx=2605500&ver=8.1.0&gcv=0&lvl=3'
    values = {'auth':AuthString}
    response = Send_Post_Req(chkHostUrl, values)
    root = ET.fromstring(response)
    for host in root.findall('Host'):
        print ip
        if(host.find('IP').text==ip):
            hostpresentflag=1
            break
    return hostpresentflag

def Delete_Host(AuthString):
    deleteHostUrl =  "https://"+ueb_ip+'/recoveryconsole/bpl/hosts.php?type=delete&sid=1&name='+name+'&ip='+ip+'&aliases=&rx=8283700&ver='+ver+'&gcv=0&lvl=3'
    values = {'auth':AuthString}
    response = Send_Post_Req(deleteHostUrl, values)
    print response
        
def deleteHostIfPresent():
    loginResponse = Get_Login_Response()
    AuthString = Get_Xml_Element(
                    "root", "AuthenticationString", loginResponse, 1)
    print AuthString
    flag = chk_host_present(AuthString)
    if(flag==1):
        Delete_Host(AuthString)
'''
Created on Nov 5, 2014

@author: Amey.k
'''


import datetime
from util import linuxMachineConnect, logHandler

def getCurrentDateTime(dateTimeFormat):
    return datetime.datetime.now().strftime(dateTimeFormat)

def convertStringToDateTimeObj(dateTimeStr,dateTimeFormat):
    return datetime.datetime.strptime(dateTimeStr, dateTimeFormat)


def getDateTimeInHMSFormat(dateTimeStr):
    dateTimeStr = dateTimeStr.split(":")
    return dateTimeStr[0] + "Hrs " + dateTimeStr[1] + "Mins " + dateTimeStr[2] + "Secs"

'''Below function "getDateInAMPMFormat" takes the input date in format "YYYY-MM-DD_HH_MM_SS" and returns the date in format "MM/DD/YYYY HH:MM:SS am/pm"
Example: 
input date = "2015-05-05_01_29_57"
formatted_date = "05/05/2015 01:29:57 am" 
'''
def getDateInAMPMFormat(input_date):
    dateList = str(input_date).split("_")
    dateList[0] = dateList[0].split("-")
    formatted_date = dateList[0][1] + "/" + dateList[0][2] + "/" + dateList[0][0] + " " + dateList[1] + ":" + dateList[2] + ":" + dateList[3]
    if (dateList[1] == '00'):
        formatted_date = dateList[0][1] + "/" + dateList[0][2] + "/" + dateList[0][0] + " " + str(int(dateList[1]) + 12) + ":" + dateList[2] + ":" + dateList[3] + " am"
    elif (dateList[1] < '12'):
        formatted_date = dateList[0][1] + "/" + dateList[0][2] + "/" + dateList[0][0] + " " + dateList[1] + ":" + dateList[2] + ":" + dateList[3] + " am"
    elif (dateList[1] == '12'):
        formatted_date = dateList[0][1] + "/" + dateList[0][2] + "/" + dateList[0][0] + " " + dateList[1] + ":" + dateList[2] + ":" + dateList[3] + " pm"
    elif (dateList[1] > '12'):
        formatted_date = dateList[0][1] + "/" + dateList[0][2] + "/" + dateList[0][0] + " " + str(int(dateList[1]) - 12) + ":" + dateList[2] + ":" + dateList[3] + " pm"
    return formatted_date

def getDateTimeFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    logHandler.logging.info("Connection: " + str(conn))
    dateTime = []
    format = "%x" #%m/%d/%Y
    cmd = "date +""" + format + "" 
    #logHandler.logging.info("cmd = " + str(cmd))
    _, out, err = conn.exec_command(cmd)
    temp = out.readlines()
    dateTime.append(str(temp[0]).strip(' \r\t\n'))
    format = "%H:%M"
    cmd = "date +""" + format + "" 
    #logHandler.logging.info("cmd = " + str(cmd))
    _, out, err = conn.exec_command(cmd)
    temp = out.readlines()
    dateTime.append(str(temp[0]).strip(' \r\t\n'))
    #logHandler.logging.info("DateTime from machine: " + str(dateTime))
    return dateTime

def getTimePlusTwoMinutes(curTime):
    timeList = curTime.split(":")
    if(timeList[1] == '59'):
        timeList[1] = '01'
        timeList[0] = str(int(timeList[0]) + 1)
    elif(timeList[1] == '58'):
        timeList[1] = '00'
        timeList[0] = str(int(timeList[0]) + 1)        
    elif(timeList[1] < '08'):
        timeList[1] = "0" + str(int(timeList[1]) + 2)
    else:
        timeList[1] = str(int(timeList[1]) + 2)
    retTime = str(timeList[0]) + ":" + str(timeList[1])
    return retTime
    
def getTimeMinusOneMinute(input_time):
    timeList = input_time.split(":")
    if(timeList[1] == '00'): 
        timeList[1] = '59'
        timeList[0] = str(int(timeList[0]) - 1)
    elif(timeList[1] == '10'):
        timeList[1] == '09'    
    else:
        timeList[1] = str(int(timeList[1]) - 1)
    retTime = str(timeList[0]) + ":" + str(timeList[1])
    return retTime

def getTimePlusOneMinute(input_time):
    timeList = input_time.split(":")
    if(timeList[1] == '59'):
        timeList[1] = '00'
        if(timeList[0] > '09'):
            timeList[0] = str(int(timeList[0]) + 1)
        else:
            timeList[0] = '0' + str(int(timeList[0]) + 1)
    elif(timeList[1] < '09'):
        timeList[1] = "0" + str(int(timeList[1]) + 1)
    else:
        timeList[1] = str(int(timeList[1]) + 1)
    retTime = str(timeList[0]) + ":" + str(timeList[1])
    return retTime


def getUnixTimeDiff(dt1, dt2):
    #dt1 - Timestamp of the 1st request
    epoch = datetime.datetime.utcfromtimestamp(dt1)
    epoch2 = datetime.datetime.utcfromtimestamp(dt2)
    delta = epoch2 - epoch
    return delta.total_seconds()
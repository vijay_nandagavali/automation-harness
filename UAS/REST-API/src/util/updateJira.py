'''
Created on Aug 20, 2015

@author: a4tech
'''
JIRA_SERVER =  'https://unitrends.atlassian.net'
PASS = ['PASS','pass','Pass','PASSED','passed','Passed']
FAIL = ['FAIL', 'fail','Fail','FAILED', 'failed','Failed']

import json
from jira import JIRA
from config import apiconfig,Constants
from model.ExecutionSummary import ExecutionSummary
from model.TestCaseResult import TestCaseResult
from os import path
from config.Constants import ExecutionStatus
from util import uebVersionHandler

def changestatus(issue_id,st):

    try:
        jira = JIRA(JIRA_SERVER,basic_auth=('sayali.kher', '@four123')) # create client with basic auth
        issue = jira.issue(issue_id)
        currentStatus = issue.fields.status      
        print "current status: " + str(currentStatus)                        
        if(currentStatus.name =='TC - Passed'):
            transitions = jira.transitions(issue)         
            for t in transitions: 
                if (t['name'] == 'Reopen'):
                    ptid = t['id']
            jira.transition_issue(issue,ptid)
                    
        if(currentStatus.name == 'TC - Failed'):
            transitions = jira.transitions(issue)           
            for t in transitions: 
                if (t['name'] == 'Reopen'):
                    ptid = t['id']
            jira.transition_issue(issue,ptid)
        
        issue = jira.issue(issue_id)        
        currentStatus = issue.fields.status
        print "current status: " + str(currentStatus)                  
        if(currentStatus.name == 'TC - Ready To Run'):
            #get transitions and Start Test 
            transitions = jira.transitions(issue)                      
            for t in transitions: 
                if (t['name'] == 'Start Test'):
                    ptid = t['id']
            jira.transition_issue(issue,ptid )
        
        issue = jira.issue(issue_id)        
        currentStatus = issue.fields.status                
        print "current status: " + str(currentStatus)                        
        if(currentStatus.name == 'In Progress'):
            transitions = jira.transitions(issue)              
            for t in transitions: 
                if t['name'] in PASS :
                    ptid = t['id']
                    print "ptid ", ptid
                if t['name'] in FAIL:
                    ftid = t['id'] 
                    print "ftid ", ftid
         
            if st in PASS:
                jira.transition_issue(issue,ptid,comment= "Updated by automation for Version " + str(uebVersionHandler.getUEBVersion()) )
            if st in FAIL:
                jira.transition_issue(issue,ftid,comment= "Updated by automation for Version " + str(uebVersionHandler.getUEBVersion()))
        
        issue = jira.issue(issue_id)        
        currentStatus = issue.fields.status                
        print "current status: " + str(currentStatus)    
  
    except Exception as e:
        print "Unexpected error:" + str(e)

def UpdateJiraForSuite(exeId):
    fileName = apiconfig.UAS_DIR_PATH + "/Reports/" + exeId + ".json"
    if path.exists(fileName):
        jsonFile = open(fileName)
        execSumm = ExecutionSummary.fromJson(jsonFile.read())        
        jsonFile.close()
        
    
    testCaseList  = execSumm.testCaseResultList;
    for index in range (len(testCaseList)):                      
        testCaseId = execSumm.testCaseResultList[index].testCaseId
        testCaseId = testCaseId[1:6]
        testCaseId = testCaseId[-4:]
        testCaseId = "MQ-" + testCaseId
        print testCaseId
        print execSumm.testCaseResultList[index].executionStatus
        changestatus(testCaseId, execSumm.testCaseResultList[index].executionStatus)
        
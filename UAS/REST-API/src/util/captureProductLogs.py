'''
Created on Jun 25 2015

@author: Sayali K
'''
from util import linuxMachineConnect, logHandler, fileHandler
from testdata import logs_testdata
import paramiko
import re
import datetime 
from datetime import datetime

from config import apiconfig

def sftpConnection():
    t = paramiko.Transport((apiconfig.environmentIp, 22))
    t.connect(username=apiconfig.machineuser, password=apiconfig.machinepassword)
    sftp = paramiko.SFTPClient.from_transport(t)
    return sftp

def getFilesSizeDict():
    sftpConn=sftpConnection()
    nameSizeDict={}
    sftpConn.chdir('/usr/bp/logs.dir/')
    dirlist = sftpConn.listdir('.')
    for fileName in dirlist:
        for text in logs_testdata.logFileKeywordsArray:
            match=re.search(text,fileName)
            if match:
                nameSizeDict[fileName]=sftpConn.stat(fileName).st_size
    print nameSizeDict
    return nameSizeDict

def getModifiedFileArray(dictStart,dictEnd):
    ModifiedFileArray=[]
    for fileName1 in dictStart:
        for fileName2 in dictEnd:
            if (fileName1==fileName2) and (dictEnd[fileName2]>dictStart[fileName1]):
                ModifiedFileArray.append(fileName1)
    print  ModifiedFileArray
    return ModifiedFileArray
               
def captureProductLogs(exeId, tcName, filesArray, t_start):
    try:
        successFlag=False
        filename = fileHandler.createProductLogFile(exeId)
        lineNumber = -1
        if (successFlag==False):
            conn = linuxMachineConnect.SshRemoteMachineConnection()
            sftp_client = paramiko.SSHClient.open_sftp(conn)
            for log in filesArray:
                print log
                fileHandler.writeFile(filename, "Test Case - " + str(tcName))
                fileHandler.writeFile(filename, "Log File - " + str(log))
                f=sftp_client.open('/usr/bp/logs.dir/'+log, "r+") 
                for line in f:
                    # Parse the section headers enclosed between the characters "[" and "]" 
                    match=re.search(r'(\d+:\d+:\d+)',line)
                    if(match):
                        datetime1=line.split(" ")
                        datetime1[1] = datetime1[1].rstrip(':')
                        try:                            
                            t1 = datetime.strptime(str(t_start), "%Y-%m-%d %H:%M:%S")
                            if(re.search(r'\D+ \d+ \d+',datetime1[0]+" " + datetime1[1])):
                                t2 = datetime.strptime(datetime1[0]+" " + datetime1[1] + " " + datetime1[2], "%b %d %Y %H:%M:%S")
                            elif(re.search(r'\d+/\d+/\d+',datetime1[0]+" " + datetime1[1])):
                                t2 = datetime.strptime(datetime1[0]+" " + datetime1[1], "%m/%d/%Y %H:%M:%S")
                            if(t2>=t1):
                                lineNumber = f.tell()
                                fileHandler.writeFile(filename, line)                        
                                break
                        except ValueError as e1:
                            print str(e1)
                            logHandler.logging.info("Value error: " + str(e1))
                            continue
                if(lineNumber >= 0):
                    f.seek(lineNumber)
                    for line in f:
                        fileHandler.writeFile(filename, line)
            
            fileHandler.writeFile(filename, "\n**************************************************\n")
            f.close()
            successFlag=True
        else:
            successFlag = False
    except Exception as e:
        print str(e)
        logHandler.logging.info("Product Log capture error: " + str(e))
    
    return successFlag
'''
Created on Apr 24, 2015

@author: Nikhil.S
'''
from testdata import replication_testdata
from util import logHandler, linuxMachineConnect


import paramiko
from __builtin__ import str
COMMENT_CHAR = ';'
OPTION_CHAR =  '='



def parse_config(option1):
    # SSH connection to UEB:
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    sftp_client = paramiko.SSHClient.open_sftp(conn)
    # Open master.ini file
    f=sftp_client.open(replication_testdata.UEB_MASTER_INI_PATH)
    options = {}
    for line in f:
        # First, remove comments:
        if COMMENT_CHAR in line:
            # split on comment char, keep only the part before
            line, comment = line.split(COMMENT_CHAR, 1)
        # Second, find lines with an option=value:
        if OPTION_CHAR in line:
            # split on option char:
            option, value = line.split(OPTION_CHAR, 1)
            # strip spaces:
            option = option.strip()
            value = value.strip()
            if(option==option1):
                break
            # store in dictionary:
            options[option] = value
    f.close()
    print "option is : " + str(option) + " and value is "+ str(value)
    if(value == "Yes"):
        return False
    else:
        return value


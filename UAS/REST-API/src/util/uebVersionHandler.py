'''
Created on Dec 2, 2014

@author: Amey.k
'''

import linuxMachineConnect

def getUEBVersion(appliance_ip):
    conn = linuxMachineConnect.SshRemoteMachineConnection(appliance_ip)
    _, out, err = conn.exec_command('dpu version | grep Version | cut -c8-26')
    temp = out.readlines()
    return str(temp[0]).strip(' \r\t\n')


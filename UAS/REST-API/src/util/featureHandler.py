import json
import requests
from util import logHandler
import paramiko
from config import apiconfig
from util import apiHelper, dbHandler
from resourcelibrary import  settings
from testdata import settings_testdata
from config.apiconfig import machineuser
from platform import node
from time import sleep

def Create_SSH_Client(machine_ip_address, machine_user, machine_password):
    try:
        machine_user = apiconfig.machineuser
        machine_password = apiconfig.machinepassword
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_system_host_keys()
        ssh.connect(hostname=machine_ip_address, username=machine_user, password=machine_password)
        print "Connection Established to :" + str(machine_ip_address)
        logHandler.logging.info("Connection Established to :" + str(machine_ip_address))
        return ssh
    except Exception as exception:
        logHandler.logging.error(exception)


def Get_Formatted_Output_For_dbQuery(ssh, machine_ip_address, db_query):
    try:
        SSH_Client = ssh
        db_command = "psql bpdb postgres -c " + "\"" + db_query  + "\""
        _, ssh_client_out, ssh_client_err = SSH_Client.exec_command(db_command)
        db_query_output = ssh_client_out.readlines()
        db_query_output = db_query_output[:-2]
        print db_query_output
        del db_query_output[1]
        # convert unicode strings to python strings
        db_query_output = [str(each_db_query_output) for each_db_query_output in db_query_output]
        # replace '|' by ','
        db_query_output = [each_db_query_output.replace('|', ',') for each_db_query_output in db_query_output]
        # replace ' ' by ''
        db_query_output = ["".join(each_db_query_output.split()) for each_db_query_output in db_query_output]
        # add string to empty list
        db_query_output = [each_db_query_output.split(',') for each_db_query_output in db_query_output]
        print db_query_output
        return db_query_output
    except Exception as exception:
        logHandler.logging.error(exception)


def Get_Encryption_Status(environment_ip,auth_token ):

    try:

        encryption_status = ""
        passphrase_status = ""
        encryption_api_url = "/api/encryption/?sid=1"
        encryption_url = "http://" + environment_ip + encryption_api_url
        logHandler.logging.info("GET Request URL: " + str(encryption_url))
        headers = {'Content-Type': 'application/json', 'AuthToken': str(auth_token)}
        r = requests.get(encryption_url, headers=headers, verify=False)
        if r.status_code != 200:
            logHandler.logging.error("Response: " + str(r.content))
            r.raise_for_status()

        logHandler.logging.info("Send GET Request: " + encryption_url)
        logHandler.logging.info("Response: " + str(r.content))
        data = json.dumps(((json.loads(r.content))['data'])[0])
        state = json.loads(json.dumps(((json.loads(data))['state'])))
        if state == "persist" or state == "on":
            encryption_status = "on"
        else:
            encryption_status = "off"

        print "encryption_status " + encryption_status
        has_passphrase = json.loads(json.dumps(((json.loads(data))['has_passphrase'])))
        if has_passphrase:
            passphrase_status = "Passphrase Set"

        else:
            passphrase_status = "Passphrase not set"

        return encryption_status

    except Exception as exception:
        logHandler.logging.error(exception)
        

def Set_Encryption_OFF_Asset(environment_ip ,vm_name,auth_token):

    try:
        machine_user = apiconfig.machineuser
        machine_password = apiconfig.machinepassword
        ssh = Create_SSH_Client(environment_ip, machine_user, machine_password)
        # get the node no of the asset
        query = "select node_no from bp.nodes where node_name = " + "'" + vm_name + "'"
        query_result = Get_Formatted_Output_For_dbQuery(ssh, environment_ip, query)
        node_no = int(query_result[1][0])
        print "node_no:" + str(node_no)

        setencryptionapi = "http://" + environment_ip + "/api/clients/" + str(node_no) + "/?sid=1"
        clientinfo = {"name": vm_name,"is_encrypted": False}
        data = json.dumps(clientinfo)
        headers = {'Content-Type': 'application/json', 'AuthToken': str(auth_token)}

        # set encryption off : This selects the option "Do not encrypt backups for this server and its asset" on UI
        setencryptiononasset = requests.put(setencryptionapi, data=data, headers=headers)
        print setencryptiononasset
        print setencryptiononasset.status_code
        if str(setencryptiononasset.status_code) != "200":
            setencryptiononasset.raise_for_status()
            return False
        else:
            encryption_status_asset_off = True
        
        return encryption_status_asset_off

    except Exception as exception:
        logHandler.logging.exception(exception)


        
def Check_Inline_dedup(machine_ip_address):
    machine_user = apiconfig.machineuser
    machine_password = apiconfig.machinepassword
    ssh = Create_SSH_Client(machine_ip_address, machine_user, machine_password)
    
    #Command to check dedup
    cmd_verify_dedup= 'cat /usr/bp/bpinit/master.ini | grep enableDeduplication= | awk \'{print $1}\''
    stdin, stdout, stderr = ssh.exec_command(cmd_verify_dedup)
    dedup_status = stdout.read().split('=')[1].strip().lower()
    if dedup_status == 'no':
        print "Dedup is not enabled "
        return False
    elif dedup_status == 'yes':
        print "Dedup is enabled"
        
    #Command to check Inline dedup
    cmd_verify_inlineDedup = 'cat /usr/bp/bpinit/master.ini | grep enableInlineDedup= | awk \'{print $1}\''
    #VerifyInlineDedup
    stdin, stdout, stderr = ssh.exec_command(cmd_verify_inlineDedup)
    inline_dedup_status = stdout.read().split('=')[1].strip().lower()

    print "Inline Dedup status is :",inline_dedup_status
    if inline_dedup_status == 'no':
        print "Inline Dedup is not enabled "
        return False
    elif inline_dedup_status == 'yes':
        print "Inline Dedup is enabled "
        return True
    
# def Set_Inline_dedup_on(applianceip, token): 
#     params = {"field":"enableDeduplication","value":"No","description":"Perform file deduplication.","section":"fileDedup","$$hashKey":"uiGrid-01LF"}
#     dedup_response = settings.setSettings(applianceip, settings_testdata.sid, params, token)
#     dedup_response_in_json = settings.getSettingsJobParametersInJson(dedup_response)
#     print "dedup response in json : " + dedup_response_in_json
#     logHandler.logging.info(dedup_response_in_json)
#     return dedup_response_in_json

def check_Encrytion_of_backup(applianceip, backupId):
    
    print "backupID: "+str(backupId)
    #dbPrpertyResult = dbHandler.executeQuery("""select properties from bp.backups where backup_no=""" +str(backupId))        
    dbPropertyResult = dbHandler.executeQuery(applianceip, """select properties from bp.backups where backup_no=""" +str(backupId))
    print "dbPropertyResult: " + str(dbPropertyResult[0][0])
    dbPropertyValue =  dbPropertyResult[0][0]
    
    #successFlag=False        
    try:        
        machine_user = apiconfig.machineuser
        machine_password = apiconfig.machinepassword
        ssh = Create_SSH_Client(applianceip, machine_user, machine_password)
                  
        cmdToFetchMetadata="show_properties "+str(dbPropertyValue)
        
        print cmdToFetchMetadata
        
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmdToFetchMetadata)                                
        output = ssh_stdout.readlines()
        
        if any("P_ENCRYPTED" in s for s in output):
            print "Backup Encrypted"
            return 1
        else:
            print"Backup  Not Encrypted"
            return 0
    
    except Exception as e:
        
        logHandler.logging.info(str(e))
        print "Check encryption exception: " + str(e)
        #successFlag = False
    #return successFlag


def Enable_Encryption_Appliance(environment_ip, passphrase, auth_token):

    try:

        encryption_api_url = "/api/encryption/persistent/?sid=1"
        encryption_url = "http://" + environment_ip + encryption_api_url

        pass_phrase = {'passphrase': passphrase}
        data = json.dumps(pass_phrase)
        headers = {'Content-Type': 'application/json', 'AuthToken': str(auth_token)}

        req = requests.put(encryption_url,data=data,headers=headers, verify=False)

        if req.status_code != 200:
            req.raise_for_status()
        else :
            print "encryption set to on"


    except Exception as exception:
        logHandler.logging.error(exception)
        
def set_inline_dedup(appliance_ip, token):
    jsondata = {}
    jsondata["mux"] = 10
    jsondata["deduplevel"] = 3
    return apiHelper.sendPUTRequest("https://" +appliance_ip + "/api/optimize/", jsondata, token)

def verifySuccessfulDedupForABackup(appliance_ip, backupNo):
    machine_user = apiconfig.machineuser
    machine_password = apiconfig.machinepassword
    ssh = Create_SSH_Client(appliance_ip, machine_user, machine_password)
    _, ssh_client_out, ssh_client_err = ssh.exec_command("""file /backups/D2DBackups/backup_""" +str(backupNo))
    db_query_output = ssh_client_out.read()
    print db_query_output
    if("tar" in str(str(db_query_output))):
        return 1
    else:
        return 0
    
    
def Set_Encryption_On_Asset(environment_ip ,client_name, token):
    
    try:
        
        machine_user = apiconfig.machineuser
        machine_password = apiconfig.machinepassword
        
        ssh = Create_SSH_Client(environment_ip, machine_user, machine_password)
        # get the node no of the asset
        query = "select node_no from bp.nodes where node_name = " + "'" + client_name + "'"
        query_result = Get_Formatted_Output_For_dbQuery(ssh, environment_ip, query)
        node_no = int(query_result[1][0])
        print "node_no:" + str(node_no)
        
        setencryptionapi= "http://" + environment_ip + "/api/clients/" + str(node_no) + "/?sid=1"
        clientinfo = {"name":client_name,"is_encrypted":True}
        data = json.dumps(clientinfo)
        headers = {'Content-Type': 'application/json', 'AuthToken': str(token)}

        #set encryption on : This selects the option "Encrypt all backups for this server and its asset" on UI
        setencryptiononasset = requests.put(setencryptionapi, data=data, headers = headers)

        if setencryptiononasset.status_code != 200:
            setencryptiononasset.raise_for_status()
            return False
            print "attempt failed"
        else:
            #print "encryption set successfully"
            encryption_status_asset_on = True
            return encryption_status_asset_on
            logHandler.logging.info("Encryption status set to " + encryption_status_asset_on)
            
    except Exception as e:
        
        logHandler.logging.info(e)
        
        
        
def set_file_dedup(appliance_ip, token):
    jsondata = {}
    jsondata["mux"] = 10
    jsondata["deduplevel"] = 2
    return apiHelper.sendPUTRequest("https://" +appliance_ip + "/api/optimize/", jsondata, token)




def check_SISCompression_Status (appliance_ip):
    try:
        
        machine_user = apiconfig.machineuser
        machine_password = apiconfig.machinepassword
        ssh = Create_SSH_Client(appliance_ip, machine_user, machine_password)
        logHandler.logging.info("Connected to client" + appliance_ip )
        print ("Connected to client" + appliance_ip )
           
        cmd_check_sisCompression_status = "awk '/\[fileDedup\]/,/\[Monitorin\g]/' /usr/bp/bpinit/master.ini | grep useSIScompression"
        stdin, stdout, stderr = ssh.exec_command(cmd_check_sisCompression_status)
        siscompression_status = stdout.read().split(" ")
        sisCompression_status = siscompression_status[2]
        print sisCompression_status
        
        return sisCompression_status
    
    except Exception as e:
        print e
        logHandler.logging.info(e)      

def change_SISCompression_status(appliance_ip, status):
    try:
        #get SISCompression Status
        machine_user = apiconfig.machineuser
        machine_password = apiconfig.machinepassword
        ssh = Create_SSH_Client(appliance_ip, machine_user, machine_password)
        logHandler.logging.info("Connected to client" + appliance_ip )
        logHandler.logging.info("connected") 
        print "connected"
        
        #check current status  
        cmd_check_sisCompression_status = " awk '/\[fileDedup\]/,/\[Monitorin\g]/' /usr/bp/bpinit/master.ini | grep useSIScompression"
        print cmd_check_sisCompression_status
        stdin, stdout, stderr = ssh.exec_command(cmd_check_sisCompression_status)
        sis_compression = stdout.read().split(" ")[2]
        logHandler.logging.info("current SISCompression status is " + sis_compression)  
        print ("current SISCompression status is " + sis_compression) 
        #change the SIScompression status
        values = sis_compression + "/" + status
        print values
        cmd_change_SIScompression_Status = "sed -i 's/" + values + "/' /usr/bp/bpinit/master.ini"
        print cmd_change_SIScompression_Status
        stdin, stdout, stderr = ssh.exec_command(cmd_change_SIScompression_Status)
        siscompression_status = stdout.read()
        return siscompression_status
        logHandler.logging.info("changed SISCompression Status is " + siscompression_status)
        print siscompression_status
    
    except Exception as e:
        print e
        logHandler.logging.info(e)
        
        
        
        
def check_compression_mode(appliance_ip, backup_device):
    machine_user = apiconfig.machineuser
    machine_password = apiconfig.machinepassword
    ssh = Create_SSH_Client(appliance_ip, machine_user, machine_password)
    
    if ( backup_device == 'D2DBackups' ):
        cmd_to_check_compression_mode = "awk '/\[D2DBackups\]/,/\[Monitorin\g]/' /usr/bp/bpinit/master.ini | grep CompressEngine"
        
        stdin, stdout, stderr = ssh.exec_command(cmd_to_check_compression_mode)
        compression_status = stdout.read().split('=')[1].strip()
        
        return compression_status
    else:
        print "Backup device is not 'D2DBackups'"
        return "Unknown"
    
    
    
def setCompressionMode(appliance_ip, backup_device, compression_mode):
    try:            
        if ( backup_device == 'D2DBackups' ):
            machine_user = apiconfig.machineuser
            machine_password = apiconfig.machinepassword
            ssh = Create_SSH_Client(appliance_ip, machine_user, machine_password)
            
            # first get current compression mode status
            cmd_to_check_compression_mode = "awk '/\[D2DBackups\]/,/\[Monitorin\g]/' /usr/bp/bpinit/master.ini | grep CompressEngine"
            
            stdin, stdout, stderr = ssh.exec_command(cmd_to_check_compression_mode)
            current_compression_mode = stdout.read().strip()
            new_compression_mode = "/CompressEngine=" + compression_mode
            
            # Command to set compression
            cmd_to_set_compression_mode = "sed -i 's/" + current_compression_mode + new_compression_mode + "/' /usr/bp/bpinit/master.ini"
            
            # execute the set command
            stdin, stdout, stderr = ssh.exec_command(cmd_to_set_compression_mode)
            
            # execute one more command to check mode is set to correct value or not
            stdin, stdout, stderr = ssh.exec_command(cmd_to_check_compression_mode)
            set_mode = stdout.read().split('=')[1].strip().lower()

            if (set_mode == compression_mode):
                return True
            else:
                return False
        else:
            print "Backup device is not 'D2DBackups'"
            return "Unknown"       
    except Exception as exception:
        logHandler.logging.error(exception)  
        
        
        
def check_lz4Compression_backup(appliance_ip, backupNo):
    machine_user = apiconfig.machineuser
    machine_password = apiconfig.machinepassword
    ssh = Create_SSH_Client(appliance_ip, machine_user, machine_password)
    _, ssh_client_out, ssh_client_err = ssh.exec_command("""file /backups/D2DBackups/backup_""" +str(backupNo))
    db_query_output = ssh_client_out.read()
    print db_query_output
    if("data" in str(str(db_query_output))):
        return True
    else:
        return False 
        
        
        
def set_no_dedup(appliance_ip, token):
    try:
        jsondata = {}
        jsondata["mux"] = 10
        jsondata["deduplevel"] = 1
        return apiHelper.sendPUTRequest("https://" +appliance_ip + "/api/optimize/", jsondata, token)
    except Exception as e:
        print e
        logHandler.logging.info("Exception in setting no dedup " + e)


def check_backup_notDeduplicated(applianceip, backupId):
    
    print "backupID: "+str(backupId)
    #dbPrpertyResult = dbHandler.executeQuery("""select properties from bp.backups where backup_no=""" +str(backupId))        
    dbPropertyResult = dbHandler.executeQuery(applianceip, """select properties from bp.backups where backup_no=""" +str(backupId))
    print "dbPropertyResult: " + str(dbPropertyResult[0][0])
    dbPropertyValue =  dbPropertyResult[0][0]
    
    #successFlag=False        
    try:        
        machine_user = apiconfig.machineuser
        machine_password = apiconfig.machinepassword
        ssh = Create_SSH_Client(applianceip, machine_user, machine_password)
                  
        cmdToFetchMetadata="show_properties "+str(dbPropertyValue)
        
        print cmdToFetchMetadata
        
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmdToFetchMetadata)                                
        output = ssh_stdout.readlines()
        
        if any("P_DEDUPLICATED" in s for s in output):
            print "Backup deduplicated"
            return False
        else:
            print"Backup not deduplicated"
            return True
    
    except Exception as e:
        
        logHandler.logging.info(str(e))
        print "Chechk deduplication exception: " + str(e)
        
        

 
    



    

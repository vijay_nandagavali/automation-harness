'''
Created on Sep 5, 2014

@author: Abhijeet.m
'''

import json
from util import logHandler
import requests



def sendGETRequest(url, auth_token):
    logHandler.logging.info("GET Request URL: " + str(url))
    headers = {'Content-Type':'application/json', 'AuthToken':str(auth_token)}
    r = requests.get(url, headers=headers, verify=False)
    if(r.status_code != 200):
        logHandler.logging.error("Response: " + str(r.content))
        r.raise_for_status()
    #logHandler.logging.info("Send GET Request: " + url)
    logHandler.logging.info("Response: " + str(r.content))
    print "Response: " + str(r.content)
    return r.content

def sendGETRequestForStatusUnderWaiting(url, auth_token):
    
    headers = {'Content-Type':'application/json', 'AuthToken':str(auth_token)}
    r = requests.get(url, headers=headers, verify=False)
    if(r.status_code != 200):
        logHandler.logging.error("Response: " + str(r.content))
        r.raise_for_status()
    #logHandler.logging.info("Send GET Request: " + url)
    
    return r.content

def sendLoginPOSTRequest(url, bodyParameters):
    # data = urllib.urlencode(bodyParameters)
    data = json.dumps(bodyParameters)
    headers = {'Content-Type':'application/json'}
    r = requests.post(url, data=data, headers=headers, verify=False)
    #print r.status_code
    logHandler.logging.info("Login POST Request: Status code - " + str(r.status_code))
#     if(r.status_code != 201):
#         r.raise_for_status()
#     print r.text
    logHandler.logging.info("Send LOGIN POST Request " + url + " with request parameter " + str(bodyParameters))
    return r.content

def sendPUTRequest(url, bodyParameters, token):
    if(bodyParameters):
        data = json.dumps(bodyParameters)
    else:
        data = ""
    headers = {'Content-Type':'application/json', 'AuthToken':str(token)}
    logHandler.logging.info("Send PUT Request " + url + " with request parameter " + str(bodyParameters))
    r = requests.put(url, data=data, headers=headers, verify=False)
    #print r.status_code
    logHandler.logging.info("Status code: " + str(r.status_code))
    #print r.content
    if(r.status_code != 200):
        logHandler.logging.error("Response Content: " + r.content)
        r.raise_for_status()
    #print r.text
    logHandler.logging.info("Response Content: " + r.content)
    #logHandler.logging.info("Response Text: " + r.text)    
    return r.content

def sendDELETERequest(url, token, bodyParameters=""):
    logHandler.logging.info("DELETE Request URL: " + url)
    data = json.dumps(bodyParameters)
    headers = {'Content-Type':'application/json', 'AuthToken':str(token)}
    r = requests.delete(url, data=data, headers=headers, verify=False)
    #print r.status_code
    if(r.status_code != 200):
        #print r.content
        logHandler.logging.error("Response content: " + str(r.content))
        r.raise_for_status()
    #print r.text
    logHandler.logging.info("Response status code: " + str(r.status_code))
    logHandler.logging.info("Response content: " + str(r.content))
    return r.content

def sendPOSTRequest(url, bodyParameters, token):
    
    data = json.dumps(bodyParameters)
    headers = {'Content-Type':'application/json', 'AuthToken':str(token)}
    r = requests.post(url, data=data, headers=headers, verify=False)
    #print r.status_code
    logHandler.logging.info("Send POST Request " + url + " with request parameter " + str(bodyParameters))
    logHandler.logging.info("Response Status code: " + str(r.status_code))
#     if(r.status_code != 201):
#         r.raise_for_status()
    #print r.text
    logHandler.logging.info("Response content: " + r.text)    
    return r.content

def sendPOSTRequestAdvanced(url, bodyParameters, token):
    
    data = json.dumps(bodyParameters)
    headers = {'Content-Type':'application/json', 'AuthToken':str(token)}
    r = requests.post(url, data=data, headers=headers, verify=False)
    #print r.status_code
    logHandler.logging.info("Send POST Request " + url + " with request parameter " + str(bodyParameters))
    logHandler.logging.info("Response Status code: " + str(r.status_code))
#     if(r.status_code != 201):
#         r.raise_for_status()
    #print r.text
    logHandler.logging.info("Response content: " + r.text)    
    return r.content, r.status_code

def sendDELETERequestAdvanced(url, token, bodyParameters=""):
    logHandler.logging.info("DELETE Request URL: " + url)
    data = json.dumps(bodyParameters)
    headers = {'Content-Type':'application/json', 'AuthToken':str(token)}
    r = requests.delete(url, data=data, headers=headers, verify=False)
    logHandler.logging.info("Response status code: " + str(r.status_code))
    logHandler.logging.info("Response content: " + str(r.content))
    return r.content, r.status_code
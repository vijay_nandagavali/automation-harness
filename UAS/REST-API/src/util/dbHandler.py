'''
Created on Sep 16, 2014

@author: Abhijeet.m

Modified on Jan 19, 2017 

@author: Divya.J
'''
from util import logHandler, linuxMachineConnect

'''
Method to get data base connection
'''

from config import apiconfig

'''
def __getDBConnection(envIP):
    try:
        connString = "dbname='" + str(apiconfig.dbname) + "' user='" + str(apiconfig.dbuser) + "' host='"+ str(envIP) +"' password='"+ str(apiconfig.dbpassword) +"'"
        conn = psycopg2.connect(connString);
        return conn
    except Exception as e:
        logHandler.logging.error("DB Connection Exception: " + str(e))
'''
'''
Method to execute query and return the output data
'''
def executeQuery(appliance_ip,query, uebType="main"):
    if(uebType=="target"):
        envIP=apiconfig.environmentIpTarget
    elif(uebType=="source"):
        envIP=apiconfig.environmentIpSource
    else:
        envIP=apiconfig.environmentIp
    conn = linuxMachineConnect.SshRemoteMachineConnection(appliance_ip)
    return executeQueryCommandLine(conn, query)


'''
def executeDeleteQuery(query, uebType="main"): 
    if(uebType=="target"):
        envIP=apiconfig.environmentIpTarget
    elif(uebType=="source"):
        envIP=apiconfig.environmentIpSource
    else:
        envIP=apiconfig.environmentIp
    dbConnection = __getDBConnection(envIP)
    cur = dbConnection.cursor()
    cur.execute(query)
    dbConnection.commit()
    return True


def executeDBDictQuery(query, uebType="main"): 
    if(uebType=="target"):
        envIP=apiconfig.environmentIpTarget
    elif(uebType=="source"):
        envIP=apiconfig.environmentIpSource
    else:
        envIP=apiconfig.environmentIp
    dbConnection = __getDBConnection(envIP)
    cur = dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute(query)
    return cur.fetchall()
'''
'''
Method to execute query for a given IP and return the output data
'''
'''
def executeQueryForIp(query,uebType = "main"):
    if(uebType=="target"):
        envIP=apiconfig.environmentIpTarget
    elif(uebType=="source"):
        envIP=apiconfig.environmentIpSource
    else:
        envIP=apiconfig.environmentIp
    dbConnection = __getDBConnection(envIP)
    cur = dbConnection.cursor()
    cur.execute(query)
    return cur.fetchall()
'''


'''
Function for executing db queries through command line
Returns list of lists
'''


def executeQueryCommandLine(SSH_Client, db_query):
    '''
    logHandler.logging.info("###################")
    logHandler.logging.info("execute query through command line function is under construction")
    logHandler.logging.info("###################")
    '''
    try:
        db_command = "psql bpdb postgres -c " + "\"" + db_query + "\""
        print db_command
        _, ssh_client_out, ssh_client_err = SSH_Client.exec_command(db_command)
        db_query_output = ssh_client_out.readlines()
        print db_query_output
        db_query_output = db_query_output[2:-2]
        print db_query_output
        
        # convert unicode strings to python strings
        db_query_output = [str(each_db_query_output) for each_db_query_output in db_query_output]
        # replace '|' by ','
        db_query_output = [each_db_query_output.replace('|', ',') for each_db_query_output in db_query_output]
        # replace ' ' by ''
        db_query_output = ["".join(each_db_query_output.split()) for each_db_query_output in db_query_output]
        # add string to empty list
        db_query_output = [each_db_query_output.split(',') for each_db_query_output in db_query_output]
        print db_query_output
        return db_query_output
    except Exception as exception:
        logHandler.logging.error(exception)


'''
Created on Sep 1, 2015

@author: prateek


'''
import subprocess
from util import logHandler

def sendGSutilDeleteCommand(delobject):
    try:
        gsutilstr="~/gsutil/gsutil rm gs://"
        commandstr=gsutilstr+delobject+"/*"
        p=subprocess.Popen(commandstr,stdout=subprocess.PIPE,shell=True)
        
        p_status=p.wait()
        logHandler.logging.info("subdirectories in bucket deleted")
        
    except Exception as e:
        logHandler.logging.info("result of command line operation: "+e)
    return "pass"
    
    
    


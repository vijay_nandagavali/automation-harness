'''
Created on Sep 16, 2014

@author: Abhijeet.m
'''

import json
from util import logHandler


def getListOfParameterInJson(input_json, isList):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
     
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
            
    jsonParameter = []
#    if(isList == True):
    try:
        for key, value in jsonData.items():
            if(key != 'timestamp'):           
                if(isinstance(value, dict)):                                                
                    for k in value.items():  
                        jsonParameter.append(str(k[0]))           
                    break
                elif(isinstance(value, list)):
                    for k in value[0].items():  
                        jsonParameter.append(str(k[0]))
                    break       
                else:     
                    jsonParameter.append(str(key))
    except (TypeError,AttributeError):        
            #print "Data not present"
            logHandler.logging.error("Data not present")
    except IndexError:
            #print "Data not present"
            logHandler.logging.error("Data not present")
    jsonParameter.sort()
    return jsonParameter   

# def getListOfParameterInJson(input_json, isList):
#    
#     #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
#     
#     jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
#            
#     jsonParameter = []
# #    if(isList == True):
#     for key, value in jsonData.items():
#         try:
#             if(key != 'timestamp'):                 
#                     for k in value[0].items():                 
#                         jsonParameter.append(str(k[0]))          
#                     break
#         except (TypeError,AttributeError,IndexError):        
#                 if(key != 'timestamp'):
#                     jsonParameter.append(str(key))
#     
#     jsonParameter.sort()
#     return jsonParameter

def getAllParametersFromJson(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.              
    for key, value in jsonData.items():
        if(key != 'timestamp'):                            
            return value      
        
def getDictionaryFromJson(input_json): 
    jsonDict = {}
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.              
    for key, value in jsonData.items():
        if(key != 'timestamp'):
            jsonDict[str(key)] = value
    # print jsonDict
    return jsonDict

def getListOfJobParameterInJson(input_json, isList):
    # print input_json
    # input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    print input_json
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.          
    jsonParameter = []
    if(isList == True):
        for key, value in jsonData.items():
            if(key != 'timestamp'):                  
                for k in value[0].items():                 
                    jsonParameter.append(str(k[0]))          
                break
    else:
        for param in jsonData:        
            if(param != 'timestamp'):
                jsonParameter.append(str(param))
    jsonParameter.sort()
    return jsonParameter  
 
def getListOfMediaParameterInJson(input_json, isList):
    # print input_json
    # input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    #print input_json
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.          
    jsonParameter = []
    for key, value in jsonData.items():
        if(key != 'timestamp' and len(jsonData['data']) > 0):                         
            for k, v in jsonData['data'][0].items():                 
                jsonParameter.append(str(k))          
    jsonParameter.sort()
    return jsonParameter   

def getListOfMediaArchiveSetParameterInJson(input_json, isList):
    # print input_json
    # input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    print input_json
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.          
    jsonParameter = []
    for key, value in jsonData.items():
        if(key != 'timestamp'):                  
            for k, v in value.sets():                 
                jsonParameter.append(str(k))          
    jsonParameter.sort()
    return jsonParameter   

def getListOfClienParameterInJson(input_json):
    responseData = json.loads(input_json)
    jsonParameter = []
    if(len(responseData['data'])): 
        for keys, values in responseData['data'][0].items():
            jsonParameter.append(str(keys))
    else:
        #print "Data not present"
        logHandler.logging.info("Data not present")
    jsonParameter.sort()
    return jsonParameter


def getSummaryDaysParameterInJson(input_json):
    
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    print "in getsummaryparameterinjson"
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    print "json data : "+str(jsonData)
    for i in range(0, 4):
        print "in uploop : "+str(i)
        for keys, values in jsonData['data'][i].items():
            print "in inloop"
            jsonParameter.append(str(keys))
            print str(keys)
    for i in range(6, 9):
        print "in uploop : "+str(i)
        for keys, values in jsonData['data'][i].items():
            print "in inloop"
            jsonParameter.append(str(keys))
            print str(keys)
    jsonParameter.sort()
    #print jsonParameter
    return jsonParameter

def getBackupSummaryDaysParameterInJson(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for i in range(0, 3):
        for keys, values in jsonData['data'][i].items():
            jsonParameter.append(str(keys))
    jsonParameter.sort()
    #print jsonParameter
    return jsonParameter

def getRestoreSummaryDaysParameterInJson(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for i in range(0, 1):
        for keys, values in jsonData['data'][i].items():
            jsonParameter.append(str(keys))
    jsonParameter.sort()
    #print jsonParameter
    return jsonParameter

def getReplicationSummaryDaysParameterInJson(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for i in range(0, 1):
        for keys, values in jsonData['data'][i].items():
            jsonParameter.append(str(keys))
    jsonParameter.sort()
    #print jsonParameter
    return jsonParameter

def getArchiveSummaryDaysParameterInJson(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for i in range(0, 2):
        for keys, values in jsonData['data'][i].items():
            jsonParameter.append(str(keys))
    jsonParameter.sort()
    #print jsonParameter
    return jsonParameter

# Method to get virtual bridge information of a network
def getVirtualBridgeNetworkInformation(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    #print input_json
    jsonParameter = []
    jsonData = json.loads(input_json)
    for keys, values in jsonData['bridge'].items():
        jsonParameter.append(str(keys))
    jsonParameter.sort()
    #print jsonParameter
    return jsonParameter

def getDateTimeParametersInJson(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    #print input_json
    jsonParameter = []
    
    jsonData = json.loads(input_json)
    for key,value in jsonData.items():
        if(key != 'timestamp'):
            if(key == 'ntp'):
                jsonParameter.append(str(key))
                for k,v in value.items():
                    jsonParameter.append(str(k))
            else:
                jsonParameter.append(str(key))
    jsonParameter.sort()       
    #print jsonParameter
    return jsonParameter    

def getListOfArchiveParameterInJson(input_json, isList):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for key,value in jsonData.items():
        if(key != 'data' and key != 'timestamp'): 
            jsonParameter.append(str(key))
    for i in range(0, 1):
        for key,value in jsonData['data'][i].items():
            jsonParameter.append(str(key))
            if(key == 'archives'):
                for k,v in value.items():
                        jsonParameter.append(str(k))       
    jsonParameter.sort()
    return jsonParameter

def getListOfStorageParameterInJson(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for key,value in jsonData.items():
        if(key != 'timestamp'): 
            jsonParameter.append(str(key))
    for i in range(0, 1):
        for key,value in jsonData['data'][i].items():
            jsonParameter.append(str(key))
            if(key == 'alerts'):
                
                for i in range(0, 1):
                    for k,v in jsonData['data'][i]['alerts'][0].items():
                            jsonParameter.append(str(k))   
            if(key == 'size_history'):
                for i in range(0, 1):
                    for k,v in jsonData['data'][i]['size_history'][0].items():
                            jsonParameter.append(str(k)) 
    #print jsonParameter      
    jsonParameter.sort()
    return jsonParameter  

def getListOfStorageStatusParameterInJson(input_json):  
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for key,value in jsonData.items():
        if(key != 'timestamp'): 
            jsonParameter.append(str(key))
    for i in range(0, 1):
        for key,value in jsonData['data'][i].items():
            jsonParameter.append(str(key))
            if(key == 'alerts'):
                
                for i in range(0, 1):
                    for k,v in jsonData['data'][i]['alerts'][0].items():
                            jsonParameter.append(str(k))   
            if(key == 'size_history'):
                for i in range(0, 1):
                    for k,v in jsonData['data'][i]['size_history'][0].items():
                            jsonParameter.append(str(k))      
    jsonParameter.sort()
    return jsonParameter 

def getCredentialResponseParameterInJson(input_json):
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for key,value in jsonData['data'].items():
        jsonParameter.append(str(key))
    
    jsonParameter.sort()
    return jsonParameter

def postResponseParse(input_json):    
    
    responseData = json.loads(input_json)

    if (str(responseData[u'result'][0][u'code']) == u'0'):
        return "Pass"
    else:
        raise Exception ("POST request did not succeed")

def postResponseParseId_u(input_json):
    id=0
    responseData = json.loads(input_json)
    try:
        id = str(responseData['result']['id'])
    except:
        #print("Id not found")
        logHandler.logging.error("Id not found")
    return id

def postResponseParseId(input_json):
    id=0
    responseData = json.loads(input_json)
    try:
        id = str(responseData['result'][0]['id'])
    except:
        #print("Id not found")
        logHandler.logging.error("Id not found")
    return id

def postResponseParseId2(input_json):
    id=0
    responseData = json.loads(input_json)
    try:
        id = str(responseData['result']['id'])
    except:
        #print("Id not found")
        logHandler.logging.error("Id not found")
    return id

def postClientsResponseParse(input_json):
    responseData = json.loads(input_json)
    id = 0
    try:
        id=str(responseData['result']['id'])
    except:
        #print "POST request did not succeed"
        logHandler.logging.error("POST request did not succeed")
    return id

        
def deleteResponseParse(input_json):
    responseData = json.loads(input_json)
    if (str(responseData[u'result'][0][u'code']) == u'0'):
        return "Pass"
    else:
        raise Exception ("DELETE request did not succeed")
    
def putResponseParse(input_json):
    
    responseData = json.loads(input_json)
    
    if (str(responseData[u'result'][0][u'code']) == u'0'):
        return "Pass"
    else:
        raise Exception ("PUT request did not succeed")  

def getSpecificJobordersParametersInJson(input_json):
    responseData = json.loads(input_json)
    jsonParameter = []
    for key,value in responseData['data'][0].items():
        jsonParameter.append(str(key))
        if(key=='calendar'):
            for k,v in responseData['data'][0]['calendar'][0].items():
                jsonParameter.append(str(k))
    jsonParameter.sort()
    return jsonParameter

def getListOfAgentCheckSpecificClienParameterInJson(input_json):
    responseData = json.loads(input_json)
    jsonParameter = []
    if(len(responseData['clients'])): 
        for keys, values in responseData['clients'][0].items():
            jsonParameter.append(str(keys))
    else:
        #print "Data not present"
        logHandler.logging.info("Data not present")
    jsonParameter.sort()
    return jsonParameter


def getDateTimeTimezonesParametersInJson(input_json):
    responseData = json.loads(input_json)
    jsonParameter = []
    for key,value in responseData.items():
        if (key!='timestamp'):
            jsonParameter.append(str(key))
    return jsonParameter

def getInstanceParametersInJson(input_json, isList):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for key,value in jsonData.items():
        if(key != 'timestamp'): 
            jsonParameter.append(str(key))
        if(key=='catalog'):
            if(len(jsonData['catalog']) > 0):
                for k, v in jsonData['catalog'][0].items():
                    jsonParameter.append(str(k))
                    if(k=='instances'):  
                            for k1, v1 in jsonData['catalog'][0]['instances'][0].items():
                                jsonParameter.append(str(k1))
                                if(k1=='backups'):  
                                        for k2, v2 in jsonData['catalog'][0]['instances'][0]['backups'][0].items():
                                            jsonParameter.append(str(k2))
#                                             if(k2=='disks' and str(jsonData['catalog'][i]['instances'][j]['backups'][l]['disks'])!= 'null'): 
#                                                 for m in range(0,len(jsonData['catalog'][i]['instances'][j]['backups'][l])):  
#                                                     for k3, v3 in jsonData['catalog'][i]['instances'][j]['backups'][l]['disks'][m].items():
#                                                         jsonParameter.append(str(k3))
                                            
    #print jsonParameter      
    jsonParameter.sort()
    return jsonParameter

def getLicenseReqResponseParameterInJson(input_json, isList):
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for key,value in jsonData.items():
        if(key != 'timestamp'): 
            jsonParameter.append(str(key))
    for k, v in jsonData['request'].items():
        jsonParameter.append(str(k))
    for k, v in jsonData['request']['registration'].items():
        jsonParameter.append(str(k))
    #print jsonParameter      
    jsonParameter.sort()
    return jsonParameter


def getIdFromPostResponse(input_json, keyForId):
    responseData = json.loads(input_json)
    response_id = 0
    try:
        response_id=str(responseData['result'][keyForId])
    except:
        #print "POST request did not succeed"
        logHandler.logging.error("POST request did not succeed")
    return response_id


def getMailConfigParametersInJson(input_json):
    responseData = json.loads(input_json)
    mailconfigList = []
    for key in responseData['data']:
        if (key != 'timestamp'):
            mailconfigList.append(str(key))
    mailconfigList.sort()    
    return mailconfigList

def getInvalidLoginResponse(input_json):
    responseData = json.loads(input_json)
    invalidLoginResponse = []
    invalidLoginResponse.append(str(responseData['result'][0]['message']))
    return invalidLoginResponse

def getListOfVirtualClienParameterInJson(input_json):
    responseData = json.loads(input_json)
    jsonParameter = []
    for i in range(len(responseData['data'])): 
        for keys, values in responseData['data'][i].items():
            jsonParameter.append(str(keys))
            if(keys=='wir'):
                for j in range(len(responseData['data'][i]['wir'])):
                    for k, v in responseData['data'][i]['wir'][j].items():
                        jsonParameter.append(str(k))
                        if(keys=='wir'):
                            for l in range(len(responseData['data'][i]['wir'][j]['data'])):
                                for k, v in responseData['data'][i]['wir'][j]['data'][l].items():
                                    jsonParameter.append(str(k))
                                         
    else:
        #print "Data not present"
        logHandler.logging.info("Data not present")
    jsonParameter.sort()
    return jsonParameter


def getTrapHistoryParametersInJson(input_json):
    
    responseData = json.loads(input_json)
    jsonParameter = []
    for key in responseData['0']:
        jsonParameter.append(str(key))
    jsonParameter.sort()
    return jsonParameter

def getJobStatusParameterInJson(input_json, jid):
    status=''
    responseData = json.loads(input_json)
    for i in range(len(responseData['data'])):
        if(str(responseData['data'][i]['id'])==str(jid)):
            status = responseData['data'][i]['status']
    return status 

def getJobPercentCompleteParameterInJson(input_json, sid, jobId):
    percent=''
    responseData = json.loads(input_json)
    for i in range(len(responseData['data'])):
        if(str(responseData['data'][i]['system_id'])==str(sid) and str(responseData['data'][i]['id'])==str(jobId)):
            percent = responseData['data'][i]['percent_complete']
    
    if(percent >= 0):
        return percent
    else:
        return "Done"

def getListOfResponseParameters(input_json):
    jsonData = json.loads(input_json)     
    jsonParameter = []
    try:
        for key, value in jsonData.items():
            if(key != 'timestamp'):           
                if(isinstance(value, dict)):                                                
                    for k in value.items():  
                        jsonParameter.append(str(k[0]))           
                    break
                elif(isinstance(value, list)):
                    for k,v in value[0].items():  
                        if(isinstance(v, dict)):
                            for ke,va in v.items():
                                jsonParameter.append(str(ke[0]))
                        elif(isinstance(v, list)):
                            for ke,va in v[0].items():  
                                jsonParameter.append(str(ke))
                        jsonParameter.append(str(k))
                    break       
                else:     
                    jsonParameter.append(str(key))
    except (TypeError, AttributeError):        
            #print "Data not present"
            logHandler.logging.error("Data not present")
    except IndexError:
            #print "Data not present"
            logHandler.logging.error("Data not present")
    jsonParameter.sort()
    return jsonParameter  

def getSpecificBackupParametersInJson(input_json):
    responseData = json.loads(input_json)
    mailconfigList = []
    for key in responseData['BackupStatus'][0]:
        if (key != 'timestamp'):
            mailconfigList.append(str(key))
    mailconfigList.sort()    
    return mailconfigList

def getCapabilitiesParametersInJson(input_json):
    jsonData = json.loads(input_json)     
    jsonParameter = []
    for key in jsonData['data'][0]['capabilities']:
        jsonParameter.append(str(key))
    jsonParameter.sort()
    return jsonParameter
        
def getListOfVirtualClientByIDInJson(input_json):
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []     
    for key,value in jsonData.items():
        if(key != 'timestamp'): 
            jsonParameter.append(str(key))
        if(key=='real_client'):
            for i in range(0,len(jsonData['real_client'])):
                for k, v in jsonData['real_client'][i].items():
                    jsonParameter.append(str(k))
        if(key=='virtual_client'):
            #for ii in range(0,len(jsonData['virtual_client'])):
            for kk, vv in jsonData['virtual_client'].items():
                jsonParameter.append(str(kk)) 
    jsonParameter.sort()
    return jsonParameter   


def getpostBackupParameterInJson(input_json):
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    try:
        for k, v in jsonData['data'][0].items():
            jsonParameter.append(str(k))
        #print jsonParameter
    except (KeyError):
        #print "No backup for specified client"
        logHandler.logging.error("No backup for specified client")          
    jsonParameter.sort()
    return jsonParameter

def getListOfVirtualClienCandidateParameterInJson(input_json):
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    responseData = json.loads(input_json)
    jsonParameter = []
    for i in range(len(responseData['data'])): 
        for keys, values in responseData['data'][0].items():
            jsonParameter.append(str(keys))
            if(keys=='Candidates'):
                for j in range(len(responseData['data'][i]['Candidates'])):
                    for keys1, values1 in responseData['data'][0]['Candidates'][j].items():
                        jsonParameter.append(str(keys1))
    b = set(jsonParameter)
    #print b
    jsonParameter=list(b)
    jsonParameter.sort()
    return jsonParameter

def getAvailableDisksResponse(input_json):
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    responseData = json.loads(input_json)
    jsonParameter = []
    for i in range(len(responseData['attached_disks'])): 
        for keys, values in responseData['attached_disks'][0].items():
            jsonParameter.append(str(keys))
    jsonParameter.sort()
    return jsonParameter

def getListofTargetInformationISCSIParameterInJson(input_json):
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for key, value in jsonData.items():
        if(key != 'timestamp'):                            
            jsonParameter.append(str(key))      
    jsonParameter.sort()
    return jsonParameter

def getNetworkBridgeParametersInJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key, value in jsonData.items():
        if(key != 'timestamp'):                            
            jsonParameter.append(str(key))      
    jsonParameter.sort()
    return jsonParameter

def getArchiveFileParametersInJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key,value in jsonData.items():
        if(key != 'timestamp'):
            jsonParameter.append(str(key))
    for k,v in jsonData['data'][0].items():
        jsonParameter.append(str(k))
    jsonParameter.sort()
    return jsonParameter
    
def getArchiveStatusParametersInJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key,value in jsonData.items():
        if(key <> 'timestamp'):
            jsonParameter.append(str(key))
        if(key == 'status'):
            for k,v in jsonData['status'][0].items():
                jsonParameter.append(str(k))
    jsonParameter.sort()
    return jsonParameter

def getArchiveStatusValuesInJson(input_json):
    jsonData = json.loads(input_json)
    jsonVal = []
    for key,value in jsonData.items():
        if(key == 'status'):
            for i in range(0,len(jsonData['status'])):
                jsonVal.append(jsonData['status'][i]['archive_set_id'])
                jsonVal.append(jsonData['status'][i]['success'])
                #jsonVal.append(jsonData['status'][i]['is_imported'])
                #jsonVal.append(jsonData['status'][i]['client_id'])
                jsonVal.append(jsonData['status'][i]['instance_id'])
                jsonVal.append(str(jsonData['status'][i]['client_name']).replace("u'", ""))
                jsonVal.append(jsonData['status'][i]['os_type_id'])
                #jsonVal.append(jsonData['status'][i]['os_type'])
                jsonVal.append(jsonData['status'][i]['app_id'])
                #jsonVal.append(jsonData['status'][i]['app_name'])
                #jsonVal.append(jsonData['status'][i]['instance_description'])
                jsonVal.append(jsonData['status'][i]['orig_backup_id'])
                #jsonVal.append(jsonData['status'][i]['type'])
                jsonVal.append(jsonData['status'][i]['size'])
                jsonVal.append(jsonData['status'][i]['files'])
                jsonVal.append(jsonData['status'][i]['compressed'])
                jsonVal.append(jsonData['status'][i]['encrypted'])
                jsonVal.append(jsonData['status'][i]['deduped'])
                #jsonVal.append(jsonData['status'][i]['fastseed'])
                jsonVal.append(jsonData['status'][i]['vmware_template'])
                jsonVal.append(jsonData['status'][i]['id'])
                jsonVal.append(jsonData['status'][i]['set_id'])
                #jsonVal.append(jsonData['status'][i]['date'])
                #jsonVal.append(jsonData['status'][i]['elapsed'])
                #jsonVal.append(jsonData['status'][i]['backup_date'])
                #jsonVal.append(jsonData['status'][i]['storage'])
    for i in range(0,len(jsonVal)):
        jsonVal[i] = str(jsonVal[i])
    return jsonVal

def getArchiveSearchParametersInJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key,value in jsonData.items():
        if(key <> 'timestamp'):
            jsonParameter.append(str(key))
        if(key == 'files'):
            for k,v in jsonData['files'][0].items():
                jsonParameter.append(str(k))
    jsonParameter.sort()
    return jsonParameter

def putArchiveResponseParse(input_json):    
    responseData = json.loads(input_json)    
    if (str(responseData[u'result'][0][u'id']) <> u'0'):
        return str(responseData[u'result'][0][u'id'])
    else:
        raise Exception ("PUT request did not succeed") 
    
def putArchiveCheckParametersInJson(input_json):    
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key, value in jsonData.items():
        if(key <> 'timestamp'):
            jsonParameter.append(str(key))
        if(key == 'result'):
            for k,v in jsonData['result'][0].items():
                jsonParameter.append(str(k))
    jsonParameter.sort()
    return jsonParameter

def postArchiveCatalogParametersInJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key,value in jsonData.items():
        if(key <> 'timestamp'):
            jsonParameter.append(str(key))
    jsonParameter.sort()
    return jsonParameter

def getListOfArchiveSetsInJson(input_json):
    """Get response parameters for archive sets from Json response"""
    
    responseData = json.loads(input_json)
    jsonParameter = []
    if(len(responseData['sets'])): 
        for key,value in responseData['sets'][0].items():
            jsonParameter.append(str(key))      
        #print jsonParameter
    else:
        #print "Data not present"
        logHandler.logging.info("Data not present")
    jsonParameter.sort()
    return jsonParameter

def getArchiveSetValuesInJson(input_json):
    """Get values for verification of archive set list from json response"""
    jsonData = json.loads(input_json)
    jsonVal = []
    for key,value in jsonData.items():
        if(key == 'sets'):
            for i in range(0,len(jsonData['sets'])):
                jsonVal.append(jsonData['sets'][i]['id'])
                jsonVal.append(jsonData['sets'][i]['date'])     
                #not checking available parameter because it is documented that it can give false positives        
                #jsonVal.append(jsonData['sets'][i]['available'])
                jsonVal.append(jsonData['sets'][i]['sid'])     
                statusval = jsonData['sets'][i]['status']                 
                if statusval.find('failed') <> -1:
                    jsonVal.append('archive failed')
                else:
                    jsonVal.append(statusval)            
                   
                jsonVal.append(jsonData['sets'][i]['system_name'])
    for i in range(0,len(jsonVal)):
        jsonVal[i] = str(jsonVal[i])
    return jsonVal


""" Below method returns the "messages" field from the Post Archive Catalog response """
def getPostArchiveCatalogResponseFromJson(input_json):
    jsonData = json.loads(input_json)
    jsonVal = []
    for key, value in jsonData.items():
        if(key == 'messages'):
            jsonVal.append(jsonData['messages'])
    return jsonVal

"""Below method returns the list of response parameters from actual response of Get Archive Media Candidates API"""
def getMediaCandidatesParametersInJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key,value in jsonData.items():
        if(key <> 'timestamp'):
            jsonParameter.append(str(key))
        if(key == 'data'):
            for k,v in jsonData['data'][0].items():
                jsonParameter.append(str(k))
    jsonParameter.sort()
    return jsonParameter

"""Below method returns the response from json"""
def getMediaCandidatesResponseFromJson(input_json):
    jsonData = json.loads(input_json)
    jsonVal =[]
    for key,value in jsonData.items():
        if(key == 'data'):
            for i in range(0,len(jsonData['data'])):
                jsonVal.append(jsonData['data'][i]['name'])
                jsonVal.append(jsonData['data'][i]['type'])
    return jsonVal

def postStorageParamatersInJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key,value in jsonData.items():
        if(key <> 'timestamp'):
            jsonParameter.append(str(key))
        if(key == 'storage'):
            for k,v in jsonData['storage'].items():
                jsonParameter.append(str(k))
    jsonParameter.sort()
    return jsonParameter


def postStorageResponseFromJson(input_json):
    jsonData = json.loads(input_json)
    jsonVal = 0
    for key,value in jsonData.items():
        if(key == 'result'):
            for k,v in jsonData['result'][0].items():
                if(k == 'id'):
                    jsonVal = jsonData['result'][0]['id']
    return jsonVal 

def getCatalogByInstanceParametersInJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key,value in jsonData.items():
        if(key <> 'timestamp'):
            jsonParameter.append(str(key))
            if(key == 'catalog'):
                if(len(jsonData['catalog']) > 0):
                    for k1,v1 in jsonData['catalog'][0].items():
                        jsonParameter.append(str(k1))
                        if(k1 == 'archives'):
                            for k2,v2 in jsonData['catalog'][0]['archives'][0].items():
                                jsonParameter.append("arch_" + str(k2))
                else:
                    logHandler.logging.info("Data not present")
    jsonParameter.sort()
    return jsonParameter

def getCatalogByInstanceResponseFromJson(input_json):
    jsonData = json.loads(input_json)
    jsonVal = []
    for key,value in jsonData.items():
        if(key == 'view'):
            jsonVal.append(value)
        if(key == 'catalog'):
            for i in range(0,len(jsonData['catalog'])):
                #jsonVal.append(jsonData['catalog'][i]['app_name'])
                jsonVal.append(jsonData['catalog'][i]['client_id'])
                jsonVal.append(jsonData['catalog'][i]['client_name'])
                jsonVal.append(jsonData['catalog'][i]['database_name'])
                jsonVal.append(jsonData['catalog'][i]['instance_id'])
                #jsonVal.append(jsonData['catalog'][i]['instance_name'])
                #jsonVal.append(jsonData['catalog'][i]['system_id'])
                #jsonVal.append(jsonData['catalog'][i]['system_name'])
                
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['success'])
                #jsonVal.append(jsonData['catalog'][i]['archives'][0]['is_imported'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['client_id'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['instance_id'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['client_name'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['os_type_id'])
                #jsonVal.append(jsonData['catalog'][i]['archives'][0]['os_type'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['app_id'])
                #jsonVal.append(jsonData['catalog'][i]['archives'][0]['app_name'])
                #jsonVal.append(jsonData['catalog'][i]['archives'][0]['instance_description'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['orig_backup_id'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['type'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['size'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['files'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['compressed'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['encrypted'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['deduped'])
                #jsonVal.append(jsonData['catalog'][i]['archives'][0]['fastseed'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['vmware_template'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['id'])
                jsonVal.append(jsonData['catalog'][i]['archives'][0]['set_id'])
                #jsonVal.append(jsonData['catalog'][i]['archives'][0]['elapsed'])
                #jsonVal.append(jsonData['catalog'][i]['archives'][0]['start_date'])
                #jsonVal.append(jsonData['catalog'][i]['archives'][0]['archive_start_date'])
                #jsonVal.append(jsonData['catalog'][i]['archives'][0]['error_string'])                
    jsonVal.sort()
    return jsonVal        

def getStorageParametersFromJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for key,value in jsonData.items():
        if(key <> 'timestamp'):
            jsonParameter.append(str(key))                                 
        if(key == 'storage'):
            for i in range(0, len(jsonData['storage'])):
                for k,v in jsonData['storage'][i].items():
                    if(k == 'type' and jsonData['storage'][i][k] == 'internal'):
                        pass      
                    if(k == 'type' and jsonData['storage'][i][k] == 'added_internal'):
                        for k1,v1 in jsonData['storage'][i]['properties'].items():
                            jsonParameter.append(str(k1))
                    if(k == 'type' and jsonData['storage'][i][k] == 'nas'):
                        for k1,v1 in jsonData['storage'][i]['properties'].items():
                            jsonParameter.append(str(k1))
                    if(k == 'alerts'):
                        for k1,v1 in jsonData['storage'][i]['alerts'][0].items():
                            jsonParameter.append(str(k1))
                    if(k == 'storage_history'):
                        jsonParameter.append(str(k))
                        for k1,v1 in jsonData['storage'][i]['storage_history'][0].items():
                            jsonParameter.append(str(k1))
                    else:
                        jsonParameter.append(str(k))
                    
        if(key == 'totals'):
            for k,v in jsonData['totals'].items():
                jsonParameter.append(str(k))
    jsonParameter.sort()
    return jsonParameter

def getStorageTypeFromJson(input_json):
    jsonData = json.loads(input_json)
    storageType = []
    for i in range(0,len(jsonData['storage'])):
        for k,v in jsonData['storage'][i].items():
            if(k == 'type'):
                storageType.append(jsonData['storage'][i][k])
    return storageType
            
def getArchiveSetByIdParamtersInJson(input_json):
    """Get response parameters for archive set by Id from Json response"""
    responseData = json.loads(input_json)
    jsonParameter = []
    jsonProfileParamter = []
    jsonProfileOptionsParameter= []
    jsonProfileInstancesParamter = []    
    if(len(responseData['sets'])): 
        for key,value in responseData['sets'][0].items():
            jsonParameter.append(str(key))  
            #get parameters from profile
            if((key == 'profile') and isinstance(value, dict)):
                for k in value.items():  
                    jsonProfileParamter.append(str(k[0]))  
                    if((k[0] == 'options') and isinstance(value, dict)):
                        for k1 in value['options']:  
                            jsonProfileOptionsParameter.append(str(k1))  
                        jsonProfileOptionsParameter.sort()     #                        
                    if(k[0] == 'instances'):
                        for k2,v2 in value['instances'][0].items():
                            jsonProfileInstancesParamter.append(str(k2))
                        jsonProfileInstancesParamter.sort()                      
                jsonProfileParamter.sort()            
    else:
        logHandler.logging.info("Data not present")
    jsonParameter.sort()    
    return jsonParameter, jsonProfileParamter, jsonProfileOptionsParameter,jsonProfileInstancesParamter

def ifSecondaryNameParameterExists(actualResponse):
    """Returns true or false if secondary_name parameter exists in json response"""
    responseData = json.loads(actualResponse)  
    flag = False
    if(len(responseData['sets'])): 
        for key,value in responseData['sets'][0].items():            
            #get parameters from profile
            if((key == 'profile') and isinstance(value, dict)):
                for k in value.items():                                                     
                    if(k[0] == 'instances'):
                        for k2,v2 in value['instances'][0].items():
                            if(str(k2) == 'secondary_name'):
                                flag = True
    else:        
        logHandler.logging.info("Data not present")   
    return flag

def getArchiveSetByIdValuesInJson(input_json):
    """Get values for verification of archive set list from json response"""
    jsonData = json.loads(input_json)
    jsonVal = []
    jsonProfileData = []
    jsonProfileInstanceData = []
    jsonProfileOptionsData = []
    for key,value in jsonData.items():
        if(key == 'sets'):
            jsonVal.append(jsonData['sets'][0]['status'])
            #not checking available parameter because it is documented that it can give false positives        
            #jsonVal.append(jsonData['sets'][i]['available'])
            #jsonVal.append(jsonData['sets'][0]['is_imported'])  
            jsonVal.append(jsonData['sets'][0]['media_serials'])
            jsonVal.append(jsonData['sets'][0]['media_label'])    
            jsonVal.append(jsonData['sets'][0]['originating_asset'])            
            jsonVal.append(jsonData['sets'][0]['id'])
            jsonVal.append(jsonData['sets'][0]['elapsed_time'])
            jsonVal.append(jsonData['sets'][0]['date'])   
            
            jsonProfileData.append(jsonData['sets'][0]['profile']['description'])
            jsonProfileData.append(jsonData['sets'][0]['profile']['target'])
#             jsonProfileData.append(jsonData['sets'][0]['profile']['clients'])
#             jsonProfileData.append(jsonData['sets'][0]['profile']['localdirs'])
#             jsonProfileData.append(jsonData['sets'][0]['profile']['types'])
#             jsonProfileData.append(jsonData['sets'][0]['profile']['start_date'])                        
#             jsonProfileData.append(jsonData['sets'][0]['profile']['end_date'])        
                        
            jsonProfileOptionsData.append(jsonData['sets'][0]['profile']['options']['append'])
            jsonProfileOptionsData.append(jsonData['sets'][0]['profile']['options']['compress'])
            jsonProfileOptionsData.append(jsonData['sets'][0]['profile']['options']['dedup'])
            jsonProfileOptionsData.append(jsonData['sets'][0]['profile']['options']['email_report'])
            jsonProfileOptionsData.append(jsonData['sets'][0]['profile']['options']['encrypt'])
            jsonProfileOptionsData.append(jsonData['sets'][0]['profile']['options']['fastseed'])
            jsonProfileOptionsData.append(jsonData['sets'][0]['profile']['options']['purge'])
            jsonProfileOptionsData.append(jsonData['sets'][0]['profile']['options']['retention_days'])                

            for j in range (0, len(jsonData['sets'][0]['profile']['instances'])):
                jsonProfileInstanceData.append(jsonData['sets'][0]['profile']['instances'][j]['id'])
               # jsonProfileInstanceData.append(jsonData['sets'][0]['profile']['instances'][j]['primary_name'])
                if 'secondary_name' in (jsonData['sets'][0]['profile']['instances'][j]).keys():
                    jsonProfileInstanceData.append(jsonData['sets'][0]['profile']['instances'][j]['secondary_name'])
                else:
                    jsonProfileInstanceData.append('None')
                                                               
    for i in range(0,len(jsonVal)):
        jsonVal[i] = str(jsonVal[i])
    for i in range(0,len(jsonProfileData)):
        jsonProfileData[i] = str(jsonProfileData[i])
    for i in range(0,len(jsonProfileOptionsData)):
        jsonProfileOptionsData[i] = str(jsonProfileOptionsData[i])
    for i in range(0,len(jsonProfileInstanceData)):
        jsonProfileInstanceData[i] = str(jsonProfileInstanceData[i])
    return jsonVal,jsonProfileData, jsonProfileOptionsData, jsonProfileInstanceData

def getIdFromJson(input_json):
    jsonData = json.loads(input_json)
    id = jsonData['id']
    return id

def putMountMediaResponseFromJson(input_json):
    """Parses Json response for Put Mount Media"""
    jsonData = json.loads(input_json)
    jsonVal = []
    for key,value in jsonData.items():
        if(key == 'result'):
            for k,v in jsonData['result'][0].items():
                jsonVal.append(k)
                jsonVal.append(v)
#                 if(k == 'code'):
#                     jsonVal = jsonData['result'][0]['code']
    return jsonVal 

def getInventorySyncInfoFromJson(input_json):
    jsonData = json.loads(input_json)
    syncInfo = []
    for key,value in jsonData.items():
        if(key == 'sync_running'):
            syncInfo.append(jsonData[key])
        if(key == 'sync_progress'):
            syncInfo.append(jsonData[key])
        if(key == 'sync_status'):
            syncInfo.append(jsonData[key])
        if(key == 'message'):
            syncInfo.append(jsonData[key])
    return syncInfo

def deleteResponseMessage(input_json):
    responseData = json.loads(input_json)
    errorMessage = (str(responseData[u'result'][0][u'message']))
    return errorMessage

def getInventoryInfoFromJson(input_json):
    jsonData = json.loads(input_json)
    inventoryInfo = []
    if(len(jsonData['inventory'][0]) > 0):
        inventoryInfo.append(jsonData['inventory'][0]['id'])
        inventoryInfo.append(jsonData['inventory'][0]['name'])
        for key,value in jsonData['inventory'][0].items():
            if (key == 'nodes' and len(jsonData['inventory'][0]['nodes']) > 0):
                tempJson = jsonData['inventory'][0]['nodes']
                len1 = len(tempJson)
                for i in range(0,len1):
                    inventoryInfo.append(tempJson[i]['id'])
                    inventoryInfo.append(tempJson[i]['name'])
                    len2 = len(tempJson[i]['nodes'])
                    for j in range(0,len2):
                        inventoryInfo.append(tempJson[i]['nodes'][j]['id'])
                        inventoryInfo.append(tempJson[i]['nodes'][j]['name'])
                        tempJson1 = tempJson[i]['nodes'][j]['nodes']
                        len3 = len(tempJson1)
                        for k in range(0,len3):
                            inventoryInfo.append(tempJson1[k]['id'])
                            inventoryInfo.append(tempJson1[k]['name'])
    inventoryInfo.sort()
    return inventoryInfo


def PostResponseMessage(input_json):
    responseData = json.loads(input_json)
    errorMessage = (str(responseData[u'result'][0][u'message']))
    return errorMessage

def getTrapsResponseFromJson(input_json):
    jsonData = json.loads(input_json)
    jsonVal = []
    for key,value in jsonData['0'].items():
        jsonVal.append(value)
    jsonVal.sort()
    return jsonVal

def getActiveJobParametersFromJson(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    if(len(jsonData['data']) > 0):
        for k,v in jsonData['data'][0].items():
            jsonParameter.append(k)
    else:
        logHandler.logging.info("Data not present")
    jsonParameter.sort()
    return jsonParameter

def getUsedAndLicensedResourceParam(input_json):
    jsonData = json.loads(input_json)
    jsonParameter = []
    for i in range(0,len(jsonData['license'])):
            for k,v in jsonData['license'][i].items():
                jsonParameter.append(str(k))  
                if(k=="resources"):
                    for key, value in jsonData['license'][i]["resources"].items():
                        jsonParameter.append(str(key))
                        if(key=="limits"):
                            for key1, value1 in jsonData['license'][i]["resources"]["limits"].items():
                                jsonParameter.append(str(key1))
                        if(key=="usage"):
                            for key2, value2 in jsonData['license'][i]["resources"]["usage"].items():
                                jsonParameter.append(str(key2))
    for param in jsonData:
        if(param != 'timestamp'):
            jsonParameter.append(str(param))
    jsonParameter.sort()
    print jsonParameter
    return jsonParameter

def getListOfParameterInJson_updates(input_json):
         
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
            
    jsonParameter = []

    try:
        for key, value in jsonData.items():
            if(key != 'timestamp'):           
                if(isinstance(value, dict)):                                                
                    for k in value.items():  
                        jsonParameter.append(str(k[0]))           
                    break
            for keys, values in jsonData['updates']['updates'][0].items():
                jsonParameter.append(str(keys))
    
    
    except (TypeError,AttributeError):        
            #print "Data not present"
            logHandler.logging.error("Data not present")
    except IndexError:
            #print "Data not present"
            logHandler.logging.error("Data not present")
    jsonParameter.sort()
    return jsonParameter
        
def getListOfClientIdsTypes(input_json):
    
    jsondata=json.loads(input_json)
    clientlist=[]
    countclients=len(jsondata['data'])
    for i in range(0,countclients):
        if(jsondata['data'][i]['os_family']):
            inlist=[]
            inlist.append(jsondata['data'][i]['id'])
            inlist.append(jsondata['data'][i]['os_family'])
            clientlist.append(inlist)
    return clientlist

def getAllParametersForSystem(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.              
    returnlist=[]
    for key, value in jsonData['appliance'][0].items():
        returnlist.append(key)                            
    returnlist.sort()
    return returnlist   

def getListOfSystemIds(input_json):
    
    jsondata=json.loads(input_json)
    systemlist=[]
    for i in range(0,len(jsondata['appliance'])):
        if(jsondata['appliance'][i]['id']):
            systemlist.append(jsondata['appliance'][i]['id'])
    return systemlist 
        

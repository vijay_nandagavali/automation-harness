from config import apiconfig
from util import logHandler
import pika  
import uuid
import time

class messagesender:
    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body
    
    
    def sendMessage(self, routingKey, messageToSend):
        logHandler.logging.info("Sending message to: "+routingKey)
        try :
            routingKey = routingKey
            messageWaitCount = 3600
            waitCount = 0
            self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=apiconfig.rabbitmqServer))
            self.channel = self.connection.channel()  
            result = self.channel.queue_declare(exclusive=True)
            self.callback_queue = result.method.queue
                
            self.channel.basic_consume(self.on_response, queue=self.callback_queue, no_ack=True)
                
            print routingKey
            self.response = None
            self.corr_id = str(uuid.uuid4())
            self.channel.basic_publish(exchange=apiconfig.rabbitmqExchangeName,
                                       routing_key=routingKey,
                                       properties=pika.BasicProperties(
                                             reply_to = self.callback_queue,
                                             correlation_id = self.corr_id,
                                             ),
                                       body=str(messageToSend))
                
            print " [x] Sent agent information %r:%r" % (apiconfig.serverbindingkey, messageToSend)
            logHandler.logging.info(" [x] Sent agent information %r:%r" % (apiconfig.serverbindingkey, messageToSend))       
                    
            while self.response is None:
                waitCount += 1
                self.connection.process_data_events()
                if (self.response is None):
                    if (waitCount > messageWaitCount):
                        break
                    else:
                        time.sleep(1)
            
            logHandler.logging.info("Finished sendMessage()\n")
            if (self.response == "true"):
                return True
            elif(self.response <> 'false'):
                return self.response
            else:
                return False
                  
        except Exception as e:
            logHandler.logging.error(e.getMessage())
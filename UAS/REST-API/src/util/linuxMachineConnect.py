'''
Created on Oct 20, 2014

@author: Amey.k
'''

import paramiko
from config import apiconfig

def SshRemoteMachineConnection(applaince_ip, username = apiconfig.machineuser, password=apiconfig.machinepassword):
    ssh=paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.load_system_host_keys()
    ssh.connect(hostname=applaince_ip, username=apiconfig.machineuser ,password=apiconfig.machinepassword)
    return ssh

    

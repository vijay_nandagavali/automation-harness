'''
Created on Oct 1, 2014

@author: Amey.k
'''
from config import Constants
from util import apiHelper
from util import jsonHandler 
from testdata import clients_testdata, virtual_clients_testdata
from __builtin__ import str

clientsAPIUrl = "/api/virtual_clients" 


def getListOfAllVirtualClients(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl, token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfVirtualClienParameterInJson(actualResponse)

def getDetailsOfVirtualClientByID(token, virtualclientID):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl + "/" + str(virtualclientID) + "/details/?type=wir&sid=" + virtual_clients_testdata.sid , token)

def getListOfActualResponseParameterVirtualClientByID(actualResponse):
    return jsonHandler.getListOfVirtualClientByIDInJson(actualResponse)

def postVirtualClient(token, ClientID, Processors, Memory, efi, Disks, Include, Exclude):
    jsondata = {}
    jsondata["client"]={"ClientID":ClientID,"Processors":Processors,"Memory": Memory,"efi":efi,"Disks":Disks,"Include":Include,"Exclude":Exclude}
#     jsondata["ClientID"] = ClientID
#     jsondata["Processors"] = Processors
#     jsondata["Memory"] = Memory
#     jsondata["efi"] = efi
#     jsondata["Disks"] = Disks
#     jsondata["Include"] = Include
#     jsondata["Exclude"] = Exclude
    print Constants.ENVIRONMENT_URL + clientsAPIUrl + "/?sid=" + virtual_clients_testdata.sid
    print jsondata
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl + "/?sid=" + virtual_clients_testdata.sid, jsondata, token)

def postClient(token, sid, name, os_type, priority, is_enabled, is_synchable, is_encrypted, is_auth_enabled, use_ssl, 
        defaultschedule, port, ip, existing_credential):
        jsondata = {}
        jsondata["os_type"] = os_type
        jsondata["name"] = name
        jsondata["priority"] = priority
        jsondata["is_enabled"] = is_enabled
        jsondata["is_synchable"] = is_synchable
        jsondata["is_encrypted"] = is_encrypted
        jsondata["is_auth_enabled"] = is_auth_enabled
        jsondata["use_ssl"] = use_ssl
        jsondata["defaultschedule"] = defaultschedule
        jsondata["port"] = port
        jsondata["host_info"] = {"ip": ip}
        jsondata["existing_credential"] = existing_credential
        print jsondata
        
        return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl , jsondata, token)

def deleteVirtualClient(token,vid):
    #print Constants.ENVIRONMENT_URL + clientsAPIUrl + "/" + str(vid) + "/?sid=" + virtual_clients_testdata.sid
    jsondata = {}
    jsondata["type"] = virtual_clients_testdata.type
    jsondata["deleteFromHypervisor"] = virtual_clients_testdata.deleteFromHypervisor    
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + clientsAPIUrl + "/" + str(vid) + "/?sid=" + virtual_clients_testdata.sid, token, jsondata)

def getListOfAllVirtualClientCandidates(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl + "/candidates", token)

def getListOfActualResponseCandidateParameter(actualResponse):
    return jsonHandler.getListOfVirtualClienCandidateParameterInJson(actualResponse)
'''
Created on Sep 11, 2014

@author: Amey.k
'''

from config import Constants
from util import apiHelper, jsonHandler

usersAPIUrl = "/api/users" 

def getAllUsers(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + usersAPIUrl,token)

def getUsersResponseParameters(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getParticularUser(userID,token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + usersAPIUrl + "/" +userID,token)

def updateExistingUser(token,currentPassword,newPassword,userID):
    params = {'current_password':currentPassword,'password':newPassword}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + usersAPIUrl + "/" +userID, params, token)

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)
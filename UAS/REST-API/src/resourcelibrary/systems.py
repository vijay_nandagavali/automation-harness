'''
Created on Nov 26, 2014

@author: Amey.k
'''

from config import Constants,apiconfig
from util import apiHelper
from util import jsonHandler 
from testdata import systems_testdata

systemsAPIUrl = "/api/systems" 

def getAllSystemsInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + systemsAPIUrl , token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getActualResponseParameter(actualResponse):
    return jsonHandler.getAllParametersForSystem(actualResponse)

def getListOfSids(actualResponse):
    return jsonHandler.getListOfSystemIds(actualResponse)

def getSpecificSystemInformation(token,systemId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + systemsAPIUrl + "/?sid=" +systemId , token)

def getSystemDetails(token,systemId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + systemsAPIUrl + "/details/?sid=" +systemId,token)

def updateSystemName(token,systemId,systemName):
    params = { 'id' : int(systemId),'name':systemName}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + systemsAPIUrl, params, token)

def putResponseParse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def getPostResponseParseId(actualResponse):
    return jsonHandler.postResponseParseId(actualResponse)

def getPostResponseParseId2(actualResponse):
    return jsonHandler.postResponseParseId2(actualResponse)

def postMakeTarget(token,network,mask,port):
    params = { 
              "network" : network,
              "mask" : mask,
              "port" : port
             }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL_TARGET +systemsAPIUrl +"/make_target", params, token)

def postResponseParser(actualResponse):
    return jsonHandler.postResponseParse(actualResponse)

def putPromoteSysRole(token, sysID):
    params= {
             "id" : str(sysID),
             "credentials": {
                             "username": str(apiconfig.machineuser),
                             "password": str(apiconfig.machinepassword)
                             }
            }
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL_TARGET + systemsAPIUrl + "/add-management", params, token)

def deleteTargetSystem(token,sysid):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL_TARGET+systemsAPIUrl + "/" + str(sysid), token)

def deleteSystem(token,sysid):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL+systemsAPIUrl + "/" + str(sysid), token)

def postAddSystem(token,sysName,sysIP):
    params = {
              "name":str(sysName),
              "ip":str(sysIP),
              "host":str(sysName),
              "credentials": {
                             "username": str(apiconfig.machineuser),
                             "password": str(apiconfig.machinepassword)
                             }
              }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + systemsAPIUrl +"/sid="+ str(systems_testdata.systemId), params, token)

def postAddSystem2(token,sysName,sysIP):
    params = {
              "name":str(sysName),
              "ip":str(sysIP),
              "host":str(sysName),
              "credentials": {
                             "username": str(apiconfig.machineuser),
                             "password": str(apiconfig.machinepassword)
                             }
              }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + systemsAPIUrl, params, token)
'''
Created on Oct 20, 2014

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 
from testdata import license_testdata


licenseAPIUrl = "/api/license"


def getLicenseInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + licenseAPIUrl + "/?sid=" + license_testdata.sid, token) 

def getLicenseRequestInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + licenseAPIUrl + "/request/?sid=" + license_testdata.sid, token) 

def getLicenseSummaryInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + licenseAPIUrl + "/summary/?sid=" + license_testdata.sid, token) 

def getUsedAndLicensedResources(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + licenseAPIUrl + "/resources/?sid=" + license_testdata.sid, token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, False)

def getLicenseResponseParameter(actualResponse):
    return jsonHandler.getListOfJobParameterInJson(actualResponse, False)

def getLicenseReqResponseParameter(actualResponse):
    return jsonHandler.getLicenseReqResponseParameterInJson(actualResponse, False)

def getListOfActualResponseParameterUsedAndLiscResource(actualResponse):
    return jsonHandler.getUsedAndLicensedResourceParam(actualResponse)    
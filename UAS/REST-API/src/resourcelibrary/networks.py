'''
Created on Oct 21, 2014

@author: Amey.k

'''

from config import Constants
from util import apiHelper
from util import jsonHandler 

networksAPIUrl = "/api/networks" 

def getNetworkInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + networksAPIUrl, token)

def getNetworkListParametersinJson(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True) 
    
def getParticularNetworkInformation(networkId, token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + networksAPIUrl + "/" + networkId, token)

def getVirtualBridgeInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + networksAPIUrl + "/virtual_bridge", token)

def getNetworkBridgeInformation(token,systemId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + networksAPIUrl + "/bridge/sid=" +systemId, token)

def getNetworkNicBridgeInformation(token,systemId,nic):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + networksAPIUrl + "/bridge/" +nic+ "/sid=" +systemId, token)

def getNetworkNicBridgeParametersInJson(actualResponse):
    return jsonHandler.getNetworkBridgeParametersInJson(actualResponse)

def gefVirtualBridgeNetworkParametersInJson(actualResponse):
    return jsonHandler.getVirtualBridgeNetworkInformation(actualResponse)

def getNetworkBridgeParametersInJson(actualResponse):
    return jsonHandler.getNetworkBridgeParametersInJson(actualResponse)

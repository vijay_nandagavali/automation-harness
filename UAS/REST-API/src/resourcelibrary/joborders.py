'''
Created on Feb 6, 2015

@author: Amey.k
'''
import time
from config import Constants
from util import apiHelper, logHandler
from util import jsonHandler 
from testdata import joborders_testdata

jobordersAPIUrl = "/api/joborders"
jobordersDisableUrl = "/api/joborders/disable"
jobordersEnableURL = "/api/joborders/enable"
jobordersCopyUrl = "/api/joborders/copy/"
jobordersRunUrl = "/api/joborders/run/"

def getAllJoborders(token):
    #logHandler.logging.info("Get all joborders URL: " + Constants.ENVIRONMENT_URL + jobordersAPIUrl)
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + jobordersAPIUrl, token)  

def getJobordersParametersInJson(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def disableJobOrder(token,joid):
    #print Constants.ENVIRONMENT_URL + jobordersDisableUrl + "/" + joid
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + jobordersDisableUrl + "/" + joid, "", token)

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def getPostRequestResponse(actualResponse):
    return jsonHandler.postResponseParseId(actualResponse)

def getSpecificJoborders(token, joid):
    #logHandler.logging.info("Get specific joborder URL: " + Constants.ENVIRONMENT_URL + jobordersAPIUrl + "/" + str(joid))
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + jobordersAPIUrl + "/" + str(joid) , token)

def getSpecificJobordersParametersInJson(actualResponse):
    return jsonHandler.getSpecificJobordersParametersInJson(actualResponse)

def deleteJoborders(token, joid):
    #print Constants.ENVIRONMENT_URL + jobordersAPIUrl + "/" + str(joid)
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + jobordersAPIUrl + "/" + str(joid) , token)

def deleteJobordersAtSource(token, joid):
    #print Constants.ENVIRONMENT_URL + jobordersAPIUrl + "/" + str(joid)
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL_SOURCE + jobordersAPIUrl + "/" + str(joid) , token)


def getDeleteRequestResponse(actualResponse):
    return jsonHandler.deleteResponseParse(actualResponse)

def enableJobOrder(token,joid):
    #print Constants.ENVIRONMENT_URL + jobordersEnableURL + "/" + joid
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + jobordersEnableURL  + "/" + joid, "", token)

def createNewJoborder(token,name,type,start_date,start_time,backup_type,storage,email_addresses,run_on,instances):
    json_data = {
         "name": name, 
         "type": type, 
         "calendar": [ 
             { 
                 "run_on": run_on, 
                 "schedule_run": "0", 
                 "start_date": start_date, 
                 "start_time": start_time + ":00", 
                 "backup_type": backup_type
             } 
          ], 
          "storage": "Internal", 
          "instances": instances, 
           "verify": "inline", 
           "email_report": False, 
           "failure_report": True, 
           "legal_hold_backups": False, 
           "email_addresses": ""
    } 
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + jobordersAPIUrl, json_data, token)


def createJobOrderCopy(token,joid):
    #print Constants.ENVIRONMENT_URL + jobordersCopyUrl + joid
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + jobordersCopyUrl + joid, "", token)

def runJobOrder(token, joid):
    #print Constants.ENVIRONMENT_URL + jobordersRunUrl + str(joid)
    #logHandler.logging.info("Run Job Order API URL: " + str(Constants.ENVIRONMENT_URL + jobordersRunUrl + str(joid)))
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + jobordersRunUrl + str(joid), "", token)
        
def parseErrorResponse(actualResponse):
    return jsonHandler.getDictionaryFromJson(actualResponse)     

def createNewArchiveJoborder(token,name,type,start_date,start_time,backup_type,storage,email_addresses,run_on,instances):
    json_data = {
         "name": name, 
         "type": type, 
         "calendar": [ 
             { 
                 "run_on": run_on, 
                 "schedule_run": "0", 
                 "start_date": start_date, 
                 "start_time": start_time + ":00", 
                 "backup_type": backup_type
             } 
          ], 
          "storage": storage, 
          "types":["Full"],
          "instances": instances, 
           "verify": "inline", 
           "email_report": False, 
           "failure_report": True, 
           "legal_hold_backups": False, 
           "email_addresses": ""
    } 
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + jobordersAPIUrl, json_data, token)

def createNewReplicationJoborder(token,instances,targetid,name,type):
    json_data = {
                 "instances":instances,
                 "target_id":targetid,
                 "name":str(name),
                 "type":str(type)
                 }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + jobordersAPIUrl, json_data, token)

def createNewReplicationJoborderAtSource(token,instances,targetid,name,type):
    json_data = {
                 "instances":instances,
                 "target_id":targetid,
                 "name":str(name),
                 "type":str(type)
                 }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL_SOURCE + jobordersAPIUrl, json_data, token)
 
       

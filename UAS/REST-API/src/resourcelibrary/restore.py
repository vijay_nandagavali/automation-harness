'''
Created on Mar 9, 2015

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler
from difflib import restore

restoreAPIUrl = "/api/restore"

def putHypervRestore(appliance_ip, token,backupId,name,cid):
    print "into hyperv restore function"
    params = {'backup_id':backupId,'target':{'name':name}, 'client_id':cid}
    return apiHelper.sendPOSTRequest("https://" + appliance_ip + restoreAPIUrl + "/full/?sid=1", params, token)

def createHypervFileLevelRestore(token,backupId,systemId):
    params = {'backup_id':backupId}
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + restoreAPIUrl + "/files/?sid=" +systemId, params, token)
    
def getRestoreJobParametersInJson(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, False)

def getJobStatus(actualResponse,strJobId):
    return jsonHandler.getJobStatusParameterInJson(actualResponse,strJobId)

def putInstantRecoveryJob(token,backupId,directory,systemId,vmName,vmIp,audit,hostName):
    params = {'backup_id':backupId,'target':{'host':hostName,'directory':directory,'name':vmName},'address':vmIp,'audit':audit}
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + restoreAPIUrl + "/instant/?sid=" +systemId, params, token)

def postResponseParse(actualResponse):    
    return jsonHandler.postResponseParse(actualResponse)

def postRestoreArchiveFull(token, archiveId):
    params = {'archive_ids': archiveId, 'association':'dr'}
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + restoreAPIUrl + "/archive/full", params, token)

def getJobNumberFromResponse(actualResponse):
    return jsonHandler.getIdFromJson(actualResponse)

def putVMWareRestore(appliance_ip, token,backupId, systemId, host, datastore, group, name, metadata, template):
    params = {'backup_id':backupId,'target':{"host": host, "datastore": datastore, "group": group, "name":name, "metadata": metadata, "template": template}}
    return apiHelper.sendPOSTRequest("https://" + appliance_ip + restoreAPIUrl + "/full/?sid=" +systemId, params, token) 

def putVMWareInstantRecoveryJob(token,backupId,datastore,systemId,vmName,vmIp,audit,hostName):
    params = {'backup_id':backupId,'target':{'host':hostName,'datastore':datastore,'name':vmName, 'group': ''}, 'address':vmIp,'audit':audit}
    #print Constants.ENVIRONMENT_URL + restoreAPIUrl + "/instant/?sid=" +systemId
    #print params
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + restoreAPIUrl + "/instant/?sid=" +systemId, params, token)

def postWindowsRestore(appliance_ip,token, backupId, client_Id,includeList,restoreFolder,sysId): 
    params = {
              "backup_id" : str(backupId),
              "target" :{
                            "flat": False,
                            "non_destructive": True,
                            "newer": False,
                            "today": False,
                            "unix": True,
                            "directory": str(restoreFolder)
                        },
                        "client_id": str(client_Id),
                        "before_cmd": "",
                        "after_cmd": "",
                        "synthesis": True,
                        "includes": "",
                        "excludes": ""
              }
    print params
    return apiHelper.sendPOSTRequest("https://" + appliance_ip + restoreAPIUrl + "/full/?sid=" +sysId, params, token)

def postLinuxRestore(appliance_ip,token, backupId, client_Id,includeList,restoreFolder,sysId): 
    params = {
              "backup_id" : str(backupId),
              "target" :{
                            "flat": False,
                            "non_destructive": False,
                            "newer": False,
                            "today": False,
                            "unix": True,
                            "directory": str(restoreFolder)
                        },
                        "client_id": str(client_Id),
                        "before_cmd": "",
                        "after_cmd": "",
                        "synthesis": False,
                        "includes": "",
                        "excludes": ""
              }
    return apiHelper.sendPOSTRequest("https://"+appliance_ip + restoreAPIUrl + "/full/?sid=" +sysId, params, token)

def postLinuxRestoreSingleFile(appliance_ip,token, backupId, client_Id,includeList,restoreFolder,sysId):
    includeList = "/root/Backup_folder/InctYPE_1.txt"
    params = {
              "backup_id" : str(backupId),
              "target" :{
                            "flat": False,
                            "non_destructive": False,
                            "newer": False,
                            "today": False,
                            "unix": True,
                            "directory": str(restoreFolder)
                        },
                        "client_id": str(client_Id),
                        "before_cmd": "",
                        "after_cmd": "",
                        "synthesis": False,
                        "includes": [str(includeList)],
                        "excludes": ""
              }
    return apiHelper.sendPOSTRequest("https://"+appliance_ip + restoreAPIUrl + "/full/?sid=" +sysId, params, token)



def postXenRestore(token,backupId, systemId, host, storage_repository, name, metadata):
    params = {'backup_id':backupId,'target':{"host": host, "storage_repository": storage_repository, "name":name, "metadata": metadata}}
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + restoreAPIUrl + "/full/?sid=" +systemId, params, token) 

def postWindowsRestoreWithOptions(token, backupId, client_Id, includeList, sysId, paramFlat, paramNondestructive, paramNewer, paramToday, paramUnix, paramDirectory): 
    params = {
              "backup_id" : str(backupId),
              "target" :{
                            "flat": paramFlat,
                            "non_destructive": paramNondestructive,
                            "newer": paramNewer,
                            "today": paramToday,
                            "unix": paramUnix,
                            "directory": str(paramDirectory)
                        },
                        "client_id": str(client_Id),
                        "before_cmd": "",
                        "after_cmd": "",
                        "synthesis": False,
                        "includes": str(includeList),
                        "excludes": ""
              }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + restoreAPIUrl + "/full/?sid=" +sysId, params, token)

   
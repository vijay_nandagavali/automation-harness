'''
Created on Nov 27, 2014

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 
from testdata import reports_testdata

reportsAPIUrl = "/api/reports" 

def getUpdateHistoryReports(token,systemId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/system/update_history/?sid=" +systemId, token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getListOfArchiveParameterInJson(actualResponse):
    #return jsonHandler.getListOfArchiveParameterInJson(actualResponse, True)
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getListOfBackups(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/backup/history/", token)

def getListOfArchives(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/archive/history/", token)

def getListOfRestores(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/restore/history/", token)

def getListOfStorage(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/storage/status/", token)

def getListOfStorageDataReduction(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/storage/data_reduction/", token)

def getListOfBackupFailure(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/backup/failure", token)

def getListOfBackupLegallyHeld(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/backup/legal_hold/?sid="+ reports_testdata.sid , token)

def getListOfStorageParameterInJson(actualResponse):
    return jsonHandler.getListOfStorageParameterInJson(actualResponse)

def getListOfStorageStatusParameterInJson(actualResponse):
    return jsonHandler.getListOfStorageStatusParameterInJson(actualResponse)

def getListOfStorageDataReductionParameterInJson(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse , True)

def getListOfBackupFailureParameterInJson(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse , True)

def getListOfReplicatedBackups(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/replication/history/?sid=1", token)

def getListOfReplicatedBackupsForTimeRange(token,end_date,start_date):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + reportsAPIUrl + "/replication/history/?end_date="+end_date+"&start_date="+start_date, token)

'''
Created on 16 July, 2015

@author: nikhil s
'''
from config import Constants
from util import apiHelper, logHandler
from util import jsonHandler, linuxMachineConnect
from testdata import clients_testdata

assetsAPIUrl = "/api/assets" 

def getProtectedAssets(token):
    print "hitting API"
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + assetsAPIUrl+"/protected/?sid="+ str(clients_testdata.sid), token)
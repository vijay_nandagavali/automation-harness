'''
Created on Sep 9, 2014

@author: Abhijeet.m
'''

from config import Constants
from util import apiHelper, logHandler
from util import jsonHandler 
from testdata import archive_testdata

mediamountAPIUrl = "/api/archive/media/mount" 
mediaunmountAPIUrl = "/api/archive/media/unmount"
connectedMediaUrl = "/api/archive/media/connected"
catalogUrl = "/api/archive/catalog/?"
deleteArchiveSetUrl = "/api/archive/catalog/"
archiveFilesUrl = "/api/archive/files/"
archiveMediaPrepareUrl = "/api/archive/media/prepare/"
archiveStatusUrl = "/api/archive/status"
archiveSearchUrl = "/api/archive/search"
archiveUrl = "/api/archive"
archiveListUrl = "/api/archive/sets/?sid="
archiveMediaSetUrl = "/api/archive/media/sets/"
mediaCandidatesUrl = "/api/archive/media/candidates"
archiveSetDetailsByIdUrl = "/api/archive/sets/"


def mountMedia(token):
    #print Constants.ENVIRONMENT_URL + mediamountAPIUrl +"/"+archive_testdata.media_name
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + mediamountAPIUrl +"/"+archive_testdata.media_name, "",token)

def unmountMedia(token):
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + mediaunmountAPIUrl +"/"+archive_testdata.media_name,"", token)

def getMediaInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + connectedMediaUrl, token)

def getMediaArchiveSetsInformation(token):
    #print Constants.ENVIRONMENT_URL + archiveMediaSetUrl + archive_testdata.cloud_media_name + "/" + "?sid=1"
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + archiveMediaSetUrl + archive_testdata.cloud_media_name + "/" + "?sid=1", token)

def getMediaListParametersinJson(actualResponse):
    return jsonHandler.getListOfMediaParameterInJson(actualResponse, "false") 

def getMediaArchiveSetParametersinJson(actualResponse):
    return jsonHandler.getListOfArchiveSetsInJson(actualResponse)

def getCatalogByInstance(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + catalogUrl + "view=" + str(archive_testdata.instance), token)

def getCatalogByInstanceParametersinJson(actualResponse):
    return jsonHandler.getCatalogByInstanceParametersInJson(actualResponse)

def getCatalogByInstanceResponseFromJson(actualResponse):
    return jsonHandler.getCatalogByInstanceResponseFromJson(actualResponse)

def deleteArchiveSet(token, setid):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + deleteArchiveSetUrl + str(setid), token, "")

def getArchiveFilesFromSpecifiedArchive(token, archiveId):
    logHandler.logging.info("GET Request API URL - " + Constants.ENVIRONMENT_URL + archiveFilesUrl + str(archiveId))
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + archiveFilesUrl + str(archiveId), token)

def putPrepareArchiveMedia(token, medianame, archivelabel):
    params = {"label":  archivelabel}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + archiveMediaPrepareUrl + str(medianame) + "/?sid=1", params, token)

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def postArchiveStatus(token):
    logHandler.logging.info("POST Archive Status URL: " + Constants.ENVIRONMENT_URL + archiveStatusUrl)
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + archiveStatusUrl, "", token)

def postArchiveStatusId(token, setid):
    logHandler.logging.info("POST Archive Status Id URL: " + Constants.ENVIRONMENT_URL + archiveStatusUrl + "/" + str(setid))
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + archiveStatusUrl + "/" + str(setid), "", token)

def postArchiveSearch(token):
    logHandler.logging.info("POST Archive Search URL: " + Constants.ENVIRONMENT_URL + archiveSearchUrl)
    params = {
              "client_name": archive_testdata.client_name, 
              "name": archive_testdata.file_name,
              "include": True,
              "expression": False,
              "case": True
              }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + archiveSearchUrl, params , token)

def putArchive(appliance_ip, token, instanceId, storageName, archiveType):
    if archiveType == 'All':
        archiveType = ["Full","Differential","Incremental","Selective","Bare Metal","Transaction"]
    else:
        archiveType = [archiveType]
    params = {
              "target":storageName,
              "start_date":0,
              "end_date":0,
              "instances":[instanceId],
              "types":archiveType,
              "options":{
                          #"append": True,
                          "purge":True,
                          #"compress": True,
                          "encrypt":False,
                          #"deduplicate": False,
                          "email_report":True
                          #"retention_days": 3                          
                          }
              }
    return apiHelper.sendPUTRequest("https://"+appliance_ip + archiveUrl, params, token)

def putArchiveCheck(token, instanceId, storageName, archiveType):
    params = {
              "description": archive_testdata.archive_description,
              "target":storageName,
              "start_date": 0,
              "end_date": 0,
              "instances": [instanceId],
              "types": [archiveType],
              "options": {
                          "append": True,
                          "purge": False,
                          "compress": True,
                          "encrypt": False,
                          "deduplicate": False,
                          "email_report": False,
                          "retention_days": 3                          
                          }
              }
    logHandler.logging.info("PUT Archive Check URL: " + Constants.ENVIRONMENT_URL + archiveUrl + "/check" + " with parameters " + str(params))
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + archiveUrl + "/check", params, token)

def postArchiveCatalog(token, storageName):
    params = {
              "name": storageName
              }
    logHandler.logging.info("POST Archive Catalog URL: " + Constants.ENVIRONMENT_URL + archiveUrl + "/catalog" + " with parameters " + str(params))
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + archiveUrl + "/catalog", params, token)

def getArchiveList(token):
    """Makes API call to get archive set list"""
    
    logHandler.logging.info("GET Archive Sets URL: " + Constants.ENVIRONMENT_URL + archiveListUrl)
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + archiveListUrl + archive_testdata.sid, token)

def getArchiveSetsInJson(actualResponse):
    """Get response parameters for archive sets from Json response"""
    return jsonHandler.getListOfArchiveSetsInJson(actualResponse)

def getArchiveMediaCandidates(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + mediaCandidatesUrl, token)

def getArchiveSetDetailsById(token, archiveSetId):
    """Makes API call to get archive set details by Id"""
    
    logHandler.logging.info("GET Archive Set Details By Id URL: " + Constants.ENVIRONMENT_URL + archiveSetDetailsByIdUrl +str(archiveSetId) + "/" + "?sid=1")
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + archiveSetDetailsByIdUrl + str(archiveSetId) + "/?sid=" + archive_testdata.sid, token)

def getArchiveSetByIdInJson(actualResponse):
    """Get response parameters for archive sets from Json response"""
    return jsonHandler.getArchiveSetByIdParamtersInJson(actualResponse)

def mountMediaForCloud(token, cloudStorageName):
    logHandler.logging.info(Constants.ENVIRONMENT_URL + mediamountAPIUrl +"/"+cloudStorageName+"/" + "?sid=1")    
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + mediamountAPIUrl +"/"+ cloudStorageName+"/" + "?sid=1", "",token)

'''
Created on Sep 9, 2014

@author: Abhijeet.m
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 
from testdata import hostname_testdata

hostnameAPIUrl = "/api/hostname" 
hostsAPIUrl = "/api/hosts"


def getHostnameInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + hostnameAPIUrl, token)
    
def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def putHostnameInformation(token):
    strputhostName = hostname_testdata.puthostName
    strputhostLongName = hostname_testdata.puthostLongName
    params = {"name":  strputhostName, "long_name": strputhostLongName}
    print Constants.ENVIRONMENT_URL + hostnameAPIUrl + "/?sid=" + hostname_testdata.sid
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + hostnameAPIUrl + "/?sid=" + hostname_testdata.sid, params, token)

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def getHostsInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + hostsAPIUrl, token)

def postHostsInformation(token):
    strposthostsName = hostname_testdata.posthostsName
    strposthostsIp = hostname_testdata.posthostsIp
    params = {"ip":  strposthostsIp, "name": strposthostsName}
    print Constants.ENVIRONMENT_URL + hostsAPIUrl + "/?sid=" + hostname_testdata.sid
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + hostsAPIUrl + "/?sid=" + hostname_testdata.sid, params, token)

def getPostRequestResponse(actualResponse):
    return jsonHandler.postResponseParse(actualResponse)

def deleteHostsInformation(token):
    strposthostsName = hostname_testdata.posthostsName
    print Constants.ENVIRONMENT_URL + hostsAPIUrl + "/" + strposthostsName + "/?sid=" + hostname_testdata.sid
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + hostsAPIUrl + "/" + strposthostsName + "/?sid=" + hostname_testdata.sid, token)

def getDeleteRequestResponse(actualResponse):
    return jsonHandler.deleteResponseParse(actualResponse)

def puttHostsInformation(token):
    strposthostsName = hostname_testdata.posthostsName
    stroriginalhostsIp = hostname_testdata.posthostsIp
    strputhostsIp = hostname_testdata.puthostsIp
    params = {"ip":  strputhostsIp, "name": strposthostsName, "original_ip": stroriginalhostsIp}
    print Constants.ENVIRONMENT_URL + hostsAPIUrl + "/?sid=" + hostname_testdata.sid
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + hostsAPIUrl + "/?sid=" + hostname_testdata.sid, params, token)

    
    
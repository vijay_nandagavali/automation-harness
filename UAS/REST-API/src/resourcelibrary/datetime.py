'''
Created on Nov 13, 2014

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 

datetimeAPIUrl = "/api/date-time"

def getAllDateInformation(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + datetimeAPIUrl, token)

def getTimezoneInformation(token,systemId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + datetimeAPIUrl + "/timezones/?sid=" +systemId, token)

def getAllDateResponseParameters(actualResponse):
    return jsonHandler.getDateTimeParametersInJson(actualResponse)

def deleteNtpservers(token,systemId):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + datetimeAPIUrl +"/ntpservers/?sid=" +systemId, token, "")

def getTimezoneResponseParameters(actualResponse):
    return jsonHandler.getDateTimeTimezonesParametersInJson(actualResponse)

def updateDateTime(token,year,month,day,hour,minute,second,timezone,ntpEnabled,ntpLocal,ntpSync):    
    params = {"year": year,
    "month": month,
    "day": day,
    "hour": hour,
    "minute": minute,
    "second": second,
    "tz": timezone,
    "ntp": {
        "enabled": ntpEnabled ,
        "local": ntpLocal,
        "sync": ntpSync,
        "servers": [
            "0.centos.pool.ntp.org",
            "1.centos.pool.ntp.org",
            "2.centos.pool.ntp.org", 
            "3.centos.pool.ntp.org"
        ]
    }}
    
    print "payload::  ", params
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + datetimeAPIUrl, params, token)

def updateDateTimeToNTP(token,timezone,ntpEnabled):
    params = {
    "tz": timezone,
    "ntp": {
        "enabled": ntpEnabled ,
         "servers": [
            "0.centos.pool.ntp.org",
            "1.centos.pool.ntp.org",
            "2.centos.pool.ntp.org", 
            "3.centos.pool.ntp.org"
        ]
    }}
    
    print "payload::  ", params
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + datetimeAPIUrl, params, token)


def getPostRequestResponse(actualResponse):
    return jsonHandler.postResponseParse(actualResponse)

def getDeleteRequestResponse(actualResponse):
    return jsonHandler.deleteResponseParse(actualResponse)
'''
Created on Jan 23, 2015

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler
from testdata import credentials_testdata

credentialsAPIUrl = "/api/credentials"


def getListOfClientCredentials(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl, token)

def getDefaultCredentials(token,sid):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/default" + "/?sid=" +sid, token)

def getNamedCredentials(token,sid):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/named" + "/?sid=" +sid, token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getListOfCredentialsResponseParameters(actualResponse):
    return jsonHandler.getCredentialResponseParameterInJson(actualResponse)
    #return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getClientApplicationCredentials(token,systemId,clientId,applicationId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/application/?sid=" +systemId+ "&cid=" +clientId+ "&aid=" +applicationId, token)

def createNewCredentials(token,displayname,username,password,domainame,defaultcredentials):
    params = {'display_name':displayname, 'username':username, 'password':password,'domain':domainame,'is_default':defaultcredentials}
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl, params, token)


def deleteCredentials(token,credentialId):
    
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/" + str(credentialId),token)

def updateCredentials(token,credentialId,systemId,username):
    params = {'username':username}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/" +credentialId + "/?sid=" +systemId, params, token)

def bindClientCredentials(token,credentialId,systemId,clientId,appId):
    params = {'clientID':clientId,'appID':appId}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/bind/client" + "/" +credentialId + "/?sid=" +systemId, params, token)

def bindInstanceCredentials(token,credentialId,systemId,instanceId,appawareParam):
    params = {'credential_id':credentialId,'app_aware':appawareParam}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/bind/instance" + "/" +instanceId + "/?sid=" +systemId, params, token)

def bindPsaCredentials(token,credentialId,systemId,psaId):
    params = {"psa_id": psaId}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/bind/psa" + "/" +credentialId + "/?sid=" +systemId, params, token)
    
def removeClientCredentialAssociation(token,systemId,clientId,appId):
    params = {'clientID':clientId,'appID':appId}
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/bind/client" + "/" + "/?sid=" +systemId , token, params)

def removeInstanceCredentialAssociation(token,systemId,instanceId):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/bind/instance" + "/" +instanceId+ "/?sid=" +systemId , token)

def removePsaCredentialAssociation(token,systemId,psaId):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + credentialsAPIUrl + "/bind/psa" + "/" +psaId+ "/?sid=" +systemId , token)

def getPostRequestResponse(actualResponse):
    return jsonHandler.postResponseParse(actualResponse)

def getDeleteRequestResponse(actualResponse):
    return jsonHandler.deleteResponseParse(actualResponse)

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def getListOfClientCredentialsOnSpecifiedSystem(token):
    url = Constants.ENVIRONMENT_URL + credentialsAPIUrl+ "/" + "/?sid=" + str(credentials_testdata.sid) 
    print url
    return apiHelper.sendGETRequest(url, token)

def getClientCredentialsForSpecificId(token, credId):
    url = Constants.ENVIRONMENT_URL + credentialsAPIUrl+ "/" + str(credId)
    print url
    return apiHelper.sendGETRequest(url, token)
'''
Created on Sept 2, 2015

@author: Nikhil S
'''

from config import Constants
from util import apiHelper, jsonHandler
import json

updatesAPIUrl = "/api/updates" 

def getAllUpdates(token,sid):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + updatesAPIUrl + "/?sid="+sid,token)

def getRequestResponse(actualResponse):
    return jsonHandler.getDictionaryFromJson(actualResponse)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson_updates(actualResponse)


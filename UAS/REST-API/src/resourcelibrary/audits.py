'''
Created on Sep 9, 2014

@author: Abhijeet.m
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 

auditAPIUrl = "/api/audits" 

def getAllAudits(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + auditAPIUrl, token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)
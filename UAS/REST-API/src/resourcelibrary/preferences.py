'''
Created on Oct 6, 2014

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 

preferenceAPIUrl = "/api/preferences" 

def getAllPreferences(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + preferenceAPIUrl, token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getUserPreferences(username, token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + preferenceAPIUrl + "/" + username, token)

def changeUserPreferences(username, token):
    params = {'dash_alert_refresh': "50"}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + preferenceAPIUrl + "/" + username, params, token)

def getUserSpecificPreference(username, token, preference):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + preferenceAPIUrl + "/" + username + "/" + preference , token)

'''
Created on Jun 8, 2015

@author: Nikhil S

Modified on Jan 20, 2017
@author: Divya J

'''
from testdata import replication_testdata
from util import apiHelper, dbHandler, jsonHandler,logHandler
from config import Constants
from config import apiconfig

replicationConfigUrl = "/api/replication/config"
replicationUrl = "/api/replication"

def putUpdateReplicationConfig(token,maxConcurrent=replication_testdata.max_Concurrent,reportMailTo=replication_testdata.report_Mail_to,reportTime=replication_testdata.report_Time,sid=replication_testdata.sid):
    params = {"max_concurrent":maxConcurrent, "report_mail_to":reportMailTo, "report_time":reportTime}
    url = Constants.ENVIRONMENT_URL + replicationConfigUrl + "/?sid="+str(sid)
    print url
    response = apiHelper.sendPUTRequest(url, params, token)
    print "putUpdateReplicationConfig"
    return response

def getReplicationConfig(token,sid):
    url = Constants.ENVIRONMENT_URL + replicationConfigUrl + "/?sid="+str(sid)
    print url
    response = apiHelper.sendGETRequest(url, token)
    print "getUpdateReplicationConfig"
    return response

def postReplicationTarget(token,type,target,ip_target,insecure):
    params = { 
              "type" : str(type),
              "target" : str(target),
              "ip" : str(ip_target),
              "insecure" : str(insecure)
             }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL_SOURCE +replicationUrl+"/target", params, token)

def deletetarget_TempCleanup(targetName):
    query = "delete from bp.source_replication_config where url_name='" + str(targetName) + "'"
    dbResult = dbHandler.executeQuery(query)
    
def getPendingReplicationSources(token):    
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL_TARGET + replicationUrl+ "/pending", token)
    
def getPendingRepSrcParam(actual_response):    
    return jsonHandler.getListOfParameterInJson(actual_response,True)
    
def postAcceptPendingRepReq(token,requestId):    
    params ={
             "accept" : "true",
             "request_id" : str(requestId)
             #,"storage_id": "1"
            }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL_TARGET + replicationUrl+ "/source", params, token) 
    
def postRejectPendingRepReq(token,requestId):    
    params ={
             "accept" : False,
             "request_id" : str(requestId),
             "message" : " Default reject message "             
            }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL_TARGET + replicationUrl+ "/source", params, token) 
    
def getRequestIDfromHostName_DB(hostName):
    query = "select request_id from bp.target_replication_config where status= 'pending'"
    dbResult = dbHandler.executeQuery(query,"target") 
    requestID = dbResult[0][0]   

def deleteAcceptedTarget():
    query = "delete from bp.target_replication_config where status = 'accepted'"
    dbResult = dbHandler.executeDeleteQuery(query,"target")#target cleanup
    logHandler.logging.info("target cleanup:  " + str(dbResult))
    
def deleteRejectedTarget():
    query = "delete from bp.target_replication_config where status = 'rejected'"
    dbResult = dbHandler.executeDeleteQuery(query,"target")#target cleanup
    logHandler.logging.info("target cleanup:  " + str(dbResult))    
        
def deletePendingTargetSource():
    query = "delete from bp.target_replication_config where status = 'pending'"
    dbResult = dbHandler.executeDeleteQuery(query,"target")#target cleanup
    logHandler.logging.info("target cleanup:  " + str(dbResult))
    
    query1 = "delete from bp.source_replication_config where status = 'pending'"
    dbResult1 = dbHandler.executeDeleteQuery(query1,"source")#source cleanup
    logHandler.logging.info("Source cleanup:  " + str(dbResult1))
    
def postResponseParser(actualResponse):
    return jsonHandler.postResponseParse(actualResponse)
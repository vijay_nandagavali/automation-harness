'''
Created on Sep 9, 2014

@author: Abhijeet.m
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 

alertAPIUrl = "/api/alerts" 

def getAllAlerts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + alertAPIUrl, token)
    
def getAlertsOfAlertID(alertID, token):     
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + alertAPIUrl + "/" + alertID, token) 

def getSeverityLevelAlerts(severity, token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + alertAPIUrl + "/" + severity, token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getOpenAlerts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + alertAPIUrl + "/open", token)

def getClosedAlerts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + alertAPIUrl + "/closed", token)

def getSeverityLevelOpenAlerts(severity,token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + alertAPIUrl + "/open/" +severity, token)

def getSeverityLevelClosedAlerts(severity,token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + alertAPIUrl + "/closed/" +severity, token)

def deleteAlert(token, alertId):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + alertAPIUrl + "/" + str(alertId), token, "")

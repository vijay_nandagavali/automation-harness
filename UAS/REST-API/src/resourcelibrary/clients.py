'''
Created on Oct 1, 2014

@author: Amey.k
'''
from config import Constants
from util import apiHelper, logHandler
from util import jsonHandler 
from testdata import clients_testdata, systems_testdata

clientsAPIUrl = "/api/clients" 


def getListOfAllClients(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl, token)

def getListOfAllClientsOnSid(token,sid):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl+"/?sid="+str(sid), token)

def getIdListOfAllClients(actualResponse):
    return jsonHandler.getListOfClientIdsTypes(actualResponse)

def getDetailsOfParticularClient(clientID, token):    
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl + "/" + str(clientID) , token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfClienParameterInJson(actualResponse)

def putUpdateClient(token, cid,updateClientName, updateClientPriority ):
    #print Constants.ENVIRONMENT_URL + clientsAPIUrl + "/" + str(cid) + "/?sid=" + clients_testdata.sid
    params = {"name":  updateClientName, "priority": updateClientPriority}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl +  "/" + str(cid) + "/?sid=" + clients_testdata.sid, params, token)

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def getListOfActualResponseParameterAgentCheckSpecific(actualResponse):
    return jsonHandler.getListOfAgentCheckSpecificClienParameterInJson(actualResponse)

def getListOfAllClientsWithAgentVersionStatus(token, sid):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl + "/agent-check" + "/?sid=" +sid, token)

def deleteClients(appliance_ip, token, cid):
    #print Constants.ENVIRONMENT_URL + clientsAPIUrl + "/" + str(cid)
    return apiHelper.sendDELETERequest("https://" + appliance_ip + clientsAPIUrl+ "/" + str(cid), token)

def deleteClientsAtSystem(token, cid):
    #print Constants.ENVIRONMENT_URL + clientsAPIUrl + "/" + str(cid)
    return apiHelper.sendDELETERequest("https://" + systems_testdata.sysIP + clientsAPIUrl+ "/" + str(cid), token)

def deleteClientsAtSource(token, cid):
    #print Constants.ENVIRONMENT_URL + clientsAPIUrl + "/" + str(cid)
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL_SOURCE + clientsAPIUrl+ "/" + str(cid), token)  

def postClient(appliance_ip, token, sid, os_type, is_enabled, is_synchable, is_encrypted, name, 
        ip, username, password):
        jsondata = {}
        jsondata["os_type"] = os_type
        jsondata["name"] = name
        jsondata["is_enabled"] = is_enabled
        jsondata["is_synchable"] = is_synchable
        jsondata["is_encrypted"] = is_encrypted
        jsondata["host_info"] = {"ip": ip}
        jsondata["credentials"] = {"username":clients_testdata.vm_username, "password":clients_testdata.vm_password}
        print jsondata
        return apiHelper.sendPOSTRequest("https://" + appliance_ip + clientsAPIUrl + "/?sid=1" , jsondata, token)
    
def postXenClient(token, name, os_type,system, ip, username, password, is_enabled, is_encrypted, is_synchable, is_auth_enabled, uebType="main"):
        
        
        if(uebType=="source"):
            environmentIP=Constants.ENVIRONMENT_URL_SOURCE
        else:
            environmentIP=Constants.ENVIRONMENT_URL
        
        jsondata = {}
        jsondata["os_type"] = os_type
        jsondata["system"] = system
        jsondata["name"] = name
        jsondata["credentials"] = {"username":username, "password":password}
        
        jsondata["is_enabled"] = is_enabled
        jsondata["is_synchable"] = is_synchable
        jsondata["is_encrypted"] = is_encrypted
        jsondata["is_auth_enabled"] = is_auth_enabled


        jsondata["host_info"] = {"ip": ip}

        return apiHelper.sendPOSTRequest(environmentIP + clientsAPIUrl , jsondata, token)
    
def postClientForSid(token, sid, name, os_type, priority, is_enabled, is_synchable, is_encrypted, is_auth_enabled, use_ssl, 
        defaultschedule, port, ip, existing_credential, username=clients_testdata.vm_username, 
        password=clients_testdata.vm_password, domain=clients_testdata.vm_domain, uebType="main"):
        
        if(uebType=="source"):
            environmentIP=Constants.ENVIRONMENT_URL_SOURCE
        else:
            environmentIP=Constants.ENVIRONMENT_URL
        
        jsondata = {}
        jsondata["os_type"] = os_type
        jsondata["name"] = name
        jsondata["priority"] = priority
        jsondata["is_enabled"] = is_enabled
        jsondata["is_synchable"] = is_synchable
        jsondata["is_encrypted"] = is_encrypted
        jsondata["is_auth_enabled"] = is_auth_enabled
        jsondata["use_ssl"] = use_ssl
        jsondata["defaultschedule"] = defaultschedule
        jsondata["port"] = port
        jsondata["host_info"] = {"ip": ip}
        jsondata["existing_credential"] = existing_credential
        jsondata["credentials"] = {"username": username, "password": password, "domain": domain}
        return apiHelper.sendPOSTRequest(environmentIP + clientsAPIUrl+"/?sid="+str(sid) , jsondata, token)
    
def postVMClient(appliance_ip, token, sid, os_type, is_enabled, is_synchable, is_encrypted, is_auth_enabled, 
        ip, username, password):
        jsondata = {}
        jsondata["os_type"] = os_type
        jsondata["is_enabled"] = is_enabled
        jsondata["is_synchable"] = is_synchable
        jsondata["is_encrypted"] = is_encrypted
        jsondata["is_auth_enabled"] = is_auth_enabled
        jsondata["host_info"] = {"ip": ip}
        jsondata["credentials"] = {"username": username, "password": password}
        return apiHelper.sendPOSTRequest("https://" + appliance_ip + clientsAPIUrl , jsondata, token)

def postWindowClient(appliance_ip, token, name, os_type, priority, is_enabled, is_synchable,use_ssl, installAgent, is_auth_enabled, is_encrypted, is_default, ip, clientType="Windows"):
        jsondata = {}
        jsondata["name"] = name
        jsondata["os_type"] = os_type        
        jsondata["priority"] = priority
        jsondata["is_enabled"] = is_enabled
        jsondata["is_synchable"] = is_synchable
        jsondata["use_ssl"] = use_ssl
        jsondata["install_agent"] = installAgent
        jsondata["is_encrypted"] = is_encrypted      
        if(clientType=="Windows"):
            jsondata["is_auth_enabled"] = is_auth_enabled  
            jsondata["credentials"] = {"display_name": "", "username": "", "password": "", "is_default": is_default}
        jsondata["host_info"] = {"ip": ip}
        return apiHelper.sendPOSTRequest("https://" + appliance_ip + clientsAPIUrl + "/?sid=" + str(clients_testdata.sid) , jsondata, token)
    
    
def postLinuxClient(appliance_ip, token, name, os_type, priority, is_enabled, is_synchable,use_ssl, installAgent, is_auth_enabled, is_encrypted, is_default, ip):
        jsondata = {}
        jsondata["name"] = name
        jsondata["os_type"] = os_type        
        jsondata["priority"] = priority
        jsondata["is_enabled"] = is_enabled
        jsondata["is_synchable"] = is_synchable
        jsondata["use_ssl"] = use_ssl
        jsondata["install_agent"] = installAgent
        jsondata["is_auth_enabled"] = is_auth_enabled
        jsondata["is_encrypted"] = is_encrypted        
        jsondata["credentials"] = {"display_name": "", "username": "", "password": "", "is_default": is_default}
        jsondata["host_info"] = {"ip": ip}
        return apiHelper.sendPOSTRequest("https://" +appliance_ip + clientsAPIUrl + "/?sid=" + str(clients_testdata.sid) , jsondata, token)
         
def getPostClientsRequestResponse(actualResponse, keyForId):
    cid = jsonHandler.getIdFromPostResponse(actualResponse, keyForId)
    return cid

def getDeleteRequestResponse(actualResponse):
    return jsonHandler.deleteResponseParse(actualResponse)

def GetClientListOfFiles(token,cid,sid,directoty):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl+'/files/'+str(cid)+'/?sid='+sid+'&dir='+directoty, token) 

def GetClientListOfVolumes(token,cid,sid):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl+'/files/'+str(cid)+'/?sid='+sid, token)

def getVMPostClientRequestResponse(actualResponse):
    response = jsonHandler.postResponseParse(actualResponse)
    return response 

def deleteVMClient(token, uuid):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + clientsAPIUrl+ "/" + str(uuid), token)

def putUpdateClientXen(token, cid, updateClientName):
    #params = {"name":  updateClientName, "priority": updateClientPriority}
    hostip = clients_testdata.ip_xenserver
    xenuser = clients_testdata.username_xenserver
    xenpassword = clients_testdata.password_xenserver
    params = {"name": updateClientName,"host_info":{"ip":hostip},"credentials":{"username":xenuser,"password":xenpassword}}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + clientsAPIUrl +  "/" + str(cid) + "/?sid=" + clients_testdata.sid, params, token)

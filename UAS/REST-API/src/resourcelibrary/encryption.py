'''
Created on Dec 9, 2014

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler, linuxMachineConnect

encryptionAPIUrl = "/api/encryption" 

def getEncryptionInformation(token,systemId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + encryptionAPIUrl + "/?sid=" +systemId, token)

def enableEncryption(token,systemId,passphrase):
    params = {'passphrase' :passphrase}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + encryptionAPIUrl + "/enable/?sid=" +systemId, params, token)

def disableEncryption(token,systemId,passphrase):
    params = {'passphrase' :passphrase}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + encryptionAPIUrl + "/disable/?sid=", params, token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getEncryptionSupportInformation(token,systemId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + encryptionAPIUrl + "/support/?sid=" +systemId, token)

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def removeEncryptionPassphrase():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    conn.exec_command('rm -rf /var/lib/misc/cryptoDaemonMasterKeys')
    conn.exec_command('rm -rf /var/lib/misc/cryptoDaemonPersistence')
    conn.exec_command('reboot')
    
def createEncryptionPassphrase(token, systemID, passphrase):
    params = {'new_passphrase' :passphrase}
    url = Constants.ENVIRONMENT_URL + encryptionAPIUrl + "/passphrase/?sid=" + systemID
    print url
    return apiHelper.sendPOSTRequest(url, params, token)

def getPostRequestResponse(actualResponse):
    return jsonHandler.postResponseParse(actualResponse) 

def removeEncryptionPersistence():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    #conn.exec_command('rm -rf /var/lib/misc/cryptoDaemonMasterKeys')
    conn.exec_command('rm -rf /var/lib/misc/cryptoDaemonPersistence')
    conn.exec_command('reboot')

def persistEncryption(token,systemId, passphrase):
    params = {'passphrase' :passphrase}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + encryptionAPIUrl + "/persistent/?sid=" + systemId, params, token)
       
def changeEncryptionPassphrase(token, systemID, currentPassphrase, newPassphrase):
    params = {'current_passphrase' : currentPassphrase, 'new_passphrase' : newPassphrase}
    url = Constants.ENVIRONMENT_URL + encryptionAPIUrl + "/passphrase/?sid=" + systemID
    print url
    return apiHelper.sendPUTRequest(url, params, token)

def enableInstanceEncryption(token,instanceid):
    params = {"instance_id":str(instanceid)}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + encryptionAPIUrl + "/enable-instance/?sid=1", params, token)

       

    
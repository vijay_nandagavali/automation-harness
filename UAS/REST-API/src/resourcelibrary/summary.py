'''
Created on Sep 12, 2014

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler

summarycountsAPIUrl = "/api/summary/counts" 
summarydaysAPIUrl = "/api/summary/days"
summarycurrentAPIUrl = "/api/summary/current"

def getSummaryCounts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarycountsAPIUrl, token)

def getSummaryCurrent(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarycurrentAPIUrl, token)

def getBackupSummaryCounts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarycountsAPIUrl + "/backup", token)

def getReplicationSummaryCounts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarycountsAPIUrl + "/replication", token)

def getReplicatedBackupSummaryCounts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarycountsAPIUrl + "/replicated_backup", token)

def getRestoreSummaryCounts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarycountsAPIUrl + "/restore", token)

def getArchiveSummaryCounts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarycountsAPIUrl + "/archive", token)

def getWirSummaryCounts(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarycountsAPIUrl + "/wir", token)           

def getListOfSummaryParameterInJson(input_json):
    return jsonHandler.getListOfParameterInJson(input_json, False)

def getListOfSummaryCountsParameterInJson(input_json):
    #return jsonHandler.getListOfParameterInJson(input_json, False)
    return jsonHandler.getListOfJobParameterInJson(input_json, False)

def getSummaryByDays(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl, token)

def getBackupSummaryByDays(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl + "/backup", token)

def getRestoreSummaryByDays(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl + "/restore", token)

def getReplicationSummaryByDays(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl + "/replication", token)

def getArchiveSummaryByDays(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl + "/archive", token)

def getParticularDaysSummary(no_days, token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl + "?" + "days=" + str(no_days), token)

def getParticularDaysBackupSummary(no_days, token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl + "/backup/?" + "days=" + str(no_days), token)

def getParticularDaysRestoreSummary(no_days, token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl + "/restore/?" + "days=" + str(no_days), token)

def getParticularDaysReplicationSummary(no_days, token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl + "/replication/?" + "days=" + str(no_days), token)

def getParticularDaysArchiveSummary(no_days, token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + summarydaysAPIUrl + "/archive/?" + "days=" + str(no_days), token)

def getSummaryDaysParameterInJson(input_json):
    return jsonHandler.getSummaryDaysParameterInJson(input_json)

def getBackupSummaryDaysParameterInJson(input_json):
    return jsonHandler.getBackupSummaryDaysParameterInJson(input_json)

def getRestoreSummaryDaysParameterInJson(input_json):
    return jsonHandler.getRestoreSummaryDaysParameterInJson(input_json)

def getReplicationSummaryDaysParameterInJson(input_json):
    return jsonHandler.getReplicationSummaryDaysParameterInJson(input_json)

def getArchiveSummaryDaysParameterInJson(input_json):
    return jsonHandler.getArchiveSummaryDaysParameterInJson(input_json)

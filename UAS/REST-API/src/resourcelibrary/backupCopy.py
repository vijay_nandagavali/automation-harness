'''
Created on Aug 10, 2015

@author: Garima.g
'''
from testdata import backupCopy_testdata
from util import apiHelper, dbHandler, jsonHandler,logHandler
from config import Constants
from config import apiconfig


backupCopyTargetUrl = '/api/backup-copy/targets/'

def getBackupCopyTarget(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL_SOURCE + backupCopyTargetUrl+ "?sid=" + str(backupCopy_testdata.sid), token)

def getBackupCopyTargetListParam(actual_response):    
    return jsonHandler.getListOfParameterInJson(actual_response,True)
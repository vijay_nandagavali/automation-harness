'''
Created on Sep 25, 2014

@author: Amey.k
'''
from config import Constants

from util import apiHelper
from util import jsonHandler
import json
from testdata import systems_testdata

loginurl = "/api/login"

def getAuthenticationToken(appliance_ip,username,password):

    params = {'username':username, 'password':password}
    responseData = json.loads(apiHelper.sendLoginPOSTRequest("https://" + appliance_ip + loginurl, params))
    auth_token = responseData['auth_token']
    return auth_token
    
def getAuthenticationToken_Source(username,password):

    params = {'username':username, 'password':password}
    responseData = json.loads(apiHelper.sendLoginPOSTRequest(Constants.ENVIRONMENT_URL_SOURCE + loginurl, params))
    auth_token = responseData['auth_token']
    return auth_token

def getAuthenticationToken_Target(username,password):

    params = {'username':username, 'password':password}
    responseData = json.loads(apiHelper.sendLoginPOSTRequest(Constants.ENVIRONMENT_URL_TARGET + loginurl, params))
    auth_token = responseData['auth_token']
    return auth_token 

def getAuthenticationToken_System(username,password, sysIp = systems_testdata.sysIP):
    params = {'username':username, 'password':password}
    responseData = json.loads(apiHelper.sendLoginPOSTRequest("https://" + sysIp + loginurl, params))
    auth_token = responseData['auth_token']
    return auth_token
    
def getActualResponseParameter(actualResponse):
    return jsonHandler.getInvalidLoginResponse(actualResponse)

def getLoginResponse(username="", password=""):
    params = {'username':username, 'password':password}
    return apiHelper.sendLoginPOSTRequest(Constants.ENVIRONMENT_URL + loginurl, params)

def getErrorMessage(actualResponse):
    responseData = json.loads(actualResponse)
    message = responseData['result'][0]['message']
    return message

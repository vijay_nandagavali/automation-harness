'''
Created on Jun 3, 2015

@author: savitha.p
'''
from testdata import policy_testdata
from util import apiHelper
from config import Constants
from util import jsonHandler

policyUrl = "/api/retention/policy"
retentionStrategyURL = "/api/retention/strategy"
rententionURL= "/api/retention"

def postPolicyByYears(token, policyName):
    params = {"name":policyName, "days":0, "weeks":0, "months":0, "years":1, "is_default":False}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def postPolicyForever(token):
    params = {"name":policy_testdata.policyNameForever, "days":-1, "weeks":-1, "months":-1, "years":-1, "is_default":False}
    response = apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def putUpdatePolicyByID(token,policyID):
    params = {"name":policy_testdata.policyName, "days":policy_testdata.updatepolicyDays, "weeks":policy_testdata.updatepolicyWeeks, "months":policy_testdata.updatepolicyMonths, "years":policy_testdata.updatepolicyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def postPolicyForUpdate(token):
    params = {"name":policy_testdata.policyNameForupdate, "days":-1, "weeks":-1, "months":-1, "years":-1, "is_default":False}
    response = apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def putModifyPolicyByID(token,policyID):
    params = {"name":policy_testdata.policyNameForupdate, "days":policy_testdata.modifyPolicyDays, "weeks":policy_testdata.modifyPolicyWeeks, "months":policy_testdata.modifyPolicyMonths, "years":policy_testdata.modifyPolicyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def postPolicyByDays(token):
    params = {"name":policy_testdata.policyName, "days":20, "weeks":0, "months":0, "years":0, "is_default":False}
    response = apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def deletePolicyByID(token,ID):
    response = apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + policyUrl + "/" + str(ID), token)
    return response

def putDecreasePolicyByID(token,policyID):
    params = {"name":policy_testdata.policyName, "days":policy_testdata.decreasepolicyDays, "weeks":policy_testdata.policyWeeks, "months":policy_testdata.policyMonths, "years":policy_testdata.policyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)
    response = apiHelper.sendPUTRequest(url, params, token)
    print "putDecreasePolicyByID"
    return response

def putIncreasePolicyByID(token,policyID):
    params = {"name":policy_testdata.policyName, "days":policy_testdata.increasepolicyDays, "weeks":policy_testdata.policyWeeks, "months":policy_testdata.policyMonths, "years":policy_testdata.policyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)    
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def putModifyYearPolicyDecreaseYears(token,policyID):
    params = {"name":policy_testdata.policyName, "days":policy_testdata.decreaseYearPolicyDays, "weeks":policy_testdata.decreaseYearPolicyWeeks, "months":policy_testdata.decreaseYearPolicyMonths, "years":policy_testdata.decreaseYearPolicyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)   
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def putModifyYearPolicyIncreaseYears(token,policyID):
    params = {"name":policy_testdata.policyName, "days":policy_testdata.increaseYearPolicyDays, "weeks":policy_testdata.increaseYearPolicyWeeks, "months":policy_testdata.increaseYearPolicyMonths, "years":policy_testdata.increaseYearPolicyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)    
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def getRetentionStrategy(token):
    url = Constants.ENVIRONMENT_URL + retentionStrategyURL + "/"+ str(policy_testdata.sid)  
    response = apiHelper.sendGETRequest(url, token)
    return response

def postPolicyByMonths(token, policyName):
    params = {"name":policyName, "days":0, "weeks":0, "months":11, "years":0, "is_default":False}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def getRetentionPolicies(token):
    url = Constants.ENVIRONMENT_URL + policyUrl
    response = apiHelper.sendGETRequest(url, token)
    return response

def postPolicyByMonthsForever(token):
    params = {"name":policy_testdata.policyNameMonthsForever, "days":0, "weeks":0, "months":-1, "years":0, "is_default":False}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def putModifyMonthPolicyIncreaseMonths(token,policyID):
    params = {"name":policy_testdata.policyNameMonths, "days":policy_testdata.increaseMonthPolicyDays, "weeks":policy_testdata.increaseMonthPolicyWeeks, "months":policy_testdata.increaseMonthPolicyMonths, "years":policy_testdata.increaseMonthPolicyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)
    print url
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def putModifyMonthPolicyDecreaseMonth(token,policyID, policyDate):
    params = {"name":policyDate, "days":policy_testdata.decreaseMonthPolicyDays, "weeks":policy_testdata.decreaseMonthPolicyWeeks, "months":policy_testdata.decreaseMonthPolicyMonths, "years":policy_testdata.decreaseMonthPolicyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)   
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def postPolicyByWeeks(token, policyName):
    params = {"name":policyName, "days":0, "weeks":8, "months":0, "years":0, "is_default":False}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def postPolicyByDaysForever(token, policyName):
    params = {"name":policyName, "days":-1, "weeks":0, "months":0, "years":0, "is_default":False}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def postPolicyByWeeksForever(token, policyName):
    params = {"name":policyName, "days":0, "weeks":-1, "months":0, "years":0, "is_default":False}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def putModifyWeeksPolicyDecreaseWeeks(token,policyID):
    params = {"name":policy_testdata.policyNameWeeks, "days":policy_testdata.decreaseWeeksPolicyDays, "weeks":policy_testdata.decreaseWeeksPolicyWeeks, "months":policy_testdata.decreaseWeeksPolicyMonths, "years":policy_testdata.decreaseWeeksPolicyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)   
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def postPolicyGeneric(token, policyName, days, weeks, months, years, id_default = False):
    params = {"name":policyName, "days":days, "weeks":weeks, "months":months, "years":years, "is_default":id_default}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def putModifyPolicyById(token,policyID,policyName, days, weeks, months, years, id_default):
    params = {"name":policyName, "days":days, "weeks":weeks, "months":months, "years":years, "is_default":id_default}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def putModifyWeeksPolicyIncreaseWeeks(token,policyID):
    params = {"name":policy_testdata.policyNameWeeks, "days":policy_testdata.increaseWeeksPolicyDays, "weeks":policy_testdata.increaseWeeksPolicyWeeks, "months":policy_testdata.increaseWeeksPolicyMonths, "years":policy_testdata.increaseWeeksPolicyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+str(policyID)   
    response = apiHelper.sendPUTRequest(url, params, token)
    return response

def postPolicyUnicodeName(token, policyName):
    params = {"name":policyName, "days":3, "weeks":0, "months":0, "years":0, "is_default":False}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl, params, token)
    return response

def deletePolicyWithoutID(token):
    response = apiHelper.sendDELETERequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl, token)
    return response


# A function to post a new policy based on Sid for a specific system (hard coded as 1 for automation purpose)
def postPolicyBySid(token, policyName):
    params = {"name":policyName, "days":0, "weeks":0, "months":0, "years":2, "is_default":False}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + policyUrl + "/?sid=1", params, token)
    print Constants.ENVIRONMENT_URL + policyUrl + "/?sid=1"
    return response

#MQ1591 - put update a policy for a specific sid, sid hardcoded to 1 for testing purpose
def putUpdatePolicyBySid(token,policyID):
    params = {"name":policy_testdata.policyName, "days":policy_testdata.updatepolicyDays, "weeks":policy_testdata.updatepolicyWeeks, "months":policy_testdata.updatepolicyMonths, "years":policy_testdata.updatepolicyYears, "is_default":False}
    url = Constants.ENVIRONMENT_URL + policyUrl + "/"+ str(policyID)+ "/?sid=1" 
    print params
    print url
    response = apiHelper.sendPUTRequest(url, params, token)
    return response


def getPolicyByID(token, policyId):
    url = Constants.ENVIRONMENT_URL + policyUrl + "/" + str(policyId)
    print url
    response = apiHelper.sendGETRequest(url, token)
    return response

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)


def getRetention(token, filterName = "", filterId =""):
    '''Get client and policy association'''
    if(filterName != "" and  filterId != ""):
        url = Constants.ENVIRONMENT_URL + rententionURL + "/?" +str(filterName) +"=" + str(filterId)        
    else:
        url = Constants.ENVIRONMENT_URL + rententionURL + "/?sid=" + str(policy_testdata.sid)     
    response = apiHelper.sendGETRequest(url, token)
    return response


def getRetentionParametersFromJson(actualResponse):
    '''Gets Json parameters for get retention '''
    responseParams = jsonHandler.getListOfParameterInJson(actualResponse,False)
    return responseParams

def deleteRetention(token, instance_id = 0):
    '''delete retention association based on instance id '''
    if(instance_id == 0):
        response = apiHelper.sendDELETERequestAdvanced(Constants.ENVIRONMENT_URL + rententionURL, token)
    else:
        response = apiHelper.sendDELETERequestAdvanced(Constants.ENVIRONMENT_URL + rententionURL + "/" + str(instance_id), token)
    return response

    
def postRetentionStrategy(token, strategyName):
    params = {"strategy":strategyName}
    response = apiHelper.sendPOSTRequestAdvanced(Constants.ENVIRONMENT_URL + retentionStrategyURL , params, token)
    return response
    
def getPolicy(token):
    '''Get all Policy details'''    
    url = Constants.ENVIRONMENT_URL + policyUrl +"/?sid=1"
    response = apiHelper.sendGETRequest(url, token)
    return response

def getPolicyParametersFromJson(actualResponse):
    '''Gets Json parameters for get policy '''
    responseParams = jsonHandler.getListOfParameterInJson(actualResponse,False)
    return responseParams


def postPolicyAssocation(token, instance_id=0, policy_id=0):
    '''POST association based on policy id and instance id'''
    if(instance_id != 0 and policy_id != 0):
        params = { 
        "retention":[
            {
                "instance_id":instance_id, 
                "policy_id":policy_id
            }
        ]
        }
    elif(instance_id != 0 and policy_id == 0):
        params = { 
        "retention":[
            {
                "instance_id":instance_id
            }
        ]
        }
    elif(instance_id == 0 and policy_id != 0):
        params = { 
        "retention":[
            { 
                "policy_id":policy_id
            }
        ]
        }
    else:
        params = { 
        "retention":[
            {

            }
        ]
        }
    
    url = Constants.ENVIRONMENT_URL + rententionURL + "/?sid=" + str(policy_testdata.sid)  
    response = apiHelper.sendPOSTRequestAdvanced(url,params,token)
    return response

def deletePolicyFromSpecifiedSystem(token,ID):
    print Constants.ENVIRONMENT_URL + policyUrl + "/" + str(ID) +  "/?sid=" + str(policy_testdata.sid) 
    response = apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + policyUrl + "/" + str(ID) + "/?sid="+ str(policy_testdata.sid), token)
    return response

def deleteRetentionFromSpecifiedSystem(token, instance_id):
    #delete retention association from a specified system with sid as defined in the test data file 
    print Constants.ENVIRONMENT_URL + rententionURL + "/" + str(instance_id) + "/?sid="+ str(policy_testdata.sid)
    response = apiHelper.sendDELETERequestAdvanced(Constants.ENVIRONMENT_URL + rententionURL + "/" + str(instance_id) + "/?sid="+ str(policy_testdata.sid), token)
    return response
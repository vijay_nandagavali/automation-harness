'''
Created on Sep 26, 2014

@author: Amey.k
'''
from config import Constants
from util import apiHelper, logHandler
import json


logouturl = "/api/logout"


def doUserLogout(appliance_ip, token):
    
    
    params = {'cookie':token}
    # print apiHelper.sendPOSTRequest(logouturl,params,token)
    resp = apiHelper.sendPOSTRequest("https://"+appliance_ip + logouturl, params, token)

    responseData = json.loads(resp)
        # print str(responseData)
    if (str(responseData[u'result'][0][u'code']) == u'0'):
        #print "Logout Successful"
        logHandler.logging.info("Logout successful")
        logoutResult = "Pass"
    else:
        logoutResult = "Fail"
    return logoutResult

# def getListOfActualResponseParameter(actualResponse):
#     return jsonHandler.getListOfParameterInJson(actualResponse,True)

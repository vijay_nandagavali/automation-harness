'''
Created on Feb 18, 2015

@author: Rahul A.
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 
from testdata import mailconfig_testdata


mailConfigAPIUrl = "/api/mail-config"

# Send a get request to obtain mail configuration data
def getMailConfig(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + mailConfigAPIUrl, token)
 
# Get response parameters obtained by sending the get request
def getMailConfigResponseParameter(actualResponse):
    return jsonHandler.getListOfJobParameterInJson(actualResponse, False)

# Get mail configuration parameters in JSON format for comparison purpose
def getMailConfigParametersInJson(actualResponse):
    return jsonHandler.getMailConfigParametersInJson(actualResponse)

#Put mail configuration parameters - bp and failure. authinfo defaults to false
def putMailConfig(token):
    strupdateBp = mailconfig_testdata.paramBp
    strupdateFailure = mailconfig_testdata.paramFailure
    strupdateSmtp = mailconfig_testdata.paramSmtp
    strupdateAuthinfo = mailconfig_testdata.paramAuthinfo
    params = {"smtp": strupdateSmtp, "authinfo": strupdateAuthinfo, "bp": strupdateBp, "failure": strupdateFailure}
    print Constants.ENVIRONMENT_URL + mailConfigAPIUrl
    print params
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + mailConfigAPIUrl, params, token)

def putMailConfigSchedule(token):
    strupdateSmtp = mailconfig_testdata.paramSmtp
    strupdateSchedule = mailconfig_testdata.paramSchedule
    params = {"smtp": strupdateSmtp, "schedule": strupdateSchedule}
    print Constants.ENVIRONMENT_URL + mailConfigAPIUrl
    print params
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + mailConfigAPIUrl, params, token)

def deleteMailConfigSchedule(token):
    print Constants.ENVIRONMENT_URL + mailConfigAPIUrl + "/schedule/?sid=" + mailconfig_testdata.sid
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + mailConfigAPIUrl + "/schedule/?sid=" + mailconfig_testdata.sid, token, "")

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def getDeleteRequestResponse(actualResponse):
    return jsonHandler.deleteResponseParse(actualResponse)
'''
Created on 16 July, 2015

@author: nikhil s
'''
from config import Constants
from util import apiHelper, logHandler
from util import jsonHandler, linuxMachineConnect
from testdata import clients_testdata

commandsAPIUrl = "/api/commands" 

def postCommand(token, command_name):
    jsondata = {}
    jsondata["name"] = command_name
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + commandsAPIUrl + "/?sid="+clients_testdata.sid , jsondata, token)
    
def getPostRequestResponse(input_json):
    return jsonHandler.getNetworkBridgeParametersInJson(input_json)
    
def executecommand():    
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    _, out, err = conn.exec_command('ps -ef |grep support.rsa')
    temp = out.readlines()
    return str(temp[0]).strip(' \r\t\n')

def getCountofRowsOnExecutingCommand():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    _, out, err = conn.exec_command('ps -ef |grep "/var/log/dpu/support.rsa" | wc -l')
    temp = out.readlines()
    return str(temp[0]).strip(' \r\t\n')
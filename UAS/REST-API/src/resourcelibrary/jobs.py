'''
Created on Sep 22, 2014

@author: Amey.k
'''

from config import Constants, apiconfig
from util import apiHelper, logHandler
from util import jsonHandler
import time
from testdata import jobs_testdata
from verificationsript import vbackups

joburl = "/api/jobs/active"
deletejoburl = "/api/jobs/"

def getJobs(token, envIp = apiconfig.environmentIp):
    envUrl = Constants.ENVIRONMENT_URL
    logHandler.logging.info("Env URL: " + str(envUrl))
    return apiHelper.sendGETRequest(envUrl + joburl, token)

def getListOfJobParameterInJson(input_json):
    #response = jsonHandler.getListOfJobParameterInJson(input_json, True)
    response = jsonHandler.getListOfParameterInJson(input_json, True)
    return response

def getBackupJobs(appliance_ip, token):    
    return apiHelper.sendGETRequest("https://" + appliance_ip + joburl + "/backup/", token)

def getBackupJobsOnWaiting(token):    
    return apiHelper.sendGETRequestForStatusUnderWaiting(Constants.ENVIRONMENT_URL + joburl + "/backup/", token)

def getBackupJobsAtSource(token):    
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL_SOURCE + joburl + "/backup/", token)

def getRestoreJobs(appliance_ip,token):    
    return apiHelper.sendGETRequest("https://" + appliance_ip + joburl + "/restore/", token)

def getArchiveJobs(appliance_ip, token):    
    return apiHelper.sendGETRequest("https://" + appliance_ip + joburl + "/archive/", token)

def getReplicationJobs(token):    
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + joburl + "/replication/", token)

def deleteJob(token, jobid):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + deletejoburl + str(jobid), token, "")

def deleteJobAtSource(token, jobid):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL_SOURCE + deletejoburl + str(jobid), token, "")

def verifyDeleteResponse(actualResponse):
    return jsonHandler.deleteResponseParse(actualResponse)

def getJobStatus(actualResponse,strJobId):
    return jsonHandler.getJobStatusParameterInJson(actualResponse,strJobId)

def waitForJobCompletion(appliance_ip,jobtype,waittime,token,jobid):
    verificationCnt = 0
    backupjobstatus=""
    while (verificationCnt < (int(waittime/40))):
        if(jobtype=='Backup'):
            actualResponse = getBackupJobs(appliance_ip,token)
        elif (jobtype=='Restore'):
            actualResponse = getRestoreJobs(appliance_ip,token)
        elif (jobtype=='Archive'):
            actualResponse = getArchiveJobs(appliance_ip, token)
        elif (jobtype=='Replication'):
            actualResponse = getReplicationJobs(token)    
        backupjobstatus = getJobStatus(actualResponse,jobid)
        if((backupjobstatus == 'Successful') or (backupjobstatus == 'Error') or (backupjobstatus =='Cancelled') or (backupjobstatus == 'null') or (backupjobstatus == 'Terminated') or (backupjobstatus == '') or (backupjobstatus == 'Warning')):
            print "backup job status from api response: " + backupjobstatus
            break
        #if(backupjobstatus != 'Active'):
        #    break
        verificationCnt += 1
        logHandler.logging.info("Waiting for Job to complete.... CheckCount: " + str(verificationCnt) + " Status: " + backupjobstatus)
        print "Waiting for Job to complete.... CheckCount: " + str(verificationCnt) + " Status: " + backupjobstatus
        logHandler.logging.info("Thread sleep for 30 sec..")
        print "Thread sleep for 30 sec.."
        time.sleep(30)
    
    #Fetching backup job status from the database as API behavior has changed. API does not return Successful status any more
    #TODO: Implement a similar approach for Restore, Archive, Replication if needed
    if(jobtype=='Backup'):
        bid = vbackups.getBackupId(appliance_ip,jobid)
        while (verificationCnt < (int(waittime/40))):
            backupjobstatus = vbackups.checkBackupSucess(appliance_ip,bid)
            if((backupjobstatus == 'Successful') or (backupjobstatus == 'Failed') or (backupjobstatus == 'Warning')):
                print "backup job status from db: " + str(backupjobstatus)
                break
            verificationCnt += 1
            logHandler.logging.info("Waiting for Job to complete.... CheckCount: " + str(verificationCnt) + " Status: " + backupjobstatus)
            print "Waiting for Job to complete.... CheckCount: " + str(verificationCnt) + " Status: " + backupjobstatus
            logHandler.logging.info("Thread sleep for 30 sec..")
            print "Thread sleep for 30 sec.."
            time.sleep(30)
    
    return backupjobstatus

def waitForJobCompletion2(jobtype,waittime,token,jobid):
    verificationCnt = 0
    backupjobstatus=""
    while (verificationCnt < (int(waittime/1))):
        if(jobtype=='Backup'):
            actualResponse = getBackupJobsOnWaiting(token)
        elif (jobtype=='Restore'):
            actualResponse = getRestoreJobs(token)
        elif (jobtype=='Archive'):
            actualResponse = getArchiveJobs(token)
        elif (jobtype=='Replication'):
            actualResponse = getReplicationJobs(token)    
        backupjobstatus = getJobStatus(actualResponse,jobid)
        if((backupjobstatus == 'Successful') or (backupjobstatus == 'Error') or (backupjobstatus =='Cancelled') or (backupjobstatus == 'null') or (backupjobstatus == 'Terminated') or (backupjobstatus == '') or (backupjobstatus == 'Warning')):
            logHandler.logging.info("Time taken....."+str(verificationCnt)+" seconds")
            break
        verificationCnt += 1
        logHandler.logging.info("Waiting for Job to complete.... CheckCount: " + str(verificationCnt) + " Status: " + backupjobstatus)
        
        time.sleep(1)
    return backupjobstatus

def waitForJobCompletionAtSource(jobtype,waittime,token,jobid):
    verificationCnt = 0
    backupjobstatus=""
    while (verificationCnt < (int(waittime/40))):
        if(jobtype=='Backup'):
            actualResponse = getBackupJobsAtSource(token)
        elif (jobtype=='Restore'):
            actualResponse = getRestoreJobs(token)
        elif (jobtype=='Archive'):
            actualResponse = getArchiveJobs(token)
        elif (jobtype=='Replication'):
            actualResponse = getReplicationJobs(token)    
        backupjobstatus = getJobStatus(actualResponse,jobid)
        if((backupjobstatus == 'Successful') or (backupjobstatus == 'Error') or (backupjobstatus =='Cancelled') or (backupjobstatus == 'null') or (backupjobstatus == 'Terminated') or (backupjobstatus == '') or (backupjobstatus == 'Warning')):
            break
        verificationCnt += 1
        logHandler.logging.info("Waiting for Job to complete.... CheckCount: " + str(verificationCnt) + " Status: " + backupjobstatus)
        logHandler.logging.info("Thread sleep for 40 sec..")
        time.sleep(40)
    return backupjobstatus

def waitForScheduleJobCompletion(jobtype,waittime,token,jobNo):
    verificationCnt = 0
    jobstatus=""
    while (verificationCnt < (int(waittime/40))):
        if(jobtype=='Backup'):
            actualResponse = getBackupJobs(token)
        elif (jobtype=='Restore'):
            actualResponse = getRestoreJobs(token)
        elif (jobtype=='Archive'):
            actualResponse = getArchiveJobs(token)
        jobstatus = getJobStatus(actualResponse,jobNo)
        if(jobstatus == ''):
            break
        verificationCnt += 1
        logHandler.logging.info("Waiting for Job to complete.... CheckCount: " + str(verificationCnt) + " Status: " + jobstatus)
        logHandler.logging.info("Thread sleep for 40 sec..")
        time.sleep(40)
        jobstatus = "No jobs"
    return jobstatus           

def getSystemJobs(token):    
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + deletejoburl + "system/" + "?sid=" + jobs_testdata.sid, token)

def suspendJob(token, JobId):
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + deletejoburl + "suspend/" +str(JobId), "", token)

def resumeJob(token, JobId):
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + deletejoburl + "resume/" +str(JobId), "", token)

def verifySuspendedJobResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def verifyResumedJobResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def getListofActiveFLRJobs(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + joburl + "/flr", token)

def getActiveFLRJobParameters(actualResponse):
    return jsonHandler.getActiveJobParametersFromJson(actualResponse)

def getListofActiveIRJobs(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + joburl + "/ir", token)

def getActiveIRJobParameters(actualResponse):
    return jsonHandler.getActiveJobParametersFromJson(actualResponse)

def getJobPercentComplete(actualResponse,sid, jobId):
    return jsonHandler.getJobPercentCompleteParameterInJson(actualResponse,sid, jobId)
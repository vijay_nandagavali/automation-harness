'''
Created on Feb 16, 2015

@author: Nikhil.s, Rahul A.
'''
from config import Constants
from util import apiHelper
from util import jsonHandler 
from testdata import backups_testdata

backupsAPIUrl = "/api/backups" 

def getRestorableBackupListByDay(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/catalog/?view=day", token)

def getListOfActualBackupResponseParameter(actualResponse):
    return jsonHandler.getInstanceParametersInJson(actualResponse, True)

def postBackupSearch(token, bid, sid):
    params = { "name": "notepad.exe", "expression" : False, "case" : False, "include" : True }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/search/?sid=" + str(sid) + "&iid=" + str(bid), params, token)

def putBackupClient(token, clientID):
    strbackupType = backups_testdata.backupType
    strstorageType = backups_testdata.storageType
    strverifyLevel = backups_testdata.verifyLevel
    params = { "client_id": int(clientID), "backup_type": strbackupType, "storage": strstorageType, "verify_level": strverifyLevel}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/?sid=" + backups_testdata.sid, params, token)

def putBackupClientForSid(token, clientID, sid):
    strbackupType = backups_testdata.backupType
    strstorageType = backups_testdata.storageType
    strverifyLevel = backups_testdata.verifyLevel
    params = { "client_id": int(clientID), "backup_type": strbackupType, "storage": strstorageType, "verify_level": strverifyLevel}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/?sid=" + str(sid), params, token)

def putBackupClientIncList(appliance_ip, token, type, verify, clientid, incl_list, email_report, failure_report, storage, backup_type):
    params = {
              "type"           : str(type),
              "verify"         : str(verify),
              "clients"        : [{
                                    "id": clientid,
                                    "incl_list" : [str(incl_list)],
                                    "excl_list" : []
                                 }],
              "email_report"   : str(email_report),
              "failure_report" : str(failure_report),
              "instances"      :[],
              "storage"        : str(storage),
              "backup_type"    : str(backup_type)
             }
    return apiHelper.sendPUTRequest("https://"+appliance_ip + backupsAPIUrl + "/?sid=" + backups_testdata.sid, params, token)

def putBackupClientExcList(appliance_ip, token, verify, clientid, incl_list,excl_list, email_report, failure_report, storage, backup_type):
    params = {
              "type"           : str(type),
              "verify"         : str(verify),
              "clients"        : [{
                                    "id": clientid,
                                    "incl_list" : [str(incl_list)],
                                    "excl_list" : [str(excl_list)]
                                 }],
              "email_report"   : str(email_report),
              "failure_report" : str(failure_report),
              "instances"      :[],
              "storage"        : str(storage),
              "backup_type"    : str(backup_type)
             }
    return apiHelper.sendPUTRequest("https://"+ appliance_ip + backupsAPIUrl + "/?sid=" + backups_testdata.sid, params, token)


# Convert the JSON to a python dictionary and return it
def getPutRequestResponse(actualResponse):
    return jsonHandler.getDictionaryFromJson(actualResponse)

def deleteBackup(token, backupid):
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/" + str(backupid), token, "")

def getDeleteRequestResponse(actualResponse):
    return jsonHandler.deleteResponseParse(actualResponse)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)

def getBackupCatalogResponse(auth_token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/catalog", auth_token)

def getBackupCatalogInstanceResponse(auth_token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/catalog/?sid=1&view=instance", auth_token)

def getBackupCatalogResponseParameter(auth_token):
    return jsonHandler.getListOfResponseParameters(apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/catalog", auth_token))

def putHypervInstanceBackup(appliance_ip, token,instanceId,backupType,storageType,verifyLevel,systemId):
    params = {"instances": [instanceId], "backup_type": backupType, "storage": storageType, "verify_level": verifyLevel}
    return apiHelper.sendPUTRequest("https://"+appliance_ip + backupsAPIUrl + "/?sid=" +systemId, params, token)

def putXenServerInstanceBackup(token,nodeId,name,backupType,storageType,verify,systemId):
    params = {"instances": [{"id":nodeId, "name":name}], "backup_type": backupType, "storage": storageType, "verify": verify}
    print "params: "+str(params)
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/?sid=" +systemId, params, token)

def putHypervInstanceBackupAtSource(token,instanceId,backupType,storageType,verifyLevel,systemId):
    params = {"instances": [instanceId], "backup_type": backupType, "storage": storageType, "verify_level": verifyLevel}
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL_SOURCE + backupsAPIUrl + "/?sid=" +systemId, params, token)

def getBackupById(token, backupId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl + "/?sid=" + backups_testdata.sid + "&bid=" + str(backupId), token)

def getListOfSpecificBackupResponseParameter(actualResponse):
    return jsonHandler.getSpecificBackupParametersInJson(actualResponse)

def postBackup(token, clientID):
    params = {"client_id": int(clientID)}
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + backupsAPIUrl, params, token)

def getPostRequestResponse(input_json):
    return jsonHandler.getpostBackupParameterInJson(input_json)

def getPostSearchRequestResponse(input_json):
    return jsonHandler.getListOfParameterInJson(input_json, True)

def putBackupOnLegalHold(token,backupId,DaysToHold):
    requestUrl= Constants.ENVIRONMENT_URL + backupsAPIUrl + "/hold/" + str(backupId) +"/"+ str(DaysToHold) 
    return apiHelper.sendPUTRequest(requestUrl, "{}", token)



def putBackupClientIncListWindows(appliance_ip, token, type, verify, clientid, incl_list, email_report, failure_report, storage, backup_type, excludeSystemState):
    params = {
              "type"           : str(type),
              "verify"         : str(verify),
              "clients"        : [{
                                    "id": clientid,
                                    "incl_list" : [str(incl_list)],
                                    "excl_list" : [],
                                    "metanames": excludeSystemState
                                    
                                 }],
              "email_report"   : str(email_report),
              "failure_report" : str(failure_report),
              "instances"      :[],
              "storage"        : str(storage),
              "backup_type"    : str(backup_type)
             }
    return apiHelper.sendPUTRequest("https://"+appliance_ip + backupsAPIUrl + "/?sid=" + backups_testdata.sid, params, token)



    
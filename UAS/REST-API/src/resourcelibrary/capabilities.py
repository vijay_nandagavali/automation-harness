'''
Created on Mar 11, 2015

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 


capabilitiesAPIUrl = "/api/capabilities" 


def getListOfCapabilities(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + capabilitiesAPIUrl, token)

def getCapabilitiesParametersInJson(actualResponse):
    return jsonHandler.getCapabilitiesParametersInJson(actualResponse)
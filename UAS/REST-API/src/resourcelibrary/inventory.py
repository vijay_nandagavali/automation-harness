'''
Created on Mar 2, 2015

@author: Amey.k
'''
import time
from config import Constants
from util import apiHelper, jsonHandler, logHandler

inventoryAPIUrl = "/api/inventory"


def getInventorySyncInformation(token,systemId,uebType="main"):
    if(uebType=="source"):
        environmentIP=Constants.ENVIRONMENT_URL_SOURCE
    elif(uebType=="target"):
        environmentIP=Constants.ENVIRONMENT_URL_TARGET
    else:
        environmentIP=Constants.ENVIRONMENT_URL
    return apiHelper.sendGETRequest(environmentIP + inventoryAPIUrl + "/sync/?sid=" +systemId, token)

def putInventorySync(token,uebType="main"):
    if(uebType=="source"):
        environmentIP=Constants.ENVIRONMENT_URL_SOURCE
    elif(uebType=="target"):
        environmentIP=Constants.ENVIRONMENT_URL_TARGET
    else:
        environmentIP=Constants.ENVIRONMENT_URL
    return apiHelper.sendPUTRequest(environmentIP + inventoryAPIUrl,"", token)

def getListOfInventoryParametersInJson(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, False)

def getSyncRunningParam(actualResponse):
    response = jsonHandler.getInventorySyncInfoFromJson(actualResponse)
    return response

def waitForInventorySync(token, waittime, sid, uebType="main"):
    time.sleep(30)
    count = 0
    result = "Fail"
    if(uebType=="source"):
        putResponse = putInventorySync(token, "source")
    elif(uebType=="target"):
        putResponse = putInventorySync(token, "target")
    else:
        putResponse = putInventorySync(token)
    resultPut= jsonHandler.putResponseParse(putResponse)
    logHandler.logging.info("put response: "+str(resultPut))
    logHandler.logging.info("Thread sleep for 5 sec")
    time.sleep(5)
    while (count < (int(waittime)/20)):
        if(uebType=="source"):
            actualResponse = getInventorySyncInformation(token, sid, "source")
        elif(uebType=="target"):
            actualResponse = getInventorySyncInformation(token, sid, "target")
        else:
            actualResponse = getInventorySyncInformation(token, sid)
        syncStatus = getSyncRunningParam(actualResponse)
        if(syncStatus[0] == False):
            break
        count += 1
        logHandler.logging.info("Waiting for Inventory sync to complete.... CheckCount: " + str(count) + " Sync Running Status: " + str(syncStatus[0]))
        logHandler.logging.info("Thread sleep for 20 sec..")
        time.sleep(20)
    logHandler.logging.info("Inventory Sync Status:" + str(syncStatus))
    if(syncStatus[3] == u'Successfully completed sync.'):
        result = "Pass"
    return result

def getInventory(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + inventoryAPIUrl, token)



def parsePutInventorySyncResponse(actualResponse):
    result = jsonHandler.putResponseParse(actualResponse)
    return result
'''
Created on Apr 12, 2016

@author: vijay.n
'''
from config import Constants

from util import apiHelper
from util import jsonHandler
import json
from testdata import settings_testdata

settingsAPIUrl = "/api/settings"


# Convert the JSON to a python dictionary and return it
def getPutRequestResponse(actualResponse):
    return jsonHandler.getDictionaryFromJson(actualResponse)

def getSettings(sid,token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + settingsAPIUrl+"/?sid=" + sid, token)

def getServerInformation(sid,token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + settingsAPIUrl+"/Server Information" + "/?sid=" + sid, token)

def getSettingsJobParametersInJson(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, False)

def setSettings(applianceip, sid, bodyParameters, token):
    return apiHelper.sendPUTRequest("https://" + applianceip + settingsAPIUrl+"/?sid=" + sid, bodyParameters,  token)
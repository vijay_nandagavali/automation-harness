'''
Created on Feb 25, 2015

@author: Amey.k
'''

from config import Constants
from util import apiHelper
from util import jsonHandler 

forumAPIUrl = "/api/forum"

def postForum(token,systemId,userId,forumUsername,forumPassword,displayName,domainName,superuser):
    params = {'user_id':userId,'superuser':superuser,'username':forumUsername,'password':forumPassword,'name':displayName,'domain':domainName}
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + forumAPIUrl + "/users/?sid=" +systemId, params, token)
    
    
def getPostResponseParse(actualResponse,keyForId):
    return jsonHandler.getIdFromPostResponse(actualResponse, keyForId)

def getUserForumInformation(token,userId,systemId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + forumAPIUrl + "/users/" +userId+ "/?sid=" +systemId , token)

def getForumParametersInJson(actualResponse):
    return jsonHandler.getListOfParameterInJson(actualResponse, True)
 
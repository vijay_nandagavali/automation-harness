'''
Created on Nov 21, 2014

@author: Amey.k, Savitha Peri
'''

from config import Constants
from util import apiHelper
from util import jsonHandler
from util import commandlineHelper 
from testdata import storage_testdata

storageAPIUrl = "/api/storage" 

def getAllStorageInformation(sid,token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/?sid=" + sid, token)

def getAllStorageInformationOnId(id,token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/?id=" + id, token)

def getListOfActualResponseParameter(actualResponse):
    return jsonHandler.getStorageParametersFromJson(actualResponse)
  
def deleteStorage(storageId, token):
    #print Constants.ENVIRONMENT_URL + storageAPIUrl + "/" + storage_testdata.deletestorageid + "/?sid=" + sid
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/" + str(storageId), token, "")

def deleteStorageByCommandline(storage):
    #print Constants.ENVIRONMENT_URL + storageAPIUrl + "/" + storage_testdata.deletestorageid + "/?sid=" + sid
    return commandlineHelper.sendGSutilDeleteCommand(storage)

def getDeleteRequestResponse(actualResponse):
    return jsonHandler.deleteResponseParse(actualResponse)

def putOnlineStorage(sid, token, storageId):
    #print Constants.ENVIRONMENT_URL + storageAPIUrl + "/online/" + storageId + "/?sid=" + sid
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/online/" + str(storageId) + "/?sid=" + sid, "", token)

def putOfflineStorage(sid, token, storageId):
    #print Constants.ENVIRONMENT_URL + storageAPIUrl + "/offline/" + storageId + "/?sid=" + sid
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/offline/" + str(storageId) + "/?sid=" + sid, "", token)

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def getAvailableDisksResponse(actualResponse):
    return jsonHandler.getAvailableDisksResponse(actualResponse)

def getAvailableDisks(sid, token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/available-disks/?sid=" + sid, token)

def getTargetInformationISCSI(token, host = storage_testdata.host):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/targets/?sid=" + storage_testdata.sid + "&host=" + host + "&port=" + storage_testdata.port, token)
    
def getListOfActualResponseParameterISCSI(actualResponse):
    return jsonHandler.getListofTargetInformationISCSIParameterInJson(actualResponse)

def getListOfLunInformation(token, targetName = storage_testdata.targetName, host = storage_testdata.host):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/targets/" + targetName + "/?sid=" + storage_testdata.sid + "&host=" + host + "&port=" + storage_testdata.port, token)

def postStorage(token, shareName, userName, passWord):
    params = {
              "name":storage_testdata.googleCloudStorageName,
              "type":4,
              "usage":"archive",
              "properties":
                    {
                     "hostname":"gs",
                     "share_name":shareName,
                     "protocol":"cloud",
                     "username":userName,
                     "password":passWord,
                     "max_size":20
                     }              
              }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + storageAPIUrl, params, token) 

def postISCSIStorage(token, iscsiStorageName, iscsiHost, iscsiTarget, iscsiLun, iscsiUserName):
    params = {
              "name":iscsiStorageName,
              "type":1,
              "usage":"stateless",
              "properties":
                    {
                     "hostname":iscsiHost,
                     "port":storage_testdata.port,
                     "target":iscsiTarget,
                     "lun":iscsiLun,
                     "username":iscsiUserName
                    }              
              }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/?sid=" + storage_testdata.sid, params, token)
    
    
def putCloudStorage(token, storageId, userName, storageSize):
    #Updating the max_size of the cloud storage to 25 from 20
    params = {
              "type":4,
              "usage":"archive",
              "properties":
                    {
                     "protocol":"cloud",
                     "username":userName,
                     "max_size":storageSize
                     }
              }
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/" + str(storageId), params, token)
                    

def postNasStorage(token, usage, name, protocol, port, hostname, share_name, username, password):
    params = {
              "name":name,
              "type":4,
              "usage":usage,
              "properties":
                    {
                     "hostname":hostname,
                     "share_name":share_name,
                     "protocol":protocol,
                     "port":port,
                     "username":username,
                     "password":password
                     }              
              }
    return apiHelper.sendPOSTRequest(Constants.ENVIRONMENT_URL + storageAPIUrl, params, token)

def postDirectStorage(appliance_ip, token, diskpartition, name, usage):
    params = {
              "name":name,
              "type":5,
              "usage":usage,
              "properties":
                    {
                     "internal_name":diskpartition
                    }              
              }
    return apiHelper.sendPOSTRequest("https://" +appliance_ip + storageAPIUrl, params, token)

def deleteStorageWithSidId(token, sid, storageId):
    print Constants.ENVIRONMENT_URL + storageAPIUrl + "/" + str(storageId) + "/?sid=" + str(sid)
    return apiHelper.sendDELETERequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/" + str(storageId) + "/?sid=" + str(sid), token, "")


def updateStorageNfsNAS( storageId, token, usage, protocol, port, username, password):
    params = {
              "usage":usage,
              "type":4,
              "properties":
                    {
                     "protocol":protocol,
                     "port":port,
                     "username":username,
                     "password":password
                     }              
              }
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/" + str(storageId), params, token)

def putNasStorage(token,storageId,protocol, port, hostname, share_name, username, password):
    params = {         
              "type":4,        
              "properties":
                    {
                     "hostname":hostname,
                     "share_name":share_name,
                     "protocol":protocol,
                     "port":port,
                     "username":username,
                     "password":password
                     }              
              }
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + storageAPIUrl + "/" + str(storageId) + "/?sid="+ storage_testdata.sid , params, token)
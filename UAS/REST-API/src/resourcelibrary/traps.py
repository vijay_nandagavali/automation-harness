'''
Created on Oct 30, 2014

@author: Amey.k
'''
from config import Constants
from util import apiHelper
from util import jsonHandler 
from testdata import traps_testdata

trapsAPIUrl = "/api/traps" 

def getTrapsHistory(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + trapsAPIUrl + "/history", token)

def getListOfActualResponseParameter(actualResponse):    
    #return jsonHandler.getListOfClienParameterInJson(actualResponse)
    return jsonHandler.getTrapHistoryParametersInJson(actualResponse)

def getTraps(token):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + trapsAPIUrl, token)

def getTrapById(token, trapId):
    return apiHelper.sendGETRequest(Constants.ENVIRONMENT_URL + trapsAPIUrl + "/" + str(trapId), token)

def putTestTrap(token):
    print Constants.ENVIRONMENT_URL + trapsAPIUrl + "/test" + "/sid=" + str(traps_testdata.sid)
    return apiHelper.sendPUTRequest(Constants.ENVIRONMENT_URL + trapsAPIUrl + "/test" + "/sid=" + str(traps_testdata.sid), "", token)

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)
                                                                    
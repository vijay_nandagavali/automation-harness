'''
Created on Feb 6, 2015

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''
from util import dbHandler, logHandler, linuxMachineConnect
from testdata import joborders_testdata
import json


def getJobordersFromDB():
    jolist = []
    dbResult = dbHandler.executeQuery("""SELECT DISTINCT ON (schedule_id) schedule_id from bp.schedules""")
    for row in dbResult:
        #print str(row[0])
        jolist.append(str(row[0])+"b")
    dbResult = dbHandler.executeQuery("""SELECT name from bp.schedules""")
    for row in dbResult:
        jolist.append(row[0])
    dbResult = dbHandler.executeQuery("""SELECT app_id from bp.schedules""")
    for row in dbResult:
        jolist.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT description from bp.schedules""")
    for row in dbResult:
        jolist.append(row[0])
    dbResult = dbHandler.executeQuery("""SELECT enabled from bp.schedules""")
    for row in dbResult:
        if (row[0] == True):
            jolist.append(str(1))
        else:
            jolist.append(str(0))
    jolist.sort()
    return jolist


def getJobordersCountFromDB():
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.schedules""")
    #print "Number of joborders are:"
    return dbResult[0][0]

def getJobordersFromResponse(actualResponse,count):

    jolist = []
    responseData = json.loads(actualResponse)
    for i in range(0,count):
        jo_id = str(responseData['data'][i]['id'])
        jolist.append(jo_id)
        jo_name = str(responseData['data'][i]['name'])
        jolist.append(jo_name)
        appid = str(responseData['data'][i]['app_id'])
        jolist.append(appid)
        desc = str(responseData['data'][i]['description'])
        jolist.append(desc)
        enabled = str(responseData['data'][i]['enabled'])
        jolist.append(enabled)
    jolist.sort()
    return jolist

def getJoborderStatusfromDB(jobID):
    dbResult = dbHandler.executeQuery("""SELECT enabled from bp.schedules where schedule_id="""+jobID )
    return dbResult[0][0]

def getSpecificJoborderFromDB(joid):
    jolist = []
    dbResult = dbHandler.executeQuery("""SELECT name from bp.schedules where schedule_id=""" + joid)
    jolist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT app_id from bp.schedules where schedule_id=""" + joid)
    jolist.append(dbResult[0][0])
    return jolist
              
def getSpecificJoborderFromResponse(actualResponse):
    jolist = []
    responseData = json.loads(actualResponse)
    jo_name=str(responseData['data'][0]['name'])
    jolist.append(jo_name)
    appid = responseData['data'][0]['app_id']
    jolist.append(appid)
    return jolist

def getAddedJoborderFromDB(id):
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.schedules where schedule_id=""" +id)        
    return dbResult[0][0]
    
def getAddedReplicationJoborderFromDB(id):
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.replication_joborders where replication_joborder_id=""" +id)        
    return dbResult[0][0] 
    
def getAddedReplicationJoborderFromDBAtSource(id):
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.replication_joborders where replication_joborder_id=""" +id,"source")        
    return dbResult[0][0]   
    
def findJobOrderByName(name):
    id=0   
    #print name
    dbResult = dbHandler.executeQuery("""SELECT schedule_id from bp.schedules where name=""" +"'"+name+"'")        
    id = dbResult[0][0]
    return id 

def findReplicationJobOrderByName(name):
    id=0   
    #print name
    dbResult = dbHandler.executeQuery("""SELECT replication_joborder_id from bp.replication_joborders where name=""" +"'"+name+"'")        
    id = dbResult[0][0]
    return id 

def findReplicationJobOrderByNameAtSource(name):
    id=0   
    #print name
    dbResult = dbHandler.executeQuery("""SELECT replication_joborder_id from bp.replication_joborders where name=""" +"'"+name+"'","source")        
    id = dbResult[0][0]
    return id 

def getInstanceIdFromVm(vmName):
    instanceId= 0 
    query = "select instance_id from bp.application_instances where key1 = " + "'"+vmName+"'"
    try:
        dbResult = dbHandler.executeQuery(query)
        instanceId = str(dbResult[0][0])
    except:
        #print "Instance Id not found"
        logHandler.logging.error("Instance Id not found")
    return int(instanceId)

def getJobRunStatusfromDB(joid):
    query = "select queued, status_string from bp.schedule_history where schedule_id = " + str(joid) + " order by job_no desc limit 1"
    #print query
    logHandler.logging.info("Get Job Run Status from DB query: " + query)
    result = 0
    dbResult = dbHandler.executeQuery(query)
    for row in dbResult:
        #print row[0], row[1]
        logHandler.logging.info("DB Result: " + str(row[0]) + " " + str(row[1]))
        #print (row[1][:12] == "In job queue")
        if(row[1] == "Active" or row[1][:12] == "In job queue"):
            result = 1
        else:
            pass
    return result  

def getNonActiveJobIdfromDB():
    query = "select distinct schedule_id from bp.schedules where schedule_id not in (select schedule_id from bp.schedule_history where queued = 1)"
    #print query
    logHandler.logging.info("Get Non Active Job Ids from DB query: " + query)
    joid = 0
    dbResult = dbHandler.executeQuery(query)
    for row in dbResult:
        joid = row[0]
    return joid

def getDateTimeFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    logHandler.logging.info("Connection: " + str(conn))
    dateTime = []
    format = "%m/%d/%Y" 
    cmd = "date +""" + format + "" 
    #logHandler.logging.info("cmd = " + str(cmd))
    _, out, err = conn.exec_command(cmd)
    temp = out.readlines()
    dateTime.append(str(temp[0]).strip(' \r\t\n'))
    format = "%H:%M"
    cmd = "date +""" + format + "" 
    #logHandler.logging.info("cmd = " + str(cmd))
    _, out, err = conn.exec_command(cmd)
    temp = out.readlines()
    dateTime.append(str(temp[0]).strip(' \r\t\n'))
    #logHandler.logging.info("DateTime from machine: " + str(dateTime))
    return dateTime

def getAllJobordersFromDB():
    dbResult = dbHandler.executeQuery("select schedule_id from bp.schedules")
    schedules = []
    if(len(dbResult) > 0):
        for row in dbResult:
            schedules.append(row[0])
    return schedules
'''
Created on Aug 10, 2015

@author: Garima.g
'''
import json
from util import dbHandler
from config import apiconfig

def getTargetListParametersFromResponse(actualResponse):
    repTargetList = []
    jsonData = json.loads(actualResponse)
    if(len(jsonData['data'][0]['targets']) > 0):
        for key,value in jsonData['data'][0]['targets'][0].items():         
            if(key == "name" or key =="status" or key =="type" or key =="message"):        
                repTargetList.append(str(key))                     
    repTargetList.sort()
    return repTargetList    

def getTargetFromResponse(actualResponse):
    repTargetList = []
    jsonData = json.loads(actualResponse)
    if(len(jsonData['data'][0]['targets']) > 0):
        for i in range(0, len(jsonData['data'][0]['targets'])):
            repTargetList.append(str(jsonData['data'][0]['targets'][i]['name']))
            repTargetList.append(str(jsonData['data'][0]['targets'][i]['status']))
            repTargetList.append(str(jsonData['data'][0]['targets'][i]['type']))
            repTargetList.append(str(jsonData['data'][0]['targets'][i]['message']))     
    repTargetList.sort()
    return repTargetList    

def getTargetListFromDB():
    repTargetList = []
    query = "select url_name,target_type ,status, message from  bp.source_replication_config"
    dbResult = dbHandler.executeQuery(query,"source")
    for row in dbResult:
        repTargetList.append(str(row[0]))
        repTargetList.append(str(row[1]))
        repTargetList.append(str(row[2]))
        repTargetList.append(str(row[3]))
    repTargetList.sort()
    return repTargetList 
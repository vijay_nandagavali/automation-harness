'''
Created on Jan 23, 2015

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''

import json

from util import dbHandler
from util import jsonHandler


def getClientCredentialsFromDB():
    
    credlist = []
    dbResult = dbHandler.executeQuery("""SELECT credential_id  from bp.client_credentials""")
    credlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT display_name from bp.client_credentials""")
    credlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT username from bp.client_credentials""")
    credlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT domain from bp.client_credentials""")
    credlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT is_default from bp.client_credentials""")
    credlist.append(dbResult[0][0])
#     dbResult = dbHandler.executeQuery("""SELECT system_id from bp.systems""")
#     for row in dbResult:
#         credlist.append(row[0])
#     dbResult = dbHandler.executeQuery("""SELECT name from bp.systems""")
#     for row in dbResult:
#         credlist.append(row[0])
    credlist.sort()
    print credlist
    return credlist

def getDefaultCredentialsFromDB():
    credlist = []
    dbResult = dbHandler.executeQuery("""SELECT credential_id  from bp.client_credentials where is_default=True""")
    credlist.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("""SELECT display_name from bp.client_credentials where is_default=True""")
    credlist.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("""SELECT username from bp.client_credentials where is_default=True""")
    credlist.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("""SELECT domain from bp.client_credentials where is_default=True""")
    credlist.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("""SELECT is_default from bp.client_credentials where is_default=True""")
    credlist.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("""SELECT system_id from bp.systems""")
    credlist.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("""SELECT name from bp.systems""")
    credlist.append(str(dbResult[0][0]))
    credlist.sort()
    return credlist

def getCredentialsCountFromDB():
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.client_credentials""")
    print "Number of credentials are:"
    print dbResult[0]
    returndbResult[0]

def getInstanceIdFromDB():
    dbResult = dbHandler.executeQuery(""" select instance_id from bp.application_instances where node_no=1 and app_id=1""")
    print dbResult[0]
    return dbResult[0]    
    

def getCredentialsFromResponse(actualResponse,count):
    credlist = []
    responseData = json.loads(actualResponse)
    for i in range(0,count):
        try:
            cred_id = responseData['data'][i]['credential_id']
            credlist.append(cred_id)
            cred_name = str(responseData['data'][i]['display_name'])
            credlist.append(cred_name)
            uname = responseData['data'][i]['username']
            credlist.append(uname)
            domain = responseData['data'][i]['domain']
            credlist.append(domain)
            is_default = responseData['data'][i]['is_default']
            credlist.append(is_default)
        except(KeyError):
            print "Data not found"     
#         sysid = responseData['data'][i]['sid']
#         credlist.append(sysid)
#         sysname = responseData['data'][i]['system_name']
#         credlist.append(sysname)
    credlist.sort()
    return credlist

def getDefaultCredentialsFromResponse(actualResponse):
    
    credlist = []
    responseData = json.loads(actualResponse)
#     cred_id = responseData['data'][0]['credential_id']
#     credlist.append(cred_id)
#     cred_name = str(responseData['data'][0]['display_name'])
#     credlist.append(cred_name)
#     uname = responseData['data'][0]['username']
#     credlist.append(uname)
#     domain = responseData['data'][0]['domain']
#     credlist.append(domain)
#     is_default = responseData['data'][0]['is_default']
#     credlist.append(is_default)
#     sysid = responseData['data'][0]['sid']
#     credlist.append(sysid)
#     sysname = responseData['data'][0]['system_name']
#     credlist.append(sysname)
    
    for key, value in responseData.items():
        try:
            if(key != 'timestamp'):    
#                 print value.items()             
#                 for k in value.items():
#                     if(str(k[0]) == "credential_id" or str(k[0]) == "display_name" or str(k[0]) == "username" or str(k[0]) == "domain" or str(k[0]) == "is_default" or str(k[0]) == "sid" or tr(k[0]) == "system_name"):                                 
#                         credlist.append(str(k[1]))          
#                 break
                if(isinstance(value, dict)):                                                
                    for k in value.items():  
                        if(str(k[0]) == "credential_id" or str(k[0]) == "display_name" or str(k[0]) == "username" or str(k[0]) == "domain" or str(k[0]) == "is_default" or str(k[0]) == "sid" or str(k[0]) == "system_name"):                                 
                            credlist.append(str(k[1]))       
                    break
                else:
                    for k in value[0].items():  
                        if(str(k[0]) == "credential_id" or str(k[0]) == "display_name" or str(k[0]) == "username" or str(k[0]) == "domain" or str(k[0]) == "is_default" or str(k[0]) == "sid" or str(k[0]) == "system_name"):                                 
                            credlist.append(str(k[1]))  
                    break
        except (TypeError,AttributeError):        
                if(key != 'timestamp'):
                    credlist.append(str(key))
        except IndexError:
                print "Data not present"
    
    credlist.sort()
    return credlist

def getAddedCredentialFromDB(displayName):
    credlist = []
    
    
#     dbResult = dbHandler.executeQuery("""SELECT credential_id  from bp.client_credentials where display_name=""" +"'"+displayName+"'")
#     for row in dbResult:
#         credlist.append(row[0])
#     dbResult = dbHandler.executeQuery("""SELECT display_name from bp.client_credentials where display_name=""" +"'"+displayName+"'")
#     for row in dbResult:
#         credlist.append(row[0])
#     dbResult = dbHandler.executeQuery("""SELECT username from bp.client_credentials where display_name=""" +"'"+displayName+"'")
#     for row in dbResult:
#         credlist.append(row[0])
#     dbResult = dbHandler.executeQuery("""SELECT domain from bp.client_credentials where display_name=""" +"'"+displayName+"'")
#     for row in dbResult:
#         credlist.append(row[0])
#     dbResult = dbHandler.executeQuery("""SELECT is_default from bp.client_credentials where display_name=""" +"'"+displayName+"'")
#     for row in dbResult:
#         credlist.append(row[0])

    dbResult = dbHandler.executeQuery("""SELECT credential_id, display_name, username, domain, is_default from bp.client_credentials where display_name=""" +"'"+displayName+"'")        
    for row in dbResult:
        for column in row:
            credlist.append(str(column))    
        
    credlist.sort()
    return credlist

def getChangedUsernameFromDB(username):
    credlist = []
    dbResult = dbHandler.executeQuery("""SELECT username from bp.client_credentials where username=""" +"'"+username+"'")
    credlist.append(dbResult[0][0])
    return credlist

def getClientCredentialAssociationFromDB(credentialId):
    credlist = []
    dbResult = dbHandler.executeQuery("""SELECT node_no,node_name,credential_id,username,display_name from bp. client_credentials join bp.nodes using(credential_id) where credential_id =""" +"'"+credentialId+"'")
    for row in dbResult:
        credlist.append(row[0])
    credlist.sort()
    return credlist

def getInstanceCredentialAssociationFromDB(instanceId):
    credlist = []
    dbResult = dbHandler.executeQuery("""SELECT instance_id, credential_id,app_aware from bp.application_instances where instance_id =""" +"'"+instanceId+"'")
    for row in dbResult:
        credlist.append(row[0])
    credlist.sort()
    return credlist

def getPsaCredentialAssociationFromDB(credentialId):
    credlist = []
    dbResult = dbHandler.executeQuery("""SELECT psa_id, credential_id from bp.psa_config where credential_id =""" +"'"+credentialId+"'")
    for row in dbResult:
        credlist.append(row[0])
    credlist.sort()
    return credlist

def getCredentialIdFromDB():
    dbResult = dbHandler.executeQuery("""SELECT credential_id  from bp.client_credentials""" )
    return dbResult[0][0]

def getSpecificCredentialsFromResponse(actualResponse):
    credlist = []
    responseData = json.loads(actualResponse)
    cred_id = responseData['data']['credential_id']
    credlist.append(cred_id)
    cred_name = str(responseData['data']['display_name'])
    credlist.append(cred_name)
    uname = responseData['data']['username']
    credlist.append(uname)
    is_default = responseData['data']['is_default']
    credlist.append(is_default)
    credlist.sort()
    return credlist

def getSpecificClientCredentialsFromDB(credentialId):
    
    credlist = []
    dbResult = dbHandler.executeQuery("SELECT credential_id  from bp.client_credentials where credential_id =" + str(credentialId))
    credlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT display_name from bp.client_credentials where credential_id =""" + str(credentialId))
    credlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT username from bp.client_credentials where credential_id =""" + str(credentialId))
    credlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT is_default from bp.client_credentials where credential_id =""" + str(credentialId))
    credlist.append(dbResult[0][0])
#     dbResult = dbHandler.executeQuery("""SELECT system_id from bp.systems""")
#     for row in dbResult:
#         credlist.append(row[0])
#     dbResult = dbHandler.executeQuery("""SELECT name from bp.systems""")
#     for row in dbResult:
#         credlist.append(row[0])
    credlist.sort()
    print credlist
    return credlist

'''
Created on Nov 14, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''

import json


from util import dbHandler
from util import jsonHandler

def getAllUsersFromDB():
    dbResult = dbHandler.executeQuery("""SELECT user_id, username from bp.users""") 
    dbdict = {}
    for row1 in dbResult:
        dbdict[row1[0]]=row1[1]
    return dbdict

def getUsersFromResponse(actualResponse):
    responseData = jsonHandler.getAllParametersFromJson(actualResponse)  
    userDict = {}  
    for user in responseData:
        userDict[user['id']] = str(user['name'])        
    return userDict

def getSpecificUserFromDB(userID):
    dbResult = dbHandler.executeQuery("""SELECT user_id, username from bp.users where user_id=""" +userID) 
    dbdict = {}
    dbdict[dbResult[0][0]]=dbResult[0][1]    
    return dbdict

def getChangedPasswordFromDB(userID):
    dbResult = dbHandler.executeQuery("""SELECT password from bp.users where user_id=""" +userID)
    password_string = str(dbResult[0][0])
    return password_string



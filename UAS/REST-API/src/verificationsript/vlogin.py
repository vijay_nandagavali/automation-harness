'''
Created on Sep 25, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''

import json

from testdata import login_testdata
from util import dbHandler
from util import jsonHandler


def getAuthTokenResponseVerification(actualResponse):
    responseData = json.loads(actualResponse)
    result = []
    if (responseData[login_testdata.responseParameter[2]] != ""):
        response = "Pass"
    else:
        response = "Fail"
    result.append(response)
    return result

def getPreAuthTokenVerificationDBCount():
    dbResult = dbHandler.executeQuery("""SELECT count(*)from bp.sessions""")
    return dbResult[0][0]
    
        
def getAuthTokenVerificationDB(count):
    dbResult = dbHandler.executeQuery("""SELECT count(*)from bp.sessions""")
    result = []
    count1 = dbResult[0][0]
    print "UUID count after login is called is: " +str(count1)
    if(count1 == (count + 1)):
        response = "Pass"
    else:
        response = "Fail"
    result.append(response)
    return result
    
    
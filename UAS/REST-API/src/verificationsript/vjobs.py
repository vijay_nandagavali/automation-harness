'''
Created on Sep 22, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''
import json, time

from testdata import jobs_testdata
from util import dbHandler, logHandler
from util import jsonHandler
from __builtin__ import str


def getJobsFromDB():
    dblist = []
    
    dbResult = dbHandler.executeQuery("""SELECT count(*)from bp.jobs""")
    if dbResult[0][0] == 0:
        logHandler.logging.info("No active jobs")
    
    
    dbResult = dbHandler.executeQuery("""SELECT job_no from bp.jobs;""")
    for row1 in dbResult:
        dblist.append(str(row1[0]))
    return dblist

def getDeletedJobFromDB():
    dblist = []
    dbResult = dbHandler.executeQuery("""select job_no from bp.jobs where (status & 32776) >0;""")
    for row1 in dbResult:
        dblist.append(str(row1[0]))
    return dblist
        
def getJobsFromResponse(actualResponse):
    joblist = []
    responseData = json.loads(actualResponse)
    no_job = 0
    if(len(responseData['data'])):
        no_job = (len(responseData['data']))
        #print "No of active jobs are:" +str(no_job)
        logHandler.logging.info("No of active jobs are: " + str(no_job))
    for i in range(0,no_job):
        joblist.append(str(responseData['data'][i][jobs_testdata.responseParameter[5]]))
    return joblist

def getSpecificJobsFromDB(list_Backup_Ids):
    dblist = []  
    no_backup_jobs = len(list_Backup_Ids)
    if(no_backup_jobs):
        for i in range(0,no_backup_jobs):
            dbResult = dbHandler.executeQuery("""SELECT job_no from bp.jobs where job_no="""+list_Backup_Ids[i])
            for row1 in dbResult:
                dblist.append(str(row1[0]))
    return dblist

def getSpecificJobsFromResponse(actualResponse):
    joblist = []
    responseData = json.loads(actualResponse)
    no_job = 0
    if(len(responseData['data'])):
        no_job = (len(responseData['data']))
    for i in range(0,no_job):
        #joblist.append(str(responseData['data'][i][jobs_testdata.responseParameter[3]]))
        joblist.append(str(responseData['data'][i]['id']))
    return joblist

def getListOfJobSpecificIds(actualResponse):
    backup_Ids = []
    responseData = json.loads(actualResponse)
    no_jobs = len(responseData['data'])
    for i in range(0,no_jobs):
            #backup_Ids.append(str(responseData['data'][i][jobs_testdata.responseParameter[3]]))
            backup_Ids.append(str(responseData['data'][i]['id']))
    #print "No of active jobs:" +str(no_jobs)
    logHandler.logging.info("No of active jobs: " + str(no_jobs))      
    return backup_Ids

def getSystemJobsFromResponse(actualResponse):
    joblist = []
    responseData = json.loads(actualResponse)
    no_jobs = len(responseData['data'])
    for i in range(0,no_jobs):
        joblist.append(str(responseData['data'][i]['notification_id']))
        joblist.append(str(responseData['data'][i]['category'])) 
        #joblist.append(str(responseData['data'][i]['username']))
        #joblist.append(str(responseData['data'][i]['system']))
        #joblist.append(str(responseData['data'][i]['message']))    
    return joblist

def getSystemJobsFromDB(actualResponse):
    joblist = [] 
    responseData = json.loads(actualResponse)
    no_jobs = len(responseData['data'])
    for i in range(0,no_jobs):
        dbResult = dbHandler.executeQuery("""select n.notification_id,n.primary_text,c.category from bp.notifications n join bp.categories c on n.category_id = c.category_id where n.notification_id = """ + str(responseData['data'][i]['notification_id']))
        for row1 in dbResult:
                joblist.append(str(row1[0])) 
                joblist.append(str(row1[2])) 
    return joblist

def getJobIdFromJoborder(joid):
    job_no=0
    logHandler.logging.info("Thread sleep for 60 sec..")
    time.sleep(60)
    query = "select job_no from bp.jobs where schedule_id="+str(joid) + " order by job_no desc"
    logHandler.logging.info("Get Job Id from Joborder query: " + str(query))
    dbResult = dbHandler.executeQuery(query)
    logHandler.logging.info("DB Result returned: " + str(dbResult))
    #print """select job_no from bp.jobs where schedule_id="""+str(joid)
    for row1 in dbResult:
        job_no=str(row1[0])
        #print "!!!!!!!" + str(job_no)
    return job_no
 
def getReplicationBackupStatus(backupid):
    query = "select status from bp.replication_queue where backup_no=" + str(backupid)
    dbResult = dbHandler.executeQuery(query)    
    status = dbResult[0][0]
    if(status == 0):
        return None
    else:
        return status   
    
def getReplicationBackupStatusAtSource(backupid):
    query = "select status from bp.replication_queue where backup_no=" + str(backupid)
    dbResult = dbHandler.executeQuery(query,"source")    
    status = dbResult[0][0]
    if(status == 0):
        return None
    else:
        return status   
            
def getReplicationJobID(backupid):
    position = 0
    query = "select position from bp.replication_queue where backup_no=" + str(backupid)
    dbResult = dbHandler.executeQuery(query)    
    position = dbResult[0][0]
    logHandler.logging.info("Position")
    return position 

def getReplicationJobIDAtSource(backupid):
    position = 0
    query = "select position from bp.replication_queue where backup_no=" + str(backupid)
    dbResult = dbHandler.executeQuery(query,"source")    
    position = dbResult[0][0]
    logHandler.logging.info("Position")
    return position   
'''
Created on Oct 20, 2014

@author: Amey.k
'''


from util import linuxMachineConnect,dbHandler
import json


def getLicenseInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Inst Date" | cut -c11-34')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Exp Date" | cut -c10-19')
    temp = (out.readlines())
    
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep Feature | cut -c9-53')
    #print str(out.readlines())
    temp = (out.readlines())
    
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "License Key" | cut -c13-52')
    #print str(out.readlines())
    temp = (out.readlines())
    
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    print featurelist
    return featurelist

def getReqLicenseInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    featurelist = []
#     _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Asset Tag"')
#     temp = out.readlines()
#     temp=(str(temp[0]).strip(' \r\t\n'))
#     print temp
#     temp=temp.split("|")
#     featurelist.append(temp[1])
    
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Client Lock"')
    temp = out.readlines()
    print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    
    temp=temp.split("|")
    featurelist.append(temp[1])
    
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Daemon Host"')
    #print str(out.readlines())
    temp = out.readlines()
    print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    
    temp=temp.split("|")
    featurelist.append(temp[1])
    
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Daemon Host-ID"')
    temp = out.readlines()
    print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    temp=temp.split("|")
    featurelist.append(temp[1])
    
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Serial Number"')
    temp = out.readlines()
    print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    temp=temp.split("|")
    featurelist.append(temp[1])
    
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Vendor Info"')
    temp = out.readlines()
    print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    temp=temp.split("|")
    featurelist.append(temp[1])
    
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "License Type"')
    temp = out.readlines()
    print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    temp=temp.split("|")
    featurelist.append(temp[1])
    
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Feature"')
    temp = out.readlines()
    print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    temp=temp.split("|")
    featurelist.append(temp[1])
    
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Daemon IP port"')
    temp = out.readlines()
    print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    temp=temp.split("|")
    featurelist.append(temp[1])
        
    print featurelist
    return featurelist

def getReqLicenseInformationFromResponse(input_json):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    #jsonParameter.append(str(jsonData['request']['registration']['AssetTag']))
    jsonParameter.append(str(jsonData['request']['registration']['ClientLock']))
    jsonParameter.append(str(jsonData['request']['registration']['DaemonHost']))
    jsonParameter.append(str(jsonData['request']['registration']['DaemonHostID']))
    jsonParameter.append(str(jsonData['request']['registration']['SerialNumber']))
    jsonParameter.append(str(jsonData['request']['registration']['Vendor information']))
    jsonParameter.append(str(jsonData['request']['registration']['Type']))
    jsonParameter.append(str(jsonData['request']['registration']['FeatureString']))
    jsonParameter.append(str(jsonData['request']['registration']['DaemonIP']))
    
    return jsonParameter
def getSummaryLicenseInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Feature"')
    temp = out.readlines()
    temp=(str(temp[0]).strip(' \r\t\n'))
    print temp
    temp=temp.split("|")
    temp=temp[1].split(",")
    featurelist.append(temp[0]) 
    featurelist.sort()
    return featurelist

def getSummaryLicenseInformationFromResponse(input_json):
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['title']))
    print jsonParameter
    return jsonParameter

def getUsedAndLiscFromResponse(input_json):
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for i in range(0,len(jsonData['license'])):
        jsonParameter.append(str(jsonData['license'][i]['sid']))
        jsonParameter.append(str(jsonData['license'][i]['license']))
    print "Values from DB: ", jsonParameter
    jsonParameter.sort()
    return jsonParameter

def getLicenseInformationFromResponse(input_json):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['install_date']))
    jsonParameter.append(str(jsonData['expiration_date']))
    jsonParameter.append(str(jsonData['feature_string']))
    jsonParameter.append(str(jsonData['key']))
    print jsonParameter
    return jsonParameter    

def getSysDetailsForLiscence(sysID):
    sysLiscDetails = []
    dbResult = dbHandler.executeQuery("SELECT system_id,name from bp.systems where system_id=" +str(sysID))
    for row in dbResult:
        sysLiscDetails.append(str(row[0]))
        sysLiscDetails.append(str(row[1]))
    sysLiscDetails.sort()    
    return sysLiscDetails
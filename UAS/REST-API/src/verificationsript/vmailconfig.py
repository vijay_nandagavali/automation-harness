'''
Created on Feb 19, 2015

@author: Rahul A.
'''


from util import linuxMachineConnect
import json

# obtain authinfo, bp and failure parameters by executing cmc_mail command
def getMailConfigInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_mail showsmtp | grep "authinfo" | cut -c25-25')
    temp = out.readlines()
    strAuthInfo = str(temp[0].strip(' \r\t\n')) 
    if(strAuthInfo == "F"):
        featurelist.append("False")
    else:
        featurelist.append("True")
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_mail get all | grep "bp" | cut -c4-8')
    temp = out.readlines()
    strBpInfo = str(temp[0].strip(' \r\t\n'))
    featurelist.append(strBpInfo)
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_mail get all | grep "failure" | cut -c9-13')
    temp = out.readlines()
    strFailureInfo = str(temp[0].strip(' \r\t\n'))
    featurelist.append(strFailureInfo)
    return featurelist

def getMailScheduleInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_mail get all | grep "schedule" | cut -c10-10')
    temp = out.readlines()
    strScheduleInfo = str(temp[0].strip(' \r\t\n'))
    return strScheduleInfo

# Build a JSONcontaining authinfo, bp and failure parameters
def getMailConfigInformationFromResponse(input_json):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['data']['authinfo']))
    jsonParameter.append(str(jsonData['data']['bp']))
    jsonParameter.append(str(jsonData['data']['failure']))
    #jsonParameter.append(str(jsonData['key']))
    print jsonParameter
    return jsonParameter


    
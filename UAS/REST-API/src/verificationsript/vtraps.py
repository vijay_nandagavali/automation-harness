'''
Created on Oct 30, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''
from util import dbHandler
from util import jsonHandler
import json

def getAllHistoryFromDB():
    dbResult = dbHandler.executeDBDictQuery("""select oid,destination,community,traptype,object,description,severity,status from bp.snmp_history ORDER BY id DESC """)
    jsonList = []
    #dbResult = dbHandler.executeQuery("""select oid from bp.snmp_history ORDER BY id DESC """)
    for row in dbResult:
        jsonList.append(row["oid"])
        jsonList.append(row["destination"])
        jsonList.append(row["community"])
        jsonList.append(row["traptype"])
        jsonList.append(row["object"])
        jsonList.append(row["description"])
        jsonList.append(row["severity"])
        jsonList.append(row["status"])
    #print jsonList
    return jsonList
    
def getHistoryCountFromDB():
    dbResult = dbHandler.executeQuery("""select count(*) from bp.snmp_history """)
    print dbResult[0][0]
    return dbResult[0][0] 



def getAllHistoryFromResponse(input_json,count):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for i in range(0,count):
        jsonParameter.append(str(jsonData[str(i)]['oid']))
        jsonParameter.append(str(jsonData[str(i)]['destination']))
        jsonParameter.append(str(jsonData[str(i)]['community']))
        jsonParameter.append(str(jsonData[str(i)]['traptype']))
        jsonParameter.append(str(jsonData[str(i)]['object']))
        jsonParameter.append(str(jsonData[str(i)]['description']))
        jsonParameter.append(jsonData[str(i)]['severity'])
        jsonParameter.append(jsonData[str(i)]['status'])
    #print jsonParameter
    return jsonParameter
    
#getAllHistoryFromDB()

def getTrapsParametersFromResponse(actualResponse):
    return jsonHandler.getTrapHistoryParametersInJson(actualResponse)

def getTrapsDataFromResponse(actualResponse):
    return jsonHandler.getTrapsResponseFromJson(actualResponse) 

def getTrapsDataFromDB():
    dbResult = dbHandler.executeQuery("select * from bp.snmp_config")
    dataFromDB = []
    if(len(dbResult) > 0):
        for row in dbResult:
            dataFromDB.append(row[0])
            dataFromDB.append(row[1])
            dataFromDB.append(row[2])
            if(row[3] == True):
                dataFromDB.append(1)
            else:
                dataFromDB.append(0)
    dataFromDB.sort()
    return dataFromDB

def getTrapIdFromDB():
    dbResult = dbHandler.executeQuery("select id from bp.snmp_config")
    if (len(dbResult) > 0):
        return dbResult[0][0]
    else:
        return 0

def getTestTrapFromDB():
    dbResult = dbHandler.executeQuery("select traptype from bp.snmp_history where id = (select MAX(id) from bp.snmp_history)")  
    return dbResult[0][0]  
'''
Created on Sept 2, 2015

@author: Nikhil S
'''

import json


from util import linuxMachineConnect,jsonHandler

def getUpdatesFromResponse(actualResponse):
    jsonData = json.loads(actualResponse)  
    updatesList = []
    for i in range(0,len(jsonData['updates']['updates'])):
        updatesList.append(str(jsonData['updates']['updates'][i]['name']))
        updatesList.append(str(jsonData['updates']['updates'][i]['arch']))
        updatesList.append(str(jsonData['updates']['updates'][i]['version']))
    updatesList.sort()
    return updatesList

def getUpdatesFromScript(count):
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    updatesList = []
    
    _, out, err = conn.exec_command('cmc_updates list| awk -F \' \' \'{print $1}\'')
    temp = out.readlines()
    for i in range(0,int(count)):
        updatesList.append(str(temp[i]).strip(' \r\t\n'))
    
    _, out, err = conn.exec_command('cmc_updates list| awk -F \' \' \'{print $2}\'')
    temp = out.readlines()
    for i in range(0,int(count)):
        updatesList.append(str(temp[i]).strip(' \r\t\n'))
        
    _, out, err = conn.exec_command('cmc_updates list| awk -F \' \' \'{print $3}\'')
    temp = out.readlines()
    for i in range(0,int(count)):
        updatesList.append(str(temp[i]).strip(' \r\t\n'))    
    updatesList.sort()
    return updatesList        
'''
Created on Oct 1, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''

import json

from util import dbHandler
from util import jsonHandler
from testdata import clients_testdata


def getVirtualClientListFromDB(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    clientID = str(responseData['data'][0]['wir']['data'][0]["virtual_id"])
    dbResult = dbHandler.executeQuery("""SELECT current_state from bp.virtual_clients where virtual_id=""" +clientID)
    cllist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT vm_name from bp.virtual_clients where virtual_id=""" +clientID)
    cllist.append(dbResult[0][0])
    cllist.sort()
    return cllist

def getVirtualClientListFromResponse(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    name = str(responseData['data'][0]['wir']['data'][0]['vm_name'])
    cllist.append(name)
    mode = responseData['data'][0]['wir']['data'][0]['Mode']
    cllist.append(mode)

    cllist.sort()
    return cllist

def getVirtualClientDetailsByIDFromDB(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    VirtualclientID = str(responseData['virtual_client']["virtual_id"])
    RealclientID = str(responseData['real_client'][0]["real_client_id"])
#     dbresult =dbHandler.executeQuery("""select vm_name from bp.virtual_clients where virtual_id =""" + VirtualclientID)
#     for row in dbresult:
#         cllist.append(row[0])
    dbresult =dbHandler.executeQuery("""select memory from bp.virtual_clients where virtual_id =""" + VirtualclientID)
    cllist.append(str(dbresult[0][0]))    
    dbresult =dbHandler.executeQuery("""select node_name from bp.nodes where node_no=""" + RealclientID)
    cllist.append(dbresult[0][0])
    cllist.sort()
    return cllist

def getVirtualClientDetailsByIDFromResponse(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    
    #vm_name = str(responseData['virtual_client']["VMName"])
    #cllist.append(vm_name)
    vm_memory = str(responseData['virtual_client']["Memory"])
    cllist.append(vm_memory)
    client_name = str(responseData['real_client'][0]["real_client_name"])
    cllist.append(client_name)
    
    cllist.sort()
    return cllist

def getVirtualClientIDFromDB(nodeID):
    vid=0
    try:
        dbresult =dbHandler.executeQuery("""select virtual_id from bp.virtual_clients where node_no=""" + nodeID)
        vid = dbresult[0][0]
    except:
        print "Virtual id not found"
    return vid

def getVirtualClientStateFromDB(nodeID):
    vid=0
    try:
        dbresult =dbHandler.executeQuery("""select pending_state from bp.virtual_clients where node_no=""" + nodeID)
        vid=dbresult[0][0]
    except:
        print "Virtual id not found"
    return vid

def getPostRequestResponse(actualResponse):
    return jsonHandler.postResponseParse(actualResponse)  

def getVirtualClientCandidatesFromDB(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    no_candidates = len(responseData['data'][0]['candidates'])
    for i in range(0,no_candidates):
        #dbResult = dbHandler.executeQuery("""select n.node_no,n.node_name,n.os,b.backup_no from bp.nodes n left outer join bp.backups b on n.node_no = b.node_no where n.node_no = """ + str(responseData['data'][0]['Candidates'][i]["ClientID"]))
        try:
            if(responseData['data'][0]['candidates'][i]["backup_id"]):
                dbResult1 = dbHandler.executeQuery("""select b.backup_no from bp.nodes n left outer join bp.backups b on n.node_no = b.node_no where n.node_no = """ + str(responseData['data'][0]['candidates'][i]["client_id"]) + """ and b.backup_no= """ + str(responseData['data'][0]['candidates'][i]["backup_id"]))
                for row1 in dbResult1:
                    cllist.append(str(row1[0])) 
       
        except:
            print "Backup ID not present for this particular client."
        try:
            if(responseData['data'][0]['candidates'][i]["VirtualID"]):
                dbResult1 = dbHandler.executeQuery("""select v.virtual_id from bp.virtual_clients v left outer join bp.nodes n on v.node_no = n.node_no where n.node_no =  """ + str(responseData['data'][0]['candidates'][i]["client_id"]) + """ and v.virtual_id= """ + str(responseData['data'][0]['candidates'][i]["VirtualID"]))
                for row1 in dbResult1:
                    cllist.append(str(row1[0]))  
        except:
            print "virtual ID not present for this particular client."           
        dbResult = dbHandler.executeQuery("""select n.node_no,n.node_name,n.os from bp.nodes n where n.node_no = """ + str(responseData['data'][0]['candidates'][i]["client_id"]))
        for row1 in dbResult:
                cllist.append(str(row1[0])) 
                cllist.append(str(row1[1])) 
                cllist.append(str(row1[2])) 
                #cllist.append(str(row1[3]))
       # dbResult1 = dbHandler.executeQuery("""select v.virtual_id from bp.virtual_clients v left outer join bp.nodes n on v.node_no = n.node_no where n.node_no =  """ + str(responseData['data'][0]['Candidates'][i]["ClientID"]))
       # for row2 in dbResult:
           # cllist.append(str(row2[0]))
    cllist.sort()
    return cllist

def getVirtualClientCandidatesFromResponse(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    no_candidates = len(responseData['data'][0]['candidates'])
    for i in range(0,no_candidates):
        try:
            backupid = str(responseData['data'][0]['candidates'][i]["backup_id"])
            cllist.append(backupid)
        except:    
            print "Backup ID not present for this particular client."    
        try:    
            virtualid = str(responseData['data'][0]['candidates'][i]["VirtualID"])
            cllist.append(virtualid)
        except:    
            print "virtual ID not present for this particular client."
        clientid = str(responseData['data'][0]['candidates'][i]["client_id"])
        cllist.append(clientid)
        clientname = str(responseData['data'][0]['candidates'][i]["client_name"])
        cllist.append(clientname)
        clientOS = str(responseData['data'][0]['candidates'][i]["client_os_id"])
        cllist.append(clientOS)
    
    
    cllist.sort()
    return cllist


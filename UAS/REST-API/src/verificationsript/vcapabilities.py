'''
Created on Mar 11, 2015

@author: Amey.k
'''

import json
from util import linuxMachineConnect, logHandler


def getCapabilitiesFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    logHandler.logging.info("Connection: " + str(conn))
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_capabilities get CE')
    temp = out.readlines()
    if(str(temp[0]).strip(' \r\t\n') == "CE"):
        featurelist.append(True)
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_capabilities get virtual_machine')
    temp = out.readlines()
    if(str(temp[0]).strip(' \r\t\n') == "virtual_machine"):
        featurelist.append(True)
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_capabilities get open_license')
    temp = out.readlines()
    if(str(temp[0]).strip(' \r\t\n') == "open_license"):
        featurelist.append(True)
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_capabilities get ui')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_capabilities get vmware_ir')
    temp = out.readlines()
    if(str(temp[0]).strip(' \r\t\n') == "vmware_ir"):
        featurelist.append(True)
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_capabilities get hyperv_ir')
    temp = out.readlines()
    if(str(temp[0]).strip(' \r\t\n') == "hyperv_ir"):
        featurelist.append(True)
#     _, out, err = conn.exec_command('/usr/bp/bin/cmc_capabilities get backup_strategies')
#     temp = out.readlines()
#     featurelist.append(str(temp[0]).strip(' \r\t\n'))
#     _, out, err = conn.exec_command('/usr/bp/bin/cmc_capabilities get backup_min_recurrence')
#     temp = out.readlines()
#     featurelist.append(str(temp[0]).strip(' \r\t\n'))
    return featurelist
    
def getCapabilitiesFromResponse(actualResponse):
    jsonData = json.loads(actualResponse)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for k,v in jsonData['data'][0]['capabilities'].items():
        if(k=='CE'):
            jsonParameter.append(jsonData['data'][0]['capabilities']['CE'])
    jsonParameter.append(jsonData['data'][0]['capabilities']['virtual_machine'])
    jsonParameter.append(jsonData['data'][0]['capabilities']['open_license'])
    jsonParameter.append(jsonData['data'][0]['capabilities']['ui'])
    jsonParameter.append(jsonData['data'][0]['capabilities']['vmware_ir'])
    jsonParameter.append(jsonData['data'][0]['capabilities']['hyperv_ir'])
    #jsonParameter.append(jsonData['data'][0]['capabilities']['backup_strategies'])
    #jsonParameter.append(jsonData['data'][0]['capabilities']['backup_min_recurrence'])
    return jsonParameter
        
    
    
    
    

        
'''
Created on Sep 18, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''

from testdata import summary_testdata
from util import dbHandler
from util import jsonHandler
import datetime
import json

def getSummaryCountsFromDB():
    summaryResParamsList = summary_testdata.responseParameter
    print "summaryResParamsList"+str(summaryResParamsList)
    query = """SELECT """ +summary_testdata.responseParameter[0]+ """,""" +summary_testdata.responseParameter[1]+ """,""" +summary_testdata.responseParameter[2]+ """,""" +summary_testdata.responseParameter[3]+ """,""" +summary_testdata.responseParameter[5]+ """,""" +summary_testdata.responseParameter[8]+ """,""" +summary_testdata.responseParameter[9]+ """,""" +summary_testdata.responseParameter[10]+ """,""" +summary_testdata.responseParameter[12]+ """,""" +summary_testdata.responseParameter[14]+ """,""" +summary_testdata.responseParameter[15]+ """,""" +summary_testdata.responseParameter[16]+ """,""" +summary_testdata.responseParameter[19]+ """,""" +summary_testdata.responseParameter[20]+ """ from bp.summary_counts"""
    print "query created"
    dbResult = dbHandler.executeQuery(query)
    print "executed: "+str(dbResult)
    summCoutList = []
    for i in range(0,14):
        summCoutList.append(dbResult[0][i]) 
    return summCoutList



def getBackupSummaryCountsFromDB():
    summaryResParamsList = summary_testdata.responseParameter
    print "summaryResParamsList"+str(summaryResParamsList)
    query = """SELECT """ +summary_testdata.responseParameter1[0]+ """,""" +summary_testdata.responseParameter1[1]+ """,""" +summary_testdata.responseParameter1[3]+ """,""" +summary_testdata.responseParameter1[5]+ """,""" +summary_testdata.responseParameter1[6]+ """ from bp.summary_counts"""
    print "query created"
    dbResult = dbHandler.executeQuery(query)
    print "executed: "+str(dbResult)
    summCoutList = []
    for i in range(0,5):
        summCoutList.append(dbResult[0][i]) 
    return summCoutList    
    
    #return dbResult


def getSummaryCountsFromResponse(actualResponse):
    
    dict1 = jsonHandler.getDictionaryFromJson(actualResponse)
    summaryapi = []
    
    summary = dict1.get(summary_testdata.responseParameter[0])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[1])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[2])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[3])
    summaryapi.append(summary)
    #summary = dict1.get(summary_testdata.responseParameter[4])
    #summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[5])
    summaryapi.append(summary)
    #summary = dict1.get(summary_testdata.responseParameter[6])
    #summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[8])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[9])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[10])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[12])
    summaryapi.append(summary)
    #summary = dict1.get(summary_testdata.responseParameter[12])
    #summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[14])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[15])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[16])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[19])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter[20])
    summaryapi.append(summary)
    
        
    return summaryapi

def getBackupSummaryCountsFromResponse(actualResponse):
    
    dict1 = jsonHandler.getDictionaryFromJson(actualResponse)
    summaryapi = []
    
    summary = dict1.get(summary_testdata.responseParameter1[0])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter1[1])
    summaryapi.append(summary)
    #summary = dict1.get(summary_testdata.responseParameter[2])
    #summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter1[3])
    summaryapi.append(summary)
    #summary = dict1.get(summary_testdata.responseParameter[4])
    #summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter1[5])
    summaryapi.append(summary)
    summary = dict1.get(summary_testdata.responseParameter1[6])
    summaryapi.append(summary)
           
    return summaryapi

def getDateList(no_days):
    today = datetime.date.today()
    dateList = []
    for i in range(0,no_days):
        oneDay = datetime.timedelta(days=i)
        oneWeek = today - oneDay
        dateList.append(str(oneWeek))
        dateList.sort()
    print dateList
    return dateList

def getSummaryDaysFromResponse(input_json):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    print "in getSummaryDaysFromResponse"
    print "input_json: "+str(input_json)
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    print "json data: "+str(jsonData)
    
    llen = len(jsonData['data'][0]['backup_speed'])
    print "llen1: "+str(llen)
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][0]['backup_speed'][i]['day']))
    print "json parameter with backup_speed: "+str(jsonParameter)
    llen = len(jsonData['data'][1]['backup_size'])
    print "llen2: "+str(llen)
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][1]['backup_size'][i]['day']))
    print "json parameter with backup_size: "+str(jsonParameter)
    llen = len(jsonData['data'][2]['backup_data_speed'])
    print "llen3: "+str(llen)
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][2]['backup_data_speed'][i]['day']))
    print "json parameter with backup_data_speed: "+str(jsonParameter)
    llen = len(jsonData['data'][3]['replication_speed'])
    print "llen4: "+str(llen)
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][3]['replication_speed'][i]['day']))
    print "json parameter with replication_speed: "+str(jsonParameter)
#     llen = len(jsonData['data'][6]['restore_speed'])
#     print "llen5: "+str(llen)
#     for i in range(0,llen):
#         jsonParameter.append(str(jsonData['data'][6]['restore_speed'][i]['day']))
#     print "json parameter with restore_speed: "+str(jsonParameter)
#     llen = len(jsonData['data'][7]['archive_size'])
#     print "llen6: "+str(llen)
#     for i in range(0,llen):
#         jsonParameter.append(str(jsonData['data'][7]['archive_size'][i]['day']))
#     print "json parameter with archive_size: "+str(jsonParameter)
#     llen = len(jsonData['data'][8]['archive_speed'])
#     print "llen7: "+str(llen)
#     for i in range(0,llen):
#         jsonParameter.append(str(jsonData['data'][8]['archive_speed'][i]['day']))
    print "jsonParameer"+str(jsonParameter)
    return jsonParameter

def getBackupSummaryDaysFromResponse(input_json):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    
    llen = len(jsonData['data'][0]['backup_speed'])
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][0]['backup_speed'][i]['day']))
    llen = len(jsonData['data'][1]['backup_size'])
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][1]['backup_size'][i]['day']))
    llen = len(jsonData['data'][2]['backup_data_speed'])
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][2]['backup_data_speed'][i]['day']))
    return jsonParameter

def getRestoreSummaryDaysFromResponse(input_json):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    llen = len(jsonData['data'][0]['restore_speed'])
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][0]['restore_speed'][i]['day']))
    return jsonParameter
    
def getReplicationSummaryDaysFromResponse(input_json):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    llen = len(jsonData['data'][0]['replication_speed'])
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][0]['replication_speed'][i]['day']))
    return jsonParameter

def getArchiveSummaryDaysFromResponse(input_json):
    
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    llen = len(jsonData['data'][0]['archive_size'])
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][0]['archive_size'][i]['day']))
    llen = len(jsonData['data'][1]['archive_speed'])
    for i in range(0,llen):
        jsonParameter.append(str(jsonData['data'][1]['archive_speed'][i]['day']))
    print jsonParameter
    return jsonParameter
    
    
def getSummaryCurrentFromDB():
    dbResult = dbHandler.executeQuery("SELECT count(*) FROM bp.alerts WHERE closed = false")
    summarycurrentlist = []
    summarycurrentlist.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("SELECT progress_pct FROM bp.inventory_sync")
    for row1 in dbResult:
        summarycurrentlist.append(str(row1[0]))
    dbResult = dbHandler.executeQuery("SELECT final_status FROM bp.inventory_sync")
    for row1 in dbResult:
        summarycurrentlist.append(str(row1[0]))
    dbResult = dbHandler.executeQuery("SELECT last_message FROM bp.inventory_sync")
    for row1 in dbResult:
        summarycurrentlist.append(row1[0])
    return summarycurrentlist

def getSummaryCurrentFromResponse(input_json):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['alert_count']))
    jsonParameter.append(str(jsonData['sync_progress']))
    jsonParameter.append(str(jsonData['sync_status']))
    jsonParameter.append(str(jsonData['message']))
    return jsonParameter
    
    
    
    
 
    
    



        
        
    
    
    

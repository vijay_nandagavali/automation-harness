'''
Created on Feb 25, 2015

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''

import json


from util import dbHandler


def getForumFromDB(userId):
    forumList = []
    dbResult = dbHandler.executeQuery("""SELECT credential_id from bp.forum_users""")
    for row in dbResult:
        forumList.append(str(row[0]))
    return forumList


def getForumInformationFromDB(userId,credentialId):
    forumlist = []
    dbResult = dbHandler.executeQuery("""SELECT user_id from bp.users where user_id=""" +userId)
    forumlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT username from bp.users where user_id=""" +userId)
    forumlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT credential_id  from bp.client_credentials where credential_id=""" +credentialId)
    forumlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT display_name from bp.client_credentials where credential_id=""" +credentialId)
    forumlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT username from bp.client_credentials where credential_id=""" +credentialId)
    forumlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT domain from bp.client_credentials where credential_id=""" +credentialId)
    forumlist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT is_default from bp.client_credentials where credential_id=""" +credentialId)
    forumlist.append(dbResult[0][0])
    forumlist.sort()
    return forumlist

def getForumInformationFromResponse(actualResponse):
    forumlist = []
    responseData = json.loads(actualResponse)
    
    user_id = responseData['data'][0]['id']
    forumlist.append(user_id)
    user_name = responseData['data'][0]['username']
    forumlist.append(user_name)
    cred_id = responseData['data'][0]['credentials']['credential_id']
    forumlist.append(cred_id)
    cred_name = str(responseData['data'][0]['credentials']['display_name'])
    forumlist.append(cred_name)
    uname = responseData['data'][0]['credentials']['username']
    forumlist.append(uname)
    domain = responseData['data'][0]['credentials']['domain']
    forumlist.append(domain)
    is_default = responseData['data'][0]['credentials']['is_default']
    forumlist.append(is_default)

    forumlist.sort()
    return forumlist
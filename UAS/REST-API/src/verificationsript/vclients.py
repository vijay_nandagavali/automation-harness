'''
Created on Oct 1, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''

import json
from util import dbHandler, jsonHandler, logHandler
from testdata import clients_testdata

def getClientListFromDB(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    for i in range(0,len(responseData['data'])):
        clientID = str(responseData['data'][i]['id'])
        if(clientID.isdigit()):
            dbResult = dbHandler.executeQuery("""SELECT node_no from bp.nodes where node_no=""" +clientID)
            cllist.append(dbResult[0][0])
            dbResult = dbHandler.executeQuery("""SELECT node_name from bp.nodes where node_no=""" +clientID)
            cllist.append(dbResult[0][0])
            dbResult = dbHandler.executeQuery("""SELECT type from bp.nodes where node_no=""" +clientID)
            cllist.append(dbResult[0][0])
            dbResult = dbHandler.executeQuery("""SELECT os from bp.nodes where node_no=""" +clientID)
            cllist.append(dbResult[0][0])
            dbResult = dbHandler.executeQuery("""SELECT priority from bp.nodes where node_no=""" +clientID)
            cllist.append(dbResult[0][0])
            #dbResult = dbHandler.executeQuery("""SELECT version from bp.nodes where node_no=""" +clientID)
            #for row in dbResult:
            #    cllist.append(row[0])
    cllist.sort()
    return cllist

def getSpecificClientDetailsFromDB(clientID):
    cllist = []
    dbResult = dbHandler.executeQuery("""SELECT node_no from bp.nodes where node_no=""" +str(clientID))
    cllist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT node_name from bp.nodes where node_no=""" +str(clientID))
    cllist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT type from bp.nodes where node_no=""" +str(clientID))
    cllist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT os from bp.nodes where node_no=""" +str(clientID))
    cllist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT priority from bp.nodes where node_no=""" +str(clientID))
    cllist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT version from bp.nodes where node_no=""" +str(clientID))
    cllist.append(dbResult[0][0])
    cllist.sort()
    return cllist
    
def getClientCountFromDB():
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.nodes""")
    return dbResult[0][0]

def getClientNamePriorityFromDB(cid):
    cllist = []
    dbResult = dbHandler.executeQuery("""SELECT node_name from bp.nodes where node_no=""" +str(cid))
    cllist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT priority from bp.nodes where node_no=""" +str(cid))
    cllist.append(dbResult[0][0])
    return cllist
       
def getClientListFromResponse(actualResponse,count):
    cllist = []
    responseData = json.loads(actualResponse)
    for i in range(0,len(responseData['data'])):
        id1 = responseData['data'][i]['id']
        if(str(id1).isdigit()):
            cllist.append(id1)
            name = str(responseData['data'][i]['name'])
            cllist.append(name)
            mtypeid = responseData['data'][i]['machine_type_id']
            cllist.append(mtypeid)
            ostypeid = responseData['data'][i]['os_type_id']
            cllist.append(ostypeid)
            priority = responseData['data'][i]['priority']
            cllist.append(priority)
            #ver =  str(responseData['data'][i]['version'])
            #cllist.append(ver)
    cllist.sort()
    return cllist

def getSpecificClientDetailsFromResponse(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    id1 = responseData['data'][0]['id']
    cllist.append(id1)
    name = str(responseData['data'][0]['name'])
    cllist.append(name)
    mtypeid = responseData['data'][0]['machine_type_id']
    cllist.append(mtypeid)
    ostypeid = responseData['data'][0]['os_type_id']
    cllist.append(ostypeid)
    priority = responseData['data'][0]['priority']
    cllist.append(priority)
    ver =  str(responseData['data'][0]['version'])
    cllist.append(ver)
    cllist.sort()
    return cllist

def getAgentCheckSpecificClientDetailsFromDB():
    cllist = []
    dbResult = dbHandler.executeQuery("""SELECT node_no from bp.nodes""")
    cllist.append(dbResult[0][0])
    dbResult = dbHandler.executeQuery("""SELECT node_name from bp.nodes""")
    cllist.append(dbResult[0][0])
    #dbResult = dbHandler.executeQuery("""SELECT version from bp.nodes""")
    #for row in dbResult:
    #    cllist.append(row[0])
#     dbResult = dbHandler.executeQuery("""SELECT fstatus from bp.nodes""")
#     for row in dbResult:
#         cllist.append(row[0])
#     dbResult = dbHandler.executeQuery("""SELECT last_update from bp.nodes""")
#     for row in dbResult:
#         cllist.append(row[0])
    cllist.sort()
    return cllist

def getAgentCheckSpecificClientDetailsFromResponse(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    for values in responseData['clients']:
        id1 = values['id']
        cllist.append(id1)
        name = values['name']
        cllist.append(name)
        #version = values['version']
        #cllist.append(version)
#         address = values['address']
#         cllist.append(address)
#         update_available = values['update_available']
#         cllist.append(update_available)        
    cllist.sort()
    return cllist
    


def getClientByIdFromDB(cid):
    strQuery = "select count(*) from bp.nodes where node_no=" + str(cid)
    dbResult = dbHandler.executeQuery(strQuery)
    cid = dbResult[0][0]
    return cid

def getSpecificClientByNameDB(appliance_ip,clientName):
    cid=0
    try:
        strQuery = "select node_no from bp.nodes where node_name=" + "'"+str(clientName)+"'"
        dbResult = dbHandler.executeQuery(appliance_ip,strQuery)
        cid = dbResult[0][0]
    except:
        logHandler.logging.info("Client not present")
    return cid

def getSpecificClientAtSourcecByNameDB(clientName):
    cid=0
    try:
        strQuery = "select node_no from bp.nodes where node_name=" + "'"+str(clientName)+"'"
        dbResult = dbHandler.executeQuery(strQuery,"source")
        cid = dbResult[0][0]
    except:
        logHandler.logging.info("Client not present")
    return cid
    

def getClientFileListFromDB(cid):
    cllist = []
    dbResult = dbHandler.executeQuery("""select volume from bp.node_volumes where node_no=""" +str(cid))
    for row in dbResult:
        cllist.append(row[0])
    cllist.sort()
    return cllist

def getClientFileListFromResponse(actualResponse):
    cllist = []
    responseData = json.loads(actualResponse)
    for i in range(0,len(responseData['data'])):
        fid = responseData['data'][i]['id']
        cllist.append(str(fid))
    cllist.sort()
    return cllist

def getClientFileListFromDB1(directory):
    cllist = []
    dbResult = dbHandler.executeQuery("""select dname from bp.dirs where dname like""" +"'"+str(directory)+"'")
    for row in dbResult:
            cllist.append(row[0])
    cllist.sort()
    return cllist
  
def getSpecificEsxServerFromDB(appliance_ip, esx_server_name):
    uuid = 0
    query = "select esx_uuid from bp.vmware_esx_servers where name = '" + str(esx_server_name) + "'"
    dbResult = dbHandler.executeQuery(appliance_ip, query)
    if(len(dbResult) > 0):
        uuid = dbResult[0][0]
    else:
        return 0
    return uuid
    
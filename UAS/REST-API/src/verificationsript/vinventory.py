'''
Created on Mar 2, 2015

@author: Amey.k
'''

import json

from util import dbHandler, jsonHandler, logHandler
from testdata import inventory_testdata, clients_testdata, backups_testdata

def getIventorySyncFromDB():
    syncList = []
    dbResult = dbHandler.executeQuery("""SELECT final_status from bp.inventory_sync""")
    for row in dbResult:
        syncList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT progress_pct from bp.inventory_sync""")
    for row in dbResult:
        syncList.append(str(row[0]))
        dbResult = dbHandler.executeQuery("""SELECT last_message from bp.inventory_sync""")
    for row in dbResult:
        syncList.append(str(row[0]))
    syncList.sort()
    return syncList

def getInventorySyncFromResponse(actualResponse):
    syncList = []
    responseData = json.loads(actualResponse)
    sync_progress = str(responseData['sync_progress'])
    syncList.append(sync_progress)
    sync_status = str(responseData['sync_status'])
    syncList.append(sync_status)
    message = str(responseData['message'])
    syncList.append(message)
    syncList.sort()
    return syncList

def getInventoryParametersFromResponse(actualResponse):
    response = jsonHandler.getListOfParameterInJson(actualResponse, False)
    return response

def getInventoryResponseFromJson(actualResponse):
    return jsonHandler.getInventoryInfoFromJson(actualResponse)

def getInventoryDataFromDB():
    inventoryData = []
    hyperVcid = 0
    vmwarecid = 0
    id = ""
    dbResult = dbHandler.executeQuery("select system_id, name from bp.systems")
    if(len(dbResult) > 0):
        sid = dbResult[0][0]
        inventoryData.append(sid)
        inventoryData.append(dbResult[0][1])
    
    dbResult = dbHandler.executeQuery("select node_no, node_name from bp.nodes where node_name = '" + str(inventory_testdata.hyperVInstanceName) + "'")
    if(len(dbResult) > 0):
        id = str(sid) + "_" + str(dbResult[0][0])
        hyperVcid = dbResult[0][0]
        logHandler.logging.info("HyperV Client ID: " + str(hyperVcid))
        inventoryData.append(id)
        inventoryData.append(dbResult[0][1])
    
    if(hyperVcid):
        dbResult = dbHandler.executeQuery("select distinct a.node_name, b.app_id, b.name from bp.nodes a, bp.application_lookup b, bp.application_instances c where a.node_no = " + str(hyperVcid) + " and a.node_no = c.node_no and b.app_id = c.app_id and c.app_id <> " + str(inventory_testdata.app_id))
        if(len(dbResult) > 0):
            id = id + "_" + str(dbResult[0][1])
            name = str(dbResult[0][0]) + "'s " + str(dbResult[0][2])
            inventoryData.append(id)
            inventoryData.append(name)
    
        dbResult = dbHandler.executeQuery("select distinct c.instance_id, c.key1 from bp.nodes a, bp.application_lookup b, bp.application_instances c where a.node_no = " + str(hyperVcid) + " and a.node_no = c.node_no and b.app_id = c.app_id and c.app_id <> " + str(inventory_testdata.app_id) + " and c.current = 't'")
        if(len(dbResult) > 0):
            for row in dbResult:
                inventoryData.append(str(id) + "_" + str(row[0]))
                inventoryData.append(row[1])
    
    dbResult = dbHandler.executeQuery("select node_no from bp.nodes where node_name = '" + str(inventory_testdata.vmwareInstanceName) + "'")
    if(len(dbResult) > 0):
        vmwarecid = dbResult[0][0]
        logHandler.logging.info("Vmware Client ID: " + str(vmwarecid))
    
    if(vmwarecid):
        dbResult = dbHandler.executeQuery("select distinct b.app_id, esx_uuid, d.name from bp.application_lookup b, bp.application_instances c, bp.vmware_esx_servers d where c.node_no = " + str(vmwarecid) + " and b.app_id = c.app_id and b.app_id <> " + str(inventory_testdata.app_id))
        if(len(dbResult) > 0):
            id = str(sid) + "_" + str(vmwarecid) + "_" + str(dbResult[0][0]) + "_" + str(dbResult[0][1])
            uuid = dbResult[0][1]
            inventoryData.append(id)
            inventoryData.append(dbResult[0][2])
        
            if(uuid):
                dbResult = dbHandler.executeQuery("select resource_pool_key, resource_pool_name from bp.vmware_resource_pools where esx_uuid = '" + str(uuid) + "' and resource_pool_key <> '" + str(inventory_testdata.resource_pool_key) + "'")
                if(len(dbResult) > 0):
                    for row in dbResult:
                        inventoryData.append(str(id) + "_" + str(row[0]))
                        inventoryData.append(row[1])
            
                dbResult = dbHandler.executeQuery("select parent_key, name, instance_id from bp.vmware_vms where esx_uuid = '" + str(uuid) + "'")
                if(len(dbResult) > 0):
                    for row in dbResult:
                        inventoryData.append(str(id) + "_" + str(row[2]))
                        inventoryData.append(row[1])
        
    inventoryData.sort()
    return inventoryData

def getXenInventoryDataFromDB():
    inventoryData = []
    hyperVcid = 0
    id = ""
    dbResult = dbHandler.executeQuery("select system_id, name from bp.systems")
    if(len(dbResult) > 0):
        sid = dbResult[0][0]
        inventoryData.append(sid)
        inventoryData.append(dbResult[0][1])
        print "inventory1: "+str(inventoryData)
    
    dbResult = dbHandler.executeQuery("select node_no, node_name from bp.nodes where node_name = '" + str(inventory_testdata.name_xenserver) + "'")
    if(len(dbResult) > 0):
        id = str(sid) + "_" + str(dbResult[0][0])
        hyperVcid = dbResult[0][0]
        #logHandler.logging.info("Xen Client ID: " + str(hyperVcid))
        inventoryData.append(id)
        inventoryData.append(dbResult[0][1])
        print "inventory2: "+str(inventoryData)
    
    if(hyperVcid):
        dbResult = dbHandler.executeQuery("select distinct a.node_name, b.app_id, b.name from bp.nodes a, bp.application_lookup b, bp.application_instances c where a.node_no = " + str(hyperVcid) + " and a.node_no = c.node_no and b.app_id = c.app_id and c.app_id <> " + str(inventory_testdata.app_id))
        if(len(dbResult) > 0):
            id = id + "_" + str(dbResult[0][1])
            name = str(dbResult[0][0]) + "'s " + str(dbResult[0][2])
            inventoryData.append(id)
            inventoryData.append(name)
            print "inventory3: "+str(inventoryData)
    
        dbResult = dbHandler.executeQuery("select distinct c.instance_id, c.key2 from bp.nodes a, bp.application_lookup b, bp.application_instances c where a.node_no = " + str(hyperVcid) + " and a.node_no = c.node_no and b.app_id = c.app_id and c.app_id <> " + str(inventory_testdata.app_id) + " and c.current = 't'")
        if(len(dbResult) > 0):
            for row in dbResult:
                inventoryData.append(str(id) + "_" + str(row[0]))
                inventoryData.append(row[1])
                print "inventory4: "+str(inventoryData)
    
    dbResult = dbHandler.executeQuery("select node_no from bp.nodes where node_name = '" + str(backups_testdata.xenserverInstanceName) + "'")
    vmwarecid = 0
    if(len(dbResult) > 0):
        vmwarecid = dbResult[0][0]
        logHandler.logging.info("Vmware Client ID: " + str(vmwarecid))
        print "Vmware Client ID: " + str(vmwarecid)
    
    if(vmwarecid):
        dbResult = dbHandler.executeQuery("select distinct b.app_id, esx_uuid, d.name from bp.application_lookup b, bp.application_instances c, bp.vmware_esx_servers d where c.node_no = " + str(vmwarecid) + " and b.app_id = c.app_id and b.app_id <> " + str(inventory_testdata.app_id))
        if(len(dbResult) > 0):
            id = str(sid) + "_" + str(vmwarecid) + "_" + str(dbResult[0][0]) + "_" + str(dbResult[0][1])
            uuid = dbResult[0][1]
            inventoryData.append(id)
            inventoryData.append(dbResult[0][2])
            print "inventory5: "+str(inventoryData)
        
            if(uuid):
                dbResult = dbHandler.executeQuery("select resource_pool_key, resource_pool_name from bp.vmware_resource_pools where esx_uuid = '" + str(uuid) + "' and resource_pool_key <> '" + str(inventory_testdata.resource_pool_key) + "'")
                if(len(dbResult) > 0):
                    for row in dbResult:
                        inventoryData.append(str(id) + "_" + str(row[0]))
                        inventoryData.append(row[1])
                        print "inventory6: "+str(inventoryData)
            
                dbResult = dbHandler.executeQuery("select parent_key, name, instance_id from bp.vmware_vms where esx_uuid = '" + str(uuid) + "'")
                if(len(dbResult) > 0):
                    for row in dbResult:
                        inventoryData.append(str(id) + "_" + str(row[2]))
                        inventoryData.append(row[1])
                        print "inventory7: "+str(inventoryData)
        
    inventoryData.sort()
    print "inventory: "+str(inventoryData)
    return inventoryData
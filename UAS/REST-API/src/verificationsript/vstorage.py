'''
Created on Nov 21, 2014

@author: Amey.k, Savitha Peri

Modified on Jan 20, 2017

@author: Divya.J
'''

from util import linuxMachineConnect, logHandler, jsonHandler
import json
from testdata import storage_testdata
from util import dbHandler
from testdata.backups_testdata import storageType


def getStorageListFromDB():
    storageList = []
    dbResult = dbHandler.executeQuery("""SELECT storage_id from bp.storage""")
    for row in dbResult:
        storageList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT storage_name from bp.storage""")
    for row in dbResult:
        storageList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT storage_usage from bp.storage""")
    for row in dbResult:
        storageList.append(str(row[0]))
    storageList.sort()
    
    return storageList

def getStorageCountFromDB():
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.storage""")
    #print "Number of storage media are:"
    return dbResult[0][0]

def getStorageListFromResponse(actualResponse,count):
    
    storageList = []
    responseData = json.loads(actualResponse)
    for i in range(0,count):
        
        id1 = str(responseData['storage'][i]['id'])
        
        storageList.append(id1)
        name = str(responseData['storage'][i]['name'])
        
        storageList.append(name)
        storage_usage = str(responseData['storage'][i]['usage'])
        
        if (storage_usage == ""):
            storageList.append(str(None))
        else:
            storageList.append(storage_usage)
    
    storageList.sort()
    return storageList

def getUpdatedStorageListFromDB(storageid):
    #print storageid
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.storage where storage_id="""+str(storageid))
    #print dbResult
    #print "Number of storage media are:"
    return dbResult[0][0]

def getOnlineStatusFromDB(storageId):
    isOnline = ''
    dbResult = dbHandler.executeQuery("""SELECT is_online from bp.storage where storage_id="""+str(storageId))
    isOnline=str(dbResult[0][0])
    return str(isOnline)

def getStorageIdByName(storageName):
    storageId = 0
    dbResult = dbHandler.executeQuery("SELECT storage_id from bp.storage where storage_name='"+str(storageName)+"'")
    if(len(dbResult)):
        storageId = dbResult[0][0]
    else:
        logHandler.logging.info("Could not find the specified storage")
    return storageId

def getListOfAvailabeDiskFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    #print conn
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_internal available_disks')
    temp = out.readlines()
    temp=(str(temp[0]).strip(' \r\t\n'))
    #print temp
    temp=temp.split(":")
    featurelist.append(temp[0])
    featurelist.append(temp[1])
    #print featurelist
    return featurelist

def getListOfAvailabeDisk(actualResponse):
    storageList = []
    responseData = json.loads(actualResponse)
    name = str(responseData['attached_disks'][0]['name'])
    storageList.append(name)
    uuid = str(responseData['attached_disks'][0]['uuid'])
    storageList.append(uuid)
    storageList.sort()
    return storageList

def getTargetInformationISCSIFromResponse(actualResponse):    
    targetlist = []
    responseData = json.loads(actualResponse)
    no_targets = len(responseData['targets'])
    for i in range(0,no_targets):
        try:
            targetid = str(responseData['targets'][i])
            targetlist.append(targetid)
        except:    
            #print "target ID not present !"
            logHandler.logging.error("Target ID not present!")    
    targetlist.sort()
    return targetlist
    
def getTargetInformationISCSIFromDB(): 
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    command = "/usr/bp/bin/cmc_iscsi discover " + storage_testdata.host + " " + storage_testdata.port
    _, out, err = conn.exec_command(command)
    temp = out.readlines()
    #print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    
    targetlist=temp.split("|")
    #targetlist.append(temp[1])
           
    return targetlist  

def getTargetInformationLUNFromResponse(actualResponse):    
    lunslist = []
    responseData = json.loads(actualResponse)
    no_luns = len(responseData['luns'])
    for i in range(0,no_luns):
        try:
            lunid = str(responseData['luns'][i])
            lunslist.append(lunid)
        except:    
            #print "Luns not present !"
            logHandler.logging.error("Luns not present!")    
    lunslist.sort()
    return lunslist

def getTargetInformationLUNFromDB(): 
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    command = "/usr/bp/bin/cmc_iscsi  listluns " + storage_testdata.targetName + " " + storage_testdata.host + " " + storage_testdata.port
    _, out, err = conn.exec_command(command)
    temp = out.readlines()
    #print temp
    temp=(str(temp[0]).strip(' \r\t\n'))
    
    lunlist=temp.split("|")
    #targetlist.append(temp[1])
           
    return lunlist

def postStorageResponseParameters(actualResponse):
    responseData = jsonHandler.postStorageParamatersInJson(actualResponse)
    return responseData
        
def getStorageIdFromPostStorageResp(actualResponse):
    storageId = jsonHandler.postStorageResponseFromJson(actualResponse)
    return storageId

def getStorageSizeFromDB(storageId):
    query = "select mb_size from bp.storage where storage_id = " + str(storageId)
    dbResult = dbHandler.executeQuery(query)
    if(len(dbResult)):
        return dbResult[0][0]
    else:
        return "fail"

def getStorageTypeFromJson(actualResponse):
    response = jsonHandler.getStorageTypeFromJson(actualResponse)
    return response

def getStorageListParameters(storageType):
    parameterList = []
    for val in storageType:
        if(val == 'internal'):
            parameterList = parameterList + storage_testdata.storageResponseParameter + storage_testdata.internalPropertiesParameter
        if(val == 'added_internal'):
            parameterList = parameterList + storage_testdata.storageResponseParameter + storage_testdata.addedInternalPropertiesParameter
        if(val == 'nas'):
            parameterList = parameterList + storage_testdata.storageResponseParameter + storage_testdata.nasPropertiesParameter
    return parameterList
        
def parseCifsNasStorageResponse(actualResponse):
    return jsonHandler.postResponseParseId2(actualResponse) 

def parseNFSNasStorageResponse(actualResponse):
    return jsonHandler.postResponseParseId_u(actualResponse) 

def getStorageIdBasedOnShareNameFromDB(share_name):
    storageId = 0
    dbResult = dbHandler.executeQuery("select storage_id from bp.nas where share_name = '" + str(share_name) + "'")
    if(len(dbResult) > 0):
        storageId = dbResult[0][0]
    else:
        logHandler.logging.info("No storage with sharename " + str(share_name) + " found")
    return storageId

def getPortNumFromDB(storageId):
    query = "select port from bp.nas where storage_id = " + str(storageId)
    dbResult = dbHandler.executeQuery(query)
    if(len(dbResult)):
        return dbResult[0][0]
    else:
        return "fail"
    

def getNasShareNameById(storageId):   
    dbResult = dbHandler.executeQuery("SELECT share_name from bp.nas where storage_id='"+str(storageId)+"'")
    if(len(dbResult)):
        shareName = dbResult[0][0]
    else:
        logHandler.logging.info("Could not find the specified storage")
    return shareName
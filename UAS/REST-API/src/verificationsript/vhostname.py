'''
Created on Nov 14, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''

import json


from util import dbHandler, linuxMachineConnect, logHandler
from util import jsonHandler
from testdata import hostname_testdata

def getHostnameFromDB():
    dbResult = dbHandler.executeQuery("""SELECT system_id, name from bp.systems""") 
    dbdict = {}
    for row1 in dbResult:
        dbdict[row1[0]]=row1[1]
    return dbdict

def getHostnameFromResponse(actualResponse):
    responseData = jsonHandler.getAllParametersFromJson(actualResponse)  
    userDict = {}  
    for user in responseData:
        userDict[user['system_id']] = str(user['name'])        
    return userDict
        
def getHostFromDB():
    dbResult = dbHandler.executeQuery("""SELECT host from bp.systems""") 
    return dbResult[0][0]

def getHostsFromResponse(actualResponse):
    responseData = jsonHandler.getAllParametersFromJson(actualResponse)  
    userDict = {}  
    for user in responseData:
        return user['name']

def verifyHostsInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    _, out, err = conn.exec_command('cat /etc/hosts')
    temp = out.readlines()
    machineData = ''.join(temp).strip(' \r\t\n')
    logHandler.logging.info("Machine Data - " + ''.join(temp).strip(' \r\t\n'))
    searchString = hostname_testdata.posthostsName
    if(searchString in machineData):
        return True
    else:
        return False
     
def verifymodifiedHostsIpFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    _, out, err = conn.exec_command('cat /etc/hosts')
    temp = out.readlines()
    machineData = ''.join(temp).strip(' \r\t\n')
    logHandler.logging.info("Machine Data - " + ''.join(temp).strip(' \r\t\n'))
    searchString = hostname_testdata.puthostsIp
    if(searchString in machineData):
        return True
    else:
        return False    




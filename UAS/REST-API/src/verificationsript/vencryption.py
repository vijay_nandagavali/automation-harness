'''
Created on Dec 9, 2014

@author: Amey.k
'''
from util import linuxMachineConnect, logHandler
import json

def getEncryptionInformationFromMAchine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cryptoDaemon')
    logHandler.logging.info("Error fetching the encryption information: "+str(err.readlines()))

    _, out, err = conn.exec_command('/usr/bp/bin/cryptoDaemon -s 2> /tmp/cryptoOut')
    logHandler.logging.info("Error fetching the encryption information: "+str(err.readlines()))

    _, out, err = conn.exec_command('cat /tmp/cryptoOut | cut -c27-32')
    logHandler.logging.info("Error fetching the encryption information: "+str(err.readlines()))


    temp = out.readlines()
    logHandler.logging.info("Error fetching the encryption information: "+str(err))
    logHandler.logging.info("Machine Data - " + str(temp[0]).strip(' \r\t\n'))
    if ((str(temp[0]).strip(' \r\t\n') == 'notset') or (str(temp[0]).strip(' \r\t\n') == 'none;')):
        featurelist.append('off')
    else:
        featurelist.append('on')
    return featurelist

def getEncryptionInformationFromResponse(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['data'][0]['state']))
    return jsonParameter

def getEncryptionSupportInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    featurelist = []
    _, out, err = conn.exec_command(' /usr/bp/bin/cmc_crypt supported')
    temp = out.readlines()
    if ((str(temp[0]).strip(' \r\t\n') == 'supported')):
        featurelist.append('1')
    else:
        featurelist.append('0')
    return featurelist
        
def getEncryptionSupportInformationFromResponse(input_json):
    
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['data'][0]['EncryptionSupported']))
    return jsonParameter

def checkPersistence():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    _, out, err = conn.exec_command('ls -l /var/lib/misc/')  
    temp = out.readlines()
    strTemp = str(temp)
    print strTemp
    strFind = "cryptoDaemonPersistence"
    if strFind in strTemp:
        return "Pass"
    else:
        return "Fail" 
    
def getEncryptionFromCatalogResponse(input_json, instancename, bid):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    encrypt = False
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    
    for i in range(0,len(jsonData['catalog'])):
        if(jsonData['catalog'][i]['database_name']==instancename):
            print "database found: "+instancename
            for j in range(0,len(jsonData['catalog'][i]['backups'])):
                if(jsonData['catalog'][i]['backups'][j]['id']==bid):
                    encrypt = jsonData['catalog'][i]['backups'][j]['encrypted']
                    print "encrypt found "+str(encrypt)
                    break
    return encrypt
    
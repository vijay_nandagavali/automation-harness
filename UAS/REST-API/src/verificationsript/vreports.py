'''
Created on Nov 27, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''


import json


from util import dbHandler, logHandler
from util import dateTimeHandler
from config import Constants
from testdata import reports_testdata

def getUpdateHistoryListFromDB():
    updateHistoryList = []
    dbResult = dbHandler.executeQuery("""SELECT status from bp.update_history""")
    for row in dbResult:
        updateHistoryList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT version_from from bp.update_history""")
    for row in dbResult:
        updateHistoryList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT version_to from bp.update_history""")
    for row in dbResult:
        updateHistoryList.append(str(row[0]))

    updateHistoryList.sort()
    return updateHistoryList

def getUpdateHistoryCountFromDB():
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.update_history""")
    print "Number of update history is:"
    print dbResult[0][0]
    return dbResult[0][0]

def getUpdateHistoryListFromResponse(actualResponse,count):
    
    updateHistoryList = []
    responseData = json.loads(actualResponse)
    for i in range(0,count):
        
        status = str(responseData['data'][i]['status'])
        updateHistoryList.append(status)
        version_from = str(responseData['data'][i]['version_from'])
        updateHistoryList.append(version_from)
        version_to = str(responseData['data'][i]['version_to'])
        updateHistoryList.append(version_to)

    updateHistoryList.sort()
    return updateHistoryList

def getArchiveReportsFromDB(actualResponse):
    archiveReportsList = []
    responseData = json.loads(actualResponse)
    if(responseData['count']> 0):
        dbResult = dbHandler.executeQuery("""SELECT archive_set_id from bp.archive_sets""")
        for row in dbResult:
            archiveReportsList.append(str(row[0]))
    for i in range(0,responseData['count']):        
        if(len(responseData['data'][i]['archives']) > 0): 
            for j in range(0,len(responseData['data'][i]['archives'])):
                arcId = str(responseData['data'][i]['archives'][j]['archive_id'])               
                dbResult = dbHandler.executeQuery("""SELECT file_mib_size from bp.archives where archive_id="""+arcId)
                archiveReportsList.append(str(dbResult[0][0]))
                dbResult = dbHandler.executeQuery("""SELECT archive_id from bp.archives where archive_id="""+arcId)
                archiveReportsList.append(str(dbResult[0][0]))
                dbResult = dbHandler.executeQuery("""SELECT file_count from bp.archives where archive_id="""+arcId)
                archiveReportsList.append(str(dbResult[0][0]))
                dbResult = dbHandler.executeQuery("""SELECT compressed from bp.archives where archive_id="""+arcId)
                archiveReportsList.append(str(dbResult[0][0]))
                dbResult = dbHandler.executeQuery("""SELECT encrypted from bp.archives where archive_id="""+arcId)
                archiveReportsList.append(str(dbResult[0][0]))
                dbResult = dbHandler.executeQuery("""SELECT deduped from bp.archives where archive_id="""+arcId)
                archiveReportsList.append(str(dbResult[0][0]))
                dbResult = dbHandler.executeQuery("""SELECT orig_backup_no from bp.archives where archive_id="""+arcId)
                archiveReportsList.append(str(dbResult[0][0]))
#        dbResult = dbHandler.executeQuery("""SELECT backtype from bp.archives where archive_id="""+arcId)
#        for row in dbResult:
#            for backuptype in range(len(Constants.BACKUP_TYPES)):
#                if str(row[0])==str(backuptype+1):
#                    archiveReportsList.append(Constants.BACKUP_TYPES[backuptype]) 
#                    break; 
#            if str(row[0])=='1038':
#                    archiveReportsList.append("System Metadata")
    if(len(archiveReportsList) == 0):
        dbResult = dbHandler.executeQuery("select archive_set_id from bp.archive_sets")
        for row in dbResult:
            archiveReportsList.append(str(row[0])) 
    archiveReportsList.sort()
    return archiveReportsList

def getArchiveReportsFromResponse(actualResponse):
    archiveReportsList = []
    responseData = json.loads(actualResponse)
    for i in range(0,responseData['count']): 

        archive_set_id = str(responseData['data'][i]['archive_set_id'])
        archiveReportsList.append(archive_set_id)
        
        if(len(responseData['data'][i]['archives']) > 0):
            for j in range(0,len(responseData['data'][i]['archives'])): 
                archive_id = str(responseData['data'][i]['archives'][j]['archive_id'])
                archiveReportsList.append(archive_id)
                
                files = str(responseData['data'][i]['archives'][j]['files'])
                archiveReportsList.append(files)
                
                compressed = str(responseData['data'][i]['archives'][j]['compressed'])
                archiveReportsList.append(compressed)
                
                encrypted = str(responseData['data'][i]['archives'][j]['encrypted'])
                archiveReportsList.append(encrypted)
                
                size = str(responseData['data'][i]['archives'][j]['size'])
                archiveReportsList.append(size)
                
        #        backuptype = str(responseData['data'][i]['archives']['type'])
        #        archiveReportsList.append(backuptype)
                       
                deduped = str(responseData['data'][i]['archives'][j]['deduped'])
                archiveReportsList.append(deduped)
#                 '''Change the json response to match the database response'''
#                 if(deduped==''):
#                     archiveReportsList.append("False")
#                 else:
#                     archiveReportsList.append("True")
                
                orig_backup_id = str(responseData['data'][i]['archives'][j]['orig_backup_id'])
                archiveReportsList.append(orig_backup_id)
            
    archiveReportsList.sort()
    return archiveReportsList

def getBackupReportsFromResponse(actualResponse):
    
    backupReportsList = []
    responseData = json.loads(actualResponse)
    for i in range(0,responseData['count']): 
        size = str(responseData['data'][i]['size'])
        backupReportsList.append(size) 
    backupReportsList.sort()
    return backupReportsList

def getBackupReportsFromDB(actualResponse):
    
    backupReportsList = []
    responseData = json.loads(actualResponse)
    for i in range(0,responseData['count']): 
        backupId = str(responseData['data'][i]['id'])
        dbResult = dbHandler.executeQuery("""SELECT total_megs from bp.backups where backup_no="""+backupId)
        backupReportsList.append(str(dbResult[0][0]))
    backupReportsList.sort()
    return backupReportsList

def getRestoreReportsFromResponse(actualResponse):
    
    backupReportsList = []
    responseData = json.loads(actualResponse)
    for i in range(0,responseData['count']): 
        size = str(responseData['data'][i]['size'])
        backupReportsList.append(size) 
    backupReportsList.sort()
    return backupReportsList

def getRestoreReportsFromDB(actualResponse):
    
    backupReportsList = []
    responseData = json.loads(actualResponse)
    for i in range(0,responseData['count']): 
        backupId = str(responseData['data'][i]['id'])
        dbResult = dbHandler.executeQuery("""SELECT total_megs from bp.backups where backup_no="""+backupId)
        backupReportsList.append(str(dbResult[0][0]))
    backupReportsList.sort()
    return backupReportsList

def getStorageReportsFromResponse(actualResponse):
    storageReportsList = []
    responseData = json.loads(actualResponse)
    for i in range(0,responseData['count']): 
        storage_id = str(responseData['data'][i]['id'])
        storageReportsList.append(storage_id)
        
        print responseData['data']
        name = str(responseData['data'][i]['name'])
        storageReportsList.append(name)
        
        storage_type = str(responseData['data'][i]['online'])
        storageReportsList.append(storage_type)
        
        mb_size = str(responseData['data'][i]['mb_size'])
        storageReportsList.append(mb_size)
               
        is_default = str(responseData['data'][i]['is_default'])
        storageReportsList.append(is_default)
        
        for j in range(0,len(responseData['data'][i]['size_history'])):
        
            day = str(responseData['data'][i]['size_history'][j]['day'])
            storageReportsList.append(day)
            
            mb_size = str(responseData['data'][i]['size_history'][j]['mb_size'])
            storageReportsList.append(is_default)
        
            mb_used = str(responseData['data'][i]['size_history'][j]['mb_used'])
            storageReportsList.append(mb_used)
            
            mb_free = str(responseData['data'][i]['size_history'][j]['mb_free'])
            storageReportsList.append(mb_free)
        
    storageReportsList.sort()
    return storageReportsList
    
def getStorageReportsFromDB(actualResponse):   
    storageReportsList = []
    dbResult = dbHandler.executeQuery("""SELECT storage_id from bp.storage""")
    for row in dbResult:
        storageReportsList.append(str(row[0])) 
    dbResult = dbHandler.executeQuery("""SELECT storage_name from bp.storage""")
    for row in dbResult:
        storageReportsList.append(str(row[0])) 
    dbResult = dbHandler.executeQuery("""SELECT is_online from bp.storage""")
    for row in dbResult:
        storageReportsList.append(str(row[0]))  
    dbResult = dbHandler.executeQuery("""SELECT mb_size from bp.storage""")
    for row in dbResult:
        storageReportsList.append(str(row[0]))     
    dbResult = dbHandler.executeQuery("""SELECT is_default from bp.storage""")
    for row in dbResult:
        storageReportsList.append(str(row[0])) 
    responseData = json.loads(actualResponse)
    for i in range(0,responseData['count']): 
        for j in range(0,len(responseData['data'][i]['size_history'])):
            day = str(responseData['data'][i]['size_history'][j]['day'])
            dbResult = dbHandler.executeQuery("""select insert_date from bp.storage_history where insert_date ="""+"'"+day+"'")
            storageReportsList.append(str(dbResult[0][0]))
            dbResult = dbHandler.executeQuery("""select mb_size from bp.storage_history where insert_date ="""+"'"+day+"'")
            storageReportsList.append(str(dbResult[0][0])) 
            dbResult = dbHandler.executeQuery("""select mb_used from bp.storage_history where insert_date ="""+"'"+day+"'")
            storageReportsList.append(str(dbResult[0][0])) 
            dbResult = dbHandler.executeQuery("""select mb_free from bp.storage_history where insert_date ="""+"'"+day+"'")
            storageReportsList.append(str(dbResult[0][0])) 
    storageReportsList.sort()
    return storageReportsList 

def getDataReductionReportsFromResponse(actualResponse):   
    datareductionReportsList = []
    responseData = json.loads(actualResponse)
    for i in range(0,responseData['count']): 
        dedup = str(round(responseData['data'][i]['dedup'], 2))
        datareductionReportsList.append(dedup)
        
        data_reduction = str(round(responseData['data'][i]['data_reduction'], 2))
        datareductionReportsList.append(data_reduction)
         
    datareductionReportsList.sort()
    return datareductionReportsList

def getDataReductionReportsFromDB(actualResponse):
    
    datareductionReportsList = []
    dbResult = dbHandler.executeQuery("""SELECT  dedup from bp.data_reduction_ratios;""")
    for row in dbResult:
        datareductionReportsList.append(str(round(row[0],2)))
    dbResult = dbHandler.executeQuery("""SELECT  compr from bp.data_reduction_ratios;""")
    for row in dbResult:
        datareductionReportsList.append(str(round(row[0], 2)))
    datareductionReportsList.sort()
    return datareductionReportsList

def getBackupFailureReportsFromResponse(actualResponse):
    backupfailureReportsList = []
    responseData = json.loads(actualResponse)
    backupidList = getbackupfailureids()
    for i in range(0,responseData['count']): 
        print responseData['data'][i]['id']
        if(str(responseData['data'][i]['id']) in backupidList):
            idn = str(responseData['data'][i]['id'])
            backupfailureReportsList.append(idn)
            
            server_name = str(responseData['data'][i]['system_name'])
            backupfailureReportsList.append(server_name)
            
            size = str(responseData['data'][i]['size'])
            backupfailureReportsList.append(size)
         
    backupfailureReportsList.sort()
    return backupfailureReportsList

def getReplicationReportFromResponse(actualResponse):
    replicationReportList = []
    responseData = json.loads(actualResponse)
    
    for i in range(0,responseData['count']): 
        print responseData['data'][i]['id']
        replicationReportList.append(responseData['data'][i]['id'])
        replicationReportList.append(int(responseData['data'][i]['size']))
    
         
    replicationReportList.sort()
    print replicationReportList
    return replicationReportList

def getReplicationReportFromDB(actualResponse):
    replicationReportList = []
    responseData = json.loads(actualResponse)
    strlist = "("
    for i in range(0,responseData['count']): 
        if(i!=0):
            strlist = strlist+","
        print responseData['data'][i]['id']
        replicationReportList.append(responseData['data'][i]['id'])
        strlist= strlist+str(responseData['data'][i]['id'])
    strlist = strlist+")"
    dbResult = dbHandler.executeQuery(""" select total_megs from bp.backups where backup_no in"""+strlist)
    print "db result: "+str(dbResult)
    replicationReportList.append(int(dbResult[0][0]))
    
    replicationReportList.sort()
    return replicationReportList    
    

    

def getBackupLegallyHeldReportsFromResponse(actualResponse):
    backupLegallyHeldReportsList = []
    responseData = json.loads(actualResponse)
    
    for i in range(0,responseData['count']): 
            id = str(responseData['data'][i]['id'])
            backupLegallyHeldReportsList.append(id)
            
            app_name = str(responseData['data'][i]['app_name'])
            backupLegallyHeldReportsList.append(app_name)
            
            client_name = str(responseData['data'][i]['client_name'])
            backupLegallyHeldReportsList.append(client_name)
            
            system_name = str(responseData['data'][i]['system_name'])
            backupLegallyHeldReportsList.append(system_name)
            
            #type = str(responseData['data'][i]['type'])
            #backupLegallyHeldReportsList.append(type)
            
            size = str(responseData['data'][i]['size'])
            backupLegallyHeldReportsList.append(size)
            
            hold_days = str(responseData['data'][i]['hold_days'])
            backupLegallyHeldReportsList.append(hold_days)
                      
         
    backupLegallyHeldReportsList.sort()
    return backupLegallyHeldReportsList
    

def getBackupFailureReportsFromDB(actualResponse):
    backupfailureReportsList = []
    dbResult = dbHandler.executeQuery("""SELECT backup_no from bp.backups where status=32832""")
    for row in dbResult:
        backupfailureReportsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT server_name from bp.backups where status=32832""")
    for row in dbResult:
        backupfailureReportsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT total_megs from bp.backups where status=32832""")
    for row in dbResult:
        backupfailureReportsList.append(str(row[0]))
    backupfailureReportsList.sort()
    return backupfailureReportsList

def getBackupLegallyHeldReportsFromDB(actualResponse):
    backupLegallyHeldReportsList = []
    responseData = json.loads(actualResponse)    
    for i in range(0,responseData['count']): 
        id = str(responseData['data'][i]['id'])
        #backupLegallyHeldReportsList.append(id)
        query = "select a.backup_no, a.total_megs, a.legal_hold, c.name, d.node_name, e.name from bp.backups a, bp.application_instances b, bp.application_lookup c, bp.nodes d, bp.systems e where a.node_no = b.node_no and a.instance_id = b.instance_id and b.app_id = c.app_id and a.node_no = d.node_no and e.system_id = " + str(reports_testdata.sid) + " and a.backup_no = " + str(id)
        dbResult = dbHandler.executeQuery(query)
        #logHandler.logging.info("DB Result: " + str(dbResult))
        if(len(dbResult) > 0):
            for j in range(0, len(dbResult[0])):
                backupLegallyHeldReportsList.append(dbResult[0][j])
        else:
            logHandler.logging.info("No data found for Backup id " + str(id))
    
    for j in range(0,len(backupLegallyHeldReportsList)):
        backupLegallyHeldReportsList[j] = str(backupLegallyHeldReportsList[j])
          
    backupLegallyHeldReportsList.sort()
    return backupLegallyHeldReportsList

        
def getbackupfailureids():
    backupidList = []
    dbResult = dbHandler.executeQuery("""SELECT  backup_no from bp.backups where status = 32832;""")
    for row in dbResult:
        backupidList.append(str(row[0]))
    print backupidList   
    return backupidList
    
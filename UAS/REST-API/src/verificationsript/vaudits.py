'''
Created on Nov 14, 2014

@author: Amey.k
'''

import json


from util import dbHandler
from util import jsonHandler

def getAuditsFromResponse(actualResponse):  
    auditHistoryList = []
    responseData = json.loads(actualResponse)
    for i in range(0,len(responseData['data'])):  
            notification_id = str(responseData['data'][i]['notification_id'])
            auditHistoryList.append(notification_id)
            username = str(responseData['data'][i]['username'])
            auditHistoryList.append(username)
            system = str(responseData['data'][i]['system'])
            auditHistoryList.append(system)
    auditHistoryList.sort()
    return auditHistoryList   

def getAuditsFromDB(actualResponse):
    auditsList = []
    responseData = json.loads(actualResponse)
    length = len(responseData['data'])
    
    event_time_end=str(responseData['data'][0]['event_time'])
    event_time_start=str(responseData['data'][length-1]['event_time'])
    
    dbResult = dbHandler.executeQuery("""SELECT notification_id from bp.audit_history where event_time >='%s' and event_time<='%s'"""%(event_time_start,event_time_end))
    for row in dbResult:
        auditsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT username from bp.audit_history where event_time >='%s' and event_time<='%s'"""%(event_time_start,event_time_end))
    for row in dbResult:
        auditsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT system from bp.audit_history where event_time >='%s' and event_time<='%s'"""%(event_time_start,event_time_end))
    for row in dbResult:
        auditsList.append(str(row[0]))
    auditsList.sort()
    return auditsList

def getAuditsFromDB2(actualResponse):
    auditsList = []
    responseData = json.loads(actualResponse)
    length = len(responseData['data'])
    
    event_time_end=str(responseData['data'][0]['event_time'])
    event_time_start=str(responseData['data'][length-1]['event_time'])
    
    dbResult = dbHandler.executeQuery("""SELECT notification_id from bp.audit_history""")
    for row in dbResult:
        auditsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT username from bp.audit_history""")
    for row in dbResult:
        auditsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT system from bp.audit_history""")
    for row in dbResult:
        auditsList.append(str(row[0]))
    auditsList.sort()
    return auditsList
    
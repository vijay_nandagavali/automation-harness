'''
Created on Sep 15, 2014

@author: Abhijeet.m

Modified on Jan 20, 2017

@author: Divya.J
'''
from util import dbHandler
from util import jsonHandler
import json

def getAllAlertsFromDB(listAlertId):
    dbdict = {}
    for i in range (0, len(listAlertId)):
        dbResult = dbHandler.executeQuery("""SELECT alert_id, alert_text from bp.alerts where alert_id="""+str(listAlertId[i])) 
        for row1 in dbResult:
            dbdict[row1[0]]=row1[1]    
    return dbdict

def getAllAlertsFromResponse(actualResponse):
    responseData = jsonHandler.getAllParametersFromJson(actualResponse)  
    alertDict = {}  
    for alert in responseData:
        alertDict[alert['id']] = str(alert['message'])
    return alertDict

def getSpecificAlertFromDB(alertID):
    dbResult = dbHandler.executeQuery("""SELECT alert_id, alert_text from bp.alerts where alert_id=""" +alertID) 
    dbdict = {}
    for row1 in dbResult:
        dbdict[row1[0]]=row1[1]    
    return dbdict

def getSeverityAlertsFromDB(severity, listAlertId):
    dbdict = {}
    for i in range (0, len(listAlertId)):
        dbResult = dbHandler.executeQuery("""SELECT alert_id, alert_text from bp.alerts where severity=""" +str(severity)+"""and alert_id="""+str(listAlertId[i]))
        for row1 in dbResult:
            dbdict[row1[0]]=row1[1]
    return dbdict

def getOpenAlertsFromDB(listAlertId):
    dbdict = {}
    for i in range (0, len(listAlertId)):
        dbResult = dbHandler.executeQuery("""SELECT alert_id, alert_text,closed from bp.alerts where alert_id="""+str(listAlertId[i]))
        for row1 in dbResult:
            if(row1[2] == False):
                dbdict[row1[0]]=row1[1]
    return dbdict

def getClosedAlertsFromDB(listAlertId):
    dbdict = {}
    for i in range (0, len(listAlertId)):
        dbResult = dbHandler.executeQuery("""SELECT alert_id, alert_text,closed from bp.alerts where alert_id="""+str(listAlertId[i]))
        for row1 in dbResult:
            if(row1[2] == True):
                dbdict[row1[0]]=row1[1]
    return dbdict

def getOpenSeverityAlertsFromDB(severity, listAlertId):
    dbdict = {}
    for i in range (0, len(listAlertId)):
        dbResult = dbHandler.executeQuery("""SELECT alert_id, alert_text,closed from bp.alerts where severity=""" +severity+"""and alert_id="""+str(listAlertId[i]))
        for row1 in dbResult:
            if(row1[2] == False):
                dbdict[row1[0]]=row1[1]
    return dbdict

def getClosedSeverityAlertsFromDB(severity, listAlertId):
    dbdict = {}
    for i in range (0, len(listAlertId)):
        dbResult = dbHandler.executeQuery("""SELECT alert_id, alert_text,closed from bp.alerts where severity=""" +severity+"""and alert_id="""+str(listAlertId[i]))
        for row1 in dbResult:
            if(row1[2] == True):
                dbdict[row1[0]]=row1[1]
    return dbdict

def getListOfAlertId(actualResponse):
    alertId = []
    responseData = json.loads(actualResponse)
    for i in range(0,len(responseData['data'])):
        alertId.append(responseData['data'][i]['id'])           
    return alertId

def getUnresolvedAlertsFromDB():
    dbdict = []
    dbResult = dbHandler.executeQuery("SELECT alert_id from bp.alerts where closed = 'f' and alert_id <> 45 order by alert_id desc")
    if(len(dbResult) > 0):
        dbdict.append(dbResult)
    dbdict = [eachrow[0] for eachrow in dbdict]               
    return dbdict

def verifyDeleteAlertFromResponse(actualResponse):
    responseResult = jsonHandler.deleteResponseParse(actualResponse)
    return responseResult

def getStatusOfParticularAlertFromDB(alertId):
    status = ''
    dbResult = dbHandler.executeQuery("select closed from bp.alerts where alert_id = " + str(alertId))
    if(len(dbResult) > 0):
        status = dbResult[0][0]
    return status
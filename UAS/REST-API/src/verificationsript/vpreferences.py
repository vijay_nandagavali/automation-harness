'''
Created on Oct 6, 2014

@author: Amey.k
'''
from util import dbHandler
from util import jsonHandler
from testdata import preferences_testdata
import json


def getAllPreferencesFromDB():
    dbResult = dbHandler.executeQuery("""SELECT DISTINCT on (item_name) item_name, item_value FROM (SELECT * from bp.nvp  WHERE nvp_type = 'user_preference' AND  nvp_name = ' !!-factory-default!!' order by item_name, nvp_name DESC)foo;""") 
    dbdict = {}
    for row1 in dbResult:
        dbdict[row1[0]]=row1[1]
    #print dbdict    
    return dbdict


def getUserSpecificPreferencesFromDB():
    query = """SELECT DISTINCT on (item_name) item_name, item_value FROM (SELECT * from bp.nvp  WHERE nvp_type = 'user_preference' AND  nvp_name = '"""  +preferences_testdata.username+  """' OR nvp_name = ' !!-factory-default!!' order by item_name, nvp_name DESC)foo;"""
    dbResult = dbHandler.executeQuery(query) 
    dbdict = {}
    for row1 in dbResult:
        dbdict[row1[0]]=row1[1]
    #print dbdict    
    return dbdict

def getUserSpecificPreferenceParameterFromDB():
    query = """SELECT DISTINCT on (item_name) item_name, item_value FROM (SELECT * from bp.nvp  WHERE nvp_type = 'user_preference' AND item_name = '""" +preferences_testdata.preference+ """' order by item_name, nvp_name DESC)foo;"""
    dbResult = dbHandler.executeQuery(query) 
    dbdict = {}
    for row1 in dbResult:
        dbdict[row1[0]]=row1[1]
    return dbdict

    
def getAllPreferencesFromResponse(actualResponse):
    
    responseData = json.loads(actualResponse)
    #print responseData['data'][0]['page_jobs_refresh']
    prefdict = {}
    prefdict['dash_active_job_refresh'] = str (responseData["data"][0]["dash_active_job_refresh"])
    prefdict['dash_alert_refresh'] = str (responseData["data"][0]["dash_alert_refresh"])
    prefdict['dash_backup_refresh'] = str (responseData["data"][0]["dash_backup_refresh"])
    prefdict['dash_replication_refresh'] = str (responseData["data"][0]["dash_replication_refresh"])
    prefdict['dash_restore_refresh'] = str (responseData["data"][0]["dash_restore_refresh"])
    prefdict['dash_storage_refresh'] = str (responseData["data"][0]["dash_storage_refresh"])
    prefdict['language'] = str (responseData["data"][0]["language"])
    prefdict['page_jobs_refresh'] = str (responseData["data"][0]["page_jobs_refresh"])
    prefdict['page_protect_refresh'] = str (responseData["data"][0]["page_protect_refresh"])
    prefdict['page_restore_refresh'] = str (responseData["data"][0]["page_restore_refresh"])
    prefdict['show_setup_wizard'] = str (responseData["data"][0]["show_setup_wizard"])
    prefdict['show_help_overlay'] = str (responseData["data"][0]["show_help_overlay"])
    return prefdict

def getSpecificUserParameterFromResponse(actualResponse):
    
    responseData = json.loads(actualResponse)
    prefdict = {}
    prefdict[preferences_testdata.preference] = str (responseData["data"][0]["dash_alert_refresh"])
    return prefdict
          


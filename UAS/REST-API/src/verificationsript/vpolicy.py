'''
Created on Jun 3, 2015

@author: savitha.p

Modified on Jan 20, 2017

@author: Divya.J
'''
import json
from util import jsonHandler, dbHandler,logHandler

def getPutRequestResponse(actualResponse):
    return jsonHandler.putResponseParse(actualResponse)

def verifyPostPolicyCountFromDB(policyName):
    query = " select count(*) from bp.gfs_policy where name = '" + str(policyName) + "'"
    dbResult = dbHandler.executeQuery(query)
    policyCount = 0
    if(len(dbResult) > 0):
        policyCount = dbResult[0][0]
    return policyCount

def getPolicyIDFromDB(policyName):
    pid = 0
    if (policyName != ""):
        query = "select policy_id from bp.gfs_policy where name = '" + str(policyName) + "'"
    else:
        query = "select policy_id from bp.gfs_policy where days != -1"
    dbResult = dbHandler.executeQuery(query)
    if(len(dbResult) > 0):
    pid = dbResult[0][0]
    return pid    

def getPolicyDetailsFromDB(policyName):
    pllist = []
    query = "SELECT days,weekly,monthly,yearly from bp.gfs_policy where name = '" + str(policyName) +"'"
    dbResult = dbHandler.executeQuery(query)
    pllist.append(dbResult[0][0])
    pllist.append(dbResult[0][1])
    pllist.append(dbResult[0][2])
    pllist.append(dbResult[0][3])
    pllist.sort()
    return pllist

def getAllPolicyDetailsFromDB(policyName):
    '''Returns days, weeks, months, years and is_default for policyName'''
    pllist = []
    query = "SELECT days,weekly,monthly,yearly,is_default from bp.gfs_policy where name = '" + str(policyName) +"'"
    dbResult = dbHandler.executeQuery(query)
    pllist.append(dbResult[0][0])
    pllist.append(dbResult[0][1])
    pllist.append(dbResult[0][2])
    pllist.append(dbResult[0][3])
    pllist.append(dbResult[0][4])
    return pllist

def getRetentionStrategyFromResponse(actualResponse):
    jsonData = json.loads(actualResponse)
    return str(jsonData['data']['strategy'])
    
def verifyRetentionPolicyFromDB():
    retention_value=""
    strQuery = "select item_value from bp.nvp where item_name='retention'"
    dbResult = dbHandler.executeQuery(strQuery)
    retention_value = dbResult[0][0] 
    return retention_value

def getRetentionPoliciesFromDB():
    dbDict = {}
    strQuery = "select policy_id, name from bp.gfs_policy"
    dbResult = dbHandler.executeQuery(strQuery)
    for row1 in dbResult:
            dbDict[row1[0]]=row1[1]
    return dbDict

def getRetentionPoliciesFromResponse(actualResponse):
    responseData = jsonHandler.getAllParametersFromJson(actualResponse)  
    policyDict = {}
    if(responseData != None):
        for policy in responseData:
            policyDict[policy['id']] = str(policy['name'])
    return policyDict   

def verifyDeletePolicyResponse (actualResponse):
    response = jsonHandler.deleteResponseParse(actualResponse)
    return response

def verifyPostPolicyResponse(actualResponse):
    response = jsonHandler.postResponseParse(actualResponse)
    return response

def verifyPolicyByUnicodeNameFromDB(policyName):
    query = " select count(*) from bp.gfs_policy where name = '" + str(policyName) + "'"
    dbResult = dbHandler.executeQuery(query)
    policyCount = 0
    if(len(dbResult) > 0):
        policyCount = dbResult[0][0]
    return policyCount

def verifyPostPolicymessage(actualResponse):
    '''returns message from POST, if POST fails '''    
    response = jsonHandler.PostResponseMessage(actualResponse)
    return response

def getPolicyInformationFromResponse(input_json):
    #input_json = input_json.replace('\n','\\n').replace('\r','\\r')
    jsonData = json.loads(input_json) #The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['data'][0]['id']))
    jsonParameter.append(str(jsonData['data'][0]['name']))
    jsonParameter.append(str(jsonData['data'][0]['days']))
    jsonParameter.append(str(jsonData['data'][0]['weeks']))
    jsonParameter.append(str(jsonData['data'][0]['months']))
    jsonParameter.append(str(jsonData['data'][0]['years']))
    return jsonParameter

def getPolicyInformationFromDB(policyId):
    policyList = []
    strQuery = "select policy_id, name, days, weekly, monthly, yearly from bp.gfs_policy where policy_id = " + str(policyId)
    print strQuery
    dbResult = dbHandler.executeQuery(strQuery)
    policyList.append(str(dbResult[0][0]))
    policyList.append(str(dbResult[0][1]))
    policyList.append(str(dbResult[0][2]))
    policyList.append(str(dbResult[0][3]))
    policyList.append(str(dbResult[0][4]))
    policyList.append(str(dbResult[0][5]))
    return policyList
    
    
def getInstancePolicyAssociationFromDB(filterName = "", filterId =""):
    '''returns database values for GET /api/retention - filterName and FilterId can be used to provide parameters'''
    policyInstanceAsso = []
    if(filterName != "" and  filterId != ""):
        if(filterName == "client_id"):
            query = "select ai.node_no,ai.app_id,al.name,ai.instance_id, case when ai.key1 isnull then al.name else COALESCE(ai.key1,'')||'|'||COALESCE(ai.key2,'') end,COALESCE(gp.policy_id,0) as policy_id,COALESCE(gp.name,'') as policy_name, COALESCE(gp.days,0) as days,COALESCE(gp.weekly,0) as weeks,COALESCE(gp.monthly,0) as months, COALESCE(gp.yearly,0)as years from bp.application_lookup al inner join bp.application_instances ai on ai.app_id = al.app_id left outer join bp.gfs_policy_association ga on ai.instance_id = ga.instance_id left outer  join bp.gfs_policy gp  on gp.policy_id = ga.policy_id where ai.current = 't' and al.name != 'Archive' and " + str(filterName)+ "=" + str(filterId)
        elif(filterName == "uuid"):  
            query = "select ai.node_no,ai.app_id,al.name,ai.instance_id, case when ai.key1 isnull then al.name else COALESCE(ai.key1,'')||'|'||COALESCE(ai.key2,'') end,COALESCE(gp.policy_id,0) as policy_id,COALESCE(gp.name,'') as policy_name, COALESCE(gp.days,0) as days,COALESCE(gp.weekly,0) as weeks,COALESCE(gp.monthly,0) as months, COALESCE(gp.yearly,0)as years from bp.application_lookup al inner join bp.application_instances ai on ai.app_id = al.app_id left outer join bp.gfs_policy_association ga on ai.instance_id = ga.instance_id left outer  join bp.gfs_policy gp  on gp.policy_id = ga.policy_id where ai.current = 't' and al.name != 'Archive' and ai.instance_id in (select instance_id from bp.vmware_vms where esx_uuid = '" + str(filterId)+"')"
        else:
            query = "select ai.node_no,ai.app_id,al.name,ai.instance_id, case when ai.key1 isnull then al.name else COALESCE(ai.key1,'')||'|'||COALESCE(ai.key2,'') end,COALESCE(gp.policy_id,0) as policy_id,COALESCE(gp.name,'') as policy_name, COALESCE(gp.days,0) as days,COALESCE(gp.weekly,0) as weeks,COALESCE(gp.monthly,0) as months, COALESCE(gp.yearly,0)as years from bp.application_lookup al inner join bp.application_instances ai on ai.app_id = al.app_id left outer join bp.gfs_policy_association ga on ai.instance_id = ga.instance_id left outer  join bp.gfs_policy gp  on gp.policy_id = ga.policy_id where ai.current = 't' and al.name != 'Archive' and ai." + str(filterName)+ "=" + str(filterId)                                    
    else:
        query = "select ai.node_no,ai.app_id,al.name,ai.instance_id, case when ai.key1 isnull then al.name else COALESCE(ai.key1,'')||'|'||COALESCE(ai.key2,'') end,COALESCE(gp.policy_id,0) as policy_id,COALESCE(gp.name,'') as policy_name, COALESCE(gp.days,0) as days,COALESCE(gp.weekly,0) as weeks,COALESCE(gp.monthly,0) as months, COALESCE(gp.yearly,0)as years from bp.application_lookup al inner join bp.application_instances ai on ai.app_id = al.app_id left outer join bp.gfs_policy_association ga on ai.instance_id = ga.instance_id left outer  join bp.gfs_policy gp  on gp.policy_id = ga.policy_id where ai.current = 't' and al.name != 'Archive'"
    dbResult = dbHandler.executeQuery(query)
    for row in dbResult:
        policyInstanceAsso.append(str(row[0]))
        policyInstanceAsso.append(str(row[1]))
        policyInstanceAsso.append(str(row[2]))
        policyInstanceAsso.append(str(row[3]))
        policyInstanceAsso.append(str(row[4]))
        policyInstanceAsso.append(str(row[5]))
        policyInstanceAsso.append(str(row[6]))
        policyInstanceAsso.append(str(row[7]))
        policyInstanceAsso.append(str(row[8]))
        policyInstanceAsso.append(str(row[9])) 
        policyInstanceAsso.append(str(row[10])) 
    return policyInstanceAsso


def getInstancePolicyAssociationFromResponse(actualResponse):
    '''parses json response and returns values for GEt /api/retention '''
    jsonData = json.loads(actualResponse)
    jsonVal = []        
    for key,value in jsonData.items():
        if(key == 'data'):
            
            for i in range(0,len(jsonData['data'])):
                jsonVal.append(str(jsonData['data'][i]['client_id']))             
                jsonVal.append(str(jsonData['data'][i]['app_id']))
                jsonVal.append(str(jsonData['data'][i]['app_name']))
                jsonVal.append(str(jsonData['data'][i]['instance_id']))
                jsonVal.append(str(jsonData['data'][i]['instance_name']))
                jsonVal.append(str(jsonData['data'][i]['policy_id']))
                jsonVal.append(str(jsonData['data'][i]['policy_name']))
                jsonVal.append(str(jsonData['data'][i]['days']))
                jsonVal.append(str(jsonData['data'][i]['weeks']))
                jsonVal.append(str(jsonData['data'][i]['months']))
                jsonVal.append(str(jsonData['data'][i]['years']))
 
    return jsonVal

def GetClientIdFromDB(): 
    strQuery = "select distinct node_no from bp.application_instances"
    dbResult = dbHandler.executeQuery(strQuery)
    dbClientId = dbResult[0][0]
    return dbClientId

def verifyDeleteRetentionResponse (actualResponse):
    response = jsonHandler.deleteResponseParse(actualResponse)
    return response

def getDeleteMessage(actualResponse):
    deleteResponseMessage = jsonHandler.deleteResponseMessage(actualResponse)
    return deleteResponseMessage

def verifyPostRetentionStrategyResponse(actualResponse):
    response = jsonHandler.postResponseParse(actualResponse)
    return response

def getDefaultPolicyFromDB():
    policyName=""
    strQuery = "select name from bp.gfs_policy where is_default = true"
    dbResult = dbHandler.executeQuery(strQuery)
    policyName = dbResult[0][0] 
    return policyName


def GetAppIdFromDB(): 
    strQuery = "select distinct app_id from bp.application_instances where key1 is not null"
    dbResult = dbHandler.executeQuery(strQuery)
    if(len(dbResult) > 0):
        dbAppId= dbResult[0][0]
    return dbAppId


def GetEsxUuidIdFromDB(): 
    strQuery = "select esx_uuid from bp.vmware_esx_servers"
    dbResult = dbHandler.executeQuery(strQuery)
    if(len(dbResult) > 0):
        dbAppId= dbResult[0][0]
    return dbAppId

def getAllPolicyInformationFromDB():
    '''Gets all the policies from the database'''
    policyList = []
    strQuery = "select policy_id, name, days, weekly, monthly, yearly, is_default from bp.gfs_policy "
    print strQuery
    dbResult = dbHandler.executeQuery(strQuery)
    for row in dbResult:
        policyList.append(str(row[0]))
        policyList.append(str(row[1]))
        policyList.append(str(row[2]))
        policyList.append(str(row[3]))
        policyList.append(str(row[4]))
        policyList.append(str(row[5]))
        policyList.append(str(row[6]))
    return policyList


def getPolicydetailsFromResponse(actualResponse):
    '''parses json response and returns values for GEt /api/retention_policy '''
    jsonData = json.loads(actualResponse)
    jsonVal = []        
    for key,value in jsonData.items():
        if(key == 'data'):       
            for i in range(0,len(jsonData['data'])):         
                jsonVal.append(str(jsonData['data'][i]['id']))
                jsonVal.append(str(jsonData['data'][i]['name']))
                if(str(jsonData['data'][i]['days'])== 'Forever'):
                    jsonVal.append(str(-1))
                else:
                    jsonVal.append(str(jsonData['data'][i]['days']))
                if(str(jsonData['data'][i]['weeks'])== 'Forever'):
                    jsonVal.append(str(-1))
                else:
                    jsonVal.append(str(jsonData['data'][i]['weeks']))
                if(str(jsonData['data'][i]['months'])== 'Forever'):
                    jsonVal.append(str(-1))
                else:
                    jsonVal.append(str(jsonData['data'][i]['months']))
                if(str(jsonData['data'][i]['years'])== 'Forever'):
                    jsonVal.append(str(-1))
                else:
                    jsonVal.append(str(jsonData['data'][i]['years']))
                jsonVal.append(str(jsonData['data'][i]['is_default']))    
    return jsonVal
        
        
def getInstanceIdFromDB():
    strQuery = "select instance_id from bp.application_instances where current = true and key1 != ''"
    dbResult = dbHandler.executeQuery(strQuery)
    if(len(dbResult) > 0):
        dbInstanceId= dbResult[0][0]
    return dbInstanceId

def verifyPostPolicyAssocationCountFromDB(instance_id, policy_id):
    query = " select count(*) from bp.gfs_policy_association where instance_id = '" + str(instance_id) + "' and policy_id = " + str(policy_id)
    dbResult = dbHandler.executeQuery(query)
    policyCount = 0
    if(len(dbResult) > 0):
        policyCount = dbResult[0][0]
    return policyCount

'''
Created on Nov 26, 2014

@author: Amey.k

Modified on Jan 20, 2017

@author: Divya.J
'''

import json


from util import dbHandler, linuxMachineConnect


def getSystemsListFromDB():
    systemsList = []
    dbResult = dbHandler.executeQuery("""SELECT system_id from bp.systems""")
    for row in dbResult:
        systemsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT location_id from bp.systems""")
    for row in dbResult:
        systemsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT name from bp.systems""")
    for row in dbResult:
        systemsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT version from bp.systems""")
    for row in dbResult:
        systemsList.append(str(row[0]))
#     dbResult = dbHandler.executeQuery("""SELECT role from bp.systems""")
#     for row in dbResult:
#         systemsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT is_active from bp.systems""")
    for row in dbResult:        
            systemsList.append(str(row[0]))
    systemsList.sort()
    
    
    return systemsList

def getSystemsCountFromDB():
    dbResult = dbHandler.executeQuery("""SELECT count(*) from bp.systems""")
    print "Number of systems are:"
    print dbResult[0][0]
    return dbResult[0][0]

def getSystemsListFromResponse(actualResponse,count):
    
    systemsList = []
    responseData = json.loads(actualResponse)
    for i in range(0,count):
        
        id1 = str(responseData['appliance'][i]['id'])
        systemsList.append(id1)
        name = str(responseData['appliance'][i]['name'])
        systemsList.append(name)
#         role = str(responseData['appliance'][i]['role'])
#         systemsList.append(role)
        version = str(responseData['appliance'][i]['version'])
        systemsList.append(version)
        sysstatus = str(responseData['appliance'][i]['online'])
        systemsList.append(sysstatus)
        custid = str(responseData['appliance'][i]['customer_id'])
        systemsList.append(custid)
    systemsList.sort()
    
    return systemsList

def getspecificSystemInformationFromDB(systemId):
    systemsList = []
    dbResult = dbHandler.executeQuery("""SELECT system_id from bp.systems where system_id=""" +systemId)
    systemsList.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("""SELECT location_id from bp.systems where system_id=""" +systemId)
    systemsList.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("""SELECT name from bp.systems where system_id=""" +systemId)
    systemsList.append(str(dbResult[0][0]))
    dbResult = dbHandler.executeQuery("""SELECT version from bp.systems where system_id=""" +systemId)
    systemsList.append(str(dbResult[0][0]))
#     dbResult = dbHandler.executeQuery("""SELECT role from bp.systems where system_id=""" +systemId)
#     for row in dbResult:
#         systemsList.append(str(row[0]))
    dbResult = dbHandler.executeQuery("""SELECT is_active from bp.systems where system_id=""" +systemId)
    systemsList.append(str(dbResult[0][0]))
    systemsList.sort()
    return systemsList

def getSystemNameFromDB(systemId):
    dbResult = dbHandler.executeQuery("SELECT name from bp.systems where system_id=" +str(systemId))
    return dbResult[0][0]
    
    
def getSystemDetailsFromResponse(actualResponse):
    systemsList = []
    responseData = json.loads(actualResponse)
    name = str(responseData['appliance']['name'])
    systemsList.append(name)
#     assetTag = str(responseData['appliance']['asset_tag'])
#     systemsList.append(assetTag)
    version = str(responseData['appliance']['version'])
    systemsList.append(version)
    osVersion = str(responseData['appliance']['os_version'])
    systemsList.append(osVersion)
    processors = str(responseData['appliance']['processors'])
    systemsList.append(processors)
    systemsList.sort()
    return systemsList

def getSystemDetailsFromDBAndMachine(systemId):
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    systemsList = []
    dbResult = dbHandler.executeQuery("""SELECT name from bp.systems where system_id=""" +systemId)
    systemsList.append(str(dbResult[0][0]))
#     _, out, err = conn.exec_command('/usr/bp/bin/cmc_license license get | grep "Asset Tag" | cut -c11-46')
#     temp = out.readlines()
#     systemsList.append(str(temp[0]).strip(' \r\t\n'))
    dbResult = dbHandler.executeQuery("""SELECT version from bp.systems where system_id=""" +systemId)
    systemsList.append(str(dbResult[0][0]))
    _, out, err = conn.exec_command('cat /etc/issue | grep Recovery')
    temp = out.readlines()
    systemsList.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('cat /proc/cpuinfo | grep processor | wc -l')
    temp = out.readlines()
    systemsList.append(str(temp[0]).strip(' \r\t\n'))
    systemsList.sort()
    return systemsList

def checkMakeTarget():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_openvpn server confirm')  
    temp = out.readlines()
    strTemp = str(temp)
    print strTemp
    strFind = "true"
    if strFind in strTemp:
        return "Pass"
    else:
        return "Fail" 
    
def getNonManagedRepSrc_DB():
    query = "select system_id from bp.systems where role = 'Non-managed Replication Source'"
    dbResult = dbHandler.executeQuery(query, "target")
    sysID = dbResult[0][0]
    return sysID

def getSystemRole(sysID):
    query = "select role from bp.systems where system_id ="+ str(sysID)
    dbResult = dbHandler.executeQuery(query, "target")
    sysRole = dbResult[0][0]
    return sysRole

def getSystemIDFromDB(sysName):
    sysid = 0
    query = "SELECT system_id from bp.systems where name='" +str(sysName) + "'"
    dbResult = dbHandler.executeQuery(query)    
    if(len(dbResult)): 
        sysid = dbResult[0][0]
    if(sysid == 0):
        return 0
    else:
        return sysid
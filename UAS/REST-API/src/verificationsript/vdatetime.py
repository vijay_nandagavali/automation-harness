'''
Created on Nov 13, 2014

@author: Amey.k
'''
from util import linuxMachineConnect
import json

def getDateTimeInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    #print conn
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime dtget | cut -c1-4')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime dtget | cut -c6-7')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime dtget | cut -c9-10')
    temp = out.readlines()
    if (str(temp[0]).startswith('0')):
        temp = str(temp[0][1])
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime tzget')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime ntpget | grep s | cut -c3-23')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime ntpget | grep s | cut -c24-45')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime ntpget | grep s | cut -c46-67')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime ntpget | grep s | cut -c68-89')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    
    return featurelist

def getDateTimeInformationFromResponse(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['year']))
    jsonParameter.append(str(jsonData['month']).zfill(2))
    jsonParameter.append(str(jsonData['day']))
    jsonParameter.append(str(jsonData['tz']))
    jsonParameter.append(str(jsonData['ntp']['servers'][0]))
    jsonParameter.append(str(jsonData['ntp']['servers'][1]))
    jsonParameter.append(str(jsonData['ntp']['servers'][2]))
    jsonParameter.append(str(jsonData['ntp']['servers'][3]))
    
    return jsonParameter

def getTimezoneInformationFromMachine(input_json):
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    tzlist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime tzlist')
    temp = out.readlines()
    for i in range(0, len(temp)):
        tzlist.append(str(temp[i]).replace("\n",""))
    
    return tzlist

def getTimezoneInformationFromResponse(input_json):
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    for value in jsonData['timezone']:
        jsonParameter.append(str(value))
    return jsonParameter

def getNtpserverlistFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_datetime ntpget | grep s | cut -c68-89')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    
    return featurelist
    
    
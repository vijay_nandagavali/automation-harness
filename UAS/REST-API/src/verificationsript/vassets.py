'''
Created on July 22, 2014

@author: Nikhil S.
'''
from util import dbHandler
from util import jsonHandler
import json

def getProtectedAssetsDetailsFromResponse(actualResponse):
    protectedAssetlist = []
    responseData = json.loads(actualResponse)
    id = str(responseData['data'][0]['id'])
    protectedAssetlist.append(id)
    name = str(responseData['data'][0]['name'])
    protectedAssetlist.append(name)
    priority = str(responseData['data'][0]['priority'])
    protectedAssetlist.append(priority)
    os_type_id = str(responseData['data'][0]['os_type_id'])
    protectedAssetlist.append(os_type_id)
    version = str(responseData['data'][0]['version'])
    protectedAssetlist.append(version)
    protectedAssetlist.sort()
    return protectedAssetlist

def getProtectedAssetsDetailsFromDB(actualResponse):
    protectedAssetlist = []
    responseData = json.loads(actualResponse)
    id = str(responseData['data'][0]['id'])
    print "protectedid :", id
    dbresult =dbHandler.executeQuery("""select node_name,priority,os,version,node_no from bp.nodes where node_no =""" + str(id))
    for row in dbresult:
        protectedAssetlist.append(str(row[0]))
        protectedAssetlist.append(str(row[1]))
        protectedAssetlist.append(str(row[2]))
        protectedAssetlist.append(str(row[3]))
        protectedAssetlist.append(str(row[4]))
    protectedAssetlist.sort()
    return protectedAssetlist
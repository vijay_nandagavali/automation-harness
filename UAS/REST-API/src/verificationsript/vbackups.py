'''
Created on Sep 15, 2014

@author: Abhijeet.m

Modified on Jan 20, 2017

@author: Divya.J
'''
from util import dbHandler
from util import jsonHandler, logHandler
import json
import time
from resourcelibrary import backups
from testdata import backups_testdata

def getRestorableBackupsFromDB(actualResponse):    
    backupsList = []
    backupsIdList = getListOfBackupIdsFromResponse(actualResponse)
    logHandler.logging.info("Backups returned from Response: " + str(backupsIdList))
    for i in range(0,len(backupsIdList)):
        backupsList.append(int(backupsIdList[i])) 
        dbResult = dbHandler.executeQuery("""SELECT legal_hold from bp.backups where backup_no="""+str(backupsIdList[i]))
        if(len(dbResult)):
            if(str(dbResult[0][0])=='0'):
                backupsList.append(False) 
            else:
                backupsList.append(True)
        else:
            backupsList.append(False) 
    backupsList.sort()
    return backupsList

def getRestorableBackupsFromResponse(actualResponse):
    backupsList = []
    jsonData = json.loads(actualResponse)
    for i in range(0,len(jsonData['catalog'])):
        for j in range(0,len(jsonData['catalog'][i]['instances'])):
            for k in range(0,len(jsonData['catalog'][i]['instances'][j]['backups'])):
                backupsList.append(jsonData['catalog'][i]['instances'][j]['backups'][k]['id'])
                backupsList.append(jsonData['catalog'][i]['instances'][j]['backups'][k]['legal_hold'])
    backupsList.sort()
    return backupsList

def getListOfBackupIdsFromResponse(actualResponse):
    backupsIdList = []
    jsonData = json.loads(actualResponse)
    for i in range(0,len(jsonData['catalog'])):
        for j in range(0,len(jsonData['catalog'][i]['instances'])):  
            for k in range(0,len(jsonData['catalog'][i]['instances'][j]['backups'])): 
                backupsIdList.append(jsonData['catalog'][i]['instances'][j]['backups'][k]['id'])
    return backupsIdList

# If a record with specific job number is found return Pass otherwise Failed
def getJobByIdFromDB(appliance_ip,jobid):
    strQuery = "select count(*) from bp.jobs where job_no=" + str(jobid)
    dbResult = dbHandler.executeQuery(appliance_ip,strQuery)
    #print dbResult[0][0]
    if(dbResult[0][0] != 0):
        return "Pass"
    else:
        return "Failed"
    
def putBackupOnLegalHold(backupid):    
    strQuery = "select legal_hold from bp.backups where backup_no=" + str(backupid)
    dbResult = dbHandler.executeQuery(strQuery)
    #print dbResult[0][0]
    if(dbResult[0][0] != 0):
        return "Pass"
    else:
        return "Failed"
    
def confirmBackupDeletion(backupid):
    try:
        strQuery = "select device from bp.sm_backups where backup_no=" + "'"+str(backupid)+"'" 
        dbResult = dbHandler.executeQuery(strQuery)
        return dbResult[0][0]
    except(IndexError):
        return 'Failed'
         

def getClientIDsFromBackupCatalogResponse(auth_token):
    apiResponse = json.loads(backups.getBackupCatalogResponse(auth_token))
    apiResInJson = json.dumps(apiResponse["catalog"])
    jp = json.loads(apiResInJson)
    instanceIDs =[]
    for i in range(0, len(jp)):
        jpe = jp[i]['instances']
        instanceIDs.append(jpe[0]['client_id'])
    return instanceIDs

def getBackupIDsFromBackupCatalogResponse(auth_token):
    apiResponse = json.loads(backups.getBackupCatalogResponse(auth_token))
    apiResInJson = json.dumps(apiResponse["catalog"])
    jp = json.loads(apiResInJson)
    backupIDs = []
    for i in range(0, len(jp)):
        jpe = jp[i]['instances']
        for b in range(0, len(jpe[i]['backups'])):
            backupIDs.append(str(jpe[0]['backups'][b]['id']))
    return backupIDs
    
def getBackupIDsFromDBForClients(clientIDs):
    backupIDs = []
    for c in range(0,len(clientIDs)):
        query = "select backup_no from bp.backups where node_no = " + clientIDs[c]
        dbResult = dbHandler.executeQuery(query)
        for row in dbResult:
            backupIDs.append(str(row[0]))
    return backupIDs

# def getInstanceIdFromVm(vmName):
#     instanceId= 0 
#     query = "select instance_id from bp.application_instances where key1 = " + "'"+vmName+"'"
#     print query
#     try:
#         dbResult = dbHandler.executeQuery(query)
#         for row in dbResult:
#             instanceId=str(row[0])
#     except:
#         print "Instance Id not found"
#     return int(instanceId)


def getInstanceIdFromVm(vmName):
#     instanceCount = 0
#     instanceId= 0 
    query = "select instance_id from bp.application_instances where key2 = " + "'"+vmName+"'"
#     try:
#         while(instanceCount < backups_testdata.waittimeInstance):
    dbResult = dbHandler.executeQuery(query)
    return int(dbResult[0][0])
#             if(instanceId != 0):
#                 break
#             instanceCount += 1
#             print "Waiting for Instance to be generated.... CheckCount: " + str(instanceCount) 
#             time.sleep(1)
#     except:
#         print "Instance Id not found"
#     return int(instanceId)

def getInstanceIdFromHyperv(appliance_ip,vmName):
#     instanceCount = 0
#     instanceId= 0 
    query = "select instance_id from bp.application_instances where key1 = " + "'"+vmName+"'"
#     try:
#         while(instanceCount < backups_testdata.waittimeInstance):
    dbResult = dbHandler.executeQuery(appliance_ip, query)
    return int(dbResult[0][0])

def getInstanceIdFromVmAtSource(vmName):
#     instanceCount = 0
#     instanceId= 0 
    query = "select instance_id from bp.application_instances where key1 = " + "'"+vmName+"'"
#     try:
#         while(instanceCount < backups_testdata.waittimeInstance):
    dbResult = dbHandler.executeQuery(query,"source")
    return int(dbResult[0][0])
#             if(instanceId != 0):
#                 break
#             instanceCount += 1
#             print "Waiting for Instance to be generated.... CheckCount: " + str(instanceCount) 
#             time.sleep(1)
#     except:
#         print "Instance Id not found"
#     return int(instanceId)
      
def getBackupId(appliance_ip,strJobId):
    strQuery = "select backup_no from bp.jobs where job_no=" + str(strJobId)
    dbResult = dbHandler.executeQuery(appliance_ip,strQuery)
    return dbResult[0][0]

def getBackupIdAtSource(strJobId):
    strQuery = "select backup_no from bp.jobs where job_no=" + str(strJobId)
    dbResult = dbHandler.executeQuery(strQuery,"source")
    return dbResult[0][0]
    
def checkSpecificBackup(backupID):   
    query = "select count(*) from bp.backups where backup_no = " + str(backupID)
    dbResult = dbHandler.executeQuery(query) 
    return dbResult[0][0]

def getFilteredBackupsFromResponse(actualResponse):
    backupsIdList = []
    jsonData = json.loads(actualResponse)
    for i in range(0,len(jsonData['data'])):
        backupsIdList.append(jsonData['data'][i]['id'])
    backupsIdList.sort()
    return backupsIdList
    
def getFilteredBackupsFromDB(node_no):
    backupsIdList = []
    strQuery = "select backup_no from bp.sm_backups where device <> '(deleted)' and node_no = " + "'"+str(node_no)+"' order by backup_no" 
    dbResult = dbHandler.executeQuery(strQuery)
    for row in dbResult:
        backupsIdList.append(int(row[0]))
    return backupsIdList

def getFullHyperVBackupsFromDB(node_no, instance_id):
    fullBackups = []
    query = "select backup_no, job_no, x_command_short from bp.backups where status = 3168 and node_no = " + str(node_no) + " and instance_id = " + str(instance_id) + " and backup_no in (select a.backup_no from bp.sm_backups a where device <> '(deleted)')  and x_command_short like ('%HYPERVFULL%') order by backup_no desc limit 1"
    dbResult = dbHandler.executeQuery(query)
    for row in dbResult:
        findHyperVFull = row[2].find("HYPERVFULL")
        if(findHyperVFull > -1):
            fullBackups.append(row)
    return fullBackups

def getFullVMWareBackupsFromDB(instance_id):
    fullBackups = []
    query = "select backup_no, job_no, x_command_short from bp.backups where instance_id = " + str(instance_id)
    dbResult = dbHandler.executeQuery(query)
    for row in dbResult:
        findHyperVFull = row[2].find("VMESXFULL")
        if(findHyperVFull > -1):
            fullBackups.append(row)
    return fullBackups

def getInstanceIdFromVmVMWare(appliance_ip, vmName):
    query = "select instance_id from bp.application_instances where key2 = " + "'"+vmName+"'"
    dbResult = dbHandler.executeQuery(appliance_ip, query)
    return int(dbResult[0][0])
    
def getIncrementalHyperVBackupsFromDB(node_no, instance_id):
    incrBackups = []
    query = "select backup_no, job_no, x_command_short from bp.backups where node_no = " + str(node_no) + " and instance_id = " + str(instance_id) + " and backup_no in (select a.backup_no from bp.sm_backups a where device <> '(deleted)')  and x_command_short like ('%HYPERVFULL%') order by backup_no desc limit 1"
    dbResult = dbHandler.executeQuery(query)
    for row in dbResult:
        findHyperVFull = row[2].find("HYPERVINCR")
        if(findHyperVFull > -1):
            incrBackups.append(row)
    return incrBackups

def getHyperVBackupsFromDB(node_no, instance_id):
    backups = []
    dbResult = dbHandler.executeQuery("select backup_no, job_no, x_command_short from bp.backups where node_no = " + str(node_no) + " and instance_id = " + str(instance_id) + " and x_command_short like ('%HYPERVFULL%') and backup_no in (select a.backup_no from bp.sm_backups a where device <> '(deleted)') order by backup_no desc limit 1")
    fullBackup = 0
    if(len(dbResult) > 0):
        for row in dbResult:
            fullBackup = row[0]
            backups.append(row[0])
    dbResult = dbHandler.executeQuery("select backup_no, job_no, x_command_short from bp.backups where node_no = " + str(node_no) + " and instance_id = " + str(instance_id) + " and x_command_short like ('%HYPERVINCR%') and backup_no > " + str(fullBackup) + " and backup_no in (select a.backup_no from bp.sm_backups a where device <> '(deleted)') order by backup_no")
    if(len(dbResult) > 0):
        for row in dbResult:
            backups.append(row[0])
    return backups

def getXenBackupsFromDB(node_no, instance_id):
    backups = []
    dbResult = dbHandler.executeQuery("select backup_no, job_no, x_command_short from bp.backups where node_no = " + str(node_no) + " and instance_id = " + str(instance_id) + " and x_command_short like ('%XENFULL%') and backup_no in (select a.backup_no from bp.sm_backups a where device <> '(deleted)') order by backup_no desc limit 1")
    fullBackup = 0
    if(len(dbResult) > 0):
        for row in dbResult:
            fullBackup = row[0]
            backups.append(row[0])
#     dbResult = dbHandler.executeQuery("select backup_no, job_no, x_command_short from bp.backups where node_no = " + str(node_no) + " and instance_id = " + str(instance_id) + " and x_command_short like ('%XENINCR%') and backup_no > " + str(fullBackup) + " and backup_no in (select a.backup_no from bp.sm_backups a where device <> '(deleted)') order by backup_no")
#     if(len(dbResult) > 0):
#         for row in dbResult:
#             backups.append(row[0])
    return backups

def getInstanceIdFromClientName(appliance_ip, clientName):
    query = "select instance_id from bp.application_instances where node_no = (select node_no from bp.nodes where node_name = '"+clientName+"')"
    dbResult = dbHandler.executeQuery(appliance_ip, query)
    return int(dbResult[0][0])
    
def getInstanceIdFromVmVMWareForSid(envIp, vmName):
    query = "select instance_id from bp.application_instances where key2 = " + "'"+ str(vmName) +"'"
    dbResult = dbHandler.executeQueryForIp(query, envIp)
    for row in dbResult:
        return int(row[0])


def checkBackupSucess(appliance_ip,backupId): 
    strQuery = "select status from bp.backups where backup_no=" + str(backupId)
    result = 0
    dbResult = dbHandler.executeQuery(appliance_ip,strQuery)
    result = int(dbResult[0][0])
    if(result == 3168):
        return "Successful"
    elif(result == 32832):
        logHandler.logging.info("the result",str(result))
        return "Warning"
    elif(result == 20544):
        logHandler.logging.info("the result",str(result))
        return "Failed"    
    else:
        logHandler.logging.info("the result",str(result))
        return "Unknown"
     
     
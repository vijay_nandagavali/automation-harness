'''
Created on Jun 9, 2015

@author: Nikhil S

Modified on Jan 20, 2017

@author: Divya.J
'''
import json
from util import jsonHandler, dbHandler
from config import apiconfig
import time

def getRepConfigFromResponse(actualResponse):
    repConfigLists = []
    jsonData = json.loads(actualResponse)
    
    maxCon = str(jsonData['config']['max_concurrent'])
    repConfigLists.append(maxCon)
    repMail = str(jsonData['config']['report_mail_to'])
    repConfigLists.append(repMail)
    repTime = str(jsonData['config']['report_time'])
    repConfigLists.append(repTime)
       
    repConfigLists.sort()
    return repConfigLists

def getRepTargetFromDB(targetName):
    
    query = "select count(*) from bp.source_replication_config where url_name = '" + str(targetName) + "'"
    dbResult = dbHandler.executeQuery(query)
    target=dbResult[0][0]
    return target

def getPendingRepSrcFromResponse(actualResponse):
    repPendingSrcList = []
    jsonData = json.loads(actualResponse)
    for i in range(0, len(jsonData['pending'])):
        repPendingSrcList.append(str(jsonData['pending'][i]['request_id']))
        repPendingSrcList.append(str(jsonData['pending'][i]['host']))
        repPendingSrcList.append(str(jsonData['pending'][i]['asset_tag']))
        repPendingSrcList.append(str(jsonData['pending'][i]['status']))
        repPendingSrcList.append(str(jsonData['pending'][i]['created']))
    repPendingSrcList.sort()
    return repPendingSrcList    

def getPendingRepSrcFromDB():
    repPendingSrcList = []
    query = "select asset_tag,created ,hostname,request_id,status from  bp.target_replication_config where status = 'pending'"
    dbResult = dbHandler.executeQuery(query,"target")
    for row in dbResult:
        repPendingSrcList.append(str(row[0]))
        repPendingSrcList.append(str(row[1]))
        repPendingSrcList.append(str(row[2]))
        repPendingSrcList.append(str(row[3]))
        repPendingSrcList.append(str(row[4]))
    repPendingSrcList.sort()
    return repPendingSrcList   
    
def getRequestStatusFromHostName_DB(reqID):
    query = "select status from bp.target_replication_config where request_id= '" +str(reqID)+"'"
    dbResult = dbHandler.executeQuery(query,"target")    
    status = dbResult[0][0]
    return status

def getTargetIDFromDB(tarName):
    query = "select target_id from bp.source_replication_config where target= '" +str(tarName)+"'"
    dbResult = dbHandler.executeQuery(query,"source")    
    if(dbResult):
        tarid = dbResult[0][0]
    else:
        tarid=0
    if(tarid == 0):
        return None
    else:
        return tarid   

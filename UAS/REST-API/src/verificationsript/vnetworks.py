'''
Created on Oct 21, 2014

@author: Amey.k
'''
from util import linuxMachineConnect
import json

def getNetworkInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    featurelist = []
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_network adapter list')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command(' /usr/bp/bin/cmc_network dns list')
    temp = (out.readlines())
    
    if(len(temp)>1):
        featurelist.append(str(temp[0]).strip(' \r\t\n'))
        featurelist.append(str(temp[1]).strip(' \r\t\n'))
    else:
        featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('/usr/bp/bin/cmc_network search list')
    # print str(out.readlines())
    temp = (out.readlines())
    if(len(temp)>0):
        featurelist.append(str(temp[0]).strip(' \r\t\n'))
    #_, out, err = conn.exec_command('cat /etc/sysconfig/network-scripts/ifcfg-eth0 | grep IPADDR | cut -d \'=\' -f2')
    _, out, err = conn.exec_command('cat /etc/sysconfig/network-scripts/ifcfg-eth0 | grep IPADDR | cut -c8-22')
    # print str(out.readlines())
    temp = (out.readlines())
    
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('cat /etc/sysconfig/network-scripts/ifcfg-eth0 | grep NETMASK | cut -c9-21')
    #_, out, err = conn.exec_command('cat /etc/sysconfig/network-scripts/ifcfg-eth0 | grep NETMASK | cut -d \'=\' -f2')
    # print str(out.readlines())
    temp = (out.readlines())
    
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('cat /etc/sysconfig/network-scripts/ifcfg-eth0 | grep GATEWAY | cut -c9-21')
    #_, out, err = conn.exec_command('cat /etc/sysconfig/network-scripts/ifcfg-eth0 | grep GATEWAY |  cut -d \'=\' -f2')
    # print str(out.readlines())
    temp = (out.readlines())
    
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    
    # print featurelist
    return featurelist


def getNetworkInformationFromResponse(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['networks'][0]['id']))
    jsonParameter.append(str(jsonData['networks'][0]['dns1']))
    if (jsonData['networks'][0]['dns2']):
        jsonParameter.append(str(jsonData['networks'][0]['dns2']))
    if (jsonData['networks'][0]['search']):
        searchStr=str(jsonData['networks'][0]['search'][0])
        if(searchStr=="v"):
            searchStr="vmware_ueb"
        jsonParameter.append(searchStr)
    jsonParameter.append(str(jsonData['networks'][0]['ip']))
    jsonParameter.append(str(jsonData['networks'][0]['netmask']))
    jsonParameter.append(str(jsonData['networks'][0]['gateway']))
    
    return jsonParameter
    
def getVirtualBridgeNetworkInformation():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    print conn
    featurelist = []
    _, out, err = conn.exec_command('sed -n "18p" /etc/qemu-restore-ifup | cut -c9-23')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('sed -n "19p" /etc/qemu-restore-ifup | cut -c9-23')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('sed -n "20p" /etc/qemu-restore-ifup | cut -c9-23')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    _, out, err = conn.exec_command('sed -n "21p" /etc/qemu-restore-ifup | cut -c11-38')
    temp = out.readlines()
    featurelist.append(str(temp[0]).strip(' \r\t\n'))
    
    return featurelist

def getVirtualBridgeNetworkInformationFromResponse(input_json):
    #input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['bridge']['network']).strip(' \r\t\n'))
    jsonParameter.append(str(jsonData['bridge']['netmask']).strip(' \r\t\n'))
    jsonParameter.append(str(jsonData['bridge']['gateway']).strip(' \r\t\n'))
    jsonParameter.append(str(jsonData['bridge']['dhcprange']).strip(' \r\t\n'))
    return jsonParameter

def getNetworkBridgeInformationFromMachine():
    conn = linuxMachineConnect.SshRemoteMachineConnection()
    #print conn
    bridgeInfo = []
    _, out, err = conn.exec_command('ifconfig | grep br0 | cut -c1-3')
    temp = out.readlines()
    if(len(temp) > 0):
        bridgeInfo.append(str(temp[0]).strip(' \r\t\n'))
    return bridgeInfo

def getNetworkBridgeInformationFromResponse(input_json):
    jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.   
    jsonParameter = []
    jsonParameter.append(str(jsonData['bridge']).strip(' \r\t\n'))
    return jsonParameter

    
    


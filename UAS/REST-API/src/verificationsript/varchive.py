'''
Created on Sep 15, 2014

@author: Abhijeet.m
'''
from util import dbHandler, dateTimeHandler, logHandler
from util import jsonHandler
import json
from testdata import archive_testdata

def getConnectedMediaFromDB():
    dbResult = dbHandler.executeQuery("""SELECT storage_name from bp.storage where storage_name!= 'Internal';""") 
    return dbResult[0][0]

def getArchiveMediaSetsFromDB(storageName):
    query = """select distinct archive_set_path, description from bp.archive_sets a, bp.archive_profiles b, bp.archives c where 
            a.profile_id = b.profile_id and a.archive_set_id = c.archive_set_id and b.target = '""" + str(storageName) + """' order by archive_set_path"""
    dbResult = dbHandler.executeQuery(query)
    return dbResult
    
def getConnectedMediaFromResponse(actualResponse):
    mediaName = ''
    responseData = jsonHandler.getAllParametersFromJson(actualResponse) 
    logHandler.logging.info("Response Data: " + str(responseData))
    if(len(responseData[0])>0):
        for key,value in responseData[0].items():
            if(key ==u'name'):
                mediaName = value
    return mediaName

def getArchiveMediaSetsFromResponse(actualResponse):
    responseData = jsonHandler.getAllParametersFromJson(actualResponse)
    return responseData
    
def getArchiveSetCountFromDB():
    query = "select count(*) from bp.archive_sets"
    dbResult = dbHandler.executeQuery(query)
    return dbResult[0][0]

def getArchiveSetIdFromDB():
    archiveSetIds = []
    query = "select archive_set_id from bp.archive_sets where media_label <> '" + str(archive_testdata.reserved) + "'"
    dbResult = dbHandler.executeQuery(query)
    if(len(dbResult) > 0):
        archiveSetIds = dbResult[0][0]
    else:
        logHandler.logging.info("No data present")
    return archiveSetIds

def verifyDeleteActualResponse(actualresponse):
    jsonList = json.loads(actualresponse)
    if (jsonList['result'][0]['code'] == '0'):
        return "Pass"
    else:
        return "Fail"
    
def getSpecificArchiveSetIdFromDB(setId):
    query = "select count(*) from bp.archive_sets where archive_set_id=" + str(setId)
    dbResult = dbHandler.executeQuery(query)
    return dbResult[0][0]
    
def getFileLevelArchiveCountFromDB():
    query = "select count(*) from bp.archives where backtype=4 and archive_success='t'"
    dbResult = dbHandler.executeQuery(query)
    return dbResult[0][0]

def getArchiveFilesActualResponseParameters(actualResponse):
    response = jsonHandler.getArchiveFileParametersInJson(actualResponse)
    return response
    
def getFileLevelArchiveIdFromDB():
    query = "select archive_id from bp.archives where backtype=4 and archive_success='t'"
    dbResult = dbHandler.executeQuery(query)
    return dbResult[0][0]

def getArchivePrepareStatusFromDB(storageName):
    query = "select count(*) from bp.archives a, bp.archive_sets b, bp.archive_profiles c, bp.archive_instances d where a.archive_set_id = b.archive_set_id and b.profile_id = c.profile_id and a.archive_instance_id = d.archive_instance_id and target = '" + storageName + "'"
    dbResult = dbHandler.executeQuery(query)
    return dbResult[0][0]

def getArchiveCountFromDB():
    query = "select count(*) from bp.archives"
    dbResult = dbHandler.executeQuery(query)
    return dbResult[0][0]

def getArchiveStatusResponseParams(actualResponse):
    responseData = jsonHandler.getArchiveStatusParametersInJson(actualResponse)
    return responseData

def getArchiveDataFromDB():
    dbResult = []
    dbArchiveId = dbHandler.executeQuery("select archive_id from bp.archives order by archive_id")
    for i in range(0,len(dbArchiveId)):
        dbArchiveId[i] = str(dbArchiveId[i]).replace(",", "")
        dbResult.append(dbHandler.executeQuery("select archive_set_id from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select archive_success from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #is_imported
        #client_id
        dbResult.append(dbHandler.executeQuery("select instance_id from bp.archive_instances where archive_instance_id = (select archive_instance_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        dbResult.append(dbHandler.executeQuery("select client_name from bp.archive_instances where archive_instance_id = (select archive_instance_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        dbResult.append(dbHandler.executeQuery("select client_os from bp.archive_instances where archive_instance_id = (select archive_instance_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        #os_type
        dbResult.append(dbHandler.executeQuery("select app_id from bp.archive_instances where archive_instance_id = (select archive_instance_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        #app_name
        #instance_description
        dbResult.append(dbHandler.executeQuery("select orig_backup_no from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #type
        dbResult.append(dbHandler.executeQuery("select file_mib_size from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select file_count from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select compressed from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select encrypted from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select deduped from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #fastseed
        dbResult.append(dbHandler.executeQuery("select vmware_template from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(str(dbArchiveId[i]))
        dbResult.append(dbHandler.executeQuery("select archive_set_id from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #dbResult.append(dbHandler.executeQuery("select archive_set_path from bp.archive_sets where archive_set_id = (select archive_set_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        #dbResult.append(dbHandler.executeQuery("select elapsed_secs from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #backup_date
        #storage
        
    for i in range(0,len(dbResult)):
        dbResult[i] = str(dbResult[i]).replace("',)]", "")
        dbResult[i] = str(dbResult[i]).replace("[('", "")
        dbResult[i] = str(dbResult[i]).replace(",)]", "")
        dbResult[i] = str(dbResult[i]).replace("[(", "")
        dbResult[i] = str(dbResult[i]).replace(")", "")
        dbResult[i] = str(dbResult[i]).replace("(", "")
    return dbResult   
        
def getArchiveDataFromResponse(actualResponse):
    responseData = jsonHandler.getArchiveStatusValuesInJson(actualResponse)
    return responseData

def getSpecificArchiveDataFromDB(setId):
    dbResult = []
    dbArchiveId = dbHandler.executeQuery("select archive_id from bp.archives where archive_set_id=" + str(setId))
    for i in range(0,len(dbArchiveId)):
        dbArchiveId[i] = str(dbArchiveId[i]).replace(",", "")
        dbResult.append(dbHandler.executeQuery("select archive_set_id from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select archive_success from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #is_imported
        #client_id
        dbResult.append(dbHandler.executeQuery("select instance_id from bp.archive_instances where archive_instance_id = (select archive_instance_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        dbResult.append(dbHandler.executeQuery("select client_name from bp.archive_instances where archive_instance_id = (select archive_instance_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        dbResult.append(dbHandler.executeQuery("select client_os from bp.archive_instances where archive_instance_id = (select archive_instance_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        #os_type
        dbResult.append(dbHandler.executeQuery("select app_id from bp.archive_instances where archive_instance_id = (select archive_instance_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        #app_name
        #instance_description
        dbResult.append(dbHandler.executeQuery("select orig_backup_no from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #type
        dbResult.append(dbHandler.executeQuery("select file_mib_size from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select file_count from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select compressed from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select encrypted from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(dbHandler.executeQuery("select deduped from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #fastseed
        dbResult.append(dbHandler.executeQuery("select vmware_template from bp.archives where archive_id=" + str(dbArchiveId[i])))
        dbResult.append(str(dbArchiveId[i]))
        dbResult.append(dbHandler.executeQuery("select archive_set_id from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #dbResult.append(dbHandler.executeQuery("select archive_set_path from bp.archive_sets where archive_set_id = (select archive_set_id from bp.archives where archive_id = " + str(dbArchiveId[i]) + ")"))
        #dbResult.append(dbHandler.executeQuery("select elapsed_secs from bp.archives where archive_id=" + str(dbArchiveId[i])))
        #backup_date
        
    for i in range(0,len(dbResult)):
        dbResult[i] = str(dbResult[i]).replace("',)]", "")
        dbResult[i] = str(dbResult[i]).replace("[('", "")
        dbResult[i] = str(dbResult[i]).replace(",)]", "")
        dbResult[i] = str(dbResult[i]).replace("[(", "")
        dbResult[i] = str(dbResult[i]).replace(")", "")
        dbResult[i] = str(dbResult[i]).replace("(", "")
    return dbResult

def getArchiveSearchResponseParams(actualResponse):
    responseData = jsonHandler.getArchiveSearchParametersInJson(actualResponse)
    return responseData

def verifyPutArchiveResponse(actualResponse):
    response = jsonHandler.putArchiveResponseParse(actualResponse)
    return response

def verifyPutArchiveDB(jobNo):
    query = "select archive_id from bp.archives where archive_instance_id=(select archive_instance_id from bp.archive_profile_instances where profile_id=(select profile_id from bp.archive_job_profiles where job_no=" + str(jobNo) + "))"
    #print query
    dbResult = dbHandler.executeQuery(query)
    logHandler.logging.info("DB Result: " + str(dbResult))
    
    if (dbResult == []):
        raise Exception("No data found in Database..")
    return dbResult[0][0] 

#created for vmware full archive
def verifyPutArchiveDB2(jobNo):
    query = "select archive_id from bp.archives"
    #print query
    dbResult = dbHandler.executeQuery(query)
    logHandler.logging.info("DB Result: " + str(dbResult))
    
    if (dbResult == []):
        raise Exception("No data found in Database..")
    return dbResult[0][0]  

def verifyPutArchiveCheckResponseParams(actualResponse):
    responseData = jsonHandler.putArchiveCheckParametersInJson(actualResponse)
    return responseData

def getPostArchiveCatalogResponseParams(actualResponse):
    responseData = jsonHandler.postArchiveCatalogParametersInJson(actualResponse)
    return responseData

def getArchiveSetResponseParams(actualResponse):
    """Get values for verification of archive set list from json response"""
    responseData = jsonHandler.getArchiveSetValuesInJson(actualResponse)
    return responseData

""" Below method fetches the archive catalog data from database and returns the same """
def getPostArchiveCatalogDataFromDB(storageName):
    query = "select archive_set_path, archive_set_id, description from bp.archive_profiles a, bp.archive_sets b where a.profile_id = b.profile_id and target = '" + storageName + "' and archive_set_path <> '" + archive_testdata.reserved + "' order by archive_set_id"
    dbResult = dbHandler.executeQuery(query)
    dbResultList = []
    for i in range(0,len(dbResult)):
        date = dateTimeHandler.getDateInAMPMFormat(dbResult[i][0])
        dbResultList.append("Archive set from " + str(date).upper() + " already in database (id " + str(dbResult[i][1]) + "): " + str(dbResult[i][2]))
    return dbResultList

""" Below method fetches the archive catalog data from response and returns the same """
def getPostArchiveCatalogFromResponse(actualResponse):
    responseData = jsonHandler.getPostArchiveCatalogResponseFromJson(actualResponse)
    return responseData[0]

""" Below method fetches the parameters from response and returns the same """
def getMediaCandidatesResponseParams(actualResponse):
    responseData = jsonHandler.getMediaCandidatesParametersInJson(actualResponse)
    return responseData

""" Below method fetches the media candidate details from DB and returns the same """
def getMediaCandidatesDataFromDB(hardCodedData):
    query = "select storage_name, storage_type from bp.storage"
    dbResult = dbHandler.executeQuery(query)
#     dbResultList = []
    for i in range(0,len(dbResult)):
        if(dbResult[i][1] <> 'internal'):
            hardCodedData.append(dbResult[i][0])
            hardCodedData.append("external")
    return hardCodedData

""" Below method fetches the media candidate details from response and returns the same """
def getMediaCandidatesDataFromResponse(actualResponse):
    responseData = jsonHandler.getMediaCandidatesResponseFromJson(actualResponse)
    return responseData

def getArchiveSetsDataFromDB():
    """Get database values for verification of archive set list"""
    dbResult = []   
    dbArchiveSetId = dbHandler.executeQuery("select archive_set_id from bp.archive_sets order by archive_set_id")
    for i in range(0,len(dbArchiveSetId)):
        dbArchiveSetId[i] = str(dbArchiveSetId[i]).replace(",", "")
        dbResult.append(dbHandler.executeQuery("select archive_set_id from bp.archive_sets where archive_set_id=" + str(dbArchiveSetId[i])))                               
        dbResult.append(dbHandler.executeQuery("select to_char(to_timestamp(creation_timestamp),'mm/dd/yyyy hh12:mi:ss am')    from bp.archive_sets where archive_set_id=" + str(dbArchiveSetId[i])))
        #not checking available parameter because it is documented that it can give false positives
        #dbResult.append(dbHandler.executeQuery("select CASE WHEN (state_success) = 't' THEN 'True' ELSE 'False' END from bp.archive_sets where archive_set_id=" + str(dbArchiveSetId[i]))) 
        #dbResult.append(dbHandler.executeQuery("select CASE WHEN count(*) > 0 THEN 'True' ELSE 'False' END from bp.archives where archive_set_id=" + str(dbArchiveSetId[i])))
        dbResult.append(str(archive_testdata.sid))
        dbResult.append(dbHandler.executeQuery("select status from bp.archive_sets where archive_set_id=" + str(dbArchiveSetId[i])))                                
        dbResult.append(dbHandler.executeQuery("select name from bp.systems"))                    
        correctDbResult(dbResult)
    return dbResult

def getArchiveSetByIdResponseParams(actualResponse):
    """Get values for verification of archive set by Id from json response"""
    responseData = jsonHandler.getArchiveSetByIdValuesInJson(actualResponse)
    return responseData

def getArchiveSetDataByIdFromDB(archiveSetId):
    """Get database values for verification of archive set data by Id"""
    dbResult = []       
    dbResult.append(dbHandler.executeQuery("select status from bp.archive_sets where archive_set_id=" + str(archiveSetId)))
    dbResult.append(dbHandler.executeQuery("select serial_no from bp.archive_disks where archive_set_id=" + str(archiveSetId)))
    dbResult.append(dbHandler.executeQuery("select media_label from bp.archive_sets where archive_set_id=" + str(archiveSetId)))       
    dbResult.append(dbHandler.executeQuery("select originating_asset from bp.archive_sets where archive_set_id=" + str(archiveSetId)))
    dbResult.append(dbHandler.executeQuery("select archive_set_id from bp.archive_sets where archive_set_id=" + str(archiveSetId)))
    dbResult.append(dbHandler.executeQuery("select CASE WHEN (elapsed_secs) = 0 THEN '' ELSE TO_CHAR((elapsed_secs || ' second')::interval, 'HH24:MI:SS') END from bp.archive_sets where archive_set_id=" + str(archiveSetId)))                              
    dbResult.append(dbHandler.executeQuery("select to_char(to_timestamp(creation_timestamp),'mm/dd/yyyy hh12:mi:ss am')    from bp.archive_sets where archive_set_id=" + str(archiveSetId)))                                    
    correctDbResult(dbResult)
    
    dbResultProfile = []
    dbProfileid = dbHandler.executeQuery("select profile_id from bp.archive_sets where archive_set_id= "+str(archiveSetId))
    dbProfileid = dbProfileid[0][0]
    dbResultProfile.append(dbHandler.executeQuery("select description from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    dbResultProfile.append(dbHandler.executeQuery("select target from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    correctDbResult(dbResultProfile)
    
    dbResultProfileOptions= []
    dbResultProfileOptions.append(dbHandler.executeQuery("select append from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    dbResultProfileOptions.append(dbHandler.executeQuery("select compress from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    dbResultProfileOptions.append(dbHandler.executeQuery("select dedup from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    dbResultProfileOptions.append(dbHandler.executeQuery("select email_report from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    dbResultProfileOptions.append(dbHandler.executeQuery("select encrypt from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    dbResultProfileOptions.append(dbHandler.executeQuery("select fastseed from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    dbResultProfileOptions.append(dbHandler.executeQuery("select purge from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    dbResultProfileOptions.append(dbHandler.executeQuery("select retention_days from bp.archive_profiles where profile_id =" + str(dbProfileid)))
    correctDbResult(dbResultProfileOptions)
    
    dbResultProfileInstances = []    
    instanceId = dbHandler.executeQuery("select archive_instance_id from bp.archive_instances where archive_instance_id in (select archive_instance_id from bp.archive_profile_instances where profile_id = "+ str(dbProfileid) + ")")
    for i in range(0,len(instanceId)):
        instanceId[i] = str(instanceId[i]).replace(",", "")
        dbResultProfileInstances.append(dbHandler.executeQuery("select instance_id from bp.archive_instances where archive_instance_id =" + str(instanceId[i])))
       # dbResultProfileInstances.append(dbHandler.executeQuery("select client_name from bp.archive_instances where archive_instance_id =" + str(instanceId[i])))
        dbResultProfileInstances.append(dbHandler.executeQuery("select key2 from bp.archive_instances where archive_instance_id =" + str(instanceId[i])))
    correctDbResult(dbResultProfileInstances)
    
    return dbResult, dbResultProfile, dbResultProfileOptions, dbResultProfileInstances

def correctDbResult(dbResult):
    for i in range(0,len(dbResult)):
        dbResult[i] = str(dbResult[i]).replace("',)]", "")
        dbResult[i] = str(dbResult[i]).replace("[('", "")
        dbResult[i] = str(dbResult[i]).replace(",)]", "")
        dbResult[i] = str(dbResult[i]).replace("[(", "")
        dbResult[i] = str(dbResult[i]).replace(")", "")
        dbResult[i] = str(dbResult[i]).replace("(", "")
    return dbResult

def getArchiveIdsFromDB(storageName, instance_id):
    query = "select a.archive_id, a.archive_set_id, orig_backup_no, backtype, orig_mib_size, file_mib_size, file_count, archive_path, b.media_label, description, target, instance_id, client_name, key1 from bp.archives a, bp.archive_sets b, bp.archive_profiles c, bp.archive_instances d where a.archive_set_id = b.archive_set_id and b.profile_id = c.profile_id and a.archive_instance_id = d.archive_instance_id and target = '" + storageName + "' and instance_id = " + str(instance_id)
    dbResult = dbHandler.executeQuery(query)
    archiveIds = []
    if(len(dbResult) > 0):
        for row in dbResult:
            archiveIds.append(row[0])
    else:
        logHandler.logging.info("No archives available for this storage..")
    return archiveIds

def getArchivesForParticularStorageFromDB(storageName):
    query = "select a.archive_id, a.archive_set_id, orig_backup_no, backtype, orig_mib_size, file_mib_size, file_count, archive_path, b.media_label, description, target, instance_id, client_name, key1 from bp.archives a, bp.archive_sets b, bp.archive_profiles c, bp.archive_instances d where a.archive_set_id = b.archive_set_id and b.profile_id = c.profile_id and a.archive_instance_id = d.archive_instance_id and target = '" + storageName + "'"
    dbResult = dbHandler.executeQuery(query)
    if len(dbResult) != 0:
        logHandler.logging.info("No archives available for this storage..")
    return dbResult

def getIncrementalArchiveBackupNosFromDB(instanceId):
    archiveInstanceId = 0
    backupsNos = []
    dbResult = dbHandler.executeQuery("select archive_instance_id from bp.archive_instances where instance_id = " + str(instanceId))
    if(len(dbResult) > 0):
        archiveInstanceId = dbResult[0][0]
    if(archiveInstanceId):
        dbResult = dbHandler.executeQuery("select orig_backup_no from bp.archives where archive_instance_id = " +str(archiveInstanceId))
        if(len(dbResult) > 0):
            for row in dbResult:
                backupsNos.append(row[0])
    return backupsNos

def getArchiveCatalogDataFromDB():
    catalogData = ['instance']
    query = "select c.node_no as client_id, b.client_name, case when c.node_no <> 1 then c.key1 else 'System Metadata' end as database_name, b.instance_id, a.archive_success, c.node_no as client_id, b.instance_id, b.client_name, b.client_os, b.app_id, a.orig_backup_no, case when a.backtype = " + str(archive_testdata.fullBacktype) + " then 'Full' when a.backtype = " + str(archive_testdata.metadataBacktype) + " then 'system metadata' end as type, a.file_mib_size, a.file_count, a.compressed, a.encrypted, a.deduped, a.vmware_template, a.archive_id, a.archive_set_id from bp.archives a, bp.archive_instances b, bp.application_instances c, bp.archive_sets d where a.archive_instance_id = b.archive_instance_id and b.instance_id = c.instance_id and a.archive_set_id = d.archive_set_id order by a.archive_id"
    dbResult = dbHandler.executeQuery(query)               
    for i in range(0,len(dbResult)):
        temp = dbResult[i]
        for j in range(0,len(temp)):
            catalogData.append(temp[j])
    catalogData.sort()
    return catalogData

def compareResponseAndDbData(dataFromResponse, dataFromDB):
    list1 = []
    list2 = []
    res = ""
    if(len(dataFromDB) == len(dataFromResponse)):
        for i in range(0,len(dataFromDB)):
            if(dataFromDB[i] <> dataFromResponse[i]):
                list1.append(dataFromResponse[i])
                list2.append(dataFromDB[i])
    for i in range(0,len(list1)):
        list1[i] = int(list1[i])
    list1.sort()
    if(len(list1) == len(list2)):
        for i in range(0,len(list1)):
            if(list1[i] <> list2[i]):
                res = "Fail"
            else:
                res = "Pass"
    return res
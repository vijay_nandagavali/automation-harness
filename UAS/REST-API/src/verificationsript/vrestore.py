'''
Created on Mar 9, 2015

@author: Amey.k
'''

import json, time
import paramiko


from util import dbHandler, logHandler
from util import jsonHandler
from testdata import restore_testdata

def isHyperVRestoredMachineAlive(instanceNameHyperV,ipAddress):
    print "instanceNameHyperV" + instanceNameHyperV
    print "ipAddress" + ipAddress
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    print "\n connecting \n"
    ssh.connect(ipAddress,username = 'Administrator', password = 'Unitrends1')
    print "\n connected \n"
    logHandler.logging.info("Hyper-V Successfully connected")
    
    startVM = "Start-VM "+str(instanceNameHyperV)
    cmdStart = "powershell -InputFormat none -OutputFormat TEXT " + startVM
    ssh.exec_command(cmdStart)  
    logHandler.logging.info("....Hyper-V VM Start Command Executed....")
    print ("....Hyper-V VM Start Command Executed....")
    time.sleep(10)
    str1  = "Get-VM "+str(instanceNameHyperV)
    cmd = "powershell " + str1

    print str1
    a=0
    count=0
    flag = 0
    while (count<60):        
        stdin, stdout, stderr = ssh.exec_command(cmd)  
                   
        for line in stdout.read().splitlines():
            print line
            
            flag=0    
            if 'Running' in line:
                #print "The VM on HyperV is Started Successfully."
                flag=1
                break
            else:
                print "Waiting for a VM Restart"
                logHandler.logging.info("Waiting for a VM Restart")   
                time.sleep(1)
        
        count = count+1
        if(flag==1):
            break
    
    print "The VM on HyperV is Alive and is Powered On Successfully."
    logHandler.logging.info("The VM on HyperV is Alive and is Powered On Successfully.") 
    
    if(flag==1):
        return True
    else:
        return False
    

def getRestoreJobIdFromResponse(actualResponse):
    responseData = json.loads(actualResponse)
    return responseData['id']

def checkRestoreJobFromDB(jobId):
    dbResult = dbHandler.executeQuery("""select job_no from bp.jobs where job_no=""" +jobId)
    return dbResult

def getJobCountFromDB(jobNo):
    query = "select count(*) from bp.jobs where job_no = " + str(jobNo)
    dbResult = dbHandler.executeQuery(query)
    if(len(dbResult) > 0):
        return dbResult[0][0]
    else:
        return "Fail"
    
def checkRestoreVMWareJobFromDB(jobId):
    dbResult = dbHandler.executeQuery("""select count(*) from bp.jobs where job_no in """ + "(" +jobId + ")")
    if(len(dbResult) > 0):
        return dbResult[0][0]
    else:
        return "Fail"
    
def getArchiveRestoreBackupIdFromDB(appliance_ip,jobNo):
    query = "select backup_no from bp.backups where job_no = " + str(jobNo)
    dbResult = dbHandler.executeQuery(appliance_ip,query)
    backupId = "0"
    if(len(dbResult) > 0):
        backupId = dbResult[0][0]
    else:
        logHandler.logging.info("Backup ID not found")
        print "Backup ID not found"
    return backupId   

def getRestoreBackupsFromDB(node_no, bid):
    restoreBackups = []
    query = "select backup_no, job_no, x_command_short from bp.backups where node_no = " + str(node_no) + " and status = " + str(restore_testdata.successStatus) + " order by backup_no desc limit 1"
    dbResult = dbHandler.executeQuery(query)
    logHandler.logging.info("DB Result: " + str(dbResult))
    if(len(dbResult) > 0):
        for row in dbResult:
            findRestore = row[2].find("BACKUPNO=" + str(bid))
            if(findRestore > -1):
                restoreBackups.append(row)
    logHandler.logging.info("Restore Backups from DB: " + str(restoreBackups))
    return restoreBackups[0][0]

    
    
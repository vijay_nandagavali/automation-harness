'''
Created on Aug 8, 2016

@author: vijay.n
'''

from os.path import sys
sys.path.append("/root/UAS/REST-API/src")
from report import Report_Mail
from util import exceptionHandler,logHandler
import Environment, TestCases
import Constants
import datetime
import logging
from config import apiconfig

if __name__ == "__main__":
    
    try:
        exeId = str(sys.argv[1])       
        test_name=str(sys.argv[2])
        emailFlag  = str(sys.argv[3])
        
        logger = apiconfig.UAS_DIR_PATH + "/REST-API/Logs/RestAPI_" + str(exeId) + ".log"
        logging.basicConfig(filename=logger, level=logging.INFO, filemode='a')
        envJsonPath =str(apiconfig.UAS_DIR_PATH)+str("/Config/") + str(exeId) + "/Environment.json" 
        InfraObj=Environment.Environment(envJsonPath)
        if(test_name.startswith("MQ") or test_name.startswith("QAAUT")):
            logging.info("Importing :" + str(test_name))
            command_module= __import__("testscript.%s" %test_name)
            test_script_module = getattr(command_module,test_name)
            test_script_class = getattr(test_script_module,test_name)
            test_script_instance = test_script_class(exeId,InfraObj.uebInfra,InfraObj.machineInfra)
            logging.info("Executing :" + str(test_name))
            test_script_instance.runTest()
        else:
            logging.info(("test_name:") + str(test_name) + "does not start with MQ or QAAUT ")
    
    except:
        logHandler.logging.error(exceptionHandler.PrintException())
    
    finally:        
        if(emailFlag == "True"):
            Report_Mail.send_mail(exeId);   
    
'''
Created on Jun 7, 2016

@author: pratik.p
'''
from util import logHandler, exceptionHandler
import json

class TestCases(object):
    def __init__(self, jsonFilePath,tcid):
        try:
            with open(jsonFilePath) as data_file:
                self.infraData=json.load(data_file)
                self.uebConfig={}
                self.setValues(jsonFilePath,tcid)
        except:
            logHandler.logging.error(exceptionHandler.PrintException())
             
    def setValues(self,tcsJsonPath,scriptId):
        try:
            with open(tcsJsonPath) as data_file:
                tcsJson = json.load(data_file)
                for testCase in tcsJson:
                    print str(testCase)
                    if(scriptId==testCase['testCaseId']):
                        for key in testCase:
                            if(key in ['clientType','backupType']):
                                #print "self.set"+key+"("+testCase[key][key]+")"
                                #eval("self.set"+key+"("+'"'+testCase[key][key]+'"'+")")
                                self.uebConfig[key]=testCase[key][key]
                            elif key not in ["id","status","releases","component","product", "testCaseId", "project", "testScriptType"] and testCase[key] != None:
                                #eval("self.set"+key+"("+'"'+testCase[key]+'"'+")")    
                                self.uebConfig[key]=testCase[key]         
        except:
            exceptionHandler.PrintException("error")
            logHandler.logging.info("Error: " + str(self.PrintException())) 
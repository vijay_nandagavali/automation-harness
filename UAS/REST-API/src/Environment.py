from util import logHandler, exceptionHandler
import json

class Environment(object):
    environmentIp=""
    dbuser =  ""
    dbpassword = ""
    dbname = ""
    machineuser = ""
    machinepassword = ""
    MAIL_RECEIVER_LIST = ""
    infraData = {}
    uebInfra={}
    machineInfra = {}

    def __init__(self, jsonFilePath):
        try:
            with open(jsonFilePath) as data_file:
                self.infraData=json.load(data_file)
                self.setValues()
        except:
            logHandler.logging.error(exceptionHandler.PrintException())
        
    def setValues(self):  
        try:  
            uebIndex=0
            for ueb in self.infraData["uebs"]:
                uebs=["Main","Source","Target"]
                
                for key in ("url","username","password","hostName","dbName", "dbUser", "dbPassword"):
                    if key in ueb.keys():                    
                        self.uebInfra[uebs[uebIndex]+key]=ueb[key]
                uebIndex+=1
            for machine in self.infraData["machines"]:
                    for key in ("type","name","ipAddress","username","password","includeList","excludeList",
                                "restoreFolder","esxName","instanceName","resourcePool","databaseName",
                                "serverName","port","target","lun","instanceIP", "flavour"):
                        if(key!="type" and machine["type"] and key in machine.keys()):
                            self.machineInfra[machine["type"]+key]=machine[key]
            logHandler.logging.info(str(self.uebInfra))
            logHandler.logging.info(str(self.machineInfra))
        except:
            logHandler.logging.error(exceptionHandler.PrintException())
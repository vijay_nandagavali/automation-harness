JOBWAITTIME = 7200
XMLVALIDITYCOUNT = 20
base_dir_path = "/root/UAS"
DATE_TIME_FORMAT = '%m/%d/%Y %H:%M:%S'
ERRORSTRINGCOUNT = 5
class ExecutionStatus(object):
    PASS = "Pass"
    FAIL = "Fail"
    SKIP = "Skip"
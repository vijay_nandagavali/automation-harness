'''
Created on Sep 18, 2014

@author: afour
'''
import json
from model.TestCaseResult import TestCaseResult


class ExecutionSummary(object):

    '''
    classdocs
    '''

    def __init__(self, _id, uebVersion, testCaseResultList=list(), startTime="", endTime="", duration="", totalTC=0, passedTC=0, failedTC=0, executionResult=""):
        '''
        Constructor
        '''
        self._id = _id
        self.buildNum = uebVersion
        self.testCaseResultList = testCaseResultList
        self.startTime = startTime
        self.endTime = endTime
        self.duration = duration
        self.totalTC = totalTC
        self.passedTC = passedTC
        self.failedTC = failedTC
        self.executionResult = executionResult

    def setExecutionResult(self, executionResult):
        self.executionResult = executionResult

    def getExecutionResult(self):
        return self.executionResult

    def setTotalTC(self, totalTC):
        self.totalTC = totalTC

    def getTotalTC(self):
        return self.totalTC

    def setPassedTC(self, passedTC):
        self.passedTC = passedTC

    def getPassedTC(self):
        return self.passedTC

    def setFailedTC(self, failedTC):
        self.failedTC = failedTC

    def getFailedTC(self):
        return self.failedTC

    def setStartTime(self, startTime):
        self.startTime = startTime
        
    def getStartTime(self):
        return self.startTime        

    def setEndTime(self, endTime):
        self.endTime = endTime
        
    def getEndTime(self):
        return self.endTime

    def getExecutionId(self):
        return self._id

    def getTestCaseResultList(self):
        return self.testCaseResultList
    
    def setDuration(self, duration):
        self.duration = duration
        
    def getDuration(self):
        return self.duration

    def toJson(self):
        return json.dumps(
            self, default=lambda o: o.__dict__, indent=4)

    @staticmethod
    def fromJson(jsonStr):
        return json.loads(jsonStr, object_hook=ExecutionSummary.__object_hook_handler)

    @classmethod
    def __object_hook_handler(cls, parsed_dict):
        """ parsed_dict argument will get 
            json object as a dictionary
        """

        if('_id' in parsed_dict):
            return cls(_id=parsed_dict['_id'],
                       uebVersion=parsed_dict['buildNum'],
                       testCaseResultList=parsed_dict['testCaseResultList'],
                       startTime=parsed_dict['startTime'],
                       endTime=parsed_dict['endTime'],
                       duration=parsed_dict['duration'],
                       totalTC=parsed_dict['totalTC'],
                       passedTC=parsed_dict['passedTC'],
                       failedTC=parsed_dict['failedTC'],
                       executionResult=parsed_dict['executionResult'])
        else:
            return TestCaseResult.object_hook_handler(parsed_dict)

'''
Created on Sep 17, 2014

@author: pravinpr
'''


class TestCaseStepResult(object):

    '''
    Class to represent a Test Case Step's Result
    '''

    def __init__(self, stepId, testStepDetail, executionStatus="", startTime="", endTime=""):
        '''
        Constructor
        '''
        self.stepId = stepId
        self.testStepDetail = testStepDetail
        self.executionStatus = executionStatus
        self.startTime = startTime
        self.endTime = endTime
        
    def setStartTime(self, startTime):
        self.startTime = startTime
        
    def setEndTime(self, endTime):
        self.endTime = endTime         

    def setExecutionStatus(self, executionStatus):
        self.executionStatus = executionStatus

    def getExecutionStatus(self):
        return self.executionStatus

    def getTestStepDetail(self):
        return self.testStepDetail

    @classmethod
    def object_hook_handler(cls, parsed_dict):
        return cls(testStepDetail=parsed_dict['testStepDetail'],
                   executionStatus=parsed_dict['executionStatus'],
                   stepId=parsed_dict['stepId'],
                   startTime=parsed_dict['startTime'],
                   endTime=parsed_dict['endTime'])

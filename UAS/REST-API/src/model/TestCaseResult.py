'''
Created on Sep 17, 2014

@author: pravinpr
'''
import json

from model.TestCaseStepResult import TestCaseStepResult
from util import dateTimeHandler
from config import Constants


class TestCaseResult(object):

    '''
    Class to represent a Test Case's Result
    '''

    def __init__(self, testCaseId, testDescription="", executionStatus="", stepResult=None, startTime="", endTime="", duration=""):
        '''
        Constructor
        '''
        stepResult = stepResult or []
        self.testCaseId = testCaseId
        self.executionStatus = executionStatus
        self.testCaseStepResult = stepResult
        self.startTime = startTime
        self.endTime = endTime
        self.duration = duration
        self.testDescription = testDescription
        
        
    def setTestDescription(self, testDescription):
        self.testDescription = testDescription        

    def setExecutionStatus(self, executionStatus):
        self.executionStatus = executionStatus

    def getExecutionStatus(self):
        return self.executionStatus
    
    def setStartTime(self, startTime):
        self.startTime = startTime
        
    def getStartTime(self):
        return self.startTime        
        
    def setEndTime(self, endTime):
        self.endTime = endTime
        
    def getEndTime(self):
        return self.endTime
        
    def setDuration(self, duration):
        self.duration = duration
        
    def getDuration(self):
        return self.duration        

    def getTestCaseId(self):
        return self.testCaseId

    def getTestCaseStepResult(self):
        return self.testCaseStepResult

    def addTestCaseStepResult(self, testStepDetail, executionStatus, startTime):
        stepId = str(len(self.getTestCaseStepResult()) + 1)
        stepResult = TestCaseStepResult(
            "TCS_" + stepId, testStepDetail, executionStatus)
        stepResult.setStartTime(startTime)
        stepResult.setEndTime("")
        self.getTestCaseStepResult().append(stepResult)
        del stepResult

    def toJson(self):
        return json.dumps(
            self, default=lambda o: o.__dict__, indent=4)

    @staticmethod
    def fromJson(jsonStr):
        return json.loads(jsonStr, object_hook=TestCaseResult.object_hook_handler)

    @classmethod
    def object_hook_handler(cls, parsed_dict):
        """ parsed_dict argument will get 
            json object as a dictionary
        """

        if('testCaseId' in parsed_dict):
            return cls(testCaseId=parsed_dict['testCaseId'],
                       executionStatus=parsed_dict['executionStatus'],
                       stepResult=parsed_dict['testCaseStepResult'],
                       startTime=parsed_dict['startTime'],
                       endTime=parsed_dict['endTime'],
                       duration=parsed_dict['duration'],
                       testDescription=parsed_dict['testDescription'])

        elif('testStepDetail' in parsed_dict):
            return TestCaseStepResult.object_hook_handler(parsed_dict)

import json
import jsonpath
from util import logHandler

class Environment(object):

    def __init__(self, envJsonPath):
        self.infraData = {}
        self.readFromJson(envJsonPath)
    
    def readFromJson(self, envJsonPath):
        try:
            with open(envJsonPath) as data_file:
                self.infraData = json.load(data_file)
        except Exception as e:
            print "There is some error in json file: " + str(e)  
            logHandler.logging.info("Error: " + str(e))     
            
    def splitString(self, string, splitItem):
        return string.split(splitItem)
            
    def setIndex(self, index):
        indexStr = self.splitString(index, "=")
        return int(indexStr[1]) - 1
        
    def getJsonDetails(self, infraDetail, infraDataValue=""):
#         splitItem = self.splitString(infraDetail, ";")
#         index = self.setIndex(splitItem[1])
#         
#         jsonPathQuery = self.splitString(splitItem[0], "||")
#         infraDeatil = self.infraData
#         for query in range(len(jsonPathQuery)):
#             infraValue = jsonpath.jsonpath(infraDeatil, jsonPathQuery[query])
#             infraDeatil = infraValue
#         return infraDeatil[index]

        index = None
        splitItem = self.splitString(infraDetail, ";")
        if (len(splitItem) > 1):
            index = self.setIndex(splitItem[1])
        
        jsonPathQuery = self.splitString(splitItem[0], "||")
        
        if (infraDataValue == ""):
            infraDataValue = self.infraData
            
        for query in range(len(jsonPathQuery)):
            infraValue = jsonpath.jsonpath(infraDataValue, jsonPathQuery[query])
            infraDataValue = infraValue
        
        if (index is not None):
            return infraDataValue[index]
        else:
            return infraDataValue
    
    def getDictValue(self, dictionary, key):
        if key in dictionary:
            return str(dictionary[key])
        else:
            return None
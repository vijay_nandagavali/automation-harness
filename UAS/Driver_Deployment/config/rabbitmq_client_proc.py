'''
Property file to configure automation again on client machine
'''
agentType="driver"
rabbitmqServer = "192.168.197.141"
rabbitmqExchangeName = "harnessexchange"
serverbindingkey = "harnesskey"
dataBaseServer = "192.168.197.141"
dataBaseServerPort = "27017"
dataBaseName = "harnessdb"
driverIPAddress = "192.168.197.18"
driverIPAddressList = ['192.168.197.15','192.168.197.24','192.168.197.25','192.168.197.8','192.168.197.21','192.168.197.47','192.168.197.49']
SCRIPT_DIR_PATH = "/root/UAS"
AGENT_BASE_DIR_PATH = "/root/UAF-Agent"
#!/bin/bash
remove_old_dir()
{
	echo "Removing older UAF and UAS Directorys"
	if [ -d "/opt/UAS" ] 
	then
		rm -rf /opt/UAS
		if [ $? -eq 0 ]
		then
			echo "Old /opt/UAS Directory removed" | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
		
		fi
	else
		echo "/opt/UAS Directory does not exist." | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	fi
sleep 2
echo "-----------------------------------------------------------" 
	if [ -d "/opt/afourtech" ]
	then
		rm -rf /opt/afourtech
	        if [ $? -eq 0 ]
        	then
                	echo "Old /opt/afourtech Directory removed" | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	        fi
	else
		echo "/opt/UAF Directory does not exist." | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	fi
sleep 2
echo "-----------------------------------------------------------" 
}

clone_repo()
{
	echo "Starting Cloning Latest repositry..."
	/root/Driver_Deployment/scripts/git_clone_expect.sh
	if [ $? -eq 0 ]
	then
		echo "Both the repositories cloned successfully."
	fi
sleep 2
echo "-----------------------------------------------------------" 
}

stop_agent()
{
	echo "Stopping Agent"
	RECEIVER_PID=`pgrep -lf receiver.py | awk '{print $1}'`
	if [ "$RECEIVER_PID" == "" ]
	then
		echo "UAF-Agent is not running." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt 
	else
		echo "UAF-Agent is running. Stopping the Agent"
		kill -9 "$RECEIVER_PID"
		if [ $? -eq 0 ]
		then
			echo "UAF-Agent is now stopped." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
		fi
	fi	
sleep 2
echo "-----------------------------------------------------------" 
}

key_backup()
{
	echo "Taking Backup of /UAF-Agent/Key/Key.txt to /opt"
	if [ -f "/root/UAF-Agent/Key/Key.txt" ]
	then 
		cp -rf /root/UAF-Agent/Key/Key.txt /opt/
		if [ $? -eq 0 ]
		then
			echo "Taking Backup of /UAF-Agent/Key/Key.txt to /opt is successfull" | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
		else
			echo "Unable to backup Key.txt file" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
		fi
	else
		echo "/root/UAF-Agent/Key/Key.txt File does not Exists" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	fi
sleep 2
echo "-----------------------------------------------------------" 
}

remove_UAS_UAF()
{
	if [ -d "/root/UAF-Agent" ]
	then
		echo "/root/UAF-Agent Directry Exists. Removing the directry"
		rm -rf /root/UAF-Agent
		if [ $? -eq 0 ]
		then
			echo "Old /root/UAF Directory Removed" | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
		fi
	else
		echo "UAF-Agent Directory does not exist at /root location. New Directory will be copied" | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	fi
sleep 2
echo "-----------------------------------------------------------" 

	if [ -d "/root/UAS" ]
	then
        	echo "Old /root/UAS Directry Exists. Removing the directry"
		rm -rf /root/UAS
		if [ $? -eq 0 ]
        	then
                	echo "Old /root/UAS Directory Removed" | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	        fi
	else
        	echo "UAS Directory does not exist at /root location. New Directory will be copied" | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	fi
sleep 2

echo "-----------------------------------------------------------"
}

copy_uas()
{
	echo "Copying new Agents and scripts"
	echo "Copying /opt/UAS to /root.Please wait"
	yes | cp -r /opt/UAS /root
	if [ -d "/root/UAS" ]
	then
		echo "/opt/UAS copied to /root Directory."  | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
		echo "Unable to Copy the UAS directry."  | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
		exit 1
	fi
sleep 2
echo "-----------------------------------------------------------"
}

copy_uaf()
{
	echo "Copying /opt/afourtech/UAF-Agent to /root.Please wait"
	yes | cp -r /opt/afourtech/UAF-Agent /root
	if [ -d "/root/UAF-Agent" ]
	then
        	echo "/opt/UAS copied to /root Directory." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
		echo "Creating Directory: Key under /root/UAF-Agent"
		mkdir /root/UAF-Agent/Key
		echo "Restoring Key.txt to /rot/UAF-Agent/Key/"
		cp /opt/Key.txt /root/UAF-Agent/Key/
		if [ $? -eq 0 ]
		then
			echo "/opt/Key.txt copied to /root/UAF-Agent/Key/ Directory" | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
		else
			echo "Unable to restore Key.txt" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
		fi
	else	
		sleep 2
	        echo "Unable to Copy the directry.Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2
echo "-----------------------------------------------------------"
}

copy_rabbitmq_config()
{
	yes | cp -r /root/Driver_Deployment/config/rabbitmq_client_proc.py /root/UAF-Agent/src/Config/ 
	if [ $? -eq 0 ]
	then
		echo "rabbitmq_client_proc.py Copied successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
		echo "Unable to Copy rabbitmq_client_proc.py" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
		exit 1
	fi
sleep 2
}

rest_api_config_edit()
{
	sed -i '/UAS_DIR_PATH/c UAS_DIR_PATH = \"\/root\/UAS\"' /root/UAS/REST-API/src/config/apiconfig.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/REST-API/src/config/apiconfig.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/REST-API/src/config/apiconfig.py" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	sed -i '/sys.path.append/c sys\.path\.append\(\"\/root\/UAS\/REST-API\/src\"\)' /root/UAS/REST-API/src/driver/RestApiDriver.py 
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/REST-API/src/driver/RestApiDriver.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/REST-API/src/driver/RestApiDriver.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2
}

dedup_config_edit()
{
	RABBITMQ_ADDR=`cat /root/Driver_Deployment/config/rabbitmq_client_proc.py | grep rabbitmqServer | awk -F '=' '{print $2}' | tr -d ' '`
	sed -i '/rabbitmqServer./c rabbitmqServer\='"${RABBITMQ_ADDR}"'' /root/UAS/Dedup/src/config/apiconfig.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/Dedup/src/config/apiconfig.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/Dedup/src/config/apiconfig.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	sed -i '/UAS_DIR_PATH/c UAS_DIR_PATH = \"\/root\/UAS\"' /root/UAS/Dedup/src/config/apiconfig.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/Dedup/src/config/apiconfig.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/Dedup/src/config/apiconfig.py" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	sed -i '/config_path /c config_path = \"\/root\/UAS\/Config\/"' /root/UAS/Dedup/src/config/Constants.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/Dedup/src/config/Constants.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/Dedup/src/config/Constants.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	sed -i '/sys.path.append/c sys\.path\.append\(\"\/root\/UAS\/Dedup\/src\"\)' /root/UAS/Dedup/src/driver/driver.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/Dedup/src/driver/driver.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/Dedup/src/driver/driver.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2
}

php_api_config_edit()
{
	sed -i '/UAS_DIR_PATH \=/c UAS_DIR_PATH \= \"\/root\/UAS\"' /root/UAS/PHP-API/src/configuration/Constants.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/PHP-API/src/configuration/Constants.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/PHP-API/src/configuration/Constants.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	RABBITMQ_ADDR=`cat /root/Driver_Deployment/config/rabbitmq_client_proc.py | grep rabbitmqServer | awk -F '=' '{print $2}' | tr -d ' '`
	sed -i '/RABBITMQSERVER\=/c RABBITMQSERVER='"${RABBITMQ_ADDR}"'' /root/UAS/PHP-API/src/configuration/Constants.py
	if [ $? -eq 0 ]
	then
        	echo "Rabbitmq configuration in /root/UAS/PHP-API/src/configuration/Constants.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unabele to edit Rabbitmq configuration in /root/UAS/PHP-API/src/configuration/Constants.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	sed -i '/^sys\.path\.append/c sys\.path\.append\(\"\/root\/UAS\/PHP-API\/src\"\)' /root/UAS/PHP-API/src/driver/Driver.py
	if [ $? -eq 0 ]
	then
        	echo " /root/UAS/PHP-API/src/driver/Driver.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/PHP-API/src/driver/Driver.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2
}

regression_config_edit()
{

	RABBITMQ_ADDR=`cat /root/Driver_Deployment/config/rabbitmq_client_proc.py | grep rabbitmqServer | awk -F '=' '{print $2}' | tr -d ' '`
		sed -i '/rabbitmqServer./c rabbitmqServer\='"${RABBITMQ_ADDR}"'' /root/UAS/Regression/src/config/apiconfig.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/Regression/src/config/apiconfig.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/Regression/src/config/apiconfig.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	DRIVER_IP=`cat /etc/hosts | grep uaf-drv-linux | awk -F ' ' '{print $1}'`
		sed -i '/driverIP./c driverIP\="'${DRIVER_IP}'"' /root/UAS/Regression/src/config/apiconfig.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/Regression/src/config/apiconfig.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/Regression/src/config/apiconfig.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	sed -i '/config_path /c config_path = \"\/root\/UAS\/Config\/"' /root/UAS/Regression/src/config/Constants.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/Regression/src/config/Constants.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/Regression/src/config/Constants.py"| tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	sed -i '/UAS_DIR_PATH/c UAS_DIR_PATH = \"\/root\/UAS\"' /root/UAS/Regression/src/config/apiconfig.py
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/Regression/src/config/apiconfig.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/Regression/src/config/apiconfig.py" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2

	sed -i '/sys.path.append/c sys\.path\.append\(\"\/root\/UAS\/Regression\/src\"\)' /root/UAS/Regression/src/driver/driver.py 
	if [ $? -eq 0 ]
	then
        	echo "/root/UAS/Regression/src/driver/driver.py edited successfully." | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	else
        	echo "Unable to edit /root/UAS/Regression/src/driver/driver.py. Exiting Now" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	        exit 1
	fi
sleep 2
}

start_receiver()
{
	nohup python /root/UAF-Agent/src/receiver.py > /root/UAF-Agent/Logs/receiver.log 2>&1 &
	RECEIVER_PID=`pgrep -lf receiver.py | awk '{print $1}'`
	if [ "$RECEIVER_PID" == "" ]
	then
        	echo "Unable to start UAF-Agent is not running." | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	else
	        echo "UAF-Agent is running. RECEIVER_PID:$RECEIVER_PID" | tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	fi
}

remove_old_dir
if [ $? -eq 0 ]
        then
                echo "Remove_Earlier_Dir `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Remove_Earlier_Dir `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

clone_repo
if [ $? -eq 0 ]
        then
                echo "Clone_Repository `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Clone_Repository `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

stop_agent
if [ $? -eq 0 ]
        then
                echo "Stop_Existing_Agent `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Stop_Existing_Agent `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

key_backup
if [ $? -eq 0 ]
        then
                echo "Key_File_Backup `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Key_FileBackup `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

remove_UAS_UAF
if [ $? -eq 0 ]
        then
                echo "Remove_UAS_UAF `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Remove_UAS_UAF `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

copy_uas
if [ $? -eq 0 ]
        then
                echo "Copy_UAS_Dir `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Copy_UAS_Dir `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

copy_uaf
if [ $? -eq 0 ]
        then
                echo "Copy_UAF `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Copy_UAF `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

copy_rabbitmq_config
if [ $? -eq 0 ]
        then
                echo "Copy_RabbitMQ_Config_File_Edit `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Copy_RabbitMQ_Config_File_Edit `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

rest_api_config_edit
if [ $? -eq 0 ]
        then
                echo "REST-API_Config_File_Edit `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "REST-API_Config_File_Edit `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

dedup_config_edit
if [ $? -eq 0 ]
        then
                echo "Dedup_Config_File_Edit `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Dedup_Config_File_Edit `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

php_api_config_edit
if [ $? -eq 0 ]
        then
                echo "PHP-API_Config_File_Edit `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "REST-API_Config_File_Edit `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

regression_config_edit
if [ $? -eq 0 ]
        then
                echo "Regression_Config_File_Edit `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Regression_Config_File_Edit `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

start_receiver
if [ $? -eq 0 ]
        then
                echo "Start_Agent `hostname` Pass" | tee -a /root/Driver_Deployment/Logs/deployment_status.log
        else
                echo "Start_Agent `hostname` Fail" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.log
fi

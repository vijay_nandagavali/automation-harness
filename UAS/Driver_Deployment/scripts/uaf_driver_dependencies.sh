#!/bin/bash
fun_check_user()
{
	if [ "`whoami`" != "root" ]
	then
		echo "Please login as a root and try again"
		exit 1
	else
		echo "Logged in as `whoami`"
	fi
}
fun_check_hostname()
{
	hostnm=`hostname`
	if [ "$hostnm" == "^localhost*" ]
	then
		echo "Set the hostname and run this script again"
		exit 1
	fi
}
fun_devl_tools () 
{
	echo "Installing Development tools"
	yum groupinstall "Development tools" -y
	echo "----------------------------------------------------------"	
	sleep 5
	
	echo "Installing zlib-devel bzip2-devel openssl-devel ncurses-devel unixODBC-devel"
	yum install zlib-devel bzip2-devel openssl-devel ncurses-devel unixODBC-devel wget postgresql-devel vim expect* -y
	echo "----------------------------------------------------------"	
	sleep 5
	
	echo "Installing psycopg2 dependencies"
	cd /opt;curl -O http://yum.postgresql.org/9.3/redhat/rhel-6.4-x86_64/pgdg-centos93-9.3-1.noarch.rpm
	rpm -ivh pgdg-centos93-9.3-1.noarch.rpm
	yum install postgresql93 postgresql93-contrib postgresql93-devel postgresql93-libs postgresql93-server -y
	yum install python-devel -y
	yum install libffi-devel -y
	echo "----------------------------------------------------------"	
}

fun_python()
{
	echo "Verifying that Python 2.7.7 is already installed."
        python -V > /tmp/py_ver.log 2>&1
        py_ver=`cat /tmp/py_ver.log | awk '{print $2}' | tr -d '\r'`
        echo "Currently installed Python version:$py_ver"
        if [ "$py_ver" == "2.7.7" ]
	then
		echo "Python is already installed. Skipping installation."
		echo "----------------------------------------------------------"	
	else
		echo "Installing Python"
		cd /opt
		wget --no-check-certificate https://www.python.org/ftp/python/2.7.7/Python-2.7.7.tar.xz
		tar xf Python-2.7.7.tar.xz
		cd Python-2.7.7
		./configure --prefix=/usr/local
		make && make altinstall
		
		echo "Making Entries to .bashrc"
		echo "export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin" >> /root/.bashrc
		sleep 3
		
		echo "unlinking python 2.7.7"
		unlink /usr/bin/python2
		unlink /usr/bin/python

		sleep 3
		echo "Linking Python"
		ln -s /usr/local/bin/python2.7 /usr/local/bin/python
		ln -s /usr/bin/python2.6 /usr/bin/python
		source /root/.bashrc
		sleep 3

		echo "Verifying python version"
		python_version=`python -V`
		if [ "$python_version" == "2.7.7" ]
		then
			echo "Python 2.7.7 is installed and default python is `python -V`"
		fi
		sleep 3
	fi
}
fun_ez_setup() 
{
	echo "Downloading ez_setup"
	cd /opt;wget https://bootstrap.pypa.io/ez_setup.py
	/usr/local/bin/python2.7 ez_setup.py
	/usr/local/bin/easy_install-2.7 pip
	/usr/local/bin/easy_install-2.7 pyodbc
	/usr/local/bin/easy_install-2.7 xmltodict
	/usr/local/bin/easy_install-2.7 pyjavaproperties
	/usr/local/bin/easy_install-2.7 pymongo
	pip install pika
	pip install mongo
	pip install configparser
	pip install requests
	easy_install -U pysphere
	pip install jira
	pip install psycopg2
	pip install automodinit
    /usr/local/bin/easy_install-2.7 paramiko
	sleep 3
}

fun_cheetah() 
{	
	echo "Installing Cheetah"
	cd /opt;wget --no-check-certificate https://pypi.python.org/packages/source/C/Cheetah/Cheetah-2.4.4.tar.gz
	tar -xvzf Cheetah-2.4.4.tar.gz
	cd Cheetah-2.4.4
	python setup.py install
	sleep 3
}

fun_jsonpath()
{
	echo "Installing jsonpath"
	echo "Downloading jsonpath-0.54.tar.gz"
	cd /opt;wget http://www.ultimate.com/phil/python/download/jsonpath-0.54.tar.gz	
	tar -xvf jsonpath-0.54.tar.gz
	cd jsonpath-0.54 
	python setup.py install
}

fun_main()
{
	echo "Checking internet connectivity..."
	ping -c3 google.com > /dev/null
	if [ $? -eq 0 ]
	then
		fun_check_hostname
		fun_devl_tools
		if [ $? -eq 0 ];then
			echo "Development tools installed successfully."
			echo "-----------------------------------------"
		else
                       echo "Error while installing Development tools"
			echo "-----------------------------------------"
			exit 1
		fi

		fun_python
		if [ $? -eq 0 ];then
                        echo "Python installed successfully."
			echo "-----------------------------------------"
                else
                        echo "Error while installing Python"
			echo "-----------------------------------------"
			exit 1
                fi

		fun_ez_setup
		if [ $? -eq 0 ];then
                        echo "ez_setup installed successfully."
			echo "-----------------------------------------"
                else
                        echo "Error while installing ez_setup."
			echo "-----------------------------------------"
                        exit 1
                fi

		fun_cheetah
		if [ $? -eq 0 ];then
                        echo "Cheetah installed successfully."
			echo "-----------------------------------------"
                else
                        echo "Error while installing cheetah."
			echo "-----------------------------------------"
                        exit 1
                fi

		fun_jsonpath
		if [ $? -eq 0 ];then
                        echo "jsonpath installed successfully."
			echo "-----------------------------------------"
                else
                        echo "Error while installing jsonpath."
			echo "-----------------------------------------"
                        exit 1
                fi

else
	echo "Check Internet connection. Exited from Ping Function"
	exit 1
fi
}

echo "Calling Main function.."
fun_main

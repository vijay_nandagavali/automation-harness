cd /opt
unset SSH_ASKPASS
git clone https://afour_manish@bitbucket.org/Unitrends/automation.git
if [ -n "$(ls -A /opt/automation)" ];then
	echo "Cloned UAS repository successfully"| tee -a /root/Driver_Deployment/Logs/deployment_status.txt
	cd /opt
	mv automation UAS
else
	echo "Unable to clone git UAS repositry" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	exit 1
fi

cd /opt
unset SSH_ASKPASS
git clone https://afour_manish@bitbucket.org/Unitrends/afourtech.git
if [ -n "$(ls -A /opt/afourtech)" ];then
	echo "Cloned UAF repository successfully"| tee -a /root/Driver_Deployment/Logs/deployment_status.txt
else
	echo "Unable to clone git UAF repositry" | tee -a /root/Driver_Deployment/Logs/deployment_status_failure.txt
	exit 1
fi

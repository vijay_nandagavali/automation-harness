from Cheetah.Template import Template
from ScriptResult import ScriptResult
import os
import socket
import Constants
import Report_Mail

def generateReport(templateFilePath, execSumm, outputFilePath):
    result = Template(
        file=templateFilePath, searchList=[{'scriptResultList': execSumm}])
    # print result
    open(outputFilePath, 'w').write(str(result))

file_size = os.path.getsize(Constants.LOGFILE_PATH_FAILURE)
if file_size > 0:
	with open(Constants.LOGFILE_PATH_FAILURE, 'r') as f:
       		data = f.readlines()
else:
	with open(Constants.LOGFILE_PATH, 'r') as f:
               	data = f.readlines()

    
scriptResultList = []

for line in data:
    tokens = line.split()
#    print tokens
    scriptResult = ScriptResult(tokens[0], tokens[1], tokens[2])
    scriptResultList.append(scriptResult)

generateReport(Constants.TEMPLATE_FILE_PATH, scriptResultList, Constants.REPORT_FILE_PATH)
Report_Mail.send_mail()


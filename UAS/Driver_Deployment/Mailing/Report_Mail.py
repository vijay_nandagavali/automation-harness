from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
import shutil
import os
import smtplib
import time

import Constants


def send_mail():
    assert type(Constants.MAIL_RECEIVER_LIST)==list
    
#     createZip(Constants.AUTOMATION_REPORT_FOLDER)
#     zipfilePath = Constants.AUTOMATION_REPORT_FOLDER + "\Report.zip"
    msg = MIMEMultipart()
    msg['From'] = Constants.FROM
    file_size = os.path.getsize(Constants.LOGFILE_PATH_FAILURE)
    if file_size > 0:
    	msg['To'] = COMMASPACE.join(Constants.MAIL_RECEIVER_LIST_FAILURE)
    else:
	msg['To'] = COMMASPACE.join(Constants.MAIL_RECEIVER_LIST)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = Constants.SUBJECT
    
    emailBody = open(Constants.REPORT_FILE_PATH,'r').read()
    
    '''msg.attach( MIMEText("Hi All,\n\nPlease find the attached automation report\n\n" + emailBody +
    "Thanks and Regards,\nAutomation Team") )'''
    msg.attach(MIMEText(emailBody,'html'))
    
#     part = MIMEBase('application', "octet-stream")
#     part.set_payload( open(zipfilePath,"rb").read() )
#     Encoders.encode_base64(part)
#     part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(zipfilePath))
#     msg.attach(part)
    
#    server = "smtp.gmail.com:587"
    server = Constants.SMTP_SERVER_DETAILS
    smtp = smtplib.SMTP(server)
    smtp.starttls()
    smtp.login(Constants.FROM, Constants.PASSWORD)
    smtp.sendmail(Constants.FROM, Constants.MAIL_RECEIVER_LIST, msg.as_string())
    smtp.close()
    print "Mail Sent..."

def createZip(filePath):
    zippath = filePath + "\Report"
    shutil.make_archive(filePath + "\Report", "zip", zippath)
    print "Zip created"

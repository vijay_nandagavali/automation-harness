* Automation agent execution flow *           
          |
          | ----- (1) Run => message_receiver.py (..\AutomationAgent\src\Message\receiver)
                 |
                 | ----- (2.1) Initialize receiver => initialize_receiver.py (..\AutomationAgent\src\Message\receiver) // ** This functionally call only first time to initialize receiver, 
                 																									   It will generate Key.txt file contains UUID for the receiver for his
                 																									   unique identification on Harness ** //   
                 | ----- (2.2) Receiver start receiving message
                   	   |
                 	   | ----- (3) Message received by receiver (In JSON format) It will call => message_parser.py (..\AutomationAgent\src\Handler) // ** This will parse the JSON format and call appropriate
                 	                                                                                                                                           action specified in it ** //
							  |
							  | ----- (4.1) Action = ExecuteScript call router.py => Router will filter the action and take appropriate action according to specified action e.g execute script will call script_executer.py
							  		|
							  		| ----- (5.1) Here it calls script_executor.py to execute text script on a machine  							                 	                                                                                                                                             
                              |
                              | ----- (4.2) Action = ExecutionStatus call router.py = Router will filter the action and take appropriate action according to specified action e.g execution stats will call execution_checker.py
                                    |
                                    | ----- (5.2) Here it calls execution_checker.py to check execution status                                 
#!/usr/bin/env python
"""
Check to see if an process is running. If not, restart.
Run this in a cron job
"""
import os
import subprocess
process_name= "/root/UAF-Agent/src/receiver.py" # change this to the name of your process
import sys
import subprocess

tmp = os.popen("ps -Af").read()
log_output_err = open("/root/UAF-Agent/Logs/receiver1.log",'a+') 
if process_name not in tmp[:]:
    print "The process is not running. Let's restart."
    """"Use nohup to make sure it runs like a daemon"""
    sp=subprocess.Popen(["nohup", "/usr/local/bin/python", process_name],
               stdout = log_output_err,
               stderr = log_output_err)
    sp.wait
else:
    print "The process is running."
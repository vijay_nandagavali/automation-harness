'''
Created on Aug 1, 2014

@author: AFour Technologies
'''

import json
from Config import Logs, rabbitmq_client_proc
import os

def createNewFile(fileNameWithPath, content):
    fo = open(fileNameWithPath, "w+")
    fo.write(content)
    fo.close()
    
#Function to convert JSON Report file to string
def getJSONStringFromFile(executionId):
    jsonReportFile = "E-" + str(executionId) + ".json"
    try:
        with open(rabbitmq_client_proc.SCRIPT_DIR_PATH + "/Reports/" + jsonReportFile) as json_file:
            json_data = json.load(json_file)
            return json_data
    except Exception as e:
        print "There is some error in json file"
        Logs.logging.error(e)
        return None

def updatePropertyfile(key, value): 
    from pyjavaproperties import Properties
    p = Properties()
    p.load(open(rabbitmq_client_proc.SCRIPT_DIR_PATH + "/Satori-UI/AutomationConfig.properties"))
    p[str(key)] = str(value)
    p.store(open(rabbitmq_client_proc.SCRIPT_DIR_PATH + "/Satori-UI/AutomationConfig.properties",'w'))
    
def createNewFolder(path, folderName):
    try:
        directory = str(path) + "/E-" + folderName
        print directory
        if not os.path.exists(directory):
            os.makedirs(directory) 
            print "folder created"
            print directory      
        return True
    except Exception as e:
        Logs.logging.error(str(e))
        print str(e)
        

'''
Created on Jan 6, 2015

@author: Sayali.k
'''
import re
import xmlparser
import subprocess, sys
cmd = ["powershell.exe", "get-vm Win7_Auto| ft VMName, VMId"]
p = subprocess.Popen(cmd,
		stdin=subprocess.PIPE, 
                stdout=subprocess.PIPE, 
                stderr=subprocess.PIPE)
out, err = p.communicate()
returncode = p.wait()
words = out.split(" ")
for word in words:
	if re.search("\d[6]", word):
		vmId=word
path = xmlparser.getAllXmlElements(vmId.upper())
print path
'''
Created on Jul 17, 2014

@author: Abhijeet.m
'''
import logging
from Config import rabbitmq_client_proc

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(filename)s:%(lineno)d %(levelname)s %(message)s',
                    filename=rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/Logs/AgentLogs.log",
                    filemode='a',
                    maxBytes=20,
                    backupCount=500)
'''
Created on Jul 10, 2014

@author: AFourtechnologies
'''
import pika
import logging

from Config import rabbitmq_client_proc, Logs

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.CRITICAL)
'''
This method is use to send agent information to Harness
'''
def sendInfoToHarness(agentInfo):    
    try :
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitmq_client_proc.rabbitmqServer))
        channel = connection.channel()
        Logs.logging.info("Connection created to rabbit MQ server " + rabbitmq_client_proc.rabbitmqServer)

        channel.exchange_declare(exchange=rabbitmq_client_proc.rabbitmqExchangeName, type='topic')
        Logs.logging.info("Channel created with exchange name " + rabbitmq_client_proc.rabbitmqExchangeName)

        #routing_key = sys.argv[1] if len(sys.argv) > 1 else 'anonymous.info'
        routing_key = rabbitmq_client_proc.serverbindingkey
        Logs.logging.info("Sender send information with binding key " + rabbitmq_client_proc.serverbindingkey)
        #message = ' '.join(sys.argv[2:]) or 'Hello World!'    
        channel.basic_publish(exchange=rabbitmq_client_proc.rabbitmqExchangeName, routing_key=routing_key, body=agentInfo)
        print " [x] Sent agent information %r:%r" % (routing_key, agentInfo)
        Logs.logging.info("Sent agent information " + str(agentInfo))
        
        connection.close()
        Logs.logging.info("Sender's rabbit MQ connection closed")
        
    except Exception as e:
        Logs.logging.info(e)
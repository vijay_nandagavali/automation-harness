'''
Created on Jul 3, 2014
This script receive messages sent from the sender and send it to router to parse the sent message
@author: AFourtechnologies
'''

import sys
import os 
# sys.path include for the execution of python message_receiver script from command line
sys.path.append(os.path.abspath(os.getcwd() + "/../" + "/../"))
import pika
from Handler import message_parser
from Config import rabbitmq_client_proc
from Config import Logs
from Message.receiver import initialize_receiver

'''
Initialize automation agent, First to check Key.txt file is created or not for the verification that receiver
is initializing first time and assign a UUID to receiver for unique identification on Harness
'''
def start():
    try:    
         
        initialize_receiver.initializeReceiver()
        # Here we connect to the broker of local machine
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitmq_client_proc.rabbitmqServer))    
        channel = connection.channel()
        print "rabbitmqServer - - "+rabbitmq_client_proc.rabbitmqServer
        Logs.logging.info("Connected to rabbit MQ server " + rabbitmq_client_proc.rabbitmqServer)
     
        # Most of the API things getting resides on this channel
        channel.exchange_declare(exchange=rabbitmq_client_proc.rabbitmqExchangeName, type="topic")
        #result = channel.queue_declare(queue='rpc_queue')
        result = channel.queue_declare(exclusive=True)
        Logs.logging.info("Channel created with exchange name " + rabbitmq_client_proc.rabbitmqExchangeName)
       
        queue_name = result.method.queue
       
        # Here we define patter for filtering the messages sent from RabbitMQ server 
        # All means all the receiver receive the message and the UUID means only the specific receiver receive the message
        #driversList = ['192.168.197.15','192.168.197.24','192.168.197.25','192.168.197.8','192.168.197.21']
        binding_keys = ['ALL']
        for i in range(0,len(rabbitmq_client_proc.driverIPAddressList)):
            driverIPAddress = rabbitmq_client_proc.driverIPAddressList[i].replace(".","")
            binding_keys.append(driverIPAddress + initialize_receiver.getHostIPAddress())
        print "Receiver binding key = " + str(binding_keys)
        Logs.logging.info("Receiver start receiving message with binding key " + str(binding_keys))
               
#         driverIPAddress = rabbitmq_client_proc.driverIPAddress.replace(".","")
#         binding_keys = ['ALL', driverIPAddress + initialize_receiver.getHostIPAddress()]
#         print "Receiver binding key = " + str(binding_keys)
#         Logs.logging.info("Receiver start receiving message with binding key " + str(binding_keys))
     
        if not binding_keys:
            print >> sys.stderr, "Usage: %s [binding_key]..." % (initialize_receiver.getReceiverUUID(),)
            Logs.logging.error("No binding key as " + initialize_receiver.getReceiverUUID())
            sys.exit(1)
                                                                    
        for binding_key in binding_keys:
            channel.queue_bind(exchange=rabbitmq_client_proc.rabbitmqExchangeName, queue=queue_name, routing_key=binding_key)        
     
        def callback(ch, method, properties, body):
            print " [x] %r:%r" % (method.routing_key, body,)
            Logs.logging.info("Routing key and body " + method.routing_key + ", " + body)
            response = message_parser.jsonParser(body)
            ch.basic_publish(exchange='',
                     routing_key=properties.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                     properties.correlation_id),
                     body=str(response))
            ch.basic_ack(delivery_tag = method.delivery_tag)
            print " [x] Awaiting RPC requests. To exit press CTRL+C"
             
        #channel.basic_qos(prefetch_count=1)
        channel.basic_consume(callback, queue=queue_name)
        print " [x] Awaiting RPC requests. To exit press CTRL+C"
        channel.start_consuming()
    except Exception as e:    
        Logs.logging.error(e)

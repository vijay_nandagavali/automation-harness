'''
Created on Jul 9, 2014

@author: AFourtechnologies
'''
import uuid, os
import socket
import sys
import string
from Message.sender import agent_info_sender
from Config import rabbitmq_client_proc, Logs
from Util import filereader_writer

keyFileName = rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/Key/Key.txt"

'''
This Method to get UUID assigned to automation receiver agent 
'''
def getReceiverUUID():
    try:
        fo = open(keyFileName, "r")
        data = fo.readline()
        fo.close()        
        return data.split('=')[1]
    except Exception as e:
        Logs.logging.info(e)

def getHostIPAddress():    
    return string.replace(str(socket.gethostbyname(socket.gethostname())),".","")

'''
This method to get Agent information in json format such as machine hostname, ipaddress, OS and UUID
'''
def getAgentInformation():
    agenInfo = "{\"action\":\"initialize\", \"machineInfo\":{\"hostName\":\"" + socket.gethostname() + "\", \"ipAddress\":\"" + socket.gethostbyname(socket.gethostname()) + "\", \"operatingSystem\":\"" + sys.platform + "\", \"machineId\":\""+ getReceiverUUID() + "\", \"agentType\":\""+ rabbitmq_client_proc.agentType + "\", \"inUse\":false}}}"
    return agenInfo

'''
This is the private method to check Key.txt file exist or not, If exist return true else false
'''
def __isReceiverInitialized():
    try :
        if not os.path.exists(keyFileName):
            return False    
        fo = open(keyFileName, "r")
        if len(fo.readline()) == 0:
            os.remove(keyFileName)
            return False
        else: 
            return True
    except Exception as e:
        Logs.logging.info(e)
        
'''
This is the private method is to create Key.txt file and add UUID value in it
'''
def __createReceiverKeyFile():        
    try :
        #fo = open(keyFileName, "w+")
        key = "Receiver UUID=" + str(uuid.uuid4())
        #fo.write(key)
        #fo.close()
        filereader_writer.createNewFile(keyFileName, key)
        Logs.logging.info("Key.txt file created with the UUID " + str(key))
    except Exception as e:
        Logs.logging.info(e)        

'''
This method is to initialize receiver 
'''      
def initializeReceiver():
    try :
        # First check receiver initialized or not, If not create Key.txt and initialized
        if not __isReceiverInitialized():
            __createReceiverKeyFile()        
            agent_info_sender.sendInfoToHarness(getAgentInformation())
            print "Receiver initialized"
            Logs.logging.info("Receiver initialized with the UUID")
    except Exception as e:
        Logs.logging.info(e)    
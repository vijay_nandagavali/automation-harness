'''
Created on Dec 16, 2014

@author: Amey.k
'''
from Handler.script_executor import checkSriptIsPresentOrNot
import subprocess
from Config import rabbitmq_client_proc, Logs
import os
from Util import xmlparser


takeSnapshotScriptPath = rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/CBTAutomationFiles/takesnapshot.ps1"
removeSnapshotScriptPath = rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/CBTAutomationFiles/removesnapshot.ps1"


def execute_batch_script(script_path,script_name,arguments,argumentsPath):
    if checkSriptIsPresentOrNot(script_path + "/" +script_name):
        if (argumentsPath != ""):
            argumentsPath = argumentsPath.replace("/", "\ ".strip())
            arguments = arguments + " " + '"' + argumentsPath +'"'
            
        
        
        sp = subprocess.Popen(script_path + "/" +script_name + " " + arguments,cwd=script_path,stdout=subprocess.PIPE,shell=True)
        sp.wait()
        out = sp.communicate()
        print str(out[0])
        
        Logs.logging.info(str(out[0]))
        #waitToCompleteTestScript(sp.pid)
        if 'SUCCESS' in str(out[0]):
            return "true"
        else:
            return "false"    
    else:
        print script_name + " does not exist at location " + script_path
        return "false"
    
def execute_powershell_script(vmName):
    if checkSriptIsPresentOrNot(takeSnapshotScriptPath):
        sp = subprocess.Popen([r'C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe',
                             '-ExecutionPolicy',
                             'Unrestricted',
                             takeSnapshotScriptPath, vmName], cwd=os.getcwd(),stdout=subprocess.PIPE,shell=True)
        sp.wait()
        out = sp.communicate()
        print str(out[0])
        Logs.logging.info(str(out[0]))
        #waitToCompleteTestScript(sp.pid)
        if 'Snapshot done' in str(out[0]):
            return "true"
        else:
            return "false"
        
            
    else:
        print takeSnapshotScriptPath + " does not exist at location "
        return "false"

def execute_powershell_script_full_response(command):
    Logs.logging.info("111111111111111")
    cmd = ["C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe", command]
    Logs.logging.info("111111111111111")
    sp = subprocess.Popen(cmd,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    Logs.logging.info("111111111111111")
    sp.wait()
    out, err = sp.communicate()
    returncode = sp.wait()
    print str(out)
    Logs.logging.info("powershell script full response : returncode "+str(returncode))
    Logs.logging.info("powershell script full response : OUT "+str(out))
    Logs.logging.info("powershell script full response : err "+str(err))
    return out



def get_Path_for_VM(vmID):
    path = xmlparser.getAllXmlElements(vmID.upper())
    Logs.logging.info("Path: " + str(path))
    return path
   
def readIOGeneratorInputFile(script_path,script_name,arguments):
    iogeneratorInputFilePath = rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/CBTAutomationFiles/input.txt"
    if checkSriptIsPresentOrNot(script_path + "/" +script_name):
        if (script_path != ""):
            script_path = script_path + "/" + script_name
            script_path = script_path.replace("/", "\ ".strip())
        
        iogeneratorInputFilePath = iogeneratorInputFilePath.replace("/", "\ ".strip())
        
        sp = subprocess.Popen(script_path + " " + arguments + " " + iogeneratorInputFilePath, cwd=os.getcwd(),stdout=subprocess.PIPE,shell=True)
        sp.wait()
        out = sp.communicate()
        print str(out[0])
        Logs.logging.info(str(out[0]))
        #waitToCompleteTestScript(sp.pid)
        if 'Data Write Complete' in str(out[0]):
            return "true"
        else:
            return "false"
        
        
            
    else:
        print script_path + "/" +script_name + " does not exist at location "
        return "false"
        
def getDataWriteInformation(script_path,script_name,arguments,argumentsPath,firstSnapshotId,secondSnapshotId):
    if checkSriptIsPresentOrNot(script_path + "/" +script_name):
        if (argumentsPath != ""):
            argumentsPath = argumentsPath.replace("/", "\ ".strip())
            arguments = arguments + " " + '"' + argumentsPath +'"'
            
        
        
        sp = subprocess.Popen(script_path + "/" +script_name + " " + arguments + " " +firstSnapshotId \
                              + " " +secondSnapshotId ,cwd=script_path,stdout=subprocess.PIPE,shell=True)
        sp.wait()
        out = sp.communicate()
        print str(out[0])
        Logs.logging.info(str(out[0]))
        #waitToCompleteTestScript(sp.pid)
        if 'SUCCESS' in str(out[0]):
            return "true"
        else:
            return "false"    
    else:
        print script_name + " does not exist at location " + script_path
        return "false"
    
def getCbtChangeResult(script_path,script_name,olDataFilePath):
    
    iogeneratorInputFilePath = rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/CBTAutomationFiles/input.txt"
    iogeneratorInputFilePath = iogeneratorInputFilePath.replace("/", "\ ".strip())
    oldataFileName = os.listdir(olDataFilePath)
    print oldataFileName[0]
    olDataFilePath = olDataFilePath + "/" + oldataFileName[0]
    olDataFilePath = olDataFilePath.replace("/", "\ ".strip())
    print olDataFilePath
    if checkSriptIsPresentOrNot(script_path + "/" +script_name):
        sp = subprocess.Popen(script_path + "/" +script_name + " " + iogeneratorInputFilePath + " " +olDataFilePath \
                              ,cwd=script_path,stdout=subprocess.PIPE,shell=True)
        sp.wait()
        out = sp.communicate()
        print str(out[0])
        Logs.logging.info(str(out[0]))
        #waitToCompleteTestScript(sp.pid)
        if 'Change Reflected' in str(out[0]):
#             #newpath = filePath + "/" + "ArchivedOLDataFiles"
#             os.chmod(olDataFilePath,0o777)
#             newpath = "C:/ArchivedOLDataFiles" 
#             if not os.path.exists(newpath): os.makedirs(newpath)
#             os.chmod(newpath,0o777)
#             copyfile(olDataFilePath, newpath)
            return "true"
        else:
            return "false"    
    else:
        print script_name + " does not exist at location " + script_path
        return "false"
    
def performCleanupActivity(script_path,script_name,arguments,vmName):
    if checkSriptIsPresentOrNot(script_path + "/" +script_name):
        sp = subprocess.Popen(script_path + "/" +script_name + " " + arguments,cwd=script_path,stdout=subprocess.PIPE,shell=True)
        sp.wait()
        out = sp.communicate()
        print str(out[0])
        Logs.logging.info(str(out[0]))
        #waitToCompleteTestScript(sp.pid)
        if 'SUCCESS' in str(out[0]):
            print "VM removed from tracking"
        else:
            return "VM not removed from tracking"
        if checkSriptIsPresentOrNot(removeSnapshotScriptPath):
            sp = subprocess.Popen([r'C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe',
                             '-ExecutionPolicy',
                             'Unrestricted',
                             removeSnapshotScriptPath, vmName], cwd=os.getcwd(),stdout=subprocess.PIPE,shell=True)
            sp.wait()
            out = sp.communicate()
            print str(out[0])
            Logs.logging.info(str(out[0]))
            #waitToCompleteTestScript(sp.pid)
            if 'Snapshot removed' in str(out[0]):
                return "true"
            else:
                return "false"
        
    else:
        print script_name + " does not exist at location " + script_path
        return "false"   
        
    

 
        
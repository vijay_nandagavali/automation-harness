'''To execute the script on client machine'''
import os
from random import randint  
def add_files(dir_path, number_of_files, folder_type):
    print "in add_files"
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    os.chdir(dir_path)
    path, dirs, files = os.walk(dir_path).next()
    file_count = len(files)
    print file_count,path,dirs      
    file_count+=1
            
    count = 0
    while (count < number_of_files):
        backupFileName = "Backup_File_" + str(file_count) + folder_type + ".txt"
        print backupFileName
        fileOpen = open(backupFileName, 'w')
        fileOpen.write("Backup File No: " + str(file_count))
        fileOpen.close()
        file_count = file_count + 1
        count = count + 1
    os.chdir(path_to_change)
    return "true"

def add_8MB_files(dir_path, number_of_files, folder_type):
    print "in add 8 mb files"
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    os.chdir(dir_path)
    path, dirs, files = os.walk(dir_path).next()
    file_count = len(files)
    print file_count,path,dirs      
    file_count+=1
            
    count = 0
    while (count < int(number_of_files)):
        backupFileName = "Backup_File_" + str(file_count) + folder_type + ".txt"
        print backupFileName
        with open(backupFileName, 'wb') as out:
            out.seek((8 * 1024 * 1024) - 1)
            out.write('\0')
        file_count = file_count + 1
        count = count + 1
    os.chdir(path_to_change)
    return "true"

''' Function to create custom sized files by specifying the file size in bytes'''
def add_custom_sized_files(dir_path,number_of_files,folder_type,file_size,sizetype):
    print "in add custom sized files"
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    os.chdir(dir_path)
    path, dirs, files = os.walk(dir_path).next()
    file_count = len(files)
    print file_count,path,dirs      
    file_count+=1
            
    count = 0
    while (count < int(number_of_files)):
        backupFileName = "Backup_File_" + str(file_count) + folder_type + ".txt"
        print backupFileName
        with open(backupFileName, 'wb') as out:
            if(sizetype == "kb"):
                desired_size=int(file_size) * 1024
            elif(sizetype == "mb"):
                desired_size=int(file_size) * 1024 * 1024            
            for x in xrange(desired_size):   
                    out.write(chr(randint(0,255))) # Write a random byte
            out.write('\0')
        file_count = file_count + 1
        count = count + 1
    os.chdir(path_to_change)
    return "true"

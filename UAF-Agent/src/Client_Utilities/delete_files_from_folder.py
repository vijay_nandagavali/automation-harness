''' To execute the script on client machine'''
import os
  
def delete_files(dir_path, fileName):
    successFlag = "false"
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    os.chdir(dir_path)
    try:
        os.remove(fileName + ".txt")
        successFlag = "true"
    except:
        print 'My exception occurred'
        successFlag = "false"
    os.chdir(path_to_change)
    return successFlag
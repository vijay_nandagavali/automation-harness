
import os
from Config import Logs
import hashlib
   

def check_folder_exist(dir_path):
    Logs.logging.info("In create_folder_on_machine()")
    '''To check if directory already exists'''
    if(os.path.exists(dir_path)):
        if not os.listdir(dir_path): 
            return "false"
        else:
            return "true"
    else:
        return "false"
    
def verifyRestoreFolderAgainstBackupFolder(backupPath, restorePath):
    return __md5SumCheck(backupPath, restorePath)

def __md5SumCheck(backUpPath, restorePath):
    md5_res_bkp = __md5sum_calculation(backUpPath)
    print md5_res_bkp
    md5_res_rstr = __md5sum_calculation(restorePath)
    print md5_res_rstr
    if (md5_res_bkp == md5_res_rstr):
        print "Match"
        return "true"
    else :
        print "Not Match"
        return "false"  

def __md5sum_calculation(folder_location):
    if os.path.isdir(folder_location):
        md5_res_arr = []
        for path, subdirs, files in os.walk(folder_location):
            for filename in files:
                fileNewName, fileExtension = os.path.splitext(filename)
                # Here we exclude md5sum calculation of .zip file.
                if fileExtension != ".zip":
                    full_file_path = os.path.join(path, filename)
                    print "----" + full_file_path
                    md5_result = hashlib.md5(open(full_file_path).read()).hexdigest()
                    print "----" + md5_result
                    md5_res_arr.append(md5_result)
        return md5_res_arr
    
def verify_restored_filecount(dir_path):
    num_files = len([f for f in os.listdir(dir_path)])
    print "Number of total files in restored location are" +str(num_files)
    return num_files

def verifylisteddirectoriesinfolder(dirpath):
    list1=[]
    for f in os.listdir(dirpath):
        list1.append(f)
    print "List1"+str(list1)
    return str(list1)
            



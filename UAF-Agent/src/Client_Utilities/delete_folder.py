''' To execute the script on client machine'''
import os
import shutil
from Config import Logs
   
def delete_folder_on_machine(dir_path):
    Logs.logging.info("In delete_folder_on_machine()")
    successFlag = "false"
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    '''To check if directory already exists'''
    for folder in dir_path.split(","):
        if(os.path.exists(folder)):
            shutil.rmtree(folder)
            successFlag = "true"
        else:
            successFlag = "false"
            break
    
    os.chdir(path_to_change)
    Logs.logging.info("Finished delete_folder_on_machine()")
    return successFlag

       
        
    

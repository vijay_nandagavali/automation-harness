'''
Created on Jul 21, 2015

@author: savitha.p
'''

import os, time
from Config import Logs
from datetime import datetime

def read_file_for_text(dir_path, fileName, searchString):
    successFlag = "false"
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    print path_to_change
    os.chdir(dir_path)
    count = 0 
    try: 
        fileOpen = open(fileName + ".txt", 'a+')
        for line in fileOpen:
            print line
            srch = line.find(searchString)
            print srch
            if(int(srch) >= 1):
                print "Search text found!"
                Logs.logging.info("Seach text found!")
                successFlag = "true"
                break        
    except IOError as e:
        print "File not found!!"
        Logs.logging.error("File not found!!")
        Logs.logging.error(str(e))        
        successFlag = "fnf"
    except Exception as e:
        Logs.logging.error("Could not open file for writing!!!")
        Logs.logging.error(str(e))
        successFlag = "false"
        fileOpen.close()
    count = count + 1
    os.chdir(path_to_change)
    return successFlag

def compare_file_currentTs_modifiedTs(dir_path, fileName):
    successFlag = "false"
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    #print path_to_change
    os.chdir(dir_path)
    count = 0 
    try: 
        mtime = time.ctime(os.path.getmtime(fileName))
        modtime = datetime.strptime(str(mtime), "%a %b %d %H:%M:%S %Y")
        modtime = str(modtime)[0:16]
        now = datetime.now()
        curtime = now.strftime("%Y-%m-%d %H:%M")
        if(curtime == str(modtime)): 
            successFlag = "true"
        else:
            successFlag = "nomatch"
    except Exception as e:
        Logs.logging.info("ERROR: " + str(e))
        successFlag = "false"
    count = count + 1
    os.chdir(path_to_change)
    print successFlag
    return successFlag
'''
Created on Oct 31, 2014

@author: prajakta.a
'''

'''TO fire database queries on client machine'''
import pyodbc, subprocess
from Config import rabbitmq_client_proc, Logs
insertValuesToEncryptedDBScriptPath = rabbitmq_client_proc.AGENT_BASE_DIR_PATH +""
script_name = "AutoEnter"

#Constants
Driver = "{SQL Server}"
database = "master"

def __getDBConnection(serverName, userId, passwd):
    try:
        connString = "DRIVER=" + str(Driver) +";SERVER=" + str(serverName) + ";DATABASE=" + str(database) + ";UID=" + str(userId) + ";PWD=" + str(passwd)
        conn = pyodbc.connect(connString,autocommit=True)
        return conn
    except Exception as exception:
        print exception

'''Create Database with SQL Authentication'''
def createDb(serverName, userId, passwd, testDBName):    
    try:
        dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        query = "SELECT name FROM master..sysdatabases"
        db_names = []
        for row in cur.execute(query).fetchall():
            db_names.append(row.name)
        new_db_names = testDBName.split(",")
        if (len(new_db_names) > 1):
            for i in new_db_names:
                if (i in db_names):
                    print "Database " + i + " already exists or is a system database"
                else:
                    cur.execute("CREATE DATABASE " + i)
                    print "Created database" , i
        else:
            if (testDBName in db_names):
                print "Database " + testDBName + " already exists or a system database"
                Logs.logging.info("Database " + testDBName + " already exists or a system database")
            else:
                cur.execute("CREATE DATABASE " + testDBName)
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False

'''Delete Database with SQL Authentication'''
def deleteDb(serverName, userId, passwd, testDBName):
    try:
        dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        query = "SELECT name FROM master..sysdatabases"
        db_names = []
        for row in cur.execute(query).fetchall():
            db_names.append(row.name)
        new_db_names = testDBName.split(",")
        if (len(new_db_names) > 1):
            for i in new_db_names:
                if (i in db_names):
                    cur.execute("DROP DATABASE " + i)
                    print "Database " + i + " deleted successfully"
                else:
                    print "Database " + i + " does not exists or a system database"
        else:
            if (testDBName in db_names):
                cur.execute("DROP DATABASE " + testDBName)
                print "Database " + testDBName + " deleted successfully"  
                Logs.logging.info("Database " + testDBName + " deleted successfully")
            else:
                print "Database " + testDBName + " does not exists or is a system database" 
                Logs.logging.info("Database " + testDBName + " does not exists or is a system database")
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False

def createTablewithValues(serverName, userId, passwd, testDBName, tableCount):
    try:
        dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        cur.execute("Use " + testDBName)
        for value in range(0, int(tableCount)):
            tableName = "TestTable_" + str(value)
            if cur.tables(table=tableName).fetchone():
                print "Table: " + tableName + "Exists."
            else:
                # Create table as per requirement
                sql = """CREATE TABLE """ + tableName + """(
                         FIRST_NAME  CHAR(20) NOT NULL,
                         LAST_NAME  CHAR(20),
                         AGE INT)"""            
                cur.execute(sql)            
                insertValuesInTable(cur, tableName, tableCount)
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False    
            
def insertValuesInTable(cur, tableName, tableCount):
    for value in range(0, int(tableCount)):
        # Prepare SQL query to INSERT a record into the database.
        firstName = "Test_" + str(value)
        lastName = "Test_" + str(value)
        tableValues = """INSERT INTO """ +  tableName + """(FIRST_NAME,
                 LAST_NAME, AGE)
                 VALUES (""" + """'""" + firstName + """'""" +  """, '""" + lastName + """'""" + """, """ + str(value) + """)"""
        print tableValues
        try:
            # Execute the SQL command
            cur.execute(tableValues)
        except Exception as e:
            print "Exception: " + str(e)
            Logs.logging.error("Exception: " + str(e))

def verifyTableexistsinDatabase(serverName, userId, passwd, testDBName, tableName):
    try:
   
        dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        print str(testDBName)
        print str(tableName)
        cur.execute("Use " + testDBName)
        rows_count=cur.execute("select LAST_NAME from "+str(tableName))
        tablecheck=0
        if(rows_count >0):
            rows = cur.fetchall()
            for row in rows:
                print row.LAST_NAME
                print "Exists Table"+str(tableName)
                tablecheck=0
                break
               
    except Exception as e:
            print "Exception: " + str(e)
            tablecheck=1
    finally:
            if(tablecheck ==1):
                print ("Table does not exist" +tableName)
                return False
            else:
                print ("Table Exists" +tableName)
                return True

def createColumnMasterKey(serverName, userId, passwd, testDBName):
    try:
        dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        cur.execute("Use " + testDBName)
        # Create Column Master Key as per requirement
        sql = """CREATE COLUMN MASTER KEY [SQLMASTERKEY] WITH
            (
            KEY_STORE_PROVIDER_NAME = N'MSSQL_CERTIFICATE_STORE',
            KEY_PATH = N'LocalMachine/My/8B3FABB664B842E2A4202F3BA7FD7062DB1DB55D'
            )"""            
        cur.execute(sql)            
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False  

def createColumnEncryptionKey(serverName, userId, passwd, testDBName):
    try:
        dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        cur.execute("Use " + testDBName)
        # Create Column Master Key as per requirement
        sql = """CREATE COLUMN ENCRYPTION KEY [COLUMNENCRYPTIONKEY] WITH VALUES
            (
            COLUMN_MASTER_KEY = [SQLMASTERKEY],
            ALGORITHM = 'RSA_OAEP',
            ENCRYPTED_VALUE = 0x01700000016C006F00630061006C006D0061006300680069006E0065002F006D0079002F0038006200330066006100620062003600360034006200380034003200650032006100340032003000320066003300620061003700660064003700300036003200640062003100640062003500350064002E930E4362022735A7AFE9BFC86788367979F8EFD37030B284AB25BE973F6FB84F95A523DB48739B15095616734DB270F6675D5B16CF1C202DD89CF82D51E1DD5D17DEB3324F3506D011A9ED417B654440D35AAC039F0922C1129B3EC0B5DA4232A64E13C53AE429F1B3A81045F9F850DFF40812F0E33098382B958D146DAC6F1F050D0018A71533BCE4CE7A4F6FCDAAE8DD6EFA7EDA87F6281ED6E415F4A0BC0996C1F5A3B8E174690106935873D9EB3F9251ADA105D332D4AE294D4579336F24F6A51BA2FC9ED622F516E6226C76317C069BC1F889F3B9F520016277C61F4929C560C805155B9FA0F0FB137F2FE9FF02AF10695515C122711C8BCF2906E7D8968E2AD937EB61D3D44D85FA188FF696756193D5E5F7A8CF0C61A5F5759B46905CC11A5281A1F3E10E262765F9E103109F7E0F92047B0286E92D9B429A4BCB413CAA259C1FD2217CC01D378451873776281617AE0CB056A5A4C98A161B796590C1837A2971D434AA9975B79BD21E69DFB311A7E12AA018B8098127E2F4FB183E66FA8008A3F4AB1088266A27AE5D1CD2A5765E6F0F753F3C3C31A6436863689F4642548C16F23687359216252E4B4209D711C2E73C265E2A905E591F181E8C5FE857B1D45F63E502056C43F41B1C39610C5B26B6C0571B96CC21BAF949CA83E80A31F5E75E4C5803EF08464AD66E4971866A1711992EEB9EFF8226DD8175079D
            )"""            
        cur.execute(sql)            
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False

def createEncryptedTable(serverName, userId, passwd, testDBName):
    try:
        dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        cur.execute("Use " + testDBName)
        #for value in range(0, int(tableCount)):
        tableName = "TestTable" 
        if cur.tables(table=tableName).fetchone():
            print "Table: " + tableName + "Exists."
        else:
            # Create table as per requirement
            sql = """CREATE TABLE """ + tableName + """(
                    [EMPLOYEEID][INT] IDENTITY(1,1) NOT NULL,
                    [FIRSTNAME][VARCHAR](50),
                    [LASTNAME][VARCHAR](50),
                    [SALARY] [NUMERIC](12)
                    ENCRYPTED WITH (ENCRYPTION_TYPE = DETERMINISTIC,
                    ALGORITHM = 'AEAD_AES_256_CBC_HMAC_SHA_256',
                    COLUMN_ENCRYPTION_KEY=COLUMNENCRYPTIONKEY) NOT NULL)"""            
            cur.execute(sql)            
            #insertValuesInTable(cur, tableName, tableCount)
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False
def createEncryptedTableProcedure(serverName, userId, passwd, testDBName):
    try:
        dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        cur.execute("Use " + testDBName)
        # Create store procedure as per requirement for AlwaysEncryptedDb
        sql = """CREATE PROCEDURE [dbo].[Insert_Always_Encrypted_DB] (
                @FIRSTNAME VARCHAR(50),
                @LASTNAME VARCHAR(50),
                @SALARY NUMERIC(12)) AS 
                INSERT INTO [dbo].[TestTable]
                (FIRSTNAME, LASTNAME, SALARY)
                VALUES (@FIRSTNAME,@LASTNAME,@SALARY)"""            
        cur.execute(sql)            
        #insertValuesInTable(cur, tableName, tableCount)
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False

def createServerAuthenticateLogin(serverName, userId, passwd, testDBName):
    try:
        dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        cur.execute("Use [master]")
        # Create store procedure as per requirement for AlwaysEncryptedDb
        sql = """CREATE LOGIN [Greg] WITH PASSWORD=N'Test', DEFAULT_DATABASE=[""" + testDBName +"""], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF"""            
        cur.execute(sql)            
        #insertValuesInTable(cur, tableName, tableCount)
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False 

def createDatabaseUser(serverName, userId, passwd, testDBName):
    try:
        print "In Function"
	dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        cur.execute("Use " + testDBName)
	print testDBName
        # Create store procedure as per requirement for AlwaysEncryptedDb
        sql = """CREATE USER [Greg] FOR LOGIN [Greg] WITH DEFAULT_SCHEMA=[dbo]"""            
        cur.execute(sql)
	print str(sql)           
        sql1 = """ALTER ROLE [db_owner] ADD MEMBER [Greg]"""
        cur.execute(sql1)
	print str(sql1)
        #insertValuesInTable(cur, tableName, tableCount)
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False 

'''Delete User with SQL Authentication'''
def deleteUser(serverName, userId, passwd, testDBName):
    try:
	dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        cur.execute("Use master")
	print testDBName
        # Create store procedure as per requirement for AlwaysEncryptedDb
        sql = """DROP LOGIN [Greg]"""            
        cur.execute(sql)
	print str(sql)           
        #insertValuesInTable(cur, tableName, tableCount)
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False

'''Delete User with SQL Authentication'''
def deleteUser(serverName, userId, passwd, testDBName):
    try:
	dbConnection = __getDBConnection(serverName, userId, passwd)
        cur = dbConnection.cursor()
        cur.execute("Use master")
	print testDBName
        # Create store procedure as per requirement for AlwaysEncryptedDb
        sql = """DROP LOGIN [Greg]"""            
        cur.execute(sql)
	print str(sql)           
        #insertValuesInTable(cur, tableName, tableCount)
        return True
    except Exception as e:
        print str(e)
        Logs.logging.error("Exception: " + str(e))
        return False

def ExecuteScript():
    try:
        print insertValuesToEncryptedDBScriptPath + "/" +script_name
        sp = subprocess.Popen(insertValuesToEncryptedDBScriptPath + "/" +script_name ,cwd=insertValuesToEncryptedDBScriptPath,stdout=subprocess.PIPE,shell=True)
        sp.wait()
        out = sp.communicate()
        print str(out[0])
        Logs.logging.info(str(out[0]))
        return True
    except Exception as e:
        Logs.logging.error("Exception: " + str(e))
        return False
    


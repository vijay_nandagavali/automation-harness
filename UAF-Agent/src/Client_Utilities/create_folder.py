''' To execute the script on client machine'''
import os
import shutil
from Config import Logs
   
def create_folder_on_machine(dir_path, number_of_files, folder_type):
    successFlag = "false"
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    '''To check if directory already exists'''
    if(os.path.exists(dir_path)):
        shutil.rmtree(dir_path)
    os.mkdir(dir_path)
    os.chdir(dir_path)
    
    '''Create given number of files in the required folder on the machine'''
    for i in range(1,number_of_files+1):
        filename="Backup_File_"+str(i)+folder_type+".txt"
        try:
            fo = open(filename, 'a')
            fo.write("Backup File_" + folder_type + "_No: ")
            fo.write(str(i))
            fo.close()
            successFlag = "true"
        except Exception as e:
            Logs.logging.error(e.getMessage())
            successFlag = "false"
    os.chdir(path_to_change)
    Logs.logging.info("Finished create_folder_on_machine()")
    return successFlag
	
def create_dedup_folder_on_machine(dir_path):

    successFlag = "true"
    try:
        if(os.path.exists(dir_path)):
            shutil.rmtree(dir_path)
        os.mkdir(dir_path)
        
    except Exception as e:
        Logs.logging.error(e.getMessage())
        successFlag = "false"

    Logs.logging.info("Finished create_folder_on_machine()")
    return successFlag	
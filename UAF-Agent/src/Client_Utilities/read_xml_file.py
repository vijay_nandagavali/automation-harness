'''
Created on Dec 23, 2014

@author: Amey.k
'''

from Handler.script_executor import checkSriptIsPresentOrNot
import xml.etree.ElementTree as ET

def read_xml_file(xml_file_path,xml_file_name,tagName):
    if checkSriptIsPresentOrNot(xml_file_path + "/" +xml_file_name):
        tree = ET.parse(xml_file_path + "/" +xml_file_name)
        root = tree.getroot()
        for child in root:
            for name in child.iter(tagName):
                print name.tag, name.text
        if (name.text != ""):
            return "true"
        else:
            return "false"

    else:
        print xml_file_name + " does not exist at location " + xml_file_path
        return "false"
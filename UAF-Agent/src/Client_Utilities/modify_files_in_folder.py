''' To execute the script on client machine'''
import os
  
def modify_files(dir_path, fileName):
    successFlag = "false"
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    print path_to_change
    os.chdir(dir_path)
    count = 0 
    try: 
        fileOpen = open(fileName + ".txt", 'a+')
        fileOpen.write("_Modified")
        successFlag = "true"
    except:
        print "Could not open file for writing!!!"   
        successFlag = "false"
    fileOpen.close()
    count = count + 1
    os.chdir(path_to_change)
    return successFlag
        
    


'''
This file will act as router for different types of receiver and redirect the flow to suitable receiver
Author : Afour Technologies

'''
from Config import rabbitmq_client_proc
from Message.receiver import driver_message_receiver
from Message.receiver import client_message_receiver
from Config import Logs
if (rabbitmq_client_proc.agentType == 'driver' ):
	Logs.logging.info("Starting driver message receiver " + rabbitmq_client_proc.rabbitmqServer)
	driver_message_receiver.start()
else:
	Logs.logging.info("Starting client message " + rabbitmq_client_proc.rabbitmqServer)
	client_message_receiver.start()

'''
Created on Sep 14, 2015

@author: a4tech
'''

import paramiko

def SshRemoteMachineConnection(ipAddress,userName, password):
    print ipAddress
    print userName
    print password
    ssh=paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.load_system_host_keys()
    ssh.connect(hostname=ipAddress, username=userName ,password=password)
    return ssh

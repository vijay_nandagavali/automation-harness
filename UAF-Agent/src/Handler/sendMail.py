'''
Created on Sep 14, 2015

@author: a4tech
'''
from Config import Logs
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
import linuxMachineConnect
import smtplib
import time
import json
import os
from Config import rabbitmq_client_proc

def sendExecutionIdAndBuildnum(envObj1,exeId):
    
    print "in sendExecutionIdAndBuildnum" 
    print "Envconfig: " + envObj1
    
    try:
        envObj = json.loads(envObj1)
        ipAddress = envObj["uebs"][0]["url"]
        print "ipAddress: "+ str(ipAddress)
        userName = envObj["uebs"][0]["username"]
        print "userName: "+ str(userName)
        password = envObj["uebs"][0]["password"]
        print "password: "+ str(password)
        UebVersion = getUEBVersion(ipAddress, userName, password)
        print "UebVersion: "+ str(UebVersion)
        MAIL_RECEIVER_LIST = envObj["emailRecipients"]
        print MAIL_RECEIVER_LIST
         
        msg = MIMEMultipart()
        user = "a4@afourtech.com"
        password = "@four123"
        msg['From'] = user
        msg['To'] = COMMASPACE.join(MAIL_RECEIVER_LIST)   
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "[Unitrends] New Automation Run Details |" + time.strftime("%c")
        msg.attach(MIMEText("Hi, <br/><br/>Thank you for triggering the automation run. You will get the report soon.<br/>Please find the details of run: <br/><br/><b>ExecutionId:</b>\tE-" + str(exeId) + "<br/><b>Build Number:</b>\t" + str(UebVersion) + "<br/><br/><br/>Thanks and Regards,<br/>Automation Team" , 'html'))         

        server = "smtp.gmail.com:587"
        smtp = smtplib.SMTP(server)
        smtp.starttls()
        smtp.login(user, password)
        toaddress = MAIL_RECEIVER_LIST
        smtp.sendmail(user, toaddress, msg.as_string())
        smtp.close()
        print "Mail Sent..."
        return
    except Exception as e:
        print "There is some Error Please check UAF Agent Logs at : " + rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/Logs/AgentLogs.log"
        Logs.logging.info("Excepion: "+str(e))  

def getUEBVersion(ipAddress,userName, password):
    try:
        print "get ueb version:"
        conn = linuxMachineConnect.SshRemoteMachineConnection(ipAddress,userName, password)
        _, out, err = conn.exec_command('dpu version | grep Version | cut -c8-')
        print err
        temp = out.readlines()
        return str(temp[0]).strip(' \r\t\n')
    except Exception as e:
        Logs.logging.info("Error in fetching UEB version : " + str(e))
        
def updateUEB(ipAddress, userName, password, buildNo):
    try:
        conn = linuxMachineConnect.SshRemoteMachineConnection(ipAddress,userName, password)
        _, out, err = conn.exec_command('cd /etc/yum.repos.d')
        print err
        temp = out.readlines()
        try:
            os.remove('/etc/yum.repos.d/unitrends-nightly-'+buildNo+".repo")
        except OSError:
            pass
        _, out, err = conn.exec_command('wget http://repository.unitrends.com/~nightly/unitrends-nightly-'+buildNo+".repo")
        print err
        temp = out.readlines()
        
        _, out, err = conn.exec_command('yum update --nogpgcheck -y')
        print err
        temp = out.readlines()
            
        return True
    except Exception as e:
        Logs.logging.info("Error in fetching UEB version : " + str(e))
def is_Core_Dump(ipAddress, userName, password):
    try:
        conn = linuxMachineConnect.SshRemoteMachineConnection(ipAddress,userName, password)
        _, out, err = conn.exec_command('ls -l /backups/dumps')
        print err
        temp = out.readlines() 
        core1=[]
        core1=list(temp) 
        if(len(core1)>1):
            Logs.logging.info("Length of temp is  " +str(len(core1)))
            Logs.logging.info("Core Dump is present")
            return core1
        else:
            return 0
        '''core=str(temp[1])'''
    except Exception as e:
        Logs.logging.info("Error in fetching the  Core Dump time stamp details: " + str(e)) 
    
def sendCoreDumpEmail(envObj1, exeId, ipAddress ,testcaseId,timestamp1):
    print "in sendCoreDumpEmail" 
    print "Envconfig: " + envObj1
    
    try:
        envObj = json.loads(envObj1)
        MAIL_RECEIVER_LIST = envObj["emailRecipients"]
        print MAIL_RECEIVER_LIST
         
        msg = MIMEMultipart()
        user = "a4@afourtech.com"
        password = "@four123"
        msg['From'] = user
        msg['To'] = COMMASPACE.join(MAIL_RECEIVER_LIST)   
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "[Unitrends] Core dump Notification"
        msg.attach(MIMEText("[Unitrends] Core dump has been generated on: " + ipAddress +" during the execution: E- "+exeId +" TestCaseID:"+testcaseId +"Timestamp "+timestamp1))       

        server = "smtp.gmail.com:587"
        smtp = smtplib.SMTP(server)
        smtp.starttls()
        smtp.login(user, password)
        toaddress = MAIL_RECEIVER_LIST
        smtp.sendmail(user, toaddress, msg.as_string())
        smtp.close()
        print "Mail Sent..."
        return
    except Exception as e:
        print "There is some Error Please check UAF Agent Logs at : " + rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/Logs/AgentLogs.log"
        Logs.logging.info("Excepion: "+str(e))     
        
def gettimestampo_DumpFile(ipAddress, userName, password ,dumpfilename):
    try:
        conn = linuxMachineConnect.SshRemoteMachineConnection(ipAddress,userName, password)
        _, out1, err = conn.exec_command('stat -c %Y /backups/dumps/'+dumpfilename)
        print err
        core = out1.readlines()       
        Logs.logging.info("Core" + str(core[0]))
        return str(core[0])
    except Exception as e:
        Logs.logging.info("Error occurred in the get time stamp function for dump file : " + str(e))

def changePrimaryUIToRRC(ipAddress, userName, password):
    try:
        conn = linuxMachineConnect.SshRemoteMachineConnection(ipAddress,userName, password)
        _, out, err = conn.exec_command('cmc_ui set rrc')
        print err
        temp = out.readlines()
        Logs.logging.info("Set default UI to rrc")   
        return True
    except Exception as e:
        Logs.logging.info("Error in setting default UI to rrc")        

        

        


import json
import threading
import router
from Config import Logs, rabbitmq_client_proc
import subprocess
from Util import constants

def parseJson(input_json):
	execution = threading.Thread(target=jsonParser, args=(input_json,))
	execution.start()	

'''
This function will take input as a json and will parse the json and depending on the value of the Action the function will Call 
other functions.
'''
	
def jsonParser(input_json):
# def parseJson(input_json):
	try:
		input_json = input_json.replace('\n', '\\n').replace('\r', '\\r')
		jsonData = json.loads(input_json)  # The json.loads() function takes a JSON string and returns it as a Python data structure.
		payloadData = json.dumps(jsonData['payload'])  # The json.dumps function takes a Python data structure and returns it as a JSON string.		
		payloadJson = json.loads(payloadData)  # converts string to json
		# print "Payload Data  - - " + payloadJson['URL']
		Logs.logging.info("Parsed JSON format:" + str(payloadJson))		
		if jsonData['action'] == 'executeScript':
			router.executeScript(payloadJson)
			
		elif jsonData['action'] == 'executeMultipleScripts':
			exeId = jsonData['payload']['executionId']
			Logs.logging.info("Execution ID: " + str(exeId))
			sp = subprocess.Popen([router.executeMultipleScripts(payloadJson, exeId)])
			sp.wait()
			constants.PROCESSIDS.insert(len(constants.PROCESSIDS), sp.pid)
			
		elif jsonData['action'] == 'executeClientUtilities':
			return router.executeClientUtilities(payloadJson)
						
		print ' [*] Waiting for logs. To exit press CTRL+C'			
	except Exception as e:
		print "There is some Error Please check UAF Agent Logs at : " + rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/Logs/AgentLogs.log"
		Logs.logging.error(e)

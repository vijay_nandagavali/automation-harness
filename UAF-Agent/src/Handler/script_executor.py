'''
This script to execute automation test scripts i.e .exe file
'''
import subprocess
from Util import constants, filereader_writer
from Config import Logs, rabbitmq_client_proc
import os
import time
from Message.receiver import initialize_receiver
import json
from Handler import sendMail
from datetime import datetime
'''
This method to execute single test script depends on the script type
'''
testCaseRepositoryPath = rabbitmq_client_proc.SCRIPT_DIR_PATH + "/Test_Automation"
testSuiteFilePath = rabbitmq_client_proc.SCRIPT_DIR_PATH + \
    "/PHP-API/Test_Suite/TestSuite.xml"
phpApiDriverPath = rabbitmq_client_proc.SCRIPT_DIR_PATH + \
    "/PHP-API/src/driver/Driver.py"
satoriDriverScriptPath = rabbitmq_client_proc.SCRIPT_DIR_PATH + "/Satori-UI"
boomerangDriverScriptPath = rabbitmq_client_proc.SCRIPT_DIR_PATH + "/Boomerang-UI"

#Use this dictionary for script types using the generic executor
driverPaths = {'REST-API':"/REST-API/src/driver/RestApiDriver.py", 'Deduplication':"/Dedup/src/driver/driver.py", 
               'CBT-AUTOMATION': "/CBTDriverAutomation/src/driver/CBTAutomationDriver.py", 'Regression': "/Regression/src/driver/driver.py",
                 'Inplace_Synthesis': "/InplaceSynthesis/src/driver/driver.py"}        

def getEnvironmentConfigFilePath(exeId):
    environmentConfigFilePath=""
    createFolder = filereader_writer.createNewFolder(rabbitmq_client_proc.SCRIPT_DIR_PATH + "/Config", str(exeId))
    print createFolder
    if(createFolder):
        environmentConfigFilePath = rabbitmq_client_proc.SCRIPT_DIR_PATH + "/Config/E-" + str(exeId) + "/Environment.json"
    else:
        Logs.logging.error("Folder not created for Environment.json")
    return environmentConfigFilePath

def execute(scriptId, project, scriptConfig, executionId, emailFlag):
    try:
        Logs.logging.info("scriptid: "+str(scriptId)+"\n")
        Logs.logging.info("project: "+str(project)+"\n")
        Logs.logging.info("executionid: "+str(executionId)+"\n")
        
        if(project == "SATORI-UI"):
            if checkSriptIsPresentOrNot(satoriDriverScriptPath + "/SatoriDriver.jar"):
                sp = subprocess.Popen(['java', '-jar', satoriDriverScriptPath + "/SatoriDriver.jar", "E-" + str(
                    executionId), scriptId, str(emailFlag)], cwd=satoriDriverScriptPath)
                sp.wait()
                #waitToCompleteTestScript(sp.pid)
                constants.PROCESSIDS.insert(
                    len(constants.PROCESSIDS), sp.pid)
            else:
                print "SatoriDriver.jar script of type " + str(project) + " does not exists at location " + satoriDriverScriptPath
                Logs.logging.error("SatoriDriver.jar script of type " + str(project) + " does not exists at location " + satoriDriverScriptPath)
                return False
        elif(project == "Boomerang_UI"):
            if checkSriptIsPresentOrNot(boomerangDriverScriptPath + "/BoomerangDriver.jar"):
                sp = subprocess.Popen(['java', '-jar', boomerangDriverScriptPath + "/BoomerangDriver.jar", "E-" + str(
                    executionId), scriptId, str(emailFlag)], cwd=boomerangDriverScriptPath)
                sp.wait()
                #waitToCompleteTestScript(sp.pid)
                constants.PROCESSIDS.insert(
                    len(constants.PROCESSIDS), sp.pid)
            else:
                print "BoomerangDriver.jar script of type " + str(project) + " does not exists at location " + boomerangDriverScriptPath
                Logs.logging.error("BoomerangDriver.jar script of type " + str(project) + " does not exists at location " + boomerangDriverScriptPath)
                return False
        elif(project == "FLEX-UI"):
            scriptPath = testCaseRepositoryPath + "/UEB_TestSuiteExecution.exe"
            if checkSriptIsPresentOrNot(scriptPath):
                if scriptConfig != 'null':
                    filereader_writer.createNewFile(
                        testCaseRepositoryPath + "/Test_Suite/TestSuite.xml", scriptConfig)
                    sp = subprocess.Popen(scriptPath)
                    sp.wait()
#                     waitToCompleteTestScript(sp.pid)
                    constants.PROCESSIDS.insert(
                        len(constants.PROCESSIDS), sp.pid)
                else:
                    print "UEB_TestSuiteExecution.exe script of type " + project + " does not have configuration XML"
                    Logs.logging.info(
                        "UEB_TestSuiteExecution.exe script of type " + project + " does not have configuration XML")
                    return False
            else:
                print "UEB_TestSuiteExecution.exe script of type " + project + " does not exists at location " + testCaseRepositoryPath
                Logs.logging.error(
                    "UEB_TestSuiteExecution.exe script of type " + project + " does not exists at location " + testCaseRepositoryPath)
                return False

        elif (project == 'PHP-API'):
            if checkSriptIsPresentOrNot(phpApiDriverPath):
                if scriptConfig != 'null':
                    filereader_writer.createNewFile(
                        testSuiteFilePath, scriptConfig)
                    sp = subprocess.Popen(
                        ['python', phpApiDriverPath, "E-" + str(executionId), str(emailFlag)])
                    sp.wait()
#                     waitToCompleteTestScript(sp.pid)
                    constants.PROCESSIDS.insert(
                        len(constants.PROCESSIDS), sp.pid)
                else:
                    print "Driver.py script of type " + project + " does not have configuration XML"
                    Logs.logging.info(
                        "Driver.py script of type " + project + " does not have configuration XML")
                    return False
            else:
                print "Driver.py script of type " + project + " does not exists at location " + phpApiDriverPath
                Logs.logging.error(
                    "Driver.py script of type " + project + " does not exists at location " + phpApiDriverPath)
                return False

        else: 
            GenericExecutor(scriptId, project, scriptConfig, executionId, emailFlag)
        print "Executed: " + str(scriptId) + "Script Type: " + str(project)
        Logs.logging.info("Executed script " + str(scriptId) + " of type " + str(project))
        return True
    except Exception as e:
        Logs.logging.error(str(e))
        return False

'''
This method is to execute multiple test script and update the execution status as 'COMPLETE' when it gets completed
'''


def executeScripts(scripts, exeId):
    from Handler.dbHandler import DBHandler
    insertReport = False
    try:
        dbHan = DBHandler()
        dbHan.connectToDB()
        print "our logs"
        Logs.logging.info(
                          "#####     Execution Id : " + str(scripts["executionId"]) + "     #####")

        '''
        Writing json data in Environment.json file
        '''
        print "our logs"
        Logs.logging.info("our logs")
        Logs.logging.info(str(scripts))
        envconfig = json.dumps(scripts["environment"])
        print exeId
        environmentConfigFilePath = getEnvironmentConfigFilePath(exeId)
        print "Environment Config path: "+str(environmentConfigFilePath)
        print str(envconfig)
        filereader_writer.createNewFile(environmentConfigFilePath, envconfig)
        # print "scripts: " + scripts
        # Logs.logging.info("scripts" + str(scripts))
        if 'browser' not in scripts:
            filereader_writer.updatePropertyfile('Browser', scripts["browser"]["name"])
        Logs.logging.info("Created Environment.json File at location: " + str(environmentConfigFilePath))

        if(scripts["testSuite"] == None):
            testCases = scripts["testCases"]["testCaseList"]
        else:
            testCases = scripts["testSuite"]["testCases"]
        
        envObj = json.loads(envconfig)
        ipAddress = envObj["uebs"][0]["url"]
        userName = envObj["uebs"][0]["username"]
        password = envObj["uebs"][0]["password"]
        
        buildNum = sendMail.getUEBVersion(ipAddress, userName, password)
        Logs.logging.info("Build Number Before update : " + str(buildNum))
        Logs.logging.info("Update flag : " + str(scripts["update"]))
        if(scripts["update"]==True):
			Logs.logging.info("\nUpdating the UEB...")
			buildNo=[]
			buildNo=buildNum.split("-")
			sendMail.updateUEB(ipAddress, userName, password, buildNo[0])
        
			buildNum = sendMail.getUEBVersion(ipAddress, userName, password)
        		Logs.logging.info("Build Number After update: " + str(buildNum)) 
        sendMail.sendExecutionIdAndBuildnum(envconfig,exeId)
        if(scripts["testSuite"] == None):
            dbHan.addBuildNumberInProduct(scripts["testCases"]["product"]["id"], buildNum)
        else:
            dbHan.addBuildNumberInProduct(scripts["testSuite"]["product"]["id"], buildNum)
        sendMail.changePrimaryUIToRRC(ipAddress, userName, password)
        Logs.logging.info("Changed Main UI to rrc")
	ipAddressSource = envObj["uebs"][1]["url"]
        if ipAddressSource :
              userNameSource = envObj["uebs"][1]["username"]
              passwordSource = envObj["uebs"][1]["password"]
              sendMail.changePrimaryUIToRRC(ipAddressSource, userNameSource, passwordSource)
              Logs.logging.info("Changed Primary UI to rrc")
	else :
	     Logs.logging.info("Primary UI is not provide")
	
        ipAddressTarget = envObj["uebs"][2]["url"]
        if ipAddressTarget :
              userNameTarget = envObj["uebs"][2]["username"]
              passwordTarget = envObj["uebs"][2]["password"]
              sendMail.changePrimaryUIToRRC(ipAddressTarget, userNameTarget, passwordTarget)
              Logs.logging.info("Changed Secondary UI to rrc")
	else :
	     Logs.logging.info("Secondary UI is not provide")
	


        for script in range(0, len(testCases)):
#             coreDump = sendMail.is_Core_Dump(ipAddress, userName, password)
#             if(coreDump.find("core")!=-1):
#                 Logs.logging.info("Core dump!!!")
#                 sendMail.sendCoreDumpEmail(envconfig, exeId, ipAddress)
#                 break
            # Check 'testScriptConfig' element is present in JSON or not, If
            # yes execute the script else not and log an error
            if script == (len(testCases) - 1):
                emailFlag = True
            else:
                emailFlag = False
            if 'testScriptConfig' in testCases[script]:
                # Check the script executed or not, If yes update the execution
                # status to 'COMPLETE'
                
                if execute(testCases[script]["testScriptId"], testCases[script]["project"], testCases[script]["testScriptConfig"], scripts["executionId"], emailFlag):
#                     if (script == 0):
#                         sendMail.sendExecutionIdAndBuildnum(envconfig,exeId)
                    testcaseId=str( testCases[script]["testScriptId"])
                    ts=time.time()
                    Logs.logging.info("Starting Time Stamp is "+str(ts))
                    dbHan.updateScriptExecutionStatus(
                        scripts["executionId"], testCases[script]["testScriptId"], "COMPLETE")
                    jsonReport_Str = filereader_writer.getJSONStringFromFile(
                        scripts["executionId"])
                    print jsonReport_Str
                    if (jsonReport_Str != None):
                        if(insertReport == False):
                            dbHan.insertExecutionReportInfo(
                                scripts["executionId"], jsonReport_Str)
                            insertReport = True
                        else:
                            dbHan.updateExecutionReportInfo(
                                "E-" + str(scripts["executionId"]), jsonReport_Str)
                    #Checking the core dump after each test case                 
                    coreDump=[]                
                    coreDump = sendMail.is_Core_Dump(ipAddress, userName, password)
                    if(coreDump <>0):
                        for x in range(1,len(coreDump)):
                            Logs.logging.info("Core Dump: "+str(coreDump))
                            if(coreDump[x].find("core")!=-1):
                                dumpfilename=str(coreDump[x]).split(':')
                                print str(dumpfilename[1]).split(' ')[1]
                                dumpfilename1=str(dumpfilename[1]).split(' ')[1]
                                Logs.logging.info("DumpFile Name is  "+str(dumpfilename1))
                                timestampdumpfile=sendMail.gettimestampo_DumpFile(ipAddress, userName, password,dumpfilename1)
                                timestamp=str(timestampdumpfile)
                                Logs.logging.info("timestampdumpfile2 "+timestampdumpfile)
                                startingtimestamp=datetime.fromtimestamp(ts).strftime( "%d.%m.%Y %H:%M:%S")
                                t = time.mktime(time.strptime(startingtimestamp, "%d.%m.%Y %H:%M:%S"))
                                Logs.logging.info("T is "+str(t))
                                Logs.logging.info("FloatT is "+str(float(t)))
                                Logs.logging.info("Float timestampdumpfile is "+str(float(timestampdumpfile)))
                                if(float(t) < float(timestampdumpfile)):
                                    Logs.logging.info("Core dump!!!")
                                    sendMail.sendCoreDumpEmail(envconfig, exeId, ipAddress,testcaseId,timestamp)
                                    break 

            else:
                print "Script does not have testScriptConfig element"
                Logs.logging.info(
                    "Script does not have testScriptConfig element")

    
    except Exception as e:
        Logs.logging.error(str(e))
     
    finally:
        dbHan.updateTestPassExecutionStatus(scripts["executionId"])
        dbHan.updateDriverMachineExecutionStatus(
            initialize_receiver.getReceiverUUID())        
        dbHan.updateEnvironmentStatus(scripts["environment"]["id"])
        dbHan.closeDBConnection()
        Logs.logging.info(
            "#####     Completed the execution of test scripts with execution Id : " + str(scripts["executionId"]) + "     #####")
        
    
'''
Method to verify that the automated script file exist at test case repository or not
'''
def checkSriptIsPresentOrNot(scriptPath):
    try:
        if os.path.isfile(scriptPath):
            return True
        else:
            return False
    except Exception as e:
        Logs.logging.error(str(e))


'''
Method to wait until test case complete with the execution
'''
# def waitToCompleteTestScript(p_id):
#     while psutil.pid_exists(p_id):
#         time.sleep(5)

def GenericExecutor(scriptId, project, scriptConfig, executionId, emailFlag):
    try:
        Logs.logging.info("scriptid: "+str(scriptId)+"\n")
        Logs.logging.info("project: "+str(project)+"\n")
        Logs.logging.info("executionid: "+str(executionId)+"\n")
        print "A generic method to execute the scripts except Satori and FLEX UI scripts"
        strSpecificPath = str(driverPaths[project])
        genericDriverPath = rabbitmq_client_proc.SCRIPT_DIR_PATH +  \
                     strSpecificPath
        print "Driver path constructed by the Generic Executor is " + str(genericDriverPath)
        if checkSriptIsPresentOrNot(genericDriverPath):
            sp = subprocess.Popen(['python', genericDriverPath, "E-" + str(executionId), scriptId, str(emailFlag)])
            sp.wait()
    #       waitToCompleteTestScript(sp.pid)
            constants.PROCESSIDS.insert(len(constants.PROCESSIDS), sp.pid)
        else:
            print "driver.py script of type " + project + " does not exist at location " + genericDriverPath
            Logs.logging.error("driver.py script of type " + project + " does not exist at location " + genericDriverPath)
            return False   
    except Exception as e:
        Logs.logging.error(str(e))       
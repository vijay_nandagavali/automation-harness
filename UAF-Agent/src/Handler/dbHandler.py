'''
Created on Jul 28, 2014

@author: AFourtechnologies
'''

from pymongo.mongo_client import MongoClient
from Config import rabbitmq_client_proc
from Config import Logs
import gridfs
from macpath import basename
import os
from __builtin__ import file
from bson.dbref import DBRef
from bson.objectid import ObjectId


class DBHandler:

    def __init__(self):
        self.client = ''
        self.db = ''
    '''
    Method to establish data base connection
    '''

    def connectToDB(self):
        try:
            self.client = MongoClient(rabbitmq_client_proc.dataBaseServer, int(
                rabbitmq_client_proc.dataBaseServerPort))
            self.db = self.client[rabbitmq_client_proc.dataBaseName]
            Logs.logging.info("Connected to database " + rabbitmq_client_proc.dataBaseServer + " at port " +
                              rabbitmq_client_proc.dataBaseServerPort + " with database name " + rabbitmq_client_proc.dataBaseName)
        except Exception as e:
            Logs.logging.error(e)

    '''
    Method to update test case execution status of a test script into the database
    '''

    def updateScriptExecutionStatus(self, executionId, testScriptId, status):
        try:
            self.db.REDTTL.update(
                {'_id': str(executionId)},
                {'$set': {"testScriptStatusList." + testScriptId: status}})
            print "Updated the execution status of test script " + testScriptId + " to 'COMPLETE' having executionId " + str(executionId)
            Logs.logging.info("Updated the execution status of test script " +
                              testScriptId + " to 'COMPLETE' having executionId " + str(executionId))
        except Exception as e:
            Logs.logging.error(e)

    '''
    Method to update test pass execution status into the database
    '''

    def updateTestPassExecutionStatus(self, executionId):
        try:
            self.db.Executions.update(
                {'_id': str(executionId)},
                {'$set': {"executionStatus": "COMPLETE"}})
            print "Updated the status of test pass execution status of executionId " + str(executionId) + " to 'COMPLETE'"
            Logs.logging.info(
                "Updated the status of test pass execution status of executionId " + str(executionId) + " to 'COMPLETE'")
        except Exception as e:
            Logs.logging.error(e)

    '''
    Method to insert report execution status into the database
    '''

    def insertExecutionReportInfo(self, executionId, jsonReport_Str):
        try:
            self.db.Reports.insert(jsonReport_Str)

            self.db.Executions.update({"_id": str(executionId)}, {
                                      "$set": {"report": DBRef("Reports", "E-" + str(executionId))}})

            print "Inserted report execution status of executionId " + str(executionId) + " to 'COMPLETE'"
            Logs.logging.info(
                "Inserted report execution status of executionId " + str(executionId) + " to 'COMPLETE'")
        except Exception as e:
            Logs.logging.error(e)

    '''
    Method to updated report execution status into the database
    '''

    def updateExecutionReportInfo(self, executionId, jsonReport_Str):
        try:
            print executionId
            self.db.Reports.save(jsonReport_Str, {'_id': executionId})
            print "Updated report execution status of executionId " + str(executionId) + " to 'COMPLETE'"
            Logs.logging.info(
                "Updated report execution status of executionId " + str(executionId) + " to 'COMPLETE'")
        except Exception as e:
            Logs.logging.error(e)

    '''
    Method to close database connection
    '''

    def closeDBConnection(self):
        self.client.close()
        Logs.logging.info("Closed database connection")

    '''
    Method  to update driver machine execution status ie. the machine in use or not for Execution
    '''

    def updateDriverMachineExecutionStatus(self, machineId):
        try:
            self.db.Agents.update(
                {'_id': str(machineId)},
                {'$set': {"inUse": False}})
            print "Updated the execution status of driver machine with machineId " + str(machineId) + " to 'false'"
            Logs.logging.info(
                "Updated the execution status of driver machine with machineId " + str(machineId) + " to 'false'")
        except Exception as e:
            Logs.logging.error(e)

    '''
    Method  to update environment status ie. the environment in use or not for Execution
    '''

    def updateEnvironmentStatus(self, envId):
        try:
            self.db.Environments.update(
                {'_id': ObjectId(envId)},
                {'$set': {"inUse": False}})
            print "Updated the execution status of driver machine with machineId " + str(envId) + " to 'false'"
            Logs.logging.info(
                "Updated the execution status of driver machine with machineId " + str(envId) + " to 'false'")
        except Exception as e:
            Logs.logging.error(e)

    '''
    Method to add build number in the respective product
    '''

    def addBuildNumberInProduct(self, productId, buildNum):
        try:
            if(buildNum!= None):
                Logs.logging.info("build number is not null: "+str(buildNum))
                if(buildNum!=''):
                    self.db.Products.update(
                    {'_id': ObjectId(productId)}, {'$addToSet': {"builds": buildNum}})
                    print "Added build Num " + str(buildNum) + " to product " + productId
                    Logs.logging.info(
                    "Added build Num " + str(buildNum) + " to product " + productId)
        except Exception as e:
            Logs.logging.error(e)

    '''
    Method to upload screenshot into database
    '''

    def uploadScreenshotInDB(self, executionID, executionReportPath):
        try:
            #png_files = [f for f in os.listdir(executionReportPath) if f.endswith('.png')]
            for file in os.listdir(executionReportPath):
                import fnmatch
                if fnmatch.fnmatch(file, '*.png'):
                    if executionID in file:
                        datafile = open(executionReportPath + "/" + file, "rb")
                        thedata = datafile.read()
                        fs = gridfs.GridFS(self.db)
                        fs.put(thedata, filename=file)
            print "Screenshot uploaded..."
            Logs.logging.info("Screenshot uploaded")
        except Exception as e:
            Logs.logging.error(e)

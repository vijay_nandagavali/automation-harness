'''
Created on Nov 14, 2014

@author: amit.t
'''
     
import os
import subprocess
import configparser

from Config import Logs, rabbitmq_client_proc
from Handler import script_executor
from Util import constants
import time


uebUpdateScriptPath = rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/UEB_Update/invoke_ueb_update.sh"
machineRevertScriptPath = rabbitmq_client_proc.AGENT_BASE_DIR_PATH + "/Revert_Snapshot/revertSnapshotFunction.ps1"

'''
Method to update ueb version
'''  
def updateUEBVersion():
    if script_executor.checkSriptIsPresentOrNot(uebUpdateScriptPath):
        sp = subprocess.Popen(args=[uebUpdateScriptPath], shell=True)
        sp.wait()
        #script_executor.waitToCompleteTestScript(sp.pid)
        constants.PROCESSIDS.insert(len(constants.PROCESSIDS), sp.pid)    
    else:
        print uebUpdateScriptPath + " does not exists at location "
        Logs.logging.error(uebUpdateScriptPath + " does not exists at location ")    
        return False
    
def revertMachineSnapshot(vmDetails):
    if script_executor.checkSriptIsPresentOrNot(machineRevertScriptPath):
        #sp = subprocess.Popen(args=[machineRevertScriptPath, serverIp, serverUserName, serverPassword, vmame], shell=True)
        sp = subprocess.Popen([r'C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe',
                             '-ExecutionPolicy',
                             'Unrestricted',
                             machineRevertScriptPath, vmDetails["serverIp"], vmDetails["serverUserName"], vmDetails["serverPassword"], vmDetails["vmName"]], cwd=os.getcwd())
        sp.wait()
        #script_executor.waitToCompleteTestScript(sp.pid)
        constants.PROCESSIDS.insert(len(constants.PROCESSIDS), sp.pid)    
    else:
        print machineRevertScriptPath + " does not exists at location "
        Logs.logging.error(machineRevertScriptPath + " does not exists at location ")    
        return False
    
def configureMasterINI(configurationDetails):
    Logs.logging.info("In configureMasterINI()")
    Logs.logging.info("Configuration Details: \n"+str(configurationDetails))
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    config = configparser.ConfigParser()
    config.readfp(open(configurationDetails['INI_FilePath']))
    original_value = config.get(configurationDetails['BackupDevice'], configurationDetails['CompressionConfig'])
    print(configurationDetails['CompressionConfig']+" was: %s" % original_value)
    
    config.set(configurationDetails['BackupDevice'], configurationDetails['CompressionConfig'], configurationDetails['CompressionConfigVal'])
    config.set(configurationDetails['BackupDevice'], configurationDetails['CompressionConfigAdd'], configurationDetails['CompressionConfigAddVal'])
    print(original_value+" replaced by: %s" % configurationDetails['CompressionConfigVal'])
    print("Added: %s" % configurationDetails['CompressionConfigAdd'] + " = %s" %configurationDetails['CompressionConfigAddVal'])
    try: 
        with open(configurationDetails['INI_FilePath'], "wb") as fp:
            config.write(fp)
        successFlag = "true"
    except:
        print "Could not open file for writing!!!"   
        successFlag = "false"
    Logs.logging.info("Finished configureMasterINI()")
    return successFlag 

def AddNewPropertyInINIFile(configurationDetails):
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    config = configparser.ConfigParser()
    config.readfp(open(configurationDetails['INI_FilePath']))
    config.set(configurationDetails['section'], configurationDetails['key'], configurationDetails['value'])
    print("Added: %s" % configurationDetails['key'] + " = %s" %configurationDetails['value'])
    try: 
        with open(configurationDetails['INI_FilePath'], "wb") as fp:
            config.write(fp)
        successFlag = "true"
    except:
        print "Could not open file for writing!!!"   
        successFlag = "false"
    Logs.logging.info("Finished configureMasterINI()")
    return successFlag

def revertMasterINI(configurationDetails):
    Logs.logging.info("In revertMasterINI()")
    Logs.logging.info("Configuration Details: \n"+str(configurationDetails))
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    config = configparser.ConfigParser()
    config.readfp(open(configurationDetails['INI_FilePath']))
    original_value = config.get(configurationDetails['BackupDevice'], configurationDetails['CompressionConfig'])
    print(configurationDetails['CompressionConfig']+" was: %s" % original_value)
    
    config.set(configurationDetails['BackupDevice'], configurationDetails['CompressionConfig'], configurationDetails['CompressionConfigVal'])
    config.remove_option(configurationDetails['BackupDevice'], configurationDetails['CompressionConfigAdd'])
    
    print(original_value+" replaced by: %s" % configurationDetails['CompressionConfigVal'])
    print("Removed: %s" % configurationDetails['CompressionConfigAdd'])
    try: 
        with open(configurationDetails['INI_FilePath'], "wb") as fp:
            config.write(fp)
        successFlag = "true"
    except:
        print "Could not open file for writing!!!"   
        successFlag = "false"
    Logs.logging.info("Finished revertMasterINI()")
    return successFlag 

def verifybackupCompression(configurationDetails):
    Logs.logging.info("In verifybackupCompression()")
    successFlag  = "false"
    try:
        p = subprocess.Popen(["file", configurationDetails['BackupFileName']], stdout=subprocess.PIPE)
        out,err = p.communicate()
    except:
        successFlag = "false"
    if(out.strip()!= configurationDetails['BackupFileName']+": data"): 
        print("Error: Backup is not compressed")
        Logs.logging.error("Error: Backup is not compressed")
        successFlag = "false"
    else:
        print("Backup has been compressed")    
        Logs.logging.info("Backup has been compressed") 
        successFlag = "true"
    Logs.logging.info("Finished verifybackupCompression()")
    return successFlag
       
def restart_xinetd():
    Logs.logging.info("In restart_xinetd()")
    try:        
        p = subprocess.Popen(["service", "xinetd", "restart"], stdout=subprocess.PIPE)
        out,err = p.communicate()
        successFlag = "true"
    except:  
        successFlag = "false"
        Logs.logging.error("Error!")
    Logs.logging.info("Finished restart_xietd()")
    return successFlag

def check_verify_dedupe(configurationDetails):
    Logs.logging.info("In check_verify_dedupe()")
    successFlag = "false"
    tempFileLocation = "/tmp/out"
    dedupCheckCount = 60
    counter = 0
    
    #Dedup the backup
    time.sleep(120)
    startDedupCommand = "fileDedup -d" + configurationDetails["BackupID"]
    startDedup = subprocess.Popen(startDedupCommand, stdout=subprocess.PIPE, stderr=None, shell=True)
    startDedupOut,startDedupErr = startDedup.communicate()
    print startDedupOut
    
    while (counter < dedupCheckCount):
        try:
            counter += 1
            dedupCommand = "fileDedup -sv > " + tempFileLocation
            dedupBackupCommand = "grep -q " + configurationDetails["BackupID"] + " /tmp/out; echo $?"

            dedup = subprocess.Popen(dedupCommand, stdout=subprocess.PIPE, stderr=None, shell=True)
            dedupOut,dedupErr = dedup.communicate()
            
            print dedupOut
                      
            dedupPresent = subprocess.Popen(dedupBackupCommand, stdout=subprocess.PIPE, stderr=None, shell=True)
            dedupPresentOut,dedupPresentErr = dedupPresent.communicate()
              
            print configurationDetails["BackupID"]
            print counter
            print "Output::" + str(dedupPresentOut[0])
              
            if (str(dedupPresentOut[0]) == "0"):
                successFlag = "true"
                break
            else:
                time.sleep(10)
        except:
            print "Exception"
            time.sleep(10)
            successFlag = "false"
            continue
            Logs.logging.error("Error!")
    Logs.logging.info("Finished check_verify_dedupe()")
    print successFlag
    return successFlag 
       
       


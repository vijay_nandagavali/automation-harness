'''
This script to execute following client utilities:
Create Folder and create files to it.
Add files to a folder.
Modify Files from folder
Delete files from folder
'''
from Config import Logs
import os
from Client_Utilities import create_folder, delete_files_from_folder, execute_scripts, read_xml_file, delete_folder
from Client_Utilities import add_files_to_folder, modify_files_in_folder, restore_verification, ueb_operations
from Client_Utilities import read_file_in_folder, database


def executeClientUtilities(utilityDetails):
    Logs.logging.info("In executeClientUtilities()")
    actionFlag = "false"
    Logs.logging.info("Utility Details: \n"+str(utilityDetails))
    path_to_change = os.path.dirname(os.path.abspath(__file__))
    
    if(utilityDetails['UtilityType'] == 'AddBackupFolder'):
        actionFlag = create_folder.create_folder_on_machine(utilityDetails['DirName'], utilityDetails['FileCount'], utilityDetails['FolderType'])

    elif(utilityDetails['UtilityType'] == 'AddDedupFolder'):
        actionFlag = create_folder.create_dedup_folder_on_machine(utilityDetails['DirName'])
        
    elif(utilityDetails['UtilityType'] == 'AddFiles'):
        actionFlag = add_files_to_folder.add_files(utilityDetails['DirName'], utilityDetails['FileCount'], utilityDetails['FolderType'])
        
    elif(utilityDetails['UtilityType'] == 'Add8MbFiles'):
        actionFlag = add_files_to_folder.add_8MB_files(utilityDetails['DirName'], utilityDetails['FileCount'], utilityDetails['FolderType'])
        
    elif(utilityDetails['UtilityType'] == 'AddCustomSizedFiles'):
        actionFlag = add_files_to_folder.add_custom_sized_files(utilityDetails['DirName'], utilityDetails['FileCount'], utilityDetails['FolderType'],utilityDetails['FileSize'],utilityDetails['SizeType'])
        
    elif(utilityDetails['UtilityType'] == 'ModifyFile'):
        actionFlag = modify_files_in_folder.modify_files(utilityDetails['DirName'], utilityDetails['FileName'])
        
    elif(utilityDetails['UtilityType'] == 'ReadTextFile'):
        actionFlag = read_file_in_folder.read_file_for_text(utilityDetails['DirName'], utilityDetails['FileName'], utilityDetails['SearchString'])
        
    elif(utilityDetails['UtilityType'] == 'CompareFileTimeStamp'):
        actionFlag = read_file_in_folder.compare_file_currentTs_modifiedTs(utilityDetails['DirName'], utilityDetails['FileName'])
    
    elif(utilityDetails['UtilityType'] == 'VerifySQLRestore'):
        actionFlag = restore_verification.check_folder_exist(utilityDetails['VerifyFolder'])
        
    elif(utilityDetails['UtilityType'] == 'DeleteFolder'):
        actionFlag = delete_folder.delete_folder_on_machine(utilityDetails['DirName'])
        
    elif(utilityDetails['UtilityType'] == 'verifybackupCompression'):
        actionFlag = ueb_operations.verifybackupCompression(utilityDetails)        
        
    elif(utilityDetails['UtilityType'] == 'restart_xinetd'):
        actionFlag = ueb_operations.restart_xinetd()
        
    elif(utilityDetails['UtilityType'] == 'ConfigureMasterINI'):
        actionFlag = ueb_operations.configureMasterINI(utilityDetails)    
    
    elif(utilityDetails['UtilityType'] == 'VerifyRestoreBackup'):
        actionFlag = restore_verification.verifyRestoreFolderAgainstBackupFolder(utilityDetails['backupPath'], utilityDetails['restorePath']) 
        
    elif(utilityDetails['UtilityType'] == 'DeleteFile'):
        actionFlag = delete_files_from_folder.delete_files(utilityDetails['DirName'], utilityDetails['FileName'])

    elif(utilityDetails['UtilityType'] == 'RevertMasterINI'):
        actionFlag = ueb_operations.revertMasterINI(utilityDetails)
    
    elif(utilityDetails['UtilityType'] == 'VerifyDedup'):
        actionFlag = ueb_operations.check_verify_dedupe(utilityDetails)  
    
    elif(utilityDetails['UtilityType'] == 'AddNewPropertyInINIFile'):
        actionFlag = ueb_operations.AddNewPropertyInINIFile(utilityDetails)
        
    elif(utilityDetails['UtilityType'] == 'ExecuteBatchScript'):
        actionFlag = execute_scripts.execute_batch_script(utilityDetails['ScriptPath'],utilityDetails['ScriptName'],utilityDetails['ScriptArguments'],utilityDetails['ScriptArgumentPath'])
    
    elif(utilityDetails['UtilityType'] == 'ExecutePowershellScript'):
        actionFlag = execute_scripts.execute_powershell_script(utilityDetails['VmName'])
    
    elif(utilityDetails['UtilityType'] == 'ExecutePowershellScriptFullResponse'):
        actionFlag = execute_scripts.execute_powershell_script_full_response(utilityDetails['Command'])
    
    elif(utilityDetails['UtilityType'] == 'getPathforVM'):
        actionFlag = execute_scripts.get_Path_for_VM(utilityDetails['vmID'])    
        
    elif(utilityDetails['UtilityType'] == 'ReadXmlFile'):
        actionFlag = read_xml_file.read_xml_file(utilityDetails['FilePath'],utilityDetails['FileName'],utilityDetails['TagName']) 
    
    elif(utilityDetails['UtilityType'] == 'ExecuteIOGenerator'):
        actionFlag = execute_scripts.readIOGeneratorInputFile(utilityDetails['ScriptPath'],utilityDetails['ScriptName'],utilityDetails['ScriptArguments'])
    
    elif(utilityDetails['UtilityType'] == 'GetChangedData'):
        actionFlag = execute_scripts.getDataWriteInformation(utilityDetails['ScriptPath'],utilityDetails['ScriptName'],utilityDetails['ScriptArguments'],utilityDetails['ScriptArgumentPath'],utilityDetails['FirstSnapshotId'],utilityDetails['SecondSnapshotId'])
    
    elif(utilityDetails['UtilityType'] == 'GetCBTOutputChangeResult'):
        actionFlag = execute_scripts.getCbtChangeResult(utilityDetails['ScriptPath'],utilityDetails['ScriptName'],utilityDetails['OLDataFilePath'])
    
    elif(utilityDetails['UtilityType'] == 'PerformCleanupActivity'):
        actionFlag = execute_scripts.performCleanupActivity(utilityDetails['ScriptPath'],utilityDetails['ScriptName'],utilityDetails['ScriptArguments'],utilityDetails['VmName'])
        
    elif(utilityDetails['UtilityType'] == 'UpdateUEB'):
        actionFlag = ueb_operations.updateUEBVersion(utilityDetails)
        
    elif(utilityDetails['UtilityType'] == 'CheckRestoreFilePath'):
        actionFlag = restore_verification.check_folder_exist(utilityDetails['DirectoryPath'])
    
    elif(utilityDetails['UtilityType'] == 'CheckRestoreFileCount'):
        actionFlag = restore_verification.verify_restored_filecount(utilityDetails['DirectoryPath'])
    
    elif(utilityDetails['UtilityType'] == 'CreateDB'):
        actionFlag = database.createDb(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'])
    
    elif(utilityDetails['UtilityType'] == 'DeleteDB'):
        actionFlag = database.deleteDb(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'])
    
    elif(utilityDetails['UtilityType'] == 'CreateTableWithValues'):
        actionFlag = database.createTablewithValues(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'], utilityDetails['tableCount'])

    elif(utilityDetails['UtilityType'] == 'VerifyTableexistsinDatabase'):
        actionFlag = database.verifyTableexistsinDatabase(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'], utilityDetails['tableName'])

    elif(utilityDetails['UtilityType'] == 'VerifyListedDirectoriesInFolder'):
        actionFlag = restore_verification.verifylisteddirectoriesinfolder(utilityDetails['backupPath'])

    elif(utilityDetails['UtilityType'] == 'createColumnMasterKey'):
        actionFlag = database.createColumnMasterKey(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'])
   
    elif(utilityDetails['UtilityType'] == 'createColumnEncryptionKey'):
        actionFlag = database.createColumnEncryptionKey(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'])
    
    elif(utilityDetails['UtilityType'] == 'createEncryptedTable'):
        actionFlag = database.createEncryptedTable(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'])
    
    elif(utilityDetails['UtilityType'] == 'createEncryptedTableProcedure'):
        actionFlag = database.createEncryptedTableProcedure(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'])

    elif(utilityDetails['UtilityType'] == 'createServerAuthenticateLogin'):
        actionFlag = database.createServerAuthenticateLogin(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'])

    elif(utilityDetails['UtilityType'] == 'createDatabaseUser'):
        actionFlag = database.createDatabaseUser(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'])

    elif(utilityDetails['UtilityType'] == 'execute_batch_script_EncryptedDb'):
        actionFlag = database.ExecuteScript()

    elif(utilityDetails['UtilityType'] == 'DeleteUser'):
        actionFlag = database.deleteUser(utilityDetails['serverName'], utilityDetails['userId'], utilityDetails['passwd'], utilityDetails['testDBName'])
    
    
    os.chdir(path_to_change)
    Logs.logging.info("Finished executeClientUtilities()")
    return actionFlag

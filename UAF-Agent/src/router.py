
'''
This file will act as placeholder for all the functions which will facilitate communication between parser and the executor program
Author : Afour Technologies

'''

from Handler import client_utility_handler
from Handler import script_executor
from Config import Logs
def executeScript(arg):
	Logs.logging.info("Executing "+str(arg['testScriptFileName']))	
	script_executor.execute(arg['testScriptFileName'],arg['testScriptLanguage'])
		
def executeMultipleScripts(scriptsDetails, exeId):
	Logs.logging.info("Executing "+str(scriptsDetails) +"Execution Id: "+str(exeId))	
	script_executor.executeScripts(scriptsDetails, exeId)

def executeClientUtilities(utilityDetails):
	Logs.logging.info("Executing client utilities: "+str(utilityDetails))
	return client_utility_handler.executeClientUtilities(utilityDetails)
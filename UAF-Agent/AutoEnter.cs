using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
// Demo of using Always Encrypted Columns
class AlwaysEncryptedDemo
{
    SqlConnection conn;
    public AlwaysEncryptedDemo()
    {
        // Instantiate the connection
        conn = new SqlConnection(
          "data source=WIN-FFOGI9IOVH9;initial catalog=TestDB_Auto;integrated security = False; Column Encryption Setting=Enabled; User ID = Greg; Password = Test;");
    }

    // call methods that demo Always Encrypted
    static void Main()
    {
        AlwaysEncryptedDemo scd = new AlwaysEncryptedDemo();
        scd.Insertdata();
        scd.Selectdata();
    }

    public void Insertdata()
    {
        try
        {
            // Open the connection for Insertion 
            conn.Open();

            // Constructed command to execute stored proceudre
            string insertString = @"dbo.Insert_Always_Encrypted_DB";

             // Declare variable tho hold insdert command
            SqlCommand icmd = new SqlCommand(insertString, conn);

             //set command type to stored procedure
            icmd.CommandType = CommandType.StoredProcedure;

             // Set value of LastName
            SqlParameter paramLastName = icmd.CreateParameter();
            paramLastName.ParameterName = @"@FIRSTNAME";
            paramLastName.DbType = DbType.AnsiStringFixedLength; ;
            paramLastName.Direction = ParameterDirection.Input;
            paramLastName.Value = "Nikita";
            icmd.Parameters.Add(paramLastName);

            // Set value of LastName
            SqlParameter paramFirstName = icmd.CreateParameter();
            paramFirstName.ParameterName = @"@LASTNAME";
            paramFirstName.DbType = DbType.AnsiStringFixedLength; ;
            paramFirstName.Direction = ParameterDirection.Input;
            paramFirstName.Value = "LAD";
            icmd.Parameters.Add(paramFirstName);

             // Set value of Birth Date
            SqlParameter
            paramBirthdate = icmd.CreateParameter();
            paramBirthdate.ParameterName = @"@SALARY";
            paramBirthdate.DbType = DbType.Decimal;
            paramBirthdate.Direction = ParameterDirection.Input;
            paramBirthdate.Value = 201545623456;
            icmd.Parameters.Add(paramBirthdate);

            // Set value of SSN
            //SqlParameter
            //paramSSN = icmd.CreateParameter();
            //paramSSN.ParameterName = @"@SSN";
            //paramSSN.DbType = DbType.AnsiStringFixedLength;
            //paramSSN.Direction = ParameterDirection.Input;
            //paramSSN.Value = "555-55-5555";
            //paramSSN.Size = 10;
            //icmd.Parameters.Add(paramSSN);
 
            // Exexute Insert 
            icmd.ExecuteNonQuery();
            MessageBox.Show("Inserted Demo Record With Salary=" + paramBirthdate.Value);

        }
        finally
        {
            // Close the connection
            if (conn != null)
            {
                conn.Close();
            }
        }
    }
   // public void Selectdata()
   // {
     //   try
       // {
            // Open the connection for Selection 
         //   conn.Open();

             // Read Encrypted data 
            //string selectString = @"SELECT FIRSTNAME, LASTNAME, SALARY FROM [dbo].[TestTable]";
            //SqlCommand scmd = new SqlCommand(selectString, conn);
            //SqlDataReader dataRead = scmd.ExecuteReader();
            //while (dataRead.Read())
            //{
              //  MessageBox.Show("Selected Data with FIRSTNAME=" + dataRead["FIRSTNAME"] + " LASTNAME=" + dataRead["LASTNAME"] +" SALARY =" + dataRead["SALARY"]);
            //}
    //    }
      //  finally
       // {
            // Close the connection
         //   if (conn != null)
          //  {
            //    conn.Close();
           // }
       // }
   // }
}
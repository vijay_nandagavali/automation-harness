Add-PSSnapin VMware.VimAutomation.Core
#Set-PowerCLIConfiguration -ProxyPolicy NoProxy

$esx_server_ip = $args[0]
echo $esx_server_ip
$esx_server_username = $args[1]
echo $esx_server_username
$esx_server_password = $args[2]
echo $esx_server_password
$vmmachine_name = $args[3]
echo $vmmachine_name

Connect-VIServer -Server $esx_server_ip -Protocol https -User $esx_server_username -Password $esx_server_password

$snap = Get-Snapshot -VM $vmmachine_name | Sort-Object -Property Created -Descending | Select -First 1
Set-VM -VM $vmmachine_name -SnapShot $snap -Confirm:$false
Start-VM -VM $vmmachine_name -Confirm:$false | Out-Null 
[Environment]::Exit(0)

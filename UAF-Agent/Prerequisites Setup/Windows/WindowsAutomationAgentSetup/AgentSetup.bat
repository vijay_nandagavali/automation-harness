@echo OFF
:: Run as administrator
goto check_Permissions
:check_Permissions
    echo Administrative permissions required. Detecting permissions...

    net session
    if %errorLevel% == 0 (
        echo Success: Administrative permissions confirmed.
    ) else (
        echo Failure: Current permissions inadequate.
        goto EndOfScript
    )
:: Checked python is installed or not 
:: Check for Python Installation
Python -V 
if errorlevel 1 goto NOPYTHON
:: Reaching here means Python is installed.
:: Execute stuff...
else goto HASPYTHON
:NOPYTHON
        cd %~dp0
	::msiexec /i python-2.7.7.msi /q
	msiexec /i "https://www.python.org/ftp/python/2.7.7/python-2.7.7.msi" /q
	:: Wait to complete installation of python	 
	Timeout 5
        c:\python27\python.exe --version >NUL 2>&1
        echo Python.exe found in C:\Python27
        if errorlevel 1 goto EndOfScript
        set path="%PATH%";c:\Python27;C:\WINDOWS\system32
        setx path %PATH%
        echo python2.7 installed successfully.
        goto HASPYTHON

:HASPYTHON
:: Install 'Easy' installer
python "%~dp0\ez_setup.py"
if errorlevel 1 goto EndOfScript				
echo Easy Installer successfully installed...

:: Set environment variable path for 'Easy' installer
set path=%PATH%;c:\Python27\Scripts
:: Set environment variable for python
setx path %PATH%
:: Install pip installer and pymongo using 'Easy' installer
IF EXIST c:\Python27\Scripts\easy_install.exe (
	easy_install pip
	if errorlevel 1 goto EndOfScript				
        echo pip successfully installed... 	

	easy_install pymongo
	if errorlevel 1 goto EndOfScript				
        echo pymongo successfully installed... 	
	)
) ELSE (
	echo Falied to install easy_install
)
:: Install pika using pip installer
pip install pika
if errorlevel 1 goto EndOfScript				
echo Pika Install Successfully

:: Install xmltodict using pip installer
pip install xmltodict
if errorlevel 1 goto EndOfScript
echo xmltodict package install Successfully

:: Install psutil package (Require to run message receiver python scirpt)
easy_install psutil-2.1.1.win32-py2.7.exe 
if errorlevel 1 goto EndOfScript
echo Installed PsUtil successfully

:: Install pycrypto
easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
if errorlevel 1 goto EndOfScript
echo Installed pycrypto successfully

:: Install paramiko
easy_install paramiko-1.7.7.1.win32-py2.7.exe 
if errorlevel 1 goto EndOfScript
echo Installed Paramiko successfully

pip install configparser
if errorlevel 1 goto EndOfScript
pip install pyodbc
if errorlevel 1 goto EndOfScript

:EndOfScript

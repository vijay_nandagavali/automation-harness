ver=`python --version 2>&1` # Command
if [ $? -eq 0 ]
then
# When Python is installed
  echo "$ver is already installed"
   # Install Pika for debian based linux   
   # Install pip  
   response=`apt-get install python-pip git-core -y 2>&1` # Command         
   if [[ "$response" == *command?not?found* ]]
   then	
    # Install Pika for red hat based linux  	
	python ez_setup.py # Command
	if [ $? != 0 ]
	then	
		echo "Failed to install ez_setup"
		exit 0	
 	fi
	# easy install pip
	easy_install pip
	if [ $? != 0 ]
	then	
		echo "Failed to install pip"
		exit 0	
 	fi
	# easy install pymongo
	easy_install pymongo
	if [ $? != 0 ]
	then	
		echo "Failed to install pymongo"
		exit 0	
 	fi
	# pip install pika
	pip install pika
	if [ $? != 0 ]
	then	
		echo "Failed to install pika"
		exit 0	
 	fi	
	# Install psutil package to run message receiver python script
	pip install psutil
	if [ $? != 0 ]
	then	
		echo "Failed to install psutil"
		exit 0	
 	fi
   else
	 # Install Pika
  	 pip install pika # Command
  	 if [ $? != 0 ]
	 then	
		echo "Failed to install Pika"
		exit 0
	 fi
	 # Install pymongo
	 pip install pymongo # Command
  	 if [ $? != 0 ]
	 then	
		echo "Failed to install pymongo"
		exit 0
	 fi
   fi   
   # Install psutil package to run message receiver python script
   apt-get install python-psutil
   if [ $? != 0 ]
   then	
   	echo "Failed to install psutil package"
   exit 0
   fi
else 
	echo "Please install Python with version > 2.6"
fi
exit 0
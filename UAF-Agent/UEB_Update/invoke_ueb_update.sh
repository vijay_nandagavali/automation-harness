#! /bin/bash

oldrelease=`rpm -qi unitrends-rr | sed -n -e 3p -e 3q | cut -c17-24`
cd /etc/yum.repos.d/
wget $2
echo "Repository file downloaded"
yum -y --nogpgcheck update
echo "Repository update successful"
rm -rf $3
crelease=`rpm -qi unitrends-rr | sed -n -e 3p -e 3q | cut -c17-24`
#currentdate=`/bin/date +%Y%m%d`
if [ $crelease -ge $oldrelease ]
then echo "Build update successful"
fi

input=`cat /usr/bp/bp_VERS | grep "Version" | cut -c9-13`
echo $input

while [ $input == "$1" ]
do
sleep 10
echo "Upgrade in progress"
if [ $input != "$1" ];then
break
else
input=`cat /usr/bp/bp_VERS | grep "Version" | cut -c9-13`
continue
fi
done
echo "Upgrade completed"
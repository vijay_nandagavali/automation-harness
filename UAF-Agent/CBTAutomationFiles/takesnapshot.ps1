$VMName = $args[0]

$DateFormat = Get-Date -Format yyyy.MM.dd
$Snapshot = $VMName + "-" + $DateFormat
 
CheckPoint-VM -Name $VMName -Snapshotname $Snapshot 
Start-Sleep -Seconds 2
 
Write-Host "Snapshot done"